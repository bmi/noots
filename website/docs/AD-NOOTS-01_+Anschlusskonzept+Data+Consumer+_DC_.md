>**Redaktioneller Hinweis**
>
>Dokument aus der vorangegangen Iteration - nicht Teil der aktuellen Iteration.
>
>Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## Einleitung

Das Anschlusskonzept Data Consumer erklärt die notwendigen Schritte zum
Anschluss an das <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS]</a>
und zum Abruf von Nachweisen über das NOOTS. Es richtet sich an IT-Verantwortliche der öffentlichen
Verwaltung mit Verantwortung für Onlinedienste und sonstige technische
Verfahren, die zum Nachweisabruf befähigt werden sollen.

### Überblick

Das Nationale Once-Only Technical System (NOOTS) ist ein technisches
System zur Vorbereitung und Durchführung von Nachweisabrufen. Es ist in
der <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">High-Level-Architecture des NOOTS (HLA)</a> mit seinen Bestandteilen, querschnittlichen und planerischen Aspekten
gesamthaft beschrieben. Das Anschlusskonzept Data Consumer setzt dessen
Kenntnis voraus und beschreibt die Aspekte eines Data Consumers und der
für ihn verantwortlichen Stellen aus deren Perspektive. <p>Das
Anschlusskonzept ist wie folgt aufgebaut:

-   Das Kapitel [Einleitung](#einleitung)
    beschreibt grundlegende Begriffe und Zusammenhänge.
-   Das Kapitel
    [Anforderungen](#anforderungen) beschreibt
    die von Data Consumern zu beachtenden Anforderungen sowie deren
    Herkunft und Umsetzung im weiteren Verlauf des Dokuments.
-   Das Kapitel
    [Kommunikationsbeziehungen](#kommunikationsbeziehungen)
    beschreibt die Kommunikation von Data Consumern mit NOOTS
    Komponenten und Data Providern im Überblick.
-   Das Kapitel [Voraussetzungen für die Teilnahme am NOOTS
    erfüllen](#voraussetzungen-für-die-teilnahme-am-noots-erfüllen) beschreibt die
    Registrierung und Anbindung von Data Consumern.
-   Die anschließenden Kapitel beschreiben die Vorbereitung und
    Durchführung von Nachweisabrufen für die Anwendungsfälle
    [1](#hla-1-nationalen-nachweis-interaktiv-abrufen),
    [2](#hla-2-nationalen-nachweis-nicht-interaktiv-abrufen) und
    [4](#hla-4-nachweis-aus-eu-mitgliedstaat-abrufen) der HLA.

Das nachfolgende Schaubild fasst die wesentlichen Schritte für einen
erfolgreichen Nachweisabruf durch Data Consumer zusammen.

![C:\\a090fd0d581ab3acb8b3cd36cee663e6](./assets/images1/media/image1.png)
**Abb. 1: Überblick**

### Nachweissubjekt und Identifikatoren

Ein Nachweissubjekt ist eine natürliche Person oder ein Unternehmen im
Sinne des § 3 Abs. 1 UBRegG, zu der/dem ein Nachweis abgerufen wird.

Die Identifikatoren einer natürlichen Person sind

-   deren Basisdaten gemäß § 6 (3) IDNrG, also mindestens Familienname,
    Wohnort, Postleitzahl und Geburtsdatum, sowie
-   deren Identifikationsnummer (IDNr) gemäß § 1 IDNrG.

Die Identifikatoren eines Unternehmens im Sinne des § 3 Abs. 1 UBRegG
sind

-   dessen Basisdaten, bestehend aus Stammdaten und
    Identifikationsnummern gemäß § 3 Abs. 2 und 3 UBRegG, sowie
-   dessen bundeseinheitliche Wirtschaftsnummer (beWiNr) gemäß § 2 (1)
    UBRegG.

### Arten von Nachweisabrufen

Das NOOTS ermöglicht folgende Arten von Nachweisabrufen:

-   **Interaktiv**: Nachweisabruf im Dialog mit einer Person, die zu
    sich selbst oder in Vertretung des
    [Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s zu
    diesem einen Nachweis abruft; die Antwort muss in kurzer Zeit zur
    Verfügung stehen (siehe <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">Anforderungen an das NOOTS-Gesamtsystem in
    der High-Level-Architecture des
    NOOTS.</a>
    Onlinedienste und Intermediäre Plattformen rufen Nachweise
    interaktiv ab.
-   **Nicht-interaktiv**: Nachweisabruf ohne Beteiligung des
    [Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s oder
    seines Vertreters; hier ist eine lange Antwortzeit (bis zu mehreren
    Tagen) erlaubt. Fachverfahren (Leistungs- und Eingriffsverwaltung)
    und Verfahren für Zwecke von Zensus und Wissenschaft rufen Nachweise
    nicht-interaktiv ab.

Ein Data Consumer kann über das NOOTS

-   einen nationalen Nachweis interaktiv abrufen
    [(HLA-1)](#hla-1-nationalen-nachweis-interaktiv-abrufen),
-   einen nationalen Nachweis nicht-interaktiv abrufen
    [(HLA-2)](#hla-2-nationalen-nachweis-nicht-interaktiv-abrufen) und
-   einen Nachweis aus einem anderen EU-Mitgliedstaat interaktiv
    abrufen [(HLA-4)](#hla-4-nachweis-aus-eu-mitgliedstaat-abrufen).

### Technische Verfahren zum Nachweisabruf

Technische Verfahren zum Nachweisabruf gemäß der Anwendungsfälle der HLA
sind Onlinedienste, Intermediäre Plattformen, Fachverfahren und
Verfahren für Zwecke von Zensus und Wissenschaft.

![C:\\1a5c00304c5745dbde06ebd275f4f31c](./assets/images1/media/image2.png)
**Abb. 2: Technische Verfahren**


Die Farbgebung der technischen Verfahren bezieht sich auf deren
Beschreibung in diesem Anschlusskonzept und wird dort jeweils erläutert.

#### Onlinedienst

Ein Onlinedienst ist das technische Verfahren zum Abruf von Nachweisen
nach den Anwendungsfällen 1 und 4 der HLA. Er wird entweder eigenständig
(autark) oder gemeinsam mit anderen Onlinediensten auf einer technischen
Onlinedienst-Plattform betrieben und kann über Portale gefunden und
aufgerufen werden. Im Sinne des NOOTS gilt:

-   Ein **Portal** (Verwaltungs-Portal, Portalverbund) dient zum Finden
    und Aufrufen von Onlinediensten. Portale sind für das NOOTS nicht
    relevant.
-   Eine **Onlinedienst-Plattform** stellt Onlinediensten technische
    Funktionalität zur Verfügung, die diese u.a. zum Nachweisabruf
    benötigen. Bietet ein Portal auch Onlinedienst-Plattform
    Funktionalität, ist es im NOOTS-Sinn eine Onlinedienst-Plattform.
-   Ein **abhängiger Onlinedienst** ruft Nachweise über eine
    Onlinedienst-Plattform ab, auf der er betrieben wird. Ein abhängiger
    Onlinedienst nimmt technisch indirekt über seine
    Onlinedienst-Plattform am NOOTS teil.
-   Ein **autarker Onlinedienst** ruft Nachweise eigenständig ab (nutzt
    dafür kein anderes technisches System). Ein autarker Onlinedienst
    nimmt technisch direkt am NOOTS teil.

![C:\\162d95ec7d206b60fc5acc4b9c0b44b0](./assets/images1/media/image3.png)
**Abb. 3: Technischen Verfahren - Onlinedienste**

#### Fachverfahren

Fachverfahren sind technische Verfahren zur Bearbeitung von
Verwaltungsvorgängen der Leistungs- und der Eingriffsverwaltung ohne
unmittelbare Beteiligung Betroffener. Fachverfahren rufen Nachweise auf
Veranlassung Sachbearbeitender nicht-interaktiv ab. Fachverfahren sind
zudem auf den Abruf nationaler Nachweise beschränkt, da ein
Nachweisabruf über das EU-OOTS nur interaktiv, also mit unmittelbarer
Beteiligung Betroffener, möglich ist.

Fachverfahren sind folglich technische Verfahren zum nicht-interaktiven
Abruf von nationalen Nachweisen nach dem Anwendungsfall 2 der HLA.

#### Verfahren für Zensus und Wissenschaft

Verfahren für Zensus und Wissenschaft sind technische Verfahren zum
nicht-interaktiven Abruf von Registerdaten nach den Anwendungsfällen 5
und 6 der HLA.

Die Anwendungsfälle 5 und 6 der HLA werden in einer späteren Version
dieses Anschlusskonzepts beschrieben.

#### Intermediäre Plattform

Der Anwendungsfall 3 der HLA beschreibt den Abruf von nationalen
Nachweisen durch technische Verfahren von EU-Mitgliedstaaten über das
Europäische Once-Only-Technical-System (EU-OOTS). Die Intermediäre
Plattform agiert hierbei als Mittler und ruft u.a. den nationalen
Nachweis über das NOOTS ab. Damit ist sie zwar ebenfalls ein
[technisches Verfahren zum
Nachweisabruf](#technische-verfahren-zum-nachweisabruf), allerdings mit
deutlichen Abweichungen zu den sonstigen technischen Verfahren zum
Nachweisabruf.

Die Schritte einer Intermediären Plattform beim Nachweisabruf werden
daher nicht in diesem Anschlusskonzept Data Consumer, sondern im
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">Grobkonzept Intermediäre
Plattform</a>
beschrieben. Bei nachfolgenden Nennungen technischer Verfahren zum
Nachweisabruf ist die Intermediäre Plattform ausgeschlossen.

### Nachweisanfordernde Stelle

Ein Onlinedienst wird von einer nachweisanfordernden Stelle gemäß §5 (2)
S. 2 EGovG verantwortet. Die Varianten nachweisanfordernder Stellen und
die dafür verwendeten Begriffe *Antragbearbeitende Behörde* (vgl. NL
1 [EfA-Mindestanforderungen des
IT-Planungsrats](https://www.it-planungsrat.de/beschluss/beschluss-2022-02-al))
und *Nachweisabrufvertreter* werden nachfolgend dargestellt.

![Varianten nachweisanfordernder
Stellen](./assets/images1/media/image4.png)
<br>**Abb. 4: Nachweisanfordernde Stellen**</br>

Antragbearbeitende Behörden können abgerufene Nachweise von anderen
öffentlichen Stellen (Nachweisabrufvertretern) weitergeleitet bekommen,
die beispielsweise Onlinedienste zur Beantragung von OZG-Leistungen und
zum Abruf entsprechender Nachweise anbieten. 

Die für den Nachweisabruf durch ein Fachverfahren oder ein Verfahren für
Zensus oder Wissenschaft verantwortliche Stelle benötigt für die
Berechtigung zum Nachweis eine eigene Rechtsgrundlage. Die hier
verantwortliche Stelle wird nachfolgend zur besseren Verständlichkeit
ebenfalls als nachweisanfordernde Stelle bezeichnet.

#### Verantwortung

Eine [nachweisanfordernde
Stelle](#nachweisanfordernde-stelle) ist für ein
technisches Verfahren zum Nachweisabruf insgesamt verantwortlich. Sie
kann Dritte mit der Umsetzung bestimmter Aspekte beauftragen, bspw.
Konzeption, Realisierung oder Betrieb.

Das NOOTS muss die Nachvollziehbarkeit eines Nachweisabrufs durch
geeignete Protokollierung unterstützen und dazu Informationen zum
fachlich und zum technisch Verantwortlichen ermitteln. Im Sinne des
NOOTS umfasst

-   die <ins>fachliche Verantwortung</ins> die rechtlichen und
    gestalterischen Aspekte des technischen Verfahrens (bspw. Zweck,
    Rechtsgrundlage, Oberflächen, benötigte Nachweise) sowie dessen
    Bereitstellung zum Betrieb,
-   die <ins>technische Verantwortung</ins> die Schaffung der
    technischen Voraussetzungen zum Betrieb des technischen Verfahrens
    (z.B. Betriebsumgebung, Netzanschluss, Zertifikate) und dessen
    technische Verfügbarkeit.

Ein technisches Verfahren zum Nachweisabruf über das NOOTS muss daher
einem Fachverantwortlichen und einem Betriebsverantwortlichen zugeordnet
sein, die bei jedem Nachweisabruf ermittelt und protokolliert werden.
Diese werden wie folgt bestimmt:

-   Die für das technische Verfahren verantwortliche
    [nachweisanfordernde
    Stelle](#nachweisanfordernde-stelle) ist der
    **Fachverantwortliche**.
-   Die das technische Verfahren betreibende Stelle ist der
    **Betriebsverantwortliche**. Bei einem autarken
    [Onlinedienst](#onlinedienst) ist der
    Betriebsverantwortliche der technisch Verantwortliche für den
    Onlinedienst; bei einem abhängigen
    [Onlinedienst](#onlinedienst) ist der
    Betriebsverantwortliche der technisch Verantwortliche für die
    Onlinedienst-Plattform, auf der der abhängige Onlinedienst betrieben
    wird.

Betreibt der Fachverantwortliche das technische Verfahren selbst, ist er
zugleich dessen Betriebsverantwortlicher.

### Data Consumer

Ein **Data Consumer** ist ein NOOTS-Teilnehmer zum Abruf von Nachweisen.
Ein NOOTS-Teilnehmer ist ein registriertes technisches Verfahren zur
Teilnahme in einer bestimmten Weise (Teilnahmeart), authentifizierbar
durch ein Zertifikat.

Ein Data Consumer hat stets einen Fachverantwortlichen und einen
Betriebsverantwortlichen (siehe
[Verantwortung](#verantwortung)).

Der **Fachverantwortliche** eines Data Consumers ist eine
nachweisabrufende Stelle gemäß §5 (2) S. 2 EGovG. Er verantwortet

-   die Rechtskonformität der veranlassten Nachweisabrufe,
-   die Korrektheit der Daten eines Nachweisabrufs, insbesondere der
    Basisdaten des
    [Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s und
    deren Vertrauensniveau, und
-   als Nachweisabrufvertreter die Weiterleitung abgerufener Nachweise
    an die [antragbearbeitende
    Behörde](#nachweisanfordernde-stelle).

Der **Betriebsverantwortliche** eines Data Consumers betreibt den Data
Consumer im Auftrag des Fachverantwortlichen. Er verantwortet

-   den technisch korrekten Betrieb des Data Consumers und
-   die Beschaffung und Verwendung der zum Betrieb des Data Consumers
    notwendigen elektronischen Zertifikate.

### Abgrenzung

Das Anschlusskonzept Data Consumer beschreibt die technischen und
organisatorischen Schritte zum elektronischen Abruf einzelner Nachweise.
Es enthält keine Aussagen

-   zu Bezahlfunktionen; die Abwicklung von Zahlungen für
    kostenpflichtige Nachweisabrufe liegt außerhalb des Regelungsumfangs
    von NOOTS und ist daher zusätzlich von dem technischen Verfahren
    (insb. vom Onlinedienst) abzudecken, das als Data Consumer Nachweise
    über das NOOTS abruft,
-   zur Bündelung von Nachweisabrufen; dies ist derzeit weder in der
    Konzeption noch im Standard XNachweis vorgesehen,
-   zur Weiterleitung von durch
    [Nachweisabrufvertreter](#nachweisanfordernde-stelle)
    abgerufenen Nachweisen an die antragbearbeitende Behörde; die
    Weiterleitungspflicht gemäß der §§ 5 und 5a EGovG ist unabhängig vom
    NOOTS zu erfüllen,
-   zum geplanten
    Fachdatenkonzept
    des PB Register, da es noch keinen belastbaren Stand aufweist,
-   zu Hilfsmitteln, die den eigentlichen Nachweisabruf für ein
    technisches Verfahren übernehmen; die Delegation des Nachweisabrufs
    an ein solches Hilfsmittel ist zulässig, aber nicht Bestandteil
    dieses Konzepts.

### Lesehilfe für Diagramme

In den nachfolgenden Diagrammen werden für Aktionen folgende Farben und
Symbole verwendet:

| Farbe / Symbol                 | Beispiel | Bedeutung                                                                                                       |
|--------------------------------|----------|-----------------------------------------------------------------------------------------------------------------|
| Blaue Schrift auf weißem Grund | ![C:\\27df3d7828625984fd5f25f7cca6bd22](./assets/images1/media/image5.png)    | Aktion eines technischen Verfahrens zum Nachweisabruf / Data Consumers oder einer dafür verantwortlichen Stelle |
| Weiße Schrift auf blauem Grund| ![C:\\fd55d56b76ea7b568cc1929439672e70](./assets/images1/media/image6.png)       | Aktion einer NOOTS-Komponente oder einer dafür verantwortlichen Stelle                                         |
| Harke / Rechen                | ![C:\\1241d6e62147471153f87c0630cbf006](./assets/images1/media/image7.png)         | Die Aktion wird in einem Unterablauf detailliert.                                                         |


## Anforderungen

Ein Data Consumer muss je nach <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">Anwendungsfall der
High-Level-Architecture des
NOOTS</a>
(siehe Überblick) die nachfolgenden Anforderungen erfüllen, um Nachweise
über das NOOTS abrufen zu können. Die Geltung für einen Anwendungsfall
wird durch ein \"x\" in folgenden Spalten dargestellt:

-   HLA-1: Gilt für interaktive nationale Nachweisabrufe gemäß der
    Anwendungsfälle 1a und 1b der HLA
-   HLA-2: Gilt für nicht-interaktive nationale Nachweisabrufe gemäß
    Anwendungsfall 2 der HLA
-   HLA-4: Gilt für interaktive Nachweisabrufe aus EU-Mitgliedstaaten
    gemäß Anwendungsfall 4 der HLA

Die Anforderungen basieren auf Rechtsnormen, Beschlüssen des IT-PLR
und/oder Entwurfsentscheidungen des PB NOOTS. Die jeweilige Grundlage
wird in der gleichnamigen Spalte angegeben. Die Hinweise zu mit Ziffern
markierten Grundlagen sind unter der Tabelle aufgeführt.

Die Anforderungen sind jeweils einem Architekturzielbild des NOOTS
zugeordnet (vgl. <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">HLA - Architekturzielbilder und Umsetzungsstufen des
NOOTS</a>
und erst beim Erreichen des jeweiligen Zielbilds umzusetzen. Die
Maßnahmen zur Umsetzung der Anforderungen werden in den nachfolgenden
Kapiteln beschrieben (siehe Spalte Fundstelle).

| Nr.       | Anforderung                                                                                                                                                                                               | Grundlage       | [HLA-1](#hla-1-nationalen-nachweis-interaktiv-abrufen) | [HLA-2](#hla-2-nationalen-nachweis-nicht-interaktiv-abrufen) | [HLA-4](#hla-4-nachweis-aus-eu-mitgliedstaat-abrufen) | Zielbild | Fundstelle im Anschlusskonzept Data Consumer                      |
|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|-------|-------|-------|----------|-------------------------------------------------------------------|
| NOOTS-198 | Der Data Consumer muss die Berechtigung zur Teilnahme am NOOTS und zum Nachweisabruf nachweisen.                                                                                                          | IT-PLR 2022/06  | x     | x     | x     | 2025     |[Voraussetzungen für die Teilnahme am NOOTS erfüllen](#voraussetzungen-für-die-teilnahme-am-noots-erfüllen). Schritte als Data Consumer in HLA-1, HLA-2 und HLA-4   |
| NOOTS-302 | Der Data Consumer muss beim Nachweisabruf den Standard XNachweis verwenden.                                                                                                                               | IT-PLR 2022/22, § 7 IDNrG <sup>1)</sup> | x     | x     | x     | 2023     | Schritte als Data Consumer in HLA-1, HLA-2 und HLA-4              |
| NOOTS-448 | Der Data Consumer muss für einen Nachweis aus Deutschland von der RDN die Verbindungsparameter des Data Providers abfragen, der für Nachweistyp und Nachweissubjekt zuständig ist.                        | IT-PLR 2022/22  | x     | x     |       | 2025     | Schritte als Data Consumer in HLA-1 und HLA-2                     |
| NOOTS-456 | Der Data Consumer MUSS für einen Nachweis aus einem anderen EU-Mitgliedstaat von der RDN die Verbindungsparameter der zuständigen Intermediären Plattform abfragen.                                       | PB NOOTS        |       |       | x     | 2025     | Schritte als Data Consumer in HLA-4                               |
| NOOTS-462 | Der Data Consumer muss für einen Nachweis aus Deutschland bei interaktiver Nutzung auf entsprechenden Wunsch eine Vorschau des Nachweises anzeigen und Möglichkeiten zur Zustimmung und Ablehnung bieten. | § 5 EGovG<sup>1)</sup>, PB NOOTS<sup>2)</sup>    | x     |       |       | 2025     | Vorbereitende Schritte in HLA-1, Ergebnis in HLA-1                                   |
| NOOTS-538 | Der Data Consumer muss bei interaktivem Nachweisabruf zu einer natürlichen Person aus Deutschland deren IDNr ermitteln und angeben, wenn damit ein Nachweisabruf möglich ist.                             | § 6 IDNrG       | x     |       |       | 2025     | Schritte als Data Consumer in HLA-1                               |
| NOOTS-539 | Der Data Consumer muss bei einem Nachweisabruf zu einem Unternehmen aus Deutschland dessen bundeseinheitliche Wirtschaftsnummer ermitteln und angeben, wenn damit ein Nachweisabruf möglich ist.          | § 2 UBRegG      | x     | x     |       | 2028     | Schritte als Data Consumer in HLA-1 und HLA-2                     |
| NOOTS-542 | Der Data Consumer muss einen Nachweis aus einem anderen EU-Mitgliedstaat über eine Intermediäre Plattform abrufen.                                                                                        | IT-PLR 2022/34  |       |       | x     | 2023     | Einleitung von HLA-4                                              |
| NOOTS-599 | Der Data Consumer muss bei interaktiver Nutzung die antragstellende Person authentifizieren und das Vertrauensniveau des verwendeten Identifizierungsmittels ermitteln.                                   | § 3 OZG<sup>1)</sup>       | x     |       | x     | 2023     | Vorbereitende Schritte in HLA-1 und HLA-4                         |
| NOOTS-600 | Der Data Consumer darf bei interaktiver Nutzung nur dann einen Nachweis abrufen, wenn sich die antragstellende Person für den automatisierten elektronischen Nachweisabruf entschieden hat.               | § 5 EGovG<sup>1)</sup>     | x     |       | x     | 2023     | Einleitung von HLA-1 und HLA-4                                    |
| NOOTS-835 | Der Data Consumer soll bei interaktiver Nutzung den Verlauf des Nachweisabrufs anzeigen.                                                                                                                  | PB NOOTS        | x     |       | x     | 2025     | Weitere zu beachtende Anforderungen in HLA-1 und HLA-4            |
| NOOTS-842 | Der Data Consumer muss bei interaktiver Nutzung sicherstellen, dass ein Nachweisabruf vor Ablauf der maximal zulässigen Zeit nicht wegen Zeitablauf abgebrochen wird.                                     | PB NOOTS        | x     |       | x     | 2023     | Weitere zu beachtende Anforderungen in HLA-1 und HLA-4            |
| NOOTS-864 | Der Data Consumer muss Nachweisabrufe protokollieren und die Protokolldaten zwei Jahre aufbewahren.                                                                                                       | § 9 IDNrG, § 76 BDSG       | x     | x     | x     | 2023     | Weitere zu beachtende Anforderungen in HLA-1, HLA-2 und HLA-4, Hinweis unter [Voraussetzungen für die Teilnahme am NOOTS erfüllen](#voraussetzungen-für-die-teilnahme-am-noots-erfüllen)     |
| NOOTS-872 | Der Data Consumer muss im Rahmen eines Nachweisabrufs gemäß den Vorgaben zur NOOTS Transportinfrastruktur kommunizieren.                                                                                  | IT-PLR 2021/05, IT-PLR 2023/38, § 7 IDNrG| x     | x     | x     | 2025     |[Voraussetzungen für die Teilnahme am NOOTS erfüllen](#voraussetzungen-für-die-teilnahme-am-noots-erfüllen). Schritte als Data Consumer in HLA-1, HLA-2 und HLA-4    |
| NOOTS-873 | Der Data Consumer muss bei nicht-interaktiver Nutzung Deutschland als Herkunftsland des Nachweises verwenden.                                                                                             | <sup>3)</sup>              |       | x     |       | 2023     | Einleitung von HLA-2                                              |
| NOOTS-875 | Der Data Consumer muss bei interaktiver Nutzung das Herkunftsland des abzurufenden Nachweises von der nutzenden Person erfragen.                                                                          | PB NOOTS        | x     |       | x     | 2023     | Einleitung von HLA-1 und HLA-4                                    |

<sup>1)</sup> Die Norm wird vom OZGÄndG entsprechend geändert.

<sup>2)</sup> Es ist eine Entwurfsentscheidung des PB NOOTS, dass die
Nachweisvorschau vom Data Consumer umzusetzen ist.

<sup>3)</sup> Es gibt keine Rechtsgrundlage, die nicht-interaktive Nachweisabrufe
in anderen EU-Mitgliedstaaten ermöglicht. Die SDG VO (EU) 2018/1724
ermöglicht Bürgern und Unternehmen nur interaktive Nachweisabrufe.

## Kommunikationsbeziehungen

Die nachfolgende Abbildung zeigt die insgesamt notwendige Kommunikation
eines Data Consumers mit den NOOTS-Komponenten und dem Data Provider
bzw. dem EU OOTS über die Intermediäre Plattform.

![C:\\9bd8707f0e61d3e63710b1fcc8a10984](./assets/images1/media/image8.png)
**Abb. 8: Kommunikationsbeziehungen**


Ein Data Consumer muss die Voraussetzungen für die Teilnahme am NOOTS
erfüllen, um kommunizieren zu können. Die dafür nötigen Schritte sind
[im folgenden Kapitel](#voraussetzungen-für-die-teilnahme-am-noots-erfüllen)
beschrieben.

In den anschließenden Kapiteln werden die zur Vorbereitung und
Durchführung eines Nachweisabrufs nötigen Schritte nach den
Anwendungsfällen [1](#hla-1-nationalen-nachweis-interaktiv-abrufen),
[2](#hla-2-nationalen-nachweis-nicht-interaktiv-abrufen) und
[4](#hla-4-nachweis-aus-eu-mitgliedstaat-abrufen) der HLA beschrieben.

## Voraussetzungen für die Teilnahme am NOOTS erfüllen

Damit ein Data Consumer einen Nachweis über das NOOTS abrufen kann, muss
er mit dem NOOTS wie vorgegeben kommunizieren und seine Berechtigung zum
Abruf des Nachweises technisch belegen können.

Der Data Consumer muss also Voraussetzungen erfüllen, die ihm die
Teilnahme in der gewünschten Weise ermöglichen. Die für den Data
Consumer [Verantwortlichen](#data-consumer)
müssen dazu folgende Schritte durchführen:

-   Den Data Consumer [an das NOOTS
    anbinden](#an-noots-anbinden), damit er mit
    den NOOTS-Komponenten und mit Data Providern kommunizieren kann
    (siehe [NOOTS-872](#anforderungen)).
-   Den Data Consumer im IAM für Behörden mit den nötigen Angaben
    [registrieren](#im-noots-registrieren), damit er für die NOOTS-Teilnahme und für Nachweisabrufe autorisiert werden
    kann (siehe [NOOTS-198](#anforderungen)).

![NOOTS
Teilnahmevoraussetzungen](./assets/images1/media/image9.png)
<br>**Abb. 9: NOOTS Teilnahmevoraussetzungen**</br>


Die Umsetzung dieser Voraussetzungen wird nachfolgend beschrieben.
Registrierung und NOOTS-Anbindung können in beliebiger Reihenfolge
durchgeführt werden.

### An NOOTS anbinden

Der Data Consumer muss an die Transportinfrastruktur von
NOOTS <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-19]</a> angebunden werden, um mit den NOOTS-Komponenten kommunizieren,
Nachweisabrufe senden und Antworten empfangen zu können. Die
Transportinfrastruktur von NOOTS gewährleistet die Vertraulichkeit und
Integrität transportierter Daten und die Durchführung der abstrakten
Berechtigungsprüfung gem. § 7 (2) IDNrG.

Für die Anbindung an das NOOTS geht die vorliegende Version des
Anschlusskonzepts Data Consumer von folgenden Annahmen und
Rahmenbedingungen aus:

-   Gemäß IT-PLR-Beschluss 2023/38 vom 3.11.2023 soll \"ein Wechsel von
    zunächst genutzten IT-Standards bzw. Komponenten zu europäisch oder
    international genutzten Alternativen\" ermöglicht werden.
-   Im Zuge des NOOTS-Ausbaus werden zahlreiche Data Consumer und Data
    Provider angeschlossen werden. Um diese zu entlasten, wird derzeit
    eine Software konzipiert, welche nicht nur den vorgenannten Wechsel
    ermöglicht, sondern zudem für Data Consumer und Data Provider
    (mindestens) folgende Aufgaben übernimmt:
    -   Kapselung des Transports,
    -   Siegelung und Verschlüsselung von Inhalten,
    -   Durchführung der abstrakten Berechtigungsprüfung.
-   Diese Software wird als \"Sicherer Anschlussknoten\" bezeichnet.
    Weitere Details werden im Übergreifenden Konzept
    Transportinfrastruktur <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-19]</a> 
    beschrieben.
-   Die vorliegende Version des Anschlusskonzepts Data Consumer geht von
    folgenden Annahmen aus: Ein Sicherer Anschlussknoten
    -   übernimmt die o.g. Aufgaben,
    -   benötigt elektronische Zertifikate zur Siegelung und
        Verschlüsselung,
    -   muss von Data Consumern zur Kommunikation mit dem NOOTS genutzt
        werden.

*Hinweis: Es ist noch in Klärung, ob der Sichere Anschlussknoten
Ereignisse in einer Weise protokolliert, die der Anforderung
[NOOTS-864](#anforderungen) entspricht.*

Die sich daraus ergebenden Aufgaben für Data Consumer und deren
Verantwortliche werden in der nachfolgenden Beschreibung berücksichtigt.

### Im NOOTS registrieren

Ein Onlinedienst wird in der NOOTS-Komponente IAM für
Behörden <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-05]</a> 
als \"Data Consumer\" mit folgenden Angaben registriert:

-   Benennung des
    [Fachverantwortlichen](#verantwortung)
    (Name und Anschrift der Behörde, Kontaktdaten des Ansprechpartners),
-   die [Behördenfunktion](#behördenfunktion-festlegen),
-   die zur Authentifizierung, Siegelung und Verschlüsselung nötigen
    elektronischen
    [Zertifikate](#zertifikate-beschaffen).

Die Zertifikate enthalten Angaben zum Betriebsverantwortlichen.

Zur Registrierung müssen der Fach- und der Betriebsverantwortliche die
nachfolgenden Schritte durchführen.

![C:\\c7d9afc5926633eddd905d721547bcbe](./assets/images1/media/image10.png)
<br>**Abb. 10: NOOTS Registrierung**</br>


1.  Der Fachverantwortliche eines Onlinediensts [legt dessen
    Behördenfunktion fest (Details
    s.u.)](#behördenfunktion-festlegen) und beauftragt den
    Betriebsverantwortlichen des Onlinediensts oder der
    Onlinedienst-Plattform (siehe
    [Verantwortung](#verantwortung)) mit der
    Registrierung als Data Consumer.
2.  Der Betriebsverantwortliche [beschafft die Zertifikate (Details
    s.u.)](#zertifikate-beschaffen) und beantragt die
    Registrierung bei der Registrierungsstelle des IAM für Behörden mit
    den nötigen Angaben.
3.  Die Registrierungsstelle des IAM für Behörden verifiziert die
    Angaben und stößt die technische Registrierung im System IAM für
    Behörden an.
4.  Das System IAM für Behörden
    a.  legt einen neuen Teilnehmer der Art \"Data Consumer\" an,
    b.  ordnet ihm die angegebene Behördenfunktion und die übergebenen
        Zertifikate zu,
    c.  legt ggf. den Fachverantwortlichen an und ordnet ihn dem
        Teilnehmer zu und
    d.  informiert den Betriebsverantwortlichen über die Registrierung.

Zur späteren [Autorisierung](#autorisierung-als-data-consumer-anfordern)
identifiziert sich der Onlinedienst mit demselben
Authentifizierungs-Zertifikat, das zu seiner Registrierung verwendet
wurde.

*Hinweise:*

-   *Die Registrierung basiert fachlich auf der
    [Behördenfunktion](#behördenfunktion-festlegen).
    Weitere Onlinedienste mit derselben Behördenfunktion können mit
    einer vorhandenen Registrierung betrieben werden.*
-   *Onlinedienste mit einer abweichenden Behördenfunktion müssen
    separat registriert werden.*
-   *Bei abhängigen Onlinediensten wird der Sichere Anschlussknoten mit
    der Plattform verbunden. Der Betriebsverantwortliche muss nur für
    den Sicheren Anschlussknoten Zertifikate zur Siegelung und
    Verschlüsselung beschaffen, aber nicht für jeden abhängigen
    Onlinedienst.*
-   *Das Konzept IAM für Behörden wird derzeit überarbeitet. Bei der
    Registrierung können daher noch Änderungen entstehen.*

#### Behördenfunktion festlegen

Eine Behördenfunktion ist ein fachlicher Aufgabenbereich einer Behörde
auf einer rechtlichen Grundlage („Rolle der Behörde", z.B.
Kfz-Zulassungsstelle). Zu einer Behördenfunktion sind im IAM für
Behörden folgende Informationen hinterlegt:

-   der zugehörige Verwaltungsbereich entsprechend § 7 (2) IDNrG,
-   Rechtsgrundlagen der Behördenfunktion zur Begründung von
    Nachweisabrufen,
-   je Rechtsgrundlage die von ihr umfassten Nachweistypen.

Ein Data Consumer darf nur Nachweise zu Nachweistypen abrufen, die
seiner Behördenfunktion zugeordnet sind. Der
[Fachverantwortliche](#verantwortung) muss
daher die zutreffende Behördenfunktion des zu registrierenden
Onlinediensts festlegen. Die Behördenfunktion muss

-   der Fachfunktion der antragbearbeitenden Behörde entsprechen, für
    die die abgerufenen Nachweise bestimmt sind (z.B. \"Meldebehörde\"),
-   im IAM für Behörden <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-05]</a>  vorhanden und für Data Consumer erlaubt sein.

*Hinweise:*

-   *Die Liste der für Data Consumer möglichen Behördenfunktionen muss
    noch erstellt werden.*
-   *Das IAM für Behörden wird voraussichtlich Funktionen zur Verfügung
    stellen, mit der die möglichen Behördenfunktionen und die
    Nachweistypen zu einer Behördenfunktion abgerufen werden können.*

#### Zertifikate beschaffen

Ein Data Consumer benötigt ein zur Authentifizierung geeignetes
elektronisches Zertifikat als Identifizierungsmittel, mit dem er sich im
NOOTS ausweist. Ohne einen solchen Ausweis kann ein Data Consumer nicht
authentifiziert und daher auch nicht berechtigt werden.

Ein Sicherer Anschlussknoten eines Onlinediensts benötigt Zertifikate,
die zur Siegelung und Verschlüsselung geeignet sind, um die
Vertraulichkeit und Integrität der Kommunikation von Teilnehmern mit dem
NOOTS und untereinander sicherzustellen.

Der
[Betriebsverantwortliche](http://www.verwaltungskooperation.de#definition_dc)
muss für die Teilnahme des Onlinediensts am NOOTS die entsprechenden
elektronischen Zertifikate beschaffen. Die Beschaffung elektronischer
Zertifikate wird im Konzept Zertifikate beschrieben.

Die Zertifikate müssen der Zertifikatsrichtlinie <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-16]</a>
von NOOTS entsprechen und dem Onlinedienst so zugeordnet sein, dass

-   der Onlinedienst sich mit dem Authentifizierungs-Zertifikat zur
    Laufzeit gegenüber dem IAM für Behörden <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-05]</a> ausweisen kann und
-   der Sichere Anschlussknoten des Onlinediensts die Kommunikation im
    nötigen Umfang siegeln und verschlüsseln kann.

*Hinweise:*

-   *Die NOOTS Zertifikatsrichtlinie <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-16]</a> muss noch erstellt werden. Bis
    dahin können noch keine Zertifikate für Zwecke des NOOTS bezogen
    werden.*
-   *Im Rahmen der Erstellung der Zertifikatsrichtlinie ist festzulegen,
    ob für die nötigen Zwecke (Authentifizierung, Siegelung,
    Verschlüsselung) je ein eigenes Zertifikat gefordert wird oder ob
    Zertifikate mit Zweck-Kombinationen zulässig sind.*

## HLA-1: Nationalen Nachweis interaktiv abrufen

Hat sich die antragstellende Person für den elektronischen Nachweisabruf
entschieden (siehe
[NOOTS-600](#anforderungen)) und soll ein
Nachweis aus Deutschland abgerufen werden (siehe
[NOOTS-875](#anforderungen)), handelt es sich
um den Anwendungsfall 1 der HLA. Es gelten die [Anforderungen gemäß der
Spalte HLA-1](#anforderungen).

### Ablauf

Der Ablauf dieses Nachweisabrufs wird nachfolgend beschrieben. Die dabei
geltenden Anforderungen werden an der umsetzenden Stelle genannt.

![Nationalen Nachweis interaktiv
abrufen](./assets/images1/media/image11.png)
<br>**Abb. 11: HLA 1: Nationalen Nachweis interaktiv abrufen**</br>


### Vorbereitende Schritte

Der Onlinedienst muss die nachfolgenden vorbereitenden Schritte für
einen Nachweisabruf durchführen. Diese Schritte sind unabhängig vom
NOOTS, also ohne Autorisierung möglich. Der Onlinedienst

1.  authentifiziert den Antragsteller (siehe
    [NOOTS-599](#anforderungen)),
2.  liest gemäß [BSI TR-03107 \[SQ-07\]](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03107/TR-03107_node.html)
    die Basisdaten aus dem zur Authentifizierung verwendeten
    Identifizierungsmittel oder aus dem Ergebnis des
    Authentifizierungsdiensts (z.B. BundID) aus,
3.  stellt gemäß [BSI TR-03107 \[SQ-07\]](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03107/TR-03107_node.html)
    das Vertrauensniveau des zur Authentifizierung verwendeten
    Identifizierungsmittels fest oder übernimmt die entsprechende
    Information aus dem Ergebnis des Authentifizierungsdiensts (z.B.
    BundID),
4.  fragt die zur Ermittlung des zuständigen Data Providers ggf. nötigen
    [Zuständigkeitsparameter](#zuständigkeitsparameter)
    vom Antragsteller ab und
5.  erfragt vom Antragsteller, ob er den Nachweis zur Entscheidung über
    seine Verwendung angezeigt bekommen möchte (Preview, siehe
    [NOOTS-462](#anforderungen)).

#### Vertretung

Das EU-OOTS und das NOOTS bieten die technische Möglichkeit, in einem
Nachweisabruf den Vertreter des
[Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s anzugeben.
Bietet ein Onlinedienst beim Nachweisabruf den Vertretungsfall an, muss
der Fachverantwortliche für geeignete Maßnahmen zur
Missbrauchsverhinderung sorgen. Das NOOTS leistet keine Unterstützung
bei der Prüfung von Vertretungsberechtigungen.

Handelt der Antragsteller im Sinne des Onlinediensts als Vertreter des
[Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s, muss der
Onlinedienst

-   die in 2. ermittelten Basisdaten und deren in 3. ermitteltes
    Vertrauensniveau im Nachweisabruf als Angaben zum Vertreter
    eintragen,
-   die Angaben zum Vertretenen vom Antragsteller erfragen und im
    Nachweisabruf als Angaben zum
    [Nachweissubjekt](#nachweissubjekt-und-identifikatoren)
    eintragen.

*Hinweise:*

-   *Weitere Aspekte im Umgang mit Vertretung werden noch untersucht und
    können zu Änderungen in diesem Anschlusskonzept führen.*

#### Zuständigkeitsparameter

Der Onlinedienst muss prüfen, ob zur Ermittlung der Verbindungsparameter
des zuständigen Data Providers Zuständigkeitsparameter (siehe
Grobkonzept Registerdatennavigation <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-07]</a>) nötig sind.

Zuständigkeitsparameter bestimmen bei Nachweistypen mit aufgeteilter
Zuständigkeit den für den Nachweis zuständigen Data Provider anhand der
Werte, die dem
[Nachweissubjekt](#nachweissubjekt-und-identifikatoren) entsprechen.
Beispiele für Zuständigkeitsparameter sind

-   der amtliche Gemeindeschlüssel (AGS) zur Ermittlung der zuständigen
    Meldebehörde anhand des AGS des Wohnsitzes der Person,
-   das Bundesland und der Name einer Hochschule zur Ermittlung der
    zuständigen Hochschule für eine Immatrikulationsbescheinigung,
-   der Name der zuständigen Krankenkasse oder eine sonstige Angabe zu
    deren Identifizierung.

Sind Zuständigkeitsparameter nötig, muss der Data Consumer die
benötigten Informationen vom Antragsteller erfragen.

*Hinweise:*

-   *Die RDN liefert bei fehlenden oder unzureichenden
    Zuständigkeitsparametern Informationen, welche Angaben für eine
    eindeutige Ermittlung des zuständigen Data Providers nötig sind.*
-   *Die Bereitstellung von Wertelisten für Zuständigkeitsparameter wird
    im Rahmen der RDN-Konzeption geklärt.*

### Schritte als Data Consumer

Für die folgenden Schritte muss der Onlinedienst mit dem NOOTS über den
sicheren Anschlussknoten kommunizieren (siehe
[NOOTS-872](#anforderungen)) und daher
zunächst seine [Autorisierung
anfordern](#autorisierung-als-data-consumer-anfordern) (siehe
[NOOTS-198](#anforderungen)). Die
Autorisierung berechtigt ihn zur Kommunikation mit NOOTS-Komponenten und
zum Abruf der Nachweistypen entsprechend seiner Behördenfunktion (s.
[Registrierung](#im-noots-registrieren)).

Bei erfolgreicher Autorisierung kann der Onlinedienst als Data Consumer
mit dem NOOTS und anderen Teilnehmern kommunizieren und führt als
solcher die nachfolgenden Schritte durch. Der Data Consumer

1.  fragt von der Registerdatennavigation die
    [Verbindungsparameter](#verbindungsparameter) des
    zuständigen Data Providers ab (siehe
    [NOOTS-448](#anforderungen)),
2.  beschafft ggf. den [Identifikator (IDNr/beWiNr) des
    Nachweissubjekts](#identifikator-für-nachweissubjekt-beschaffen), wenn
    der Data Provider damit abgefragt werden kann (siehe
    [NOOTS-538](#anforderungen) und
    [NOOTS-539](#anforderungen)),
3.  stellt die Daten für den Abruf eines nationalen Nachweises gemäß
    XNachweis zusammen (siehe
    [NOOTS-302](#anforderungen)), insbesondere<br>
    </br>a.  Angaben zu den Beteiligten (Data Consumer, Data Provider,
        antragbearbeitende Behörde),<br>
    </br>b.  Angaben zum Nachweis (Typ, Format, Abrufgrund) und<br>
    c.  Angaben zum
        [Nachweissubjekt](#nachweissubjekt-und-identifikatoren)
        (siehe [Identifikator für Nachweissubjekt
        beschaffen](#identifikator-für-nachweissubjekt-beschaffen)) und ggf.
        zum [Vertreter](#vertretung),
4.  sendet den Nachweisabruf anhand der ermittelten Verbindungsdaten an
    den Data Provider,
5.  nimmt dessen Antwort entgegen und wertet sie aus (siehe
    [Ergebnis](#ergebnis)).

*Hinweise:*

-   *Ist das Vertrauensniveau des Identifizierungsmittels (siehe
    [Vorbereitende Schritte](#vorbereitende-schritte))
    für den Nachweisabruf unzureichend (siehe notwendiges
    Vertrauensniveau unter*
    [Verbindungsparameter](#verbindungsparameter)*),
    soll der Data Consumer den Nachweisabruf abbrechen und die nutzende
    Person zur Re-Authentifizierung mit einem Identifizierungsmittel mit
    ausreichendem Vertrauensniveau auffordern.*
-   *Der Data Consumer muss beim Verwaltungsbereichs übergreifenden
    Nachweisabruf mit IDNr nicht für eine abstrakte Berechtigungsprüfung
    gem. § 7 (2) IDNrG sorgen. Dies ist eine Aufgabe der NOOTS
    Transportinfrastruktur und wird voraussichtlich durch sichere
    Anschlussknoten umgesetzt([siehe An NOOTS anbinden](#an-noots-anbinden)).*

#### Autorisierung als Data Consumer anfordern

Der Onlinedienst muss für den Zugriff auf NOOTS-Komponenten und für den
Nachweisabruf als Data Consumer autorisiert sein (siehe
[NOOTS-198](#anforderungen)).

![Autorisierung als Data Consumer
anfordern](./assets/images1/media/image12.png)
<br>**Abb. 12: Autorisierung als Data Consumer anfordern**</br>


Der Onlinedienst sendet eine Autorisierungsanfrage an das IAM für
Behörden mit dem Zertifikat, mit dem der Onlinedienst als Data Consumer
registriert wurde. Das IAM
für Behörden
-   authentifiziert den Onlinedienst anhand des Zertifikats als
    Teilnehmer der Art \"Data Consumer\",
-   autorisiert den Onlinedienst zum Abruf der Nachweistypen, die seiner
    Behördenfunktion zugeordnet sind,
-   autorisiert den Onlinedienst zum Zugriff auf die NOOTS-Komponenten,
    die der Teilnahmeart \"Data Consumer\" zugeordnet sind, und
-   stellt dem Onlinedienst ein Token mit den ermittelten Berechtigungen
    aus.

#### Verbindungsparameter

Der Data Consumer ruft von der Registerdatennavigation die
Verbindungsparameter des zuständigen Data Providers anhand des
Nachweistyps und der ggf. nötigen
[Zuständigkeitsparameter](#zuständigkeitsparameter) ab.
Verbindungsparameter enthalten folgende Informationen:

-   Technische Referenz auf den Data Provider
-   Für den Nachweisabruf nötiges Vertrauensniveau. Es bestimmt, wie
    hoch das Vertrauensniveau der Basisdaten des
    [Nachweissubjekts](#nachweissubjekt-und-identifikatoren) oder
    dessen Vertreters mindestens sein muss, um einen Nachweis dieses
    Typs abrufen zu können.
-   Für den Nachweistyp angebotene Nachweisformate. Sie bestimmen, in
    welchen Formaten (z.B. Fachstandard-XML in Version x.y) der Data
    Consumer einen Nachweis beziehen kann. Der Data Consumer muss eines
    der angebotenen Nachweisformate im Nachweisabruf angeben.
-   Angabe, ob ein Nachweis dieses Typs mit [IDNr (natürliche Person)
    bzw. beWiNr (Unternehmen)](#nachweissubjekt-und-identifikatoren)
    abgerufen werden kann. Deren Verwendung wird im Folgeabschnitt
    erläutert.

Fehlt beim Abruf der Verbindungsparameter ein Zuständigkeitsparameter
oder wird ein ungültiger Wert angegeben, antwortet die
Registerdatennavigation mit einer entsprechenden Fehlermeldung.

#### Identifikator für Nachweissubjekt beschaffen

Nationale Nachweise sollen möglichst über eindeutige Identifikatoren
abgerufen werden. Bei natürlichen Personen ist der eindeutige
[Identifikator](#nachweissubjekt-und-identifikatoren) die IDNr, bei
Unternehmen die beWiNr. Die IDNr wird vom IDM für Personen (IDM-P), die beWiNr vom IDM für
Unternehmen (IDM-U) bezogen.

![Identifikator für Nachweissubjekt
beschaffen](./assets/images1/media/image13.png)
<br>**Abb. 13: Identifikator für Nachweissubjekt beschaffen**</br>


Für einen Nachweisabruf zu einer natürlichen Person muss der Data
Consumer anhand der [Verbindungsparameter des Data
Providers](#verbindungsparameter) prüfen, ob ein
Nachweis mit IDNr abgerufen werden kann. Data Provider sollen diese
Information in der Registerdatennavigation hinterlegen, sobald sie ihre
Pflichten nach dem IDNrG umgesetzt haben und Nachweisabrufe mit IDNr
möglich sind. Ist das der Fall und liegt eine Rechtsgrundlage (insb. § 5
(4) EGovG, § 6 IDNrG) zum Abruf mit IDNr vor, muss der Data Consumer
anhand der Basisdaten des
[Nachweissubjekts](#nachweissubjekt-und-identifikatoren)
(Familienname, Wohnort, Postleitzahl und Geburtsdatum gem. § 6 (3)
IDNrG) die IDNr vom IDM für Personen abrufen und sie im Nachweisabruf
als Identifikator des
[Nachweissubjekts](#nachweissubjekt-und-identifikatoren) verwenden
(siehe [NOOTS-538](#anforderungen)).

Für einen Nachweisabruf zu einem
[Unternehmen](#nachweissubjekt-und-identifikatoren) muss der Data
Consumer anhand der [Verbindungsparameter des Data
Providers](#verbindungsparameter) prüfen, ob ein
Nachweis mit beWiNr abgerufen werden kann. Ist das der Fall und liegt
eine Rechtsgrundlage (insb. §§ 2, 5 UBRegG) zum Abruf mit beWiNr vor,
muss der Data Consumer anhand der Basisdaten des
[Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s die beWiNr
vom IDM für Unternehmen abrufen und sie im Nachweisabruf als
Identifikator des
[Nachweissubjekt](#nachweissubjekt-und-identifikatoren)s verwenden
(siehe [NOOTS-539](#anforderungen)).

*Hinweise:*

-   *Das IDM für Unternehmen wird derzeit konzipiert. Änderungen und
    Präzisierungen für den Abruf der beWiNr werden bei der
    Fortschreibung des Anschlusskonzepts Data Consumer berücksichtigt.*
-   *Ein wesentliches Ziel ist die Vereinheitlichung von Datenabrufen zu
    Unternehmen anhand der beWiNr. Es ist noch offen, inwieweit
    Nachweisabrufe zu Unternehmen ohne beWiNr über das NOOTS ermöglicht
    werden.*

### Ergebnis

Der Data Consumer wertet die erhaltene Antwort des Data Providers aus.
Enthält die Antwort

-   eine Fehlermeldung, soll der Data Consumer die Fehlermeldung
    anzeigen und die Fehlerursache beheben, soweit sie ihm zuzurechnen
    ist.
-   einen Nachweis und ist eine Preview
    gewünscht, muss der Data Consumer den Nachweis anzeigen und dem Antragsteller eine
    Entscheidung über die Verwendung des Nachweises ermöglichen (siehe
    [NOOTS-462](#anforderungen)).
    -   Bei Zustimmung zur Verwendung darf der Data Consumer über den
        Nachweis im erlaubten Umfang verfügen.
    -   Bei Ablehnung der Verwendung muss der Data Consumer den Nachweis
        verwerfen.
-   einen Nachweis und ist keine Preview gewünscht, darf der Data
    Consumer über den Nachweis im erlaubten Umfang verfügen.

*Hinweis: Der Data Provider erhält keine Rückmeldung zu einem in der
Preview abgelehnten Nachweis.*

### Weitere zu beachtende Anforderungen

In diesem Anwendungsfall sind außerdem folgende weiteren Anforderungen
zu beachten. Der Data Consumer

-   darf den Nachweisabruf frühestens nach 60 Sekunden wegen Zeitablauf
    abbrechen (siehe
    [NOOTS-842](#anforderungen)). Die Frist
    beginnt mit der
    [Autorisierung](#autorisierung-als-data-consumer-anfordern) und endet
    mit dem [Ergebnis](#ergebnis).
-   soll den aktuellen Stand des Verlaufs anzeigen (siehe
    [NOOTS-835](#anforderungen)). Dazu soll
    der Data Consumer den jeweils aktuellen Schritt und alle bereits
    durchgeführten Schritte anzeigen.
-   muss den Nachweisabruf protokollieren und die Protokolldaten zwei
    Jahre aufbewahren (siehe
    [NOOTS-864](#anforderungen)).

## HLA-2: Nationalen Nachweis nicht-interaktiv abrufen

Soll ein Nachweis nicht-interaktiv von einem nationalen Data Provider
abgerufen werden, handelt es sich um den Anwendungsfall 2 der HLA. Es
gelten die [Anforderungen gemäß der Spalte
HLA-2](#anforderungen).

Nicht-interaktive Abrufe von
[Fachverfahren](#fachverfahren) sind auf
nationale Nachweise beschränkt (siehe
[NOOTS-873](#anforderungen)) und bieten keine
Möglichkeit zum Preview.

### Ablauf

Der Ablauf dieses Nachweisabrufs wird nachfolgend beschrieben. Die dabei
geltenden Anforderungen werden an der umsetzenden Stelle genannt.

![Nationalen Nachweis nicht-interaktiv
abrufen](./assets/images1/media/image14.png)
<br>**Abb. 14: HLA 2: Nationalen Nachweis nicht-interaktiv abrufen**</br>

### Vorbereitende Schritte

Das Fachverfahren muss die nachfolgenden vorbereitenden Schritte für
einen Nachweisabruf durchführen. Diese Schritte sind unabhängig vom
NOOTS, also ohne Autorisierung möglich. Das Fachverfahren muss

1.  die Basisdaten des
    [Nachweissubjekts](#nachweissubjekt-und-identifikatoren) und
    deren Vertrauensniveau festlegen; der Fachverantwortliche des
    Fachverfahrens ist entsprechend dem jeweiligen Fachrecht für
    korrekte Angaben verantwortlich, und
2.  die zur Ermittlung des zuständigen Data Providers ggf. nötigen
    [Zuständigkeitsparameter](#zuständigkeitsparameter)
    festlegen.

### Schritte als Data Consumer

Für die folgenden Schritte muss das Fachverfahren mit dem NOOTS über den
sicheren Anschlussknoten kommunizieren (siehe
[NOOTS-872](#anforderungen)) und daher
zunächst seine [Autorisierung analog zum Onlinedienst
anfordern](#autorisierung-als-data-consumer-anfordern) (siehe
[NOOTS-198](#anforderungen)). Die
Autorisierung berechtigt das Fachverfahren zur Kommunikation mit
NOOTS-Komponenten und zum Abruf der Nachweistypen entsprechend seiner
Behördenfunktion (s.
[Registrierung](#im-noots-registrieren)).

Bei erfolgreicher Autorisierung kann der Onlinedienst als Data Consumer
mit dem NOOTS und anderen Teilnehmern kommunizieren und führt als
solcher [dieselben Schritte wie beim
Onlinedienst](#schritte-als-data-consumer) durch mit
folgenden Abweichungen:

1.  Zur Beschaffung und Verwendung des [Identifikators (IDNr/beWiNr des
    Nachweissubjekts)](#nachweissubjekt-und-identifikatoren) muss
    für das Fachverfahren eine Rechtsgrundlage zum Abruf mit dem
    Identifikator vorliegen.
2.  In den Daten für den Nachweisabruf gemäß XNachweis muss der Preview
    verneint werden.

### Ergebnis

Der Data Consumer wertet die erhaltene Antwort des Data Providers aus.
Enthält die Antwort einen Nachweis, darf der Data Consumer über den
Nachweis im erlaubten Umfang verfügen.

### Weitere zu beachtende Anforderungen

In diesem Anwendungsfall sind außerdem folgende weiteren Anforderungen
zu beachten. Der Data Consumer

-   muss den Nachweisabruf protokollieren und die Protokolldaten zwei
    Jahre aufbewahren
    (siehe [NOOTS-864](#anforderungen)).

## HLA-4: Nachweis aus EU-Mitgliedstaat abrufen

Hat sich die antragstellende Person für den elektronischen Nachweisabruf
entschieden (siehe
[NOOTS-600](#anforderungen)) und soll dieser
Nachweis aus einem anderen EU-Mitgliedstaat abgerufen werden (siehe
[NOOTS-875](#anforderungen)), handelt es sich
um den Anwendungsfall 4 der HLA. Es gelten die [Anforderungen gemäß der
Spalte HLA-4](#anforderungen).

Ein Nachweis aus einem anderen EU-Mitgliedstaat muss über die zuständige
Intermediäre Plattform abgerufen werden (siehe
[NOOTS-542](#anforderungen)). Da diese nur
interaktiv genutzt werden kann, ist ein solcher Nachweisabruf nur
interaktiv über einen Onlinedienst möglich.

### Ablauf

Der Ablauf dieses Nachweisabrufs wird nachfolgend beschrieben. Die dabei
geltenden Anforderungen werden an der umsetzenden Stelle genannt.

![Nachweis aus EU-Mitgliedstaat
abrufen](./assets/images1/media/image15.png)
<br>**Abb. 15: HLA 4: Nachweis aus EU-Mitgliedstaat abrufen**</br>


### Vorbereitende Schritte

Der Onlinedienst muss [dieselben vorbereitenden Schritte wie bei einem
nationalen Nachweisabruf](#vorbereitende-schritte)
durchführen mit folgenden Ausnahmen:

1.  Zur Bestimmung der zuständigen Intermediären Plattform ist derzeit
    keine weitere Angabe erforderlich, da von nur einer Intermediären
    Plattform ausgegangen wird. Falls künftig mehrere Intermediäre
    Plattform zur Verfügung stehen, müssen Zuständigkeitsparameter
    relevant sind.
2.  Der Preview-Wunsch wird in der Nutzeroberfläche der Intermediären Plattform abgefragt.    

### Schritte als Data Consumer

Für die folgenden Schritte muss der Onlinedienst mit dem NOOTS über den
sicheren Anschlussknoten kommunizieren (siehe
[NOOTS-872](#anforderungen)) und daher
zunächst seine [Autorisierung
anfordern](#autorisierung-als-data-consumer-anfordern) (siehe
[NOOTS-198](#anforderungen)). Die
Autorisierung berechtigt ihn zur Kommunikation mit NOOTS-Komponenten und
zum Abruf der Nachweistypen entsprechend seiner Behördenfunktion.

Bei erfolgreicher Autorisierung kann der Onlinedienst als Data Consumer
mit dem NOOTS und anderen Teilnehmern kommunizieren und führt als
solcher die nachfolgenden Schritte durch. Der Data Consumer muss

1.  die Verbindungsparameter der zuständigen Intermediären Plattform von
    der Registerdatennavigation abrufen (siehe
    [NOOTS-456](#anforderungen)),
2.  die für einen Nachweis aus einem anderen EU-Mitgliedstaat nötigen
    Abrufdaten gemäß
    [XNachweis](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)
    zusammenstellen (siehe
    [NOOTS-302](#anforderungen)), insbesondere<br>
    </br>a.  Angaben zu den Beteiligten (Data Consumer, Intermediäre
        Plattform, antragbearbeitende Behörde),<br>
    </br>b.  Angaben zum Nachweis (Nachweistyp oder Nachweisanforderung, wenn
        der Nachweistyp nicht bekannt ist, sowie das Nachweisformat),<br>
    </br>c.  Angaben zum
        [Nachweissubjekt](#nachweissubjekt-und-identifikatoren)
        (Basisdaten) und<br>
    d.  URL für die Rückkehr zur Nutzeroberfläche des Data Consumers,
3.  die Abrufdaten an die Intermediäre Plattform senden (EvidenceOrder,
    siehe [NOOTS-542](#anforderungen)),
4.  aus der Antwort der Intermediären Plattform (EvidenceOrderResponse)
    deren RedirectURL entnehmen und dem Antragsteller zum Aufruf
    anbieten,
5.  auf Veranlassung des Antragstellers die Nutzeroberfläche der
    Intermediären Plattform aufrufen,
6.  nach der Rückkehr von der Intermediären Plattform die Verfügbarkeit
    von Nachweisen ermitteln (GetEvidence).

### Ergebnis

Der Data Consumer wertet die erhaltene Antwort der Intermediären
Plattform zur Verfügbarkeit von Nachweisen aus. Bei Abruf aus anderen
EU-Mitgliedstaaten kann es zu einem angefragten Nachweistyp mehrere
Nachweise geben.

Sind Nachweise verfügbar, ruft der Data Consumer diese von der
Intermediären Plattform ab (Abrufe anhand RegistryObjectList von
GetEvidenceResponse) und darf darüber im erlaubten Umfang verfügen.

### Weitere zu beachtende Anforderungen

In diesem Anwendungsfall sind außerdem folgende weiteren Anforderungen
zu beachten. Der Data Consumer

-   darf den Nachweisabruf frühestens nach 60 Sekunden wegen Zeitablauf
    abbrechen (siehe
    [NOOTS-842](#anforderungen)). Die Frist
    beginnt mit der
    [Autorisierung](#autorisierung-als-data-consumer-anfordern) und endet
    mit dem Aufruf der Intermediären Plattform.
-   soll den aktuellen Stand des Verlaufs anzeigen (siehe
    [NOOTS-835](#anforderungen)). Dazu soll
    der Data Consumer den jeweils aktuellen Schritt und alle bereits
    durchgeführten Schritte anzeigen.
-   muss den Nachweisabruf protokollieren und die Protokolldaten zwei
    Jahre aufbewahren
    (siehe [NOOTS-864](#anforderungen)).
