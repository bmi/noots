>**Redaktioneller Hinweis**
>
>Dokument aus der vorangegangen Iteration - nicht Teil der aktuellen Iteration.
>
>Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## Änderungshistorie

| Release | Datum      | Beschreibung                                                                                                                                                                                                                       |
|---------|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0.95    | 01.09.2023 | <ul><li><p style="color:#184B76;">Initiales Release Anschlusskonzept</p></ul>                                                                                                                                                                                                 |
| Q4.2023 | 15.12.2023 | <ul><li><p style="color:#184B76;">Einführung Definition Data Provider</p> <li><p style="color:#184B76;">Übersicht der funktionalen und nicht-funktionalen Anforderungen</p> <li><p style="color:#184B76;">Systemumgebung Data Provider und Liste der Schnittstellen <li><p style="color:#184B76;">Prozessbeschreibung "Ablauf einer interaktiven Nachweislieferung"</p></ul> |
                                                                                                                                               


## Abstract

Eine nachweisliefernde Stelle ist diejenige öffentliche Stelle, die für
die Ausstellung von nationalen Nachweisen zuständig ist. In Deutschland
erfolgt die Ausstellung dieser Nachweise durch nationale Register, die
für den Nachweisabruf an das nationale Once-Only Technical Systems
(NOOTS) angeschlossen werden. Ein Nachweisabruf kann durch einen
nationalen Data Consumer oder ein technisches Verfahren der
EU-Mitgliedstaaten unter Verwendung des europäischen
Once-Only-Technical-Systems (EU-OOTS) und der NOOTS-Komponente
Intermediäre Plattform (IP) ausgelöst werden. In diesem Anschlusskonzept
werden die Anforderungen an den technischen NOOTS-Anschluss von
nationalen Registern definiert, um nationale Register zur Lieferung von
ausgestellten Nachweisen zu befähigen.

## Einleitung

Im High-Level-Architecture-Konzept 
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>
werden die Anwendungsfälle, die NOOTS-Komponenten und übergreifende
Querschnittsaspekte des NOOTS und EU-OOTS betrachtet, die sich aus dem
Architekturzielbild des NOOTS ableiten. In einigen dieser
Anwendungsfälle sind die nationalen Register (*Data Provider)* als
Prozessbeteiligte eingebunden und übernehmen die Aufgabe, Nachweise für
natürliche Personen oder Unternehmen im Sinne des § 3 Abs. 1
UBRegG anhand einer XNachweis-Anfrage gemäß XNachweis-Spezifikation
([**\[XS-01\]**](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
auszustellen. Um die Ausstellung dieser Nachweise durch die nationalen
Register zu ermöglichen, ist eine technische Anbindung an das NOOTS
erforderlich. Neben den funktionalen Anforderungen müssen auch
nicht-funktionale Anforderungen für den technischen Anschluss erfüllt
werden.

> Die kontinuierliche Weiterentwicklung der NOOTS-Konzepte wird
> Auswirkungen auf dieses Anschlusskonzept haben, was wiederum eine
> Aktualisierung der Konzeptinhalte in kommenden Veröffentlichungen zur
> Folge haben wird.

### Zielgruppe

Die Zielgruppe des Dokuments sind IT-Verantwortliche der öffentlichen
Verwaltung, IT-Dienstleister im Auftrag der öffentlichen
Verwaltung/Fachverfahrenshersteller sowie nachweisliefernde Stellen, die
gleichzeitig den Anschluss an das NOOTS verantworten.

### Aufbau des Dokuments, Leseanleitung

Dieses Dokument gliedert sich in die folgenden Kapitel:

Nach der Zusammenfassung im Abstract (**Kapitel 1**) und einer
Einleitung (**Kapitel 2**) wird im **Kapitel 3** der Begriff und die
Merkmale eines *Data Providers* sowie das fachliche Datenmodell
erläutert.

In **Kapitel 4** werden die funktionalen und nicht-funktionalen
Anforderungen an den *Data Provider* aufgelistet.

In **Kapitel 5** werden die Vorbedingungen für die Teilnahme des *Data
Providers* am NOOTS betrachtet, die in direktem Zusammenhang mit dem
technischen Anschluss eines *Data Providers* stehen.

Das **Kapitel 6** beschreibt den technischen Anschluss eines *Data
Provider* an das NOOTS.

In **Kapitel 7** sind die notwendigen administrativen Aktivitäten für
den technischen Anschluss eines *Data Providers enthalten.* 

Das **Kapitel 8** vermittelt einen Ausblick auf Themen, die in
zukünftigen Releases behandelt werden.

### Abgrenzung

Ein umfassendes Verständnis für den technischen NOOTS-Anschluss
erfordert auch die Berücksichtigung von fachlichen, rechtlichen und
organisatorischen Vorbedingungen, die im Vorfeld des technischen
Anschlusses sorgfältig geprüft werden müssen. In diesem Konzept werden
Vorbedingungen benannt, die im direkten Zusammenhang mit dem technischen
NOOTS-Anschluss stehen. 

**Eine vollständige Liste der Vorbedingungen mit einer detaillierten
Beschreibung wird zukünftig in den Dokumenten zum Fachdatenkonzept und
Vorgehensmodell des Programmbereichs (PB) Register zu finden sein.** 

Im Dokument „**Vorgehensmodell für den Anschluss an das NOOTS"** des PB
Register werden rechtliche, organisatorische, fachliche und technische
Besonderheiten beim Anschluss von nachweisliefernden Stellen an das
NOOTS übersichtlich dargestellt. Das Vorgehensmodell bildet durch die
darin beschriebene generische Vorgehensweise die Grundlage für die
individuelle, eigenständige Projektplanung der Roll-Out Vorhaben, indem
darin standardisierte Rollout-Phasen beschrieben und auf relevante
Informationen für jede Rollout-Phase verwiesen wird. 

Im Dokument \"**Fachdatenkonzept**\" des PB Register wird beschrieben,
wie der automatisierte Nachweisaustausch von Fachdaten zwischen *Data
Consumer* und *Data Provider* in verschiedenen Reifegraden über das
NOOTS fachlich gewährleistet werden kann. Die hierfür benötigten
Bedingungen, wie bspw. ein semantischer Fachdatenstandard, werden
entsprechend definiert. Somit bildet das Fachdatenkonzept die fachliche
Grundlage für den automatisierten Datenaustausch. 

Die Verweise zu den entsprechenden Dokumenten werden an dieser Stelle
ergänzt, sobald diese vorliegen.

## Data Provider (DP)

In den folgenden Unterkapiteln werden die Merkmale eines *Data
Providers* anhand der Rahmenbedingungen und eines fachlichen
Datenmodells näher erläutert.

### Rahmenbedingungen

#### Rechtsgrundlage

Eine nachweisliefernde Stelle ist für die Ausstellung von Nachweisen
nach §5 Absatz 2 des Entwurfes eines Gesetzes zur Änderung des
Onlinezugangsgesetzes sowie weiterer Vorschriften zur Digitalisierung
der Verwaltung (OZG-Änderungsgesetz -- OZGÄndG) zuständig. Nach §5
Absatz 3 des OZGÄndG darf die nachweisliefernde Stelle den Nachweis an
die nachweisanfordernde Stelle übermitteln, wenn

1.  dies zur Erfüllung der Aufgabe der nachweisanfordernden Stelle
    erforderlich ist
2.  die nachweisanfordernde Stelle den Nachweis auch aufgrund anderer
    Rechtsvorschriften beim Antragsteller erheben dürfte.

Nach §5 Absatz 4 darf die nachweisanfordernde Stelle die
Identifikationsnummer nach § 1 des Identifikationsnummerngesetzes
(**IDNrG**) und zusätzlich weitere Daten im Sinne von § 4 Absatz 2 und 3
**IDNrG** an die nachweisliefernde Stelle übermitteln. Die
nachweisliefernde Stelle darf diese Daten zum Zwecke der Validierung der
Zuordnung verarbeiten.

#### IT-Planungsrat Beschlüsse

In der High-Level-Architecture
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>
werden in dem Kapitel \"Übergreifende Entwurfsentscheidungen\"
Beschlüsse und Vorschläge des IT-Planungsrates von übergreifender
Relevanz für die NOOTS-Gesamtarchitektur aufgelistet. Die folgenden
Beschlüsse und Vorschläge sind für dieses Anschlusskonzept *Data
Provider* von besonderer Relevanz.

| Beschlüsse und Vorschläge                                                 | Erläuterung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|---------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| IT-PLR-Beschluss 2022/34 - NOOTS-Registeranbindung [[IT-PLR-B-09]](https://www.it-planungsrat.de/beschluss/beschluss-2022-34)                                                | "TOP 4: Anbindung der Register an das nationale Once-Only-Technical-System zum Nachweisabruf" Data Provider müssen für den technischen NOOTS-Anschluss eine Vielzahl an Anforderungen erfüllen. Während es zentralen Registern voraussichtlich mit akzeptablem Aufwand möglich sein wird, diese Anforderungen zu erfüllen, gestaltet sich die Situation bei dezentralen Registern deutlich komplexer. Insbesondere dann, wenn sich die Dezentralität bis auf kommunale Ebene erstreckt, kann nicht davon ausgegangen werden, dass die notwendigen Ressourcen zur Anbindung an das NOOTS vorhanden sind. Auf technischer Ebene ist daher in diesen Fällen überlegenswert, ob der Anschluss an die Systeme zum Nachweisabruf als Anlass genommen werden kann, auch hier zentrale Strukturen, wie Spiegelregister oder Abrufportale, zu schaffen. Die fachlichen, rechtlichen und organisatorischen Hürden können nur von den entsprechenden Fachbereichen abgeschätzt und bewertet werden.|                                                                                                                                                                                                        
| IT-PLR-Beschluss 2022/22 - Entscheidung asynchrone Prozesse [[IT-PLR-E-02]](https://www.it-planungsrat.de/beschluss/beschluss-2022-22) | "TOP 3: Entscheidung zur Unterstützung asynchroner Prozesse in der Architektur der Registermodernisierung" Durch einen Online-Dienst dürfen nur fachlich synchrone Nachweisabrufe genutzt werden und im Rahmen der Behörde-zu Behörde-Kommunikation sind zusätzlich auch fachlich asynchrone Nachweisabrufe zu ermöglichen. |


Tabelle: IT-Planungsrat Beschlüsse und Vorschläge *Data Provider*

#### Anwendungsfälle der High-Level-Architecture für nationale Register

In der High-Level-Architecture
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>
werden die zentralen Anwendungsfälle der Registermodernisierung zum
Abruf von Nachweisen mit den beteiligten NOOTS-Komponenten beschrieben.
Für nationale Register sind folgende Anwendungsfälle relevant.

 | ID | Anwendungsfall                                                                                                   |
|----|------------------------------------------------------------------------------------------------------------------|
| 1a | Interaktiver Nachweisabruf im NOOTS durch natürliche Person                                                      |
| 1b | Interaktiver Nachweisabruf im NOOTS durch Unternehmen im Sinne des § 3 Abs. 1 UBRegG                             |
| 2  | Nicht-Interaktiver Nachweisabruf im NOOTS                                                                        |
| 3  | Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das Europäische Once-Only-Technical-System (EU-OOTS) |


Tabelle: Anwendungsfälle für nationale Register

### Begriff Data Provider

> Ein *Data Provider* ist ein NOOTS-Teilnehmer zur Lieferung von
> Nachweisen für ein Nachweissubjekt. Ein Nachweissubjekt ist die
> natürliche Personen oder Unternehmen im Sinne des § 3 Abs. 1 UBRegG, für
> die ein Nachweis ausgestellt wird.

Ein im NOOTS-Teilnehmer ist ein registriertes technisches Verfahren zur
Teilnahme in einer bestimmten Weise (Teilnahmeart), authentifizierbar
durch ein Zertifikat.

Folgende Merkmale gelten für *Data Provider*: 

1.  kann mehrere Nachweistypen pro Registertyp aus einem fachlichen
    Aufgabenbereich auf einer rechtlichen Grundlage (Behördenfunktion)
    ausstellen. Bei fachlicher Notwendigkeit können auch mehrere *Data
    Provider* pro Registertyp definiert werden.
2.  muss durch ein von der V-PKI ausgestelltes Zertifikat mit der
    Nutzungsart für die Authentisierung
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-16])</B></a> (Übergreifendes Konzept Zertifikate)
    eindeutig identifizierbar sein.
3.  verfügt über ein XNachweis-Endpunkt, über den die Lieferung von
    Nachweisen verschiedener Nachweistypen gemäß XNachweis
    ([**\[XS-01\]**](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
    erfolgt.

> Die Begriffsschärfung im Zusammenhang mit dem *Data Provider* befindet
sich derzeit in Abstimmung innerhalb der Gesamtsteuerung
Registermodernisierung.

### Fachliches Datenmodell

![C:\\76cc6463961bdd4f1617a78c85653cd7](./assets/images2/media/image1.png)
<br>**Abb. 1: Fachliches Datenmodell Data Provider**</br>


#### Anschlussmodelle für Register

Register müssen befähigt werden, Nachweise zu erzeugen. Der Anschluss
der nationalen Register an das nationale Once-Only Technical Systems
(NOOTS) kann über verschiedene Anschlussmodelle
[**\[IT-PLR-B-09\]**](https://www.it-planungsrat.de/beschluss/beschluss-2022-34) -
nachfolgend auch Registerstrukturen genannt - erfolgen:

1.  Zentrale Register auf Bundesebene
2.  Dezentrale Registerstrukturen: verteilte Register **ohne**
    zentralisierte Strukturen.
3.  Dezentrale Registerstrukturen **mit** zentralisierten Strukturen,
    zum Beispiel Abrufportale oder Spiegelregister:
        <br>a.  Bundesweite Register mit dezentralen Registern auf Landesebene</br> <br>b.  Föderal verteilte Register mit dezentralen Registern auf</br>
           kommunaler Ebene

Eine zentralisierte Struktur, wie ein Spiegelregister oder Abrufportal,
kann im Rahmen eines Nachweisabrufs die Nachweise auf Basis eines
dezentral gepflegten Datenbestandes bereitstellen, ist aber nicht
zugleich für die Datenpflege verantwortlich. Registerstrukturen, die
gemäß Fachdatenkonzept Nachweise ausstellen können, werden
nachweisausstellende Registerstrukturen genannt.

Durch die Nutzung von zentralisierten Strukturen für die Anbindung an
das NOOTS bei verteilten Registern können Funktionen und Aufgaben für
den technischen Anschluss bei der zentralisierten Struktur gebündelt
werden, die ansonsten bei jeder dezentralen Registerinstanz umgesetzt
werden müssen. Mit Hilfe einer zentralisierten Struktur kann ein
NOOTS-Anschluss in vielen Fällen schneller, aufwandsärmer und
wartungsfreundlicher erfolgen. Die technischen, fachlichen, rechtlichen
und organisatorischen Herausforderungen bei der Verwendung von
zentralisierten Strukturen für die NOOTS-Anbindung müssen von den
Fachbereichen bewertet werden.

**XNachweis-Endpunkt**

Ein XNachweis-Endpunkt implementiert die XNachweis-Spezifikation
**[\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)**
und ist Bestandteil des *Data Providers*. Über den XNachweis-Endpunkt
erfolgt die Lieferung von Nachweisen durch den *Data Provider* an das
NOOTS.

**NOOTS Anschlussknoten**

Ein Anschlussknoten ist ein NOOTS-Infrastrukturelement, welches
unmittelbar bei dem *Data Provider* verortet ist und einen sicheren,
einfachen und fehlerfreien Zugang zur NOOTS Transportinfrastruktur
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-19])</B></a> für den Nachweisabruf ermöglicht.
Der Anschlussknoten ist Bestandteil der NOOTS Transportinfrastruktur und ist somit also **kein** Bestandteil
eines *Data Providers*. Der Anschluss eines *Data Providers* an den
NOOTS Anschlussknoten erfolgt über das Anschlussprotokoll zwischen einem
XNachweis-Endpunkt des *Data Providers* und dem Anschlussknoten. 

## Vorbedingungen an NOOTS-Teilnehmer 

Vor einem technischen Anschluss eines *Data Providers* als
NOOTS-Teilnehmer müssen verschiedene technische, rechtliche, fachliche
und organisatorische Vorbedingungen geprüft und berücksichtigt werden.

**Eine vollständige Liste der Vorbedingungen wird in Zukunft in den
Dokumenten zum Fachdatenkonzept und Vorgehensmodell des PB Register
verfügbar sein. Weitere Informationen zum Fachdatenkonzept und
Vorgehensmodell sind im Kapitel \"Abgrenzungen\" zu finden.**

### Technische Vorbedingungen

Die nachweisliefernde Stelle muss technische Vorbedingungen
berücksichtigen, die nicht Bestandteil des technischen NOOTS-Anschlusses
sind, die aber eine effiziente Teilnahme am NOOTS unterstützen.

 | Aufgaben                                                                             | Beschreibung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|--------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Datenabgleich mit  Identitätsdatenabruf IDA                                          | Die nachweisliefernden Stellen, deren Register im IDNrG genannt sind, MÜSSEN die IDNr zusätzlich zu den Basisdaten in ihren Datenbestand integriert haben und Personen über die IDNr eindeutig referenzieren ([§1 IDNrG, [RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)]). Wenn Register nicht im IDNrG genannt sind, dann dürfen die IDNr und Basisdaten nicht in den Datenbestand integriert werden.                                                                                                                                                                                                                                                                                                                                            |
| Datenabgleich mit Unternehmensbasisdatenregister                                     | Die nachweisliefernden Stellen nach § 4 Absatz 1 und § 5 Absatz 1 in (UBRegG,[RGR-02](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)) dürfen die bundeseinheitliche Wirtschaftsnummer für Unternehmen in ihren Registern oder sonstigen Datenbeständen speichern und verwenden, soweit dies für ihre Aufgabenerfüllung erforderlich ist. Die bundeseinheitliche Wirtschaftsnummer für Unternehmen ist bei jeder Übermittlung an das und aus dem Basisregister anzugeben, wenn sie vergeben und durch das Basisregister an die Quellregister übermittelt wurde ([§2 (3) UBRegG, [RGR-02](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)]). Als bundeseinheitliche Wirtschaftsnummer für Unternehmen dient die Wirtschafts-Identifikationsnummer nach § 139c der Abgabenordnung([§2 (1) UBRegG, [RGR-02](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)]). |
| Bereitstellung Schnittstelle Datenschutzcockpit für natürliche Personen              | Die nachweisliefernde Stellen, die Nachweisabrufe mittels IDNr für natürliche Personen ermöglichen, sind verpflichtet, Protokollinformationen zu den Nachweisabrufen über eine Datenschutzcockpit-Schnittstelle gemäß XDatenschutzcockpit-Standard [XS-02](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit) zur Verfügung zu stellen ([§2 (3), §9 (2) IDNrG, [RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)]).                                                                                                                                                                                                                                                                                                                                                                                     |
| Bereitstellung Protokollinformationen für Unternehmen im Sinne des § 3 Abs. 1 UBRegG | Die nachweisliefernden Stellen, die Nachweisabrufe mittels beWiNr für Unternehmen im Sinne des § 3 Abs. 1 UBRegG ermöglichen, sind verpflichtet, Protokollinformationen zu den Nachweisabrufen zur Verfügung zu stellen ([§7 (3) UBRegG, [RGR-02](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)]). |
|Bewertung der Vorteile und Risiken bei Einsatz von zentralisierten Strukturen | Die nachweisliefernde Stelle bewertet im Vorfeld die Vorteile und Risiken bei einer Anbindung an das NOOTS von dezentralen Registerstrukturen über vorhandene oder neue zentralisierte Registerstrukturen (IT-PLR-Beschluss2022/34 [IT-PLR-E-07](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_NOOTS-Registeranbindung.pdf)). |                                                                                                                                                                                                                                                                                                                     

### Rechtliche Vorbedingungen

Die folgende Tabelle enthält beispielhaft Voraussetzungen an die
rechtssetzenden und rechtsunterworfenen Stellen. Bei allen Änderungen
auf Seiten der nachweisliefernden Stelle muss diese sicherstellen, dass
sie nicht in Konflikt mit rechtlichen Regelungen stehen. Rechtsanpassung
sind i.d.R. zeitaufwändig und sollten sehr frühzeitig initiiert werden.
Voraussichtlich sind Gesetze, Verordnungen, Handlungsanweisungen auf
Bundes-, Landes- oder Kommunalebene betroffen.

 | Aufgaben                                                                       | Beschreibung                                                                                                                                                                                                                                                                                              |
|--------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Prüfung der Protokollierung und Datenbereinigung gemäß Fachrecht               | Die nachweisliefernde Stelle muss sicherstellen, dass sie alle Anforderungen zur Protokollierung und Datenbereinigung aus ihrem Fachrecht erfüllt.                                                                                                                                                        |
| Prüfung der Rechtsgrundlage für die Ausstellung von Nachweisen gemäß Fachrecht | Die nachweisliefernde Stelle muss entsprechend ihrem Fachrecht sicherstellen, dass die rechtlichen Grundlagen für den Austausch der Nachweise vorhanden sind und der Austausch mit dem anfragenden Data Consumer in dem entsprechenden Kontext und auf dem entsprechenden Kommunikationsweg zulässig ist. |


### Fachliche Vorbedingungen

Die folgende Tabelle enthält Vorbedingungen an die Fachdomäne, die
erfüllt sein sollen oder müssen, bevor der DP am NOOTS teilnehmen kann.

| Aufgaben                                         | Beschreibung       |
|--------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Ausstellung der Nachweise gemäß Fachdatenkonzept | <p><p style="color:#184B76;">Die Nachweisdaten, die über XNachweis ausgetauscht werden, müssen semantisch und syntaktisch standardisiert sein, damit nachweisanfragende Stellen und nachweisliefernde Stellen die Informationen korrekt interpretieren können. Die Standardisierung muss der nachweisanfragenden Stelle und der nachweisliefernden Stelle bekannt sein.</p></p> <p><p style="color:#184B76;">XNachweis [XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis) enthält keine Standardisierung der Fachinhalte.</p></p> <p><p style="color:#184B76;">Als Grundlage der Bewertung der Register bzw. der Nachweise sollte der IT-Planungsrat-Beschluss 22/2022 ([[IT-PLR-B-08]](https://www.it-planungsrat.de/beschluss/beschluss-2022-22)) herangezogen werden, der den notwendigen Reifegrad der Register und Nachweise zum Anschluss an das NOOTS darstellt.</p> |
| Definition der Berechtigungen                    | Neben der Standardisierung der Nachweisdaten muss auch das Berechtigungsmodell definiert sein. Aus Basis der Rechtsgrundlagen für den Nachweisabruf müssen die Berechtigungen im IAM für Behörden hinterlegt werden.|


### Organisatorische Vorbedingungen

Die folgende Tabelle enthält beispielhaft Vorbedingungen, die sich auf
die Organisation der nachweisliefernden Stelle beziehen.

 | Aufgaben                                                                                  | Beschreibung                                                                                                                                                                                                                                                                   |
|-------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Anpassen bzw. Einrichten von betrieblichen Supportprozessen zur Erfüllung der NOOTS-SLA's | Die technischen Systeme eines Data Providers müssen betrieben und gewartet werden. Zur Erfüllung der Service Level Agreements (SLAs) des NOOTS müssen betriebliche Support- und Wartungsprozesse mit zugehörigen Organisationsstrukturen angepasst bzw. neu eingeführt werden. |


## Anforderungen an NOOTS-Teilnehmer

Für den Anschluss an das NOOTS muss der *Data Provider* die
verschiedenen Anforderungen erfüllen, die im Folgenden dargestellt
sind. Die Anforderungen sind jeweils einem Architekturzielbild des NOOTS
zugeordnet (vgl.
Umsetzungsstufen <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-20])</B></a>)
und erst beim Erreichen des jeweiligen Architekturzielbilds umzusetzen.
Die Maßnahmen zur Umsetzung der Anforderungen werden in den Kapiteln
\"6. Technischer Anschluss eines NOOTS-Teilnehmers\" und \"7.
Administration eines NOOTS Teilnehmers\" beschrieben.

                                                                                      
### Funktionale Anforderungen

Funktionale Anforderungen sind die geforderten Funktionalitäten oder
Verhalten des *Data Providers*. Sie beschreiben, was der zu entwickelnde
*Data Provider* tun oder können soll.

 | Nr.       | Anforderung                                                                                                                                                                                                                                        | Architekturzielbild des NOOTS | Referenz Kapitel                                  |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|---------------------------------------------------|
| NOOTS-363 | Der Data Provider MUSS den Nachweisabrufstandard XNachweis [[XS-01]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis) implementieren.                                                                                                                                                                 | 2025                          | Technische Anbindung, XNachweis-Endpunkt, XNachweis-Spezifikation, Ablauf einer interaktiven Nachweislieferung           |
| NOOTS-880 | Der Data Provider MUSS die Identifizierung einer natürlichen Person anhand von Basisdaten ermöglichen, wenn das Register Daten von natürlichen Personen führt.                                                                                     | 2025                          | Technische Anbindung, XNachweis-Endpunkt, XNachweis-Spezifikation, Ablauf einer interaktiven Nachweislieferung                           |
| NOOTS-854 | Der Data Provider MUSS die Identifizierung einer natürlichen Person anhand der **IDNr** ermöglichen, wenn das Register Daten von natürlichen Personen führt.                                                                                           | 2025                          | Technische Anbindung, XNachweis-Endpunkt, XNachweis-Spezifikation, Ablauf einer interaktiven Nachweislieferung  |
| NOOTS-855 | Der Data Provider MUSS die Identifizierung von Unternehmen im Sinne des § 3 Abs. 1 UBRegG anhand von Basisdaten ermöglichen, wenn das Register Daten von Unternehmen im Sinne des § 3 Abs. 1 UBRegG führt.                                         | 2028                          | Technische Anbindung, XNachweis-Endpunkt, XNachweis-Spezifikation, Ablauf einer interaktiven Nachweislieferung                                                  |
| NOOTS-856 | Der Data Provider MUSS die Identifizierung von Unternehmen im Sinne des § 3 Abs. 1 UBRegG anhand der bundeseinheitlichen Wirtschaftsnummer ermöglichen, wenn das Register Daten von Unternehmen im Sinne des § 3 Abs. 1 **UBRegG** nach **beWiNr** führt.  | 2028                          | Technische Anbindung, XNachweis-Endpunkt, XNachweis-Spezifikation, Ablauf einer interaktiven Nachweislieferung                                                  |
| NOOTS-519 | Data Provider, die Nachweisabrufe mittels IDNr für natürliche Personen ermöglichen, sind verpflichtet, diese Abrufe inklusive der übermittelten Inhaltsdaten zu protokollieren [IDNrG §9, [RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)]. Der Data Provider MUSS einen Nachweisabruf, der die IDNr für natürliche Personen enthält, so protokollieren, dass er auf Anfrage die Informationen zu einem Nachweisabruf gemäß XDatenschutzcockpit [XS-02](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit)  liefern kann.                                 | 2025                          | Ablauf einer interaktiven Nachweislieferung, Fachliche Protokollierung      |
| NOOTS-862 | Der Data Provider, der Nachweisabrufe mittels beWiNr für Unternehmen im Sinne des § 3 Abs. 1 UBRegG ermöglicht, MUSS diese Abrufe inklusive der übermittelten Inhaltsdaten protokollieren [UBRegG §7, [[RGR-02]](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)].                                   | 2028                          | Ablauf einer interaktiven Nachweislieferung, Fachliche Protokollierung       |
| NOOTS-351 | Der Data Provider MUSS Zuständigkeitsparameter, notwendiges Vertrauensniveau, Nachweisformate und die Möglichkeiten eines Abrufs über die **IDNr** bzw. beWinNr für jeden angebotenen Nachweistypen in der **RDN** hinterlegen.                            | 2025                          | Lieferung initiale Daten  Registerdatennavigation, Aktualisierung Registerdatennavigation |
| NOOTS-362 | Der Data Provider MUSS zu jedem angebotenen Nachweistyp die erforderlichen Verbindungsparameter in der **RDN** bereitstellen.                                                                                                                          | 2025                          | Lieferung initiale Daten Registerdatennavigation, Aktualisierung Registerdatennavigation  |
| NOOTS-503 | Der Data Provider MUSS bei Verschlüsselungen und Siegelungen von der V-PKI ausgestelltes Zertifikate verwenden. | 2025 | Beschaffung elektronische Zertifikate | 
| NOOTS-541 | Der Data Provider MUSS zur Validierung von Zertifikaten eingehender Anfragen die V-PKI verwenden. | 2025 | Beschaffung elektronische Zertifikate |
| NOOTS-355 | Der Data Provider MUSS sich selbst mittels eines von der V-PKI ausgestellten Zertifikates authentisieren können.                                                                                                                                   | 2025                          | Registrierung als NOOTS-Teilnehmer                |
| NOOTS-353 | Der Data Provider MUSS das einheitliche IAM für Behörden nutzen, sobald es verfügbar ist. | 2028 | Registrierung als NOOTS-Teilnehmer |        | NOOTS-410 | Der Data Provider MUSS sich über den sicheren Anschlussknoten an das NOOTS anschließen.                                                                                                                                                            | 2025                          | Technische Anbindung XNachweis-Endpunkt, Anbindung Transportinfrastruktur          |
| NOOTS-881 | Der Data Provider MUSS interaktive Nachweisabrufe ermöglichen.                                                                                                                                                                                     | 2025                          | Abrufarten                                        |
| NOOTS-882 | Der Data Provider SOLL nicht-interaktive Nachweisabrufe ermöglichen.                                                                                                                                                                               | 2025                          | Abrufarten                                        |


Tabelle: Liste Funktionale Anforderungen *Data Provider*

### Nicht-funktionale Anforderungen

Nicht-Funktionale Anforderungen sind Anforderungen an die Qualität, in
welcher die geforderte Funktionalität zu erbringen ist. Dazu zählen
beispielsweise Verfügbarkeit und Antwortzeiten eines Systems.

 | Nr.       | Anforderung                                                                                                                                                                                                                            | Architekturzielbild des NOOTS | Referenz Kapitel                      |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|---------------------------------------|
| NOOTS-859 | Der Data Provider SOLL bei interaktiven Nachweisabrufen innerhalb von 3 Sekunden eine Antwort liefern.                                                                                                                                 | 2025                          | Antwortzeiten von Nachweisabrufen     |
| NOOTS-883 | Der Data Provider MUSS eine effiziente Lokalisierung und Analyse von Fehlern wirksam unterstützen.                                                                                                                                     | 2025                          | Technische Protokollierung            |
| NOOTS-884 | Der Data Provider MUSS die einschlägigen nationalen IT-Sicherheitsstandards erfüllen.                                                                                                                                                  | 2025                          |                           |
| NOOTS-885 | Der Data Provider MUSS Mechanismen vorsehen, die den Parallelbetrieb unterschiedlicher Versionen einzelner Standards und Schnittstellen erlauben, so dass Umstellungen nicht zeitgleich in allen betroffenen Systemen erfolgen müssen. | 2025                          | Releases                              |
| NOOTS-886 | Der Data Provider MUSS alle maßgeblichen Datenschutzvorschriften einhalten.                                                                                                                                                            | 2025                          | Fachliche Protokollierung, Technische Protokollierung           |
| NOOTS-887 | Der Data Provider MUSS Protokolldaten bezüglich der gelieferten Nachweise im Standardfall zwei Jahre aufbewahren und danach unverzüglich löschen.                                                                                      | 2025                          |            |
| NOOTS-888 | Der Data Provider MUSS sicherstellen, dass in der Leistungsverwaltung personenbezogene Daten ausschließlich von berechtigten Personen abgerufen werden können.                                                                         | 2025                          |                                       |

Tabelle: Liste Nicht-funktionale Anforderungen *Data Provider*

## Technischer Anschluss eines NOOTS-Teilnehmers

Im Folgenden wird der technische Anschluss zum Nachweisabruf eines *Data
Providers* beschrieben. Die notwendigen Aktivitäten für die
Administration eines *Data Providers* zum technischen Anschluss an das
NOOTS werden im Kapitel \"NOOTS-Administration\" betrachtet.

### Systemumgebung Data Provider

*Data Provider* interagieren über Schnittstellen mit anderen
NOOTS-Komponenten und Systemen. Die nachfolgende Abbildung
veranschaulicht die Kommunikationsbeziehungen der *Data Provider* zu
anderen Komponenten. An dieser Stelle wird bewusst auf die Darstellung
der Topologie der NOOTS-Transportinfrastruktur verzichtet, um eine
vereinfachte Darstellung zu gewährleisten.

![C:\\12529487e17d55e82264e3319f9ba21e](./assets/images2/media/image2.png)
<br>**Abb. 2: Systemumgebung Data Provider**</br>

Die Komponenten der Systemumgebung werden in folgende Bereiche
unterteilt:

| Bereich                 | Beschreibung                                                                                                                                                   | Dokument-Referenzen                                          |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------|
| Data Provider           | ist ein NOOTS Teilnehmer zur Lieferung von Nachweisen einer nachweisausstellenden Registerstruktur. Die Funktionalität eines Data Provider unterteilt sich in: <ul><li><p style="color:#184B76;">**technischer Anschluss:** NOOTS Anschluss über den XNachweis-Endpunkt und NOOTS-Anschlussknoten;</p> <li><p style="color:#184B76;">**fachlicher Anschluss:** nachweisausstellende Registerstrukturen zur Erzeugung der fachlichen Nachweise</p></ul> | <ul><li><p style="color:#184B76;">dieses Anschlusskonzept</p> <li><p style="color:#184B76;">Vorgehensmodell und Fachdatenkonzept PB Register</p></ul>             |
| NOOTS Anschlussknoten   | ist eine NOOTS-Komponente für den sicheren Zugang eines Data Providers an die NOOTS-Transportinfrastruktur zur Lieferung von Nachweisen.                       | <ul><li><p style="color:#184B76;">Übergreifendes Konzept Transportinfrastruktur <a href="https://noots-bmi-f1b75b263f79b0c227f60ca64a041cc06082286d3520c034f8662.usercontent.opencode.de/Glossar/" target= "_blank">[NOOTS-19]</a></p></ul>  |
| NOOTS-Komponenten       | die für den Data Provider relevant sind.                                                                                                                       | <p><p style="color:#184B76;">siehe AD-NOOTS Konzepte:</p></p> <ul><li><p style="color:#184B76;">Übergreifendes Konzept Zertifikate (V-PKI) <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-16]</a></p> <li><p style="color:#184B76;">Grobkonzept Intermediäre Plattform (IP) <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-06]</a></p> <li><p style="color:#184B76;">Komponentensteckbrief Datenschutzcockpit (DSC) <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank">[AD-NOOTS-09] </a></p></ul>                                    |
| nationale Data Consumer | sind NOOTS-Teilnehmer wie zum Beispiel Onlinedienste oder Fachverfahren zum Abruf von Nachweisen über das NOOTS.                                               | <ul><li><p style="color:#184B76;">Anschlusskonzept Data Consumer (DC) <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">[NOOTS-01]</a></p></ul>         |
| Vorbedingungen          | Komponenten und Schnittstellen zur Erfüllung der Vorbedingungen (siehe Kapitel "Vorbedingungen an NOOTS-Teilnehmer").                                          | <ul><li><p style="color:#184B76;">Identitätsdatenabruf (IDA): [[XRepository:XBasisdaten]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten_1.4#version)</p> <li><p style="color:#184B76;">Unternehmensbasisdatenregister: XUnternehmen</p>    |

Tabelle:  Systemumgebung *Data Provider*

### Abrufarten

Das NOOTS ermöglicht folgende Arten von Nachweisabrufen:

-   **Interaktiv**: Nachweisabruf im Dialog mit einer Person, die für
    sich selbst, in Vertretung einer natürlichen Person oder eines
    Unternehmens im Sinne des § 3 Abs. 1 UBRegG für diese einen Nachweis
    abruft; die Antwort muss in kurzer Zeit zur Verfügung stehen.

-   **Nicht-interaktiv**: Nachweisabruf ohne Beteiligung der betroffenen
    natürlichen Person oder Unternehmen im Sinne des § 3 Abs. 1 UBRegG
    oder ihres Vertreters; hier ist eine lange Antwortzeit (bis zu
    mehreren Tagen) erlaubt. 

Im aktuellen Konzept wird die interaktive Abrufart betrachtet, da diese
gegenüber der nicht-interaktiven Abrufart über eine höhere Priorität
verfügt. Die nicht-interaktive Abrufart wird in einer zukünftigen
Release-Version dieses Dokumentes berücksichtigt. 

### Schnittstelle für die Lieferung von Nachweisen

In der folgenden Tabelle werden die Data Consumer mit den Abrufarten
aufgelistet, an die ein *Data Provider* Nachweise gemäß
XNachweis-Spezifikation
([**\[XS-01\]**](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
liefern kann.

| Data Consumer          | Abrufart          | Anwendungsfall High-Level-Architecture                                                                                                      | Beschreibung                                                                                                                                     |
|------------------------|-------------------|---------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| Onlinedienste          | Interaktiv        | <ul><li><p style="color:#184B76;">1a: Interaktiver Nachweisabruf im NOOTS durch natürliche Person,</p> <li><p style="color:#184B76;">1b: Interaktiver Nachweisabruf im NOOTS durch Unternehmen im Sinne des § 3 Abs. 1 UBRegG.</p></ul>                                                   | Der Abruf erfolgt durch ein nationales Antragsverfahren für natürliche Personen oder Unternehmen im Sinne des § 3 Abs. 1 UBRegG.                  |
| Fachverfahren          | Nicht Interaktiv  | <ul><li><p style="color:#184B76;">2: Nicht-Interaktiver Nachweisabruf im NOOTS</p></ul>                                                                        | Der Abruf erfolgt durch ein nationales Fachverfahren im Rahmen der Bereitstellung einer OZG-Leistung oder im Kontext der Eingriffsverwaltung.     |
| Intermediäre Plattform | Interaktiv        | <ul><li><p style="color:#184B76;">3: Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das Europäische Once-Only-Technical-System (EU-OOTS)</p></ul> | Der Abruf erfolgt durch europäische Evidence Requester via Intermediäre Plattform für natürliche oder Unternehmen im Sinne des § 3 Abs. 1 UBRegG. |


Tabelle: Schnittstelle für die Lieferung von Nachweisen durch den *Data
Provider*

### Schnittstellen aus technischen Vorbedingungen

Die technischen Vorbedingungen beinhalten Aufgaben, die die
Bereitstellung zusätzlicher Schnittstellen durch ein Register
erforderlich machen können. Diese Schnittstellen müssen vor der
Aktivierung der Schnittstelle zur Lieferung von Nachweisen umgesetzt
werden.

 | Sender             | Empfänger                      | Fachstandard                     | Beschreibung                                                                                                                                                                                                                                                                             |
|--------------------|--------------------------------|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Datenschutzcockpit | Register                       | [XRepository: XDatenschutzcockpit](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit) | Für den Fall, dass ein Data Provider einen Nachweisabruf für natürliche Personen mit einer **IDNr** zur Verfügung stellt, ist die Implementierung einer Datenschutzcockpit-Schnittstelle für den Abruf der Protokolldaten zum Nachweisabruf erforderlich.                                    |
| Register           | Identitätsdatenabruf (IDA)     | [XRepository:XBasisdaten](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten_1.4#version)          | Für den Fall, dass ein Data Provider einen Nachweisabruf für natürliche Personen mit einer **IDNr** zur Verfügung stellt, ist die initiale Übernahme der **IDNr** als Identifikationsnummer in den Datenbestand und die Synchronisierung der Basisdaten erforderlich.                            |
| Register           | Unternehmensbasisdatenregister | XUnternehmen                     | Für den Fall, dass ein Data Provider einen Nachweisabruf für Unternehmen im Sinne des § 3 Abs. 1 UBRegG mit einer **beWiNr** zur Verfügung stellt, ist die initiale Übernahme der **beWiNr** als Identifikationsnummer in den Datenbestand und die Synchronisierung der Basisdaten erforderlich. |


Tabelle:  Schnittstellen aus technischen Vorbedingungen 

### Technische Anbindung XNachweis-Endpunkt

Die Registerstrukturen werden über den *Data Provider* an NOOTS
angebunden. Ein XNachweis-Endpunkt verfügt über folgende Merkmale:

-   Implementierung der XNachweis-Spezifikation
    **[\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)**
    (siehe Kapitel \"XNachweis-Spezifikation\")
-   Technische Lieferung von Nachweisen eines Nachweistyps
-   Bestandteil des *Data Providers,* d.h. Umsetzung und Bereitstellung
    XNachweis-Endpunkt erfolgt durch den *Data Provider*
-   Implementierung des NOOTS-Anschlussprotokolls für den Anschluss an
    den NOOTS-Anschlussknoten.

Die Bereitstellung der konkreten technischen Vorgaben des
Anschlussprotokolls liegt in der Verantwortung der
NOOTS*-*Transportinfrastruktur
(<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-19])</B></a>)
und werden so bald verfügbar in diesem Konzept ergänzt.

                                                                                     
![C:\\8a41bfcafd99315fc014617b56ff1d16](./assets/images2/media/image3.png)
<br>**Abb. 3:  XNachweis-Endpunkt des Data Providers**</br>

### XNachweis-Spezifikation

Die XNachweis-Spezifikation im XRepository
**[\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)**
definiert die Nachrichtentypen für den Datenaustausch im NOOTS. Die
Spezifikation basiert auf XÖV-Standards und orientiert sich inhaltlich
an der europäischen EU-EDM Spezifikation. In der aktuellen Version 0.7
liegt der Fokus im Kontext *Data Provider* auf den \"Anwendungsfall 3:
Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das
Europäische Once-Only-Technical-System (EU-OOTS)\" der
High-Level-Architecture
(<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>).

Die notwendigen Erweiterungen der XNachweis-Spezifikation zur Abbildung
der nationalen Anwendungsfälle 1 und 2 der High-Level-Architecture sind
in Q1/2024 geplant. Dabei werden Strukturen um nationale Anforderungen
und Besonderheiten, wie z.B. die eindeutige Identifikationsnummer bei
natürlichen Personen, erweitert.

Für ein gemeinsamen Verständnis werden nachfolgend die wesentlichen
Elemente der XNachweis-Spezifikation
**[\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)**
mit Fokussierung auf den *Data Provider* dargestellt. Weitere Details
sind in der XNachweis-Spezifikation
**[\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)**
zu finden. Im nachfolgenden Kapitel \"Ablauf einer interaktiven
Nachweislieferung\" werden die Prozessschritte einer Lieferung von
Nachweisen beschrieben.

Ein XNachweis-Endpunkt muss folgende Nachrichten der
XNachweis-Spezifikation implementieren:

  | Nachricht                     | Nachrichtentyp          | Beschreibung                                                                                                                                                                                                           |
|-------------------------------|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| DE.EvidenceRequest.0301       | XNachweis-Anfrage       | Parameter, die für die Ausstellung von Nachweisen benötigt werden                                                                                                                                                      |
| DE.EvidenceResponse.0302      | XNachweis-Antwort       | Bereitstellung der Ergebnisse einer erfolgreich verarbeiteten Nachweis-Anfrage in Form von einem oder mehreren Nachweisen                                                                                              |
| DE.EvidenceErrorResponse.0303 | XNachweis-Fehlerantwort | Fehlermeldungen, die durch Fehler bei der syntaktischen und semantischen Validierung der Nachweis-Anfrage oder bei der Bereitstellung von Nachweisen gemeldet werden. Nachweise werden in diesem Fall nicht geliefert. |


Tabelle: XNachweis-Nachrichtentypen für *Data Provider*

#### Nachweisanfrage an einen nationalen Data Provider

Die Parameter, die für die Ausstellung von Nachweisen benötigt werden,
werden über die XML-Struktur \"DE.EvidenceRequest.0301\" übermittelt.

![C:\\73bd5fe11b6976745829647ec6e181b1](./assets/images2/media/image4.png)
<br>**Abb. 4: XNachweis-Anfrage (DE.EvidenceRequest.0301)**</br>

Die folgende Tabelle listet die wesentlichen Elemente einer
XNachweis-Anfrage auf.

| Referenz-ID | Name                 | Klasse              | Beschreibung                                                           |
|-------------|----------------------|---------------------|------------------------------------------------------------------------|
| 1           | angefragter Nachweis | QueryType_NOOTS     | Beschreibung des gewünschten Nachweises mit Nachweistyp, Formaten, ... |
| 2           | Nachweissubjekt      | PersonType, LegalPersonType          | <ul><li><p style="color:#184B76;">Basisdaten für eine natürliche Person</p> <li><p style="color:#184B76;">Basisdaten für Unternehmen im Sinne des § 3 Abs. 1 UBRegG</p></ul>              |
| 3           | Data Consumer        | AgentType, (EvidenceRequester)           | Daten zum Data Consumer                                                |
| 4           | Data Provider        | AgentType_NOOTS, (EvidenceProvider)     | Daten zum Data Provider                                                |


Tabelle:  Elemente der XNachweis-Anfrage: DE.EvidenceRequest.0301

#### Nachweisantwort von einem nationalen Data Provider

Nach einer erfolgreichen Erzeugung von einem oder mehreren Nachweisen
durch den *Data Provider* erfolgt die Übertragung dieser Nachweise in
die XML-Struktur \"RepositoryIObjectTyp\". Ein Nachweis setzt sich
zusammen aus den Nachweisdaten (RepositoryItemRef) sowie seinen
Metadaten (EvidenceMetadata).

![C:\\a1cbac9ca16e7ac24a326833378dec6a](./assets/images2/media/image5.png)
<br>**Abb. 5: Nachweis-Antwort (DE.EvidenceResponse.0302)**</br>

Die folgende Tabelle listet die wesentlichen Elemente einer
XNachweis-Antwort auf.

| Referenz-ID | Name            | Klasse              | Beschreibung                                                              |
|-------------|-----------------|---------------------|---------------------------------------------------------------------------|
| 1           | Data Consumer   | AgentType (EvidenceRequester)           | Daten zum Data Consumer                                                   |
| 2           | Data Provider   | AgentType (EvidenceRequester)           | Daten zum Data Provider                                                   |
| 3           | Nachweisobjekt  | RegistryObjectType  | Nachweisdaten (RepositoryItemRef) und seinen Metadaten (EvidenceMetadata) |


Tabelle: Elemente der XNachweis-Antwort: DE.EvidenceResponse.0302

### Ablauf einer interaktiven Nachweislieferung

Ein *Data Provider* kann eine oder mehrere XNachweis-konforme
Schnittstellen zum Abruf von Nachweisen über XNachweis-Endpunkte
bereitstellen. Die XNachweis-Spezifikation wird in einem eigenen
Dokument im xRepository veröffentlicht
([**\[XS-01\]**](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)). 

> Hinweis zu Identifikationsnummer
>
> Nachweisabrufe über eine XNachweis-Anfrage können mittels Basisdaten als
> Identifikator für eine Person oder Unternehmen, als auch mittels
> **IDNr** bei natürlichen Personen bzw. **beWiNr** bei Unternehmen im
> Sinne des § 3 Abs. 1 UBRegG -- je nach Fachlichkeit der geführten
> Registerdaten -- durchgeführt werden. Wenn der Nachweisabruf über die
> NOOTS-Komponente *Intermediäre Plattform* (IP) erfolgt, muss die
> natürliche Person oder das Unternehmen im Sinne des § 3 Abs. 1 UBRegG
> immer über die Basisdaten identifiziert werden
> <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-06])</B></a>
> Zum einen darf die **IDNr** natürlicher Personen von der IP nicht für
> Nachweisanfragen aus EU-Mitgliedsstaaten genutzt werden, wodurch eine
> Nachweisanfrage anhand der Basisdaten erforderlich wird. Zum anderen ist
> das Vorliegen der **beWiNr **in den Mitgliedstaaten der EU nicht
> zwangsläufig gegeben, weshalb auch hier die Nachweisanfrage anhand von
> Basisdaten möglich sein muss.

Im Folgenden werden die funktionalen Aktivitäten für den technischen
Anschluss beschrieben.

![C:\\3726a3be4014632b2c654c2e5f9d9798](./assets/images2/media/image6.png)
<br>**Abb. 6:  Ablauf einer interaktiven Nachweislieferung**</br>

Im Folgenden werden die einzelnen Prozessschritte des Ablaufs einer
interaktiven Nachweislieferung in tabellarischer Form beschrieben.

| Prozessschritt                                                     | Beschreibung                                                                                                                                                                                                                                                                                  | Anmerkung                                                                                                                                                                                          |
|--------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **[DP-01]**  Nachweis-Anfrage empfangen                                | <ul><li><p style="color:#184B76;">Die Nachweis-Anfrage wird über das NOOTS-Anschlussprotokoll empfangen</p></ul>                                                                                                                                                                                                                         | <ul><li><p style="color:#184B76;">Authentifizierung zwischen Data Provider und Anschlussknoten erfolgt gemäß Anschlussprotokoll</p></ul>                                                                                                      |
| **[DP-02]** XNachweis-Anfrage lesen und validieren                     | <ul><li><p style="color:#184B76;">Parsen und Validieren der XML Struktur XNachweis-Anfrage (DE.EvidenceRequest.0301)</p></ul>                                                                                                                                                                                                            | <ul><li><p style="color:#184B76;">Die Entschlüsselung der XNachweis-Anfrage und Siegelprüfung übernimmt der Anschlussknoten</p> <li>[XRepository:XNachweis](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis_0.7#version) ([[XS-01]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)) <li><p style="color:#184B76;">Abgleich des Vertrauensniveaus des authentifizierten Nutzers im Onlinedienst mit dem erforderlichen Vertrauensniveau für den Nachweisabruf ist in Klärung</p></ul>                                                                                                         |
| **[DP-03]** Prüfung Identifikationsnummer                              | <p><p style="color:#184B76;">Anwendbar nur bei verwaltungsbereichsübergreifendem Nachweisabruf mit Identifikationsnummer.</p></p> <p><p style="color:#184B76;">Prüfen, ob die Identifikationsnummer aus der XNachweis-Anfrage im Datenbestand des Registers eingespeichert ist:</p></p> <ul><li><p style="color:#184B76;">bei einer natürlichen Person über die Identifikationsnummer **IDNr**</p> <li><p style="color:#184B76;">bei Unternehmen im Sinne des § 3 Abs. 1 UBRegG über die **beWiNr**</p></ul> <p><p style="color:#184B76;">Wenn die Prüfung fehlschlägt, dann wird eine XNachweis-Fehlerantwort (DE.EvidenceErrorResponse.0303) erzeugt.</p></p>                                                                                                                                                                                                   | <ul><li><p style="color:#184B76;">Die abstrakte Berechtigungsprüfung erfolgt durch den sicheren Anschlussknoten</p> <li><p style="color:#184B76;">Vorbedingung für die eindeutige Identifizierung mit der **IDNr** bei einer natürlichen Person ist die Datensynchronisation mit Identitätsdatenabruf (IDA)</p> <li><p style="color:#184B76;">Vorbedingung für die eindeutige Identifizierung mit der **beWiNr** bei Unternehmen im Sinne des § 3 Abs. 1 UBRegG ist die Datensynchronisation mit Unternehmensbasisdatenregister</p></ul>
| **[DP-04]** Prüfung Basisdaten                                         | <p><p style="color:#184B76;">Anwendbar nur bei Nachweisabruf **ohne Identifikationsnummer.**</p></p> <p><p style="color:#184B76;">Prüfen anhand der Basisdaten aus der XNachweis-Anfrage, ob eine natürliche Personen oder  Unternehmen im Sinne des § 3 Abs. 1 UBRegG im Datenbestand des Register eingespeichert ist.</p></p> Wenn die Prüfung fehlschlägt, dann wird eine XNachweis-Fehlerantwort (DE.EvidenceErrorResponse.0303) erzeugt.                                                                                                                                                                                                                                   | <ul><li><p style="color:#184B76;">Bei Nachweisabruf über die Intermediäre Plattform (IP) kann die Prüfung nur anhand Basisdaten erfolgen, da im EU-OOTS keine Identifikationsnummer bekannt ist</p></ul>                                      |
| **[DP-05]** Nachweis erzeugen                                          | <ul><li><p style="color:#184B76;">Erzeugung der Nachweise für die natürliche Person bzw. Unternehmen im Sinne des § 3 Abs. 1 UBRegG aus **[DP-03]** bzw. **[DP-04]** auf Basis der Daten aus der XNachweis-Anfrage (Nachweistyp, Format, Datenfelder, …) durch die Registerstruktur.</p> <li><p style="color:#184B76;">In dem Anwendungsfall 3 "Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das EU-OOTS" der High-Level-Architecture <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a> muss ein Nachweisformat in Abhängigkeit vom Umsetzungsstand der zentralen Preview-Komponente der Intermediäre Plattform (IP) geliefert werden:</p>    <ul><li><p style="color:#184B76;">Stufe 1: menschenlesbares Format des Nachweises, i.d.R. ein;</p> <li><p style="color:#184B76;">Stufe 2: strukturiertes Datenformat des Nachweises mit zusätzlichen Formatvorgaben für die menschenlesbare Aufbereitung in der Preview der IP.</p></ul></ul>                                                     | <ul><li><p style="color:#184B76;">Erzeugung von Fachdaten für den Nachweis siehe Fachdatenkonzept</p></ul>                                                                                                                                    |
| **[DP-06]** Fachliche Protokollierung                                  | <p><p style="color:#184B76;">Anwendbar nur bei Nachweisabruf **mit Identifikationsnummer**</p></p> <p><p style="color:#184B76;">Erzeugung eines Protokolleintrages mit Nachweisinhalten:</p></p> <ul><li><p style="color:#184B76;">bei natürlichen Personen mit einer **IDNr** gemäß [**§9 IDNrG,** [[RGR-01]](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)] und</p> <li><p style="color:#184B76;">bei Unternehmen im Sinne des § 3 Abs. 1 UBRegG mit einer **beWiNr** gemäß [**§7 UBRegG , [RGR-02]**]</p></ul>                                                                                                                                                                                                                                     | <ul><li><p style="color:#184B76;">Siehe Kapitel "Fachliche Protokollierung"</p> <li><p style="color:#184B76;">Bei natürlichen Personen müssen die Protokolleinträge so gespeichert werden, dass diese von der Datenschutzcockpit-Schnittstelle gemäß XDatenschutzcockpit-Standard [[XS-02]](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit) ausgelesen werden kann</p></ul> |                                                                                                                                                        |
| **[DP-07]** Protokollierung zur Nachvollziehbarkeit des Nachweisabrufs | <ul><li><p style="color:#184B76;">Technische Protokollierung der Nachweisabrufe zur schnellen Identifizierung von Fehlersituationen</p></ul>                                                                                                                                                                                             | <ul><li><p style="color:#184B76;">Siehe Kapitel "Technische Protokollierung"</p></ul>                                                                                                                                                         |
| **[DP-08]** XNachweis-Antwort erzeugen                                 | <ul><li><p style="color:#184B76;">Erzeugen der  XNachweis-Antwort</p> <ul><li><p style="color:#184B76;">Antwort mit Nachweis: DE.EvidenceResponse.0302</p> <li><p style="color:#184B76;">Fehlermeldung: DE.EvidenceErrorResponse.0303</p></ul>                                                                                                                                                                                                                                                               | <ul><li>[XRepository:XNachweis](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis_0.7#version) ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)) <li><p style="color:#184B76;">Erzeugung von Fachdaten für den Nachweis siehe Fachdatenkonzept</p></ul> | 
| **[DP-09]** XNachweis-Antwort zurücksenden                             | <ul><li><p style="color:#184B76;">Zurücksenden von XNachweis-Antwort und Nachweis</p></ul>                                                                                                                                                                                                                                               |


Tabelle: Prozessschritte des Ablaufs einer interaktiven
Nachweislieferung

### Qualitätsmerkmale

Durch NOOTS werden nicht-funktionale Gesamtanforderungen an den
Nachweisabruf definiert. Diese Gesamtanforderungen müssen auf die
beteiligten NOOTS-Komponenten heruntergebrochen werden.

In dem Dokument \"Once-Only domain Service Level Agreement -- eDelivery
Access Points\" werden Qualitätsmerkmale wie Verfügbarkeit für den
eDelivery Access Point, also der *Intermediären Plattform* (IP) im
NOOTS, durch die EU/SDG vorgegeben. Diese Vorgaben können nicht auf
NOOTS-Komponenten oder NOOTS-Teilnehmer übertragen werden. 

#### Verfügbarkeit

Im Rahmen der NOOTS-übergreifenden nicht-funktionalen Anforderung zur
Verfügbarkeit wird die Gesamtverfügbarkeit eines NOOTS-Nachweisabrufes
unter Berücksichtigung der Anwendungsfälle der High-Level-Architecture
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>
definiert. Daraus werden sich dann die konkreten Anforderungen an die
Verfügbarkeit der beteiligten NOOTS-Komponenten ableiten. Die
geforderten Gesamtverfügbarkeiten für Nachweisabrufe und die
Verfügbarkeiten für die NOOTS-Komponenten werden aktuell noch
ausgearbeitet. 

Die Verfügbarkeiten für den NOOTS-Teilnehmer *Data Provider* werden in
einer späteren Version dieses Dokumentes veröffentlicht.

In den NOOTS Service Level Agreement (SLA) werden die Reaktionszeiten,
Servicezeiten und Reglungen zu Wartungsfenstern definiert. NOOTS Service
Level Agreement (SLA) werden zukünftig veröffentlicht.

#### Antwortzeiten von Nachweisabrufen

Bei einem interaktiven Nachweisabruf durch einen *Data Consumer* sind
kurze Antwortzeiten ein entscheidender Faktor für eine positive
Kundenerfahrung im Onlinedienst. Der *Data Provider* mit seinem
technischen Anschluss und der Ausstellung von Nachweisen hat einen
entscheidenden Einfluss auf die Gesamtantwortzeit. Ein *Data Provider*
soll innerhalb von 3 Sekunden bei interaktiven Nachweisabrufen eine
Antwort liefern. Die Antwortzeit beginnt ab dem Zeitpunkt, an dem die
XNachweis-Anfrage bei dem XNachweis-Endpunkt eingegangen ist, und
erstreckt sich bis zur Lieferung des Nachweises im XNachweis-Endpunkt.

#### Sicherheit

Der technische Anschluss der nachweisausstellenden Registerstrukturen
zur Lieferung von Nachweisen erfolgt über XNachweis-Endpunkte, die als
HTTP-basierte Webservices gemäß NOOTS-Anschlussprotokoll umzusetzen
sind. Neben den Sicherheitsvorgaben aus dem NOOTS-Anschlussprotokoll
sind bei der Implementierung und dem Betrieb der XNachweis-Endpunkte
folgende Sicherheitsrichtlinien zu berücksichtigen (NOOTS-721, NOOTS-738
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>
und NOOTS-884):

-   [IT-Grundschutz
    Kompendium](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Kompendium/IT_Grundschutz_Kompendium_Edition2023.pdf?__blob=publicationFile&v=4#download=1):
    [APP.3.1 Webanwendungen und
    Webservices](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/IT-GS-Kompendium_Einzel_PDFs_2022/06_APP_Anwendungen/APP_3_1_Webanwendungen_und_Webservices_Edition_2022.pdf?__blob=publicationFile&v=4),
-   [ Open Web Application Security Projekt
    (OWASP)](https://owasp.org/API-Security/editions/2023/en/0x11-t10/).

Die sichere Netzintegration von NOOTS-Anschlussknoten und
XNachweis-Endpunkten in das Netz-Zonenmodell der Systeme der
nachweisausstellenden Registerstruktur gemäß IT-Grundschutz \"[NET.1.1:
Netzarchitektur und
-design](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Kompendium_Einzel_PDFs_2021/09_NET_Netze_und_Kommunikation/NET_1_1_Netzarchitektur_und_design_Edition_2021.pdf?__blob=publicationFile&v=23.)\"
und der sichere Betrieb der Systeme gemäß [IT-Grundschutz
Kompendium](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Kompendium/IT_Grundschutz_Kompendium_Edition2023.pdf?__blob=publicationFile&v=4#download=1)
sind nicht Bestandteil dieses Konzeptes.

#### Technische **Protokollierung**

Das NOOTS-Gesamtsystem MUSS eine effiziente Lokalisierung und Analyse
von Fehlern wirksam unterstützen (NOOTS-836
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-03])</B></a>
und NOOTS-883). Dazu ist zwingend erforderlich, dass der Datenfluss
eines Nachrichtenabrufs anhand eines Identifikators durch die
verschiedenen NOOTS-Komponenten effizient verfolgt werden kann -
nachfolgend auch Nachverfolgbarkeit genannt. Zu einem Identifikator
eines Nachweisabrufes können verschiedene Parameter eines
Nachweisabrufes bzw. auch Fehlerursachen protokolliert werden.  

Bei der technischen Protokollierung sind die Vorgaben der
Datenschutzgrundverordnung (DSGVO) einzuhalten. Die technischen
Protokolle müssen anhand von Suchparametern effizient durch berechtigte
Personen analysiert werden können. Durch NOOTS erfolgen keine Vorgaben
zu Protokoll-Formaten oder Analysewerkzeuge.

Folgende Daten aus der XNachweis-Anfrage können beispielsweise für die
technische Protokollierung herangezogen werden.

 | Information    | Beschreibung                                                         | Datenquelle XNachweis: DE.EvidenceRequest.0301                |
|----------------|----------------------------------------------------------------------|---------------------------------------------------------------|
| Identifikator  | Identifikator für die Nachweisanfrage                                | DE.EvidenceRequest.0301.id                                    |
| WER            | Informationen zu Data Consumer                                       | DE.EvidenceRequest.AgentType (EvidenceRequester)              |
| VON WEM        | Informationen zu Data Provider                                       | DE.EvidenceRequest.Agent.IdentificationType(EvidenceProvider) |
| WAS            | Nachweistyp                                                          | QueryType.TypeOfEvidence.DataServiceType.Identifier           |
| WIE            | Nachweisformat, Version, ...                                         | QueryType.TypeOfEvidence.DataServiceType.DistributionType     |
| Wann           | Ausstellungsdatum und -uhrzeit der Nachricht                         | DE.EvidenceRequest.0301.IssueDateTime                         |
| Dauer          | Zeitdauer der Bereitstellung eines Nachweises im XNachweis-Endpunkt  | Zeitmessung im XNachweis-Endpunkt                             |
| Fehlerursache  | Fehlerursache bei Auftreten von technischen oder fachlichen Fehlern  | XNachweis-Endpunkt oder nachweisausstellende Registerstruktur |


Tabelle: Informationen für die technische Protokollierung

#### Fachliche Protokollierung

Nachweisabrufe mit einer Identifikationsnummer müssen protokolliert
werden:

-   bei natürlichen Personen mit einer **IDNr** gemäß
    \[**§9 IDNrG,** [**\[RGR-01\]**](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)\] und
-   bei Unternehmen im Sinne des § 3 Abs. 1 UBRegG mit einer
    **beWiNr **gemäß \[**§7 UBRegG** ,
    [**\[RGR-02\]**](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)\]

Die Aufbewahrungs- und Löschfristen der Protokolldaten sind gemäß
\[**§9 IDNrG,** [**\[RGR-01\]**](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)\] 
und \[**§7 UBRegG** ,
[**\[RGR-02\]**](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)\]
einzuhalten.

Die Protokollierung der Nachweisabrufe bei natürlichen Personen muss mit
Hilfe des XÖV-Standards [XRepository:
XDatenschutzcockpit](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit)
erfolgen
\[**§9 IDNrG,** [**\[RGR-01\]**](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)\]**.**
In der Dokumentation \"Datenschutzcockpit Teil 1: Technologieunabhängige
Spezifikation Version 1.0.0, Kapitel 6.2.3.2. ProtokolldatenListe\"
werden die notwendigen Protokollinhalte beschrieben. Einen ersten
Eindruck vermittelt der Anhang \"Datenschutzcockpit: Auszug
Protokollinformationen\" in diesem Dokument. Die Protokollinformationen
gemäß XDatenschutzcockpit können vom Datenschutzcockpit über eine
Schnittstelle der nachweisausstellenden Registerstruktur abgerufen
werden (siehe technische Vorbedingungen Datenschutzcockpit).

Eine weiterführende fachliche Protokollierung, z.B. zur Erkennung von
Bedrohungen oder der Compliance regulatorischer Vorgaben im Rahmen eines
Security Information and Event Management (SIEM) oder aus dem Fachrecht
der Register begründet, sind nicht Bestandteil dieses Dokumentes.

#### Releases

Das NOOTS erlaubt den Parallelbetrieb unterschiedlicher Versionen
einzelner NOOTS-Standards und Schnittstellen, so dass Umstellungen nicht
zeitgleich in allen betroffenen NOOTS-Komponenten und NOOTS-Teilnehmern
erfolgen müssen. Für den technischen Anschluss des *Data Providers* sind
insbesondere die folgenden Versionen relevant:

-   XNachweis
    ([**\[XS-01\]**](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
-   Anschlussprotokoll zum sicheren Anschlussknoten.

Bei Abkündigung einer Version durch NOOTS muss der *Data Provider*
rechtzeitig auf die aktuelle Version migrieren. Eine maximale Anzahl von
unterstützten Versionen ist nicht definiert.

#### Kompatibilität

Zur Sicherstellung einer reibungslosen Integration der
XNachweis-Endpunkte eines *Data Providers* mit dem NOOTS-Anschlussknoten
sind NOOTS Kompatibilitätstests geplant (NOOTS-105).

## Administration eines NOOTS Teilnehmers

Die technische Anbindung der XNachweis-Endpunkte eines *Data Provider*
an den NOOTS Anschlussknoten erfordert initiale und fortlaufende
administrative Tätigkeiten. Die notwendigen elektronischen Zertifikate
müssen beschafft werden, die Daten für die Registerdatennavigation
geliefert werden und der *Data Provider* muss sich im NOOTS registrieren
und über den Anschlussknoten mit der NOOTS-Transportinfrastruktur
verbinden. Die initialen Schritte werden im folgenden Kapitel
\"Erstanbindung\" beleuchtet und die fortlaufenden Tätigkeiten im
Kapitel \"Administration\".

### Erstanbindung

Im Rahmen der Erstanbindung sind einmalige Schritte zur Einrichtung des
technischen Anschlusses *Data Provider* an das NOOTS notwendig. 

#### Anbindung Transportinfrastruktur

Nachweise werden als elektronische Nachrichten durch die
NOOTS-Transportinfrastruktur unter Gewährleistung bestimmter
Qualitätsmerkmale vom *Data Consumer* zum *Data Provider* übermittelt*.*
Die NOOTS-Transportinfrastruktur bedient sich dazu eines
Transportmediums (z.B. Behördennetze) für den physischen Transport der
Nachrichten, legt darauf ein Transportprotokoll fest und stellt
Infrastrukturelemente, wie die sicheren Anschlussknoten, bereit. Die
sicheren Anschlussknoten sind in unmittelbarer Nähe zu den
XNachweis-Endpunkten eines *Data Providers* verortet.
XNachweis-Endpunkte eines *Data Providers* müssen sich über das
NOOTS-Anschlussprotokoll mit dem Anschlussknoten gemäß den Vorgaben zum
Datentransport der NOOTS Transportinfrastruktur
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-19])</B></a>
technisch verbinden, um Nachweise an *Data Consumer* liefern zu können.


Installation, Betrieb und Wartung des sicheren Anschlussknoten und des
*Data Providers* liegt in der Verantwortung der nachweisausstellenden
Behörde und ist durch diese sicherzustellen. Die Installation und
Konfiguration der Software für den sicheren Anschlussknoten und *Data
Provider* umfasst auch, sofern erforderlich, die Netzfreischaltungen
zwischen den sicheren Anschlussknoten und der
NOOTS-Transportinfrastruktur.  

*Die Vorgaben zum Datentransport werden derzeit erarbeitet und in einer
künftigen Version bereitgestellt. Diese werden die vorgenannten Aspekte
beinhalten.*

#### Beschaffung elektronische Zertifikate

Der Einsatz von der V-PKI ausgestellten Zertifikate ist Bestandteil des
NOOTS-Anschlusses für *Data Provider*. Für den NOOTS
Nachrichtenaustausch werden die Inhaltsdaten von Nachrichten mit Hilfe
von der V-PKI ausgestellten Zertifikate signiert und verschlüsselt
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-16])</B></a>.
Bei den Inhaltsdaten von NOOTS Nachrichten handelt es sich um XNachweis-
und Nachweis-Daten. Da der *Data Provider* als IT-Lösung zur Umsetzung
von automatisierten IT-Prozessen zu verstehen ist, sind die von der
V-PKI ausgestellten Zertifikate der Variante 4 für automatisierte
IT-Prozesse zu verwenden
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-16])</B></a>.

Die Beantragung eines von der V-PKI ausgestellten Zertifikats für
Siegelprüfungen, Datenverschlüsselungen oder für Authentisierungen gemäß
Konzeption IAM für Behörden
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-05])</B></a>
erfolgt durch die zuständige Behörde gemäß \"Anwendungsfälle im Kontext
der Administration/Registrierung\"
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-16])</B></a>.

> Hinweis zu Nutzungsarten von Schlüsseln
>
> Über die Zertifikatserweiterung „KeyUsage" kann der Verwendungszweck der
> Zertifikate für Siegelprüfungen, Datenverschlüsselungen oder für
> Authentisierungen eingeschränkt werden. Weitere Details sind in
> [\"Zertifizierungsinfrastruktur für die PKI-1-Verwaltung Kapitel 2.2.3
> Nutzungsarten von
> Schlüsseln\"](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/VerwaltungsPKI/techwurzelz_pdf.pdf?__blob=publicationFile&v=1)
> zu finden.

Die Zertifikate müssen in folgenden NOOTS-Komponenten hinterlegt werden:

 | NOOTS-Komponente        | Zertifikat mit Verwendungszweck |
|-------------------------|---------------------------------|
| Registerdatennavigation | Datenverschlüsselungen          |
| IAM Behörden            | Authentisierung                 |


**Weitere Zertifikate**

In Abhängigkeit vom gewählten Anschlussprotokoll sind ggf. weitere
Zertifikate für den Transport, z.B. TLS-Zertifikate, notwendig. Die
Vorgaben werden durch NOOTS Transportinfrastruktur
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-19])</B></a>
festgelegt.

#### Registrierung als NOOTS-Teilnehmer

Ein *Data Provider* ist ein NOOTS-Teilnehmer und muss bei IAM für
Behörden mit der Teilnahmeart \"Data Provider\" und seiner
Behördenfunktion registriert werden. Die Registrierung ist notwendig,
damit der *Data Provider* an der abstrakten Berechtigungsprüfung der
Vermittlungsstellen
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-08])</B></a>
teilnehmen kann. Dabei muss ein in der V-PKI hinterlegtes Zertifikat mit
der Nutzungsart für die Authentisierung referenziert werden. Mit
demselben Zertifikat darf kein weiterer *Data Provider* mit der
Behördenfunktion registriert werden.

Es ist davon auszugehen, dass IAM für Behörden stufenweise eingeführt
wird
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-05])</B></a>.

#### Lieferung initiale Daten Registerdatennavigation

Um die Konfigurationsdaten für den *Data Provider*, wie beispielsweise
Verbindungs- und Zuständigkeitsparameter zur Lokalisierung des *Data
Providers*, in der Registerdatennavigation zu hinterlegen, ist die
Nutzung eines Pflegesystems der Registerdatennavigation erforderlich.
Eine Aufstellung der erforderlichen Konfigurationsdaten und
Informationen zum Pflegesystem sind in der Schnittstellenbeschreibung
\"Pflege Dienste und Zuständigkeiten\" im Kapitel \"Schnittstellen der
Registerdatennavigation\" im Konzept \"Grobkonzept
Registerdatennavigation
(RDN)\"<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-07])</B></a>
zu finden.

### Fortlaufende Administration

Nach erfolgreicher Erstanbindung und der Aufnahme des Betriebes der
technischen Anbindung sind kontinuierliche Tätigkeiten erforderlich.

#### Aktualisierung elektronische Zertifikate

Von der V-PKI ausgestellte Zertifikate verfügen über ein zeitliches
Ablaufdatum. Rechtzeitig vor dem Ablaufdatum müssen die Zertifikate
gemäß \"Anwendungsfälle im Kontext der
Administration/Registrierung\"<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-16])</B></a>
erneuert werden.

#### Aktualisierung Registerdatennavigation

Die Aktualisierung von Konfigurationsdaten für den *Data Provider* in
der Registerdatennavigation erfolgt durch die Schnittstelle \"Pflege
Dienste und Zuständigkeiten\" im Pflegesystem der
Registerdatennavigation.

## Ausblick & Weiterführende Aspekte

Der technische NOOTS-Anschluss eines *Data Providers* ist insbesondere
abhängig von den Konzepten der Transportinfrastruktur
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-19])</B></a>
und anderer NOOTS-Komponenten, die in der Systemumgebung im Kapitel
\"Technischer Anschluss eines NOOTS Teilnehmers\" dargestellt werden.
Diese Konzepte werden im Rahmen des NOOTS-Anforderungsmanagement
angepasst oder erweitert. Diese Weiterentwicklung der Konzepte wird auch
Einfluss auf die Inhalte dieses Konzeptes haben, was eine Aktualisierung
in zukünftigen Releases zur Folge hat. 

Im Folgenden werden einige geplante Erweiterungen für dieses Konzept
aufgelistet:

-   Konkretisierung der technischen Vorgaben zum Anschluss des *Data
    Providers* an das NOOTS über den sicheren Anschlussknoten
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-19])</B></a>
-   Konkretisierung zum sicheren Anschlussknoten: zum Beispiel Zugriff
    auf IAM Behörden und Registerdatennavigation über Anschlussknoten 
-   Konkretisierung des Konzepts \"Sicherer Anschlussknoten\" kann zu
    Änderungen der Anforderungen an den *Data Provider* führen
-   Änderungen in der Konzeption der Registerdatennavigation können zu
    Änderungen der Anforderungen an den *Data Provider* führen
-   Erweiterung der funktionalen und nicht-Funktionalen Anforderungen,
    zum Beispiel Verfügbarkeit
-   Betrachtung der nicht-interaktiven Abrufart
-   Erweiterung der XNachweis-Spezifikation
    **[\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)**
    zur Abbildung der nationalen Anwendungsfälle 1 und 2 der
    High-Level-Architecture. Dabei werden Strukturen um nationale
    Anforderungen und Besonderheiten erweitert, wie z.B. die eindeutige
    Identifikationsnummer bei natürlichen Personen, Daten für die
    fachliche Protokollierung oder Zuständigkeitsparameter für die
    Zuständigkeitsfindung in zentralisierten Registerstrukturen
-   Checkliste für *Data Provider* soll einen knappen Überblick über die
    nötigen Maßnahmen verschaffen. 

Die Weiterentwicklung dieses Konzeptes hat aber nur geringe Auswirkungen
auf die Vorbedingungen zum technischen Anschluss, wie beispielsweise die
Ertüchtigung der Registerstrukturen oder die fachliche Spezifikation der
Nachweise im Rahmen des Fachdatenkonzeptes bzw. Vorgehensmodell.

## Anhang

### Datenschutzcockpit: Auszug Protokollinformationen

In der Dokumentation \"Datenschutzcockpit Teil 1: Technologieunabhängige
Spezifikation Version 1.0.0, Kapitel 6.2.3.2. ProtokolldatenListe\"
werden die notwendigen Informationen für ein Protokolleintrag gemäß
[XRepository:
XDatenschutzcockpit](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit) beschrieben.
In der folgenden Abbildung wird ein beispielhafter Protokolleintrag
dargestellt.

![Protokoll](./assets/images2/media/image7.PNG)
<br>**Abb. 7 Auszug Protokollinformationen DSC**</br>