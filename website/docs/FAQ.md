### **Diese FAQs basieren auf dem Feedback, das im Rahmen des Konsultationsprozesses der AD-NOOTS eingegangen ist.**



### Warum werden IT-Sicherheitsfragen nicht ausreichend adressiert?

> Die bisher veröffentlichten AD-NOOTS (Architektur Dokumente NOOTS) beschäftigen sich noch nicht ausdrücklich mit der Sicherheitsthematik. Es gibt zu den Sicherheitsthemen bisher noch kein eigenes Dokument in den AD-NOOTS.
Um das Thema vollumfänglich zu betrachten, müssen über die bisher angewendeten „Security by Design“ Ansätze hinaus, mögliche Angriffsszenarien umfassend analysiert werden. Das wird momentan noch getan und eine entsprechende Dokumentation erstellt.




### In welchem Dokument wird das Thema IT-Sicherheit adressiert?

> Momentan ist der Arbeitstitel „Übergreifendes Konzept Zero-Trust Architektur (ZTA)“. Im Kontext der AD-NOOTS wird nur das Thema Risikobasierte Sicherheitsanalyse und, daraus abgeleitet, das Konzept einer Zero-Trust IT-Architektur für das NOOTS betrachtet. Diskussionsfähig wird das Dokument voraussichtlich bis Ende des Jahres.




### Wie werden die SAKs bereitgestellt?

> Die SAKs sind ein wichtiger Teil der NOOTS Transportinfrastruktur. Als solche müssen sie zentral bereitgestellt werden. Die Frage wie das NOOTS und damit die SAKs in Betrieb genommen werden, ist aktuell in Klärung und beinhaltet u. a.  die kontinuierlichen Weiterentwicklungs- und Rolloutprozesse der NOOTS Komponenten und damit auch wie die SAKs aussehen müssen. Diese Information wird zukünftig in den Dokumenten zum Rollout adressiert.




### Wo werden die SAKs betrieben?

> Die SAKs müssen vom Data Consumer/Provider betrieben werden. Sie werden zwar zentral bereitgestellt und gemonitort, aber der Betrieb erfolgt in Verantwortung von DC/DP.




### Warum wird nicht die bereits vorhandene Infrastruktur von OSCI/XTA verwendet?

> Die aktuell vorhandene Infrastruktur unter Verwendung von OSCI (und XTA) ist für einen ganz anderen Zweck gebaut. Dabei werden vergleichsweise aufwendig Komponenten vorgehalten, die bestimmten Prozessen der Nachvollziehbarkeit in asynchronen Prozessen dienen. Für die augenblickliche Verwendung und den Scope von OSCI ist das nachvollziehbar. 
Das NOOTS (in Befolgung von EU Vorgaben) bedient ein anderes Prinzip. So ist der Status eines interaktiven Nachweisabrufs in diesem Moment gültig (Stichwort: Authenticated Protected Channel) und kann so sauber dokumentiert werden. Damit wären viele Funktionen von OSCI nicht erforderlich und das NOOTS bietet somit die Möglichkeit, eine ressourcenschonende Lösung umzusetzen.
Wenn man also OSCI für das NOOTS verwenden wollte, müsste man die nicht erforderlichen Systeme trotzdem komplett neu aufsetzen und neu konfigurieren. 
Zusammenfassend würde mit der Nutzung von OSCI eine Infrastruktur, die primär für einen anderen Zweck entworfen wurde, für Arbeit des NOOTS zweckentfremdet. 





### Warum wird ein tokenbasierter Dienst verwendet, obwohl in der Anmerkung zum IDNrG steht, dass die Vermittlungsstelle sich „auf“ der Transportstrecke befinden muss?

> Der Wortlaut in § 7 Abs. 2 S. 1 IDNrG spricht davon, dass Datenübermittlungen unter Nutzung einer IDNr über Vermittlungsstellen erfolgen. Am reinen Wortlaut orientiert müssten Vermittlungsstellen streng genommen Bestandteil der Transportinfrastruktur sein, über die die Kommunikation läuft und diese Transportinfrastruktur auch zur Verfügung stellen. 
Bei der nun gewählten Lösung sind die Vermittlungsstellen zwar Bestandteil der Transportinfrastruktur, stellen diese jedoch nicht zur Verfügung. Die Kommunikation erfolgt über Sichere Anschlussknoten (SAK) und nicht zwischen Data Consumer (DC) und Data Provider (DP). Diese Lösung ist unter datenschutzrechtlichen Aspekten entwickelt worden. Als Ergebnis der Prüfung verschiedener Lösungsansätze wird sowohl aus juristischer als auch technischer Sicht der Datenschutzanforderung mittels abstrakter, und unabhängig von Sender und Empfänger stattfindender, Berechtigungsprüfung mit der augenblicklichen Lösung vollumfänglich Rechnung getragen. Gleichzeitig wird mit dem gewählten Lösungsansatz der Anpassungsaufwand gering gehalten und zudem ein weiteres Sicherheitsrisiko (ausschließlich abstrakte Berechtigungsprüfung durch die Vermittlungsstellen verhindert nicht das Risiko der unberechtigten Kommunikation zwischen DC und DP im Fall der Angabe von falschen Metadaten) vermieden. Der Wortlaut des § 7 Abs. 2 S.1 IDNrG wird in Zukunft konsequenterweise angepasst, so dass sich der Sinn und Zweck der Norm – Entwicklung einer datenschutzkonformen und vertrauenswürdigen Transportinfrastruktur – auch im Wortlaut des Gesetzestextes widerspiegeln.
