>**Redaktioneller Hinweis**
>
>Dokument aus der vorangegangen Iteration - nicht Teil der aktuellen Iteration.
>
>Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.
>
> Dieses Konzept wurde zuletzt im Zeitraum Oktober - Dezember 2023 im
> PB *NOOTS* überarbeitet. Es liefert ein Update und kann gerne
> kommentiert werden. Der derzeitige Arbeitsstand wird sich sehr
> wahrscheinlich auf Grund folgender Einflüsse noch ändern:
>
>-  Mit Beschluss 2023/38 vom 3.11.2023
>    **\[[IT-PLR-B-10](https://www.it-planungsrat.de/beschluss/beschluss-2023-38)\]**
>    beauftragt der IT-Planungsrat die Gesamtsteuerung
>    Registermodernisierung mit der \"Konzeption einer NOOTS-spezifischen
>    Kommunikationsinfrastruktur\", welche \"zukunftsfähig konzeptioniert
>    werden \[soll\], so dass ein Wechsel von zunächst genutzten
>    IT-Standards bzw. Komponenten zu europäisch oder international
>    genutzten Alternativen ermöglicht wird\". Diese Konzeptionsarbeiten
>    sind noch nicht abgeschlossen. Ein wesentliches Element dieser
>    laufenden Konzeptionsarbeiten ist ein sogenannter \"Sicherer
>    Anschlussknoten\", der Data Consumer und Data Provider entlasten und
>    technische Details der Transporttechnologie (Verschlüsselung,
>    Verbindungsparameter etc.) kapseln soll. Daraus resultieren
>    Änderungen bei den Schnittstellenbeschreibungen der
>    Registerdatennavigation (RDN), die in dieser Version der
>    Grobkonzeption auf Annahmen beruhen.>
>-   In XNachweis, Version 0.7 vom 31.10.2023
>    **\[[XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)\]**
>    sind die Nachrichten für die rein nationalen Anwendungsfälle noch
>    nicht enthalten. Im grenzüberschreitenden Kontext wird die RDN bei
>    einem Nachweisabruf aus dem europäischen Ausland von der
>    Intermediären Plattform genutzt. Diese Version der Grobkonzeption
>    RDN nimmt an, dass die Unterschiede zwischen den nationalen
>    Anwendungsfällen und dem Nachweisabruf aus dem europäischen Ausland
>    geringgehalten werden und orientiert sich an den in XNachweis v0.7
>    beschriebenen Nachrichten und Datenmodellen.
>
>-   Derzeit ist ein Fachdatenkonzept in Arbeit, welches möglicherweise
>    sowohl XNachweis als auch die RDN Grobkonzeption beeinflussen wird.
>
>-   Einige Aspekte der Grobkonzeption werden voraussichtlich durch die
>    Umsetzung, die Auswahl des Betreibers, und Fragen der Govenance
>    beeinflusst. 


## Management Summary

Die Registerdatennavigation (**RDN**) ist eine zentrale Komponente des
NOOTS. Sie liefert einem *Data Consumer* die Information, von welchem
technischen Dienst dieser einen gewünschten *Nachweis* abrufen kann,
welche Behörde diesen Dienst betreibt, ob die IDNr (bzw.
beWiNr) grundsätzlich im zuständigen Register eingespeichert ist, welche
Formate und Schemata von einem technischen Dienst angeboten werden und
welche Verbindungsparameter für einen Abruf erforderlich sind. Damit
nimmt sie eine zentrale Rolle im Prozess des Nachweisabrufs ein.
Die **RDN** wird sowohl für nationale Abrufe als auch für Abrufe aus dem
EU-Ausland verwendet.

Dieses Kapitel dient als Grobkonzept für die Umsetzung der **RDN**. Es
beschreibt mögliche konkrete Lösungsansätze für das Vorhaben der
Registermodernisierung. Im weiteren Verlauf des Vorhabens müssen
einerseits noch einige offene Punkte aufgelöst, andererseits in diesem
Konzept skizzierte Lösungsansätze verifiziert und ausgearbeitet werden.
Auf Basis erläuterter Annahmen beschreibt das vorliegende Dokument die
Lösungsidee mit der Dokumentation von fachlichen Anforderungen,
Schnittstellen, der sicheren Anbindung externer Systeme mittels
Authentifizierung, Protokoll-Mechanismen sowie IT-betrieblichen
Aspekten. Die identifizierten Punkte werden im weiteren Verlauf des
Umsetzungsprojekts näher beleuchtet, sodass eine Spezifikation der
Annahmen um die Ergänzung weiterer Faktoren und Erkenntnisse erfolgen
wird.

## Einleitung

### Ziel des Kapitels

Dieses Dokument formuliert Anforderungen und architektonische Vorgaben
für die Komponente Registerdatennavigation (**RDN**). Diese bilden die
Grundlage zur Beauftragung der Umsetzung der Komponente
Registerdatennavigation.

### Zielgruppe

Dieses Konzept richtet sich an die Umsetzer der Komponente
Registerdatennavigation sowie an Interessierte an den
Architekturkomponenten der Registermodernisierung.

### Aufbau

In Kapitel
[Kontext](#kontext)
wird zunächst der Kontext dargestellt, in der die **RDN** genutzt werden
soll. Kapitel [Annahmen und
Rahmenbedingungen](#annahmen-und-rahmenbedingungen)
sammelt die Annahmen und Rahmenbedingungen, die für die Konzeption
der **RDN** von Bedeutung sind. Kapitel [Fachliche
Anforderungen](#fachliche-anforderungen)
formuliert die Anforderungen an die **RDN**. Eine mögliche fachliche
Umsetzung der Anforderungen wird in Kapitel
[Facharchitektur](#facharchitektur)
skizziert und insbesondere die Schnittstellen der **RDN** vertiefend
dargestellt. Darüber hinaus benennt Kapitel [Technische
Aspekte](#technische-aspekte)
die technischen Anforderungen, soweit diese bisher bekannt sind. Das
Kapitel Ausblick & Weiterführende
[Aspekte](#ausblick--weiterführende-aspekte)
informiert über offene Punkte und weitere Aspekte, welche
Hintergrundinformationen zu diesem Dokument beschreiben.

## Kontext

### Ausgangslage

Im Zielbild der Registermodernisierung des IT-Planungsrats ist
beschrieben, dass es bei Once-Only-Datenabfragen notwendig sei, die
zuständige Behörde für ein Datum anhand eines zentralen Verzeichnisses
für Nachweistypen zu ermitteln und eine entsprechende Navigation
einzuleiten.

Es sei zu prüfen, ob ein solches Verzeichnis sowie eine mögliche
Registerdatennavigation über die Erweiterung des deutschen
Verwaltungsdienste Verzeichnis (**DVDV**), die
Verwaltungsdaten-Informationsplattform (VIP), die nach dem **IDNrG** zu
erstellende Registerlandkarte oder eine gänzlich neue Komponente
umgesetzt werden könne (IT-Planungsrat
**\[[IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05)\]**,
Beschlussdokument vom Januar 2021, S. 9).

Nach entsprechender Analyse hat der IT-PLR-Beschluss 2022/22 vom
22.06.2022
**\[[IT-PLR-E-05](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL6_Registerdatennavigation.pdf)\]**
die Umsetzung der Komponente Registerdatennavigation als zentralen
Routing-Dienst auf Grundlage des Deutschen Verwaltungsdienste
Verzeichnis unter Wiederverwendung von Lösungsansätzen aus FIT-Connect
beauftragt. Im Beschluss heißt es, das Kompetenzteam Architektur
(inzwischen PB NOOTS) der Gesamtsteuerung Registermodernisierung soll
den dafür notwendigen Projektauftrag konkretisieren. Dieser Auftrag
liegt der Erstellung des vorliegenden Konzepts zugrunde.

Im Architektur-Dokument "High-Level-Architecture"
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-03]</B></a>
wird die Verbindung der Registerdatennavigation zu anderen Komponenten
in der Abbildung "NOOTS-Zielbild 2028" dargestellt. Im gleichen Dokument
wird in den tabellarischen Prozessbeschreibungen der verschiedenen
Anwendungsfälle (siehe Kapitel \"Anwendungsfälle\" in
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-03]</B></a>)
die Einbindung der Registerdatennavigation in die Nachweisabruf-Prozesse
beschrieben.

### NOOTS-spezifische Kommunikationsinfrastruktur und Sicherer Anschlussknoten

Gemäß IT-PLR-Beschluss 2023/38 vom 3.11.2023
**\[[IT-PLR-B-10](https://www.it-planungsrat.de/beschluss/beschluss-2023-38)\]**
soll \"ein Wechsel von zunächst genutzten IT-Standards bzw. Komponenten
zu europäisch oder international genutzten Alternativen\" ermöglicht
werden. Im Zuge des NOOTS-Ausbaus werden zahlreiche Data Consumer und
Data Provider angeschlossen werden. Um diese zu entlasten, wird derzeit
im Rahmen der Konzeption der Transportinfrastruktur (siehe
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-19]</B></a>) eine Software konzipiert, welche für Data Consumer und Data Provider
(mindestens) folgende Aufgaben übernimmt: Kapselung des Transports,
abstrakte Berechtigungsprüfung unter Einbindung einer
Vermittlungsstelle, Verschlüsselung. Diese Software wird als \"Sichere
Anschlussknoten\" bezeichnet, da sie im gleichen Verantwortungsbereich
wie Data Consumer bzw. Data Provider betrieben werden soll, nur abstrakt
berechtigungsgeprüfte Nachweisabrufe ermöglicht (d.h. ungeprüfte
Nachweisabrufe verhindert) und verschlüsselte Kommunikationsverbindungen
zwischen zwei Endpunkten der NOOTS-Kommunikationsinfrastruktur aufbauen
soll. Weitere Details werden im Kapitel \"Übergreifendes Konzept
Transportinfrastruktur\"
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-19]</B></a>
beschrieben.

Die vorliegende Version der Grobkonzeption RDN geht von der Annahme
einer verpflichtenden Nutzung der Sicheren Anschlussknoten durch Data
Consumer und Data Provider aus [\[ANN-08\]](#annahmen-und-rahmenbedingungen). Im Unterschied zu der Ende
2022 veröffentlichten Fassung dieses Grobkonzepts muss die RDN für die
Data Consumer zweierlei Arten von Informationen getrennt bereitstellen:
diverse fachlich geprägte Informationen für den Aufbau der
XNachweis-Nachrichten (zuständige Behörde, erforderliches
Vertrauensniveau, vom Data Provider angebotene Formate und Schemata
etc.) und technisch geprägte Informationen für den Aufbau der sicheren,
verschlüsselten Kommunikationsverbindung (Verbindungsparameter). Gemäß
dem zuvor erwähnten IT-PLR-Beschluss 2023/38
**\[[IT-PLR-B-10](https://www.it-planungsrat.de/beschluss/beschluss-2023-38)\]**
soll die NOOTS-spezifische Kommunikationsinfrastruktur einen Wechsel der
für den Transport genutzten IT-Standards ermöglichen. Die RDN muss
deshalb grundsätzlich in der Lage sein, Verbindungsparameter für
verschiedene Transporttechnologien zu ermitteln und bereitzustellen
[\[ANN-09\]](#annahmen-und-rahmenbedingungen). 

### Ziel der Registerdatennavigation

Um einen Nachweisabruf gemäß der im Dokument "High-Level-Architecture
(HLA)"
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-03]</B></a>
beschriebenen Anwendungsfälle durchführen zu können, benötigt der *Data
Consumer* vor dem Nachweisabruf Informationen zum zuständigen *Data
Provider*. Dazu fragt der Data Consumer notwendige Informationen bei der
Registerdatennavigation an.

![C:\\9be65ae77e80f4c84ed56d93fe524adb](./assets/images7/media/image1.jpeg)
<br>**Abb. 1: Zwei Schritte beim Nachweisabruf im NOOTS (Ausschnitt aus der Once-Only Datenkette, vereinfachte Darstellung)**</br>

Die Registerdatennavigation stellt dem Data Consumer zunächst zur
Verfügung:

-   Informationen zur zuständigen Behörde (insbesondere bei dezentralen
    Registern durch die RDN zu ermitteln),
-   Information über das für den jeweiligen Nachweistyp erforderliche
    Vertrauensniveau,
-   die Information, ob die IDNr (bzw. beWiNr) grundsätzlich im
    zuständigen Register eingespeichert ist und zum Nachweisabruf
    verwendet werden kann,
-   die Information darüber, welche Formate und Schemata für den
    angefragten Nachweistyp von dem technischen Dienst der zuständigen
    Behörde angeboten werden können (siehe Code.OOTSMediaTypes und
    URI.DataModelScheme in XNachweis
    **\[[XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)\]**)
-   einen eindeutigen Identifier des technischen Dienstes der
    zuständigen Behörde.

Über den eindeutigen Identifier des zuständigen technischen Dienstes (im
folgenden Service-ID genannt) kann die RDN in einem zweiten Schritt die
für den Aufbau einer sicheren, verschlüsselten Kommunikationsverbindung
erforderlichen Verbindungsparameter ermitteln.

Dementsprechend lautet das primäre Ziel der Komponente
Registerdatennavigation die Erfüllung dieser zwei Aufgaben: Auf
Grundlage des gewünschten Nachweistyps und weiterer Informationen
(Zuständigkeitsparameter) erstens die für den Nachweisabruf benötigten
Informationen zum Data Provider und den eindeutigen Identifier des
zuständigen Dienstes zu bestimmen und bereitzustellen sowie zweitens,
dessen Verbindungsparameter für den Aufbau einer verschlüsselten
Verbindung zu ermitteln.

Die Erwartung an die Registerdatennavigation ist dabei, dass sie alle
von der deutschen Verwaltung vorgehaltenen Nachweistypen und zuständigen
Data Provider kennt, benötigte Informationen zu den Data Providern
bereithält und Auskunft über die Daten der entsprechenden Dienste aller
zentral wie dezentral organisierten *Data Provider* geben kann. Für die
Pflege der Daten sind geeignete Pflegeschnittstellen erforderlich.

### Anschluss an das EU-OOTS

Das *NOOTS* muss laut der **SDG**-VO sicherstellen, dass ein
Nachweisabruf auch grenzübergreifend mit *Evidence Requester* und
*Evidence Provider* aus dem EU-Ausland funktioniert. Auf europäischer
Ebene ergibt sich also ebenfalls der Bedarf nach einer Komponente, die
europäischen *Evidence Requester* das passende deutsche Register mit dem
benötigten Dienst und entsprechenden Verbindungsparametern nennt.

Das vorliegende Konzept empfiehlt, aufgrund der Erkenntnisse zu den
Intermediären Plattformen, eine komplementäre Nutzung von zentralem
europäischen DSD und **RDN**. Das EU-**OOTS** und der Beschluss 2022/34
vom IT-Planungsrat
**\[[IT-PLR-B-09](https://www.it-planungsrat.de/beschluss/beschluss-2022-34)\]**
sieht Intermediäre Plattformen vor. Der Einsatz Intermediärer
Plattformen hat zur Folge, dass sich europäische *Evidence Requester*
mit ihrem Nachweisabruf nicht direkt an ein deutsches Register wenden,
sondern an eine Intermediäre Plattform, die den Nachweisabruf
ins *NOOTS* weiterleiten. Entsprechend müssen europäische *Evidence
Requester* nur die Zuständigkeit und Verbindungsparameter
der *Intermediäre Plattform* erfahren.

Europäisches DSD und **RDN** sind im Zusammenspiel zu nutzen: Über das
zentrale europäische DSD (EU DSD) ermittelt der europäische *Evidence
Requester* die zuständige Intermediäre Plattform. Diese routet den
Nachweisabruf mithilfe der **RDN** an die zuständige Stelle in
Deutschland weiter (siehe Abbildung \"Zusammenspiel der Komponenten bei
Nachweisabruf aus EU-Ausland mit IP\").

Der in Anlage 5 zum **IT-PLR** Beschluss
2022/22 **\[ [IT-PLR-E-04](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL5_DSD.pdf) \]** für
diese Alternative angeführte Nachteil (Zitat: \"führt aber zu einem
dauerhaften doppelten Pflegeprozess von Registerinformationen auf
nationaler und europäischer Ebene.\") ist nicht zutreffend, da im
zentralen europäischen DSD nur die Intermediären Plattformen eingepflegt
werden müssen (was ohnehin notwendig ist).

![C:\\6ccdbebc113ebff8a83195eee271c385](./assets/images7/media/image2.jpeg)
<br>**Abb. 2: Zusammenspiel der Komponenten bei Nachweisabruf aus EU-Ausland mit IP**</br>

> Hinweis: Ausgangspunkt dieser Darstellung ist, dass der europäische
> *Evidence Requester* beim "EU Evidence Broker" bereits den passenden
> deutschen Nachweistyp ermittelt, sowie beim „DSD Registry" das für
> Deutschland zuständige EU *Data Service Directory* abgefragt hat.

Mit diesem Vorgehen werden die Präferenzen des IT-Planungsrat erfüllt,
die föderale Struktur vor den europäischen *Evidence Requestern* zu
verbergen und doppelte Pflege von Zuständigkeiten zu vermeiden, da das
EU DSD und die **RDN** jeweils unterschiedliche Datenbestände umfassen.
Gleichzeitig muss die **RDN** keine Schnittstelle zum
EU-**OOTS** bereithalten, sondern überlässt diese Aufgabe den eigens
dafür konzipierten Intermediären Plattformen.

Bei Nachweisabrufen aus Deutschland ins EU-Ausland wenden sich
deutsche *Data Consumer* direkt an die für sie zuständige
nationale *Intermediäre Plattform* [\[ANN-005\]](#annahmen-und-rahmenbedingungen). Die **RDN** wird in
diesem Fall nur für den Abruf der Verbindungsparameter durch die
sicheren Anschlussknoten verwendet.

## Annahmen und Rahmenbedingungen

Das Zielbild der Lösungsarchitektur wird von verschiedenen
übergreifenden organisatorischen, technischen, zeitlichen und fachlichen
Faktoren eingegrenzt, die sich aus den Gesetzen und den
infrastrukturellen Gegebenheiten ergeben. Diese werden im Folgenden
aufgeführt.

Tabelle AD-NOOTS-07-01: Annahmen und Rahmenbedingungen für die
Konzeption der Registerdatennavigation

  | ID         | Annahme / Rahmenbedingung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [ANN-01]   | Die Festlegung, welche Nachweistypen es gibt und welche Zuständigkeitsparameter im Falle dezentraler Zuständigkeiten benötigt werden, ist nicht Aufgabe der **RDN** (siehe [RMBED-04]).  Die **RDN** benötigt diese Informationen, ist jedoch nicht zwingend das führende System dafür. Es könnte bereits bestimmte Stellen (Verzeichnisse, Webseiten, etc.) geben, in denen für eine bestimmte Fachlichkeit die Verteilung der Zuständigkeiten gepflegt wird und nachgeschlagen werden kann. Inwieweit die Informationen solcher Quellen automatisiert in das DVZV übernommen werden können, wäre zu prüfen. (siehe [OP-06]).                              |
| [ANN-02]   | Die Anbindung an das EU-**OOTS** erfolgt flächendeckend über Intermediäre Plattformen, die Anfragen aus dem EU-**OOTS** für eine weitere Bearbeitung im NOOTS umwandeln [IT-PLR-B-09](https://www.it-planungsrat.de/beschluss/beschluss-2022-34). Für das Routing der Anfragen bedeutet dies: Europäische Evidence Requester werden über das zentrale europäische DSD zunächst an die Intermediären Plattformen in Deutschland geleitet. Die IP ermittelt dann mit Hilfe der **RDN** den zuständigen nationalen Data Provider.                                                                                                                                                                                           |
| [ANN-03]   | Das NOOTS wird über eine zentrale Komponente “IAM für Behörden” verfügen (vgl. Kapitel "Grobkonzept IAM für Behörden" <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-05]</B></a>). Darin erfolgt mittelfristig die Verwaltung von technischen Benutzerkonten und deren Rollen. Es wird angenommen, dass im IAM auch Zugriffsrechte von Teilnehmern für Zugriffe auf die RDN verwaltet werden und ein Teilnehmer den berechtigten RDN-Zugriff nachweisen kann. Inwieweit es bspw. für Administratorenrechte durch das IAM nicht abgedeckte Bedarfe der **RDN**-Zugriffsverwaltung gibt, ist im Rahmen der Umsetzungskonzeption zu klären.                                                           |
| [ANN-04]   | Die **RDN** wird in allen Anwendungsfällen der High-Level-Architecture (siehe Kapitel "Anwendungsfälle des NOOTS" in <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-03]</B></a> der Registermodernisierung genutzt werden. Für den Anwendungsfall 4 wird die RDN nur für den Abruf der Verbindungsparameter durch die sicheren Anschlussknoten genutzt, siehe auch [ANN-05] und[OP-05].                                                                                                                                                                                                                                                                                                          |
| [ANN-05]   | Bei Nachweisabrufen deutscher Data Consumer bei einem Evidence Provider im EU-Ausland wenden sich deutsche Data Consumer direkt an die für sie zuständige nationale Intermediäre Plattform (ggf. gibt es nur eine). Gegenwärtig wird davon ausgegangen, dass die **RDN** in diesem Fall nur für den Abruf der Verbindungsparameter durch die sicheren Anschlussknoten genutzt wird.                                                                                                                                                                                                                                                                     |
| [ANN-06]   | Die Zuständigkeit für einen Nachweis lässt sich eindeutig bestimmen, d.h. Zuständigkeitsparameter können so zugeschnitten werden, dass ein zuständiger Dienst eindeutig ermittelt werden kann und es zu keiner Trefferliste mit verschiedenen möglichen Diensten kommt.                                                                                                                                                                                                                                                                                                                                                                             |
| [ANN-07]   | Ein zuständiger Dienst kann einen Nachweis in einem oder mehreren Formaten anbieten, der Data Consumer muss eine Kombination aus Format und ggf. DataModellScheme auswählen (siehe OOTSMediaTypes und DataModelScheme des QueryType_NOOTS in XNachweis [[XS-01]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))                                                                                                                                                                                                                                                                                                                                                                                     |
| [ANN-08]   | Data Consumer und Data Provider sind verpflichtet, die Software für "Sichere Anschlussknoten" zu nutzen. Diese Software übernimmt für Data Consumer und Data Provider (mindestens) die folgenden Aufgaben: Kapselung des Transports, abstrakte Berechtigungsprüfung unter Einbindung einer Vermittlungsstelle, Verschlüsselung.                                                                                                                                                                                                                                                                                                                     |
| [ANN-09]   | Gemäß IT-PLR-Beschluss 2023/38 [[IT-PLR-B-10]](https://www.it-planungsrat.de/beschluss/beschluss-2023-38) soll die NOOTS-spezifische Kommunikationsinfrastruktur einen Wechsel der für den Transport genutzten IT-Standards ermöglichen. Die RDN muss deshalb grundsätzlich in der Lage sein, Verbindungsparameter für verschiedene Transporttechnologien zu ermitteln und bereitzustellen.                                                                                                                                                                                                                                                                                                             |
| [ANN-10]   | Auch die RDN wird die Software für "Sichere Anschlussknoten" nutzen.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| [ANN-11]   | Die Unterschiede zwischen den nationalen Anwendungsfällen und dem Nachweisabruf durch Evidence Requester aus dem europäischen Ausland werden in XNachweis geringgehalten. Das Grobkonzept RDN orientiert sich an den in XNachweis beschriebenen Nachrichten und Datenmodellen.                                                                                                                                                                                                                                                                                                                                                                      |
| [ANN-12]   | Für Nachweistypen mit unterschiedlichen Nachweisreifegraden (Nachweisreifegrade B-D) werden bestehende Strukturen aus XNachweis verwendet, insbesondere QueryType_NOOTS (Zitat XNachweis: "QueryType zur Verwendung im NOOTS") und die darin enthaltenen Elemente EvidenceTypeClassification (gibt den Nachweistyp an), OOTSMediaTypes (beschreibt das Format, bspw. PDF, XML, JSON) und URI.DataModelScheme (verweist für strukturierte, maschinenlesbare Datenformate (bspw. XML, JSON) auf konkrete Datenmodell-Schemata). Die API für die RDN-Abfrage durch Data Consumer unterscheidet sich daher für verschiedene Nachweisreifegrade nicht.   |
| [RMBED-01] | Die für den Abruf eines Nachweises erforderlichen Parameter werden in Syntax und Semantik auf Basis der TDDs des EU-**OOTS** durch den Once-Only Standard des NOOTS (= XNachweis) definiert. Der Standard XNachweis liegt derzeit in der Version 07 vor (siehe [[XS-01]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)). Es wird davon ausgegangen, dass es gemeinsame Wertelisten (in EU und DE) geben wird und somit beim grenzüberschreitenden Nachweisaustausch keine Umschlüsselung nötig ist.                                                                                                                                                                                                     |
| [RMBED-02] | Ungeachtet der Nutzung des **DVDV** als einer der zentralen Datenspeicher der **RDN** bleibt das **DVDV** außerhalb der Registermodernisierung weiterhin als einzeln nutzbarer Dienst mit seinen bisherigen APIs verfügbar. Bestehende Nutzende des **DVDV** müssen nicht angepasst werden.                                                                                                                                                                                                                                                                                                                                                                         |
| [RMBED-03] | Eine Nachweistypen-Liste wird gegenwärtig im Rahmen des Gesamtvorhabens Registermodernisierung erarbeitet und wird langfristig von einer zentralen Stelle verantwortet und gepflegt werden.                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| [RMBED-04] | Die Festlegung, welche Nachweistypen es gibt und welche Zuständigkeitsparameter im Falle dezentraler Zuständigkeiten benötigt werden, ist nicht Aufgabe der **RDN**.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |


## Fachliche Anforderungen

In diesem Abschnitt wird das fachliche Konzept der
Registerdatennavigation (**RDN**) beschrieben.

### Systemkontext

Die Registerdatennavigation wird als zentrale Komponente im
Architekturzielbild des NOOTS konzipiert und liefert auf Anfrage die für
einen *Nachweisabruf* notwendigen Informationen zu einer zuständigen
Behörde sowie den technischen Dienst mit seinen Verbindungsparametern,
über den für einen Nachweistyp *Nachweise* in mindestens einem Format
abgerufen werden können. Neben den Abfrageschnittstellen bietet es auch
Möglichkeiten zur Pflege und zum Import und Export von in der RDN
benötigten Daten. In der Umsetzung können auch Pflegeclients oder
Weboberflächen der **RDN** selbst zum Einsatz kommen.

![C:\\b0143f4c3cb827e9d737e33f426d05a6](./assets/images7/media/image3.jpeg)
<br>**Abb. 3: Kontextdiagramm Registerdatennavigation**</br>


### Zentrale Begriffe

Im Rahmen der konzeptionellen Vorarbeiten zur Registerdatennavigation
wurden zentrale Begriffe und Abhängigkeiten definiert und innerhalb des
Kompetenzteams Architektur abgestimmt. Es wurden in diesem Release nur
geringfügige Änderungen vorgenommen, eine Überarbeitung ist für ein
kommendes Release geplant (siehe Offene Punkte \[OP-08\])

![C:\\65f67491d5472951e5a59dbdf3c44963](./assets/images7/media/image4.jpeg)
<br>**Abb. 4: Zentrale Begriffe der Registerdatennavigation**</br>


Die in Abbildung 4 \"Zentrale
Begriffe der Registerdatennavigation\" dargestellten Begriffe werden im
Folgenden erläutert und mit mindestens einem Beispiel versehen. Die
Abbildung zeigt, wie die Begriffe zueinander in Beziehung stehen, sie
ist kein Datenmodell oder Klassendiagramm.

Anmerkung zum Nachweisreifegrad: Die Begriffe und auch die API für die
RDN-Abfragen durch Data Consumer unterscheiden sich bei verschiedenen
Nachweisreifegraden nicht [\[ANN-12\]](#annahmen-und-rahmenbedingungen). Angenommen, es gäbe einen
Nachweistyp \"Hauptwohnsitz\" (denkbares Beispiel für Nachweisreifegrad
D1), könnte in der Spalte \"Beispiel\" der folgenden Tabelle das Wort
\"Meldebescheinigung\" (vrsl. im europäischen Kontext primär mit
Reifegrad B relevant) auch durch das Wort \"Hauptwohnsitz\" ersetzt
werden.

Tabelle AD-NOOTS-07-02: Zentrale Begriffe und deren Zusammenhänge

| Begriff                 | Bedeutung                                                                                                                                                                                                                                                                                                                                                                                                                   | Beispiel                                                                     |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| Verwaltungsverfahren    | Es liefert oder benötigt Nachweise. Es kann, muss sich aber dabei nicht um eine Leistung handeln.                                                                                                                                                                                                                                                                                                                           | “Meldedatensatz zum Abruf Bereitstellung” (elektronische Meldebescheinigung) |
| Behördentyp             | <p><p style="color:#184B76;">Kategorie von Behörden, die die gleichen Leistungen erbringen und die gleichen Prozesse ausführen.</p></p> <p><p style="color:#184B76;">Je nach Fachdomäne gibt es eine oder mehrere Behörden vom selben Behördentyp.</p></p> <p style="color:#184B76;">Behörden desselben Behördentyps führen Register desselben Registertyps. Behördentypen können auch mehrere Registertypen führen.     | Meldebehörden                                                                |
| Behörde                 | Konkrete Behörde eines Behördentyps. Sie führt eine konkrete Registerinstanz und bietet einen technischen Endpunkt, über den auf diese zugegriffen werden kann.                                                                                                                                                                                                                                                             | Einwohnermeldeamt Köln                                                       |
| Technischer Dienst      | <p><p style="color:#184B76;">Unter dem technischen Dienst wird die konkrete Implementierung zur Ausstellung eines Nachweises bei einer konkreten Registerinstanz bzw. nachweisliefernden Stelle verstanden. Der technische Dienst verfügt über einen eindeutigen Identifier (Service-ID).</p></p> <p><p style="color:#184B76;">Andere technische Dienste, die Softwarekomponenten anbieten, sind im Kontext dieses Konzepts nicht gemeint.</p> <p><p style="color:#184B76;">Für den Abruf von Nachweisen von einem technischen Dienst sind Verbindungsparameter</p> erforderlich.                                                                                                                                                               | Dienstinstanz NOOTS_Meldebescheinigung für die Stadt Köln                    |
| Verbindungsparameter    | <p><p style="color:#184B76;">Die Gesamtheit aller Informationen, die benötigt werden, um einen Technischen Dienst zu nutzen. Dazu gehört mindestens die URL des Dienstes, Zertifikate, etc.</p></p> <p><p style="color:#184B76;">Da gemäß IT-PLR-Beschluss 2023/38 [**[IT-PLR-B-10]**](https://www.it-planungsrat.de/beschluss/beschluss-2023-38) die NOOTS-spezifische Kommunikationsinfrastruktur einen Wechsel der für den Transport genutzten IT-Standards ermöglichen soll, muss die RDN ggf. Verbindungsparameter für verschiedene Transporttechnologien bereitstellen können.</p>           | <p><p style="color:#184B76;">URL der Dienstinstanz NOOTS_Meldebescheinigung für die Stadt Köln</p></p> Zertifikat des Einwohnermeldeamts Köln           |
| Nachweistyp             | <p><p style="color:#184B76;">Ein fachliches Objekt, um einen Sachverhalt nachzuweisen. Dabei kann es sich um ein (elektronisches) Dokument oder eine Datenstruktur handeln.</p></p> Ein *Nachweistyp* wird von Registern desselben Registertyps ausgestellt.                                                                                                                                                                                                                                                                              | Meldebescheinigung                                                           |
| Registertyp             | <p><p style="color:#184B76;">Eine Kategorie von IT-Systemen, die Daten der Verwaltung speichern und elektronische Nachweise ausstellen können.</p></p> Registerinstanzen desselben Registertyps werden von Behörden desselben Behördentyps geführt.                                                                                                                                                                                                                                                                                                           | Melderegister                                                                |
| Registerinstanz         | Ein IT-System, welches von einer Behörde geführt wird. Registerinstanzen betreiben technische Endpunkte, über die von ihnen erstellte Nachweise abgerufen werden können.                                                                                                                                                                                                                                                    | Melderegister der Stadt Köln                                                 |
| Zuständigkeitsparameter | Je *Nachweistyp* festgelegte Parameter zur Ermittlung der für die Ausstellung des Nachweises für ein Nachweissubjekt zuständigen Stelle. Nur bei Registertypen mit verteilten Zuständigkeiten erforderlich. Vom Data Consumer werden die Werte aller Parameter spezifisch für den jeweiligen Nachweisabruf an die Registerdatennavigation übermittelt. Die RDN ermittelt die zuständige Behörde und deren technischen Dienst. | Postleitzahl oder DVDV-Schlüssel (bspw. ags…)                                |


### Anwender und Systeme

Beschreibung der Nutzenden und Systeme, die auf die
Registerdatennavigation zugreifen oder für die Aufgabenerfüllung
notwendig sind.

Tabelle AD-NOOTS-07-03: Anwender und Systeme

| Akteur                                        | Typ                                                              | Beschreibung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|-----------------------------------------------|------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Data Consumer                                 | Fremd-system                                                     | Ein **Data Consumer** ist ein im NOOTS registriertes technisches Verfahren zum Abruf von Nachweisen. Der Data Consumer greift auf die Registerdatennavigation zu, um die Zuständigkeit für einen Nachweis und die technischen Verbindungsdaten zu ermitteln. Darunter fallen eine Vielzahl unterschiedlicher öffentlicher Stellen und deren Anwendungssysteme, beispielsweise Onlineanträge, Antragsportale oder Fachverfahren.                                                                                                                                                                                                                                                                                                            |
| Data Consumer EU-Ausland / Evidence Requester | Fremd-system                                                     | <p><p style="color:#184B76;">Öffentliche Stellen der EU-Mitgliedstaaten, die NICHT auf die **RDN** zugreifen. Die entsprechenden Stellen bedienen sich der Intermediären Plattform, um über diese Nachweise bei deutschen Stellen abzurufen. Die Intermediäre Plattform ermittelt die Zuständigkeit für einen Nachweis und die technischen Verbindungsparameter.</p></p> *Hinweis: Im europäischen **OOTS** wird der Data Consumer als Evidence Requester bezeichnet.* | 
| Intermediäre Plattform                        | Fremd-system                                                     | <p><p style="color:#184B76;">Infrastrukturkomponente, die den Nachweisabruf von Evidence Requestern aus dem EU-Ausland entgegennimmt und an Data Provider weitervermittelt. Aus Sicht des NOOTS nimmt die Intermediäre Plattform die Rolle eines Data Consumers ein und nutzt daher dieselbe Schnittstelle wie der Data Consumer.</p></p> Werden in Deutschland Nachweise aus dem europäischen Ausland benötigt, wenden sich deutsche Data Consumer direkt an die für sie zuständige Intermediäre Plattform (ggf. gibt es nur eine). | 
| Pflegeverantwortliche für Nutzende/ Rechte    | Nutzende/ Pflegeclient                                           | <p><p style="color:#184B76;">Für die Verwaltung von Nutzenden und Rechten zuständige Stelle (RDN-Administration).</p></p> *Hinweis: Mit der geplanten Einführung eines zentralen IAM (siehe [ANN-003](#annahmen-und-rahmenbedingungen)) werden Teile dieser Pflegeverantwortung, soweit sie den Zugriff technischer Systeme auf die RDN betreffen, in IAM verschoben.* |
| Pflegeverantwortliche Nachweistypen           | Öffentliche Stelle, ggfs. unterstützt durch ein Anwendungssystem | Öffentliche Stelle, die verwaltet, welche Nachweistypen es gibt und welche Zuständigkeitsparameter je Nachweistyp erforderlich sind.  |
| Pflegeverantwortliche Data Provider          | Öffentliche Stelle, ggfs. unterstützt durch ein Anwendungssystem | Öffentliche Stelle, die verwaltet, welche Data Provider für welche Nachweise zuständig sind, welches Vertrauensniveau für einen Nachweistyp erforderlich ist, ob die IDNr. (bzw. beWiNr) verwendet werden kann, über welche Service-ID der technische Dienst eindeutig identifiziert werden kann und in welchen Formaten mit welchen Schemata der Nachweis vom technischen Dienst angeboten wird. Des weiteren werden für jeden technischen Dienst die Verbindungsparameter für den Aufbau einer sicheren, verschlüsselten Kommunikationsverbindung verwaltet bzw. die notwendigen Informationen mit denen diese Verbindungsparameter ggf. bei Transporttechnologie spezifischen Verzeichnissen durch die RDN abgerufen werden können.</p> </p>Von den Data Providern bezogene Informationen werden durch diese Pflegeverantwortlichen qualitätsgesichert. |
| Bestehendes Zuständigkeitsverzeichnis         | Ein oder mehrere Fremdsysteme                                    | Bestehendes Zuständigkeitsverzeichnis, aus dem Zuständigkeitsinformationen ganz oder zum Teil in die **RDN** übernommen werden können. Zuständigkeitsinformationen sind Werte oder Wertebereiche für die Zuständigkeitsparameter je zuständiger Behörde, über deren Gesamtheit eine eindeutige Ermittlung der zuständigen Behörde und des technischen Dienstes möglich ist. |
| Open Data Nutzer                              | Zu klären                                                        | Noch zu bestimmende, öffentlich zugängliche Plattform, in der die Zuständigkeitsinformationen der **RDN** publiziert werden, sofern diese keiner Sichtbarkeitseinschränkung unterliegen. |


### Anwendungsfälle der Registerdatennavigation

Beschreibung von Anwendungsfällen, für deren Erfüllung die
Registerdatennavigation benötigt wird.

#### Verbindungsparameter für Nachweis abrufen

Dieser Anwendungsfall beschreibt die Nutzung der Registerdatennavigation
zur Ermittlung eines zuständigen Data Providers und seiner
Verbindungsparameter.

Tabelle AD-NOOTS-07-04: Anwendungsfall 1: Verbindungsparameter für
Nachweis abrufen

| Anwendungsfall ID                  | Anwendungsfall 1 [UC_1]                                                                                                                                                                                                                                                                                                                                                                |
|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Verbindlichkeit                    | Muss                                                                                                                                                                                                                                                                                                                                                                                   |
| Akteure                            | Data Consumer                                                                                                                                                                                                                                                                                                                                                                          |
| Vorbedingung/ auslösendes Ereignis | <p><p style="color:#184B76;">Ein Data Consumer benötigt einen Nachweis.</p></p> Der gesuchte Nachweistyp sowie die zur Ermittlung der Zuständigkeit notwendigen Zuständigkeitsparameter sind bereits bekannt. | 
| Nachbedingung/ Ergebnisse          | <p><p style="color:#184B76;">Der Data Consumer erhält</p></p> <ul><li><p style="color:#184B76;">Behördenbeschreibung (für XNachweis / Vermittlungsstelle benötigte Informationen zum Data Provider)</p> <li><p style="color:#184B76;">Information über das erforderliche Vertrauensniveau, <li><p style="color:#184B76;">den Identifier (Service-ID) des entsprechenden technischen Dienstes,</p></p> <li><p style="color:#184B76;">die Information zur Verwendbarkeit der IDNr (bzw. beWiNr), <li><p style="color:#184B76;">sowie die von diesem Dienst angebotenen Formate und Schemata.</p></p></ul></p> <p><p style="color:#184B76;">Zur Service-ID erhält der Data Consumer die Verbindungsparameter des technischen Dienstes. Die Verbindungsparameter umfassen alle Informationen, die für den Abruf eines Nachweises über den nationalen Once-Only Standard erforderlich sind. Das sind bspw. die URL des Endpunkts sowie Informationen für eine Ende-zu-Ende-Verschlüsselung zwischen Data Consumer und Data Provider.</p> |
| Standardablauf                     | <p><p style="color:#184B76;">1. Der Data Consumer sendet eine Anfrage an die Registerdatennavigation und übermittelt den gesuchten Nachweistyp und notwendige Zuständigkeitsparameter.</p></p> <p><p style="color:#184B76;">2. Die **RDN** prüft, ob der Aufrufer für den Zugriff autorisiert ist.</p> <p><p style="color:#184B76;">3. Die Registerdatennavigation ermittelt die zuständige Behörde, deren Behördenbeschreibung, Information über das erforderliche Vertrauensniveau, den Identifier (Service-ID) des entsprechenden technischen Dienstes, die Information zur Verwendbarkeit der IDNr (bzw. beWiNr), sowie die von diesem Dienst angebotenen Formate und Schemata.</p> <p><p style="color:#184B76;">4. Der Data Consumer sendet eine Anfrage an die Registerdatennavigation mit der Service-ID des zuständigen technischen Dienstes.</p> <p><p style="color:#184B76;">5. Die Registerdatennavigation ermittelt die technischen Verbindungsparameter für den Aufbau einer sicheren, verschlüsselten Kommunikationsverbindung.</p> | 
| Alternativer Ablauf                | <p><p style="color:#184B76;">In Schritt 1 des Standardablaufs wurden nicht alle Informationen übermittelt, die für die Ermittlung der zuständigen Behörde benötigt werden.</p></p> <p><p style="color:#184B76;">2a. Die **RDN** wirft eine Exception. Darin übermittelt sie, welche Zuständigkeitsparameter für die Ermittlung der zuständigen Behörde erforderlich sind.</p> <p><p style="color:#184B76;">3a. Der Data Consumer erhebt die fehlenden Zuständigkeitsparameter.</p> <p><p style="color:#184B76;">4a. Der Data Consumer sendet erneut die Anfrage aus Schritt 1, ergänzt um die zusätzlich erhobenen Zuständigkeitsparameter aus Schritt 3a.</p> <p><p style="color:#184B76;">5a. Weiter mit dem Standardablauf Schritt 2.</p> | 
| Nutzungshäufigkeit                 | Für jeden Nachweisabruf aus der Leistungsverwaltung im nationalen Kontext und perspektivisch auch für Nachweisabrufe aus der Eingriffsverwaltung notwendig.                                                                                                                                                                                                                            |


#### Verbindungsparameter für Nachweis abrufen mit bekannter Behörde

Dieser Anwendungsfall beschreibt die Nutzung der Registerdatennavigation
für den Abruf von Informationen zu einem technischen Dienst sowie dessen
technische Verbindungsparameter, über die der
gesuchte *Nachweis* abgerufen werden kann. Dieser Anwendungsfall ist
bspw. für Fälle in der Eingriffsverwaltung vorgesehen, bei denen die
zuständige Behörde bekannt ist, oder über einen anderen Mechanismus als
die **RDN** ermittelt wird.

Tabelle AD-NOOTS-07-05: Anwendungsfall 2: Verbindungsparameter für
bekannte Behörde abrufen

| Anwendungsfall ID                  | Anwendungsfall 2 [UC_2]                                                                                                                                                                                                                                                                                                                                                                |
|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Anmerkung                          | Dieser Anwendungsfall ist im nationalen Kontext relevant. Er dient dazu, die Umstellung von Verfahren der Eingriffsverwaltung auf das NOOTS zu unterstützen, die bisher direkt das **DVDV** nutzen.                                                                                                                                                                                        |
| Verbindlichkeit                    | Soll                                                                                                                                                                                                                                                                                                                                                                                   |
| Akteure                            | Data Consumer (Eingriffsverwaltung)                                                                                                                                                                                                                                                                                                                                                    |
| Vorbedingung/ auslösendes Ereignis | <p><p style="color:#184B76;">Ein Data Consumer benötigt einen Nachweis.</p></p> Die für diesen Nachweis zuständige Behörde ist bereits bekannt und soll oder kann nicht erneut ermittelt werden. Der gesuchte Nachweistyp ist bekannt. |
| Nachbedingung/ Ergebnisse          | Der Data Consumer erhält <ul><li><p style="color:#184B76;">Behördenbeschreibung (für XNachweis / Vermittlungsstelle benötigte Informationen zum Data Provider) <li><p style="color:#184B76;">Information über das erforderliche Vertrauensniveau,</p> <li><p style="color:#184B76;">den Identifier (Service-ID) des entsprechenden technischen Dienstes,</p> <li><p style="color:#184B76;">die Information zur Verwendbarkeit der IDNr (bzw. beWiNr),</p> <li><p style="color:#184B76;">sowie die von diesem Dienst angebotenen Formate und Schemata.</p></p></ul></p> <p><p style="color:#184B76;">Zur Service-ID erhält der Data Consumer die Verbindungsparameter des technischen Dienstes. Die Verbindungsparameter umfassen alle Informationen, die für den Abruf eines Nachweises über den nationalen Once-Only Standard erforderlich sind. Das sind bspw. die URL des Endpunkts sowie Informationen für eine Ende-zu-Ende-Verschlüsselung zwischen Data Consumer und Data Provider.</p> | 
| Standardablauf                     | <ol><li><p style="color:#184B76;">Der Data Consumer sendet eine Anfrage an die Registerdatennavigation und übermittelt den gesuchten Nachweistyp und die **DVDV**-Kennung der zuständigen Behörde.</p> <li><p style="color:#184B76;">Die **RDN** prüft, ob der Aufrufer für den Zugriff autorisiert ist.</p> <li><p style="color:#184B76;">Die Registerdatennavigation ermittelt zur DVDV-Kennung der zuständigen Behörde: die Behördenbeschreibung, Information über das erforderliche Vertrauensniveau, den Identifier (Service-ID) des entsprechenden technischen Dienstes, die Information zur Verwendbarkeit der IDNr (bzw. beWiNr), sowie sowie die von diesem Dienst angebotenen Formate und Schemata.</p> <li><p style="color:#184B76;">Der Data Consumer sendet eine Anfrage an die Registerdatennavigation mit der Service-ID des zuständigen technischen Dienstes.</p><li><p style="color:#184B76;">Die Registerdatennavigation ermittelt für den Sicheren Anschlusskoten die technischen Verbindungsparameter (inkl. Verschlüsselungsparameter)</p></ol> | 
| Alternativer Ablauf                | Keiner                                                                                                                                                                                                                                                                                                                                                                                 |
| Nutzungshäufigkeit                 | Mittelfristig für Nachweiseabrufe aus der Eingriffsverwaltung notwendig.                                                                                                                                                                                                                                                                                                               |



#### Nutzende anlegen und Rechte erteilen

Dieser Anwendungsfall beschreibt die Einrichtung von Benutzerkonten und
Berechtigungen für *Data Consumer* oder Pflegeverantwortliche zur
Nutzung der Registerdatennavigation.

Tabelle AD-NOOTS-07-06: Anwendungsfall 3: Nutzende anlegen und Rechte
verteilen

| Anwendungsfall ID                  | Anwendungsfall 3 [UC_3]                                                                                                                                                                                                                             |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Anmerkung                          | Für alle von der **RDN** angebotenen Dienste muss das aufrufende System authentifiziert und autorisiert werden. Wenn es IAM für Behörden gibt, geht es hier nicht mehr um aufrufende Systeme, sondern nur noch um die Rechte für Pflegeverantwortliche. |
| Verbindlichkeit                    | Muss                                                                                                                                                                                                                                                |
| Akteure                            | Pflegeverantwortliche für Nutzende/Rechte                                                                                                                                                                                                           |
| Vorbedingung/ auslösendes Ereignis | Zugriffsrechte für ein System bzw. Pflegeverantwortliche, welche(s) die **RDN** nutzen möchte, müssen eingerichtet oder geändert werden.                                                                                                                |
| Nachbedingung/ Ergebnisse          | Zugriffsrechte wurden eingerichtet oder geändert.                                                                                                                                                                                                   |
| Standardablauf                     | <p><p style="color:#184B76;">1. Die Pflegeverantwortlichen für Nutzende/Rechte übermitteln eine Neuanlage oder Änderung an einem Benutzerkonto.</p></p> <p><p style="color:#184B76;">2. Die Pflegeverantwortlichen für Nutzende/Rechte übermitteln Änderungen an den Rechten, die einem Benutzerkonto zugeordnet sind.</p></p> <p><p style="color:#184B76;">3. Die **RDN** prüft, ob die Pflegeverantwortlichen für die Änderung autorisiert sind.</p></p><p style="color:#184B76;">4. Die **RDN** speichert die übermittelten Daten. |
| Alternativer Ablauf                | Keiner                                                                                                                                                                                                                                              |
| Nutzungshäufigkeit                 | Selten                                                                                                                                                                                                                                              |


#### Nachweistypen und benötigte Zuständigkeitsparameter pflegen

Dieser Anwendungsfall beschreibt die Pflege von Nachweistypen und
Zuständigkeitsparametern für die Registerdatennavigation.

Tabelle AD_NOOTS-07-07: Anwendungsfall 4: Nachweistypen und benötigte
Zuständigkeitsparameter pflegen

| Anwendungsfall ID                  | Anwendungsfall 4 [UC_4]                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Anmerkung                          | Für die Ermittlung der Zuständigkeit innerhalb eines Nachweistyps wird ein fester Satz an Zuständigkeitsparametern benötigt, wenn für einen Nachweistyp die Zuständigkeit aufgeteilt ist. Bei Nachweistypen mit ungeteilter Zuständigkeit (nur eine zuständige Stelle) gibt es keine Zuständigkeitsparameter. Über diesen Anwendungsfall wird gepflegt, welche Nachweistypen in der **RDN** bekannt sind und welche Zuständigkeitsparameter je *Nachweistyp* benötigt werden. |
| Verbindlichkeit                    | Muss                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| Akteure                            | Pflegeverantwortliche für Nachweistypen                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| Vorbedingung/ auslösendes Ereignis | Die von der **RDN** unterstützten Nachweistypen sollen ergänzt oder geändert werden und/oder die für die Auflösung der Zuständigkeit erforderlichen Zuständigkeitsparameter eines Nachweistyps sollen ergänzt oder geändert werden.                                                                                                                                                                                                                                         |
| Nachbedingung/ Ergebnisse          | Die Nachweistypen und Zuständigkeitsparameter werden aktualisiert. Alle Zuständigkeiten im Datenbestand der **RDN** sind konsistent mit den Nachweistypen und Zuständigkeitsparametern.                                                                                                                                                                                                                                                                                     |
| Standardablauf                     | <p><p style="color:#184B76;">1. Die Pflegeverantwortlichen übermitteln Änderungen an den Nachweistypen und/oder an den Zuständigkeitsparametern, die für einen Nachweistyp benötigt werden. Zusätzlich übermitteln die Pflegeverantwortlichen ein Datum (Stichtag), zu dem die Änderungen wirksam werden sollen.</p></p> <p><p style="color:#184B76;">2. Die **RDN** prüft, ob die Änderungen in Konflikt mit dem bestehenden Datenbestand stehen und löst ggfs. eine Bereinigung der Konflikte aus. Die Umsetzung der Konfliktbereinigung ist noch zu klären.</p> <p style="color:#184B76;">3. Nach Auflösung der Konflikte werden für die zu ändernden Einträge im bestehenden Datenbestand ein Gültigkeitsende (Stichtag - 1) ergänzt und neue Einträge mit den Änderungen und dem Gültigkeitsbeginn (Stichtag) in den Datenbestand übernommen.</p> |
| Alternativer Ablauf                | Keiner                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| Nutzungshäufigkeit                 | Selten                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |


#### Zuständigkeiten, Nachweistyp- bzw. dienstspezifische Informationen sowie Verbindungsparameter pflegen

Dieser Anwendungsfall beschreibt die Pflege von Zuständigkeiten,
Nachweistyp spezifischen Informationen (erforderliches
Vertrauensniveau), dienstspezifischen Informationen (Service-ID,
Verwendbarkeit der IDNr. (bzw. beWiNr), Formate und Schemata) sowie
Verbindungsparametern für die Registerdatennavigation. Siehe auch
Anwendungsfall 6 als Alternative.

Tabelle AD-NOOTS-07-08: Anwendungsfall 5: Zuständigkeiten, Nachweistyp-
bzw. dienstspezifische Informationen sowie Verbindungsparameter pflegen

| Anwendungsfall ID                  | Anwendungsfall 5 [UC_5]                                                                                                                                                                                                                                                                                                                        |
|------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Anmerkung                          | Dieser Anwendungsfall soll die Pflege von Zuständigkeiten, Nachweistyp- bzw. dienstspezifischen Informationen sowie Verbindungsparametern ermöglichen. Im Rahmen der Ausarbeitung der Pflegeprozesse ist zu klären, wer die Pflege verantwortet und welche IT-Unterstützung (Pflegeclient, Self-Service-Portal, etc.) dabei zum Einsatz kommt. |
| Verbindlichkeit                    | Muss                                                                                                                                                                                                                                                                                                                                           |
| Akteure                            | Pflegebeauftragter des Data Provider                                                                                                                                                                                                                                                                                                           |
| Vorbedingung/ auslösendes Ereignis | <p><p style="color:#184B76;">Ein Data Provider ist bereit, einen elektronischen Nachweistyp im Rahmen seiner Zuständigkeit zu liefern und möchte, dass diese über das **NOOTS** abgerufen werden können.</p></p> <p><p style="color:#184B76;">Nachweistyp und Zuständigkeitsparameter sind in der **RDN** bereits bekannt.</p></p> |
| Nachbedingung/ Ergebnisse          | <p><p style="color:#184B76;">Die Daten der **RDN** sind aktualisiert.</p></p> Aufrufe des UC_1 liefern von nun an die von den Data Providern übermittelten Informationen sowie die Verbindungsparameter, wenn der entsprechende Nachweistyp und die entsprechenden Zuständigkeitsparameter übergeben werden. |
| Standardablauf                     | <ol><li><p style="color:#184B76;">Der Pflegebeauftragte des Data Provider übermitteln an die **RDN**,</p> <ul><li><p style="color:#184B76;">für welchen Nachweistyp, und welche Ausprägungen der Zuständigkeitsparameter ein Data Provider zuständig ist,</p> <li><p style="color:#184B76;">Information über das erforderliche Vertrauensniveau,</p> <li><p style="color:#184B76;">die Behördeninformation (oder einen eindeutigen Identifier für den Abruf der entsprechenden Informationen in einem geeigneten Verzeichnis, z.B. DVDV),</p> <li><p style="color:#184B76;">den Identifier (Service-ID) des entsprechenden technischen Dienstes,</p> <li><p style="color:#184B76;">die Information zur Verwendbarkeit der IDNr (bzw. beWiNr),</p> <li><p style="color:#184B76;">die von diesem Dienst angebotenen Formate und Schemata.</p></p> <p><li><p style="color:#184B76;">Er übermittelt des Weiteren die Verbindungsparameter des technischen Dienstes (oder einen eindeutigen Identifier für den Abruf der entsprechenden Informationen in einem geeigneten Verzeichnis, z.B. DVDV).</p></p> <p><li><p style="color:#184B76;">Zusätzlich übermitteln die Pflegeverantwortlichen ein Datum (Stichtag), zu dem die Änderungen wirksam werden sollen.</p></ul> <li><p style="color:#184B76;">Die **RDN** prüft, ob der Pflegebeauftragter des Data Provider für die Pflege der Zuständigkeit berechtigt ist.</p> <li><p style="color:#184B76;">Die **RDN** prüft, ob die übermittelten Zuständigkeitsinformationen konform zu den für den Nachweistyp festgelegten Zuständigkeitsparametern sind. Ist dies nicht der Fall, wird die Änderung abgelehnt.</p> <li><p style="color:#184B76;">Die **RDN** ergänzt für die zu ändernden Einträge im bestehenden Datenbestand ein Gültigkeitsende (Stichtag - 1) und erzeugt neue Einträge mit den Änderungen und dem Gültigkeitsbeginn (Stichtag) im Datenbestand.</p></ol> |  
| Alternativer Ablauf                | Keiner                                                                                                  |
| Nutzungshäufigkeit                 | Selten                                                                                                                                                                                                                                                                                                                                         |


#### Bestehende Zuständigkeiten, Verbindungsparameter etc. importieren

Dieser Anwendungsfall beschreibt die Möglichkeit eines Imports von
Zuständigkeiten, Verbindungsparametern und weiteren benötigten
Informationen (siehe Anwendungsfall 5) aus bereits existierenden
Datenquellen.

Tabelle AD-NOOTS-07-09: Anwendungsfall 6: Bestehende Zuständigkeiten,
Verbindungsparameter etc. importieren

| Anwendungsfall ID                  | Anwendungsfall 6 [UC_6]                                                                                                                                                                                                                                       |
|------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Anmerkung                          | <p><p style="color:#184B76;">Es ist fachlich zu klären, für welche Fachdomänen geeignete Quellen für einen Import von Zuständigkeiten und/oder weiteren benötigten Informationen (siehe Anwendungsfall 5) sowie Verbindungsparametern existieren. Bekannte Kandidaten sind:</p></p> <ul><li><p style="color:#184B76;">**PVOG**</p> <li><p style="color:#184B76;">**DVDV**-Schlüsselkonzepte</p></ul></p> <p><p style="color:#184B76;">Ziel ist es, eine Mehrfachpflege von Zuständigkeiten und Verbindungsparametern zu vermeiden.</p>                 |
| Verbindlichkeit                    | Kann                                                                                                                                                                                                                                                          |
| Akteure                            | <p><p style="color:#184B76;">Je nach Auslegung des Importmechanismus</p></p> <ul><li><p style="color:#184B76;">Bestehendes Zuständigkeitsverzeichnis</p> <li><p style="color:#184B76;">**RDN**</p></ul> |
| Vorbedingung/ auslösendes Ereignis | Ein Import wird manuell oder zeitgesteuert ausgelöst.                                                                                                                                                                                                         |
| Nachbedingung/ Ergebnisse          | Die **RDN** kann zuständige Behörden anhand der importieren Zuständigkeiten ermitteln und erforderliche Informationen für den Nachweisabruf liefern.                                                                                                              |
| Standardablauf                     | <ol><li><p style="color:#184B76;">Das bestehende Zuständigkeitsverzeichnis übermittelt Zuständigkeiten und/oder weitere benötigte Informationen und/oder Verbindungsparameter.</p> <li><p style="color:#184B76;">Die **RDN** prüft, ob die übermittelten Zuständigkeitsinformationen konform zu den für den Nachweistyp festgelegten Zuständigkeitsparametern sind. Datensätzen, bei denen diese nicht konform sind, werden ausgesteuert und einer manuellen Prüfung zugeführt.</p><li><p style="color:#184B76;">Die **RDN** integriert die übermittelten Informationen in den eigenen Datenbestand. Gegebenenfalls müssen dabei weitere Informationen, wie Gültigkeitsdatum, Fachdomäne etc. angegeben werden, damit die Informationen korrekt zugeordnet werden können.</p></ol> | 
| Alternativer Ablauf                | Keiner                                                                                                                                                                                                                                                        |
| Nutzungshäufigkeit                 | Offen                                                                                                                                                                                                                                                         |


### Liste der funktionalen Anforderungen

Auflistung der fachlichen Anforderungen an die Registerdatennavigation.
Die Anforderungen sind wie folgt priorisiert:

-   Hoch: Anforderung wird für die initiale Inbetriebnahme
    des *NOOTS* benötigt
-   Mittel: Anforderung wird zu einem späteren Zeitpunkt benötigt

Tabelle AD-NOOTS-07-10: Registerdatennavigation - Funktionale
Anforderungen

| ID                | Anforderung                                                                                                                                                                                                                                                                                                                                                                                                                 | Erläuterung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Priorität |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| **[AFO-API-NAT-01]**  | Die **RDN** muss eine Funktion anbieten, die anhand des übergebenen Nachweistyps und weiterer Zuständigkeitsparameter die Beschreibung der zuständigen Behörde, Information über das erforderliche Vertrauensniveau, den Identifier (Service-ID) des entsprechenden technischen Dienstes, die Information zur Verwendbarkeit der IDNr (bzw. beWiNr), sowie die von diesem Dienst angebotenen Formate und Schemata ermittelt.    | <p><p style="color:#184B76;">Im Rahmen der Umsetzungskonzeption ist ein geeigneter Identifikator für Nachweistypen festzulegen und zu klären, wie dessen Pflege erfolgt. Als mögliche Quelle kann die Registerlandkarte des **BVA** in Betracht gezogen werden.</p></p> Welche Zuständigkeitsparameter für welchen Nachweistyp erforderlich sind, ist im Rahmen der Umsetzungskonzeption zu klären.  | hoch      |
| **[AFO-API-NAT-01a]** | Die in **[AFO-API-NAT-01]** angewendete Zuständigkeitslogik muss eine regionale Zuständigkeit auf Grundlage des Amtlichen Gemeindeschlüssel (AGS) oder eines vergleichbaren nationalen Zuständigkeitsschlüssels unterstützen.                                                                                                                                                                                                   | Ein vergleichbarer nationaler Zuständigkeitsschlüssel ist der Allgemeine Regionalschlüssel (ARS).                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | hoch      |
| **[AFO-API-NAT-01b]** | Die in **[AFO-API-NAT-01]** angewendete Zuständigkeitslogik soll die Ermittlung der regionalen Zuständigkeit anhand der postalischen Adresse unterstützen.                                                                                                                                                                                                                                                                      |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | mittel    |
| **[AFO-API-NAT-01c]** | Für die in **[AFO-API-NAT-01]** ermittelte Behörde soll die RDN eine Beschreibung der Behörde ermitteln.                                                                                                                                                                                                                                                                                                                        | Welche Informationen in der Behördenbeschreibung konkret darzustellen sind, ist im Rahmen der Umsetzungskonzeption zu klären. Die Behördenbeschreibung sollte alle aus fachlicher Sicht für den Nachweisabruf und die abstrakte Berechtigungsprüfung benötigten Informationen zum Data Provider enthalten.                                                                                                                                                                                                                                                              | hoch      |
| **[AFO-API-NAT-01d]** | Für die Kombination aus Nachweistyp und die in **[AFO-API-NAT-01]** ermittelte Behörde soll die RDN eine Information zur Verwendbarkeit der IDNr (bzw. beWiNr) ermitteln.                                                                                                                                                                                                                                                       | Nur wenn die IDNr (bzw. beWiNr) im Register der Behörde eingespeichert wurde, kann die IDNr (bzw. beWiNr) zum Nachweisabruf verwendet werden. Basierend auf dieser Information kann der Data Consumer bspw. für natürliche Personen entscheiden, ob er für den Nachweisabruf die IDNr. per Identitätsdatenabruf ermittelt, oder die Personendaten (Name, etc.) verwendet.                                                                                                                                                                                               | hoch      |
| **[AFO-API-NAT-01e]** | Die **RDN** muss das für den Abruf eines Nachweises erforderliche Vertrauensniveau liefern.                                                                                                                                                                                                                                                                                                                                     | Vergleiche dazu die Technische Richtlinie TR-03107-1 “Elektronische Identitäten und Vertrauensdienste im E-Government - Teil 1: Vertrauensniveaus und Mechanismen”.                                                                                                                                                                                                                                                                                                                                                                                                     | hoch      |
| **[AFO-API-NAT-01f]** | Die **RDN** muss den Identifier (Service-ID) des zuständigen technischen Dienstes liefern.                                                                                                                                                                                                                                                                                                                                      | Über die eindeutige Service-ID wird für das NOOTS eine von der verwendeten Transporttechnologie unabhängige Identifikation des technischen Dienstes ermöglicht.                                                                                                                                                                                                                                                                                                                                                                                                         | hoch      |
| **[AFO-API-NAT-01g]** | Die **RDN** muss zum zuständigen Dienst die von diesem Dienst angebotenen Formate, und Schemata liefern.                                                                                                                                                                                                                                                                                                                        | Insbesondere bei dezentralen Registerstrukturen sind Szenarien denkbar, in denen nicht alle Data Provider alle für einen Nachweistyp definierten Formate und/oder Schemata unterstützen. Wo und in welcher Form die verfügbaren Nachweistypen, die für den jeweiligen Nachweistyp zulässigen Formate und Schemata (und ggfs. deren Versionen) verwaltet und publiziert werden, ist im Rahmen der Umsetzungskonzeption zu klären.                                                                                                                                        | hoch      |
| **[AFO-API-NAT-02]**  | Die **RDN** soll einen Dienst anbieten, der anhand des übergebenen Nachweistyps und der für den Nachweis zuständige Behörde alle Informationen ermittelt, die für den Abruf eines Nachweises erforderlich sind (die Anforderungen **[AFO-API-NAT-01c]** bis **[AFO-API-NAT-01g]** gelten hier analog)                                                                                                                                   | <p><p style="color:#184B76;">Dieser Dienst erwartet, dass die zuständige Behörde bereits bekannt ist. Er dient dazu, Verfahren den Umstieg auf das NOOTS zu erleichtern, die bisher direkt das **DVDV** verwenden und die Zuständigkeit nicht über die **RDN** neu ermitteln können. Das ist dann der Fall, wenn die Zuständigkeit selbst im Datenbestand des Data Consumer vorliegt (Beispiel: Meldewesen), die für **[AFO-API-NAT-01]** erforderlichen Zuständigkeitsparameter jedoch nicht vorliegen und auch nicht einfach neu von den Nutzenden erfragt werden können (behördeninitiierte Anwendungsfälle).</p></p> Welche ID für die Behörde übergeben werden muss, ist im Rahmen der Umsetzungskonzeption zu klären. Um eine einfache Umstellung vom **DVDV** zu ermöglichen, bietet es sich an, dafür die **DVDV**-Schlüssel zu verwenden. | mittel    |
| **[AFO-API-NAT-03]**  | Die **RDN** muss eine Funktion anbieten, die anhand der Service-ID die Verbindungsparameter des technischen Dienstes ermittelt. Die Verbindungsparameter umfassen alle Informationen, die für den Aufbau einer sicheren, verschlüsselten Kommunikationsverbindung erforderlich sind. Das sind bspw. die URL des Endpunkts sowie Informationen für eine Ende-zu-Ende-Verschlüsselung zwischen Data Consumer und Data Provider.   | <p><p style="color:#184B76;">Die erforderlichen Verbindungsparameter werden durch den Nachweisabrufstandard XNachweis [[XS-01]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis) und die zu verwendende Transportinfrastruktur definiert.</p></p> Unterstützt die Transportinfrastruktur parallel mehrere Transporttechnologien (wie es für Sichere Anschlussknoten geplant ist), muss für jede Transporttechnologie ein eigenes Set an Verbindungsparametern ermittelt werden. Die Service-ID ist Transporttechnologie unabhängig und bildet die Klammer. | hoch      |
| **[AFO-API-NAT-04]**  | Falls die **RDN** die zuständige Behörde nicht zweifelsfrei ermitteln kann, muss sie fehlende Zuständigkeitsparameter in Form einer Exception melden.                                                                                                                                                                                                                                                                           | Dazu muss der gleiche Mechanismus wie in **[AFO-EU-API-01]** verwendet werden.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | mittel    |
| **[AFO-API-NAT-05]**  | Die **RDN** soll alle Daten, die nicht schützenswert sind, als Open-Data bereitstellen.                                                                                                                                                                                                                                                                                                                                         | Die **RDN** muss die Daten nicht selbst über eine Schnittstelle im Internet bereitstellen. Es genügt, wenn sie diese auf einer Open-Data Plattform bereitstellt. Im Rahmen der Umsetzungskonzeption ist zu prüfen, ob die Registerlandkarte dazu dienen kann. <p><p style="color:#184B76;">Die Daten sollen mit Mitteln des Semantic Web auswertbar sein.</p></p> Derzeit sind keine Daten erkennbar, die nicht veröffentlicht werden dürfen und deshalb schützenwert wären.   | mittel    |
| **[AFO-IMPL-01]**     | Die **RDN** muss auf dem bestehenden **DVDV** aufsetzen und dieses um eine weitere Komponente zur Abbildung der Zuständigkeit für Nachweistypen ergänzen.                                                                                                                                                                                                                                                                           | Die Routinglogik soll nicht in das bestehende Datenschema des **DVDV** übernommen werden. Eine zum bestehenden **DVDV** redundante Pflege der Behörden soll vermieden werden. Es soll geprüft werden, ob diese Komponente über die Nachnutzung bestehender Zuständigkeitsfinder realisiert werden kann.  | hoch      |
| **[AFO-IMPL-01a]**    | Die Komponente zur Abbildung der Zuständigkeit gemäß **[AFO-IMPL-01]** soll so gestaltet werden, dass sie perspektivisch nicht nur Zuständigkeiten für Nachweise, sondern auch andere Arten von Zuständigkeiten verwalten kann.                                                                                                                                                                                                 |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | mittel    |
| **[AFO-IMPL-02]**     | Die **RDN** muss in der Lage sein, um beliebige Zuständigkeitslogiken erweitert zu werden.                                                                                                                                                                                                                                                                                                                                      | Eine Erweiterbarkeit kann durch Konfigurierbarkeit (bspw. in dem abstrakten Parametern P1, P2, ..., Pn per Konfiguration je Nachweistyp bestimmte Bedeutungen, Wertebereiche, Wertelisten etc. zugewiesen werden können) oder durch Softwareentwicklung erfolgen (bspw. neue logische Verknüpfungen von Parametern für einen bestimmten Nachweistyp), sollte aber keine destruktiven Änderungen an den Schnittstellen der **RDN** erfordern (bspw. Schnittstellen-Erweiterungen nur mit optionalen Parametern oder zusätzlichen Funktionen)                                 | mittel    |
| **[AFO-PFL-01]**      | Die **RDN** muss die benötigten Funktionen für eine konsistente Pflege von Zuständigkeitsinformationen für Nachweise implementieren.                                                                                                                                                                                                                                                                                            | <p><p style="color:#184B76;">Ob dabei ein zentral überwachter Pflegeprozess, wie bisher im **DVDV**, zum Einsatz kommt oder ein eher dezentraler Self-Service Ansatz nach dem Vorbild von FIT Connect, ist im Rahmen der Umsetzung mit dem Kompetenzteam Recht & Datenschutz und dem für die Governance zuständigen Federführer zu klären.</p></p> "konsistent" meint mindestens: Einhaltung von zulässigen Wertebereichen und widerspruchsfrei. Bei sehr komplizierter Zuständigkeitslogik einzelner Nachweistypen müssen eventuell spezielle Funktionen die Überprüfung von Änderungen bei der Pflege unterstützen. Details sind in der Umsetzungskonzeption zu klären. | hoch      |
| **[AFO-PFL-01a]**     | Die in **[AFO-PFL-01]** genannten Zuständigkeitsinformationen müssen stichtagsgenau umgeschaltet werden können.                                                                                                                                                                                                                                                                                                                 | Beispielsweise durch die Zusammenlegung von Behörden können sich Änderungen in den Zuständigkeiten ergeben, die zu einem bestimmten Stichtag wirksam werden.                                                                                                                                                                                                                                                                                                                                                                                                            | hoch      |
| **[AFO-PFL-01b]**     | Falls es geeignete Quellen für Zuständigkeiten gibt und diese bestehenden Zuständigkeitsdaten automatisiert zugeordnet werden können, muss die **RDN** eine Schnittstelle anbieten, über die Daten importiert werden können.                                                                                                                                                                                                    | Aus welcher Quelle die Zuständigkeitsinformationen übernommen werden, ist im Rahmen der Ausarbeitung der Pflegeprozesse zu klären. Eine redundante manuelle Pflege soll vermieden werden.                                                                                                                                                                                                                                                                                                                                                                               | mittel    |
| **[AFO-PFL-02]**      | Die **RDN** muss die benötigten Funktionen für eine konsistente Pflege von Informationen eines Data Providers zu einem bestimmten Nachweistyp implementieren: Information über das erforderliche Vertrauensniveau.                                                                                                                                                                                                              | Wie bei [AFO-PFL-01] sind die Pflegeprozesse mit dem Kompetenzteam Recht & Datenschutz und dem für die Governance zuständigen Federführer zu klären.                                                                                                                                                                                                                                                                                                                                                                                                                    | hoch      |
| **[AFO-PFL-02a]**     | Die in **[AFO-PFL-02]** genannten Informationen müssen je Nachweistyp und Data Provider stichtagsgenau umgeschaltet werden können.                                                                                                                                                                                                                                                                                              | Beispielsweise könnte ab einem bestimmten Stichtag die Verwendbarkeit der IDNr. gegeben sein.                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | hoch      |
| **[AFO-PFL-02b]**     | Falls es geeignete Quellen für die in **[AFO-PFL-02]** genannten Informationen gibt und diese bestehenden Einträgen im Datenbestand automatisiert zugeordnet werden können, muss die **RDN** eine Schnittstelle anbieten, über die Daten importiert werden können.                                                                                                                                                                  | Aus welcher Quelle die Informationen übernommen werden, ist im Rahmen der Ausarbeitung der Pflegeprozesse zu klären. Eine redundante manuelle Pflege soll vermieden werden.                                                                                                                                                                                                                                                                                                                                                                                             | mittel    |
| **[AFO-PFL-03]**      | Die **RDN** muss die benötigten Funktionen für eine konsistente Pflege von Informationen eines Data Providers zu einem technischen Dienst implementieren: Beschreibung der zuständigen Behörde, den Identifier (Service-ID) des zuständigen technischen Dienstes), die Information zur Verwendbarkeit der IDNr (bzw. beWiNr), von diesem Dienst angebotenen Formate und Schemata, Verbindungsparameter des technischen Dienstes | Wie bei [AFO-PFL-01] sind die Pflegeprozesse mit dem Kompetenzteam Recht & Datenschutz und dem für die Governance zuständigen Federführer zu klären.                                                                                                                                                                                                                                                                                                                                                                                                                    | hoch      |
| **[AFO-PFL-03a]**     | Die in **[AFO-PFL-02]** genannten Informationen müssen je technischem Dienst stichtagsgenau umgeschaltet werden können.                                                                                                                                                                                                                                                                                                         | Beispielsweise könnte ab einem bestimmten Stichtag ein technischer Dienst andere Formate unterstützen.                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | hoch      |
| **[AFO-PFL-03b]**     | Falls es geeignete Quellen für die in **[AFO-PFL-03]** genannten Informationen gibt und diese bestehenden Einträgen im Datenbestand automatisiert zugeordnet werden können, muss die **RDN** eine Schnittstelle anbieten, über die Daten importiert werden können.                                                                                                                                                                  | Aus welcher Quelle die Informationen übernommen werden, ist im Rahmen der Ausarbeitung der Pflegeprozesse zu klären. Eine redundante manuelle Pflege soll vermieden werden.                                                                                                                                                                                                                                                                                                                                                                                             | mittel    |
| **[AFO-PFL-04]**      | Die **RDN** muss alle Änderungen im Rahmen der Pflege revisionssicher protokollieren.                                                                                                                                                                                                                                                                                                                                           |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | hoch      |


## Facharchitektur

Dieses Kapitel stellt einen **empfohlenen** Lösungsansatz zur Umsetzung
der **RDN** vor. Es ist **nicht als zwingende Vorgabe zu verstehen**,
sondern als Umsetzungsoption. Darüber hinaus dient es dem vertieften
Verständnis der **RDN**.

### Lösungsansatz Registerdatennavigation

Abbildung 5 \"Skizze der RDN-Lösungsarchitektur\" skizziert die Lösung aus Sicht der
Registermodernisierung. Die Verortung des **DVDV** und DVZV innerhalb
der **RDN** soll deutlich machen, dass diese von den Nutzenden
der **RDN** nicht direkt verwendet werden sollen. Dies impliziert jedoch
nicht, dass das **DVDV** in der **RDN** aufgeht. Gemäß \[RMBED-02\]
bleibt das **DVDV** als eigenständiges Dienste Verzeichnis erhalten.

![C:\\ed359eb92b20be83018331f6e664b58c](./assets/images7/media/image5.jpeg)
<br>**Abb. 05: Skizze der RDN-Lösungsarchitektur**</br>


Tabelle AD-NOOTS-07-11: Registerdatennavigation - Bausteine der
Lösungsarchitektur

| Komponente                                            | Aufgabe                                                                                                                                                                                                                                                                 | Anmerkung                                                                                                                                                                                                                                                                                |
|-------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Deutsches Verwaltungsdienste Verzeichnis (**DVDV**)       | Verwaltet die technischen Dienste mit ihren Verbindungsparametern.                                                                                                                                                                                                      | Das bestehende **DVDV** soll so weit wie möglich wiederverwendet werden. (IT-PLR Beschluss 2023/38 [[IT-PLR-B-10]](https://www.it-planungsrat.de/beschluss/beschluss-2023-38): "Dabei sollen bestehende Lösungsbausteine des IT-Planungsrats und vorhandene Infrastrukturelemente soweit sinnvoll nachgenutzt werden.")                                   |
| Deutsches Verwaltungszuständigkeitsverzeichnis (DVZV) | Verwaltet Behörden und ihre Zuständigkeiten für Nachweise.                                                                                                                                                                                                              | Die Bezeichnung DVZV ist ein Arbeitstitel. Er weist darauf hin, dass mit zunehmender Anzahl an Zuständigkeitsverzeichnissen (bspw. **PVOG**, Zufi) über ein zentrales Verzeichnis nach dem Vorbild des **DVDV** nachgedacht werden sollte. Das DVZV könnte diese Rolle mittelfristig übernehmen. |
| NOOTS Dienste Verzeichnis (NDV)                       | <p><p style="color:#184B76;">Verwaltet die NOOTS spezifischen Informationen zu den technischen Diensten.</p></p> Bspw. Service-ID, Information zur Verwendbarkeit der IDNr (bzw. beWiNr.), erforderliches Vertrauensniveau, die von einem Dienst angebotenen Formate und Schemata, DVDV-Schlüssel und Dienst-URI für den Abruf der Verbindungsparameter. | <p><p style="color:#184B76;">Im Gegensatz zu DVDV und DVZV ist das NDV nicht auf Wiederverwendbarkeit angelegt und nur für solche Informationen gedacht, für die es sonst kein Verzeichnis gibt und die nur in der RDN für das NOOTS vorgehalten und gepflegt werden müssen.</p></p> Es bleibt zu prüfen, ob auf das NDV verzichtet und stattdessen eine auf dem DVDV basierende Lösung sinnvoll ist (ggf. auch als eine auf der Software des DVDV basierende Lösungen - ähnlich wie bspw. bei FIT-Connect). Siehe Offen Punkte [OP-03].  |
| Technische Komponenten                                | <p><p style="color:#184B76;">Für den Betrieb des Systems wichtige technische Bausteine wie Logging Systeme, betriebliche Überwachungssysteme, Lastverteiler, etc.</p></p> Da die Systeme unabhängig von der konkreten Fachlichkeit der **RDN** sind, werden sie hier nicht weiter betrachtet.                                                                                                                                   | <p><p style="color:#184B76;">Das bestehende **DVDV** verfügt bereits über viele entsprechende Lösungen, die für eine Wiederverwendung der **RDN** infrage kommen.</p></p> Aufrufe zu technischen Komponenten werden der Übersicht halber nicht dargestellt. |
| Internes Identity and Access Management (IAM)         | Interne Benutzer- und Rollendatenverwaltung sowie Authentifizierungs- und Autorisierungsmechanismus für alle Nutzenden der **RDN**.                                                                                                                                         | <p><p style="color:#184B76;">Wird als Interimslösung benötigt, bis das zentrale IAM für Behörden zur Verfügung steht.</p></p> <p><p style="color:#184B76;">Eine Wiederverwendung des bestehenden IAM des **DVDV** sollte geprüft werden.</p></p> Aufrufe des internen IAM werden der Übersicht halber nicht dargestellt.  |  
| Abfrage Orchestrator                                  | Führt Abfragen der Zuständigkeit, der Verbindungsparameter etc. aus, indem es Daten des DVZV und des **DVDV** abfragt und miteinander verknüpft.                                                                                                                            | Diese Komponente stellt die Implementierung der Routing-**API** dar.                                                                                                                                                                                                                         |
| Datenpflege Orchestrator                              | Die Pflege von Zuständigkeitsdaten, Verbindungsparametern etc. muss konsistent zum **DVDV** und DVZV erfolgen. Diese Komponente ist für die Verteilung von Pflegezugriffen auf beide Verzeichnisdienste und die Wiederherstellung der Konsistenz in Fehlerfällen zuständig. |                                                                                                                                                                                                                                                                                          |
| Admin Orchestrator                                    | Übernimmt die Pflege von Nachweistypen und die je Nachweistyp erforderlichen Zuständigkeitsparameter                                                                                                                                                                    | Die Komponente muss insbesondere dafür sorgen, dass durch Pflege an den Nachweistypen keine Inkonsistenzen im Gesamtdatenbestand entstehen.                                                                                                                                              |
| Datenimporter                                         | Modul zum Import von Datenbeständen aus bestehenden Datenquellen. Kümmert sich um Abbildung der übermittelten Daten auf interne Datenstrukturen und Verknüpfung diese mit dem bestehenden Datenbestand.                                                                 | <p><p style="color:#184B76;">Für unterschiedliche Datenquellen werden voraussichtlich unterschiedliche Importer benötigt.</p></p> Da Datenquellen vermutlich nur Teile des Datenbestands liefern werden (bspw. kennt **PVOG** den Begriff des Nachweistyps nicht), muss eine Möglichkeit zur Anreicherung der importierten Daten geschaffen werden.   | 
| Datenexporter                                         | Modul zum Export der Datenbestände in eine noch festzulegende Open Data Plattform.                                                                                                                                                                                      |


### Entwurfsentscheidungen und Lösungsansätze 

Im Zuge der Konzeption einer Lösung wurden verschiedene Lösungsansätze
geprüft. Um eine zuverlässige Bewertung durchführen zu können, wurden
die lösungsspezifischen Vor- und Nachteile gegenübergestellt sowie
Chancen und Risiken abgewogen. Im Folgenden wird der als am meisten
tragfähig bewertete Lösungsansatz zur Umsetzung der
Registerdatennavigation hergeleitet, der sich bereits in den oben
ausgeführten fachlichen Anforderungen widerspiegelt. Zum Zwecke der
Transparenz und der Vermeidung von Doppelarbeit sind im Anhang
[Alternative Lösungsansätze
Registerdatennavigation](#alternative-lösungsansätze-registerdatennavigation)
Lösungsvarianten festgehalten, die sich nach eingehender Prüfung als
nicht ausreichend tragfähig herausgestellt haben.

#### Entwurfsentscheidungen

Der Lösungsansatz, der als vielversprechend für die Umsetzung der
Registerdatennavigation bewertet wurde, setzt auf das **DVDV** für die
Ermittlung von Verbindungsparameter, sieht jedoch den Aufbau einer neuen
Komponente **DVZV** für die Zuständigkeitsermittlung vor. Inwieweit
diese neue Komponente auf bestehende Systeme aufbaut, wird in der
Umsetzungskonzeption ausgearbeitet. Nachfolgend werden die
Entwurfsentscheidungen dargestellt:

##### A. Die RDN wird als zentraler Routing-Dienst (Routing As a Service) entwickelt.

Die heterogenen Zuständigkeitslogiken unterschiedlicher Registertypen
führen zu einer hohen Komplexität der Zuständigkeitsdaten, wenn alle
Zuständigkeitslogiken im selben Datenbestand abgebildet werden müssen.
Zudem ist diese auf Dauer schwer wartbar und schwerer
weiterzuentwickeln, wenn *Data Consumer* die Zuständigkeitsinformationen
direkt abrufen und interpretieren.

Daher wird eine einfach erweiterbare **API** vorgesehen, die die
jeweilige Zuständigkeitslogik je Registertyp verbirgt und immer nur die
für diesen Registertyp relevanten Zuständigkeitsparameter erwartet.

Dadurch wird die Komplexität der Zuständigkeitslogik vor dem *Data
Consumer* verborgen. Zudem kann das interne Datenmodell der **RDN** nach
Bedarf weiterentwickelt werden, ohne dass sich dies auf bestehenden
Nutzenden der **API** auswirkt.

##### B. Das DVDV wird genutzt und wird Teil der RDN.

Das **DVDV** ist als zentrales Dienste Verzeichnis des IT-Planungsrats
etabliert. Die Speicherung der Nachweisabrufdienste in einem anderen
Dienste Verzeichnis würde dem Anspruch des **DVDV** als zentrales
Dienste Verzeichnis widersprechen. Daher sollte das **DVDV** als
integraler Bestandteil der **RDN** betrachtet werden. Inwiefern diese
Integration technisch vollzogen wird, bleibt der Umsetzungskonzeption
überlassen.

##### C. Zur Abbildung von Zuständigkeiten wird ein neues Verzeichnis aufgebaut, das DVZV.

Bestehende Zuständigkeitsverzeichnisse oder mithilfe von
Schlüssellogiken abgebildete Zuständigkeiten im **DVDV** bieten nicht
den Funktionsumfang, der von der **RDN** gefordert wird. Die Abbildung
von Zuständigkeiten ist entweder auf einzelne Fachdomänen beschränkt
(Beispiel: Schlüsselkonzept im Meldewesen) und nicht ausreichend
flexibel für die Registermodernisierung oder auf andere Arten von
Zuständigkeiten spezialisiert (Beispiel: **PVOG** für
Antragsbearbeitung).

Für die **RDN** wird daher die Schaffung einer neuen Komponente **DVZV**
unterstellt, die explizit nicht nur für Nachweise ausgelegt werden soll,
sondern künftig unterschiedliche Zuständigkeiten abbilden können soll.
Hierdurch wird ausdrücklich nicht ausgeschlossen, dass das DVZV durch
Weiterentwicklung einer bestehenden Komponente (bspw. **PVOG**)
geschaffen werden kann. Dies ist Gegenstand der Umsetzungskonzeption
der **RDN**.

D. überarbeitete Entscheidung, siehe Lösungsansätze

##### E. Die RDN setzt bei der Pflege von Zuständigkeiten und Nachweisen auf bestehende Quellen.

Vorteile und Begründung: Um das benötigte Zuständigkeitsverzeichnis DVZV
aufzubauen, wird die Registerdatennavigation Daten aus bestehenden
Quellen importieren oder darauf aufbauen. Dies spart Aufwand bei der
Pflege von Zuständigkeitsinformationen und trägt dazu bei, dass
bestehende Zuständigkeits- und Nachweisverzeichnisse harmonisiert und
aktuell sind.

#### Lösungsansätze

##### D. Die RDN bietet dem Data Consumer drei Funktionen an.

Obwohl die Konzeption \"Sicherer Anschlussknoten\" noch nicht
abgeschlossen ist, sind auf Grund neuer Anforderungen (siehe \[ANN-08\]
und \[ANN-09\]) notwendige Änderungen der früheren Entwurfsentscheidung
\"D. Die RDN bietet dem Data Consumer zwei Funktionen\" absehbar. Auf
Basis der früheren Entwurfsentscheidung wurde zur Berücksichtigung der
neuen Anforderungen ein modifizierter Lösungsansatz entwickelt, der im
folgenden dargestellt wird:

Die bisher geplanten Funktionen API-NAT-1 (für [\[AFO-**API**-NAT-01\]](#liste-der-funktionalen-anforderungen))
und API-NAT-2 (für [\[AFO-**API**-NAT-02\]](#liste-der-funktionalen-anforderungen)) ermittelten die
Verbindungsparameter direkt. Nun sollen aber Data Consumer und Data
Provider verpflichtend Sichere Anschlussknoten verwenden, welche die
Transporttechnologie kapseln. Diesem Umstand geschuldet wird für den
zuständigen technischen Dienst zunächst eine Service-ID ermittelt,
welche von der Transporttechnologie unabhängig ist. Der Prozessschritt
zur Ermittlung der Verbindungsparameter wird aus den Funktionen
API-NAT-1 und API-NAT-2 herausgelöst und in eine eigene Funktion
API-NAT-3 gepackt. Damit wird die neue Anforderung
[\[AFO-**API**-NAT-03\]](#liste-der-funktionalen-anforderungen) abgedeckt.

Die Schnittstellen der Funktionen sind im Kapitel [Schnittstellen der
Registerdatennavigation](#schnittstellen-der-registerdatennavigation)
beschrieben.

### Fachliches Datenmodell

Aufgrund neuer Anforderungen ([\[AFO-API-NAT-01d\]](#liste-der-funktionalen-anforderungen), [\[AFO-API-NAT-01e\]](#liste-der-funktionalen-anforderungen),
[\[AFO-API-NAT-01f\], \[AFO-API-NAT-01g\], \[AFO-API-NAT-03\]](#liste-der-funktionalen-anforderungen)) und
offener Punkte (insbesondere \[OP-03\] und \[OP-08\]) wird das
Datenmodell derzeit überarbeitet, eine Veröffentlichung ist mit dem
kommenden Release geplant.

### Schnittstellen der Registerdatennavigation

Gemäß Lösungsarchitektur in [Abbildung 5](#lösungsansatz-registerdatennavigation) \"Skizze der
RDN-Lösungsarchitektur\" benötigt die **RDN** die in der Tabelle
aufgeführten Schnittstellen und Export- bzw. Importmechanismen in
Verbindung mit anderen Akteuren. Eine direkte Benutzerschnittstelle ist
zunächst nicht vorgesehen, könnte aber bei der Umsetzungskonzeption des
Pflegesystems als relevant erachtet werden. Es wird die Anbindung von
Pflegeclients über die entsprechenden Schnittstellen favorisiert.

Tabelle AD-NOOTS-07-12: Übersicht Schnittstellen der
Registerdatennavigation

| Name der Schnittstelle                        | Kommunikationspartner der RDN               | Zweck                                                           |
|-----------------------------------------------|---------------------------------------------|-----------------------------------------------------------------|
| API RDN Abfrage                               | Data Consumer                               | **RDN**-Funktionen für die Vorbereitung eines Nachweisabrufs        |
| Pflege Nachweistypen/ Zuständigkeitsparameter | Pflegeverantwortliche für Nachweistypen     | Pflege                                                          |
| Pflege Dienste und Zuständigkeiten            | Pflegeverantwortliche für Data Provider     | Pflege                                                          |
| Benutzeradministration                        | Pflegeverantwortliche für Nutzende/ Rechte  | Administration der Pflege-Nutzenden                             |
| Datenimport-mechanismus                       | Bestehende(s) Zuständigkeitsverzeichnis(se) | Pflege: Import von Zuständigkeiten und Verbindungsparameter     |
| Datenexport-mechanismus                       | Open Data Plattform                         | Veröffentlichung der Zuständigkeiten und weiterer Informationen |


#### API RDN Abfragen

Wie in Lösungsansatz D beschrieben, wird die **RDN** API drei Funktionen
vorhalten, von denen zwei vom *Data Consumer* alternativ angefragt
werden können, je nachdem, ob diesem die für den *Nachweis* zuständige
Behörde unbekannt (Funktion **API**-NAT-1) oder bekannt
(Funktion **API**-NAT-2) ist.

Tabelle AD-NOOTS-07-13: Ein- und Ausgabedaten der drei Funktionen der
RDN Abfrage API

| Funktion                   | Eingabedaten (von Data Consumer)                                                                                            | Ausgabedaten (von RDN)                                                                                 |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| Funktion **API**-NAT-1 findEvidenceService       | <ol><li><p style="color:#184B76;">Nachweistyp</p> <li><p style="color:#184B76;">Zuständigkeitsparameter: Angaben zur regionalen Zuständigkeit, z.B. PLZ oder **AGS**, ARS, und weitere Parameter nach Bedarf</p></ol>                                                                                                             | <ol><li><p style="color:#184B76;">Behördenbeschreibung (für XNachweis / Vermittlungsstelle benötigte Informationen zum Data Provider)</p> <li><p style="color:#184B76;">erforderliches Vertrauensniveau</p> <li><p style="color:#184B76;">Service-ID des technischen Dienstes (bspw. UUID)</p> <li><p style="color:#184B76;">Information zur Verwendbarkeit der ID-Nr (bzw. beWiNr.)</p> <li><p style="color:#184B76;">Liste der Formate und Schemata (OOTSMediaType, URI.DataModelScheme)</p></ol> |
| Funktion **API**-NAT-2 findEvidenceServiceByKey        | <ol><li><p style="color:#184B76;">Nachweistyp</p> <li><p style="color:#184B76;">Behörden-ID (bspw. DVDV-Schlüssel)</p></ol>                                                                                                              | <ol><li><p style="color:#184B76;">Behördenbeschreibung (für XNachweis / Vermittlungsstelle benötigte Informationen zum Data Provider)</p> <li><p style="color:#184B76;">erforderliches Vertrauensniveau</p> <li><p style="color:#184B76;">Service-ID des technischen Dienstes (bspw. UUID)</p> <li><p style="color:#184B76;">Information zur Verwendbarkeit der ID-Nr (bzw. beWiNr.)</p> <li><p style="color:#184B76;">Liste der Formate und Schemata (OOTSMediaType, URI.DataModelScheme)</p></ol>|
| Funktion API-NAT3 findEvidenceServiceAddress| <ol><li><p style="color:#184B76;">Service-ID</p></ol>    | <ol><li><p style="color:#184B76;">Liste der Transportbindings</p> <li><p style="color:#184B76;">Je Transportbinding: Verbindungsparameter des technischen Dienstes, inkl. Verschlüsselungsparameter</p></ol>  |

Aufgrund neuer Anforderungen (\[AFO-API-NAT-01d\], \[AFO-API-NAT-01e\],
\[AFO-API-NAT-01f\], \[AFO-API-NAT-01g\], \[AFO-API-NAT-03\]) und
offener Punkte (insbesondere \[OP-03\] und \[OP-08\]) werden die
Datenflüsse der Funktionen API-NAT-1, API-NAT-2 und API-NAT-3 derzeit
überarbeitet, eine Veröffentlichung ist mit dem kommenden Release
geplant.

#### Pflege Nachweistypen / Zuständigkeitsparameter

Über diese Funktion erhält der Admin Orchestrator vom Pflegesystem für
Nachweistypen mindestens Informationen über:

-   Verfügbare Nachweistypen aus der Verwaltung
    1.  für deutsche wie EU Data Consumer
    2.  für Nutzende-initiierte wie Behörden-initiierte Nachweisabrufe
-   Benötigte Zuständigkeitsparameter pro Nachweistyp

Es wird also je Nachweistyp festgelegt, welche Zuständigkeitsparameter
erforderlich sind.

#### Pflege Dienste und Zuständigkeiten

Über diese Funktion erhält der Datenpflege Orchestrator vom Pflegesystem
für nachweisliefernde Stellen (Data Provider) mindestens Informationen
über:

-   Bereitgestellte Nachweistypen
-   für jeden Nachweistyp für den bei diesem Data Provider zu nutzenden
    technischen Dienst:
    -   Service-ID des technischen Dienstes (bspw. UUID)
    -   Passende Ausprägungen der Zuständigkeitsparameter z.B. für
        welche Region ein Dienst genutzt werden kann.
    -   Information zum erforderlichen Vertrauensniveau
    -   Information über die Verwendbarkeit der IDNr. (bzw. beWiNr.)
    -   die unterstützenten Formate und Schemata
    -   den DVDV-Schlüssel
    -   die Organisationskategorie
    -   die Dienst-URI (ggf. mehrere, je Transporttechnologie eine)
    -   Verbindungsparameter für den Nachweisabruf (ggf. mehrere, je
        Transporttechnologie ein Set)

Der Datenpflege Orchestrator verteilt dann die erhaltenen Informationen
an die verschiedenen Verzeichnisse (DVZV, NDV, DVDV).

#### Benutzeradministration

Mit dieser Funktion werden die zuvor im Pflegesystem angelegten
Nutzerprofile in das Identitäts- und Zugriffsmanagement (Identity and
Access Management - IAM) der **RDN** importiert. Die Nutzerprofile
bestehen mindestens aus

-   der Bezeichnung der pflegenden Stelle (Behörde oder von ihnen
    beauftragte Stellen)
-   eindeutige Kennnummer
-   Passwörter oder Zertifikate für den Zugriff
-   gewährte Schreibrechte für die Pflege von Nachweistypen und Daten
    der Data Provider.

Weiteres ist in der Umsetzungskonzeption in Anlehnung an die
Registermodernisierungskomponente "IAM für Behörden" (Kapitel
\"Grobkonzept IAM für Behörden\" <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target="_blank"><B>[AD-NOOTS-05]</B></a>) zu klären.

#### Datenimportmechanismus

Die für die Zuständigkeitsermittlung notwendigen Informationen liegen
zum Teil bereits in anderen Systemen (z.B. Zuständigkeitsfindern) vor.
Damit keine redundante Erfassung von Zuständigkeitsinformationen
erfolgt, werden im Rahmen der Umsetzungskonzeption geeignete Quellen
identifiziert, aus denen die Registerdatennavigation
Zuständigkeitsinformationen importieren kann.

Hierfür kann sich eine XZuFi-Schnittstelle eignen, die standardisiert
den unabhängigen Austausch von Informationen zu
Verwaltungsdienstleistungen, Onlinediensten, Gebieten, Formularen und
den hierfür zuständigen Organisationseinheiten im Kontext von
Zuständigkeitsfindern, Bürger- und Unternehmensinformationssystemen und
Leistungskatalogen ermöglicht.

#### Datenexportmechanismus

Über diesen Mechanismus werden Informationen zu Zuständigkeiten,
Diensten und Verbindungsparametern, sofern sie als nicht schützenswert
eingestuft werden, auf einer noch zu definierenden Open Data Plattform
im Open-Data Format veröffentlicht. Dies kann in regelmäßigen
Intervallen oder bei Änderungen wiederholt werden.

## Technische Aspekte

### Datenschutz

Die **RDN** ermittelt und beauskunftet Zuständigkeitsinformationen sowie
technische Verbindungsparameter von Diensten. Für diese Aufgaben muss
sie prinzipiell keine Kenntnis der Person haben, für die ein
Nachweisabruf getätigt werden soll. Es gibt dennoch drei Bereiche, die
potenziell datenschutzrelevant sind:

#### Szenario 1: Zuständigkeitsermittlung lässt Rückschlüsse auf Person zu

Bei Anfragen an die **RDN** zur behördlichen Zuständigkeit könnten die
Angaben zum Nachweistyp, Adresse und ggf. weiterer Parameter der
antragstellenden Person in ihrer Kombination so selten sein, dass daraus
die Identität der antragstellenden Person abgeleitet werden kann. Dies
könnte bei der Beantragung einer seltenen Leistung, für die nur eine eng
gefasste Personengruppe anspruchsberechtigt sind, vorkommen. Dem kann
begegnet werden, in dem RDN-Abfragen transportverschlüsselt erfolgen
müssen, denn Rückschlüsse können nur anhand der Inhalte von RDN-Abfragen
gezogen werden.

#### Szenario 2: Personenbezogene Daten im Pflegesystem

Die Pflege von behördlichen Zuständigkeiten oder Nachweisen könnte von
verantwortlichen Personen vorgenommen werden, die für diese Aufgabe
eindeutig identifizierbar im Pflegesystem angemeldet sind. Deren
personenbezogenen Daten sind schützenswert gemäß DSGVO. Dem kann
begegnet werden, in dem RDN-Daten über Pflege-Clients gepflegt werden.
Die personenbezogenen Daten werden dann allerdings dort gespeichert
(d.h. Verlagerung, nicht Vermeidung des Szenarios).

#### Szenario 3: Personenbezogene Daten als Teil von Zuständigkeitsinformationen

Verzeichnisse zu behördlichen Zuständigkeiten, wie das **PVOG**,
enthalten die FIM-Leistungsbeschreibung. Obwohl diese es formell nicht
vorsieht, kann es vorkommen, dass in dieser Beschreibung Personen
namentlich und ggf. mit Kontaktdaten genannt werden. Bei Integration
oder Import von Daten aus diesen Verzeichnissen kann es entsprechend zur
Übernahme von personenbezogenen Daten in die **RDN** kommen. Die RDN
sollte ihre Datenhaltung auf relevante Informationen beschränken,
Beschreibungen sollten möglichst nicht übernommen bzw. gefiltert werden.

Diese Szenarien sollten im Rahmen der Umsetzungskonzeption bewertet und
bei Bedarf durch die Anwendung der DSGVO adressiert werden. Dies
schließt auch die personenbezogenen Daten ein, die in Protokollen
erfasst werden.

### IT-Sicherheit

Während der Konzeption wurde eine vorläufige Einschätzung zum
Schutzbedarf vorgenommen. Ausführungen zu der Begründung sind im Anhang
[Erläuterung zur vorläufigen Einschätzung zum
Schutzbedarf](#erläuterung-zur-vorläufigen-einschätzung-zum-schutzbedarf)
zu finden.

Tabelle AD-NOOTS-07-14: Vorläufige Einschätzung zum Schutzbedarf
der **RDN**

IT-Sicherheit

| Kategorie       | Vorläufige Einschätzung zu Schutzbedarf | Begründung                                                                                                                                                                                                                                                            | Resultierende nicht-funktionale Anforderung |
|-----------------|-----------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------|
| Vertraulichkeit | Normal                                  | <p><p style="color:#184B76;">Daten zu behördlichen Zuständigkeiten und Verbindungsdaten zu Diensten bieten wenig Grundlage für Missbrauch und können im Nutzerkreis von Behörden geteilt werden.</p></p> *Hinweis: Die bestehenden Schutzmechanismen des **DVDV** bleiben hiervon unberührt.*                                                                                                   | [NFA_S004] <p><p style="color:#184B76;">[NFA_S005]</p> | 
| Integrität      | Hoch                                    | Eine Manipulation der Daten der **RDN** könnte die Fehlleitung von Nachweisabrufen und damit potenziell Falschausstellung von Nachweisen zur Folge haben und ist aufgrund der Betrugsmöglichkeiten zu vermeiden.                                                          | [NFA_S001]                                  |
| Verfügbarkeit   | Hoch                                    | Die **RDN** muss verlässlich erreichbar sein, da sonst kein Nachweisabruf möglich ist, was zum einen die Leistung der Nutzenden und zum anderen das korrekte Arbeiten der Verwaltung beeinträchtigt. Dies ist vor allem im Zusammenhang mit Sicherheitsbehörden kritisch. | [NFA_Z001]                                  |


Die Konkretisierung der IT-Sicherheitsanforderungen, entsprechend den
Anforderungen des IT-Grundschutzes, wird in der Projektplanung
umgesetzt. Insbesondere ist hier eine Risikoanalyse frühzeitig
einzuplanen und ein Sicherheitskonzept in Einvernehmen mit dem Bundesamt
für Sicherheit in der Informationstechnik (BSI) zu erstellen. Dabei
müssen Betriebsumgebung sowie das Rechte- und Rollensystem für die
Zugriffe auf die **RDN** den formulierten Schutzzielen entsprechen.

### Antwortzeit und Last

Zum aktuellen Zeitpunkt sind weder die Antwortzeiten noch das Lastprofil
vorhersehbar. In jedem Fall sollen sie einen synchronen Prozess des
Nachweisabrufs ermöglichen. Die dafür nötigen Antwortzeiten des gesamten
Nachweisabrufs werden zu einem späteren Zeitpunkt für
das *NOOTS* festgelegt. Zur Anzahl zugreifender Verfahren, deren
Nutzungshäufigkeit und zeitliche Verteilung gibt es keine ausreichenden
Erkenntnisse.

Daraus lassen sich folgende Konsequenzen ableiten:

-   Eine abschließende Anforderung an Lastfähigkeit und Antwortzeiten
    ist nicht möglich.
-   Die Lastanforderungen sind mit jeder Zuschaltung weiterer *Data
    Consumer* zu überprüfen und die Skalierung der
    Registerdatennavigation ist entsprechend anzupassen. Initiale
    Lastanforderungen sind im Folgenden aufgeführt und im Rahmen der
    Umsetzungskonzeption zu überprüfen.
-   Langfristig ist eine hochgradig automatisiert skalierbare
    Betriebsumgebung anzustreben, welche auch die erforderliche
    Elastizität bietet, um Lastspitzen aus Onlineverfahren abzufedern.

### Liste der nicht-funktionalen Anforderungen

Tabelle AD-NOOTS-07-15: Registerdatennavigation - Nicht-funktionale
Anforderungen

| ID                 | Kriterium                 | Anforderung                                                                                                                                                                                                                                                                                                                   | Prüfkriterium                                                                                                                     |
|--------------------|---------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| **Leistungseffizienz** |                           |                                                                                                                                                                                                                                                                                                                               |                                                                                                                                   |
| NFA_L001           | Zeitverhalten             | Im Gesamtverfahren eines Nachweisabrufes muss die Antwort der **RDN** so schnell durchgeführt werden, dass die Nutzerfreundlichkeit des Gesamtablaufs aufrechterhalten wird. Das genaue Zeitverhalten ist bei Tests in Pilotverfahren und unter Berücksichtigung des Zusammenspiels aller eingesetzten Komponenten zu definieren. | Architekturreview, Ende-zu-Ende und Lasttests                                                                                     |
| NFA_L002           | Kapazität                 | <p><p style="color:#184B76;">Im regulären Wirkbetrieb muss eine hohe Anzahl an Anfragen pro Sekunde verarbeitet werden.</p></p> Eine Abschätzung ist zum aktuellen Zeitpunkt nicht möglich. Daher muss die **RDN** einen sukzessiven Ausbau der Kapazität ermöglichen.                                                                                                                                                                                                                                    | Erfolgreiche Durchführung entsprechender Ende-zu-Ende Tests auf einer produktionsnahen Umgebung an allen externen Schnittstellen. |
| NFA_L003           | Skalierbarkeit            | Das System muss so ausgelegt werden, dass perspektivisch bei Lastspitzen eine automatische Skalierung erfolgen kann.</p> </p>*Diese Anforderung ist bei jeder Ausbaustufe, in der weitere Verfahren angebunden werden, zu berücksichtigen.*                                                                                                                                                                                                         | Architekturreview, Lasttests                                                                                                      |
|**Zuverlässigkeit**     |                           |                                                                                                                                                                                                                                                                                                                               |                                                                                                                                   |
| NFA_Z001           | Verfügbarkeit             | Vorläufige Einschätzung: Die Registerdatennavigation muss eine hohe Verfügbarkeit erreichen und perspektivisch zu 99,9% verfügbar sein.                                                                                                                                                                                       | Bewertung durch ein Architekturreview<p> </p>Messungen im Produktivbetrieb                            |
| NFA_Z002           | Nutzungszeiten            | Die **RDN** muss 24/7 nutzbar sein.                                                                                                                                                                                                                                                                                               |                                                                                                                                   |
| NFA_Z003           | Reifegrad                 | Die Lösung verwendet ausgereifte Authentifizierungsverfahren wieder, die im NOOTS und EU-**OOTS** verwendet werden.                                                                                                                                                                                                               | Bewertung durch ein Architekturreview                                                                                             |
| NFA_Z003           | Fehlertoleranz            | Die Registerdatennavigation muss Mechanismen zur Wiederherstellung nach vorübergehenden Übertragungsausfällen sicherstellen.                                                                                                                                                                                                  |                                                                                                                                   |
| **Sicherheit**         |                           |                                                                                                                                                                                                                                                                                                                               |                                                                                                                                   |
| NFA_S001           | Integrität                | Vorläufige Einschätzung: Es muss eine hohe Integrität (gemäß Schutzbedarf) gewährleistet werden.                                                                                                                                                                                                                              | Bewertung durch ein Sicherheitsaudit                                                                                              |
| NFA_S002           | Nachweisbarkeit           | Die **RDN** muss alle Änderungen im Rahmen der Pflege revisionssicher protokollieren.                                                                                                                                                                                                                                             | Bewertung durch ein Architekturreview<p> </p>Erfolgreiche Durchführung eines funktionalen Tests.                                                                                            |
| NFA_S003           | Authentizität             | Die Authentizität der Nutzenden muss in allen Fällen sichergestellt werden.                                                                                                                                                                                                                                                   | Bewertung durch ein Architekturreview<p> </p>Bewertung durch ein Sicherheitsaudit                                                                                             |
| NFA_S004           | Vertraulichkeit           | Vorläufige Einschätzung: Die Vertraulichkeit wird als normal eingeordnet.<p></p>Alle Nutzenden dürfen nur die Daten sehen, für die sie ermächtigt wurden (eine rechtliche Grundlage zur Einsicht der Daten besteht). <p></p> Allen Nutzenden werden nur die Funktionen angeboten, die sie auch tatsächlich nutzen dürfen.                                                                                                                                                                                                                                                     | Bewertung durch ein Architekturreview<p></p>Bewertung durch ein Sicherheitsaudit                                                                                             |
| NFA_S005           | Rollen und Berechtigungen | Die RDN muss über ein Rollen- und Rechtesystem verfügen, über das ihre Dienste gemäß der dafür noch zu identifizierenden Schutzziele vor unberechtigtem Zugriff geschützt werden.                                                                                                                                             | Bewertung durch ein Architekturreview<p></p>Bewertung durch ein Sicherheitsaudit | 
|**Kompatibilität**     |                           |                                                                                                                                                                                                                                                                                                                               |                                                                                                                                   |
| NFA_K001           | Interoperabilität         | Unter der Annahme, das Sichere Anschlussknoten sowohl von Data Consumer [ANN-08] als auch von der Registerdatennavigation [ANN-10] genutzt werden, müssen Abfragedienste der **RDN** aus den Netzen des Bundes (**NdB**) und aus Landes- und Kommunalnetzen erreichbar sein.                                                          | Bewertung durch ein Architekturreview                                                                                             |
| NFA_K002           | Interoperabilität         | Die Dienste der **RDN** sollen auf einfache Nutzbarkeit ausgelegt werden und auf gängigen Marktstandards aufsetzen.                                                                                                                                                                                                               | Bewertung durch ein Architekturreview                                                                                             |


## Ausblick & Weiterführende Aspekte

### Offene Punkte

Tabelle AD-NOOTS-07-16: Offene Punkte relevant für die Konzeption der
Registerdatennavigation

| ID    | Offene Punkte                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|-------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| OP-01 | Bei mehrstufigen Zuständigkeitslogiken ist zu prüfen, ob der **RDN**-Mechanismus tragfähig ist. Es fehlt aber bisher an konkreten Beispielen.                                                                                                                                                                                                                                                                                                                                                                                                |
| OP-02 | Die nicht-funktionalen Anforderungen sind anzupassen, sobald nicht-funktionalen Anforderungen auf Ebene der Gesamtarchitektur spezifiziert werden.                                                                                                                                                                                                                                                                                                                                                                                       |
| OP-03 | Für die technischen Dienste müssen neben den Verbindungsparameter weitere Informationen (Verwendbarkeit der IDNr (bzw. beWiNr), Information über das erforderliche Vertrauensniveau, Identifier (Service-ID) des entsprechenden technischen Dienstes, sowie die von einem Dienst angebotenen Formate und Schemata) gespeichert werden können. Es ist zu klären, ob dafür das DVDV geeignet ist, ggf. geeignet erweitert werden kann, oder ob dafür ein zusätzliches NOOTS Dienste Verzeichnis (NDV) in der RDN vorgesehen werden sollte? |
| OP-04 | <p><p style="color:#184B76;">Mindestens für den HLA-Anwendungsfall 3 (EU Evidence Requester benötigt Nachweis aus Deutschland) muss im zentralen europäischen DSD gepflegt werden, welche Intermediäre Plattform (IP) für einen bestimmten Nachweistyp als Evidence Provider fungiert und welche Formate und Schemata die IP für diesen Nachweistyp unterstützt. Es ist zu prüfen:</p></p> <ul><li><p style="color:#184B76;">inwieweit für einen Nachweistyp nur solche Formate und Schemata einzutragen sind, die bei dezentralen Registern von allen / einer Mehrzahl der zuständigen deutschen Register geliefert werden können?</p> <li><p style="color:#184B76;">ob die in jedem Fall notwendige Pflege der Informationen zu den IPs im zentralen europäischen Data Service Directory (EU-DSD) durch die Aggregation und/oder Synchronisation von RDN-Informationen sinnvoll unterstützt werden kann?</p> <li><p style="color:#184B76;">ob RDN-Informationen (ggf. aggregiert) automatisch an EU-EB und/oder EU-DSD propagiert werden sollen?</p> |
| OP-05 | Es ist zu prüfen, ob [ANN-05] weiter aufrechterhalten werden kann ("[ANN-05]: Bei Nachweisabrufen aus Deutschland ins EU-Ausland wenden sich deutsche Data Consumer direkt an die für sie zuständige nationale Intermediäre Plattform (ggf. gibt es nur eine). Gegenwärtig wird davon ausgegangen, dass die **RDN** in diesem Fall nur für den Abruf der Verbindungsparameter durch die sicheren Anschlussknoten genutzt wird.")                                                                                                             |
| OP-06 | Es könnte bereits bestimmte Stellen (Verzeichnisse, Webseiten, etc.) geben, in denen für eine bestimmte Fachlichkeit die Verteilung der Zuständigkeiten gepflegt wird und nachgeschlagen werden kann. Inwieweit die Informationen solcher Quellen automatisiert in das DVZV übernommen werden können, wäre zu prüfen.                                                                                                                                                                                                                    |
| OP-07 | Es ist zu klären, ob das IAM für Behörden von Anfang an durch die RDN genutzt werden kann (d.h. IAM für Behörden müsste vor 2025 entwickelt werden).                                                                                                                                                                                                                                                                                                                                                                                     |
| OP-08 | Die Begriffsbildung zu Data Consumer und Data Provider wurde in Q4/2023 geschärft. Es ist zu prüfen, wie sich die neuen Erkenntnisse auf die in diesem Dokument verwendeten Begriffe auswirken und in einem kommenden Release in diesem Dokument stärkere Berücksichtigung finden können.                                                                                                                                                                                                                                                |
| OP-09 | Durch die neuen Anforderungen [ANN-07], [ANN-08] und [ANN-09] sind die Funktionen **API**-NAT-1 und **API**-NAT-2 sehr ähnlich. Es ist zu prüfen, ob eine eigene Funktion **API**-NAT-2 für Anforderung [AFO-**API**-NAT-02] noch einen Mehrwert bringt.                                                                                                                                                                                                                                                                                                 |
| OP-10 | Benötigen die Funktionen API-NAT-1 und API-NAT-2 tatsächlich eine Behördenbeschreibung als Rückgabeparameter, oder besser eine Referenz auf den im IAM gepflegten Teilnehmer?                                                                                                                                                                                                                                                                                                                                                            |
| OP-11 | Sollen Nutzerprofile (mit personenbezogenen Daten) in zwei Systemen ("Pflegesystem", "IAM der RDN") gepflegt werden? Oder wäre es günstiger, Zugriffsrechte für Pflege-Clients im IAM für Behörden zu verwalten und die Pflege der RDN-Daten ausschließlich über **Pflege-Clients** zu realisieren?                                                                                                                                                                                                                                          |
| OP-12 | Muss die RDN im Zugriff beschränkt werden, oder sind die Informationen öffentlich und quasi "jeder" kann lesend darauf zugreifen?                                                                                                                                                                                                                                                                                                                                                                                                        |


### Datenquellen für das Konzept der Registerdatennavigation

Aufbereitung von Datenquellen, die im Rahmen der Lösungskonzeption der
Registerdatennavigation bewertet wurden.

*Verwaltungsdaten-Informationsplattform (VIP):* Die vom Statistischen
Bundesamt geführte Verwaltungsdaten-Informationsplattform (VIP) bietet
einen umfassenden Überblick über die in der Verwaltung gehaltenen
Datenbestände. Die VIP erfasst hierbei ausschließlich Metadaten. Neben
allgemeinen Informationen, etwa zur Registerführung oder technischen und
rechtlichen Aspekten, liefert die Plattform detaillierte Beschreibungen
zu den in den Datenbeständen erfassten Merkmalen: so
z.B. "Familienname" oder \"E-Mail-Adresse".

Quelle: [Destatis](https://www.destatis.de/DE/Themen/Staat/Buerokratiekosten/VIP/vip.html)

*Evidence Survey:* Erhebung der Europäischen Kommission. Umfasst die
für **SDG**-2 relevanten Leika-Leistungen sowie die abstrakten
verfahrensbezogenen Nachweisanforderungen für 20 **SDG** Verfahren
(Batch 1) (ausgenommen Verfahren Nr. 16 Business Procedure und Nr. 21-24
EU-Richtlinien, die erst in Batch 2 bearbeitet werden). Auch
Nachweistypen, die jeweils für die einzelnen LeiKa-Leistungen relevant
sind, wurden in einem ersten Schritt national bei den Bundesressorts
erhoben und liegen ohne verbindliche Abstimmung vor. Für die finale
Erhebung der Nachweistypen bedarf es weiterer Abstimmung mit den
Vollzugsbehörden / Ländern. Zudem stehen neue Anforderungen seitens der
Europäischen Kommission im Raum, die noch in der Klärung mit der
Kommission sind. Seitens der Europäischen Kommission wurde noch kein
Zeitplan für die Meldung konkreter Nachweistypen aus den Mitgliedstaaten
formuliert.

Quelle: Auszug aus dem **BMI** *Evidence* Survey Zwischenbericht

*Leistungskatalog (LeiKa):* Die Abkürzung LeiKa bezeichnet den
\"Leistungskatalog der öffentlichen Verwaltung". Der Leistungskatalog
stellt ein einheitliches, vollständiges und umfassendes Verzeichnis der
Verwaltungsleistungen über alle Verwaltungsebenen in Deutschland hinweg
dar und wird ständig fortgeschrieben. Der LeiKa umfasst derzeit einen
Bestand von mehr als 8.000 Einträgen im Katalog des Bausteins Leistungen
(Stand: 30.06.2021). Dies beinhaltet alle drei Arten: Leistungsobjekte,
Leistungsobjekte mit Verrichtungskennung sowie Leistungsobjekte mit
Verrichtungskennung und Detail.

Quelle: [Onlinezugangsgesetz](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/das-gesetz/das-gesetz-node.html)

*Deutsches Verwaltungsdienste Verzeichnis (**DVDV**):* Das Deutsche
Verwaltungsdienste Verzeichnis (**DVDV**) ermöglicht es
E-Government-Anwendungen, deutschlandweit sicher und rechtskonform Daten
auszutauschen. Das Dienste Verzeichnis ist eine föderale Anwendung des
IT-Planungsrats und wurde 2007 zunächst für das elektronische
Melderegister entwickelt und im Oktober 2019 von der neuen, flexibleren
Version **DVDV** 2.0 abgelöst. Über 30.000 Fachverfahren bundesweit sind
derzeit in dem Verzeichnis registriert. Für Beratung und
Weiterentwicklung ist die Koordinierende Stelle **DVDV** im ITZBund
zuständig. Das **DVDV** wird von Bund, Ländern und Kommunen gemeinsam
bereitgestellt. Das föderale Prinzip spiegelt sich in der dezentralen
Serverstruktur wider, die die Sicherheit und Verfügbarkeit des
Datenaustauschs erhöht. In ihrem Kern steht der Bundesmaster, den das
ITZBund betreibt. Er ist der einzige Server, an dem die für die
Kommunikation relevanten Daten der angeschlossenen Behörden ergänzt und
verändert werden können. Jedes Bundesland beauftragt eigens dazu eine
Pflegende Stelle. Nur berechtigte Personen können über einen modernen
Web-Client auf die zentrale Datenbank zugreifen.

Quelle: [ITZBund](https://www.itzbund.de/DE/itloesungen/standardloesungen/dvdv/dvdv.html)

*XRepository:* Mit dem XRepository steht allen E-Government-Vorhaben
eine verlässliche Drehscheibe zur Bereitstellung und zum Bezug
XÖV-konformer Standards und Codelisten zur Verfügung. Die Plattform wird
im Auftrag des IT-Planungsrats durch die Koordinierungsstelle für
IT-Standards (**KoSIT**) betrieben.

Quelle: [XRepository](https://www.xrepository.de/)

*Föderales Informationsmanagement (FIM):* Das Föderale
Informationsmanagement (FIM) dient dazu, leicht verständliche
Bürgerinformationen, einheitliche Datenfelder für Formularsysteme und
standardisierte Prozessvorgaben für den Verwaltungsvollzug
bereitzustellen. Ziel ist es, den Übersetzungs- und
Implementierungsaufwand rechtlicher Vorgaben zu senken. Länder und
Kommunen sollen - bezogen auf die redaktionelle und organisatorische
Umsetzung eines Verwaltungsverfahrens - nicht mehr für sich alleine
agieren müssen. Stattdessen können sie auf qualitätsgesicherte
Vorarbeiten der nächsthöheren Verwaltungsebene zurückgreifen.

Quelle: [FIM-Portal](https://fimportal.de/)

*Registerlandkarte:* Mit der Bereitstellung der Registerübersicht für
die Federführenden der Registermodernisierung wurde ein wichtiges
Etappenziel in der Entwicklung der Registerlandkarte erreicht. Die
Registerübersicht ist eine Access-Datenbank, die zentral fachliche,
technische und rechtliche Informationen für die 51 Register nach
**IDNrG** und den damit verbundenen Verwaltungsleistungen übersichtlich
darstellt. Damit liefert sie nicht nur nachhaltig wichtige Erfahrungen
für das Endprodukt Registerlandkarte; sie ist auch ein wichtiges
Planungstool für die Registermodernisierung, welche nicht zuletzt
die **OZG**-Umsetzung forciert. Die Verknüpfung von Leistungen und
Registern ermöglicht eine Planung von Digitalisierungs- und
Modernisierungsvorhaben.

Quelle: [BVA](https://www.bva.bund.de/SharedDocs/Kurzmeldungen/DE/Behoerden/Verwaltungsdienstleistungen/Registermodernisierung/Etappenziel_erreicht.html?nn=44066)

### Alternative Lösungsansätze Registerdatennavigation

Die folgenden Lösungsansätze wurden durch das **KT** Architektur geprüft
und für nicht ausreichend tragfähig befunden.

*Eigene Routing-Datenbank:* Der erste Ansatz sieht vor, alle für die
Zuständigkeitsermittlung notwendigen Daten in einer einzigen
Routing-Datenbank abzulegen. Innerhalb der Routing-Datenbank würden sich
demnach Informationen zu Nachweistypen, den dafür zuständigen Behörden,
durch dieses angebotene Diensten und deren technische
Verbindungsparameter abgelegt werden. Diese Lösung hätte den Vorteil,
dass auf eine verteilte Datenhaltung verzichtet werden kann, was
Synchronisations- und Pflegeaufwände reduziert. Zudem könnten
Zuständigkeiten einfach modelliert und Zuständigkeitslogiken ohne großen
Aufwand erweitert werden. Da dieser Ansatz jedoch auf die
Wiederverwendung bestehender Komponenten
wie **DVDV** und **PVOG** verzichtet und einen redundanten Datenbestand
aufbauen würde, wäre dieser Ansatz nicht konform zur Strategie des
IT-Planungsrats.

**DVDV** erweitert um Routingdaten: Der zweite Ansatz sieht vor, die
im **DVDV** geführten Einträge zu Behörden, die durch Behörden
angebotenen Dienste und deren technischen Verbindungsparameter
wiederzuverwenden und um Routingdaten zu erweitern, die zur Ermittlung
der Zuständigkeit notwendig sind. Neben den Informationen
des **DVDV** könnten zudem vorhandene Replikationsmechanismen und
das **DVDV**-Pflegekonzept nachgenutzt werden. Ein weiterer Vorteil ist
die einfache Modellierung von Zuständigkeiten und die aufwandsarme
Erweiterbarkeit der Zuständigkeitslogik. Auch würde diese Lösung keine
eigene Datenhaltung benötigen, da die Zuständigkeitsdaten
im **DVDV** ergänzt werden würden. Dies jedoch hätte eine umfangreiche
und aufwendige Erweiterung des **DVDV** zur Folge. Auch würde auf die
Verwendung von bereits im **PVOG** geführten Zuständigkeitsdaten
verzichtet werden, was zu einem doppelten Pflegeaufwand führt und
deshalb als nicht erstrebenswert bewertet wird.

**DVDV** mit erweiterter Schlüssellogik*: Dieser dritte Ansatz sieht
vor, die im **DVDV** geführten Einträge zu Behörden, die durch Behörden
angebotene Dienste und deren technischen Verbindungsparameter
wiederzuverwenden und Zuständigkeiten über eine Erweiterung der
Schlüssellogik abzubilden. Auch hier wäre der Vorteil, dass eine
zentrale Datenhaltung im **DVDV** zum Tragen kommen würde und dass
bereits etablierte Replikationsmechanismen und
das **DVDV**-Pflegekonzept nachgenutzt werden könnten. Im Rahmen der
Bewertung wurde jedoch festgestellt, dass die Erweiterung der
Schlüssellogik an sich sehr aufwändig und zum Teil unzureichend ist,
wenn es um die Abbildung komplexer Zuständigkeiten geht.

### Alternativer Lösungsansatz API RDN Abfrage

Betrachtet, jedoch verworfen, wurde folgende Alternative der
vollständigen Bündelung der beiden Prozessschritte in einer einzigen
Schnittstellen-Funktion, die von allen Data Consumern zu nutzen wäre:

![C:\\af48702220e86b591d384d0afa9de5aa](./assets/images7/media/image6.jpeg)
<br>**Abb. 6: Registerdatennavigation API mit gebündelter Funktion**</br>


Registerdatennavigation API mit gebündelter Funktion

Der Vorteil dieser vollständigen Bündelung liegt darin, dass für das
Nutzen der Registerdatennavigation ein einheitlicher Aufruf
implementiert werden muss, was Aufwand auf Seiten der *Data
Consumer* und der **RDN** spart. Als entscheidend nachteilig bewertet
wurde aber, dass in Fällen, in denen Prozessschritt 1 aufgrund fehlender
Zuständigkeitsparameter vom *Data Consumer* nicht durchlaufen werden
kann, die gebündelte Funktion für diese Nutzergruppe nicht genutzt
werden kann.

### Erläuterung zur vorläufigen Einschätzung zum Schutzbedarf

#### Vertraulichkeit

Wie in Kapitel
[Datenschutz](#datenschutz)
ausgeführt, ist die Erfassung und Verarbeitung von personenbezogenen
Daten durch die **RDN** eine Ausnahme, weshalb hier ein geringes Risiko
für die Verletzung der Vertraulichkeit für Personen entsteht.

Bei den Daten zur behördlichen Zuständigkeit ist davon auszugehen, dass
es sich um öffentliche Datenbestände handelt, die auch in anderen
Quellen nachzulesen und deshalb nicht als vertraulich einzustufen sind.
Technische Verbindungsparameter sind ebenfalls unkritische Informationen
innerhalb des Nutzerkreises von Behörden.

Um den Zugriff ausschließlich Behörden zu ermöglichen, muss die
Authentizität der nachfragenden Stellen zweifelsfrei belegbar sein und
auf ihre Zugangsberechtigung geprüft werden. Dabei kann, analog zum IAM
für Behörden, eine Differenzierung der Behörden vorgenommen werden.
Sicherheitsbehörden und ihre Daten haben ggf. höhere Anforderungen an
die Vertraulichkeit, die in der Umsetzungskonzeption berücksichtigt
werden müssen; in diesem Fall wäre die Einschätzung ggf. anzupassen.

Einschätzung zu Vertraulichkeit: Normal

#### Integrität

Angriffsszenario 1: Korruption der **RDN**-Datenbestände

Ein Angreifer verschafft sich Zugang zur **RDN** und manipuliert dort
Datenbestände, beispielsweise die Verbindungsdaten zu technischen
Diensten. Auf diese Weise können Nachweisabrufe entweder nicht
erfolgreich abgeschlossen oder auf unbefugte Server geleitet werden,
wodurch sich zwei Betrugsmöglichkeiten ergeben:

-   Möglichkeit 1: Die personenbezogenen Daten des Nachweisabrufs
    geraten in unbefugte Hände und geben dem Angreifer Rückschlüsse auf
    die Antragstellenden.
-   Möglichkeit 2: Die unbefugte Stelle stellt gefälschte Nachweise aus.
    Damit wäre es den Antragstellenden möglich, sich auf Grundlage
    falscher Nachweise Leistungen zu erschleichen, was wirtschaftliche
    Konsequenzen für die Verwaltung hat.

Angriffsszenario 2: Abfangen und Verändern von **RDN**-Auskünften

Ein Angreifer fängt die Antwort der **RDN** an einen *Data Consumer* ab
und manipuliert die gesendeten Informationen, z.B. zu den technischen
Verbindungsdaten eines Dienstes. In der Folge wird der Nachweisabruf des
Data Consumer an eine unbefugte Stelle geschickt, woraus sich die zwei
unter Angriffsszenario 1 geschilderten Möglichkeiten mit ihren
datenschutzrelevanten, wirtschaftlichen und vertrauenseinbüßenden Folgen
ergeben.

Einschätzung zu Integrität: Hoch

#### Verfügbarkeit

Die **RDN** wird für Nachweisabrufe im Kontext von Bürgeranträgen auf
Verwaltungsleistungen oder behördeninternen Verfahren eingesetzt. Ein
Ausfall der **RDN** hätte eine Verzögerung oder einen Abbruch der
Nachweis-Bereitstellung und somit der damit im Zusammenhang stehenden
Antragstellungen oder Behördenprozesse zur Folge.

Zwar könnten im Notfall bei längerem Ausfall alternative
Papier-gebundene Wege der Nachweisübermittlung genutzt werden, dies
würde aber bei den Nutzenden zu Vertrauensverlust und Frustration mit
dem neuen System führen und ist daher soweit es geht zu vermeiden.

Sollten Sicherheitsbehörden die Infrastruktur der Registermodernisierung
nutzen wollen und eigene, strengere Anforderungen an die Verfügbarkeit
der **RDN**-Dienste stellen, wäre die hier gefasste Einschätzung neu zu
bewerten.

Einschätzung zu Verfügbarkeit: Hoch