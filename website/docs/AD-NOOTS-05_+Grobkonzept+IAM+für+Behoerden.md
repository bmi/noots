>**Redaktioneller Hinweis**
>
>Dokument aus der zweiten Iteration - nicht Teil der aktuellen Iteration.
Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## Abstract

<p>Das Identity- und Access Management f&uuml;r Beh&ouml;rden (IAM f&uuml;r Beh&ouml;rden) ist ein zentrales System zur Zuordnung und zum Abruf der Identit&auml;tsdaten und Rollen von IT-Komponenten, die von &ouml;ffentlichen Stellen verantwortet werden. Jede IT-Komponente wird von einer registrierten &ouml;ffentlichen Stelle fachlich verantwortet (fachverantwortliche Stelle) und von einer sonstigen Stelle technisch betrieben (betriebsverantwortliche Stelle). Das IAM f&uuml;r Beh&ouml;rden erm&ouml;glicht die Registrierung dieser verantwortlichen Stellen und deren IT-Komponenten samt zweckgem&auml;&szlig;er Zuordnung von Rollen entsprechend ihrer Fachlichkeit. Es erm&ouml;glicht den Abruf registrierter Identit&auml;tsdaten und Rollen f&uuml;r Zugriffe auf andere IT-Komponenten (Ressourcen). Es erm&ouml;glicht Fachaufsichten die Pr&uuml;fung der Korrektheit fachlicher Zuordnungen &ouml;ffentlicher Stellen und zweckgem&auml;&szlig;er Rollen von IT-Komponenten.</p>
<p>Das IAM f&uuml;r Beh&ouml;rden stellt bei der Registrierung verantwortlicher Stellen die Verwendung vertrauensw&uuml;rdiger Identit&auml;ten und eine eindeutige fachliche Zuordnung sicher. Es stellt bei der Registrierung von IT-Komponenten die eindeutige Zuordnung deren verantwortlicher Stellen und zweckgem&auml;&szlig;er Rollen sicher. Es stellt beim Abruf von Identit&auml;tsdaten und Rollen deren &uuml;berpr&uuml;fbare Integrit&auml;t sicher. Das IAM f&uuml;r Beh&ouml;rden erm&ouml;glicht dadurch Ressourcen die sichere Pr&uuml;fung von Zugriffsberechtigungen anfragender IT-Komponenten anhand von integren Identit&auml;tsdaten und Rollen.</p>
<p>Das IAM f&uuml;r Beh&ouml;rden wird als zentraler Berechtigungsdienst f&uuml;r technische Teilnehmer und Komponenten des Nationalen Once-Only Technical System (NOOTS) eingesetzt. Jede IT-Komponente muss ein&nbsp;Zugriffstoken</a> mit vom IAM f&uuml;r Beh&ouml;rden best&auml;tigten Identit&auml;tsdaten und Rollen vorweisen, wenn sie auf eine Ressource des NOOTS (z.B. auf einen Data Provider) zugreifen will. Die Ressource muss die Zugriffsberechtigung der anfragenden IT-Komponente anhand der im Zugriffstoken enthaltenen Rollen pr&uuml;fen und beim Vorliegen einer berechtigten Rolle die anfragende IT-Komponente f&uuml;r den Zugriff autorisieren. Die Ressource kann die im Zugriffstoken enthaltenen Identit&auml;tsdaten f&uuml;r weitere Zwecke verwenden, bspw. zur Ermittlung fachlicher Zugeh&ouml;rigkeit oder zur Zugriffsprotokollierung.</p>
<p>Das IAM f&uuml;r Beh&ouml;rden ist kein Berechtigungsdienst f&uuml;r nicht-technische Teilnehmer, insbesondere nicht f&uuml;r B&uuml;rgerinnen und B&uuml;rger oder Unternehmen, die Onlinedienste mit NOOTS-Anbindung nutzen. Solche Onlinedienste sind hingegen technische Teilnehmer und nutzen das IAM f&uuml;r Beh&ouml;rden zum Zugriff auf Ressourcen des NOOTS.</p>
<p>Das vorliegende Konzept beschreibt die&nbsp;Ziele</a>, Akteure</a> und Prozesse</a> des IAM f&uuml;r Beh&ouml;rden und die daf&uuml;r n&ouml;tige Datenhaltung</a>. Es richtet sich an IT-Verantwortliche der &ouml;ffentlichen Verwaltung mit Verantwortung f&uuml;r das IAM f&uuml;r Beh&ouml;rden, mit Verantwortung f&uuml;r IT-Komponenten zur elektronischen Kommunikation mit &ouml;ffentlichen Stellen oder mit Fachaufsicht &uuml;ber &ouml;ffentliche Stellen, die solche IT-Komponenten betreiben.</p>

## Einf&uuml;hrung und Ziele

### &Uuml;berblick

<p>Das IAM f&uuml;r Beh&ouml;rden ist ein zentrales System zur Zuordnung und zum Abruf der Identit&auml;tsdaten und Rollen von IT-Komponenten f&uuml;r Zugriffe auf andere IT-Komponenten (Ressourcen</a>) gem&auml;&szlig; rechtlichen Vorgaben. Das IAM f&uuml;r Beh&ouml;rden</p>
<ol>
<li>erm&ouml;glicht die Registrierung und Pflege von Akteuren</a>&nbsp;und&nbsp;IT-Komponenten</a>,</li>
<li>erm&ouml;glicht die fachliche Zuordnung</a> von fachverantwortlichen Stellen, Fachaufsichten und IT-Komponenten</a>,</li>
<li>erm&ouml;glicht die Zuordnung zweckgem&auml;&szlig;er Rollen</a> zu IT-Komponenten,</li>
<li>erm&ouml;glicht IT-Komponenten den Abruf ihrer Identit&auml;tsdaten und Rollen</a>,</li>
<li>verlangt zu seiner Nutzung vertrauensw&uuml;rdige elektronische Identit&auml;ten</a>,</li>
<li>protokolliert &Auml;nderungen im Datenbestand und die Nutzung seiner Funktionen.</li>
</ol>

### Ziele der Komponente

<p>Zur Sicherstellung des n&ouml;tigen Vertrauens von Beteiligten in das IAM f&uuml;r Beh&ouml;rden und in die von ihm abgerufenen Identit&auml;tsdaten und Rollen sowie f&uuml;r seine m&ouml;glichst effiziente Nutzung werden folgende Ziele verfolgt:</p>
<p><span style="color: #000000;"><strong>Tab. 1: Ziele</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">&nbsp;</span></th>
<th><span style="color: #000000;">Aspekt</span></th>
<th><span style="color: #000000;">Ziel</span></th>
</tr>
<tr>
<td><span style="color: #000000;">1</span></td>
<td><span style="color: #000000;">&Uuml;bergreifend</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden erm&ouml;glicht die vertrauensw&uuml;rdige und effiziente Registrierung verantwortlicher Stellen und IT-Komponenten samt fachlicher Zuordnung und zweckgem&auml;&szlig;en Rollen.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">2</span></td>
<td><span style="color: #000000;">&Uuml;bergreifend</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden erm&ouml;glicht die vertrauensw&uuml;rdige und effiziente Bereitstellung registrierter Identit&auml;tsdaten und Rollen von IT-Komponenten f&uuml;r Zugriffe auf Ressourcen des NOOTS.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">3</span></td>
<td><span style="color: #000000;">Vertrauensstellung</span></td>
<td>
<p><span style="color: #000000;">Akteure sind vertrauensw&uuml;rdig und &ouml;ffentliche Stellen eindeutig als solche erkennbar.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">4</span></td>
<td><span style="color: #000000;">Vertrauensstellung</span></td>
<td>
<p><span style="color: #000000;">&Ouml;ffentliche Stellen sind fachlich korrekt zugeordnet.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">5</span></td>
<td><span style="color: #000000;">Vertrauensstellung</span></td>
<td>
<p><span style="color: #000000;">&Ouml;ffentliche Stellen verantworten&nbsp;IT-Komponenten und deren fachliche Zuordnung samt zweckgem&auml;&szlig;er Rollen.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">6</span></td>
<td><span style="color: #000000;">Vertrauensstellung</span></td>
<td>
<p><span style="color: #000000;">Prozessnutzer (Akteure und IT-Komponenten) werden eindeutig authentifiziert.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">7</span></td>
<td><span style="color: #000000;">Vertrauensstellung</span></td>
<td>
<p><span style="color: #000000;">Prozesse k&ouml;nnen nur in der Authentifizierung entsprechenden Umfang genutzt werden.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">8</span></td>
<td><span style="color: #000000;">Vertrauensstellung</span></td>
<td>
<p><span style="color: #000000;">Die Korrektheit von Daten und Zuordnungen wird angemessen gepr&uuml;ft.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">9</span></td>
<td><span style="color: #000000;">Effiziente Nutzung</span></td>
<td>
<p><span style="color: #000000;">Akteure k&ouml;nnen sich per Zertifikat registrieren und anschlie&szlig;end sofort Prozesse nutzen.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">10</span></td>
<td><span style="color: #000000;">Effiziente Nutzung</span></td>
<td>
<p><span style="color: #000000;">IT-Komponenten werden von registrierten Akteuren registriert und ben&ouml;tigen keine eigenen Zertifikate.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">11</span></td>
<td><span style="color: #000000;">Effiziente Nutzung</span></td>
<td>
<p><span style="color: #000000;">IT-Komponenten werden gem&auml;&szlig; der fachlichen Zuordnung ihrer fachverantwortlichen Stelle fachlich zugeordnet.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">12</span></td>
<td><span style="color: #000000;">Effiziente Nutzung</span></td>
<td>
<p><span style="color: #000000;">IT-Komponenten werden &uuml;ber ihre Teilnahmeart den zweckgem&auml;&szlig;en Rollen f&uuml;r Ressourcenzugriffe zugeordnet.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">13</span></td>
<td><span style="color: #000000;">Effiziente Nutzung</span></td>
<td>
<p><span style="color: #000000;">Zum Betrieb n&ouml;tige Daten und Prozesse stehen zur Verf&uuml;gung, zur fachlichen Steuerung n&ouml;tige Daten werden nach Bedarf eingebracht.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">14</span></td>
<td><span style="color: #000000;">Effiziente Nutzung</span></td>
<td>
<p><span style="color: #000000;">Die Beh&ouml;rdenfunktionen und Verwaltungsbereiche k&ouml;nnen von Berechtigten zur Nachnutzung bezogen werden.</span></p>
</td>
</tr>
</tbody>
</table>

### Begriffsdefinitionen

<p>Die f&uuml;r das Verst&auml;ndnis dieses Konzepts relevanten Akteure und Begriffe werden nachfolgend definiert. Die jeweilige Definition ist ggf. auf die Zwecke des Konzepts begrenzt.</p>

#### Akteure

<p>Ein Akteur ist eine Organisationseinheit, die Prozesse des IAM f&uuml;r Beh&ouml;rden nutzt. Es gibt folgende Akteure:</p>
<p><span style="color: #000000;"><strong>Tab. 2: Akteure</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">Akteur</span></th>
<th><span style="color: #000000;">Beschreibung</span></th>
</tr>
<tr>
<td><span style="color: #000000;">&Ouml;ffentliche Stelle</span></td>
<td><span style="color: #000000;">Beh&ouml;rde oder sonstige &ouml;ffentliche Stelle gem&auml;&szlig; &sect; 2 BDSG</span></td>
</tr>
<tr>
<td><span style="color: #000000;">Fachverantwortliche Stelle</span></td>
<td><span style="color: #000000;">F&uuml;r den Betrieb einer IT-Komponente fachlich verantwortliche &ouml;ffentliche Stelle. Eine fachverantwortliche Stelle kann mehrere IT-Komponenten im selben Verwaltungsbereich verantworten.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">Betriebsverantwortliche Stelle</span></td>
<td>
<p><span style="color: #000000;">Von der fachverantwortlichen Stelle einer IT-Komponente mit deren technischem Betrieb Beauftragter. Eine betriebsverantwortliche Stelle kann mehrere IT-Komponenten im selben Verwaltungsbereich technisch betreiben.</span></p>
<p><span style="color: #000000;">Betriebsverantwortliche Stelle kann eine &ouml;ffentliche Stelle oder ein Unternehmen gem&auml;&szlig; &sect; 3 UBRegG sein.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">Pflegende Stelle</span></td>
<td><span style="color: #000000;">Mit der Datenpflege und &Uuml;berwachung des IAM f&uuml;r Beh&ouml;rden beauftragte &ouml;ffentliche Stelle</span></td>
</tr>
<tr>
<td><span style="color: #000000;">Fachaufsicht</span></td>
<td>
<p><span style="color: #000000;">&Ouml;ffentliche Stelle mit Fachaufsicht &uuml;ber andere &ouml;ffentliche Stellen</span></p>
</td>
</tr>
</tbody>
</table>

#### Begriffe

<p><span style="color: #000000;"><strong>Tab. 3: Begriffe</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">&nbsp;</span></th>
<th><span style="color: #000000;">Begriff</span></th>
<th><span style="color: #000000;">Bedeutung</span></th>
</tr>
<tr>
<td><span style="color: #000000;">1</span></td>
<td><span style="color: #000000;">Zertifikat</span></td>
<td><span style="color: #000000;">Elektronischer Identit&auml;tsnachweis zur Authentifizierung eines Akteurs (siehe Umgang mit Zertifikaten), ausgestellt von einer Zertifizierungsstelle</span></td>
</tr>
<tr>
<td><span style="color: #000000;">2</span></td>
<td><span style="color: #000000;">Zertifizierungsstelle (CA)</span></td>
<td><span style="color: #000000;">Von einer Wurzelzertifizierungsstelle direkt oder indirekt zur Ausstellung von Zertifikaten berechtigte Stelle (Certification Authority)</span></td>
</tr>
<tr>
<td><span style="color: #000000;">3</span></td>
<td><span style="color: #000000;">Wurzelzertifizierungsstelle (Root CA)</span></td>
<td><span style="color: #000000;">Vertrauensanker bei der Zertifikatspr&uuml;fung. Jede Zertifizierungsstelle muss von einer &uuml;bergeordneten Zertifizierungsstelle zur Ausstellung von Zertifikaten berechtigt sein. Diese Kette &uuml;bergeordneter Stellen endet bei einer Wurzelzertifizierungsstelle.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">4</span></td>
<td><span style="color: #000000;">IT-Komponente</span></td>
<td><span style="color: #000000;">IT-Anwendung oder Basisdienst zur elektronischen Kommunikation mit&nbsp;&ouml;ffentlichen Stellen (vgl. &sect; 2 (6) OZG <a style="color: #000000;" href="https://www.gesetze-im-internet.de/ozg/OZG.pdf">[RGR-03]</a>)</span></td>
</tr>
<tr>
<td><span style="color: #000000;">5</span></td>
<td><span style="color: #000000;">Verwaltungsbereich</span></td>
<td><span style="color: #000000;">Bestimmter Teil der &ouml;ffentlichen Verwaltung gem&auml;&szlig; &sect; 12 (1) IDNrG</span></td>
</tr>
<tr>
<td><span style="color: #000000;">6</span></td>
<td><span style="color: #000000;">Rechtsnorm</span></td>
<td><span style="color: #000000;">Rechtliche Regelung, zu deren Einhaltung &ouml;ffentliche Stellen verpflichtet sind (z.B. EU-Verordnung, Bundesgesetz, Rechtsverordnung)</span></td>
</tr>
<tr>
<td><span style="color: #000000;">7</span></td>
<td><span style="color: #000000;">Beh&ouml;rdenfunktion</span></td>
<td><span style="color: #000000;">Fachlicher Aufgabenbereich einer &ouml;ffentlichen Stelle in einem Verwaltungsbereich aufgrund einer Rechtsnorm (z.B. Zulassungsbeh&ouml;rde gem&auml;&szlig; &sect; 1 (1) StVG im Verwaltungsbereich Verkehr)</span></td>
</tr>
<tr>
<td><span style="color: #000000;">8</span></td>
<td><span style="color: #000000;">Identit&auml;tsdaten Akteur</span></td>
<td>
<p><span style="color: #000000;">Durch das Zertifikat belegte Eigenschaften des Akteurs (siehe G&uuml;ltigkeit von Zertifikaten, Nr. 1.g bis 1.i) sowie identifizierende Merkmale des Zertifikats G&uuml;ltigkeit von Zertifikaten, Nr. 1.a und 1.b), bei fachverantwortlichen Stellen und Fachaufsicht zudem Beh&ouml;rdenfunktion(en) und Verwaltungsbereich.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">9</span></td>
<td><span style="color: #000000;">Identit&auml;tsdaten IT-Komponente</span></td>
<td>
<p><span style="color: #000000;">Komponenten-ID, Bezeichnung, fach- und betriebsverantwortliche Stelle sowie eine der Beh&ouml;rdenfunktionen ihrer fachverantwortlichen Stelle (siehe Registrierung einer IT-Komponente) und deren Verwaltungsbereich.</span></p>
<p><span style="color: #000000;">Die Identit&auml;tsdaten einer IT-Komponente sind im Zugriffstoken (s.u.) enthalten und k&ouml;nnen von Ressourcen (s.u.) nach Bedarf verwendet werden.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">10</span></td>
<td><span style="color: #000000;">Ressource</span></td>
<td><span style="color: #000000;">IT-Komponente, die Daten und Funktionen &uuml;ber Schnittstellen zur Nutzung durch andere IT-Komponenten bereitstellt. Eine Ressource kann den Zugriff &uuml;ber Rollen beschr&auml;nken.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">11</span></td>
<td><span style="color: #000000;">Rolle</span></td>
<td>
<p><span style="color: #000000;">Bestimmte Zugriffsart auf eine Ressource. Eine Rolle kann den Zugriff auf eine oder mehrere Schnittstellen/-methoden der Ressource umfassen.</span></p>
<p><span style="color: #000000;">Die Rollen einer IT-Komponente werden &uuml;ber ihre Teilnahmeart (s.u.) zugeordnet, im Zugriffstoken (s.u.) aufgef&uuml;hrt und von Ressourcen zur Pr&uuml;fung der Zugriffsberechtigung verwendet.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">12</span></td>
<td><span style="color: #000000;">Teilnahmeart</span></td>
<td>
<p><span style="color: #000000;">Fachlicher Zweck einer IT-Komponente mit den daf&uuml;r n&ouml;tigen Rollen.</span></p>
<p><span style="color: #000000;">Aus dem fachlichen Zweck einer IT-Komponente ergeben sich deren Ressourcenzugriffe und die daf&uuml;r n&ouml;tigen Rollen. Diese werden als Teilnahmeart geb&uuml;ndelt und k&ouml;nnen dadurch gleichartigen Teilnehmern in einfacher Weise zugeordnet werden. Das reduziert den Pflegeaufwand und Fehlerm&ouml;glichkeiten.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">13</span></td>
<td><span style="color: #000000;">Zugriffstoken</span></td>
<td><span style="color: #000000;">Gesiegelte Zusammenstellung von Identit&auml;tsdaten, Teilnahmeart und Rollen einer IT-Komponente mit einem G&uuml;ltigkeitszeitraum (siehe Zugriffstoken abrufen)</span></td>
</tr>
</tbody>
</table>

## Randbedingungen

### Technische / Organisatorische / Rechtliche Randbedingungen

#### Betrachtete IT-Komponenten

<p>Im Rahmen dieses Konzepts werden nur IT-Komponenten und Ressourcen mit Bezug zur Registermodernisierung betrachtet.</p>
<p>Betrachtete IT-Komponenten sind</p>
<ul>
<li>die Komponenten des NOOTS (Registerdatennavigation, Intermedi&auml;re Plattformen, IDM f&uuml;r Personen, IDM f&uuml;r Unternehmen, Vermittlungsstelle, Datenschutzcockpit) und</li>
<li>die Teilnehmer des NOOTS (Data Consumer, Data Provider).</li>
</ul>
<p>Davon werden die Registerdatennavigation, das IDM f&uuml;r Personen, das IDM f&uuml;r Unternehmen, die Vermittlungsstelle, Intermedi&auml;re Plattformen (<a href="https://bmi.usercontent.opencode.de/noots/Glossar/">beim Abruf von EU-Nachweisen nach Anwendungsfall 4 in der High-Level-Architecture</a>) und Data Provider als Ressourcen betrachtet.</p>

#### Protokollierung von Ereignissen

<p>Jede Nutzung eines Prozesses des IAM f&uuml;r Beh&ouml;rden (siehe Laufzeitsicht</a>) ist ein relevantes Ereignis und muss vom IAM f&uuml;r Beh&ouml;rden protokolliert werden (siehe&nbsp;NOOTS-953</a>).</p>
<p>Die Protokollierung muss geeignet sein, um diese Ereignisse nachvollziehen und Auff&auml;lligkeiten, St&ouml;rungen und Angriffsversuche erkennen zu k&ouml;nnen. Insbesondere muss der Prozessnutzer (Zertifikatsinhaber, siehe&nbsp;Umgang mit Zertifikaten</a>) eindeutig identifizierbar protokolliert werden.</p>
<p>Die pflegende Stelle muss protokollierte Ereignisse regelm&auml;&szlig;ig hinsichtlich der vorgenannten Aspekte analysieren und ggf. entsprechende Ma&szlig;nahmen zu deren Behebung oder Vermeidung unternehmen.</p>

### Abgrenzungen

<p>Das IAM f&uuml;r Beh&ouml;rden umfasst nicht</p>
<ul>
<li>die Einf&uuml;hrung und Verwendung eindeutiger Beh&ouml;rden-Identifikatoren.
<ul>
<li>Pflegende, fachverantwortliche und betriebsverantwortliche Stellen</a> werden anhand von Zertifikaten</a> authentifiziert.</li>
</ul>
</li>
<li>die Pr&uuml;fung der Identit&auml;t einer Zertifikats-beantragenden Stelle und die Ausstellung eines Zertifikats f&uuml;r eine beantragende Stelle.
<ul>
<li>Daf&uuml;r sind Zertifizierungsstellen</a> zust&auml;ndig.</li>
</ul>
</li>
<li>die Sperrung von nicht mehr ben&ouml;tigten oder kompromittierten Zertifikaten.
<ul>
<li>Die Veranlassung einer Zertifikatssperrung bei der Zertifizierungsstelle ist Aufgabe der Zertifikatsbesitzer.</li>
<li>Die Aufnahme gesperrter Zertifikate in Zertifikats-Sperrlisten ist Aufgabe der Zertifizierungsstellen.</li>
<li>Das IAM f&uuml;r Beh&ouml;rden beachtet Eintr&auml;ge in Zertifikats-Sperrlisten bei der Pr&uuml;fung der G&uuml;ltigkeit von Zertifikaten.</li>
</ul>
</li>
<li>die abstrakte Berechtigungspr&uuml;fung gem&auml;&szlig; &sect;&sect; 7 (2) und 12 (4) IDNrG.
<ul>
<li>F&uuml;r deren Veranlassung sind die Sicheren Anschlussknoten der <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Data Consumer</a> zust&auml;ndig.</li>
<li>F&uuml;r deren Durchf&uuml;hrung ist die <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Vermittlungsstelle</a> zust&auml;ndig.</li>
<li>F&uuml;r die Pr&uuml;fung des Vorliegens einer abstrakten Berechtigung sind dieSicheren Anschlussknoten der&nbsp;<a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Data Provider</a> zust&auml;ndig.</li>
</ul>
</li>
<li>einen besonderen Umgang mit unberechtigt oder fehlerhaft agierenden Teilnehmern.
<ul>
<li>Das IAM f&uuml;r Beh&ouml;rden l&auml;sst nur eine Nutzung durch Besitzer g&uuml;ltiger Zertifikate</a> zu.</li>
<li>Die Pr&uuml;fung n&ouml;tiger Rollen f&uuml;r Zugriffe auf Ressourcen ist Sache der Ressourcen</a>.</li>
</ul>
</li>
<li>die Zuordnung von Rollen zu ressourcenspezifischen Zugriffsberechtigungen.
<ul>
<li>Das ist Aufgabe der Ressourcen.</li>
</ul>
</li>
<li>die Zuordnung und den Abruf von Identit&auml;tsdaten und Rollen von Personen.
<ul>
<li>Antragstellende werden &uuml;ber Nutzerkonten gem. &sect; 2 (5) OZG zur Kommunikation mit &ouml;ffentlichen Stellen berechtigt.</li>
<li>Sachbearbeitende werden spezifisch f&uuml;r den Zugriff auf Fachverfahren berechtigt.</li>
</ul>
</li>
<li>den Abruf eindeutiger Identifikatoren von nat&uuml;rlichen Personen (IDNr) und Unternehmen (beWiNr).
<ul>
<li>Diese Identifikatoren werden von den Ressourcen</a> IDM f&uuml;r Personen bzw. IDM f&uuml;r Unternehmen abgerufen.</li>
</ul>
</li>
<li>die Zuordnung und den Abruf von Identit&auml;tsdaten und Rollen von Verfahren anderer EU-Mitgliedstaaten im Rahmen des EU-OOTS.
<ul>
<li>Die zur Anbindung an das EU-OOTS vorgesehenen&nbsp;<a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Intermedi&auml;ren Plattformen</a> werden im IAM f&uuml;r Beh&ouml;rden als IT-Komponenten verwaltet.</li>
</ul>
</li>
<li>die Protokollierung von Nachweisabrufen gem&auml;&szlig; &sect; 9 IDNrG und von sonstigen personenbezogenen Daten&uuml;bermittlungen gem&auml;&szlig; &sect; 76 BDSG.
<ul>
<li>Daf&uuml;r sind die <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Data Consumer</a> und <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Data Provider</a> zust&auml;ndig.</li>
</ul>
</li>
</ul>

## Kontextabgrenzung

### Fachlicher Kontext

<p>Das IAM f&uuml;r Beh&ouml;rden ist der zentrale Berechtigungsdienst f&uuml;r das Nationale Once-Only Technical System (NOOTS). Eine dar&uuml;berhinausgehende Verwendung ist nicht Teil der Betrachtungen dieses Konzepts.</p>
<p>Das IAM f&uuml;r Beh&ouml;rden pflegt die Verwaltungsbereiche, Beh&ouml;rdenfunktionen und Rechtsnormen, die zur fachlichen Zuordnung &ouml;ffentlicher Stellen n&ouml;tig sind und dadurch Teil von deren Identit&auml;tsdaten werden. Die in Zugriffstoken enthaltenen fachlichen Zuordnungen (Beh&ouml;rdenfunktion und Verwaltungsbereich) k&ouml;nnen durch Ressourcen ausgewertet und f&uuml;r ihre Zwecke verwendet werden. Die Vermittlungsstelle verwendet sie zur Pr&uuml;fung der abstrakten Berechtigung nach &sect;&sect; 7 (2), 12(4) IDNrG, um der unerw&uuml;nschten Bildung von Pers&ouml;nlichkeitsprofilen entgegenzuwirken. Aus demselben Grund wird die Zust&auml;ndigkeit <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Intermedi&auml;rer Plattformen</a> nach Verwaltungsbereichen aufgeteilt.</p>
<p>Das IAM f&uuml;r Beh&ouml;rden schafft mit abrufbaren Zugriffstoken und den darin enthaltenen Rollen die Voraussetzung f&uuml;r den autorisierten Zugriff von IT-Komponenten auf Ressourcen. Die Ressourcen sind daf&uuml;r verantwortlich,</p>
<ul>
<li>Zugriffe auf die von ihnen daf&uuml;r vorgesehenen Rollen zu beschr&auml;nken,</li>
<li>die vorgesehenen Rollen mit den entsprechenden Berechtigungen f&uuml;r den technischen Zugriff zu verkn&uuml;pfen,</li>
<li>Zugriffstoken hinsichtlich G&uuml;ltigkeitszeitraum und Siegel zu validieren,</li>
<li>die darin enthaltenen Rollen mit den f&uuml;r den Zugriff n&ouml;tigen Rollen abzugleichen und</li>
<li>den Zugriff bei Entsprechung zu gew&auml;hren (Autorisierung) oder andernfalls abzulehnen.</li>
</ul>

### Technischer Kontext

<p>Das IAM f&uuml;r Beh&ouml;rden ist unabh&auml;ngig von anderen technischen Systemen. Es wird vielmehr von anderen technischen Systemen als zentraler Berechtigungsdienst genutzt.</p>

## Anforderungen

<p>Die Anforderungen an das IAM f&uuml;r Beh&ouml;rden werden in funktionale und nichtfunktionale Anforderungen unterschieden. Sie ber&uuml;cksichtigen die anwendbaren Bestandteile der nachfolgenden Technischen Richtlinien des Bundesamts f&uuml;r Sicherheit in der Informationstechnik und sind auf das Vertrauensniveau "substantiell" ausgerichtet. "Substantiell" bedeutet, dass Software-Token gem. Unterkap. 4.2 der TR-03107-1 f&uuml;r private Schl&uuml;ssel von Zertifikaten zul&auml;ssig sind.</p>
<p><strong>Tab. 4: BSI-Richtlinien</strong></p>
<table>
<tbody>
<tr>
<th>Nummer</th>
<th>Richtlinie</th>
<th>Teil</th>
<th>Quelle</th>
<th>Anwendbare Bestandteile</th>
</tr>
<tr>
<td>TR-02103</td>
<td>
<p>X.509 Zertifikate und Zertifizierungspfadvalidierung</p>
</td>
<td>&nbsp;</td>
<td><a href="https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr02103/tr02103_node.html">[SQ-08]</a></td>
<td>alle</td>
</tr>
<tr>
<td>TR-03107-1</td>
<td>Elektronische Identit&auml;ten und Vertrauensdienste im E-Government</td>
<td>Teil 1: Vertrauensniveaus und Mechanismen</td>
<td><a href="https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03107/TR-03107_node.html">[SQ-07]</a></td>
<td>Kap. 1 - 6, Unterkap. 10.2 und 10.6</td>
</tr>
<tr>
<td>TR-03116-4</td>
<td>
<p>Kryptographische Vorgaben f&uuml;r Projekte der Bundesregierung</p>
</td>
<td>
<p>Teil 4: Kommunikationsverfahren in Anwendungen</p>
</td>
<td>
<p><a href="https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03116/TR-03116_node.html">[SQ-10]</a></p>
</td>
<td>Kap. 1 - 2, Unterkap. 6.1</td>
</tr>
</tbody>
</table>

### Funktionale Anforderungen

<p>Das IAM f&uuml;r Beh&ouml;rden muss die nachfolgenden funktionalen Anforderungen erf&uuml;llen. Die Ma&szlig;nahmen zur Umsetzung der Anforderungen werden nachfolgend beschrieben.</p>
<p><span style="color: #000000;"><strong>Tab. 5: Funktionale Anforderungen</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">Nr.</span></th>
<th>
<p><span style="color: #000000;">Anforderungen</span></p>
</th>
<th>
<p><span style="color: #000000;">Abgedeckte Ziele</span></p>
</th>
<th>
<p><span style="color: #000000;">Beschreibung</span></p>
</th>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-808</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS die Nutzung seiner Prozesse durch Besitzer g&uuml;ltiger und zul&auml;ssiger elektronischer Identit&auml;ten sicherstellen.</span></p>
</td>
<td>
<p><span style="color: #000000;">3, 6</span></p>
</td>
<td>
<p><span style="color: #000000;">Prozesse werden von Akteuren und IT-Komponenten genutzt. Ein Prozess darf nur genutzt werden, wenn dabei ein g&uuml;ltiges Zertifikat als elektronische Identit&auml;t verwendet und der Besitz des zugeh&ouml;rigen privaten Schl&uuml;ssels bewiesen wird. Das Zertifikat muss au&szlig;erdem zul&auml;ssig sein, also auf eine vom IAM f&uuml;r Beh&ouml;rden vorgesehene Wurzelzertifizierungsstelle zur&uuml;ckgef&uuml;hrt werden k&ouml;nnen.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1139</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS die Unterscheidung elektronischer Identit&auml;ten &ouml;ffentlicher Stellen von denen sonstiger Stellen sicherstellen.</span></p>
</td>
<td>
<p><span style="color: #000000;">3</span></p>
</td>
<td>
<p><span style="color: #000000;">Die meisten Pflege- und Registrierungsprozesse sind &ouml;ffentlichen Stellen vorbehalten. Zudem m&uuml;ssen jeder IT-Komponente eine &ouml;ffentliche Stelle als fachverantwortliche Stelle und eine sonstige Stelle als betriebsverantwortliche Stelle zugeordnet werden. Das IAM f&uuml;r Beh&ouml;rden muss elektronische Identit&auml;ten entsprechend unterscheiden k&ouml;nnen. Die pflegende Stelle muss dazu jede zul&auml;ssige Wurzelzertifizierungsstelle entsprechend klassifizieren (s.u.).</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1140</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS die Nutzung seiner Prozesse auf daf&uuml;r Berechtigte beschr&auml;nken.</span></td>
<td><span style="color: #000000;">7</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden muss Missbrauch und unberechtigte Einsicht verhindern, insbesondere dass sonstige Stellen Prozesse &ouml;ffentlicher Stellen nutzen.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-988</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS der pflegenden Stelle die Pflege von Wurzelzertifizierungsstellen, Verwaltungsbereichen, Rollen und Teilnahmearten erm&ouml;glichen.</span></td>
<td>
<p><span style="color: #000000;">5, 13</span></p>
</td>
<td><span style="color: #000000;">Die pflegende Stelle ist f&uuml;r die fachlichen und betrieblichen Grundlagen des IAM f&uuml;r Beh&ouml;rden verantwortlich. Sie pflegt die zul&auml;ssigen Wurzelzertifizierungsstellen samt Klassifikation (&ouml;ffentliche/sonstige Stellen), die von der Bundesregierung festgelegten Verwaltungsbereiche, die mit den Ressourcen abgestimmten Rollen f&uuml;r Ressourcenzugriffe und die Teilnahmearten zur B&uuml;ndelung von Rollen nach fachlichem Zweck der Teilnehmer.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1027</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS &ouml;ffentlichen Stellen die Pflege von Beh&ouml;rdenfunktionen und deren Zuordnung zu Verwaltungsbereichen erm&ouml;glichen.</span></td>
<td>
<p><span style="color: #000000;">4, 13</span></p>
</td>
<td><span style="color: #000000;">&Ouml;ffentliche Stellen sind f&uuml;r die fachlichen Zuordnungen von sich selbst und ihren IT-Komponenten verantwortlich und m&uuml;ssen die Grundlagen entsprechender Zuordnungen pflegen k&ouml;nnen.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1025</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS &ouml;ffentlichen Stellen die Registrierung als fachverantwortliche Stelle oder als Fachaufsicht erm&ouml;glichen und dabei die Zuordnung zu Beh&ouml;rdenfunktionen sicherstellen.</span></td>
<td>
<p><span style="color: #000000;">1, 4, 8, 9</span></p>
</td>
<td><span style="color: #000000;">Jede fachverantwortliche Stelle und jede Fachaufsicht muss mindestens einer Beh&ouml;rdenfunktion zugeordnet sein, damit sie die jeweiligen Prozesse nutzen kann. Eine Registrierung als fachverantwortliche Stelle oder Fachaufsicht ist &ouml;ffentlichen Stellen vorbehalten.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-987</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS sonstigen Stellen die Registrierung als betriebsverantwortliche Stelle erm&ouml;glichen.</span></td>
<td>
<p><span style="color: #000000;">1, 9</span></p>
</td>
<td><span style="color: #000000;">Sonstige Stellen k&ouml;nnen sich ausschlie&szlig;lich als betriebsverantwortliche Stelle registrieren, um als solche (a) einer zu registrierenden IT-Komponente zugeordnet werden zu k&ouml;nnen oder (b) deren Registrierung selbst zu veranlassen. Die fachverantwortliche Stelle muss die Registrierung der IT-Komponente veranlasst haben (a) oder best&auml;tigen (b).</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-967</span></td>
<td><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS fach- und betriebsverantwortlichen Stellen die einvernehmliche Registrierung einer IT-Komponente erm&ouml;glichen.</span></td>
<td><span style="color: #000000;">10</span></td>
<td><span style="color: #000000;">Die Registrierung einer IT-Komponente ist einvernehmlich, wenn eine der beiden verantwortlichen Stellen die Registrierung veranlasst und die andere verantwortliche Stelle die Registrierung best&auml;tigt hat. Ohne Registrierungsbest&auml;tigung wird eine IT-Komponente vom IAM f&uuml;r Beh&ouml;rden nicht authentifiziert und kann daher kein Zugriffstoken abrufen. Unbest&auml;tigte Registrierungen werden nach Fristablauf gel&ouml;scht.</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1141</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS die Zuordnung jeder IT-Komponente zu ihrer fachverantwortlichen Stelle, zu ihrer betriebsverantwortlichen Stelle, zu einer Beh&ouml;rdenfunktion ihrer fachverantwortlichen Stelle und zu zweckgem&auml;&szlig;en Rollen sicherstellen.</span></p>
</td>
<td>
<p><span style="color: #000000;">1, 5, 11, 12</span></p>
</td>
<td>
<p><span style="color: #000000;">Diese Zuordnungen werden im Betrieb zu verschiedenen Zwecken genutzt. Die fachverantwortliche Beh&ouml;rde gibt die m&ouml;glichen Beh&ouml;rdenfunktionen der IT-Komponente vor. Die festgelegte Beh&ouml;rdenfunktion der IT-Komponente bestimmt deren Fachzugeh&ouml;rigkeit und kann bspw. zur abstrakten Berechtigungspr&uuml;fung (s. Vermittlungsstelle) genutzt werden. Die zugeordneten Rollen erm&ouml;glichen der IT-Komponente entsprechende Ressourcenzugriffe. Das Zertifikat der betriebsverantwortlichen Stelle (und der zugeh&ouml;rige private Schl&uuml;ssel) ist f&uuml;r den Abruf des Zugriffstokens der IT-Komponente n&ouml;tig.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-966</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS einer authentifizierten IT-Komponente den Abruf ihrer Identit&auml;tsdaten und Rollen mit befristeter Integrit&auml;t erm&ouml;glichen.</span></p>
<p><span style="color: #000000;">&nbsp;</span></p>
</td>
<td>
<p><span style="color: #000000;">2, 6</span></p>
<p><span style="color: #000000;">&nbsp;</span></p>
</td>
<td>
<p><span style="color: #000000;">Eine IT-Komponente kann ihre Identit&auml;tsdaten und Rollen als Zugriffstoken abrufen. Damit die Vertrauensw&uuml;rdigkeit der enthaltenen Daten erhalten bleibt und von Ressourcen gepr&uuml;ft werden kann, versieht das IAM f&uuml;r Beh&ouml;rden das Zugriffstoken mit einem G&uuml;ltigkeitszeitraum und siegelt es mit dem daf&uuml;r vorgesehenen Zertifikat.</span></p>
<p><span style="color: #000000;">&nbsp;</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1031</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS den Abruf des Zertifikats erm&ouml;glichen, mit dem es Zugriffstoken siegelt.</span></p>
</td>
<td>
<p><span style="color: #000000;">2, 13</span></p>
</td>
<td>
<p><span style="color: #000000;">Ressourcen k&ouml;nnen die Integrit&auml;t gesiegelter Zugriffstoken nur anhand des Zertifikats pr&uuml;fen, das zur Siegelung verwendet wurde. Es muss daher bereitgestellt werden.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-951</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS berechtigten Stellen den Abruf von Beh&ouml;rdenfunktionen und Verwaltungsbereichen zur Nachnutzung erm&ouml;glichen.</span></p>
</td>
<td>
<p><span style="color: #000000;">14</span></p>
</td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden pflegt mit Beh&ouml;rdenfunktionen und Verwaltungsbereichen Daten von &uuml;bergreifender Relevanz. Berechtigte Stellen mit Bedarf an diesen Daten k&ouml;nnen sie vom IAM f&uuml;r Beh&ouml;rden abrufen und nachnutzen.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1033</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS die Erneuerung von Zertifikaten erm&ouml;glichen und Verantwortliche rechtzeitig &uuml;ber den G&uuml;ltigkeitsablauf hinterlegter Zertifikate informieren.</span></p>
</td>
<td>
<p><span style="color: #000000;">13</span></p>
</td>
<td>
<p><span style="color: #000000;">F&uuml;r den Betrieb des IAM f&uuml;r Beh&ouml;rden sind die hinterlegten Zertifikate von entscheidender Bedeutung. Abgelaufene Zertifikate verhindern die Prozessnutzung durch Zertifikatsinhaber, die Authentifizierung von IT-Komponenten betroffener verantwortlicher Stellen und (bei abgelaufenen Zertifikaten von Wurzelzertifizierungsstellen) die Zul&auml;ssigkeitspr&uuml;fung von Zertifikaten. Die rechtzeitige Zertifikatserneuerung ist daher essentiell f&uuml;r den reibungslosen Betrieb und muss entsprechend unterst&uuml;tzt werden.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1142</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS verwaltungsbereichs&uuml;bergreifende Zuordnungen von fach- und betriebsverantwortlichen Stellen und IT-Komponenten verhindern.</span></p>
</td>
<td>
<p><span style="color: #000000;">4</span></p>
</td>
<td>
<p><span style="color: #000000;">Eine fachverantwortliche Stelle darf sich nur Beh&ouml;rdenfunktionen aus demselben Verwaltungsbereich zuordnen. Eine betriebsverantwortliche Stelle darf nur IT-Komponenten zugeordnet werden, deren Beh&ouml;rdenfunktionen demselben Verwaltungsbereich zugeordnet sind. Dadurch werden eine Missbrauch erm&ouml;glichende Kumulation von Zust&auml;ndigkeiten und Daten verhindert und Auswirkungen abgelaufener und gesperrter Zertifikate begrenzt.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1143</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS die Fachaufsicht &uuml;ber Zuordnungen von Beh&ouml;rdenfunktionen, Verwaltungsbereichen und Rollen zu fachverantwortlichen Stellen und IT-Komponenten unterst&uuml;tzen.</span></p>
</td>
<td>
<p><span style="color: #000000;">4, 8</span></p>
</td>
<td>
<p><span style="color: #000000;">Fachaufsichten pr&uuml;fen im eigenen Ermessen die Korrektheit fachlicher Zuordnungen in ihrer Zust&auml;ndigkeit. Sie ben&ouml;tigen dazu entsprechende Sichten auf den Datenbestand, die ihnen das IAM f&uuml;r Beh&ouml;rden bereitstellen muss.</span></p>
</td>
</tr>
</tbody>
</table>

### Nicht-funktionale Anforderungen

<p>Die nichtfunktionalen Anforderungen (sonstige Qualit&auml;tskriterien entsprechend ISO 25010) an das IAM f&uuml;r Beh&ouml;rden sind in der nachfolgenden Tab. 5 aufgef&uuml;hrt.</p>
<p><span style="color: #000000;"><strong>Tab. 6: Nichtfunktionale Anforderungen</strong></span></p>
<table>
<thead>
<tr>
<th>
<p><span style="color: #000000;">Nr.</span></p>
</th>
<th>
<p><span style="color: #000000;">Anforderung</span></p>
</th>
<th>
<p><span style="color: #000000;">Qualit&auml;tskriterium (ISO 25010)</span></p>
</th>
</tr>
</thead>
<tbody>
<tr>
<td><span style="color: #000000;">NOOTS-1118</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS eine hohe Anzahl von Abrufen von Zugriffstoken pro Sekunde verarbeiten k&ouml;nnen.</span></p>
</td>
<td>
<p><span style="color: #000000;">Performanz - Kapazit&auml;ten</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1119</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS den Abruf eines Zugriffstokens innerhalb von einer Sekunde beantworten k&ouml;nnen.</span></p>
</td>
<td>
<p><span style="color: #000000;">Performanz - Zeitverhalten</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-953</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS relevante Ereignisse in einer technisch auswertbaren Weise protokollieren.</span></p>
</td>
<td><span style="color: #000000;">Sicherheit</span></td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1120</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS relevante Ereignisse ohne Zeitverzug protokollieren k&ouml;nnen.</span></p>
</td>
<td>
<p><span style="color: #000000;">Performanz - Kapazit&auml;ten</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1121</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS 24/7 zu 99,9% verf&uuml;gbar sein.</span></p>
</td>
<td>
<p><span style="color: #000000;">Sicherheit - Verf&uuml;gbarkeit</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1122</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS 24/7 mit mindestens zwei &ouml;rtlich getrennten und inhaltlich redundant gehaltenen Instanzen betrieben werden.</span></p>
</td>
<td>
<p><span style="color: #000000;">Sicherheit - Wiederherstellbarkeit</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">NOOTS-1123</span></td>
<td>
<p><span style="color: #000000;">Das IAM f&uuml;r Beh&ouml;rden MUSS 24/7 bei einem Ausfall innerhalb von wenigen Stunden wiederhergestellt werden.</span></p>
</td>
<td>
<p><span style="color: #000000;">Sicherheit - Wiederherstellbarkeit</span></p>
</td>
</tr>
</tbody>
</table>

## L&ouml;sungsstrategie

<p>Das IAM f&uuml;r Beh&ouml;rden wird als zentraler Berechtigungsdienst f&uuml;r nationale Beh&ouml;rden, deren Dienstleister und den von diesen betriebenen IT-Komponenten umgesetzt.</p>
<p>Das n&ouml;tige Vertrauen von Beteiligten in das IAM f&uuml;r Beh&ouml;rden wird durch folgende Ma&szlig;nahmen sichergestellt:</p>
<ul>
<li>Die Identit&auml;t jedes Akteurs ist von einer unabh&auml;ngigen vertrauensw&uuml;rdigen Instanz durch ein Zertifikat</a> best&auml;tigt.</li>
<li>Zertifikate &ouml;ffentlicher Stellen werden eindeutig von Zertifikaten sonstiger Stellen unterschieden.</li>
<li>IAM-Prozesse k&ouml;nnen nur auf der Grundlage von Zertifikaten genutzt werden.</li>
<li>Fach- und betriebsverantwortliche Stellen registrieren selbst&auml;ndig ihre IT-Komponenten. Die jeweils andere (fach-/betriebs-)verantwortliche Stelle best&auml;tigt die Registrierung.</li>
<li>Jede IT-Komponente ist einer Beh&ouml;rdenfunktion und dar&uuml;ber einem Verwaltungsbereich zugeordnet.</li>
<li>Beh&ouml;rdenfunktionen, Verwaltungsbereiche und deren Verkn&uuml;pfungen und Rechtsgrundlagen werden zentral im IAM f&uuml;r Beh&ouml;rden gepflegt.</li>
<li>Jeder IT-Komponente sind nur die Rollen ihrer Teilnahmeart zugeordnet.</li>
<li>Fachaufsichten pr&uuml;fen Beh&ouml;rdenfunktionen, Verwaltungsbereiche und Rechtsnormen, deren Zuordnungen untereinander und zu IT-Komponenten und fachverantwortlichen Stellen sowie Teilnahmearten, deren Rollen und Zuordnungen im eigenen Ermessen.</li>
</ul>
<p>Die m&ouml;glichst effiziente Nutzung des IAM f&uuml;r Beh&ouml;rden wird durch folgende Ma&szlig;nahmen sichergestellt:</p>
<ul>
<li>Akteure ben&ouml;tigen nur ein g&uuml;ltiges Zertifikat, das zur gew&uuml;nschten Registrierung geeignet ist.</li>
<li>Die Registrierung eines Akteurs muss nicht von anderen Akteuren best&auml;tigt werden.</li>
<li>IT-Komponenten werden mit ihrer KomponentenID und dem Zertifikat ihrer betriebsverantwortlichen Stelle sicher authentifiziert.</li>
<li>&Ouml;ffentliche Stellen registrieren Beh&ouml;rdenfunktionen und Rechtsnormen nach ihrem Bedarf.</li>
<li>Die pflegende Stelle ordnet den Teilnahmearten die n&ouml;tigen Rollen f&uuml;r Ressourcen-Zugriffe nach fachlichem Bedarf zu.</li>
<li>Die pflegende Stelle sorgt f&uuml;r die Verf&uuml;gbarkeit des IAM f&uuml;r Beh&ouml;rden samt seiner grundlegenden Daten und Funktionen und &uuml;berwacht dessen Betrieb.</li>
<li>Ressourcen melden der pflegenden Stelle die f&uuml;r den jeweiligen Zugriff n&ouml;tigen Rollen.</li>
</ul>

## Bausteinsicht

### Datenmodell

<p>Die vom IAM f&uuml;r Beh&ouml;rden vorzuhaltenden Informationen werden nachfolgend als Entit&auml;ten dargestellt. Die Assoziationen der Entit&auml;ten und deren Kardinalit&auml;ten bilden die Vorgaben dieses Konzepts ab. Die Beisteuerung der Daten durch verschiedene Akteure und sonstige Stellen ist farblich gekennzeichnet (siehe "Datenherkunft" in der Abbildung).</p>
<p><a href="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/1.png"><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/1.png" alt="" /></a></p>
<p><strong>Abb. 1: Datenhaltung im IAM f&uuml;r Beh&ouml;rden</strong></p>

### Facharchitektur

#### Umgang mit Zertifikaten

<p>Im IAM f&uuml;r Beh&ouml;rden d&uuml;rfen ausschlie&szlig;lich Funktionszertifikate gem&auml;&szlig; BSI TR-03107</a> (nachfolgend kurz: Zertifikate) verwendet werden. Zertifikate m&uuml;ssen g&uuml;ltig</a>, eindeutig</a> und ihre Herkunft</a> zul&auml;ssig sein. Zertifikate werden bei der Registrierung von Akteuren</a> im IAM f&uuml;r Beh&ouml;rden hinterlegt und m&uuml;ssen&nbsp;rechtzeitig erneuert</a> werden.</p>
<p>Nachfolgend werden die statischen Aspekte von Zertifikaten beschrieben, die&nbsp;im Rahmen von Prozessen</a> zu beachten sind.</p>

##### Herkunft von Zertifikaten

<p>Zul&auml;ssige Herk&uuml;nfte werden &uuml;ber Wurzelzertifizierungsstellen</a> festgelegt (siehe&nbsp;NOOTS-988</a>). Wurzelzertifizierungsstellen werden gekennzeichnet mit dem Herkunftskennzeichen</p>
<ul>
<li>ROOT_CA_BEH&Ouml;RDEN: zul&auml;ssige Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen<br />oder</li>
<li>ROOT_CA_SONST: zul&auml;ssige Wurzelzertifizierungsstelle f&uuml;r Zertifikate sonstiger Stellen.</li>
</ul>
<p>Jede zul&auml;ssige Wurzelzertifizierungsstelle hat eines dieser Herkunftskennzeichen.</p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Die Nutzung von Prozessen &ouml;ffentlicher Stellen,</a> von Prozessen fachverantwortlicher Stellen</a> und von Prozessen der Fachaufsicht</a> ist auf Akteure beschr&auml;nkt, deren Zertifikate auf eine Wurzelzertifizierungsstelle mit dem Herkunftskennzeichen ROOT_CA_BEH&Ouml;RDEN zur&uuml;ckgef&uuml;hrt werden k&ouml;nnen.</em></li>
</ul>

##### G&uuml;ltigkeit von Zertifikaten

<p>Ein Zertifikat ist g&uuml;ltig, wenn alle folgenden Bedingungen eingehalten werden:</p>
<ol>
<li>Das Zertifikat entspricht dem Standard RFC 5280 und enth&auml;lt mindestens folgende Inhalte:
<ol>
<li>Identifikationsmerkmal (Seriennummer),</li>
<li>&Ouml;ffentlicher Schl&uuml;ssel,</li>
<li>Zertifizierungsstelle,</li>
<li>Verweis auf Zertifikats-Sperrlisten (Certificate Revocation List / CRL) der Zertifizierungsstelle oder einer von ihr beauftragten Stelle (Certificate Registration Authority / RA),</li>
<li>G&uuml;ltigkeitszeitraum,</li>
<li>Verwendungszwecke (z.B. Authentisierung, Siegelung, Verschl&uuml;sselung),</li>
<li>Organisation des Zertifikatsinhabers,</li>
<li>Zertifikatsinhaber (Funktionstr&auml;ger in der Organisation),</li>
<li>Anschrift des Zertifikatsinhabers,</li>
<li>E-Mail-Adresse des Zertifikatsinhabers oder des Schl&uuml;sselbeauftragten (Funktions-Postfach).</li>
</ol>
</li>
<li>Die Herkunft des Zertifikats</a> ist &uuml;ber seine Zertifizierungsstelle (1.c) bis zu einer zul&auml;ssigen Wurzelzertifizierungsstelle sicher nachvollziehbar.</li>
<li>Das Zertifikat ist in keiner referenzierten Zertifikats-Sperrliste (1.d) aufgef&uuml;hrt.</li>
<li>Der Pr&uuml;fzeitpunkt liegt im G&uuml;ltigkeitszeitraum (1.e) des Zertifikats.</li>
<li>Die Authentisierung ist einer der Verwendungszwecke (1.f) des Zertifikats.</li>
</ol>

##### Eindeutigkeit von Zertifikaten

<p>Ein Zertifikat ist eindeutig, wenn es von allen im IAM f&uuml;r Beh&ouml;rden hinterlegten Zertifikaten verschieden ist.</p>

##### Erneuerung von Zertifikaten

<p>Zertifikate werden bei der Registrierung von Akteuren</a> im IAM f&uuml;r Beh&ouml;rden hinterlegt. Der Zertifikatsinhaber (Akteur) kann das Zertifikat unter folgenden Voraussetzungen erneuern:</p>
<ul>
<li>Das bisherige und das neue Zertifikat sind (noch) g&uuml;ltig</a>.</li>
<li>Das neue Zertifikat ist eindeutig</a>.</li>
<li>Das neue Zertifikat kann auf eine Wurzelzertifizierungsstelle mit demselben Herkunftskennzeichen</a> zur&uuml;ckgef&uuml;hrt werden wie das bisherige Zertifikat.</li>
</ul>
<p>Ist der G&uuml;ltigkeitszeitraum eines hinterlegten Zertifikats bereits abgelaufen,</p>
<ul>
<li>kann der Zertifikatsinhaber kein erneuertes Zertifikat hinterlegen und</li>
<li>k&ouml;nnen dem Zertifikatsinhaber zugeordnete IT-Komponenten nicht authentifiziert</a> werden.</li>
</ul>

#### Fachliche Zuordnung

<p>Fachliche Zuordnungen von Fachaufsichten, fachverantwortlichen Stellen und IT-Komponenten sind n&ouml;tig, um ihnen die fachlich vorgesehenen Wirkm&ouml;glichkeiten einr&auml;umen und sie darauf begrenzen zu k&ouml;nnen. Die genannten Akteure sollen bei der Nutzung der Prozesse des IAM f&uuml;r Beh&ouml;rden gem&auml;&szlig; ihrer Fachlichkeit unterst&uuml;tzt und zugleich auf ihre Fachlichkeit begrenzt werden. IT-Komponenten sollen auf Ressourcenzugriffe in der fachlich vorgesehenen Weise beschr&auml;nkt werden k&ouml;nnen. Fachliche Zuordnungen sind daher Teil der Identit&auml;tsdaten der genannten Akteure und Bestandteil der Zugriffstoken von IT-Komponenten. Dadurch k&ouml;nnen Ressourcen fachliche Zugeh&ouml;rigkeiten anfragender IT-Komponenten pr&uuml;fen (z.B. im Rahmen der abstrakten Berechtigungspr&uuml;fung), protokollieren und Berechtigungen oder sonstige Ma&szlig;nahmen daraus ableiten.</p>
<p>Fachliche Zuordnungen umfassen</p>
<ul>
<li>die Fachgrundlage</a>, bestehend aus Beh&ouml;rdenfunktionen, Rechtsnormen, Verwaltungsbereichen und deren Zuordnungen,</li>
<li>die zugeordnete Fachlichkeit</a>, bestehend aus den Zuordnungen von Beh&ouml;rdenfunktionen zu Fachaufsichten, fachverantwortlichen Stellen und IT-Komponenten, sowie</li>
<li>die Eindeutigkeit des Verwaltungsbereichs</a> von registrierten Akteuren und IT-Komponenten.</li>
</ul>
<p>Nachfolgend werden die Regeln der fachlichen Zuordnung beschrieben. Das IAM f&uuml;r Beh&ouml;rden stellt die Einhaltung dieser Regeln bei der Registrierung und Pflege von Akteuren und IT-Komponenten sicher (siehe entsprechende Prozesse in der Laufzeitsicht</a>) und erm&ouml;glicht den Abruf der Fachgrundlage (siehe Beh&ouml;rdenfunktionen abrufen</a>).</p>

##### Fachgrundlage

<p>F&uuml;r die Pflege der Fachgrundlage gelten folgende Regeln:</p>
<ol>
<li>Jeder Verwaltungsbereich hat
<ul>
<li>genau eine Langbezeichnung gem&auml;&szlig; der Rechtsverordnung der Bundesregierung nach &sect; 12 (1) IDNrG und</li>
<li>genau eine eindeutige Kurzbezeichnung</li>
</ul>
</li>
<li>Jede Rechtsnorm hat
<ul>
<li>genau eine Langbezeichnung (Schreibweise gem&auml;&szlig; Ver&ouml;ffentlichung, z.B. Stra&szlig;enverkehrsgesetz),</li>
<li>genau eine Kurzbezeichnung (Abk&uuml;rzung gem&auml;&szlig; Ver&ouml;ffentlichung, z.B. StVG) und</li>
<li>m&ouml;glichst einen Verweis auf einen entsprechenden Eintrag in einem Rechtsnormen-Verzeichnis (z.B. Verweis auf Eintrag in <a href="http://www.gesetze-im-internet.de">www.gesetze-im-internet.de</a>).</li>
</ul>
</li>
<li>Jede Beh&ouml;rdenfunktion hat
<ul>
<li>genau einen zugeordneten Verwaltungsbereich,</li>
<li>genau eine zugeordnete Rechtsnorm (z.B. Stra&szlig;enverkehrsgesetz),</li>
<li>genau eine Fundstelle in der Rechtsnorm, an der die Beh&ouml;rdenfunktion begr&uuml;ndet wird (Artikel, Paragraph, Absatz o.&auml;. der Rechtsnorm, z.B. &sect; 1 Absatz 1) und</li>
<li>genau eine Bezeichnung im Singular gem&auml;&szlig; der Fundstelle in der Rechtsnorm (z.B. Zulassungsbeh&ouml;rde).</li>
</ul>
</li>
</ol>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Eine Beh&ouml;rdenfunktion hat eine Rechtsgrundlage. Eine Rechtsgrundlage besteht aus einer Rechtsnorm und der begr&uuml;ndenden Fundstelle in der Rechtsnorm. Eine Rechtsnorm kann begr&uuml;ndende Fundstellen mehrerer Beh&ouml;rdenfunktionen enthalten.Rechtsnormen werden daher separat von Beh&ouml;rdenfunktionen gepflegt und von diesen als Teil ihrer Rechtsgrundlage referenziert.</em></li>
<li><em>Rechtsnormen sollen auf entsprechende Eintr&auml;ge in Rechtsnorm-Verzeichnissen (bspw. mit <a href="http://www.gesetze-im-internet.de">www.gesetze-im-internet.de</a>) verweisen, um eine einfache Einsichtnahme der Rechtsgrundlage zu erm&ouml;glichen. Perspektivisch ist eine noch st&auml;rkere Verzahnung bis hin zur &Uuml;bernahme solcher Eintr&auml;ge in das IAM f&uuml;r Beh&ouml;rden vorstellbar.<br /></em></li>
</ul>

##### Zugeordnete Fachlichkeit

<p>F&uuml;r zugeordnete Fachlichkeit gelten folgende Regeln:</p>
<ol>
<li>Jede als fachverantwortliche Stelle oder als Fachaufsicht registrierte &ouml;ffentliche Stelle ist mindestens einer Beh&ouml;rdenfunktionzugeordnet.</li>
<li>Jede IT-Komponente ist genau einer der Beh&ouml;rdenfunktionen ihrer fachverantwortlichen Stelle zugeordnet.</li>
<li>Jede betriebsverantwortliche Stelle ist der Fachlichkeit ihrer IT-Komponenten zugeordnet.</li>
</ol>

##### Eindeutigkeit des Verwaltungsbereichs

<p>F&uuml;r die Eindeutigkeit des Verwaltungsbereichs gelten folgende Regeln:</p>
<ol>
<li>Alle einer Fachaufsicht zugeordneten Beh&ouml;rdenfunktionen geh&ouml;ren zu demselben Verwaltungsbereich.</li>
<li>Alle einer fachverantwortlichen Stelle zugeordneten Beh&ouml;rdenfunktionen geh&ouml;ren zu demselben Verwaltungsbereich.</li>
<li>Alle einer betriebsverantwortlichen Stelle zugeordneten IT-Komponenten haben eine Beh&ouml;rdenfunktion aus demselben Verwaltungsbereich.</li>
</ol>

#### Zweckgem&auml;&szlig;e Rollen

<p>IT-Komponenten ben&ouml;tigen zur Erf&uuml;llung ihres Zwecks die Berechtigung zum Zugriff auf bestimmte Ressourcen. Die pflegende Stelle stimmt mit den Ressourcen zweckgem&auml;&szlig;e Zugriffe und daf&uuml;r n&ouml;tige Rollen ab. Rollen sind also das gemeinsame &bdquo;Vokabular&ldquo; von Ressourcen und IAM f&uuml;r Beh&ouml;rden zur Regelung zweckgem&auml;&szlig;er Ressourcenzugriffe von IT-Komponenten.</p>
<p>Die pflegende Stelle b&uuml;ndelt die Rollen</a> f&uuml;r einen bestimmten fachlichen Zweck zu einer Teilnahmeart</a>. Das IAM f&uuml;r Beh&ouml;rden stellt bei der Registrierung und Pflege von IT-Komponenten deren Zuordnung zu einer Teilnahmeart</a> sicher.</p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Ressourcen definieren grunds&auml;tzlich selbst&auml;ndig, welche Rollen anfragende IT-Komponenten ben&ouml;tigen, um von der Ressource zur jeweiligen Nutzung autorisiert zu werden, und melden der pflegenden Stelle die definierten Rollen. </em></li>
<li><em>Rollen regeln Ressourcen-Zugriffe ohne Bezug auf m&ouml;gliche Inhalte, die dar&uuml;ber abgerufen werden k&ouml;nnen. Rollen dienen insbesondere nicht zur Steuerung abrufbarer Nachweise. Das ist Aufgabe der Vermittlungsstelle.</em></li>
<li><em>Bei mehreren Ressourcen derselben Teilnahmeart (z.B. bei Data Providern) muss eine Rolle f&uuml;r alle Ressourcen dieser Teilnahmeart dieselbe Bedeutung und Wirkung haben. Die pflegende Stelle koordiniert die Festlegung solcher Rollen.</em></li>
<li><em>Rollen werden IT-Komponenten stets &uuml;ber ihre Teilnahmeart, also mittelbar zugeordnet.</em></li>
<li><em>Teilnahmearten erm&ouml;glichen eine bedarfsgerechte Unterscheidung von Zwecken und die gleichartige Zuordnung n&ouml;tiger Rollen zu IT-Komponenten mit demselben Zweck.</em></li>
<li><em>Teilnahmearten und Rollen sind Bestandteile von Zugriffstoken</a>.</em></li>
</ul>

##### Rollen

<p>Eine Rolle hat</p>
<ul>
<li>einen Bezeichner entsprechend der Namenskonvention der pflegenden Stelle,</li>
<li>einen Berechtigungszweck und</li>
<li>eine oder mehrere berechtigende Ressourcen.</li>
</ul>
<p><span style="color: #000000;"><strong>Tab. 7: Beispiele f&uuml;r Rollen</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">Rolle</span></th>
<th><span style="color: #000000;">Berechtigt zu</span></th>
<th><span style="color: #000000;">Bei Ressource</span></th>
</tr>
<tr>
<td><span style="color: #000000;">RDN.NACHWEISANGEBOT</span></td>
<td><span style="color: #000000;">Abruf von Nachweisangeboten</span></td>
<td><span style="color: #000000;">Registerdatennavigation</span></td>
</tr>
<tr>
<td><span style="color: #000000;">RDN.VERBINDUNGSPARAMETER</span></td>
<td><span style="color: #000000;">Abruf von Verbindungsparametern</span></td>
<td><span style="color: #000000;">Registerdatennavigation</span></td>
</tr>
<tr>
<td><span style="color: #000000;">RDN.IDNRDP</span></td>
<td><span style="color: #000000;">Abruf aller Data Provider, die Nachweisabrufe mit IDNr anbieten</span></td>
<td><span style="color: #000000;">Registerdatennavigation</span></td>
</tr>
<tr>
<td><span style="color: #000000;">IDMP.IDNR</span></td>
<td><span style="color: #000000;">Abruf von Identifikationsnummern nat&uuml;rlicher Personen</span></td>
<td><span style="color: #000000;">Identity Management f&uuml;r Personen</span></td>
</tr>
<tr>
<td><span style="color: #000000;">IDMU.BEWINR</span></td>
<td><span style="color: #000000;">Abruf von bundeseinheitlichen Wirtschaftsnummern von Unternehmen</span></td>
<td><span style="color: #000000;">Identity Management f&uuml;r Unternehmen</span></td>
</tr>
<tr>
<td><span style="color: #000000;">VS.ABSTRAKTEBERECHTIGUNG</span></td>
<td><span style="color: #000000;">Ausl&ouml;sung abstrakter Berechtigungspr&uuml;fungen</span></td>
<td><span style="color: #000000;">Vermittlungsstelle</span></td>
</tr>
<tr>
<td><span style="color: #000000;">DP.NACHWEIS</span></td>
<td><span style="color: #000000;">Abruf von Nachweisen aus Deutschland</span></td>
<td><span style="color: #000000;">Data Provider</span></td>
</tr>
<tr>
<td><span style="color: #000000;">DP.PROTOKOLLDATEN</span></td>
<td><span style="color: #000000;">Abruf von Protokolldaten zu Nachweisabrufen mit IDNr</span></td>
<td><span style="color: #000000;">Data Provider</span></td>
</tr>
<tr>
<td><span style="color: #000000;">IP.NACHWEIS</span></td>
<td><span style="color: #000000;">Abruf von Nachweisen aus einem EU-Mitgliedstaat</span></td>
<td><span style="color: #000000;">Intermedi&auml;re Plattform</span></td>
</tr>
</tbody>
</table>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Die angef&uuml;hrten Beispiele haben keinen normativen Charakter, sondern dienen nur zur Veranschaulichung des Konzepts.</em></li>
<li><em>Die pflegende Stelle und die Ressourcen k&ouml;nnen beliebig davon abweichende Rollen und Zwecke festlegen.</em></li>
</ul>

##### Teilnahmearten

<p>Eine Teilnahmeart hat</p>
<ul>
<li>einen zweckgem&auml;&szlig;en Bezeichner und</li>
<li>mindestens eine zugeordnete Rolle.</a></li>
</ul>
<p><span style="color: #000000;"><strong>Tab. 8: Beispiele f&uuml;r Teilnahmearten unter Nutzung der o.g. Beispiel-Rollen</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">Teilnahmeart</span></th>
<th><span style="color: #000000;">Rollen</span></th>
<th><span style="color: #000000;">Zuordnung zu</span></th>
<th><span style="color: #000000;">Hinweise</span></th>
</tr>
<tr>
<td><span style="color: #000000;">DC_ONLINEDIENST</span></td>
<td>
<ul>
<li><span style="color: #000000;">RDN.NACHWEISANGEBOT</span></li>
<li><span style="color: #000000;">RDN.VERBINDUNGSPARAMETER</span></li>
<li><span style="color: #000000;">IDMP.IDNR</span></li>
<li><span style="color: #000000;">IDMU.BEWINR</span></li>
<li><span style="color: #000000;">VS.ABSTRAKTEBERECHTIGUNG</span></li>
<li><span style="color: #000000;">DP.NACHWEIS</span></li>
<li><span style="color: #000000;">IP.NACHWEIS</span></li>
</ul>
</td>
<td><span style="color: #000000;">Onlinedienst als Data Consumer</span></td>
<td>
<p><span style="color: #000000;">Ein Onlinedienst kann interaktive Abrufe von Nachweisen aus Deutschland und EU-Mitgliedstaaten veranlassen. Er ben&ouml;tigt die aufgef&uuml;hrten Rollen zur Vorbereitung und Durchf&uuml;hrung solcher Nachweisabrufe, um</span></p>
<ul>
<li><span style="color: #000000;">von der Registerdatennavigation Nachweisangebote und Verbindungsparameter abzurufen,</span></li>
<li><span style="color: #000000;">ggf. die IDNr abzurufen, wenn das Nachweissubjekt eine nat&uuml;rliche Person ist,</span></li>
<li><span style="color: #000000;">ggf. die beWiNr abzurufen, wenn das Nachweissubjekt ein Unternehmen ist,</span></li>
<li><span style="color: #000000;">die abstrakte Berechtigungspr&uuml;fung der jeweiligen Daten&uuml;bermittlung auszul&ouml;sen und</span></li>
<li><span style="color: #000000;">ggf. den Nachweis aus Deutschland abzurufen oder</span></li>
<li><span style="color: #000000;">ggf. den Nachweis &uuml;ber eine Intermedi&auml;re Plattform aus einem EU-Mitgliedstaat abzurufen.</span></li>
</ul>
</td>
</tr>
<tr>
<td><span style="color: #000000;">DC_FACHVERFAHREN</span></td>
<td>
<ul>
<li><span style="color: #000000;">RDN.NACHWEISANGEBOT</span></li>
<li><span style="color: #000000;">RDN.VERBINDUNGSPARAMETER</span></li>
<li><span style="color: #000000;">IDMP.IDNR</span></li>
<li><span style="color: #000000;">IDMU.BEWINR</span></li>
<li><span style="color: #000000;">VS.ABSTRAKTEBERECHTIGUNG</span></li>
<li><span style="color: #000000;">DP.NACHWEIS</span></li>
</ul>
</td>
<td><span style="color: #000000;">Fachverfahren als Data Consumer</span></td>
<td>
<p><span style="color: #000000;">Ein Fachverfahren kann nicht-interaktive Abrufe von Nachweisen aus Deutschland veranlassen. Es ben&ouml;tigt die aufgef&uuml;hrten Rollen zur Vorbereitung und Durchf&uuml;hrung solcher Nachweisabrufe.</span></p>
<p><span style="color: #000000;">Im Vergleich zur Teilnahmeart DC_ONLINEDIENST fehlt die Rolle IP.NACHWEIS, da Abrufe aus EU-Mitgliedstaaten nur interaktiv m&ouml;glich sind.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">DC_IP</span></td>
<td>
<ul>
<li><span style="color: #000000;">RDN.NACHWEISANGEBOT</span></li>
<li><span style="color: #000000;">RDN.VERBINDUNGSPARAMETER</span></li>
<li><span style="color: #000000;">IDMU.BEWINR</span></li>
<li><span style="color: #000000;">DP.NACHWEIS</span></li>
</ul>
</td>
<td><span style="color: #000000;">Intermedi&auml;re Plattform als</span><br /><span style="color: #000000;">Data Consumer /</span><br /><span style="color: #000000;">Evidence Requester Proxy</span></td>
<td>
<p><span style="color: #000000;">Eine Intermedi&auml;re Plattform kann stellvertretend f&uuml;r Verfahren anderer EU-Mitgliedstaaten Nachweise aus Deutschland abrufen. Sie ben&ouml;tigt die aufgef&uuml;hrten Rollen zur Vorbereitung und Durchf&uuml;hrung solcher Nachweisabrufe.</span></p>
<p><span style="color: #000000;">Im Vergleich zur Teilnahmeart DC_ONLINEDIENST fehlen die Rollen IDMP.IDNR, VS.ABSTRAKTEBERECHTIGUNG und IP.NACHWEIS,da sie f&uuml;r Nachweise zu nat&uuml;rlichen Personen keine IDNr abrufen darf und ein Aufruf einer weiteren Intermedi&auml;ren Plattform unzul&auml;ssig ist.</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">DSC</span></td>
<td>
<ul>
<li><span style="color: #000000;">RDN.VERBINDUNGSPARAMETER</span></li>
<li><span style="color: #000000;">RDN.IDNRDP</span></li>
<li><span style="color: #000000;">DP.PROTOKOLLDATEN</span></li>
</ul>
</td>
<td><span style="color: #000000;">Datenschutzcockpit</span></td>
<td><span style="color: #000000;">Das Datenschutzcockpit muss Nachweisabrufe mit einer bestimmten IDNr anzeigen und dazu entsprechende Protokolldaten von allen Data Providern anfordern, die Nachweisabrufe mit IDNr anbieten. Diese Data Provider und deren Verbindungsparameter ermittelt sie &uuml;ber die Registerdatennavigation und ruft von ihnen die ben&ouml;tigten Protokolldaten ab.</span></td>
</tr>
</tbody>
</table>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Die angef&uuml;hrten Beispiele haben keinen normativen Charakter, sondern dienen nur zur Veranschaulichung des Konzepts.</em></li>
<li><em>Die pflegende Stelle kann beliebig davon abweichende Teilnahmearten und zugeordnete Rollen festlegen.</em></li>
</ul>

##### Zuordnung von Teilnahmearten

<p>Das IAM f&uuml;r Beh&ouml;rden stellt sicher, dass jede IT-Komponente einer Teilnahmeart zugeordnet ist. Dadurch verf&uuml;gt jede IT-Komponente &uuml;ber alle f&uuml;r ihren Zweck n&ouml;tigen Rollen.</p>
<p>F&uuml;r Teilnehmer zum Abruf von Nachweisen (Data Consumer) k&ouml;nnten beispielsweise folgende Teilnahmearten festgelegt werden:</p>
<ul>
<li>DC_Onlinedienst f&uuml;r Nachweisabrufe durch nationale Onlinedienste / Antragsverfahren (siehe <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Anwendungsf&auml;lle 1a, 1b und 4 in der High-Level-Architecture</a>)</li>
<li>DC_Fachverfahren f&uuml;r Nachweisabrufe durch ein nationales Fachverfahren (siehe&nbsp;<a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Anwendungsfall 2 in der High-Level-Architecture</a>)</li>
<li>DC_Intermedi&auml;rePlattform f&uuml;r Nachweisabrufe durch einen EU-Mitgliedstaat &uuml;ber eine Intermedi&auml;re Plattform (siehe <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Anwendungsfall 3 in der High-Level-Architecture</a>)</li>
</ul>

#### Registrierung von Akteuren

<p>Jeder Akteur muss sich mit einem&nbsp g&uuml;ltigen</a> und eindeutigen</a> Zertifikat zul&auml;ssiger&nbsp;Herkunft</a> im IAM f&uuml;r Beh&ouml;rden registrieren. Die Registrierung eines Akteurs wird abgewiesen, wenn eine dieser Bedingungen verletzt ist. Bei erfolgreicher Registrierung hinterlegt das IAM f&uuml;r Beh&ouml;rden das verwendete Zertifikat zum Akteur. Der Akteur ist daf&uuml;r verantwortlich, das hinterlegte Zertifikat&nbsp;rechtzeitig zu erneuern</a>.</p>
<p>Die M&ouml;glichkeiten zur Registrierung eines Akteurs unterscheiden sich je nach der&nbsp;Herkunft des Zertifikats</a>:</p>
<ul>
<li>&Ouml;ffentliche Stellen haben ein Zertifikat, das auf eine ROOT_CA_BEH&Ouml;RDEN zur&uuml;ckgef&uuml;hrt werden kann. Sie k&ouml;nnen sichunter Angabe einer Beh&ouml;rdenfunktion als fachverantwortliche Stelle</a> oder als Fachaufsicht</a> registrieren (vgl. Fachliche Zuordnung</a>).</li>
<li>Sonstige Stellen haben ein Zertifikat, das auf eine ROOT_CA_SONST zur&uuml;ckgef&uuml;hrt werden kann. Sie k&ouml;nnen sich als betriebsverantwortliche Stelle</a> registrieren.</li>
</ul>
<p>Die pflegende Stelle registriert sich nicht als Akteur. Vielmehr wird das Zertifikat der pflegenden Stelle in einem manuellen Prozess im IAM f&uuml;r Beh&ouml;rden hinterlegt.</p>

#### Registrierung von IT-Komponenten

<p>Eine IT-Komponente kann&nbsp;von einer fachverantwortlichen Stelle</a> oder von einer betriebsverantwortlichen Stelle</a> unter Angabe der jeweils anderen Stelle, der Teilnahmeart und einer Beh&ouml;rdenfunktion der fachverantwortlichen Stelle registriert werden. Die jeweils andere Stelle muss die Registrierung best&auml;tigen</a>. Die Prozesse von IT-Komponenten</a> k&ouml;nnen nur von best&auml;tigten IT-Komponenten genutzt werden.</p>

## Laufzeitsicht

<p>Nachfolgend werden die Prozesse der IT-Komponenten und der Akteure beschrieben, die vom IAM f&uuml;r Beh&ouml;rden angeboten werden.</p>
<p>Die Prozesse von IT-Komponenten</a> werden von IT-Komponenten genutzt, die auf andere IT-Komponenten zugreifen wollen, insbesondere von Data Consumern, die f&uuml;r Zugriffe auf NOOTS-Komponenten und Data Provider von diesen autorisiert werden m&uuml;ssen. Sie rufen dazu&nbsp;Zugriffstoken</a> ab, die die n&ouml;tigen Rollen f&uuml;r den jeweiligen Zugriff enthalten.</p>
<p>Alle &uuml;brigen Prozesse sind Pflegeprozesse der jeweiligen Akteure.</p>

### Nutzungsbedingungen

<p>F&uuml;r die Nutzung aller vom IAM f&uuml;r Beh&ouml;rden angebotenen Prozesse gilt:</p>
<ul>
<li>Der Prozessnutzer muss den Prozess unter Verwendung eines g&uuml;ltigen Zertifikats</a> aufrufen.</li>
<li>Das IAM f&uuml;r Beh&ouml;rden muss die G&uuml;ltigkeit des verwendeten Zertifikats pr&uuml;fen.</li>
<li>Das IAM f&uuml;r Beh&ouml;rden muss vom Prozessnutzer einen Beweis des Besitzes des privaten Schl&uuml;ssels des verwendeten Zertifikats verlangen, z.B. &uuml;ber ein kryptographisches Challenge-Response-Verfahren.</li>
<li>Das IAM f&uuml;r Beh&ouml;rden muss den Prozess abbrechen, wenn ein ung&uuml;ltiges Zertifikat verwendet oder dessen Besitz nicht bewiesen wird (siehe NOOTS-808</a>).</li>
</ul>
<p>Bei Einhaltung dieser Nutzungsbedingungen und der jeweiligen Prozess-Bedingungen f&uuml;hrt das IAM f&uuml;r Beh&ouml;rden den genutzten Prozess durch und best&auml;tigt dessen Durchf&uuml;hrung.</p>

### Prozesse von IT-Komponenten

<p>IT-Komponenten k&ouml;nnen die nachfolgenden Schnittstellenmethoden des IAM f&uuml;r Beh&ouml;rden aufrufen, um insbesondere ihr Zugriffstoken f&uuml;r den Zugriff auf andere IT-Komponenten abzurufen.</p>
<p>Das IAM f&uuml;r Beh&ouml;rden bietet IT-Komponenten folgende Schnittstellenmethoden an:</p>
<p><span style="color: #000000;"><strong>Tab. 9: Schnittstellenmethoden</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">&nbsp;</span></th>
<th><span style="color: #000000;">Methode</span></th>
<th><span style="color: #000000;">Zweck</span></th>
<th><span style="color: #000000;">Aufrufparameter</span></th>
<th><span style="color: #000000;">Aufgerufene Prozesse</span></th>
<th><span style="color: #000000;">Ergebnis (bei erfolgreicher Authentifizierung und erfolgreichem Folgeprozess)</span></th>
</tr>
<tr>
<td><span style="color: #000000;">1</span></td>
<td><span style="color: #000000;">getZugriffstoken</span></td>
<td><span style="color: #000000;">Abruf eines Zugriffstokens</span></td>
<td>
<ul>
<li><span style="color: #000000;">Komponenten-ID</span></li>
<li><span style="color: #000000;">Zertifikat der betriebsverantwortlichen Stelle</span></li>
</ul>
</td>
<td>
<ul>
<li><span style="color: #000000;">IT-Komponente authentifizieren</span></li>
<li><span style="color: #000000;">Zugriffstoken abrufen</span></li>
</ul>
</td>
<td><span style="color: #000000;">Zugriffstoken mit den nachfolgend beschriebenen Bestandteilen</span></td>
</tr>
<tr>
<td><span style="color: #000000;">2</span></td>
<td><span style="color: #000000;">getSiegelzertifikat</span></td>
<td><span style="color: #000000;">Abruf des zur Siegelung von Zugriffstoken verwendeten Zertifikats</span></td>
<td>
<ul>
<li><span style="color: #000000;">Komponenten-ID</span></li>
<li><span style="color: #000000;">Zertifikat der betriebsverantwortlichen Stelle</span></li>
</ul>
</td>
<td>
<ul>
<li><span style="color: #000000;">IT-Komponente authentifizieren</span></li>
<li><span style="color: #000000;">Siegelzertifikat abrufen</span></li>
</ul>
</td>
<td><span style="color: #000000;">Elektronisches Zertifikat gem&auml;&szlig; Standard RFC 5280</span></td>
</tr>
<tr>
<td><span style="color: #000000;">3</span></td>
<td><span style="color: #000000;">getBeh&ouml;rdenfunktionen</span></td>
<td><span style="color: #000000;">Abruf der Fachgrundlage zur Nachnutzung in anderen Systemen &ouml;ffentlicher Stellen</span></td>
<td>
<ul>
<li><span style="color: #000000;">Komponenten-ID</span></li>
<li><span style="color: #000000;">Zertifikat der betriebsverantwortlichen Stelle</span></li>
</ul>
</td>
<td>
<ul>
<li><span style="color: #000000;">IT-Komponente authentifizieren</span></li>
<li><span style="color: #000000;">Beh&ouml;rdenfunktionen abrufen</span></li>
</ul>
</td>
<td><span style="color: #000000;">Liste aller Beh&ouml;rdenfunktionen des IAM f&uuml;r Beh&ouml;rden, je mit Rechtsgrundlage und zugeordnetem Verwaltungsbereich</span></td>
</tr>
</tbody>
</table>

#### Zugriffstoken abrufen

<p>Eine authentifizierte IT-Komponente kann jederzeit vom IAM f&uuml;r Beh&ouml;rden ein Zugriffstoken f&uuml;r den Zugriff auf andere IT-Komponenten abrufen (siehe NOOTS-958</a>).</p>
<p>Ein Zugriffstoken besteht aus folgenden Bestandteilen:</p>
<p><span style="color: #000000;"><strong>Tab. 10: Bestandteile von Zugriffstoken</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">&nbsp;</span></th>
<th><span style="color: #000000;">Bestandteil</span></th>
<th><span style="color: #000000;">Beschreibung</span></th>
</tr>
<tr>
<td><span style="color: #000000;">1</span></td>
<td>
<p><span style="color: #000000;">Komponenten-ID</span></p>
</td>
<td><span style="color: #000000;">Vom IAM f&uuml;r Beh&ouml;rden bei der Registrierung vergebene ID der IT-Komponente</span></td>
</tr>
<tr>
<td><span style="color: #000000;">2</span></td>
<td>
<p><span style="color: #000000;">Bezeichnung</span></p>
</td>
<td><span style="color: #000000;">Bezeichnung der IT-Komponente</span></td>
</tr>
<tr>
<td><span style="color: #000000;">3</span></td>
<td>
<p><span style="color: #000000;">Beh&ouml;rdenfunktion</span></p>
</td>
<td><span style="color: #000000;">Beh&ouml;rdenfunktion der IT-Komponente</span></td>
</tr>
<tr>
<td><span style="color: #000000;">4</span></td>
<td>
<p><span style="color: #000000;">Verwaltungsbereich</span></p>
</td>
<td><span style="color: #000000;">Verwaltungsbereich der Beh&ouml;rdenfunktion der IT-Komponente</span></td>
</tr>
<tr>
<td><span style="color: #000000;">5</span></td>
<td>
<p><span style="color: #000000;">Teilnahmeart</span></p>
</td>
<td><span style="color: #000000;">Teilnahmeart der IT-Komponente</span></td>
</tr>
<tr>
<td><span style="color: #000000;">6</span></td>
<td>
<p><span style="color: #000000;">Rollen</span></p>
</td>
<td><span style="color: #000000;">Menge von Rollen gem&auml;&szlig; der Teilnahmeart</span></td>
</tr>
<tr>
<td><span style="color: #000000;">7</span></td>
<td><span style="color: #000000;">FV-ID</span></td>
<td><span style="color: #000000;">Vom IAM f&uuml;r Beh&ouml;rden bei der Registrierung vergebene ID der fachverantwortlichen Stelle</span></td>
</tr>
<tr>
<td><span style="color: #000000;">8</span></td>
<td><span style="color: #000000;">FV-Organisation</span></td>
<td><span style="color: #000000;">Organisation der fachverantwortlichen Stelle laut deren Zertifikat</span></td>
</tr>
<tr>
<td><span style="color: #000000;">9</span></td>
<td><span style="color: #000000;">FV-Funktionstr&auml;ger</span></td>
<td><span style="color: #000000;">Funktionstr&auml;ger der fachverantwortlichen Stelle laut deren Zertifikat</span></td>
</tr>
<tr>
<td><span style="color: #000000;">10</span></td>
<td><span style="color: #000000;">FV-Anschrift</span></td>
<td><span style="color: #000000;">Anschrift der fachverantwortlichen Stelle laut deren Zertifikat</span></td>
</tr>
<tr>
<td><span style="color: #000000;">11</span></td>
<td><span style="color: #000000;">BV-ID</span></td>
<td><span style="color: #000000;">Vom IAM f&uuml;r Beh&ouml;rden bei der Registrierung vergebene ID der betriebsverantwortlichen Stelle</span></td>
</tr>
<tr>
<td><span style="color: #000000;">12</span></td>
<td><span style="color: #000000;">BV-Organisation</span></td>
<td><span style="color: #000000;">Organisation der betriebsverantwortlichen Stelle laut deren Zertifikat</span></td>
</tr>
<tr>
<td><span style="color: #000000;">13</span></td>
<td><span style="color: #000000;">BV-Funktionstr&auml;ger</span></td>
<td><span style="color: #000000;">Funktionstr&auml;ger der betriebsverantwortlichen Stelle laut deren Zertifikat</span></td>
</tr>
<tr>
<td><span style="color: #000000;">14</span></td>
<td><span style="color: #000000;">BV-Anschrift</span></td>
<td><span style="color: #000000;">Anschrift der betriebsverantwortlichen Stelle laut deren Zertifikat</span></td>
</tr>
</tbody>
</table>
<p>Das IAM f&uuml;r Beh&ouml;rden</p>
<ul>
<li>ermittelt die <strong>Werte</strong> f&uuml;r die Bestandteile des Zugriffstoken anhand der Identit&auml;tsdaten und der direkt oder &uuml;ber die Teilnahmeart zugeordneten Rollen der IT-Komponente,</li>
<li>ermittelt den <strong>G&uuml;ltigkeitszeitraum</strong> der ermittelten Werte mit sekundengenauen Zeitpunkten f&uuml;r
<ul>
<li>Beginn (Erstellungszeitpunkt) und</li>
<li>Ende (Erstellungszeitpunkt zuz&uuml;glich der G&uuml;ltigkeitsdauer</a> von Zugriffstoken, siehe&nbsp;NOOTS-1031</a>),</li>
</ul>
</li>
<li><strong>strukturiert</strong> die ermittelten Daten als Zugriffstoken (siehe Ausblick</a>),</li>
<li><strong>siegelt</strong> das Zugriffstoken mit dem privaten Schl&uuml;ssel seines Siegel-Zertifikats</a> (siehe NOOTS-1031</a>) und</li>
<li>stellt der IT-Komponente das gesiegelte Zugriffstoken zur Verf&uuml;gung.</li>
</ul>

#### Siegelzertifikat abrufen

<p>Eine authentifizierte</a> IT-Komponente kann jederzeit das vom IAM f&uuml;r Beh&ouml;rden zur Siegelung von Zugriffstoken verwendete Zertifikat abrufen.</p>

#### Beh&ouml;rdenfunktionen abrufen

<p>Eine authentifizierte</a> IT-Komponente kann jederzeit die im IAM f&uuml;r Beh&ouml;rden verwalteten Beh&ouml;rdenfunktionen samt jeweiliger Rechtsgrundlage und zugeordnetem Verwaltungsbereiche &uuml;ber folgende Schnittstelle abrufen:</p>

#### IT-Komponente authentifizieren

<p>Eine IT-Komponente wird vom IAM f&uuml;r Beh&ouml;rden unter folgenden Bedingungen authentifiziert (siehe NOOTS-966</a>):</p>
<ul>
<li>Die angegebene Komponenten-ID verweist auf eine registrierte IT-Komponente.</li>
<li>Die Registrierung der IT-Komponente ist best&auml;tigt.</li>
<li>Das verwendete Zertifikat ist der betriebsverantwortlichen Stelle der IT-Komponente zugeordnet.</li>
<li>Das Zertifikat der fachverantwortlichen Stelle der IT-Komponente ist g&uuml;ltig (siehe&nbsp;Zertifikate</a>).</li>
</ul>
<p>Liegt eine der Voraussetzungen nicht vor, lehnt das IAM f&uuml;r Beh&ouml;rden die Authentifizierung der IT-Komponente ab.</p>
<p>Dieser Prozess wird nicht &uuml;ber eine Schnittstellenmethode zum Aufruf durch IT-Komponenten angeboten. Er wird vom IAM f&uuml;r Beh&ouml;rden vor der Durchf&uuml;hrung eines per Schnittstellenmethode aufgerufenen Prozesses verpflichtend als Vor-Prozess durchgef&uuml;hrt. Das stellt sicher, dass nur authentifizierte IT-Komponenten die per Schnittstelle angebotenen Prozesse nutzen k&ouml;nnen.</p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Die IT-Komponente wird von der betriebsverantwortlichen Stelle betrieben. Daher wird deren Zertifikat zur Authentisierung verwendet. Es muss den Nutzungsbedingungen</a> gen&uuml;gen, also u.a. ebenfalls g&uuml;ltig sein.</em></li>
</ul>

### Prozesse &ouml;ffentlicher Stellen

<p>&Ouml;ffentliche Stellen k&ouml;nnen die nachfolgenden Prozesse des IAM f&uuml;r Beh&ouml;rden nutzen, um Rechtsnormen oder Beh&ouml;rdenfunktionen als Teil der Fachgrundlage</a> zu pflegen oder um sich als fachverantwortliche Stelle oder als Fachaufsicht zu registrieren.</p>
<p>Nach der Registrierung k&ouml;nnen sie die entsprechenden Prozesse nutzen (siehe&nbsp;Prozesse fachverantwortlicher Stellen</a> und Prozesse der Fachaufsicht</a>). Die Nutzung der Prozesse zur Pflege von Rechtsnormen und Beh&ouml;rdenfunktionen bleibt &ouml;ffentlichen Stellen auch nach einer Registrierung erhalten.</p>

#### Rechtsnormen pflegen

<p>Eine&nbsp;&ouml;ffentliche Stelle</a> kann jederzeit bei berechtigtem Interesse eine Rechtsnorm unter folgenden Bedingungen anlegen:</p>
<ul>
<li>Das verwendete Zertifikat kann auf eine&nbsp;Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen</a> zur&uuml;ckgef&uuml;hrt werden.</li>
<li>Schreibweise und Kurzbezeichnung der Rechtsnorm sind angegeben und noch nicht im IAM f&uuml;r Beh&ouml;rden vorhanden (Eindeutigkeit im Namensraum des IAM f&uuml;r Beh&ouml;rden).</li>
<li>Der Verweis auf den entsprechenden Eintrag in einer Rechtsnormen-Datenbank ist angegeben und technisch g&uuml;ltig (URL kann aufgel&ouml;st werden).</li>
</ul>
<p>Eine &ouml;ffentliche Stelle kann jederzeit eine vorhandene Rechtsnorm unter Einhaltung der vorgenannten Bedingungen &auml;ndern.</p>
<p><em>Hinweise: </em></p>
<ul>
<li><em>Die Fachaufsicht kontrolliert</a> die Rechtsnormen der von ihr beaufsichtigten Beh&ouml;rdenfunktionen.</em></li>
</ul>

#### Beh&ouml;rdenfunktionen pflegen

<p>Eine&nbsp;&ouml;ffentliche Stelle</a> kann jederzeit bei berechtigtem Interesse eine Beh&ouml;rdenfunktion unter folgenden Bedingungen anlegen:</p>
<ul>
<li>Das verwendete Zertifikat kann auf eine&nbsp;Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen</a> zur&uuml;ckgef&uuml;hrt werden.</li>
<li>Die Bezeichnung der Beh&ouml;rdenfunktion sowie deren Rechtsnorm, Fundstelle und Verwaltungsbereich sind angegeben.</li>
<li>Der Rechtsnorm ist keine Beh&ouml;rdenfunktion mit derselben Bezeichnung zugeordnet (Eindeutigkeit im Namensraum der Rechtsnorm).</li>
<li>Die Kombination von Fundstelle und Rechtsnorm (Rechtsgrundlage) wird von keiner anderen Beh&ouml;rdenfunktion verwendet (fachliche Eindeutigkeit).</li>
<li>Der Verwaltungsbereich der Beh&ouml;rdenfunktion ist einer der Verwaltungsbereiche der Rechtsnorm oder die Rechtsnorm ist keinem Verwaltungsbereich zugeordnet.</li>
</ul>
<p>Eine &ouml;ffentliche Stelle kann jederzeit eine vorhandene Beh&ouml;rdenfunktion unter Einhaltung der vorgenannten Bedingungen &auml;ndern.</p>
<p><em>Hinweise: </em></p>
<ul>
<li><em>Die &ouml;ffentliche Stelle kann eine noch nicht im IAM f&uuml;r Beh&ouml;rden vorhandene </em><em>Rechtsnorm anlegen</a></em><em>.</em></li>
<li><em>Die &ouml;ffentliche Stelle muss ggf. Anpassungen an Berechtigungsregeln bei der <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">Vermittlungsstelle</a> veranlassen, um aufgrund der registrierten Beh&ouml;rdenfunktion Nachweise abrufen zu d&uuml;rfen.</em></li>
<li><em>Die Fachaufsicht kontrolliert</a> die von ihr beaufsichtigten Beh&ouml;rdenfunktionen.</em></li>
</ul>

#### Als fachverantwortliche Stelle registrieren

<p>Eine &ouml;ffentliche Stelle</a> kann sich jederzeit im IAM f&uuml;r Beh&ouml;rden als fachverantwortliche Stelle unter folgenden Bedingungen registrieren:</p>
<ul>
<li>Das verwendete Zertifikat kann auf eine&nbsp;Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen</a> zur&uuml;ckgef&uuml;hrt werden.</li>
<li>Mit dem verwendeten Zertifikat wurde noch kein Akteur registriert.</li>
<li>Eine Beh&ouml;rdenfunktion ist zugeordnet.</li>
</ul>
<p><a href="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/2.png"><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/2.png" alt="" /></a></p>
<p><strong>Abb. 2: Als fachverantwortliche Stelle registrieren</strong></p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Die &ouml;ffentliche Stelle kann eine noch nicht im IAM f&uuml;r Beh&ouml;rden vorhandene Beh&ouml;rdenfunktion anlegen</a></em><em>.</em></li>
<li><em>Die Fachaufsicht kontrolliert</a> fachverantwortliche Stellen mit einer von ihr beaufsichtigten Beh&ouml;rdenfunktion.</em></li>
</ul>

#### Als Fachaufsicht registrieren

<p>Eine &ouml;ffentliche Stelle</a> kann sich jederzeit im IAM f&uuml;r Beh&ouml;rden als Fachaufsicht unter folgenden Bedingungen registrieren:</p>
<ul>
<li>Das verwendete Zertifikat kann auf eine&nbsp;Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen</a> zur&uuml;ckgef&uuml;hrt werden.</li>
<li>Mit dem verwendeten Zertifikat wurde noch kein Akteur registriert.</li>
<li>Eine Beh&ouml;rdenfunktion ist zur Aufsicht angegeben.</li>
<li>Die zur Aufsicht angegebene Beh&ouml;rdenfunktion ist keiner anderen Fachaufsicht zugeordnet.</li>
</ul>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Ein Zertifikat darf nur zur Registrierung von genau einem Akteur</a> verwendet werden.</em></li>
<li><em>Die &ouml;ffentliche Stelle kann eine noch nicht im IAM f&uuml;r Beh&ouml;rden vorhandene&nbsp;Beh&ouml;rdenfunktion anlegen</a></em><em>.</em></li>
</ul>

### Prozesse fachverantwortlicher Stellen

<p>Eine fachverantwortliche Stelle kann die nachfolgenden Prozesse des IAM f&uuml;r Beh&ouml;rden nutzen, um eine IT-Komponente zu registrieren, deren Registrierung zu best&auml;tigen oder Daten zu korrigieren.</p>

#### IT-Komponente registrieren (fachverantwortliche Stelle)

<p>Eine fachverantwortliche Stelle kann eine IT-Komponente unter folgenden Bedingungen registrieren (siehe NOOTS-967</a>):</p>
<ul>
<li>Die Bezeichnung der IT-Komponente sowie deren Teilnahmeart, Beh&ouml;rdenfunktion und betriebsverantwortliche Stelle sind angegeben.</li>
<li>Teilnahmeart,Beh&ouml;rdenfunktion undbetriebsverantwortliche Stelle sind im IAM f&uuml;r Beh&ouml;rden registriert.</li>
<li>Die angegebene Beh&ouml;rdenfunktion ist eine der Beh&ouml;rdenfunktionen der fachverantwortlichen Stelle.</li>
<li>Es gibt keine IT-Komponente mit derselben Bezeichnung, die der fachverantwortlichen oder der betriebsverantwortlichen Stelle zugeordnet ist (Eindeutigkeit in den Namensr&auml;umen beider verantwortlicher Stellen).</li>
<li>Die Beh&ouml;rdenfunktionen aller sonstigen der betriebsverantwortlichen Stelle zugeordneten IT-Komponenten sind demselben Verwaltungsbereich wie die fachverantwortliche Stelle zugeordnet (Eindeutigkeit des Verwaltungsbereichs beider verantwortlicher Stellen).</li>
</ul>
<p>Das IAM f&uuml;r Beh&ouml;rden muss der fachverantwortlichen Stelle M&ouml;glichkeiten bieten zur</p>
<ul>
<li>Suche und Auswahl der Teilnahmeart,</li>
<li>Suche und Auswahl der betriebsverantwortlichen Stelle und</li>
<li>zur Auswahl der Beh&ouml;rdenfunktion der IT-Komponente aus den Beh&ouml;rdenfunktionen der fachverantwortlichen Stelle.</li>
</ul>
<p><a href="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/3.png"><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/3.png" alt="" /></a></p>
<p><strong>Abb. 3: IT-Komponente registrieren (fachverantwortliche Stelle)<br /></strong></p>
<p>Das IAM f&uuml;r Beh&ouml;rden f&uuml;hrt bei Einhaltung der Bedingungen folgende Schritte durch:</p>
<ol>
<li>Die IT-Komponente anlegen mit
<ul>
<li>neuer <strong>Komponenten-ID</strong> als eindeutiger und dauerhafter Identifikator im Namensraum des IAM f&uuml;r Beh&ouml;rden,</li>
<li><strong>Bezeichnung,</strong> <strong>Teilnahmeart</strong> und <strong>Beh&ouml;rdenfunktion </strong>aus den Angaben zur Registrierung,</li>
<li>Zuordnung zurregistrierenden <strong>fachverantwortlichen Stelle</strong>,</li>
<li>Zuordnung zur angegebenen <strong>betriebsverantwortlichen Stelle</strong>,</li>
<li>Zustand "<strong>unbest&auml;tigt</strong>" (im Datenmodell</a>: best&auml;tigt = false).</li>
</ul>
</li>
<li>Die Registrierung gegen&uuml;ber der fachverantwortlichen Stelle unter Angabe der Komponenten-ID best&auml;tigen und auf die n&ouml;tige Best&auml;tigung durch die betriebsverantwortliche Stelle innerhalb der vorgegebenen Frist hinweisen.</li>
<li>Die Betriebsverantwortliche Stelle unverz&uuml;glich unter Angabe der Komponenten-ID &uuml;ber die Registrierung informieren und zur Best&auml;tigung innerhalb einer bestimmten Frist auffordern.</li>
</ol>
<p>Eine unbest&auml;tigte IT-Komponente</p>
<ul>
<li>darf vom IAM f&uuml;r Beh&ouml;rden nicht authentifiziert</a> werden.</li>
<li>kann bis zum Ablauf der Best&auml;tigungsfrist durch die betriebsverantwortliche Stelle best&auml;tigt oder abgelehnt werden.</li>
<li>wird nach Ablauf der Best&auml;tigungsfrist aus dem IAM f&uuml;r Beh&ouml;rden gel&ouml;scht.</li>
</ul>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Eine IT-Komponente kann auch von der betriebsverantwortlichen Stelle registriert werden.</em></li>
<li><em>Die pflegende Stelle gibt f&uuml;r die Best&auml;tigungsfrist eine angemessene Zeitspanne</a> vor.</em></li>
</ul>

#### Registrierung einer IT-Komponente best&auml;tigen (fachverantwortliche Stelle)

<p>Eine bei der Registrierung einer IT-Komponente durch eine betriebsverantwortliche Stelle angegebenefachverantwortliche Stelle muss die Registrierung dieser IT-Komponente best&auml;tigen, damit die IT-Komponente verwendet werden kann (siehe NOOTS-1032</a>).</p>
<p><a href="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/4.png"><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/4.png" alt="" /></a></p>
<p><strong>Abb. 4: Registrierung einer IT-Komponente best&auml;tigen (fachverantwortliche Stelle)</strong></p>
<p>Das IAM f&uuml;r Beh&ouml;rden muss einer fachverantwortlichen Stelle die Best&auml;tigung oder Ablehnung der Registrierung von IT-Komponenten erm&ouml;glichen, die ihr als fachverantwortlicher Stelle zugeordnet sind. Es f&uuml;hrt dazu folgende Schritte durch:</p>
<ol>
<li>Unbest&auml;tigte IT-Komponenten mit deren Identit&auml;tsdaten darstellen,</li>
<li>M&ouml;glichkeiten zur Best&auml;tigung oder Ablehnung der Registrierung anbieten.</li>
</ol>
<p>Lehnt die fachverantwortliche Stelle die Registrierung ab, l&ouml;scht das IAM f&uuml;r Beh&ouml;rden die unbest&auml;tigte IT-Komponente und best&auml;tigt dies gegen&uuml;ber der fachverantwortlichen und der betriebsverantwortlichen Stelle.</p>
<p>Best&auml;tigt die fachverantwortliche Stelle die Registrierung, &uuml;berf&uuml;hrt das IAM f&uuml;r Beh&ouml;rden die IT-Komponente in den Zustand "best&auml;tigt" und best&auml;tigt dies gegen&uuml;ber der fachverantwortlichen und der betriebsverantwortlichen Stelle.</p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Das IAM f&uuml;r Beh&ouml;rden soll der fachverantwortlichen Stelle im Rahmen der Registrierungsbest&auml;tigung die &Auml;nderung von Bezeichnung, Beh&ouml;rdenfunktion und Teilnahmeart der IT-Komponente erm&ouml;glichen, um den Aufwand bei Korrekturbedarf zu minimieren.</em></li>
<li><em>Die fachverantwortliche Stelle kann diese Angaben auch nach der Registrierung &auml;ndern</a></em><em>.</em></li>
</ul>

#### Eigenschaften einer IT-Komponente &auml;ndern

<p>Eine fachverantwortliche Stelle kann jederzeit die Bezeichnung, die Beh&ouml;rdenfunktion und die Teilnahmeart einer ihr zugeordneten IT-Komponenteunter folgenden Bedingungen &auml;ndern:</p>
<ul>
<li>Die Bezeichnung der IT-Komponente sowie deren Beh&ouml;rdenfunktion und Teilnahmeart</a> sind angegeben.</li>
<li>Es gibt keine andere IT-Komponente mit dieser Bezeichnung, die der fachverantwortlichen oder der betriebsverantwortlichen Stelle zugeordnet ist (Eindeutigkeit im Namensraum beider verantwortlichen Stellen).</li>
<li>Die angegebene Beh&ouml;rdenfunktion und die angegebene Teilnahmeart sind im IAM f&uuml;r Beh&ouml;rden registriert.</li>
<li>Die angegebene Beh&ouml;rdenfunktion ist eine der Beh&ouml;rdenfunktionen der fachverantwortlichen Stelle.</li>
</ul>

#### Betriebsverantwortliche Stelle einer IT-Komponente &auml;ndern

<p>Eine fachverantwortliche Stelle kann jederzeit die betriebsverantwortliche Stelle einer ihr zugeordneten IT-Komponente unter folgenden Bedingungen &auml;ndern:</p>
<ul>
<li>Eine betriebsverantwortliche Stelle ist angegeben.</li>
<li>Es gibt keine andere IT-Komponente mit diesem Namen, die der betriebsverantwortlichen Stelle zugeordnet ist (Eindeutigkeit im Namensraum verantwortlicher Stellen).</li>
<li>Die Beh&ouml;rdenfunktion der IT-Komponente ist demselben Verwaltungsbereich zugeordnet wie die Beh&ouml;rdenfunktionen aller sonstigen der betriebsverantwortlichen Stelle zugeordneten IT-Komponenten (Eindeutigkeit des Verwaltungsbereichs der betriebsverantwortlichen Stelle).</li>
</ul>
<p>Nach Abschluss der &Auml;nderung muss das IAM f&uuml;r Beh&ouml;rden</p>
<ul>
<li>die IT-Komponente in den Zustand "unbest&auml;tigt" setzen und</li>
<li>den Prozess zur Best&auml;tigung einer registrierten IT-Komponente durch die betriebsverantwortliche Stelle ansto&szlig;en.</li>
</ul>

#### Eigene Beh&ouml;rdenfunktionen &auml;ndern

<p>Eine fachverantwortliche Stelle kann jederzeit ihre zugeordneten Beh&ouml;rdenfunktionen unter folgenden Bedingungen &auml;ndern:</p>
<ul>
<li>Nach Abschluss der &Auml;nderungen gibt es mindestens eine zugeordnete Beh&ouml;rdenfunktion.</li>
<li>Der Verwaltungsbereich der zugeordneten Beh&ouml;rdenfunktionen ist eindeutig</a>.</li>
<li>Alle IT-Komponenten der fachverantwortlichen Stelle bleiben einer der Beh&ouml;rdenfunktionen der fachverantwortlichen Stelle zugeordnet.</li>
</ul>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Mehrere Beh&ouml;rdenfunktionen einer fachverantwortlichen Stelle im selben Verwaltungsbereich sollten zur Abbildung der Realit&auml;t im Kommunalbereich erm&ouml;glicht werden.</em></li>
<li><em>Die Zugeh&ouml;rigkeit einer fachverantwortlichen Stelle zu mehr als einem Verwaltungsbereich widerspricht dem Ansatz der abstrakten Berechtigungspr&uuml;fung beim Nachweisabruf nach &sect; 7 (2) IDNrG zur Vermeidung der Bildung von Pers&ouml;nlichkeitsprofilen.</em></li>
</ul>

### Prozesse betriebsverantwortlicher Stellen

<p>Von &ouml;ffentlichen Stellen zum Betrieb von IT-Komponenten beauftragte sonstige Stellen k&ouml;nnen die nachfolgenden Prozesse des IAM f&uuml;r Beh&ouml;rden nutzen, um sich selbst oder eine IT-Komponente zu registrieren oder die Registrierung einer IT-Komponente zu best&auml;tigen.</p>

#### Als betriebsverantwortliche Stelle registrieren

<p>Eine sonstige Stelle kann sich jederzeit unter folgenden Bedingungen als betriebsverantwortliche Stelle registrieren:</p>
<ul>
<li>Mit dem verwendeten Zertifikat wurde noch kein Akteur registriert.</li>
</ul>
<p><a href="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/5.png"><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images5/media/5.png" alt="" /></a></p>
<p><strong>Abb. 5: Als betriebsverantwortliche Stelle registrieren</strong></p>

#### IT-Komponente registrieren (betriebsverantwortliche Stelle)

<p>Eine betriebsverantwortliche Stelle</a> kann eine IT-Komponente unter folgenden Bedingungen registrieren (siehe NOOTS-967</a>):</p>
<ul>
<li>Die Bedingungen bei der Registrierung einer IT-Komponente durch eine fachverantwortliche Stelle</a> sind analog eingehalten; statt der betriebsverantwortlichen wird die fachverantwortliche Stelle angegeben.</li>
</ul>
<p>Der weitere Ablauf ist analog zur Registrierung einer IT-Komponente durch eine fachverantwortliche Stelle</a> mit der Abweichung, dass die fachverantwortliche Stelle zur Best&auml;tigung der Registrierung aufgefordert wird.</p>

#### Registrierung einer IT-Komponente best&auml;tigen (betriebsverantwortliche Stelle)

<p>Eine bei der Registrierung einer IT-Komponente durch eine fachverantwortliche Stelle</a> angegebene betriebsverantwortliche Stelle muss die Registrierung dieser IT-Komponente best&auml;tigen, damit die IT-Komponente authentifiziert werden kann (siehe NOOTS-1032</a>).</p>
<p>Der Ablauf ist analog zur Best&auml;tigung der Registrierung einer IT-Komponente durch eine fachverantwortliche Stelle</a> mit der Abweichung, dass die betriebsverantwortliche Stelle die Angaben zur IT-Komponente nicht &auml;ndern darf.</p>

### Prozesse der Fachaufsicht

<p>Eine &ouml;ffentliche Stelle</a> kann die nachfolgenden Prozesse nutzen, um sich selbst als Fachaufsicht</a> zu registrieren und um beaufsichtigte Beh&ouml;rdenfunktionen und deren Rechtsnormen zu kontrollieren.</p>

#### Registrierte Beh&ouml;rdenfunktionen einsehen

<p>Die Fachaufsicht</a> kann jederzeit die vorhandenen Beh&ouml;rdenfunktionen einsehen. Das IAM f&uuml;r Beh&ouml;rden zeigt zu jeder Beh&ouml;rdenfunktion die zugeordnete Fachaufsicht an. Eine Beh&ouml;rdenfunktion ohne zugeordnete Fachaufsicht ist unbeaufsichtigt und wird vom IAM f&uuml;r Beh&ouml;rden entsprechend angezeigt.</p>
<p>Stellt die Fachaufsicht eine falsche Zuordnung einer Beh&ouml;rdenfunktion zu sich als Fachaufsicht fest, &auml;ndert sie ihre beaufsichtigten Beh&ouml;rdenfunktionen</a> entsprechend.</p>
<p>Stellt die Fachaufsicht eine falsche Zuordnung einer Beh&ouml;rdenfunktion zu einer anderen Fachaufsicht fest, veranlasst sie au&szlig;erhalb des IAM f&uuml;r Beh&ouml;rden eine entsprechende Korrektur.</p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Die Fachaufsicht kann sich jederzeit eine unbeaufsichtigte Beh&ouml;rdenfunktion zuordnen</a></em><em>.</em></li>
</ul>

#### Beaufsichtigte Beh&ouml;rdenfunktionen &auml;ndern

<p>Die Fachaufsicht</a> kann jederzeit die von ihr beaufsichtigten (also ihr zugeordneten)Beh&ouml;rdenfunktionen unter folgenden Bedingungen &auml;ndern:</p>
<ul>
<li>Mindestens eine Beh&ouml;rdenfunktion ist zur Aufsicht zugeordnet.</li>
<li>Die zugeordneten Beh&ouml;rdenfunktionen sind keiner anderen Fachaufsicht zugeordnet.</li>
<li>Der Verwaltungsbereich der zugeordneten Beh&ouml;rdenfunktionen ist eindeutig</a>.</li>
</ul>

#### Beaufsichtigte Beh&ouml;rdenfunktionen kontrollieren

<p>Die Fachaufsicht</a> kontrolliert Beh&ouml;rdenfunktionen unter ihrer Aufsicht nach folgenden Gesichtspunkten:</p>
<ul>
<li>Alle beaufsichtigten Beh&ouml;rdenfunktionen sind korrekt registriert und dem korrekten Verwaltungsbereich zugeordnet.</li>
<li>Den beaufsichtigten Beh&ouml;rdenfunktionen sind die richtigen &ouml;ffentlichen Stellen zugeordnet.</li>
</ul>
<p>Die Fachaufsicht kann jederzeit</p>
<ul>
<li>die ihr zugeordneten Beh&ouml;rdenfunktionen einsehen,</li>
<li>die fachverantwortlichen Stellen</a> einsehen, die den Beh&ouml;rdenfunktionen der Fachaufsicht zugeordnet sind,</li>
<li>eine ihr zugeordnete Beh&ouml;rdenfunktion &auml;ndern,</li>
<li>eine noch nicht vorhandene Beh&ouml;rdenfunktion anlegen</a> und sich zuordnen</a>.</li>
</ul>
<p>Stellt die Fachaufsicht eine falsche Zuordnung einer fachverantwortlichen Stelle zu einer beaufsichtigten Beh&ouml;rdenfunktion fest, veranlasst sie au&szlig;erhalb des IAM f&uuml;r Beh&ouml;rden eine entsprechende Korrektur.</p>
<p>Die Fachaufsicht kann jederzeit eine ihr zugeordnete Beh&ouml;rdenfunktion unter folgenden Bedingungen l&ouml;schen:</p>
<ul>
<li>Es gibt keine fachverantwortliche Stelle</a>, die dieser Beh&ouml;rdenfunktion zugeordnet ist.</li>
</ul>

#### Rechtsnormen beaufsichtigter Beh&ouml;rdenfunktionen kontrollieren

<p>Die Fachaufsicht</a> kontrolliert Rechtsnormen von Beh&ouml;rdenfunktionen unter ihrer Aufsicht nach folgenden Gesichtspunkten:</p>
<ul>
<li>Die Rechtsnormen beaufsichtigter Beh&ouml;rdenfunktionen sind korrekt registriert.</li>
<li>Die Rechtsnormen beaufsichtigter Beh&ouml;rdenfunktionen sind ggf. gem&auml;&szlig; der&nbsp;Rechtsgrundlage f&uuml;r Verwaltungsbereiche</a> den korrekten Verwaltungsbereichen zugeordnet.</li>
</ul>
<p>Die Fachaufsicht kann jederzeit eine Rechtsnorm</a> beaufsichtigter Beh&ouml;rdenfunktionen registrieren oder &auml;ndern.</p>
<p>Die Fachaufsichtkann jederzeit eine&amp;nbsp Rechtsnorm unter folgenden Bedingungen l&ouml;schen:</p>
<ul>
<li>Es gibt keine Beh&ouml;rdenfunktion, die auf diese Rechtsnorm (als Teil der Rechtsgrundlage) verweist.</li>
</ul>

### Prozesse der pflegenden Stelle

<p>Die pflegende Stelle kann die nachfolgenden Prozesse des IAM f&uuml;r Beh&ouml;rden nutzen, um die zum Betrieb des IAM f&uuml;r Beh&ouml;rden n&ouml;tigen Daten einzubringen und aktuell zu halten. F&uuml;r die Nutzung jedes dieser Prozesse gilt:</p>
<ul>
<li>Das verwendete Zertifikat muss im IAM f&uuml;r Beh&ouml;rden als Zertifikat der pflegenden Stelle hinterlegt sein.</li>
</ul>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Organisatorische Prozesse (bspw. Anlass, H&auml;ufigkeit und Auswahl von Analysen protokollierter Ereignisse) werden hier nicht beschrieben.</em></li>
</ul>

#### Wurzelzertifizierungsstellen verwalten

<p>Die f&uuml;r das IAM f&uuml;r Beh&ouml;rden verantwortliche Stelle</p>
<ul>
<li>bestimmt die vom IAM f&uuml;r Beh&ouml;rden zu verwendenden Wurzelzertifizierungsstellen und</li>
<li>legt jeweils fest, ob es eine Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen ist.</li>
</ul>
<p>Die pflegende Stelle verwaltet Zertifikate von Wurzelzertifizierungsstellen</a> nach diesen Vorgaben. Sie stellt insbesondere sicher,</p>
<ul>
<li>dass jede Wurzelzertifizierungsstelle entweder mit "Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen" oder mit "Wurzelzertifizierungsstelle f&uuml;r Zertifikate sonstiger Stellen" gekennzeichnet ist,</li>
<li>dass es jederzeit mindestens eine Wurzelzertifizierungsstelle f&uuml;r Zertifikate &ouml;ffentlicher Stellen gibt,</li>
<li>dass es jederzeit mindestens eine Wurzelzertifizierungsstelle f&uuml;r Zertifikate sonstiger Stellen gibt und</li>
<li>dass die Zertifikate aller verwalteten Wurzelzertifizierungsstellen g&uuml;ltig sind.</li>
</ul>
<p>Das IAM f&uuml;r Beh&ouml;rden soll die pflegende Stelle rechtzeitig &uuml;ber den anstehenden G&uuml;ltigkeitsablauf von Zertifikaten der Wurzelzertifizierungsstellen informieren (siehe NOOTS-1033</a>).</p>

#### Verwaltungsbereiche verwalten

<p>Die pflegende Stelle verwaltet Verwaltungsbereiche auf der Grundlage einer entsprechenden Verordnung der Bundesregierung nach &sect; 12 (1) IDNrG oder im Vorgriff auf eine zu erwartende solche Rechtsgrundlage nach den Vorgaben der f&uuml;r das IAM f&uuml;r Beh&ouml;rden verantwortlichen Stelle.</p>
<p>Die pflegende Stelle kann jederzeit einen Verwaltungsbereich mit seinem Namen unter folgenden Bedingungen anlegen und &auml;ndern:</p>
<ul>
<li>Der Name des Verwaltungsbereichs ist noch nicht vorhanden.</li>
</ul>
<p>Die pflegende Stelle kann jederzeit einen Verwaltungsbereich unter folgenden Bedingungen l&ouml;schen:</p>
<ul>
<li>Es gibt keine Beh&ouml;rdenfunktionen und keine Rechtsnormen, die dem Verwaltungsbereich zugeordnet sind.</li>
</ul>

#### Rollen verwalten

<p>Die pflegende Stelle verwaltet Rollen</a> aufgrund entsprechender Meldungen von Ressourcen.</p>
<p>Eine Rolle wird nach dem Schema &lt;Pr&auml;fix&gt;.&lt;Name&gt; bezeichnet. Dabei wird</p>
<ul>
<li><strong>&lt;Pr&auml;fix&gt;</strong> ersetzt mit der <strong>Kurzbezeichnung der Teilnahmeart oder der Ressource</strong>, bspw. <em>RDN</em> f&uuml;r "Registerdatennavigation" oder <em>DP</em> f&uuml;r "Data Provider".</li>
<li><strong>&lt;Name&gt;</strong> ersetzt mit einer <strong>pr&auml;gnanten Bezeichnung der Zugriffsart</strong> nach Wahl der Ressource.</li>
</ul>
<p>Die pflegende Stelle gibt die zu verwendenden Pr&auml;fixe vor und achtet auf die Einhaltung der Namenskonvention.</p>
<p>Die pflegende Stelle kann jederzeit eine Rolle unter folgenden Bedingungen anlegen und &auml;ndern:</p>
<ul>
<li>Die o.g. Namenskonvention f&uuml;r Rollen ist eingehalten.</li>
<li>Es gibt noch keine Rolle mit diesem Namen.</li>
</ul>
<p>Die pflegende Stelle kann jederzeit eine Rolle unter folgenden Bedingungen l&ouml;schen:</p>
<ul>
<li>Die Rolle ist keiner Teilnahmeart zugeordnet.</li>
</ul>

#### Teilnahmearten verwalten

<p>Die pflegende Stelle verwaltet Teilnahmearten</a> zur nachvollziehbaren Zuordnung von Rollen zu IT-Komponenten.</p>
<p>Die pflegende Stelle kann jederzeit den <strong>Namen</strong> einer Teilnahmeart unter folgenden Bedingungen anlegen und &auml;ndern:</p>
<ul>
<li>Es gibt noch keine Teilnahmeart mit diesem Namen.</li>
<li>Der Bezeichner ist an der Fachlichkeit orientiert. Im Kontext von NOOTS sollen die Bezeichner der NOOTS-Komponenten und f&uuml;r entsprechende Teilnehmer die Bezeichner "Data Consumer" und "Data Provider" verwendet werden.</li>
</ul>
<p>Die pflegende Stelle kann jederzeit unter folgenden Bedingungen <strong>Rollen</strong>zu einer Teilnahmeart hinzuf&uuml;gen und von einer Teilnahmeart entfernen:</p>
<ul>
<li>Das Hinzuf&uuml;gen oder Entfernen entspricht den fachlichen Bed&uuml;rfnissen f&uuml;r die Teilnahmeart.</li>
</ul>
<p>Die pflegende Stelle kann jederzeit eine Teilnahmeart unter folgenden Voraussetzungen l&ouml;schen:</p>
<ul>
<li>Die Teilnahmeart ist keiner IT-Komponente zugeordnet.</li>
</ul>

#### Zertifikat zur Siegelung verwalten

<p>Das IAM f&uuml;r Beh&ouml;rden muss ein zur Siegelung geeignetes Zertifikat (Siegelzertifikat) und den zugeh&ouml;rigen privaten Schl&uuml;ssel vorhalten, um abgerufene Zugriffstoken</a> und Identit&auml;tsdaten siegeln zu k&ouml;nnen und dadurch die Pr&uuml;fung deren Integrit&auml;t und Herkunft durch Dritte zu erm&ouml;glichen (siehe NOOTS-1031</a>).</p>
<p>Die pflegende Stelle verwaltet das Siegelzertifikat des IAM f&uuml;r Beh&ouml;rden und gew&auml;hrleistet dessen Verf&uuml;gbarkeit, G&uuml;ltigkeit und Abrufbarkeit durch Dritte, um diesen die Pr&uuml;fung der Herkunft und G&uuml;ltigkeit des Siegelzertifikats zu erm&ouml;glichen (siehe Siegelzertifikat abrufen</a>).</p>

#### G&uuml;ltigkeitsdauer von Zugriffstoken verwalten

<p>Die G&uuml;ltigkeitsdauer f&uuml;r Zugriffstoken wird mit 60 Sekunden vorbelegt.</p>
<p>Die pflegende Stelle kann jederzeit die G&uuml;ltigkeitsdauer f&uuml;r Zugriffstoken</a> unter folgenden Bedingungen &auml;ndern:</p>
<ul>
<li>Die G&uuml;ltigkeitsdauer ist nicht kleiner als 30 Sekunden.</li>
<li>Die G&uuml;ltigkeitsdauer ist nicht gr&ouml;&szlig;er als 300 Sekunden.</li>
<li>Die G&uuml;ltigkeitsdauer ist auf die Bed&uuml;rfnisse der Teilnehmer abgestimmt.</li>
</ul>
<p>Das IAM f&uuml;r Beh&ouml;rden verwendet die von der pflegenden Stelle festgelegte G&uuml;ltigkeitsdauer bei der Ausstellung von Zugriffstoken.</p>

#### Frist f&uuml;r Registrierungsbest&auml;tigung verwalten

<p>Die Frist zur Best&auml;tigung der Registrierung von IT-Komponenten wird mit sieben Kalendertagen vorbelegt.</p>
<p>Die pflegende Stelle kann jederzeit die Best&auml;tigungsfrist unter folgenden Bedingungen &auml;ndern:</p>
<ul>
<li>Die Best&auml;tigungsfrist ist nicht kleiner als drei Kalendertage.</li>
<li>Die Best&auml;tigungsfrist ist nicht gr&ouml;&szlig;er als 14 Kalendertage.</li>
<li>Die Best&auml;tigungsfrist ist auf die Bed&uuml;rfnisse der Teilnehmer abgestimmt.</li>
</ul>
<p>Das IAM f&uuml;r Beh&ouml;rden verwendet die von der pflegenden Stelle festgelegte Best&auml;tigungsfrist zur Pr&uuml;fung des Fristablaufs unbest&auml;tigter Registrierungen von IT-Komponenten und l&ouml;scht unbest&auml;tigte Registrierungen von IT-Komponenten nach Fristablauf.</p>

### Prozesse von Ressourcen

#### Rollen melden

<p>Die fachverantwortliche oder betriebsverantwortliche Stelle einer Ressource meldet der pflegenden Stelle die f&uuml;r den Zugriff auf die Ressource definierten Rollen.</p>
<p><em>Hinweise:</em></p>
<ul>
<li><em>Verantwortliche Stellen von Ressourcen melden die f&uuml;r Zugriffe n&ouml;tigen Rollen dem IAM f&uuml;r Beh&ouml;rden auf organisatorischem Weg. Eine technische Unterst&uuml;tzung solcher Meldungen durch das IAM f&uuml;r Beh&ouml;rden ist nicht vorgesehen.</em></li>
</ul>

## Ausblick und weiterf&uuml;hrende Aspekte

<p>Das IAM f&uuml;r Beh&ouml;rden soll auf der Grundlage des Standards OAuth2 (RFC 6749 oder Nachfolger, siehe <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">[SQ12]</a>) umgesetzt werden, m&ouml;glichst auf der Grundlage einer daf&uuml;r geeigneten Open-Source-Software wie Keycloack(siehe <a href="https://bmi.usercontent.opencode.de/noots/Glossar/">[SQ-11]</a>).</p>
<p>Die Verwendung des Standards OAuth2 und auf dieser Grundlage erstellte Zugriffstoken</a> entsprechen dem Stand der Technik. Angefragte Ressourcen k&ouml;nnen anhand der Informationen im Zugriffstoken selbst&auml;ndig entscheiden, ob ein Zugriff gew&auml;hrt wird oder nicht.</p>
<p>Die Ausstellung eines Zugriffstokens zur Verwendung bei mehreren Ressourcenzugriffen (innerhalb seiner G&uuml;ltigkeitsdauer) entlastet Ressourcen von spezifischen Pr&uuml;fungen und Abfragen und vermeidet entsprechende zus&auml;tzliche Kommunikation.</p>

### Offene Punkte

<p><span style="color: #000000;"><strong>Tab. 11: Offene Punkte</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">&nbsp;</span></th>
<th><span style="color: #000000;">ID</span></th>
<th><span style="color: #000000;">Aspekt</span></th>
<th><span style="color: #000000;">Beschreibung</span></th>
</tr>
<tr>
<td><span style="color: #000000;">1</span></td>
<td><span style="color: #000000;">IAM-OP-01</span></td>
<td><span style="color: #000000;">Personenbezogene Authentifizierung</span></td>
<td><span style="color: #000000;">Muss f&uuml;r bestimmte Prozesse (insb. f&uuml;r Prozesse &ouml;ffentlicher und fachverantwortlicher Stellen, der Fachaufsicht und der pflegenden Stelle) eine zus&auml;tzliche personenbezogene Authentifizierung verlangt und protokolliert werden?</span></td>
</tr>
<tr>
<td><span style="color: #000000;">2</span></td>
<td><span style="color: #000000;">IAM-OP-02</span></td>
<td><span style="color: #000000;">Vertrauensniveau</span></td>
<td><span style="color: #000000;">Muss das IAM f&uuml;r Beh&ouml;rden auf das Vertrauensniveau "hoch" ausgerichtet werden? Dann m&uuml;ssen gem&auml;&szlig; Unterkap. 3.2, 3.7 und 4.2 der Technischen Richtlinie BSI TR-03107-1 Hardware-Token (elektronische Mitarbeiterausweise, Chipkarten, spezielle USB-Sticks, etc.) f&uuml;r private Schl&uuml;ssel von Zertifikaten verwendet werden.</span></td>
</tr>
</tbody>
</table>

## &Auml;nderungsdokumentation

<p><span style="color: #000000;"><strong>Tab. 12: &Auml;nderungen</strong></span></p>
<table>
<tbody>
<tr>
<th><span style="color: #000000;">&nbsp;</span></th>
<th><span style="color: #000000;">Release-Versionen</span></th>
<th><span style="color: #000000;">Kapitel</span></th>
<th><span style="color: #000000;">Beschreibung der &Auml;nderung</span></th>
<th><span style="color: #000000;">Art der &Auml;nderung</span></th>
<th><span style="color: #000000;">Priorisierung/ major changes</span></th>
<th><span style="color: #000000;">Quelle der &Auml;nderung</span></th>
</tr>
<tr>
<td><span style="color: #000000;">1</span></td>
<td><span style="color: #000000;">Q1/2024</span></td>
<td><span style="color: #000000;">Einleitung</span></td>
<td>
<ul>
<li><span style="color: #000000;">Vollst&auml;ndige &Uuml;berarbeitung des Konzepts der Datenhaltung</span></li>
</ul>
</td>
<td><span style="color: #000000;">neuer Inhalt</span></td>
<td><span style="color: #000000;">⭐</span></td>
<td>
<p><span style="color: #000000;">NOOTS-Board</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">2</span></td>
<td><span style="color: #000000;">Q1/2024</span></td>
<td><span style="color: #000000;">Fachliches Konzept</span></td>
<td>
<ul>
<li><span style="color: #000000;">Erweiterung entsprechend Konzepts der Datenhaltung; geht auf in Unterkapitel Einleitung - Datenhaltung</span></li>
</ul>
</td>
<td><span style="color: #000000;">neuer Inhalt</span></td>
<td><span style="color: #000000;">⭐</span></td>
<td>
<p><span style="color: #000000;">NOOTS-Board</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">3</span></td>
<td><span style="color: #000000;">Q1/2024</span></td>
<td><span style="color: #000000;">Prozesse</span></td>
<td>
<ul>
<li><span style="color: #000000;">Neu: Prozesse der IT-Komponenten und der Akteure beschrieben, die vom IAM f&uuml;r Beh&ouml;rden angeboten werden</span></li>
</ul>
</td>
<td><span style="color: #000000;">neuer Inhalt</span></td>
<td><span style="color: #000000;">⭐</span></td>
<td>
<p><span style="color: #000000;">NOOTS-Board</span></p>
</td>
</tr>
<tr>
<td><span style="color: #000000;">4</span></td>
<td><span style="color: #000000;">Q2/2024</span></td>
<td><span style="color: #000000;">Alle</span></td>
<td>
<ul>
<li><span style="color: #000000;">Vollst&auml;ndige &Uuml;berarbeitung und Restrukturierungauf der Grundlage von Arc42</span></li>
</ul>
</td>
<td><span style="color: #000000;">neuer Inhalt</span></td>
<td><span style="color: #000000;">⭐</span></td>
<td>
<p><span style="color: #000000;">NOOTS-Board</span></p>
</td>
</tr>
</tbody>
</table>