>**Redaktioneller Hinweis**
>
>Dokument aus der vorangegangen Iteration - nicht Teil der aktuellen Iteration.
>
>Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## Abstract

Das Dokument High-Level-Architecture (HLA) verschafft einen Überblick
über das Nationale Once-Only-Technical-System (NOOTS). Es beschreibt
übergreifende Anforderungen an das NOOTS-Gesamtsystem, den nationalen
und europäischen Rechtsrahmen, das Architekturzielbild des NOOTS, die
zentralen Komponenten sowie deren Zusammenwirken in Anwendungsfällen und
übergreifenden Konzepten.

## Überblick

Das Dokument High-Level-Architecture (HLA) schafft eine Grundlage zur
Darstellung der Gesamtarchitektur des Nationalen
Once-Only-Technical-Systems (NOOTS). 

Es umfasst folgende Inhalte: 

-   Darstellung der Rahmenbedingungen und Rechtsgrundlagen des NOOTS
-   Beschreibung des Architekturzielbilds des NOOTS
-   Beschreibung von Anforderungen an das NOOTS-Gesamtsystem
-   Beschreibung des übergreifenden Zusammenwirkens der
    NOOTS-Komponenten in nationalen und europäischen Anwendungsfällen.

Die Zielgruppe des Dokuments sind sowohl Verantwortliche in
Entscheidungsgremien (z. B. IT-Planungsrat, Lenkungskreis der
Registermodernisierung, Leitungsrunde der Registermodernisierung) als
auch Verantwortliche für die Umsetzung (Umsetzungsprojekte und
assoziierte Vorhaben) und IT-Architektur (z.B. Verantwortliche für
NOOTS-Komponenten, nachweisabrufende Stellen, nachweisliefernde
Stellen).

### Umfang des NOOTS

Das NOOTS ist ein System aus technischen Komponenten, Schnittstellen und
Standards sowie organisatorischen und rechtlichen Regelungen, das
öffentlichen Stellen den rechtskonformen Abruf von elektronischen
Nachweisen aus den Registern der deutschen Verwaltung ermöglicht. Zu
diesem Zweck stellt das NOOTS einheitliche Komponenten bereit und
formuliert Anschlussbedingungen an Data Consumer und Data Provider. Über
einen Anschluss an das EU-OOTS wird ein Austausch von Nachweisen mit dem
EU-Ausland ermöglicht. Der Austausch von nationalen Nachweisen erfolgt
dabei über das NOOTS, der Austausch von EU-Nachweisen über das NOOTS und
EU-OOTS.

Die folgende Grafik zeigt das NOOTS und alle Systeme, die mit ihm
interagieren, sowohl im nationalen als auch im grenzüberschreitenden
Kontext innerhalb der EU. Das NOOTS umfasst alle NOOTS-Komponenten, die
für die Durchführung oder Nachvollziehbarkeit eines Nachweisabrufs
notwendig sind. Data Consumer und Data Provider sind NOOTS-Teilnehmer,
die sich als registrierte technische Verfahren am NOOTS beteiligen. Sie
selbst werden jedoch nicht als Bestandteil des NOOTS betrachtet. Auch
die Datenverarbeitung oder Datenübermittlung, die nach dem Nachweisabruf
durch den Data Consumer erfolgt, ist kein Bestandteil des NOOTS.
Insbesondere gehört auch die Weiterleitung des Nachweises im Zuge der
Antragseinreichung nicht zum NOOTS.

Datenaustausche zum Zweck des Registerzensus oder wissenschaftlicher
Auswertungen werden in dieser Version des NOOTS noch nicht explizit
adressiert. Sie sind daher nur ausgegraut dargestellt.

> Redaktioneller Hinweis
>
> Die Gesamtarchitektur der Registermodernisierung ist aktuell in
> Entwicklung. Sobald genauere Informationen bekannt sind, werden Hinweise
> zur Abgrenzung zwischen dem Architekturzielbild des NOOTS und der
> Gesamtarchitektur der Registermodernisierung ergänzt. 

![C:\\8d783d6ac4a8c97322a852c2aabf07fa](./assets/images3/media/image1.png)
<br>**Abb. 1: Umfang des NOOTS**</br>


### Architekturdokumentation NOOTS

Der Programmbereich (PB) NOOTS verantwortet die Erstellung der
Architekturdokumentation des Nationalen Once-Only-Technical-Systems
(AD-NOOTS), die sich aus unterschiedlichen Dokumenten zusammensetzt.

![C:\\7338655f5fca6bd2c3e04346a5d8e1a1](./assets/images3/media/image2.png)
<br>**Abb. 2: Dokumentenstruktur AD-NOOTS**</br>


Die AD-NOOTS unterscheidet zwei übergreifende Formen von
Klammerdokumenten:

-   High-Level-Architecture: Das hier vorliegende Dokument dient dem
    Einstieg in die Zielarchitektur des NOOTS. Es beschreibt
    übergreifende Anforderungen an die NOOTS-Infrastruktur, den
    nationalen und europäischen Rechtsrahmen, das Architekturzielbild
    des NOOTS, die zentralen Komponenten und deren Zusammenwirken in
    Anwendungsfällen und übergreifenden Konzepten.
-   Anschlusskonzepte: Ein weiteres Klammerdokument stellen die
    Anschlusskonzepte für <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">Data Consumer</a>
    und <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank">Data Provider</a> dar.
    Diese dienen dem Zweck, Anschluss- und Rahmenbedingungen zu
    dokumentieren und aufzubereiten, um den Anschluss von
    nachweisabrufenden und nachweisliefernden Stellen an das NOOTS und
    das EU-OOTS zu ermöglichen.

Neben den übergreifenden Dokumenten gibt es eigenständige
Komponenten-Dokumentationen und übergreifende Konzepte, die jeweils
Teilaspekte des NOOTS beschreiben.

Über Querverweise werden Abhängigkeitsbeziehungen zwischen den
Dokumenten dargestellt.

-   Grobkonzepte sind Dokumente, die durch den PB NOOTS erstellt werden
    und die eine NOOTS-Komponente auf konzeptioneller Ebene beschreiben.
    Dazu werden komponentenspezifische funktionale und nicht-funktionale
    Anforderungen, Anwendungsfälle und sowie fachliche und technische
    Konzepte dokumentiert.
-   Komponentensteckbriefe werden dann erstellt, wenn eine
    NOOTS-Komponente durch ein assoziiertes Vorhaben umgesetzt wird. Ein
    assoziiertes Vorhaben ist ein Vorhaben, welches nicht aus
    FITKO-Mitteln finanziert wird, von oder zu dem jedoch relevante
    Abhängigkeiten bestehen
    ([IT-PLR-E-09](https://www.it-planungsrat.de/fileadmin/beschluesse/2023/Beschluss2023-22_RegMo_Finanzplan.pdf)).
    Innerhalb eines Komponentensteckbriefs dokumentiert der
    PB NOOTS spezifische Aspekte, die für die Funktionalität der
    Gesamtarchitektur und die Anschlusskonzepte von besonderer Relevanz
    sind. Der Komponentensteckbrief stellt eine Ergänzung zum
    Umsetzungskonzept dar, welches durch die assoziierten Vorhaben
    selbst veröffentlicht wird.
-   Umsetzungskonzepte werden durch assoziierte Vorhaben oder
    Umsetzungsprojekte erstellt. Umsetzungsprojekte sind Projekte, die
    einen unmittelbaren und substanziellen Beitrag zur Erreichung der
    Ziele der Gesamtsteuerung der Registermodernisierung leisten, indem
    sie auf die Erprobung einzelner Komponenten des NOOTS, des EU-OOTS
    und/oder Anschlussbedingungen an das NOOTS bzw. EU-OOTS einzahlen
    ([IT-PLR-E-09](https://www.it-planungsrat.de/fileadmin/beschluesse/2023/Beschluss2023-22_RegMo_Finanzplan.pdf)).
    Umsetzungskonzepte beschreiben eine Komponente auf Ebene der
    technischen Lösung.
-   Übergreifende Konzepte sind Dokumente, die übergreifende fachliche
    oder technische Regelungsbedarfe im NOOTS beschreiben, die nicht
    einer einzelnen NOOTS-Komponente zugeordnet werden können, z.B. die
    Nutzeridentifizierung und -authentifizierung.
-   Standards umfassen technische Spezifikationen einer Schnittstelle
    oder von Datenstrukturen zum Austausch über eine Schnittstelle, zum
    Beispiel dem XÖV-Rahmenwerk entsprechend.

### Dokumentenversionen

Diese Tabelle stellt eine Übersicht der vergangenen Releases sowie die
Planung zukünftiger Releases der Dokumente der AD-NOOTS dar. Dadurch
ermöglicht sie eine bessere Nachverfolgbarkeit der Weiterentwicklung der
Dokumente.

-   0.X = Das Dokument stellt einen aktuellen Arbeitsstand der, der noch
    weiter ausgearbeitet wird.
-   Version 1.0 = Das Dokument stellt einen stabilen Stand dar.
    Aktualisierungen und Verbesserungen sind weiterhin geplant und in
    kommenden Releases möglich.
-   Version 1.X = Das Dokument umfasst Aktualisierungen und
    Verbesserungen zu einer Version 1.0.

| Dokumentenname                                                                          | Version</p>  Q 4 2022    | Version</p> Q 3 2023  | Version</p> Q4 2023    | Version</p> Q1 2024  | Version</p> Q2 2024|
|-----------------------------------------------------------------------------------------|------------|----------|-------------|----------|---------|
| Anschlusskonzept Data Consumer                                                          | 0.9        | 0.9.5    | 1.0         | 01.   | 01.  |
| Anschlusskonzept Data Provider                                                          | 0.9        | 0.9.5    | 1.0         | 01.   | 01.  |
| High-Level-Architecture                                                                 | 0.9        | 1.0      | 01.     | 01.   | 01.  |
| Grobkonzept Access Point                                                                | -          | 1.0      | 1.0         | 1.0      | 01.  |
| Grobkonzept IAM für Behörden                                                            | 0.9        | 0.9      | 0.9         | 1.0      | 01.  |
| Grobkonzept Intermediäre Plattform                                                      | 0.9        | 1.0      | 1.0         | 1.0      | 01.  |
| Grobkonzept Registerdatennavigation                                                     | 0.9        | 0.9      | 0.9.5       | 1.0      | 1.0     |
| Grobkonzept Vermittlungsstellen                                                         | 0.9        | 0.9.5    | 0.9.5       | 1.0      | 1.0     |
| Komponentensteckbrief Datenschutzcockpit                                                | 0.9        | 1.0      | 1.0         | 1.0      | 1.0     |
| Komponentensteckbrief IDM Personen                                                      | 0.9        | 0.9      | 1.0         | 1.0      | 1.0     |
| Komponentensteckbrief IDM Unternehmen                                                   | 0.9        | 0.9      | 0.9         | 1.0      | 1.0     |
| Übergreifendes Konzept  Nutzeridentifizierung und -Authentifizierung, Datensatzabgleich | neu        | keine    | 0.5         | 0.9      | 1.0     |
| Übergreifendes Konzept Zertifikate                                                      | 0.9        | 0.9      | 0.9         | 1.0      | 1.0     |
| Übergreifendes Konzept Datenschutz und IT-Sicherheit                                    | neu        | keine    | keine       | 0.5      | 0.9     |
| Übergreifendes Konzept Betrieb                                                          | neu        | keine    | 0.5         | 0.9      | 1.0     |
| Übergreifendes Konzept  Transportinfrastruktur                                          | neu        | keine    | 0.5         | 0.9      | 1.0     |
| Spezifikation XNachweis                                                                 | Vorversion | 0.1      | 0.7 und 1.0 | 01.   | 01.  |


## Rahmenbedingungen 

Die Rahmenbedingungen des NOOTS und des Anschlusses an das EU-OOTS
ergeben sich aus unterschiedlichen Quellen: 

-   Rechtsgrundlagen und Dokumente der Europäischen Kommission
    zur SDG-Umsetzung ([EU-01](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX%3A32022R1463%3ADE%3AHTML) bis
    [EU-02](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents))
-   Nationale Rechtsgrundlagen im Kontext der Registermodernisierung
    ([RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf), [RGR-02](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf), [RGR-03](https://www.gesetze-im-internet.de/ozg/OZG.pdf),
    [RGR-04](https://www.gesetze-im-internet.de/regzenserpg/RegZensErpG.pdf))
-   Beschlüsse des IT-Planungsrats und Entscheidungen des IT-Planungsrats ([IT-PLR-B-01](https://www.it-planungsrat.de/beschluss/beschluss-2019-03), [02](https://www.it-planungsrat.de/beschluss/beschluss-2019-23), [03](https://www.it-planungsrat.de/beschluss/beschluss-2020-25), [04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05), [05](https://www.it-planungsrat.de/beschluss/beschluss-2021-25), [06](https://www.it-planungsrat.de/beschluss/beschluss-2021-35), [07](https://www.it-planungsrat.de/beschluss/beschluss-2022-06), [08](https://www.it-planungsrat.de/beschluss/beschluss-2022-22), [09](https://www.it-planungsrat.de/beschluss/beschluss-2022-34))
        & ([IT-PLR-E-01](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL2_Reifegradmodell.pdf), [02](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL3_Asynchrone_Prozesse.pdf), [03](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL4_Nachweisabruf.pdf), [04](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL5_DSD.pdf), [05](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL6_Registerdatennavigation.pdf), [06](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_EU-OOTS.pdf), [07](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_NOOTS-Registeranbindung.pdf), [08](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_SDG-Connector.pdf), [09](https://www.it-planungsrat.de/fileadmin/beschluesse/2023/Beschluss2023-22_RegMo_Finanzplan.pdf))
-   Sonstige weiterführende Dokumente ([SQ-01](https://www.normenkontrollrat.bund.de/Webs/NKR/SharedDocs/Downloads/DE/Positionspapiere/faktencheck-registermodernisierungsgesetz.pdf?__blob=publicationFile) bis
    [SQ-03](https://www.verwaltungskooperation.de/conf/display/RMPH/Gesamtsteuerung+Registermodernisierung))

Zusätzlich zu diesen Quellen gibt es weitere Dokumente und
Rechtsgrundlagen, die Auswirkungen auf das NOOTS haben können (z.B.
IT-NetzG, ITSiV-PV). Diese werden hier nicht detailliert betrachtet.

### Übergreifende Entwurfsentscheidungen

Die folgenden IT-Planungsrat-Beschlüsse und Entscheidungen sind von
besonderer Relevanz für die Architekturzielbilder des NOOTS. 

| Entscheidung                                                                                                                                                      | Erläuterung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <p><p style="color:#184B76;">IT-PLR Beschluss 2021/05: Registermodernisierung [(IT-PLR-B-04)](https://www.it-planungsrat.de/beschluss/beschluss-2021-05)</p></p>                                                                                                                  | <p><p style="color:#184B76;">Der IT-Planungsrat beschließt das vom Koordinierungsprojekt Registermodernisierung erarbeitete Zielbild der Registermodernisierung und schlägt die Einrichtung des Bund-Länder-Projekts “Gesamtsteuerung Registermodernisierung” vor, um ein systematisches und schlüssiges Vorgehen bei der Modernisierung der deutschen Registerlandschaft sicherzustellen. Das Projekt, unter der Leitung des BMI sowie der Länder Bayern und Hamburg, soll eine ressort- und ebenenübergreifende Umsetzung aller Teilprojekte der Registermodernisierung vorantreiben. Auf Grund des Umfangs des Gesamtvorhabens Registermodernisierung sowie der bereichs- und ebenenübergreifenden Relevanz wurde die Federführerschaft im Projektverlauf durch Baden-Württemberg und Nordrhein-Westfalen erweitert (IT-PLR Beschluss 2021/25: Registermodernisierung)</p></p> |                                                                                                                                      
| IT-PLR Beschluss 2022/06: Entscheidung zur Umsetzung eines NOOTS [(IT-PLR-B-07)](https://www.it-planungsrat.de/beschluss/beschluss-2022-06)                                                                                                   | Im Rahmen der Validierung des Zielbilds der Registermodernisierung wurde festgestellt, dass nicht alle nationale Anforderungen durch das bei der Europäischen Kommission (EU-KOM) in Entwicklung befindliche EU-OOTS abgedeckt werden können. Aus diesem Grund wurde entschieden, dass ein NOOTS umgesetzt werden muss. Um die Anschlussfähigkeit an das EU-OOTS sicherzustellen, dürfen Abweichungen zwischen den nationalen und europäischen Lösungen nur dann erfolgen, wenn diese für die Umsetzung der nationalen Anforderungen zwingend erforderlich sind. Der Anschluss an die EU soll zusätzlich durch eine zentral entwickelte technische Lösung unterstützt werden, die sogenannte “Intermediäre Plattform”. |
| IT-PLR Beschluss 2022/22: Aufbau eines nationalen DSD und Nutzung des europäischen EB [(IT-PLR-E-04)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL5_DSD.pdf)                                                                               | Der Lenkungskreis Registermodernisierung beschließt, bei der Bereitstellung des Data Service Directory (DSD) für das technische System der EU zum Nachweisabruf nach Artikel 14 SDG-VO zur Bereitstellung der Routing-Informationen zu deutschen Evidence Providern (registerführenden Stellen) auf eine nationale Implementierung des DSD zu setzen. Der Lenkungskreis Registermodernisierung beschließt außerdem, bei der Bereitstellung des Evidence Brokers (EB) für das technische System der EU zum Nachweisabruf nach Artikel 14 SDG-VO die von der Europäischen Kommission zentral bereitgestellte Lösung zu nutzen und auf eine separate nationale Implementierung zu verzichten. |                                                                                                                                                  
| IT-PLR-Beschluss 2022/22: Entscheidung zur Umsetzung der Komponente Registerdatennavigation [(IT-PLR-E-05)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL6_Registerdatennavigation.pdf)                                                                        | Der Lenkungskreis Registermodernisierung beauftragt das Kompetenzteam Architektur mit der Formulierung eines Projektauftrags, aus dem Zielsetzung und Rahmenbedingungen zur Umsetzung der Komponente Registerdatennavigation hervorgehen. Die Registerdatennavigation soll als neue Komponente nach dem Vorbild des FIT-Connect Routingdienstes aufgebaut werden. Die technischen Dienste werden, wie bisher im DVDV, zentral verwaltet. Für die Zuständigkeiten wird ein neues Zuständigkeitsverzeichnis aufgebaut, das jedoch Zuständigkeiten in einer verallgemeinerten Form speichert, so dass es gleichermaßen für Leistungen aus dem LeiKa, für Nachweise aus der Registermodernisierung und für Zuständigkeiten für beliebige Rechtsgrundlagen geeignet ist. Die Pflege der Daten soll analog zum PVOG über XZuFi erfolgen. Darüber sind bspw. automatisierte Abgleiche mit dem PVOG möglich. Perspektivisch kann dieses Zuständigkeitsverzeichnis zu einem zentralen Zuständigkeitsverzeichnis der Deutschen Verwaltung (DVZV) entwickelt werden. |    
| IT-PLR-Beschluss 2022/22: Entscheidung asynchrone Prozesse [(IT-PLR-E-02)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL3_Asynchrone_Prozesse.pdf)                                                                                                         | Im Zielbild der Registermodernisierung werden sowohl synchrone als auch asynchrone Prozesse vorgesehen. Die Begriffe “synchron“ und „asynchron” sind dabei nicht in einem technischen Sinne zu verstehen, sondern entsprechend der Eignung für einen Nachweisabruf unter Beteiligung von Bürgerinnen und Bürgern und Unternehmen. Um die für den Nachweisabruf notwendige Nutzerinteraktion zu ermöglichen, muss ein Nachweis synchron, also innerhalb weniger Sekunden, für die Weiterverarbeitung bereitstehen. In asynchronen Prozessen hingegen könnte die Zeit mehrere Minuten, Stunden oder Tage betragen, was sich für einen Nachweisabruf mit Nutzerinteraktion nicht eignet. Dies entspricht auch den Anforderungen des EU-OOTS, in dem ausschließlich synchrone, “automatisiert austauschbare” Nachweisabrufe unterstützt werden. Entsprechend wurde im IT-PLR-Beschluss 2022/22 festgelegt, dass wenn der Data Consumer ein Onlinedienst ist (z. B. ein Portal oder ein Formularmanagementsystem), nur fachlich synchrone Nachweisabrufe möglich sein sollen. Für die Behörde-zu-Behörde-Kommunikation, in denen keine Nutzerinteraktion notwendig ist, sollen auch fachlich asynchrone Nachweisabrufe möglich sein. Dies ermöglicht die Anbindung von Registern an das NOOTS, die noch nicht in der Lage sind, synchron zu antworten. |
| IT-PLR-Beschluss 2022/22: Entscheidung Nachweisabrufstandard [(IT-PLR-E-03)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL4_Nachweisabruf.pdf)                                                                                                      | Für die Umsetzung des Once-Only-Prinzips wird es erforderlich sein, eine Vielzahl heterogener IT-Systeme für den Nachweisabruf zu ertüchtigen und miteinander zu verbinden. Um den Abruf und die Übermittlung von Nachweisen zu standardisieren, hat der IT-Planungsrat entschieden, einen generischen Nachweisabrufstandard zu entwickeln. Um Vorteile gleichermaßen im nationalen wie auch europäischen Kontext zu erzeugen und die SDG-Umsetzung zu fördern, soll dieser Standard auf Grundlage des Exchange Data Model (EDM) des EU-OOTS entwickelt werden und nur dann von diesem abweichen, wenn es im NOOTS zwingend erforderlich ist. Im Folgenden wird dieser Standard auch als XNachweis bezeichnet.                                                                                                                                                    
| IT-PLR-Beschluss 2022/22: Entscheidung Reifegradmodell [(IT-PLR-E-01)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL2_Reifegradmodell.pdf)                                                                                                           | Für die erfolgreiche Umsetzung des Once-Only-Prinzips in der öffentlichen Verwaltung müssen Data Consumer (Antrags- bzw. Fachverfahren) und Data Provider (registerführende öffentliche Stellen) umfassend digitalisiert werden, um einen medienbruchfreien und automatisierten Abruf von Nachweisen zu ermöglichen. Der Lenkungskreis Registermodernisierung beschließt, ein Reifegradmodell für Nachweisabrufe einzuführen. Dabei sollte mindestens der Nachweisabruf-Reifegrad C erreicht werden. Das Ziel ist die Erreichung von Nachweisabruf-Reifegrad D. Wenn bei Registern kurzfristig nicht Reifegrad C erreicht werden kann, können diese in einer Übergangszeit auch im Reifegrad B angebunden werden. Diese Übergangszeit sollte jedoch – ggf. auch rechtlich – begrenzt werden. | 
| IT-PLR-Beschluss 2022/34: Entscheidung NOOTS-Registeranbindung [(IT-PLR-E-06)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_EU-OOTS.pdf)                                                                                                     | Sowohl das EU-OOTS als auch das NOOTS stellen eine Vielzahl unterschiedlicher Anforderungen, sogenannte Anschlussbedingungen, die beim Anschluss an die nationale oder europäische Infrastruktur umgesetzt werden müssen. Während es zentralen Registern voraussichtlich mit akzeptablem Aufwand möglich sein wird, diese Anforderungen zu erfüllen, gestaltet sich die Situation bei dezentralen Registern deutlich komplexer. Insbesondere dann, wenn sich die Dezentralität bis auf kommunale Ebene erstreckt, kann nicht davon ausgegangen werden, dass die notwendigen Ressourcen zur Anbindung an das NOOTS vorhanden sind. Auf technischer Ebene ist daher in diesen Fällen überlegenswert, ob der Anschluss an die Systeme zum Nachweisabruf als Anlass genommen werden kann, auch hier zentrale Strukturen, wie Spiegelregister oder Abrufportale, zu schaffen und diesen dann die Rolle als Data Provider zu übertragen. |                                                                                                                                                  
| IT-PLR-Beschluss 2022/34: Entscheidung zur Anbindung der Register und Online-Services an das europäische Once-Only-Technical-System über intermediäre Plattformen [(IT-PLR-E-06)](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_EU-OOTS.pdf) | Um die Aufwände und Belastungen für die verantwortlichen Stellen zur Anbindung der Register und Onlineservices an das EU-OOTS möglichst gering zu halten, sollen im Rahmen der Registermodernisierung Aufgaben und Funktionen zur Umsetzung der Anschlussbedingungen soweit möglich und sinnvoll von zentralen Strukturen übernommen werden. Das EU-OOTS erlaubt gemäß Artikel 1 Nr. 6 der Durchführungsverordnung (DVO) für den Anschluss an das System die indirekte Anbindung über Intermediäre Plattformen, die von den Mitgliedstaaten ausgestaltet werden können. Dadurch kann verhindert werden, dass spezifische Funktionalitäten, die nur für die EU-Anbindung benötigt werden, in einer Vielzahl von dezentralen Registern und Onlineservices implementiert werden müssen. Der Anschluss öffentlicher Stellen an das technische System gemäß Artikel 14 SDG-VO (EU-OOTS) soll daher verpflichtend über Intermediäre Plattformen (technische Komponenten i.S.v. Artikel 1 Nr. 6 DVO zu Artikel 14 SDG-VO) erfolgen. Die technische, rechtliche, finanzielle und organisatorische Ausgestaltung der Anbindung soll im Rahmen der Ausarbeitung detailliert werden.  |                                                                                                                                                     
| IT-PLR-Beschluss 2023/22: Konkretisierung des Zielbildes Registermodernisierung in Form von zwei Aufträgen [(IT-PLR-B-11)](https://www.it-planungsrat.de/beschluss/beschluss-2023-22)                                                       | <p><p style="color:#184B76;">Der IT-Planungsrat beschließt die Konkretisierung des Zielbildes Registermodernisierung in Form von zwei Aufträgen. Auf dieser Grundlage wurde ein Gesamtplan zur Umsetzung, Steuerung und Überwachung des Programmfortschritts erstellt:</p></p> <ul><li><p style="color:#184B76;">Auftrag 1: Umsetzung des Once-Only-Prinzips: Bereitstellung des technischen Systems und Entwurfserstellung der rechtlichen Grundlagen zur Umsetzung des Art. 14 SDG-VO sowie Begleitung des Anschlusses der SDG relevanten Register / Nachweise und Onlinedienste / Serviceportale an das NOOTS.</p><br><li><p style="color:#184B76;">Auftrag 2: Umsetzung des Once-Only-Prinzips: Entwicklung und Betrieb der technischen Infrastruktur zur Nachweisübermittlung (NOOTS) sowie Entwurf der rechtlichen Regelung des NOOTS zur Umsetzung des Once-Only-Prinzips und Begleitung des Anschlusses der Top Register / Nachweise und Onlinedienste / Serviceportale an das NOOTS 2.</p></br><ul> |                                                                                                                                                           
| IT-PLR-Beschluss 2023/38: Kommunikationsinfrastruktur für den Nachweisabruf über das NOOTS [(IT-PLR-B-10)](https://www.it-planungsrat.de/beschluss/beschluss-2023-38)                                                                         | Es wird eine Kommunikationsinfrastruktur für den Abruf von Nachweisen über das NOOTS benötigt, die alle sich aus den Zielen des NOOTS ergebenden Anforderungen erfüllt und rechtzeitig zur Bereitstellung des NOOTS zur Verfügung steht. Der IT-Planungsrat beauftragt die Gesamtsteuerung Registermodernisierung mit der Konzeption einer NOOTS-spezifischen Kommunikationsinfrastruktur. Diese soll auf bestehenden Grundlagen aufbauen, sodass Lösungsbausteine des IT-Planungsrats und Infrastrukturelemente soweit sinnvoll nachgenutzt werden können. |


## Anforderungen an das NOOTS-Gesamtsystem

> **Hinweis zum NOOTS-Gesamtsystem**
>
> Das NOOTS-Gesamtsystem umfasst alle NOOTS-Komponenten, die für
> die Durchführung oder Nachvollziehbarkeit eines Nachweisabrufs notwendig
> sind. Data Consumer und Data Provider sind NOOTS-Teilnehmer, die sich
> als registrierte technische Verfahren am NOOTS beteiligen. Sie sind
> jedoch kein Bestandteil des NOOTS-Gesamtsystems.

In diesem Kapitel werden funktionale und nicht-funktionale Anforderungen
an das NOOTS-Gesamtsystem dokumentiert. Diese beschreiben, welche
Eigenschaften, Leistungen und Qualitätsmerkmale das NOOTS-Gesamtsystem
auf übergeordneter Ebene erbringen soll.  In Ergänzung dazu werden
funktionale und nicht-funktionale Anforderungen an die einzelnen
Komponenten des NOOTS in den jeweiligen Konzepten oder
Komponentensteckbriefen beschrieben, wenn passend unter Bezugnahme auf
die entsprechende übergreifende Anforderung.

Die Übersicht der Anforderungen entspricht dem aktuellen Stand der
Konzeption des NOOTS. Durch neue Entwicklungen in der
Registermodernisierung können zusätzliche funktionale und
nicht-funktionale Anforderungen an das NOOTS-Gesamtsystem entstehen,
welche in zukünftigen Versionen der High-Level-Architecture ergänzt
werden.

#### Vorgehensweise und Quellen

Die Anforderungen an das NOOTS-Gesamtsystem wurden in mehreren Schritten
ermittelt. Zur Identifikation der relevanten Anforderungen wurden
folgende Quellen analysiert:

-   zentrale Rechtsgrundlagen im Kontext der Registermodernisierung
    (IDNrG, UBRegG, etc.)
-   Rechtsgrundlage des europäischen Vorhabens Single Digital Gateway
    (SDG-Verordnung, SDG-Durchführungsverordnung)
-   Beschlüsse und Entscheidungen des IT-Planungsrats
-   föderale IT-Architekturrichtlinien

Für die nicht-funktionalen Anforderungen wurden zusätzlich die
Qualitätskriterien des ISO-Standards 25010 betrachtet. Sofern sich zu
den dort genannten Kriterien aus den oben stehenden Quellen keine
Anforderung ableiten ließ, das Kriterium für das NOOTS aber als relevant
erachtet wurde, wurde eine entsprechende Anforderung ergänzt. Diese
Anforderungen tragen den Quellen-Verweis \"PB NOOTS\".

Bei der Auswertung der Quellen wurden solche Aspekte betrachtet, die für
die Entwicklung des NOOTS-Gesamtsystems relevant sind. Im Kontext der
nicht-funktionalen Anforderungen wurde dabei insbesondere
berücksichtigt, dass das NOOTS-Gesamtsystem selbst keine technische
Umsetzung umfasst und damit einige Aspekte der Quellen nicht zutreffen,
z.B. die Anforderung aus den föderalen IT-Architekturrichtlinien,
bevorzugt Open Source zu entwickeln. Diese sind aber für die konkrete
Implementierung in den Umsetzungsprojekten einschlägig und werden daher
in der Anforderungserhebung auf Komponentenebene berücksichtigt.

Die aus diesen Quellen ermittelten Anforderungen wurden dokumentiert, um
alle Informationen detailliert zu erfassen. Im Anschluss wurden die
Anforderungen verifiziert und validiert. Hierzu konnten Mitglieder des
PB NOOTS und weitere relevante Stakeholder Rückmeldungen zu den
einzelnen Anforderungen geben und es erfolgten weitere Abstimmungen zu
Anforderungen mit Klärungsbedarf. 

Funktionale Anforderungen aus weiteren Quellen, z.B. zu den Themen
IT-Sicherheit und Datenschutz, konnten noch nicht in vollem Umfang
berücksichtigt werden.

#### Funktionale Anforderungen an das NOOTS-Gesamtsystem

| ID        | Anforderung                                                                                                                                              | Ergänzende Erläuterung nach Bedarf                                                                                                                                                                                                                                                                                                                                                                                                      | Quelle            |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|
| NOOTS-635 | Das NOOTS-Gesamtsystem DARF die automatische Befüllung von Online-Anträgen mit Hilfe von Nachweisdaten nicht verhindern.                                 | Die Nachweisdaten, die mit Unterstützung des NOOTS aus dem Register abgerufen werden, müssen vom Data Consumer gelesen und entschlüsselt werden können, sodass dieser sie zur automatischen Befüllung von Online-Anträgen nutzen kann. Die automatische Befüllung der Online-Anträge liegt jedoch nicht im Umfang des NOOTS.                                                                                                            | [IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05)       |
| NOOTS-654 | Das NOOTS-Gesamtsystem MUSS interaktive Nachweisabrufe ermöglichen.                                                                                      | Eine interaktive Verarbeitung bedeutet, dass eine direkte Interaktion mit der Benutzerin / dem Benutzer stattfindet, z.B. bei einem Online-Antrag. Die Daten können bei Bedarf von Benutzerinnen und Benutzern abgefragt und ergänzt werden. Die Benutzerinnen und Benutzer haben Erwartungen an die Antwortzeit und Usability im Kontext einer interaktiven Verarbeitung. Sonderfall, wenn ein interaktiver Nachweisabruf nicht erfolgreich ist: Sollte ein Register gegenüber einem Onlinedienst nicht in der Lage sein, einen Nachweis interaktiv zu liefern, so besteht weiterhin die Möglichkeit, dass der Nachweis nach erfolgter Online-Antragstellung im Wege der Behörde-zu-Behörde Kommunikation über den nicht-interaktiven Kanal eingeholt wird, sofern hierfür eine rechtliche Grundlage gegeben ist.                                                              | [IT-PLR-E-02](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL3_Asynchrone_Prozesse.pdf)       |
| NOOTS-655 | Das NOOTS-Gesamtsystem SOLL nicht-interaktive Nachweisabrufe ermöglichen.                                                                                | Eine nicht-interaktive Verarbeitung bedeutet, dass eine Hintergrund-Verarbeitung bspw. bei System-zu-System-Kommunikation stattfindet. Sachbearbeiterinnen und Sachbearbeiter arbeiten als Nutzerinnen und Nutzer in mehreren Schritten an dem Fall.                                                                                                                                                                                    | [IT-PLR-E-02](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL3_Asynchrone_Prozesse.pdf)       |
| NOOTS-656 | Das NOOTS-Gesamtsystem MUSS die Kommunikation zwischen Data Consumern und Data Providern durch einen fachunabhängigen Nachweisabrufstandard ermöglichen. | Fachunabhängiger Nachweisabrufstandard: Der Nachweisabruf bei Data Providern erfolgt grundsätzlich zu gleichen rechtlichen, organisatorischen und technischen Bedingungen, sodass die Data Consumer nicht für verschiedene Nachweise jeweils neue Fachstandards und deren Schnittstellen implementieren müssen.                                                                                                                         | [IT-PLR-E-03](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL4_Nachweisabruf.pdf)       |
| NOOTS-657 | <p style="color:#184B76;">Das NOOTS-Gesamtsystem MUSS Abrufe von Nachweisen zu natürlichen Personen und Unternehmen im Sinne des § 3 Abs. 1 UBRegG ermöglichen.</p>                    | <p><p style="color:#184B76;">Unternehmen im Sinne des § 3 Abs. 1 UBRegG sind:</p></p> <ul><li><p style="color:#184B76;">Kaufleute im Sinne des Handelsgesetzbuchs</p> <li><p style="color:#184B76;">Genossenschaften im Sinne des Genossenschaftsgesetzes</p> <li><p style="color:#184B76;">Partnerschaften im Sinne des Partnerschaftsgesellschaftsgesetzes</p> <li><p style="color:#184B76;">Vereine im Sinne des Bürgerlichen Gesetzbuchs</p> <li><p style="color:#184B76;">wirtschaftlich Tätige im Sinne der Abgabenordnung:</p> <ul><li><p style="color:#184B76;">natürliche Personen, die wirtschaftlich tätig sind</p> <li><p style="color:#184B76;">juristische Personen und</p> <li><p style="color:#184B76;">Personenvereinigungen</p></ul> <li><p style="color:#184B76;">sowie weitere Unternehmen im Sinne des Siebten Buches Sozialgesetzbuch.</p>                                                                                                                                                                                                                                                                                                                                                                                       | [IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05)       |
| NOOTS-658 | Das NOOTS-Gesamtsystem MUSS einem Data Consumer den Abruf von Nachweisen aus Deutschland und anderen EU-Mitgliedstaaten ermöglichen.                     |                                                                                                                                                                                                                                                                                                                                                                                                                                         | [Art. 14 EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)     |
| NOOTS-690 | Das NOOTS-Gesamtsystem MUSS unterstützen, dass natürliche Personen behördlichen Datenaustausch unter Verwendung der IDNr nachvollziehen können.          | Datenaustausch betrifft Protokoll- und Inhaltsdaten der Register                                                                                                                                                                                                                                                                                                                                                                        | [§ 2 Abs. 3 RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf) |
| NOOTS-691 | Das NOOTS-Gesamtsystem MUSS bei einem Nachweisabruf die Verwendung der IDNr zur eindeutigen Identifikation von Personen ermöglichen.                     |                                                                                                                                                                                                                                                                                                                                                                                                                                         | [IT-PLR-B-07](https://www.it-planungsrat.de/beschluss/beschluss-2022-06)       |
| NOOTS-693 | Das NOOTS-Gesamtsystem MUSS gewährleisten, dass Kommunikation nur zwischen dazu autorisierten technischen Komponenten öffentlicher Stellen möglich ist.  | Verwendung der Definition von öffentlichen Stellen gem. § 2 BSDG                                                                                                                                                                                                                                                                                                                                                                        | [IT-PLR-B-07](https://www.it-planungsrat.de/beschluss/beschluss-2022-06)       |
| NOOTS-860 | Das NOOTS-Gesamtsystem MUSS den Abruf nationaler Nachweise durch andere EU-Mitgliedstaaten ermöglichen.                                                  |                                                                                                                                                                                                                                                                                                                                                                                                                                         | [Art. 14 EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)     |


#### Nicht-funktionale Anforderungen an das NOOTS-Gesamtsystem

| ID        | Anforderung                                                                                                                                                                                                                    | Ergänzende Erläuterung nach Bedarf                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | ISO-Kriterium      | Quelle      |
|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------|-------------|
| NOOTS-628 | Das NOOTS-Gesamtsystem SOLL bei interaktiven Nachweisabrufen von nationalen Nachweisen innerhalb von 40 s und MUSS innerhalb von 60 s eine Antwort liefern.                                                                    | Für die am Nachweisabruf beteiligten NOOTS-Komponenten sowie für Data Provider ergeben sich aus dieser Anforderung jeweils eine Antwortzeit von maximal drei Sekunden; die übrige Zeit wird für den Transport veranschlagt. Genaueres wird in den Komponentenkonzepten festgelegt.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Leistungseffizienz | PB NOOTS    |
| NOOTS-721 | Das NOOTS-Gesamtsystem MUSS Sicherheitseinstellungen bereits in der Grundkonfiguration seiner Dienste aktiviert haben (Security by default).                                                                                   | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Sicherheit         | [ITPLR-FAR](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/)   |
| NOOTS-535 | Das NOOTS-Gesamtsystem MUSS es NOOTS-Teilnehmern ermöglichen, sicherzustellen, dass in der Leistungsverwaltung personenbezogene Daten ausschließlich von berechtigten Personen abgerufen werden können.                        | Berechtigt können Personen sein, die entweder Daten für sich selbst abrufen oder vertretungsberechtigt für eine andere Person sind. Entsprechende (Vertretungs-)Berechtigungen zu definieren und zu prüfen, obliegt den Data Consumern bzw. Data Providern. Aus diesem Grund ist die Anforderung so formuliert, dass das NOOTS eine solche Prüfung nur ermöglicht, nicht aber selbst als verantwortliches System sicherstellt.                                                                                                                                                                                                                                                                                                                                            | Sicherheit         | PB NOOTS    |
| NOOTS-605 | Das NOOTS-Gesamtsystem MUSS eine Profilbildung rechtlich und technisch wirksam ausschließen.                                                                                                                                   | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Sicherheit         | [IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05) |
| NOOTS-729 | Das NOOTS-Gesamtsystem MUSS der Registermodernisierungsbehörde die Möglichkeit bieten, die Zulässigkeit der Abrufe durch geeignete Stichprobenverfahren sowie wenn dazu Anlass besteht zu prüfen.                              | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Sicherheit         | [RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)      |
| NOOTS-733 | Das NOOTS-Gesamtsystem MUSS Protokolldaten  (gemäß §9 IDNrG und §7 UBRegG) bezüglich Nachweisaustauschen im Standardfall zwei Jahre aufbewahren und danach unverzüglich löschen.                                               | <p><p style="color:#184B76;">Laut §9 Abs. 3 IDNrG kann eine längere Aufbewahrung zulässig sein, sofern diese zur Erfüllung eines Zwecks nach Absatz 2 erforderlich ist. In diesem Fall können Protokolldaten länger als zwei Jahre gespeichert werden. Die Dauer und die Gründe der Erforderlichkeit einer längeren Aufbewahrung sind zu dokumentieren.</p></p> *Hinweis: Genauere Protokollierungsvorgaben werden auf Komponentenebene beschrieben.*                                                                                                                                                                                                                                                                                                                                                                                                                                               | Sicherheit         | [RGR-01](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)      |
| NOOTS-735 | Das NOOTS-Gesamtsystem MUSS alle einschlägigen Datenschutzvorschriften einhalten.                                                                                                                                              | Einschlägig sind die Regelungen der Datenschutzgrundverordnung (DSGVO), insbesondere die Artikel 5-7 sowie 25, und des Bundesdatenschutzgesetzes.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Sicherheit         | [EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)       |
| NOOTS-738 | Das NOOTS-Gesamtsystem MUSS die einschlägigen nationalen IT-Sicherheitsstandards erfüllen.                                                                                                                                     | Laut BSI muss es eine Risikoanlyse gemäß IT-Grundschutz [(SQ-04)](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/IT-Grundschutz-Kompendium/it-grundschutz-kompendium_node.html) mit entsprechender Maßnahmenableitung pro Komponente geben. Anschließend ist zu bewerten, ob die pro Komponente identifizierten Maßnahmen für das System insgesamt die nötige Sicherheit gewährleisten.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Sicherheit         | [EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)       |
| NOOTS-739 | Das NOOTS-Gesamtsystem MUSS bei der Übertragung schützenswerter Daten deren Vertraulichkeit sicherstellen.                                                                                                                     | <p><p style="color:#184B76;">Definition von "Vertraulichkeit" gemäß BSI IT-Grundschutz-Kompendium Edition 2023: "Vertraulichkeit ist der Schutz vor unbefugter Preisgabe von Informationen. Vertrauliche Daten und Informationen dürfen ausschließlich Befugten in der zulässigen Weise zugänglich sein" [(SQ-04)](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/IT-Grundschutz-Kompendium/it-grundschutz-kompendium_node.html).</p></p> <p><p style="color:#184B76;">Dies gilt sowohl für den Zugriff auf Daten in Datenspeichern, als auch für den Zugriff auf übertragene Daten während des Transports.</p></p> <p><p style="color:#184B76;">Um die Vertraulichkeit der Daten beim Transport sicher zu stellen, müssen sie verschlüsselt übertragen werden. Hierzu fordert auch das §7 Abs 2 IDNrG: "Datenübermittlungen unter Nutzung einer Identifikationsnummer nach diesem Gesetz zwischen öffentlichen Stellen verschiedener Bereiche erfolgen über Vermittlungsstellen verschlüsselt in gesicherten Verfahren, die dem aktuellen Stand von Sicherheit und Technik entsprechen müssen."</p></p> *Hinweis: Das Thema Verschlüsselung wird im Arbeitspaket "IT-Sicherheit und Datenschutz" ausgearbeitet.*                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Sicherheit         | [EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)       |
| NOOTS-740 | Das NOOTS-Gesamtsystem MUSS die Integrität der übermittelten Nachweise sicherstellen.                                                                                                                                          | <p><p style="color:#184B76;">Definition von "Integrität" gemäß BSI IT-Grundschutz-Kompendium Edition 2023:</p></p> "Integrität bezeichnet die Sicherstellung der Korrektheit (Unversehrtheit) von Daten und der korrekten Funktionsweise von Systemen. Wenn der Begriff Integrität auf „Daten“ angewendet wird, drückt er aus, dass die Daten vollständig und unverändert sind. In der Informationstechnik wird er in der Regel aber weiter gefasst und auf „Informationen“ angewendet. Der Begriff „Information“ wird dabei für „Daten“ verwendet, denen je nach Zusammenhang bestimmte Attribute wie z. B. Autorenschaft oder Zeitpunkt der Erstellung zugeordnet werden können. Der Verlust der Integrität von Informationen kann daher bedeuten, dass diese unerlaubt verändert, Angaben zur verfassenden Person verfälscht oder Zeitangaben zur Erstellung manipuliert wurden" [(SQ-04)](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/IT-Grundschutz-Kompendium/it-grundschutz-kompendium_node.html).| Sicherheit         | [EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)       |
| NOOTS-737 | Das NOOTS-Gesamtsystem MUSS dem Empfänger von Nachweisen die Information über deren Herkunft eindeutig und überprüfbar zur Verfügung stellen.                                                                                  | Die Anforderung ist eng verküpft zur Anforderung NOOTS-740 zu Integrität von Nachweisen. In der Umsetzung empfiehlt es sich, beide gemeinsam zu denken.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Sicherheit         | [EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)       |
| NOOTS-741 | Das NOOTS-Gesamtsystem MUSS sicherstellen, dass Nachweise nur in notwendigem Umfang und für die notwendige Dauer innerhalb des NOOTS verarbeitet werden.                                                                       | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Sicherheit         | [EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)       |
| NOOTS-722 | Das NOOTS-Gesamtsystem MUSS so gestaltet sein, dass es nicht durch nur ein einziges IT-Produkt oder einen einzigen Hersteller umgesetzt werden kann.                                                                           | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Portierbarkeit     | [ITPLR-FAR](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/)   |
| NOOTS-717 | Das NOOTS MUSS Mechanismen vorsehen, die den Parallelbetrieb unterschiedlicher Versionen einzelner Standards und Schnittstellen erlauben, so dass Umstellungen nicht zeitgleich in allen betroffenen Systemen erfolgen müssen. | <p><p style="color:#184B76;">Diese Anforderung ergibt sich aus der Rahmenbedingung, dass sowohl Data Consumer als auch Data Provider zu heterogen sind, um Umstellungen an einem gemeinsamen Stichtag durchzuführen.</p></p> <p><p style="color:#184B76;">Erklärung: ein Parallelbetrieb unterschiedlicher Versionen von Standards (z.B. XNachweis), als auch Schnittstellen (z.B. RDN) innerhalb des Gesamtsystems NOOTS muss möglich sein. Die NOOTS-Komponenten und DC/ DP müssen bei ihrer Planung, Implementierung und Betrieb berücksichtigen, dass angebotene Schnittstellen verschiedene Versionen eines Standards parallel liefern können und genutzte Services ggf. mit unterschiedlichen Versionen antworten.</p></p> Bei der Einführung neuer Versionen von Schnittstellen und Standards muss eine Übergangszeit für den Parallelbetrieb mit einem festgelegten Enddatum für die Unterstützung abgelöster Versionen definiert werden.                                                    | Portierbarkeit     | PB NOOTS    |
| NOOTS-723 | Das NOOTS-Gesamtsystem MUSS modular aufgebaut sein.                                                                                                                                                          | Modular bedeutet in diesem Kontext, dass jeder Baustein eigenständig nutzbar sein und entsprechend unabhängig weiterentwickelt, aktualisiert und betrieben werden können soll. Dabei sind Schnittstellen stabil zu halten.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Wartbarkeit        | [ITPLR-FAR](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/)   |
| NOOTS-836 | Das NOOTS-Gesamtsystem MUSS eine effiziente Lokalisierung und Analyse von Fehlern wirksam unterstützen.                                                                                                                        | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Wartbarkeit        | PB NOOTS    |
| NOOTS-105 | Das NOOTS-Gesamtsystem MUSS für jede NOOTS-Komponente und NOOTS-Teilnehmer deren Schnittstellen so spezifizieren, dass deren Implementierungen durch Kompatibilitätstests verifiziert werden können.                           | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Wartbarkeit        | [ITPLR-FAR](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/)   |
| NOOTS-720 | Das NOOTS-Gesamtsystem SOLL soweit möglich und zweckmäßig bestehende Lösungen, u.a. des IT-Planungsrats, wiederverwenden oder weiterentwickeln.                                                                                | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Wartbarkeit        | [IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05) |
| NOOTS-726 | Das NOOTS-Gesamtsystem SOLL sich an dem Aufbau und den Standards des EU-OOTS orientieren und bei nationalen Abweichungen Interoperabilität mit dem EU-OOTS gewährleisten.                                                      | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Kompatibilität     | [IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05) |
| NOOTS-743 | Das NOOTS-Gesamtsystem SOLL die Vorgaben des Europäischen Interoperabilitätsrahmenwerks (EIF) erfüllen.                                                                                                                        | Näheres zum Europäischen Interoperabilitätsrahmenwerk in SQ-05. Ausblick: Es entsteht zur Zeit die EU-Verordnung "Interoperable Europe Act" (SQ-06), die ähnliche Aspekte adressiert. Sobald diese im Jahr 2024 verabschiedet wird, ist diese zusätzlich zu berücksichtigen.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Kompatibilität     | [IT-PLR-B-04](https://www.it-planungsrat.de/beschluss/beschluss-2021-05) |

Weitere Anforderung zu den folgenden Themen befinden sich in Bearbeitung
und werden, sobald möglich, in der Dokumentation ergänzt:

-   Antwortzeit bei nicht-interaktiven Nachweisabrufen, vgl. NOOTS-628
-   Verfügbarkeit des NOOTS
-   Verfügbarkeit im Katastrophenfall

## Architekturzielbilder und Umsetzungsstufen des NOOTS

Im Rahmen der Weiterentwicklung des NOOTS wurde das Architekturzielbild
des NOOTS
([IT-PLR-B-05](https://www.it-planungsrat.de/beschluss/beschluss-2021-25))
überarbeitet und in mehrere Umsetzungsstufen unterteilt. Dazu hat der
Programmbereich NOOTS, in Abstimmung mit der Gesamtsteuerung, die
NOOTS-Umsetzungsstufen 2023, 2025 und 2028 definiert. Neben sich aus
Konzeption und Umsetzung ergebenden Anpassungsbedarfen, die im
Architekturzielbild nachgehalten werden müssen, verfolgt die Aufteilung
insbesondere das Ziel, die Entwicklung des NOOTS zu verdeutlichen. Auch
fördert das Architekturzielbild die Kommunikation mit Stakeholdern, da
zielgruppenspezifische Sichten erstellt werden können. Außerdem ist es
mit dem Architekturzielbild möglich, Architekturmodelle aus
Umsetzungsprojekten und assoziierte Vorhaben einfacher in die
Gesamtarchitektur der Registermodernisierung einzugliedern.

Der Programmbereich NOOTS hat die NOOTS-Zielbilder 2023, 2025 und 2028
definiert.

-   Im Zielbild 2023 steht die termingerechte Erfüllung
    der SDG-Anschlussverpflichtung im Vordergrund. Aus diesem Grund
    liegt der Fokus der Umsetzung auf NOOTS Komponenten, die für den
    europaweit grenzüberschreitenden und automatisierten Austausch von
    Nachweisen erforderlich sind. In dieser Umsetzungsstufe wird es noch
    keine Unterstützung nationaler Nachweisabrufe geben.
-   Im Zielbild 2025 erfolgt der fortlaufende Aufbau der nationalen
    technischen Infrastruktur. Zu diesem Zeitpunkt wurden
    essenzielle *NOOTS* Komponenten umgesetzt und in Betrieb genommen.
    In dieser Stufe werden sowohl nationale als auch europäische
    Nachweisabrufe unterstützt.
-   Im Zielbild 2028 sind alle NOOTS Komponenten fertiggestellt und
    vollumfänglich skaliert in Betrieb genommen. In dieser
    Umsetzungsstufe steht der kontinuierliche Anschluss von Behörden im
    Vordergrund.

### Umsetzungsstufe Architekturzielbild 2023

In der Umsetzungsstufe 2023 liegt der Fokus der Umsetzung auf der
Erreichung der Fristen der SDG-VO und dem Anschluss an das EU-OOTS. Es
werden nur wenige ausgewählte Data Consumer und Data Provider, die
gemäß SDG-VO zum Abruf oder der Bereitstellung von europäischen
Nachweisen verpflichtet sind, an die NOOTS-Infrastruktur angeschlossen
werden. Alle Nachweisabrufe über das EU-OOTS erfolgen unter Verwendung
der Intermediären Plattform und über einen Access Point. Die roten
Pfeile im Schaubild beschreiben den Abruf von europäischen Nachweisen
durch Data Consumer ([Anwendungsfall
3](#anwendungsfall-3-abruf-von-nationalen-nachweisen-aus-eu-mitgliedstaaten-über-das-eu-oots)). Die schwarzen Pfeile
beschreiben den Abruf von nationalen Nachweisen durch Evidence
Requester ([Anwendungsfall
4](#anwendungsfall-4-abruf-von-europäischen-nachweisen-durch-nationale-data-consumer-über-das-europäische-once-only-technical-system-eu-oots)).

Nationale Nachweisabrufe sind im Zielbild 2023 noch nicht vorgesehen.
Dennoch gibt es zu diesem Zeitpunkt bereits umgesetzte nationale
Komponenten (mit einem roten Punkt markiert), die zur Vorbereitung auf
die nationalen Anwendungsfälle genutzt werden können.

![C:\\e713b841e00cb01e84487e2d934d4f07](./assets/images3/media/image3.png)
<br>**Abb. 3: Architekturzielbild des NOOTS 2023**</br>


Folgende Komponenten werden in der Umsetzungsstufe 2023 für
den SDG-Nachweisabruf genutzt:

-   Intermediäre Plattform: Die Intermediäre Plattform
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-06])</B></a>
    steht für EU-Nachweisabrufe bereit. Da es zu diesem Zeitpunkt noch
    keine Registerdatennavigation gibt, wird bei dem Abruf nationaler
    Nachweise aus EU-Mitgliedsstaaten (Anwendungsfall 4) kein
    dynamisches Routing stattfinden können. Stattdessen muss der
    Nachrichtenaustausch über Routingparameter in der Intermediären
    Plattform erfolgen. In der Umsetzungsstufe 2023 wird es zunächst nur
    eine Intermediäre Plattform geben, in späteren Umsetzungsstufen ist
    der dezentrale Betrieb mehrerer Intermediärer Plattformen
    vorgesehen.
-   Access Points: Eine Access Point
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-04])</B></a>
    steht für EU-Nachweisabrufe bereit.

In der Umsetzungsstufe 2023 werden nationale Nachweisabrufe noch nicht
unterstützt. Dennoch werden nationale Komponenten und übergreifende
Konzepte teilweise bereits umgesetzt.

-   IDM für Personen: Das IDM für Personen
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-10])</B></a>
    ist umgesetzt und der Standard XBasisdaten
    ([XS-03](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten))
    steht im XRepository bereit. Ab diesem Zeitpunkt können sich
    nachweisanfordernde Behörden auf den nationalen Nachweisabruf
    vorbereiten und eine Schnittstelle zum IDM für Personen
    implementieren, um die IDNr gem. XBasisdaten abzurufen.
-   Datenschutzcockpit: Das Datenschutzcockpit
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-09])</B></a>
    ist umgesetzt und der Standard XDatenschutzcockpit
    ([XS-02](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit))
    steht im XRepository bereit. Ab diesem Zeitpunkt können sich
    registerführende Behörden auf den nationalen Nachweisabruf
    vorbereiten und eine Schnittstelle zum Datenschutzcockpit
    implementieren.
-   IAM für Behörden: Das IAM für Behörden
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-05])</B></a>
    wird im Zielbild 2023 noch nicht umgesetzt sein. Stattdessen wird
    auf die bestehenden IAM-Lösungen der anderen Komponenten (z.B. IDM
    für Personen) zurückgegriffen.
-   XNachweis: Der Nachweisabrufstandard XNachweis steht im XRepository
    ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
    in einer ersten Umsetzungsstufe bereit. Der Fokus dieser
    XNachweis-Version liegt auf den für den europäischen Nachweisabruf
    notwendigen Nachrichten. Nachrichten für den nationalen
    Nachweisabruf (insb. unter Verwendung der IDNr), werden erst in
    späteren Versionen von XNachweis unterstützt.
-   V-PKI: Die Verwaltungs-PKI
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-16])</B></a> 
    stellt Zertifikate für die NOOTS-Komponenten bereit.

                                                                                        
### Umsetzungsstufe Architekturzielbild 2025

In der Umsetzungsstufe 2025 liegt der Fokus der Umsetzung auf der
nationalen technischen Infrastruktur. Ausgewählte Register werden dazu
ertüchtigt, sich an das *NOOTS* anzuschließen. Alle Nachweisabrufe über
das EU-OOTS erfolgen weiterhin unter Verwendung der Intermediären
Plattform und des Access Points. Neben den europäischen Nachweisabrufen
(rote und schwarze Pfeile) werden nun auch nationale Nachweisabrufe
(grüne Pfeile) unterstützt. Zu diesem Zeitpunkt sind alle essenziellen
NOOTS-Komponenten umgesetzt. Ein vollumfänglicher Anschluss aller
nachweisabrufenden und nachweisliefernden Stellen steht in der
Umsetzungsstufe 2025 noch nicht im Fokus, ist aber möglich.

![C:\\1db796fe4b12805a0957e9584a4915e0](./assets/images3/media/image4.png)
<br>**Abb. 4: Architekturzielbild des NOOTS 2025**</br>


Zunächst werden diejenigen Komponenten aus der Umsetzungsstufe 2025
beschrieben, die initial umgesetzt werden. Im späteren Verlauf wird dann
beschrieben, welche Anpassungen an Komponenten der Umsetzungsstufe 2023
vorgenommen werden:

-   Registerdatennavigation: Die Registerdatennavigation
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-07])</B></a>
    ist umgesetzt und kann für nationale und europäische Nachweisabrufe
    verwendet werden. Das Zuständigkeitsverzeichnis ist zu diesem
    Zeitpunkt noch nicht vollständig, sondern wird mit dem Anschluss
    neuer Register fortlaufend befüllt. Die Schnittstelle zur
    Registerdatennavigation ist spezifiziert und veröffentlicht.
-   Vermittlungsstelle: Die Vermittlungsstelle
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-08])</B></a>
    ist umgesetzt und muss für bereichsübergreifende nationale
    Nachweisabrufe unter Verwendung der IDNr verwendet werden.

An folgenden Komponenten, die bereits seit der Umsetzungsstufe 2023
nutzbar sind, werden Anpassungen vorgenommen:

-   Intermediäre Plattform: Die Intermediäre
    Plattform <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-06])</B></a>
    wird weiterhin für die Anbindung an das EU-OOTS verwendet. Im
    Unterschied zur Umsetzungsstufe 2023 kann nun auf die
    Registerdatennavigation zurückgegriffen werden, was ein dynamisches
    Routing und insbesondere die Anbindung von dezentralen Registern
    ohne zentralisierende Strukturen ermöglicht. Dazu stellt
    die Intermediäre Plattform generische Benutzerdialoge bereit, über
    die notwendige Routingparameter zur Zuständigkeitsfindung vom
    Antragsstellenden abgefragt werden können.
-   IAM für Behörden: Das IAM für Behörden
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-05])</B></a>
    wird im Zielbild 2025 noch nicht umgesetzt sein. Stattdessen wird
    auf die bestehenden IAM-Lösungen der anderen Komponenten (z.B. IDM
    für Personen) zurückgegriffen.
-   IDM für Unternehmen: Das IDM für Unternehmen
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-11])</B></a>
    ist umgesetzt und der Standard XUnternehmen
    ([XS-04](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:basismodul))
    steht im XRepository bereit. Ab diesem Zeitpunkt können sich
    nachweisanfordernde Behörden eine Schnittstelle zum IDM für
    Unternehmen implementieren und die W-IDNr gem. XUnternehmen abrufen.
-   Datenschutzcockpit: Das Datenschutzcockpit
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-09])</B></a>
    ist umgesetzt und der Standard XDatenschutzcockpit
    ([XS-02](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit))
    steht im XRepository bereit. Bürgerinnen und Bürger können das
    Datenschutzcockpit nutzen, um bereichsübergreifende Nachweisabrufe
    unter Verwendung der IDNr nachzuvollziehen.
-   XNachweis: Der Nachweisabrufstandard XNachweis steht im XRepository
    ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
    in einer überarbeiten Version bereit, die nun auch Nachrichten für
    den nationalen Nachweisabruf umfasst.

### Umsetzungsstufe Architekturzielbild 2028

In der Umsetzungsstufe 2028 sind alle NOOTS-Komponenten vollständig
umgesetzt. Im Unterschied zur Umsetzungsstufe 2025, in der nur eine
begrenzte Zahl europäischer Nachweisabrufe (rote und schwarze Pfeile)
und nationaler Nachweisabrufe (grüne Pfeile) möglich war, nimmt die Zahl
der angeschlossenen nationalen und europäischen nachweisabrufenden und
nachweisliefernden Stellen kontinuierlich zu. Zudem sind spätestens mit
dieser Umsetzungsstufe alle erforderlichen Instanzen von dezentralen
NOOTS-Komponenten in Betrieb genommen.

Die einzig neue Komponente in der Umsetzungsstufe 2028 ist das IAM für
Behörden. Die in den Umsetzungsstufen 2023 und 2025 umgesetzten
Komponenten werden in der rechtlich geforderten Anzahl dezentral
betrieben.

-   IAM für Behörden: Das IDM für Behörden
    <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-05])</B></a>
    ist umgesetzt und löst damit die dezentralen IAM-Lösungen der
    NOOTS-Komponenten ab. Das IAM für Behörden ermöglicht die sichere
    Authentifizierung und Autorisierung von öffentlichen Stellen beim
    Zugriff auf Register und die technischen Komponenten des NOOTS.

An folgenden Komponenten, die bereits seit der Umsetzungsstufe 2023
nutzbar sind, werden Anpassungen vorgenommen:

-   XNachweis: Der Nachweisabrufstandard XNachweis
    ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis))
    steht im XRepository in einer finalen Version bereit, die alle
    Anforderungen vollständig abdeckt.

![C:\\2de841726a8a5493f8860c1ef9d32038](./assets/images3/media/image5.png)
<br>**Abb. 5: Architekturzielbild des NOOTS 2028**</br>


### Kurzbeschreibung der Rollen im NOOTS

| Bezeichnung                      | Beschreibung                                                                                                                                                                                                                                                                                                                                                    | 
|----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Data Consumer (AD-NOOTS-01)](https://bmi.usercontent.opencode.de/noots/Glossar/)      | <p><p style="color:#184B76;">Ein Data Consumer ist ein NOOTS-Teilnehmer zum Abruf von Nachweisen.</p></p> Ein NOOTS-Teilnehmer ist ein registriertes technisches Verfahren zur Teilnahme in einer bestimmten Weise (Teilnahmeart), authentifizierbar durch ein Zertifikat.|
| [Data Provider (AD-NOOTS-02)](https://bmi.usercontent.opencode.de/noots/Glossar/)      | <p><p style="color:#184B76;">Ein Data Provider ist ein NOOTS-Teilnehmer zur Lieferung von Nachweisen für ein Nachweissubjekt.</p></p> <p><p style="color:#184B76;">Ein NOOTS-Teilnehmer ist ein registriertes technisches Verfahren zur Teilnahme in einer bestimmten Weise (Teilnahmeart), authentifizierbar durch ein Zertifikat.</p></p> Ein Nachweissubjekt ist eine natürliche Person oder ein Unternehmen im Sinne des § 3 Abs. 1 UBRegG, für die oder das ein Nachweis ausgestellt wird. |                                                                                                                                                        
| Evidence Requester ([[AD-NOOTS-01]](https://bmi.usercontent.opencode.de/AD-NOOTS-01_%2BAnschlusskonzept%2BData%2BConsumer%2B_DC_/#nachweisanfordernde-stelle)) | <p><p style="color:#184B76;">Ein Evidence Requester ist eine Rolle im EU-OOTS, die dazu berechtigt ist (entweder durch Entscheidung des Antragsstellenden oder Rechtsgrundlage) Daten (u.a. Nachweise) auf elektronischem Weg von Evidence Providern (über das EU-OOTS) im Kontext der Leistungsverwaltung abzurufen.</p></p> <p><p style="color:#184B76;">Beim Datenabruf muss der Evidence Requester oder gegebenenfalls die Intermediäre Plattform die Vollständigkeit und Rechtmäßigkeit der Nachweisanfrage sicherstellen. Dabei muss gewährleistet werden, dass die Nachweise für das jeweilige Verfahren, von Nutzerinnen und Nutzern angefordert werden oder eine Rechtsgrundlage zum Abruf der Nachweise besteht.</p> Hinweis: Wenn von deutschen Stellen ein Nachweis aus dem EU-Ausland abgerufen werden soll, übernimmt die Intermediäre Plattform die Rolle</p> des Evidence Requesters im EU-OOTS. Evidence Requester werden rechtlich als “nachweisanfordernde Behörde” bezeichnet (Artikel 1 DVO EU 2022/1463).                                                                    |
| Evidence Provider ([[AD-NOOTS-02]](https://bmi.usercontent.opencode.de/AD-NOOTS-02_%2BAnschlusskonzept%2BData%2BProvider%2B_DP_/#ablauf-einer-interaktiven-nachweislieferung)) | <p><p style="color:#184B76;">Ein Evidence Provider ist eine Rolle im EU-OOTS, die dazu verpflichtet ist, Daten (u.a. Nachweise) auf elektronischem Weg an Evidence Requester (über das EU-OOTS) im Kontext der Leistungsverwaltung bereitzustellen.</p></p> <p><p style="color:#184B76;">Der Evidence Provider oder gegebenenfalls die Intermediäre Plattform muss prüfen, ob die angeforderten Nachweise, die sich in seinem bzw. ihrem Besitz befinden, dem Antragstellenden gemäß Artikel 16 DVO (EU) 2022/1463 zugeordnet werden können, ob der Antragstellende berechtigt ist, den angeforderten Nachweis zu verwenden.</p></p> Hinweis: Wenn von europäischen Stellen ein Nachweis aus dem Deutschland abgerufen werden soll, übernimmt die Intermediäre Plattform die Rolle des Evidence Providers im EU-OOTS. Evidence Provider werden rechtlich als “nachweisliefernde Stelle” bezeichnet (OZGÄndG, Artikel 2: Änderung des E-Government-Gesetzes §5).  |


### Kurzbeschreibung der Komponenten im NOOTS

 | Bezeichnung                                              | Beschreibung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|----------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Datenschutzcockpit <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-09])</B></a>                         | Das Datenschutzcockpit (DSC), nach § 10 des Onlinezugangsgesetzes (OZG), hat die Aufgabe, Datenübermittlungen unter Verwendung der Identifikationsnummer (IDNr) anzuzeigen. Mit der Einführung des DSC werden Bürgerinnen und Bürger die Möglichkeit erhalten, einfach und digital nachvollziehen zu können, welche öffentlichen Stellen Daten, zu welchem Zeitpunkt und zu welchem Zweck unter Verwendung der IDNr ausgetauscht haben. Es fördert demnach Transparenz und den Abbau von Vorbehalten gegen die Einführung der IDNr. Das DSC selbst darf keine Daten über Personen speichern. Das DSC übernimmt im Nachweisabrufprozess selbst keine Funktion, sondern dient der transparenten Nachverfolgung von Nachweisabrufen unter Verwendung der Identifikationsnummer. Dazu greift das DSC auf die Protokolldaten der Register zu. Datenabrufe, die ohne Verwendung der IDNr stattfinden, können im DSC nicht eingesehen werden. |
| Identity Management für Personen <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-10])</B></a>           | Das Identity Management (IDM) für Personen übernimmt die Bereitstellung der Identifikationsnummer (IDNr) anhand von personenbezogenen Basisdaten (mindestens Familiennamen, Wohnort, Postleitzahl und Geburtsdatum), die zur zweifelsfreien Identifikation von Bürgerinnen und Bürgern in Registern benötigt wird. Damit wird eine zentrale Grundlage für den automatisierten Nachweisabruf geschaffen.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| Identity Management für Unternehmen <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-11])</B></a>        | Das Identity Management (IDM) für Unternehmen übernimmt die Bereitstellung der bundeseinheitlichen Wirtschaftsnummer (beWiNr) anhand von Unternehmensbasisdaten (Identifikationsnummern, Stamm- und Metadaten), die zur zweifelsfreien Identifikation von Unternehmen im Sinne des § 3 Abs. 1 UBRegG  in Registern benötigt wird. Damit wird eine zentrale Grundlage für den automatisierten Nachweisabruf geschaffen.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| Registerdatennavigation <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-07])</B></a>                    | Die Registerdatennavigation (RDN) ist der zentrale Zuständigkeitsdienst im NOOTS. Auf Anfrage liefert die RDN einem Data Consumer die Information, von welchem technischen Dienst dieser einen gewünschten Nachweis abrufen kann, welche Behörde diesen Dienst betreibt und welche Verbindungsparameter für einen Abruf erforderlich sind. Die RDN wird sowohl für nationale Nachweisabrufe eingesetzt als auch beim Nachweisaustausch mit dem EU-Ausland.                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| Identity and Accessmanagement für Behörden <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-05])</B></a> | Das Identity- and Accessmanagement (IAM) für Behörden ermöglicht die sichere Authentifizierung und Autorisierung von öffentlichen Stellen beim Zugriff auf Register und die technischen Komponenten des NOOTS. Dazu stellt das IAM für Behörden die Einhaltung der Governance für eine gesetzeskonforme Erteilung von Identitäten sowie von berechtigungsrelevanten Informationen (Rollen der Identitäten) sicher, protokolliert und überwacht die Zugriffe. Das IAM für Behörden ist ausschließlich auf nationale Identitäts- und Berechtigungsprüfungen beschränkt. Für Abrufe aus dem EU-Ausland sind derzeit keine IAM-Komponenten vorgesehen. Die Authentifizierung von Nutzenden des NOOTS (z.B. Bürgerinnen und Bürger oder Unternehmen) ist nicht die Aufgabe des IAM für Behörden.                                                                                                                                            |
| Vermittlungsstellen <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-08])</B></a>                        | Vermittlungsstellen (VS) sind dritte öffentliche Stellen, die gem. § 7 Abs. 2 IDNrG ([RGR-01]) bei Datenübermittlungen unter Nutzung einer IDNr zwischen öffentlichen Stellen verschiedener Bereiche zum Einsatz kommen müssen. Sie sind für den sicheren, verlässlichen und nachvollziehbaren Transport elektronischer Nachrichten zuständig. Sie kontrollieren und protokollieren abstrakt die Übermittlungsberechtigung, also ob ein Data Consumer Daten von einem Data Provider zu einem anzugebenden Zweck abrufen darf. Liegt die Übermittlungsberechtigung abstrakt nicht vor, werden keine personenbezogenen Daten übermittelt. Vermittlungsstellen müssen ihre Aufgabe ohne Kenntnis der Nachrichteninhalte erbringen.                                                                                                                                                                                                        |
| Intermediäre Plattformen <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-06])</B></a>                   | Intermediäre Plattformen vermitteln zwischen dem NOOTS und dem EU-OOTS, indem Nachweisabfragen aus dem EU-Ausland in nationale Abfragen umgewandelt werden und umgekehrt. Damit stellen die Intermediären Plattformen eine bedeutende Komponente für die Umsetzung der Verpflichtung zum Anschluss an das EU-OOTS gem. SDG-VO ([EU-03]) dar. Durch den Einsatz Intermediärer Plattformen (IP) können Funktionen für grenzüberschreitenden Nachweisaustausch gebündelt werden, die sonst durch die Data Consumer und Data Provider selbst erbracht werden müssten.                                                                                                                                                                                                                                                                                                                                                                      |
| Access Point <a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-04])</B></a>                               | Der “eDelivery Access Point” ist eine eigenständige und fachunabhängige Komponente für den Nachweisabruf im europäischen Kontext (gemäß SDG). Er verbindet die Intermediäre Plattform mit den eDelivery-Access Points anderer EU-Mitgliedstaaten und übernimmt den Übergang vom Transportstandard des NOOTS (aktuell OSCI/XTA) in den Transportstandard des EU-OOTS (AS4).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |

## Abgrenzungen

### Begriff Nachweis

Der Begriff "Nachweis" spielt im NOOTS sowie im EU-OOTS eine zentrale
Rolle. In diesem Kapitel wird er zunächst definiert und dann näher
differenziert.

In Artikel 3 der SDG-VO werden Nachweise beschrieben als "alle
Unterlagen oder Daten, einschließlich Text-, Ton-, Bild- oder
audiovisuelle Aufzeichnungen, unabhängig vom verwendeten Medium, die von
einer zuständigen Behörde verlangt werden, um Sachverhalte oder die
Einhaltung der in Artikel 2 Absatz 2 Buchstabe b genannten
Verfahrensvorschriften
nachzuweisen" ([EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)).

Gemäß § 5 Abs. 2 der Änderung des E-Government-Gesetzes im Entwurf zum
OZG-Änderungsgesetz (OZGÄndG) sind Nachweise \"Unterlagen und Daten
jeder Art unabhängig vom verwendeten Medium, die zur Ermittlung des
Sachverhalts geeignet sind.\" 

Gemäß dieser Definitionen sind Nachweise nicht durch technische Merkmale
definiert, sondern durch ihre Verwendung zum Nachweis eines
Sachverhalts. Das Maß, in dem ein Nachweis elektronisch verarbeitet
werden kann, hat dabei erhebliche Auswirkungen auf den Nutzen, den der
Nachweisabruf für die Verwaltungsdigitalisierung stiften kann. Daher
definiert das Reifegradmodell unterschiedliche Digitalisierungsgrade von
Nachweisabrufen
<a href="https://bmi.usercontent.opencode.de/noots/Glossar/" target= "_blank"><B>([NOOTS-13])</B></a>.
                                                                                                  
### Begriff Registerdatenabruf

Der Abruf von Nachweisen erfolgt nur in Antragsverfahren oder
Fachverfahren zur Umsetzung des Once-Only Prinzips oder der SDG-VO zum
Zweck des Nachweises von Sachverhalten. In anderen Kontexten
(Verwaltungsdigitalisierung, Registerzensus, Wissenschaft) werden
dieselben Daten zu einem anderen Zweck benötigt. Da der Nachweisbegriff
hier nicht einschlägig ist, wird in diesen Fällen von
Registerdatenabrufen gesprochen. Technisch existiert kein Unterschied
zwischen Registerdaten und Nachweisen. Auch das Reifegradmodell kann auf
Registerdaten angewendet werden.

### Standardisierung der Nachweistypen

Damit Nachweise automatisiert verarbeitet werden können, muss Einigkeit
unter allen beteiligten Akteuren über Semantik, Struktur und Syntax der
Nachweise bestehen. Dazu ist eine fachliche Standardisierung aller
Nachweistypen erforderlich. Diese Standardisierung ist nicht im Umfang
des NOOTS. Im Dokument \"Fachdatenkonzept\" des PB Register wird
beschrieben, wie der automatisierte Nachweisaustausch von Fachdaten
zwischen Data Consumer und Data Provider in verschiedenen Reifegraden
über das NOOTS fachlich gewährleistet werden kann. Die hierfür
benötigten Bedingungen, wie bspw. ein semantischer Fachdatenstandard,
werden entsprechend definiert. Somit bildet das Fachdatenkonzept die
fachliche Grundlage für den automatisierten Datenaustausch. Weitere
Informationen werden in einem zukünftigen Release ergänzt.

### Befähigung der Register

Die Register müssen nicht nur technisch an das NOOTS angebunden werden.
Sie müssen auch befähigt werden, Nachweise in angemessener Zeit zu
liefern. Dazu gehören bspw.

-   die Einspeicherung der Identifikationsnummer und bundeseinheitlichen
    Wirtschaftsnummer, wenn eine gesetzliche Verpflichtung dazu besteht,
-   die Bereinigung des Datenbestands, z.B. durch Auflösen von
    Doubletten,
-   die Schaffung technischer Voraussetzungen, um Verfügbarkeit und
    Antwortzeit zu gewährleisten.

Im Einzelfall ergeben sich daraus weitere Herausforderungen, bspw. wenn
das Register keine Datenhoheit über den eigenen Datenbestand hat (bspw.
Spiegelregister). Die Steuerung und Umsetzung dieser Maßnahmen liegt
nicht im Umfang des NOOTS. Das NOOTS definiert lediglich
Anschlussbedingungen, die einen Nachweisabruf über das NOOTS technisch
grundsätzlich möglich machen.

## Exkurs: Nationale Anbindung an das EU-OOTS

Die Single Digital Gateway Verordnung der Europäischen Union (Verordnung
(EU) 2018/1724, kurz: SDG-VO) setzt das Ziel des grenzüberschreitenden
automatisierten Austausches von Nachweisen nach dem Once-Only-Prinzip
([EU-03](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)).
Für Bürgerinnen, Bürger und Unternehmen erübrigt sich damit das
mehrfache Bereitstellen von Nachweisen, sofern sie einer Übermittlung
des jeweiligen Nachweises zustimmen.

Die EU hat zur Umsetzung der SDG-VO das europäische Once-Only Technical
System (EU-OOTS) errichtet, an das alle zum Anschluss verpflichteten
nationalen Behörden bis zum 12.12.2023 angeschlossen sein mussten. Für
Deutschland wurde unter Leitung des SDG-Koordinators mithilfe der
Evidence Survey ermittelt und auf der OZG-Informationsplattform
dokumentiert, welche Onlinedienste und welche Register mit den folgenden
Auswirkungen betroffen sind:

-   Für die betroffenen Onlinedienste gilt nach Artikel 6 der SDG-VO die
    Pflicht der vollständigen Online-Bereitstellung von Verfahren sowie
    die Pflicht, Nachweisabrufe über das EU-OOTS gemäß Artikel 14 der
    SDG-VO in das EU-Ausland zu ermöglichen.
-   Für die betroffenen Register gilt nach Artikel 14 der SDG-VO die
    Pflicht der vollständigen Online-Bereitstellung von Nachweisen sowie
    die Pflicht, Nachweisabrufe über das EU-OOTS aus dem EU-Ausland zu
    ermöglichen.

### Relevanter IT-Planungsratsbeschluss 2022/34

In der 39. Sitzung hat der IT-Planungsrat den Beschlüssen des
Lenkungskreis Registermodernisierung zugestimmt und entschieden, dass

-   der Anschluss an das EU-OOTS über Intermediäre Plattformen
    (technische Komponenten i.S.v. Artikel 1 Nr. 6 DVO zu Artikel 14
    SDG-VO) erfolgen soll;
-   der Anschluss öffentlicher Stellen an das EU-OOTS ausschließlich
    über Intermediäre Plattformen erfolgen soll. Es ist nicht
    vorgesehen, dass sich nationale Behörden direkt an das EU-OOTS
    anschließen. Dadurch kann verhindert werden, dass spezifische
    Funktionalitäten, die nur für die EU-Anbindung benötigt werden, in
    einer Vielzahl von dezentralen Registern und Onlinediensten
    implementiert werden müssen;
-   ein SDG-Connectors als Produkt des IT-Planungsrates entwickelt
    werden soll. Bei dem SDG-Connector handelt es sich um eine Sammlung
    von standardisierten und wiederverwendbaren Softwarebausteinen,
    darunter die eigenständige, fachunabhängige und lauffähige
    Komponente für den Nachweisabruf im europäischen Kontext, dem
    sogenannten "eDelivery Access Point".

### Zusammenwirken Intermediäre Plattform und eDelivery Access Point

Der Anschluss des NOOTS an das EU-OOTS erfolgt durch ein Zusammenspiel
zwischen der Intermediären Plattform und dem eDelivery Access Point. Der
eDelivery Access Point verbindet die Intermediäre Plattform mit den
eDelivery-Access Points anderer EU-Mitgliedstaaten.

Sowohl die Intermediäre Plattform als auch der eDelivery Access Point
erbringen spezifische Funktionen beim europäischen Nachweisabruf.

-   Die Intermediäre Plattform verantwortet die Fachnachricht und
    überführt dabei eine nationale XNachweis-Nachricht in eine
    europäische EDM-Nachricht und umgekehrt.
-   Der eDelivery Access Point verantwortet die Kommunikation mit dem
    EU-OOTS und tauscht dabei Nachrichten mit anderen Access Points im
    Transportstandard des EU-OOTS (AS4) aus.

### Nutzung der Common Services der EU-Kommission

Um den Austausch von Nachweisen zwischen Evidence Requestern
und Evidence Providern zu unterstützen, verwendet das EU-OOTS Dienste,
die in Artikel 4 der Durchführungsverordnung als Common Services
bezeichnet werden. Für die Dienste Evidence Borker und Data Service
Directory lässt Artikel 8 DVO den Mitgliedstaaten die Wahl, ihre Daten
zu dem zentralen Verzeichnis der Europäischen Kommission zuzuliefern
oder ein eigenes nationales Verzeichnis für diese Aufgaben
bereitzustellen.

| Common Service <p><p style="color:#184B76;">Rechtsgrundlage</p>        | Funktion                                                                                                                                                                                                                                                                                          | Verwendung durch die Bundesrepublik Deutschland                                                                                                                                  |
|------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Evidence Broker Artikel 6 SDG-DVO        | Dienst, mit dem ein Evidence Requester bestimmen kann, welchen Nachweistyp er von einem anderen Mitgliedstaat für einen bestimmten Zweck in einem bestimmten Kontext anfordern kann.                                                                                                              | Deutschland wird gemäß [IT-Planungsrat-Beschluss 2022/22](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL5_DSD.pdf) den zentralen Evidence Broker der EU-Kommission nutzen; eine nationale Instanz des Evidence Brokers wird es nicht geben. |
| Data Service Directory Artikel 5 SDG-DVO | Dienst, mit dem ein Evidence Requester Informationen über den Data Service erhalten kann, der in einem anderen Mitgliedstaat die gewünschten Nachweise bereitstellt.                                                                                                                              | Deutschland wird das zentrale Data Service Directory der EU-Kommission nutzen; eine nationale Instanz des DSD wird es nicht geben.</p> Zur Begründung siehe Hinweis unterhalb der Tabelle.                                               |                                                                                          |
| Semantic Repository Artikel 7 SDG-DVO    | Archiv semantischer Spezifikationen von Nachweistypen, die mit dem Evidence Broker und dem DSD verknüpft sind und aus Definitionen von Namen, Datentypen und Datenelementen bestehen, um das gegenseitige Verständnis der Beteiligten beim Austausch von Nachweisen über das OOTS sicherzustellen | n.a. |


Hinweis zur Verwendung des zentralen DSD: Im IT-Planungsrat-Beschluss
2022/22 vom Juni 2022 wurde ursprünglich festgelegt, ein
nationales *Data Service Directory* zu entwickeln bzw. dieses als
Funktionalität in die Komponente Registerdatennavigation aufzunehmen.
Begründung für diese Entscheidung war, dass die komplexen Routinglogiken
durch die deutsche Registerlandschaft in einer nationalen Komponente
besser pflegbar wären und außerdem nicht redundant zur
Registerdatennavigation in einem europäischen Verzeichnisdienst gepflegt
werden sollten. Im weiteren Projektverlauf wurde aber im November
desselben Jahres der verpflichtende Einsatz Intermediärer Plattformen
für den OOTS-Anschluss beschlossen
([IT-PLR-B-09](https://www.it-planungsrat.de/beschluss/beschluss-2022-34)).
Diese fungieren gegenüber dem EU-Ausland als Evidence Provider und
kapseln damit die nationale Registerwelt. Da die Intermediären
Plattformen die im DSD einzutragenden Data Services bereitstellen und es
voraussichtlich nur eine ein- oder niedrige zweistellige Anzahl von
Intermediären Plattformen geben wird, greift das Argument der komplexen
Pflege nicht mehr. Daher wurde im PB NOOTS beschlossen, die Data
Services der Intermediären Plattformen im zentralen DSD zu hinterlegen
und nicht in einem nationalen Dienst.

> Redaktioneller Hinweis
>
> <p>Das Pflegekonzept für die Common Services werden durch den PB OZG /
> EU OOTS bzw. den SDG-Koordinator geliefert.

### Nationale zentrale Kontaktstelle

Jeder Mitgliedstaat benennt gem. Artikel 21 SDG-DVO (2022/1463) eine
zentrale Kontaktstelle für technische Unterstützung, um den Betrieb und
die Wartung der einschlägigen Komponenten des OOTS, für die der
jeweilige Mitgliedstaat gemäß Abschnitt 9 zuständig ist,
sicherzustellen. Die nationale zentralen Kontaktstelle hat eine Vielzahl
von Aufgaben, darunter das

-   Untersuchen und Beheben von Sicherheitsverletzungen (Artikel 21 (2)
    b SDG-DVO) und das Unterrichten anderer Kontaktstellen über alle
    Vorgänge, die zu einer (mutmaßlichen) Sicherheitsverletzung führen
    können (Artikel 21 (2) c SDG-DVO)
-   Untersuchen und Lösen von Ausfällen einzelner Komponenten (z.B.
    eDelivery Access Points oder IP) oder der ganzen nationalen Domäne
    (NOOTS),
-   Überprüfen von Nachweisanfragen (und der Historie) bei Zweifel an
    der Rechtmäßigkeit (Artikel 21 (3) SDG-DVO),
-   Erstellen von Risikomanagementplänen zur Ermittlung und Bewertung
    von Risken und potenziellen Auswirkungen und zur Planung von
    geeigneten technischen und organisatorischen Gegenmaßnahmen (Artikel
    24 (4) SDG-DVO),
-   Bereitstellen von Fachwissen und Beratung zur Unterstützung der
    Systeme des OOTS und anzuschließender Behörden (Artikel 21 (2)
    a SDG-DVO),
-   Vermitteln von Kontakten zu anderen Kontaktstellen (Artikel 21 (2)
    a SDG-DVO).

### Rollen beim Nachweisabruf über das EU-OOTS

Definitionen der Rollen im NOOTS und EU-OOTS finden sich im [Kapitel
Rollen](#kurzbeschreibung-der-rollen-im-noots) der
High-Level-Architecture. In diesem Kapitel soll das Zusammenwirken der
Rollen beschrieben werden. Der verpflichtende Einsatz einer
Intermediären Plattform hat zur Folge, dass gegenüber dem EU-Ausland
nationale Data Consumer nicht direkt als Evidence Requester auftreten
und nationale Data Provider nicht direkt als Evidence Provider
auftreten. Abhängig davon, ob ein Nachweis über das EU-OOTS aus einem
Mitgliedstaat oder aus Deutschland abgerufen wird, fungiert die
Intermediäre Plattform stets als Stellvertreterin und Proxy für
Nachweisabrufe.

![C:\\84baf94caf3710cb703f606cc3774435](./assets/images3/media/image6.png)
<br>**Abb. 6: Rollenmodell im NOOTS unter Verwendung der Intermediären Plattform**</br>


## Anwendungsfälle des NOOTS

In diesem Kapitel werden die zentralen Anwendungsfälle der
Registermodernisierung beschrieben. Dazu wird das Architekturzielbild
2028 des NOOTS als Grundlage genutzt, Die Modellierung erfolgt in
Anlehnung an die Prozessmodellierung der EU-TDD unter Verwendung der
ArchiMate-Notation
([EU-02](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents)).
Jedes Ziel der Registermodernisierung wird durch mindestens einen
Anwendungsfall adressiert.

Folgende Anwendungsfälle werden erst in späteren Versionen dieses
Dokuments modelliert werden:

-   Anwendungsfall 5: Abruf von Registerdaten für den Registerzensus
-   Anwendungsfall 6: Abruf von Registerdaten für die Wissenschaft

### Annahmen

| Annahme | Beschreibung                                                                                                                                                                                                 |
|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ANN-001 | <ul><li><p style="color:#184B76;">In nationalen Anwendungsfällen ist dem Data Consumer bekannt, welche Nachweise für die Erbringung einer Verwaltungsleistung abgefragt werden müssen.</p></ul>                                                         |
| ANN-002 | <ul><li><p style="color:#184B76;">In nationalen Anwendungsfällen ist dem Data Consumer bekannt, welche Zuständigkeitsparameter für die Ermittlung der zuständigen Behörde (dem technischen Endpunkt des Data Service) abgefragt werden müssen.</p></ul> |
| ANN-003 | <ul><li><p style="color:#184B76;">In europäischen Anwendungsfällen wird dem Data Consumer bekannt sein, an welche Intermediäre Plattform dieser eine XNachweis-Anfrage schicken muss, um einen europäischen Nachweisabruf auszulösen.</p></ul>          |


### Hinweise zur Modellierung der Anwendungsfälle

-   In der Regel werden für einen Antrag mehrere Nachweise benötigt.
    Dann müssen die Prozessschritte für den Nachweisabruf mehrfach
    durchlaufen werden.
-   Zweck des Nachweisabrufs ist es, abgerufene Nachweise zusammen mit
    dem Antrag bei der zuständigen Behörde einzureichen. Weder das
    Antragsverfahren noch das für den Nachweis zuständige Register
    liegen im Umfang des NOOTS, weshalb das Ausfüllen des Antrags, das
    Ausstellen von Nachweisen und die Antragseinreichung nicht explizit
    beschrieben werden.
-   Auf eine detaillierte (technische) Betrachtung der zugrundeliegenden
    Transportinfrastruktur sowie der Prozesse zur Sicherstellung von
    IT-Sicherheit und Datenschutz (Ausstellung von Zertifikaten, Ver-
    und Entschlüsselung von Nachrichten, Signatur, Token-Prüfung, etc.)
    wird in dieser Darstellung verzichtet.
    -   Diese (Sub-) Prozesse sind für den nationalen und europäischen
        Nachweisabruf essenziell, werden durch alle Anwendungsfälle
        adressiert und in eigenen Konzepten detailliert beschrieben.
-   In der Beschreibung der nationalen Anwendungsfälle (1-3) wird darauf
    verzichtet, die am Prozess beteiligten Akteure mit dem
    Zusatz "national" zu kennzeichnen. In den nationalen
    Anwendungsfällen (1-3) wird auf Seiten der Data Consumer zwischen
    Antragsverfahren und Fachverfahren unterschieden.
    -   Antragsverfahren sind i.d.R. Onlinedienste, über die
        Antragstellende die Möglichkeit haben, elektronische
        Verwaltungsleistungen abzuwickeln. Dabei findet eine direkte
        Interaktion statt, sodass der Nachweis ohne zeitlichen Verzug
        zur Verfügung gestellt wird. 
    -   Fachverfahren sind technische Verfahren zur Bearbeitung von
        Verwaltungsvorgängen der Leistungs- und der Eingriffsverwaltung.
        Dabei findet eine Hintergrundverarbeitung statt, sodass der
        Nachweis mit zeitlichem Verzug zur Verfügung gestellt wird. 
    -   Weiterführende Informationen können dem <a href="https://bmi.usercontent.opencode.de/AD-NOOTS-01_%2BAnschlusskonzept%2BData%2BConsumer%2B_DC_/#nachweisanfordernde-stelle" target= "_blank">Anschlusskonzept</a> Data
        Consumer entnommen
        werden.
-   Die Anwendungsfälle unterscheiden zwischen Abrufen, die durch
    natürliche Personen (Bürgerinnen und Bürger), Unternehmen
    (Unternehmen nach § 3 Abs. 1 UBRegG) oder Behörden ausgelöst werden.
    -   Wird ein Nachweisabruf durch eine natürliche Person oder ein
        Unternehmen initiiert, werden diese in der Prozessbeschreibung
        kurz Antragstellende oder Nutzende genannt.
    -   Im Fall eines Unternehmens würde eine vertretungsberechtigte
        Person interagieren, verkürzt wird hier immer von Unternehmen
        gesprochen.
-   Die in den Abbildungen gezeigten \"nachweisbereitstellenden
    Dienste\" sind nicht mit Bündelungs- und Orchestratordiensten
    (Nachweisabrufdienst / NAD) zu verwechseln, die außerhalb des NOOTS
    bereitgestellt werden können, sondern stellen den technischen Dienst
    der Register dar, über welche man den Nachweis abrufen kann. 

### Übersicht der modellierten Anwendungsfälle

 | Anwendungsfall                                                                      | Ziel                       | Initiator                          | Data Consumer                                                                       | Data Provider                                                                       |
|-------------------------------------------------------------------------------------|----------------------------|------------------------------------|-------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|
| 1a: Interaktiver Nachweisabruf zu einer natürlichen Person über das NOOTS           | Once-Only                  | Natürliche Person                  | Deutscher Onlinedienst                                                              | Deutsche nachweisliefernde Stelle                                                   |
| 1b: Interaktiver Nachweisabruf zu einem Unternehmen über das NOOTS                  | Once-Only                  | Unternehmen                        | Deutscher Onlinedienst                                                              | Deutsche nachweisliefernde Stelle                                                   |
| 2: Nicht-Interaktiver Nachweisabruf im NOOTS                                        | Verwaltungsdigitalisierung | Deutsche Behörde                   | Deutsches Fachverfahren                                                             | Deutsche nachweisliefernde Stelle                                                   |
| 3: Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das EU-OOTS          | SDG-VO                     | Natürliche Person oder Unternehmen | Intermediäre Plattform als Mittlerin für Evidence Requester eines EU-Mitgliedstaats | Deutsche nachweisliefernde Stelle                                                   |
| 4: Abruf von europäischen Nachweisen durch nationale Data Consumer über das EU-OOTS | SDG-VO                     | Natürliche Person oder Unternehmen | Deutscher Onlinedienst                                                              | Intermediäre Plattform als Mittlerin für Evidence Provider  eines EU-Mitgliedstaats |


### Anwendungsfall 1a: Interaktiver Nachweisabruf zu einer natürlichen Person über das NOOTS

Der Anwendungsfall "Interaktiver Nachweisabruf zu einer natürlichen
Person über das NOOTS" beschreibt den von einer natürlichen Person
(Bürgerinnen und Bürger) initiierten Abruf nationaler Nachweise aus
nationalen Registern. Der Abruf erfolgt durch einen nationalen
Onlinedienst, insbesondere im Rahmen der Bereitstellung
einer OZG-Leistung.

![C:\\020e06adf4fba5ae6f3964deecf5595e](./assets/images3/media/image7.png)
<br>**Abb. 7: Anwendungsfall 1a: Interaktiver Nachweisabruf zu einer natürlichen Person**</br>


| Prozessschritt | Beschreibung                                                                                                                                                                                                     | Erläuterung                                                                                                                                       | Anmerkung                                                                                                                                                                            |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [UC1a-01]      | <ul><li><p style="color:#184B76;">Für den Bezug einer OZG-Verwaltungsleistung meldet sich eine natürliche Person beim beim Data Consumer (hier Onlinedienst) an.</p> <li><p style="color:#184B76;">Der Nachweisabruf wird mit der Entscheidung durch die natürliche Person ausgelöst.</p></ul>                                                                                   | <ul><li><p style="color:#184B76;">Der Onlinedienst muss die Authentizität der natürlichen Person durch geeignete Maßnahmen sicherstellen.</p>                                           | <ul><li><p style="color:#184B76;">Über das Nutzerkonto muss eine Authentifizierung mit einem Identifikationsmittel erfolgen, das das für den Abruf des jeweiligen Nachweistyps erforderliche Vertrauensniveau erfüllt.</p> <li><p style="color:#184B76;">Prozesse innerhalb der Data Consumer (hier Onlinedienst) werden nicht im Detail betrachtet.</p></ul> |
| [UC1a-02]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) stellt fest, ob es sich um einen nationalen oder einen europäischen Nachweisabruf handelt.</p></ul>                                                                                 | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess handelt es sich um einen nationalen Nachweisabruf.</p>                                                                        |                                                                                                                                                                                      |
| [UC1a-03]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) ermittelt das für einen gesuchten Nachweistyp zuständige nationale Register und dessen technische Verbindungsparameter (inkl. Zertifikat) über einen Zuständigkeitsdienst.</p> <li><p style="color:#184B76;">Der Zuständigkeitsdienst gibt Auskunft darüber, ob die IDNr im Register eingespeichert ist und für den Nachweisabruf verwendet werden kann.</p></ul> | <ul><li><p style="color:#184B76;">Der Zuständigkeitsdienst wird durch die Komponente Registerdatennavigation bereitgestellt.</p> <ol><li><p style="color:#184B76;">Über die Verbindungsparameter kann der technische Dienst eines Registers aufgerufen werden, über den der Nachweis bereitgestellt wird.</p></ol></ul>                                                        | <ul><li><p style="color:#184B76;">Annahme: In nationalen Anwendungsfällen ist dem Data Consumer (hier Onlinedienst) bekannt, welche Nachweistypen für eine Verwaltungsleistung abgerufen werden müssen.</p></ul> 
| [UC1a-04]      | <p><p style="color:#184B76;">Prozessvariante IDNr: Wenn die IDNr im Register eingespeichert ist und die Zustimmung der natürlichen Person oder eine Rechtsgrundlage zur Verwendung der IDNr. vorliegt:</p></p> <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) ruft die Identifikationsnummer (IDNr) der natürlichen Person über einen Identifikationsdienst ab.</p> <li><p style="color:#184B76;">Anschließend ruft der Data Consumer (hier Onlinedienst) ein Berechtigungstoken über einen Vermittlungsdienst ab.</p></ul>                                       | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess wird die (IDNr) für die verwaltungsbereichsübergreifende Kommunikation verwendet.</p> <li><p style="color:#184B76;">Der Identifikationsdienst wird durch die Komponente IDM für Personen bereitgestellt.</p> <ol><li><p style="color:#184B76;">Durch Übergabe von Basisdaten (z.B. aus der eID) ermittelt die Komponente IDM für Personen die IDNr der natürlichen Person. <li><p style="color:#184B76;">Mithilfe der IDNr wird die Identifizierung einer natürlichen Person im Register vereinfacht.</p></ol> <li><p style="color:#184B76;">Der Vermittlungsdienst wird durch die Komponente Vermittlungsstelle bereitgestellt.</p> <ol><li><p style="color:#184B76;">Der Vermittlungsdienst führt eine abstrakte Berechtigungsprüfung durch, stellt ein Berechtigungstoken aus und protokolliert den Nachweisabruf.</p></ol></ul>                                        | <ul><li><p style="color:#184B76;">Einschränkung: Der Vermittlungsdienst muss nur dann aufgerufen werden, wenn es sich um eine verwaltungsbereichsübergreifende Kommunikation unter Verwendung der (IDNr) handelt.</p></ul>       |
| [UC1a-05]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) erzeugt eine XNachweis-Anfrage und übermittelt diese an den Data Provider, vom dem der Nachweis abgerufen werden soll.</p></ul>                                                     | <ul><li><p style="color:#184B76;">Der Data Provider steht hier für einen technischen Endpunkt, über den das Register einen Nachweis bereitstellt.</p></ul>                                  | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p></ul>                                                                                |
| [UC1a-06]      | <ul><li><p style="color:#184B76;">Der Data Provider (hier nachweisliefernde Stelle) verarbeitet die XNachweis-Anfrage und stellt einen Nachweis aus.</p> <li><p style="color:#184B76;">Anschließend übermittelt der Data Provider den Nachweis in einer XNachweis-Antwortnachricht an den Data Consumer (hier Onlinedienst).</p></ul> |                                                                                                                                                | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p> <li><p style="color:#184B76;">Prozesse innerhalb der Data Provider (hier nachweisliefernde Stelle) werden nicht im Detail betrachtet.</p></ul>                                                                                |
| [UC1a-07]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) verarbeitet die XNachweis-Antwort des Data Provider (hier nachweisliefernde Stelle)</p></ul>                                                                                        |                                                                                                                                                   | <ul><li><p style="color:#184B76;">Prozesse innerhalb der Data Consumer (hier Onlinedienst) werden nicht im Detail betrachtet.</p></ul>                                                                                          |
| [UC1a-08]      | <p><p style="color:#184B76;">Prozessvariante Preview Die natürliche Person kann entscheiden, ob ein Preview des Nachweises angezeigt werden soll:</p> <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) stellt eine Preview des Nachweises bereit.</p></ul>                                                                                            | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess entscheidet sich die natürliche Person für die Preview und entscheidet über die Verwendung des Nachweis.</p></ul>                  | <ul><li><p style="color:#184B76;"></p> <p style="color:#184B76;">Bei einem nationalen Nachweisabruf ist der Data Consumer (hier Onlinedienst) für die Aufbereitung und Anzeige der Preview verantwortlich.</p></ul> |                                           |
| [UC1a-09]      | <ul><li><p style="color:#184B76;">Der Nachweisabruf ist abgeschlossen.</p></ul>                                                                                                                                                                             |


### Anwendungsfall 1b: Interaktiver Nachweisabruf zu einem Unternehmen über das NOOTS

Der Anwendungsfall "Interaktiver Nachweisabruf zu einem Unternehmen über
das NOOTS" beschreibt den von einem Unternehmen initiierten Abruf
nationaler Nachweise aus nationalen Registern. Der Abruf erfolgt durch
einen nationalen Onlinedienst, insbesondere im Rahmen der Bereitstellung
einer OZG-Leistung.

Hinweis: Die bundeseinheitliche Wirtschaftsnummer wird im Folgenden
verkürzt „beWiNr" genannt.

![C:\\28e481a530adc37cf33fb899234297e6](./assets/images3/media/image8.png)
<br>**Abb. 8: Anwendungsfall 1b: Interaktiver Nachweisabruf zu einem Unternehmen**</br>

| Prozessschritt | Beschreibung                                                                                                                                                                                                     | Erläuterung                                                                                                                               | Anmerkung                                                                                                                                                                              |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [UC1b-01]      | <ul><li><p style="color:#184B76;">Für den Bezug einer OZG-Verwaltungsleistung melden sich ein Unternehmen beim beim Data Consumer (hier Onlinedienst) an.</p> <li><p style="color:#184B76;">Der Nachweisabruf wird mit der Entscheidung durch das Unternehmen Person ausgelöst.</p></ul>                                                                                          | <ul><li><p style="color:#184B76;">Der Onlinedienst muss die Authentizität des Unternehmens (der Vertretungsberechtigung) durch geeignete Maßnahmen sicherstellen.</p></ul>           | <ul><li><p style="color:#184B76;">Über das Unternehmenskonto muss eine Authentifizierung mit einem Identifikationsmittel erfolgen, das für den Abruf des jeweiligen Nachweistyps erforderliche Vertrauensniveau erfüllt.</p> <li><p style="color:#184B76;">Prozesse innerhalb der Data Consumer (hier Onlinedienst) werden nicht im Detail betrachtet.</p></ul> |
| [UC1b-02]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) stellt fest, ob es sich um einen nationalen oder einen europäischen Nachweisabruf handelt.</p></ul>                                                                                 | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess handelt es sich um einen nationalen Nachweisabruf.</p></ul>                                                                |                                                                                                                                                                                        |
| [UC1b-03]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) ermittelt das für einen gesuchten Nachweistyp zuständige nationale Register und dessen technische Verbindungsparameter (inkl. Zertifikat) über einen Zuständigkeitsdienst.</p> <li><p style="color:#184B76;">Der Zuständigkeitsdienst gibt Auskunft darüber, ob die beWiNr im Register eingespeichert ist und für den Nachweisabruf verwendet werden kann.</p></ul> | <ul><li><p style="color:#184B76;">Der Zuständigkeitsdienst wird durch die Komponente Registerdatennavigation bereitgestellt.</p> <ol><li><p style="color:#184B76;">Über die Verbindungsparameter kann der technische Dienst eines Registers aufgerufen werden, über den der Nachweis bereitgestellt wird.</p></ol></ul>                                              | <ul><li><p style="color:#184B76;">Annahme: In nationalen Anwendungsfällen ist dem Data Consumer (hier Onlinedienst) bekannt, welche Nachweistypen für eine Verwaltungsleistung abgerufen werden müssen.</p></ul>                  |
| [UC1b-04]      | Prozessvariante beWiNr, wenn die beWiNr im Register eingespeichert ist:</p> <p><ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) ruft die bundeseinheitliche Wirtschaftsnummer (beWiNr) des Unternehmens über einen Identifikationsdienst ab.</p></ul>                                                                                                                                          |<ul><li><p style="color:#184B76;">Im vorliegenden Prozess wird die beWiNr für die Kommunikation verwendet.</p> <li><p style="color:#184B76;">Der Identifikationsdienst wird durch die Komponente IDM für Unternehmen bereitgestellt.</p> <ol><li><p style="color:#184B76;">Durch Übergabe von Basisdaten ermitteltdie Komponente IDM für Unternehmen die beWiNr des Unternehmens.</p> <li><p style="color:#184B76;">Mithilfe der beWiNr wird die Identifizierung eines Unternehmens im Register vereinfacht.</p></ol></ul>                                                               |                                                                                   
| [UC1b-05]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) erzeugt eine XNachweis-Anfrage und übermittelt diese an den Data Provider, vom dem der Nachweis abgerufen werden soll.</p></ul>                                                     | <ul><li><p style="color:#184B76;">Der Data Provider steht hier für einen technischen Endpunkt, über den das Register einen Nachweis bereitstellt.</p></ul>                           | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p></ul>                                                                                  |
| [UC1b-06]      | <ul><li><p style="color:#184B76;">Der Data Provider (hier nachweisliefernde Stelle) verarbeitet die XNachweis-Anfrage und stellt einen Nachweis aus.</p> <li><p style="color:#184B76;">Anschließend übermittelt der Data Provider den Nachweis in einer XNachweis-Antwortnachricht an den Data Consumer (hier Onlinedienst).</p></ul>                                                                                             |                                                                                                                                           | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p> <p><li><p style="color:#184B76;">Prozesse innerhalb der Data Provider (hier nachweisliefernde Stelle) werden nicht im Detail betrachtet.</p></ul>                                                                                  |
| [UC1b-07]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) verarbeitet die XNachweis-Antwort des Data Provider (hier nachweisliefernde Stelle)</p><ul>                                                                                        |                                                                                                                                           | <ul><li><p style="color:#184B76;">Prozesse innerhalb der Data Consumer (hier Onlinedienst) werden nicht im Detail betrachtet.</p><ul>                                                                                            |
| [UC1b-08]      | Prozessvariante Preview Das Unternehmen kann entscheiden, ob ein Preview des Nachweises angezeigt werden soll:</p> <p><ul><li><p style="color:#184B76;">Der Data Consumer (hier Onlinedienst) stellt eine Preview des Nachweises bereit.</p></ul>                                                                                                 | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess entscheidet sich das Unternehmen für die Preview und entscheidet über die Verwendung des Nachweis.</p></ul>               | <ul><li><p style="color:#184B76;">Bei einem nationalen Nachweisabruf ist der Data Consumer (hier Onlinedienst) für die Aufbereitung und Anzeige der Preview verantwortlich.</p></ul>                                              |
| [UC1b-09]      | <ul><li><p style="color:#184B76;">Der Nachweisabruf ist abgeschlossen.</p></ul>                                                                                                                                                                             |



### Anwendungsfall 2: Nicht-Interaktiver Nachweisabruf im NOOTS

Der Anwendungsfall "Nicht-Interaktiver Nachweisabruf im
NOOTS" beschreibt den von einer Behörde (Fachverfahren) initiierten
Abruf nationaler Nachweise aus nationalen Registern. Der Abruf erfolgt
durch ein nationales Fachverfahren im Rahmen der Bereitstellung
einer OZG-Leistung oder im Kontext der Eingriffsverwaltung. Diese
Nachweisabrufe benötigen eine andere Rechtsgrundlage als Art 2 OZGÄndG
(§5 EGovG), da keine Interaktion mit natürlichen Personen oder
Unternehmen stattfinden kann.

Hinweis: Die Verwendung der bundeseinheitliche
Wirtschaftsnummer „beWiNr" wird noch untersucht und deshalb erst in
einem kommenden Release modelliert.

![C:\\d1bb2f7bd7a812d9641b3a2423c4e004](./assets/images3/media/image9.png)
<br>**Abb. 9: Anwendungsfall 2: Nicht-Interaktiver Nachweisabruf im NOOTS**</br>


| Prozessschritt | Beschreibung                                                                                                                                                                                                      | Erläuterung                                                                                                                                       | Anmerkung                                                                                                                                                                      |
|----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [UC2-01]       | <ul><li><p style="color:#184B76;">Für die Bereitstellung einer OZG-Verwaltungsleistung oder im Kontext der Eingriffsverwaltung muss ein Nachweis durch den Data Consumer (hier Fachverfahren) abgerufen werden.</p></ul>                                     |                                                                                                                                                   |                                                                                                                                                                                |
| [UC2-02]       | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Fachverfahren) stellt fest, ob es sich um einen nationalen oder einen europäischen Nachweisabruf handelt.</p></ul>                                                                                 | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess handelt es sich um einen nationalen Nachweisabruf.</p></ul>                                                                        |                                                                                                                                                                                |
| [UC2-03]       | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Fachverfahren) ermittelt das für einen gesuchten Nachweistyp zuständige nationale Register und dessen technische Verbindungsparameter (inkl. Zertifikat) über einen Zuständigkeitsdienst.</p> <li><p style="color:#184B76;">Der Zuständigkeitsdienst gibt Auskunft darüber, ob die IDNr im Register eingespeichert ist und für den Nachweisabruf verwendet werden kann.</p></ul> | <ul><li><p style="color:#184B76;">Der Zuständigkeitsdienst wird durch die Komponente Registerdatennavigation bereitgestellt.</p> <ol><li><p style="color:#184B76;">Über die Verbindungsparameter kann der technische Dienst eines Registers aufgerufen werden, über den der Nachweis bereitgestellt wird.</p></ol></ul>                                                        | <ul><li><p style="color:#184B76;">Annahme: In nationalen Anwendungsfällen ist dem Data Consumer (hier Fachverfahren) bekannt, welche Nachweistypen für eine Verwaltungsleistung abgerufen werden müssen.</p></ul>         |
| [UC2-04]       | Prozessvariante IDNr: Wenn die IDNr im Register eingespeichert ist und eine Rechtsgrundlage zum Abruf dieser vorliegt:</p> <p><ul><li><p style="color:#184B76;">Der Data Consumer (hier Fachverfahren) ruft die Identifikationsnummer (IDNr) einer natürlichen Person über einen Identifikationsdienst ab.</p> <li><p style="color:#184B76;">Anschließend ruft der Data Consumer (hier Fachverfahren) ein Berechtigungstoken über einen Vermittlungsdienst ab.</p></ul>                                                                                            | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess wird die (IDNr) für die verwaltungsbereichsübergreifende Kommunikation verwendet.</p> <li><p style="color:#184B76;">Der Identifikationsdienst wird durch die Komponente IDM für Personen bereitgestellt.</p> <ol><li><p style="color:#184B76;">Durch Übergabe von Basisdaten ermittelt die Komponente IDM für Personen die IDNr der natürlichen Person.</p> <li><p style="color:#184B76;">Mithilfe der IDNr wird die Identifizierung einer natürlichen Person im Register vereinfacht.</p></ol> <li><p style="color:#184B76;">Der Vermittlungsdienst wird durch die Komponente Vermittlungsstelle bereitgestellt.</p> <ol><li><p style="color:#184B76;">Der Vermittlungsdienst führt eine abstrakte Berechtigungsprüfung durch, stellt ein Berechtigungstoken aus und protokolliert den Nachweisabruf.</p></ol></ul>                                        | <ul><li><p style="color:#184B76;">Einschränkung: Der Vermittlungsdienst muss nur dann aufgerufen werden, wenn es sich um eine verwaltungsbereichsübergreifende Kommunikation unter Verwendung der (IDNr) handelt.</p></ul>   |
| [UC1b-05]      | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Fachverfahren) erzeugt eine XNachweis-Anfrage und übermittelt diese an den Data Provider, vom dem der Nachweis abgerufen werden soll.</p></ul>                                                     | <ul><li><p style="color:#184B76;">Der Data Provider steht hier für einen technischen Endpunkt, über den das Register einen Nachweis bereitstellt.</p></ul>                                   | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p></ul>                                                                          |
| [UC2-06]       | <ul><li><p style="color:#184B76;">Der Data Provider (hier nachweisliefernde Stelle) verarbeitet die XNachweis-Anfrage und stellt einen Nachweis aus.</p> <li><p style="color:#184B76;">Anschließend übermittelt der Data Provider den Nachweis in einer XNachweis-Antwortnachricht an den Data Consumer (hier Fachverfahren).</p></ul>                                                                                                 |                                                                                                                                                   | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p> <li><p style="color:#184B76;">Prozesse innerhalb der Data Provider (hier nachweisliefernde Stelle) werden nicht im Detail betrachtet.</p></ul> |                                                                          |
| [UC2-07]       | <ul><li><p style="color:#184B76;">Der Data Consumer (hier Fachverfahren) verarbeitet die XNachweis-Antwort des Data Provider (hier nachweisliefernde Stelle)</p></ul>                                                                                        |                                                                                                                                                   | <ul><li><p style="color:#184B76;">Prozesse innerhalb der Data Consumer (hier Fachverfahren) werden nicht im Detail betrachtet.</p></ul>                                                                                   |
| [UC2-08]       | <ul><li><p style="color:#184B76;">Der Nachweisabruf ist abgeschlossen.</p></ul>                                                                                                                                                                              |



### Anwendungsfall 3: Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das EU-OOTS

Der Anwendungsfall „Abruf von nationalen Nachweisen aus
EU-Mitgliedstaaten über das EU-OOTS beschreibt den von natürlichen
Personen (Bürgerinnen und Bürger) oder Unternehmen initiierten Abruf
nationaler Nachweise aus nationalen Registern über Evidence Requester
der EU-Mitgliedstaaten unter Verwendung des EU-OOTS.

![C:\\ef1ffecacdfb2409180be250c379757f](./assets/images3/media/image10.png)
<br>**Abb. 10: Anwendungsfall 3: Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten über das EU-OOTS**</br>


| Prozessschritt           | Beschreibung                                                                                                                                                                              | Erläuterung                                                                                                                                                    | Anmerkung                                                                                                                                                                                                                                                                                                                                                 |
|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [UC3-01]                 | <ul><li><p style="color:#184B76;">Für den Bezug einer Verwaltungsleistung eines EU-Mitgliedstaats (EU-MS) melden sich Antragsstellende bei einem europäischen Evidence Requester an.</p> <li><p style="color:#184B76;">Mit dem ausdrücklichen Ersuchen (“explicit request”) des Antragsstellenden wird der Nachweisabruf ausgelöst.</p></ul>                                        | <ul><li><p style="color:#184B76;">Der Evidence Requester muss die Authentizität des Antragsstellenden durch geeignete Maßnahmen sicherstellen.</p></ul>                                                   | <ul><li><p style="color:#184B76;">Prozesse innerhalb der Evidence Requester werden nicht detailliert betrachtet.</p> <li><p style="color:#184B76;">Zur Authentifizierung wird ein eIDAS-konformes Authentifizierungsmittel verwendet.</p></ul> | 
| [UC3-02]                 | <ul><li><p style="color:#184B76;">Der Evidence Requester ermittelt, welcher Nachweistyp im anzufragenden EU-MS zu einem zu belegenden Sachverhalt (Requirement) zugeordnet ist.</p> <li><p style="color:#184B76;">Dazu ruft der Evidence Requester den EB des MS auf.</p></ul>                                            | <ul><li><p style="color:#184B76;">Diese Auskunft erhält der Evidence Requester im Evidence Broker des MS.</p></ul>                                                                                        | <ul><li><p style="color:#184B76;">Dieser Prozess muss für jeden Sachverhalt wiederholt werden.</p> <li><p style="color:#184B76;">Es ist möglich, dass der Evidence Broker mehr als einen Nachweistypen ermittelt und dem Evidence Requester eine Auswahlliste bereitstellt.</p></ul> |
| [UC3-03]                 | <ul><li><p style="color:#184B76;">Der Evidence Requester ermittelt den für einen Nachweistyp zuständigen Evidence Provider und die technischen Verbindungsparameter.</p> <li><p style="color:#184B76;">Dazu ruft der Evidence Requester das DSD des MS auf.</p></ul>                                                        | <ul><li><p style="color:#184B76;">Diese Auskunft erhält der Evidence Requester im Data Service Directory des MS.</p></ul>                                                                                 | <ul><li><p style="color:#184B76;">Dieser Prozess muss für jeden Nachweistyp wiederholt werden, für den sich der Evidence Requester zuvor entschieden hat, Nachweise abzurufen.</p> <li><p style="color:#184B76;">Es ist möglich, dass das Data Service Directory mehr als einen Data Service ermittelt und dem Evidence Requester eine Auswahlliste bereitstellt.</p></ul> |      
| [UC3-04]                 | <ul><li><p style="color:#184B76;">Der Evidence Requester generiert einen EDM-Request und übermittelt diesen über einen Access Point an die nationale Intermediäre Plattform.</p>                                                |                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                           |
| **Prozess National beginnt** |                                                                                                                                                                                           |                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                           |
| [UC3-05]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform empfängt den EDM-Request und beantwortet diesen mit einer RegRep-Error-Message.</p> <li><p style="color:#184B76;">Der Evidence Requester verarbeitet die RegRep-Error-Message und führt eine Weiterleitung des Antragstellenden auf den Preview-Space der Intermediären Plattform durch.</p></ul>                                                                                | <ul><li><p style="color:#184B76;">Innerhalb der RegRep-Error-Message übermittelt die IP eine Preview-URL, um Antragstellende auf den Preview-Space der IP umzuleiten.</p></ul>                            | <ul><li><p style="color:#184B76;">Der Preview-Space ist eine Benutzeroberfläche, über die Antragstellende Entscheidungen (unter anderem Freigabe der Preview) treffen oder fehlende Informationen liefern können.</p></ul>  | 
| [UC3-06]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform führt eine (Re-) Authentifizierung des Antragsstellenden durch.</p></ul>                                                                                                |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Es muss Authentifizierung mit dem Identifikationsmittel für das für den Abruf des jeweiligen Nachweistyps erforderliche Vertrauensniveau erfolgen.</p></ul>                                                                                                                                                                                                        |
| [UC3-07]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform ermittelt das für einen Nachweistyp zuständige nationale Register und die technischen Verbindungsparameter (inkl. Zertifikat) über einen Zuständigkeitsdienst.</p></ul> | <ul><li><p style="color:#184B76;">Der Zuständigkeitsdienst wird durch die Registerdatennavigation bereitgestellt.</p> <li><p style="color:#184B76;">Über die Verbindungsparameter kann der Nachweisabrufdienst des Registers aufgerufen werden.</p></ul>                                                                                 | <ul><li><p style="color:#184B76;">In diesem Schritt kann eine Nutzerinteraktion notwendig sein, sofern die Authentifizierungsinformationen des Antragstellenden für die Ermittlung des zuständigen Registers nicht ausreichen. Die Re-Authentifizierung dient der Sicherstellung, dass der Nutzer aus dem EU-Ausland nun auf der Bedienoberfläche der Intermediären Plattform gelandet ist.</p></ul> |
| [UC3-08]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform generiert eine XNachweis-Anfrage und übermittelt diese an den Data Provider.</p></ul>                                                                                   |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p> <li><p style="color:#184B76;">In diesem Schritt kann eine Nutzerinteraktion notwendig sein, sofern die Authentifizierungsinformationen des Antragsstellenden für die Ermittlung des richtigen Datensatzes im Register nicht ausreichen.</p></ul> |
| [UC3-09]                 | <ul><li><p style="color:#184B76;">Das Register empfängt die XNachweis-Anfrage, verarbeitet diese und stellt einen Nachweis aus.</p> <li><p style="color:#184B76;">Anschließend übermittelt der Data Provider den Nachweis in einer XNachweis-Antwortnachricht an die Intermediäre Plattform.</p></ul>                                                                                            |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Prozesse innerhalb der Register werden hier nicht im Detail betrachtet.</p></ul>   | 
| [UC3-10]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform empfängt die XNachweis-Antwort des Registers.</p> <li><p style="color:#184B76;">Die Intermediäre Plattform führt eine Preview durch.</p></ul>                                                                                                                 | <ul><li><p style="color:#184B76;">Bevor ein Nachweis an das für den Antrag zuständige Fachverfahren übermittelt werden darf, muss die Freigabe des Antragsstellenden (Preview) eingeholt werden.</p> <li><p style="color:#184B76;">Im vorliegenden Prozess liegt die Zustimmung des Antragsstellenden vor und die Nachweise dürfen in den Antragsprozess übernommen werden.</p></ul> | <ul><li><p style="color:#184B76;">Bei einem Nachweisabruf aus der EU ist die IP für die Aufbereitung und Anzeige der Preview verantwortlich.</p> <li><p style="color:#184B76;">Werden mehrere Nachweise abgerufen, ist es aus Sicht des Antragsstellenden wünschenswert, die Preview zu bündeln. Die Nachweisabrufe können parallel erfolgen.</p></ul> |  
| [UC3-11]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform führt eine Weiterleitung des Antragstellenden auf die Nutzeroberfläche des Evidence Requesters durch.</p></ul>                                                          |                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                           |
| **Prozess National endet**        |                                                                                                                                                                                           |                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                           |
| [UC3-12]                 | <ul><li><p style="color:#184B76;">Der Evidence Requester generiert einen erneuten EDM-Request und übermittelt diesen über einen Access Point an die nationale Intermediäre Plattform.</p></ul>                                       |                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                           |
| [UC3-13]                 | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform verarbeitet den EDM-Request und erzeugt eine EDM-Response, die den Nachweis enthält.</p> <li><p style="color:#184B76;">Die Intermediäre Plattform übermittelt die EDM-Response über einen Access Point an den Evidence Requester.</p></ul>                                                                          | 
| [UC3-14]                 | <ul><li><p style="color:#184B76;">Der Nachweisabruf ist abgeschlossen.</p></ul>                                                                                                                                                      |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Falls mehrere Nachweise benötigt werden, wird der Prozess ab dem Schritt UC3-07 für jeden Nachweis wiederholt.</p></ul>                                                                                                                                                                                                                                            |


### Anwendungsfall 4: Abruf von europäischen Nachweisen durch nationale Data Consumer über das Europäische Once-Only-Technical-System (EU-OOTS)

Der Anwendungsfall „Abruf von europäischen Nachweisen durch nationale
Data Consumer über das Europäische Once-Only-Technical-System (EU-OOTS)"
beschreibt den durch natürliche Personen (Bürgerinnen und Bürger) oder
Unternehmen initiierten Abruf von Nachweisen von Evidence Providern
anderer EU-Mitgliedstaaten unter Verwendung des EU-OOTS.

![C:\\f914e1bdcdaf2e720899b62d8ff61ac8](./assets/images3/media/image11.png)
<br>**Abb. 11: Anwendungsfall 4: Abruf von europäischen Nachweisen durch nationale Data Consumer über das EU-OOTS**</br>


| Prozessschritt                | Beschreibung                                                                                                                                                      | Erläuterung                                                                                                                                                    | Anmerkung                                                                                                                                                                                                                                                   |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [UC4-01]                      | <ul><li><p style="color:#184B76;">Für den Bezug einer Verwaltungsleistung melden sich Antragstellende am Data Consumer an.</p> <li><p style="color:#184B76;">Der Nachweisabruf wird durch die Entscheidung des Antragsstellenden ausgelöst.</p></ul>                                                                          | <ul><li><p style="color:#184B76;">Der Data Consumer muss die Authentizität des Antragsstellenden durch geeignete Maßnahmen sicherzustellen.</p></ul>                                                      | <ul><li><p style="color:#184B76;">Über das Nutzerkonto muss eine Authentifizierung mit dem Identifikationsmittel für das das für den Abruf des jeweiligen Nachweistyps erforderliche Vertrauensniveau erfolgen.</p> <li><p style="color:#184B76;">Prozesse innerhalb der Portale / Onlinedienste werden hier nicht im Detail betrachtet.</p></ul>                                                                               |
| [UC4-02]                      | <ul><li><p style="color:#184B76;">Der Data Consumer stellt fest, ob es sich um einen nationalen oder europäischen Nachweisabruf handelt.</p></ul>                                                            | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess handelt es sich um einen europäischen Nachweisabruf.</p></ul>                                                                                   |                                                                                                                                                                                                                                                             |
| [UC4-03]                      | <ul><li><p style="color:#184B76;">Der Data Consumer generiert eine XNachweis-Anfrage und übermittelt dabei mindestens die Verfahren gemäß Anhang 2 der SDG VO an die Intermediäre Plattform.</p></ul>        | <ul><li><p style="color:#184B76;">Im vorliegenden Prozess wird angenommen, dass der Data Consumer eine nachzuweisende Tatsache (Requirement) übergibt.</p></ul>                                           | <ul><li><p style="color:#184B76;">Die Übergabe von nachzuweisenden Tatsachen (Requirement), Ländercode (CountryCode) pro Requirement und explizierter Zustimmung “explicit request” pro Sachverhalt ist (gem. EDM) optional.</p></ul>                                                                  |
| [UC4-04]                      | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform empfängt die XNachweis-Anfrage des Data Consumer.</p></ul>                                                                                      |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-05]                      | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform ermittelt, welcher Nachweistyp im anzufragenden EU-Mitgliedstaat (MS) zu einem zu belegenden Sachverhalt (Requirement) zugeordnet ist.</p> <li><p style="color:#184B76;">Dazu ruft die Intermediäre Plattform den EB des Mitgliedstaats auf.</p></ul> | <ul><li><p style="color:#184B76;">Diese Auskunft erhält die Intermediäre Plattform vom Evidence Broker des Mitgliedstaats.</p></ul>                                                                       | <ul><li><p style="color:#184B76;">Dieser Prozess muss für jeden Sachverhalt wiederholt werden.</p> <li><p style="color:#184B76;">Es ist möglich, dass der Evidence Broker mehr als einen Nachweistypen ermittelt und der Intermediäre Plattform eine Auswahlliste bereitstellt.</p> <li><p style="color:#184B76;">In dieser Beschreibung wird darauf verzichtet, alle möglichen Prozessvarianten und Nutzerinteraktionen, die innerhalb der Intermediären Plattform auftreten können, zu beschreiben. Diese werden im Grobkonzept zu der Intermediären Plattform detailliert.</p></ul> | 
| [UC4-06]                      | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform ermittelt den für einen Nachweistyp zuständigen Evidence Provider und die technischen Verbindungsparameter.</p> <li><p style="color:#184B76;">Dazu ruft die Intermediäre Plattform das DSD des Mitgliedstaats auf.</p></ul>                           | <ul><li><p style="color:#184B76;">Diese Auskunft erhält die Intermediäre Plattform im Data Service Directory des Mitgliedstaats.</p> <li><p style="color:#184B76;">Über die Verbindungsparameter kann der Data Service des Evidence Provider aufgerufen werden.</p></ul>                                                                 | <ul><li><p style="color:#184B76;">Dieser Prozess muss für jeden Nachweistyp wiederholt werden, für den sich der Evidence Requester zuvor entschieden hat, Nachweise abzurufen.</p> <li><p style="color:#184B76;">Es ist möglich, dass das Data Service Directory mehr als einen Data Service ermittelt und dem Evidence Requester eine Auswahlliste bereitstellt.</p></ul>                                                                                                                |
| [UC4-07]                      | <ul><li><p style="color:#184B76;">Mit dem ausdrücklichen Ersuchen (explicit request) des Antragsstellenden wird der Nachweisabruf ausgelöst.</p> <li><p style="color:#184B76;">Die Intermediäre Plattform generiert einen EDM-Request und übermittelt diesen über einen Access Point an den Evidence Provider.</p></ul>                                                       |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Aus Sicht des Evidence Provider tritt die Intermediäre Plattform in der Rolle eines Evidence Requester auf.</p></ul> |
| **Prozess im EU-Ausland beginnt** |                                                                                                                                                                   |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-08]                      | <ul><li><p style="color:#184B76;">Der Evidence Provider empfängt den EDM-Request und beantwortet diesen mit einer RegRep-Error-Message.</p></ul>                                                             | <ul><li><p style="color:#184B76;">Innerhalb der RegRep-Error-Message übermittelt die IP eine Preview-URL, um Antragsstellende auf den Preview-Space der IP umzuleiten.</p></ul>                           | <ul><li><p style="color:#184B76;">Der Preview-Space ist eine Benutzeroberfläche, über die Antragstellende Auswahlentscheidungen (u. A. Preview) treffen oder fehlende Informationen liefern können.</p></ul>                                                                                           |
| [UC4-09]                      | <ul><li><p style="color:#184B76;">Der Evidence Requester verarbeitet die RegRep-Error-Message und führt eine Weiterleitung des Antragstellenden auf den Preview-Space des Evidence Provider durch.</p></ul>  |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-10]                      | <ul><li><p style="color:#184B76;">Der Evidence Provider ermittelt die gesuchten Nachweise.</p> <li><p style="color:#184B76;">Der Evidence Provider führt eine Preview durch.</p></ul>                                                                                                         | <ul><li><p style="color:#184B76;">Bevor ein Nachweis an das für den Antrag zuständige Fachverfahren übermittelt werden darf, muss die Freigabe des Antragsstellenden (Preview) eingeholt werden.</p> <li><p style="color:#184B76;">Im vorliegenden Prozess liegt die Zustimmung des Antragsstellenden vor und die Nachweise dürfen in den Antragsprozess übernommen werden.</p></ul> | <ul><li><p style="color:#184B76;">Prozesse innerhalb des Evidence Provider (z.B. (Re-)Authentifizierung oder Nutzerinteraktion) werden hier nicht genauer beschrieben.</p></ul>                                                                                                                        |
| [UC4-11]                      | <ul><li><p style="color:#184B76;">Der Evidence Provider führt eine Weiterleitung des Antragsstellenden auf die Nutzeroberfläche der Intermediären Plattform durch.</p></ul>                                  |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| **Prozess im EU-Ausland endet**   |                                                                                                                                                                   |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-12]                      | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform generiert einen erneuten EDM-Request und übermittelt diesen über einen Access Point an den Evidence Provider.</p></ul>                          |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| **Prozess im EU-Ausland beginnt** |                                                                                                                                                                   |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-13]                      | <ul><li><p style="color:#184B76;">Der Evidence Provider verarbeitet den EDM-Request und erzeugt eine EDM-Response, die den Nachweis enthält.</p> <li><p style="color:#184B76;">Der Evidence Provider übermittelt die EDM-Response über einen Access Point an die Intermediäre Plattform.</p></ul>                                                       |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| **Prozess im EU-Ausland endet**   |                                                                                                                                                                   |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-14]                      | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform verarbeitet die EDM-Response und speichert den Nachweis für den späteren Abruf durch den Data Consumer.</p> <li><p style="color:#184B76;">Die Intermediäre Plattform führt eine Weiterleitung des Antragsstellenden auf die Nutzeroberfläche des Data Consumers durch.</p></ul>                                 |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Der Antragsstellende wird über den Nachweisabruf informiert.</p></ul>  |    
| [UC4-15]                      | <ul><li><p style="color:#184B76;">Der Data Consumer erzeugt eine XNachweis-Anfrage und übermittelt diese an die Intermediäre Plattform.</p></ul>                                                             |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Auf eine detaillierte Beschreibung von Verschlüsselung und Signatur wird an dieser Stelle verzichtet.</p></ul>                                                                                                                                                       |
| [UC4-16]                      | <ul><li><p style="color:#184B76;">Die Intermediäre Plattform generiert eine XNachweis-Antwort, die die abgerufenen Nachweise enthält, und übermittelt diese an den Data Consumer.</p></ul>                   |                                                                                                                                                                |                                                                                                                                                                                                                                                             |
| [UC4-17]                      | <ul><li><p style="color:#184B76;">Der Nachweisabruf ist abgeschlossen.</p></ul>                                                                                                                              |                                                                                                                                                                | <ul><li><p style="color:#184B76;">Falls mehrere Nachweise benötigt werden, wird der Prozess ab Schritt UC4-05 für jeden Nachweis wiederholt.</p></ul>                                                                                                                                                  |


## Ausblick und weiterführende Aspekte

Auflistung offener Klärungspunkte aus der High-Level-Architecture des
NOOTS.

| Bezeichnung                                      | Beschreibung                                                                                                                                                     | Jira-Ticket? |
|--------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|
| Verwendung von Vermittlungsstellen               | Sollte der Einsatz einer Vermittlungsstelle / die abstrakte Berechtigungsprüfung auch dann erfolgen, wenn der Nachweisabruf ohne Verwendung der IDNr erfolgt?    | Ja           |
| Pflegeprozesse im EU-OOTS                        | Wer ist für die Datenpflege des “EU Central Data Service Directory“ und des „EU Central Evidence Broker” verantwortlich?                                         | Ja           |
| Nicht-interaktive Nachweisabrufe im NOOTS        | Welche juristischen, prozessualen und IT-sicherheitstechnischen Voraussetzungen müssen für den nicht-interaktiven Nachweisabruf (Anwendungsfall 2) erfüllt sein? | Nein         |
| Begriffsschärfung                                | Klärung der Verwendung einzelner Begriffe und Anpassung der Bezeichnungen entsprechend der aktualisierten Definitionen: <ul><li><p style="color:#184B76;">Data Consumer</p> <li><p style="color:#184B76;">Data Provider</p> <li><p style="color:#184B76;">Register / nachweisliefernde Stelle</p> <li><p style="color:#184B76;">Nachweis / Nachweisdaten / Registerdaten</p></ul>                                          | Nein         |
| Detaillierung Reifegradmodell & Fachdatenkonzept | Ergänzung relevanter Informationen und Rahmenbedingungen für das NOOTS aus Dokumenten, die aktuell noch in anderen Programmbereichen erstellt werden:<ul><li><p style="color:#184B76;">Wie sieht die detaillierte Ausgestaltung der einzelnen Reifegrade aus (Reifegradmodell)?</p> <li><p style="color:#184B76;">Wie wird der automatisierte Nachweisaustausch von Fachdaten zwischen Data Consumer und Data Provider ausgestaltet (Fachdatenkonzept)?</p></ul>           | Nein         |
| Rückblick SDG-Umsetzung                          | Soll die Umsetzungsstufe 2023 nachträglich auf den Ist-Stand zum 12.12.2023 angepasst werden?                                                                    | Nein         |

