>**Redaktioneller Hinweis**
>
>Dokument aus der zweiten Iteration - nicht Teil der aktuellen Iteration.
Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## 1. Abstract

<p>Die Vermittlungsstelle ist gem. &sect; 7 Abs. 2 Identifikationsnummerngesetz (IDNrG) (<a href="https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf">RGR-01</a>) eine dritte &ouml;ffentliche Stelle, die bei verwaltungsbereichs&uuml;bergreifenden Nachweisabrufen unter Nutzung der Identifikationsnummer (IDNr) eingeschaltet werden muss. Eine Einschaltung auch beim bereichsinternen Nachweisabruf gem. &sect; 12 Abs. 2 IDNrG ist durch Beschluss m&ouml;glich. In diesen F&auml;llen f&uuml;hrt sie eine abstrakte (das bedeutet ohne Wissen &uuml;ber den Nachrichteninhalt und insbesondere die antragstellende Person) Berechtigungspr&uuml;fung durch, um sicherzustellen, dass nachweisabrufende Stelle und nachweisliefernde Stelle zu einem angegebenen Zweck miteinander kommunizieren d&uuml;rfen. Ziel der Einrichtung der Vermittlungsstelle ist, das Risiko zu verringern, dass personenbezogene Daten einer B&uuml;rgerin oder eines B&uuml;rgers zu einem Pers&ouml;nlichkeitsprofil zusammengef&uuml;hrt werden. Sie stellt somit einen wichtigen Baustein f&uuml;r den Datenschutz dar. Die Vermittlungsstelle muss ihre Aufgabe ohne Kenntnis der Nachrichteninhalte erbringen. So ist sichergestellt, dass die Vermittlungsstelle selbst nicht zu einer Gefahrenquelle f&uuml;r den Datenschutz wird.</p>
<p>Aufgrund der fehlenden Einsicht in den Nachrichteninhalt ist die Mitwirkung der Kommunikationspartner notwendig, damit die Vermittlungsstelle ihre Aufgabe zuverl&auml;ssig erf&uuml;llen kann. Es muss der Data Consumer wahrheitsgem&auml;&szlig;e Angaben f&uuml;r die abstrakte Berechtigungspr&uuml;fung (d.h. &uuml;ber Kommunikationspartner, Kommunikationszweck und Angabe, ob die IDNr verwendet wird) machen und entsprechend dem Ergebnis der abstrakten Berechtigungspr&uuml;fung handeln, also bei negativer abstrakter Berechtigungspr&uuml;fung die Nachricht nicht versenden. Der Data Provider muss die gemachten Angaben mit dem tats&auml;chlichen Nachrichteninhalt abgleichen (also das Ergebnis der abstrakten Berechtigungspr&uuml;fung best&auml;tigen) und die Nachricht abweisen, falls der Data Consumer falsche Angaben gemacht hat oder die Nachricht ohne Pr&uuml;fung der Vermittlungsstelle versendet hat. Der folgend beschriebene Tokenansatz zielt darauf ab, die Mitwirkung der beiden Kommunikationsteilnehmern entsprechend dem Zero-Trust-Prinzip ("niemals vertrauen, immer verifizieren") &uuml;berpr&uuml;fbar zu machen und sicherzustellen.</p>
<p>Die Anforderungen an die Vermittlungsstelle werden durch eine Token-Architektur abgebildet, die die Mitwirkungvon Data Consumer und Data Provider erheblich vereinfacht. Der Data Consumer fordert vor dem Nachweisabruf eine Abrufberechtigung von der Vermittlungsstelle an. Er &uuml;bermittelt dabei die f&uuml;r die Berechtigungspr&uuml;fung notwendigen Daten an die Vermittlungsstelle (Kommunikationspartner, Kommunikationszweck und ob die IDNr verwendet wird). Nach positivem Ergebnis der abstrakten Berechtigungspr&uuml;fung stellt die Vermittlungsstelle ein Abruftoken aus, das diesen Nachweisabruf autorisiert. In dem Abruftoken hinterlegt die Vermittlungsstelle die Datengrundlage der durchgef&uuml;hrten Pr&uuml;fung. Liegt das Abruftoken vor, versendet der Data Consumer daraufhin den Nachweis-Request gemeinsam mit dem Abruftoken an den Data Provider. Dieser kann mittels des Abruftokens nachvollziehen, ob der Nachweisabruf von der Vermittlungsstelle autorisiert wurde, indem er die Informationen im Abruftoken mit dem tats&auml;chlichen Nachweis-Request abgleicht. Au&szlig;erdem kann der Data Provider anhand des Zertifikats der Vermittlungsstelle pr&uuml;fen, dass das Abruftoken echt ist und nicht ver&auml;ndert wurde (Vertrauensstellung der Vermittlungsstelle). Um die Komplexit&auml;t f&uuml;r Data Consumer und Data Provider zu reduzieren, erfolgt eine Einschaltung der Vermittlungsstelle bei allen Nachweisabrufen &uuml;ber das NOOTS. Ist keine abstrakte Berechtigungspr&uuml;fung notwendig, stellt die Vermittlungsstelle pauschal ein Abruftoken aus und signalisiert so, dass sie die Nicht-Notwendigkeit der abstrakten Berechtigungspr&uuml;fung festgestellt hat. So ben&ouml;tigen Data Consumer und Data Provider kein Wissen dar&uuml;ber, wann eine abstrakte Berechtigungspr&uuml;fung notwendig ist.</p>
<p>Eine weitere Ebene der Sicherheit wird durch die Integration der Vermittlungsstelle in die Transportinfrastruktur erreicht. Die Transportinfrastruktur des NOOTS sieht den verpflichtenden Einsatz von "Sicheren Anschlussknoten" vor. Das sind NOOTS-Komponenten, die zentral bereitgestellt, aber dezentral durch die Data Consumer und Data Provider betrieben werden. Sie stellen die Endpunkte der NOOTS-Transportinfrastruktur dar und erm&ouml;glichen eine einfache und sichere Anbindung an das NOOTS. Da sie im Verantwortungsbereich von Data Consumer bzw. Data Provider betrieben werden, d&uuml;rfen sie Zugriff auf die unverschl&uuml;sselte Nachricht haben. So k&ouml;nnen sie sicherheitsrelevante Funktionen, wie die Verschl&uuml;sselung der Nachricht, &uuml;bernehmen. Insbesondere ist es ihnen m&ouml;glich, die f&uuml;r die abstrakte Berechtigungspr&uuml;fung relevanten Daten aus demNachweis-Requestzu ermitteln unddie notwendige Mitwirkung bei der Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung zu gew&auml;hrleisten. Dies erm&ouml;glicht die Verlagerung der Anforderung des Abruftokens vom Data Consumer zu seinem Sicheren Anschlussknoten und die Verlagerung der Pr&uuml;fung des Abruftokens vom Data Provider zu dessen Sicheren Anschlussknoten. Damit kann auf Seite des Data Consumers sichergestellt werden, dass die abstrakte Berechtigungspr&uuml;fung auf Basis der korrekten Angaben durchgef&uuml;hrt wird und dass kein Versand bei negativer Pr&uuml;fung erfolgt. Auf Seite des Data Providers erfolgt eine weitere Sicherungsma&szlig;nahme (entsprechend dem Zero-Trust-Prinzip) in Form der Pr&uuml;fung des Abruftokens durch den Sicheren Anschlussknoten (auf Echtheit sowie gegen den Nachrichteninhalt) vor Weitergabe des Requests an den Data Provider. Durch die Verankerung der Vermittlungsstelle in den Sicheren Anschlussknoten und die Notwendigkeit des Abruftokens der Vermittlungsstelle f&uuml;r den Transport wird die Vermittlungsstelle so zu einem integralen Bestandteil der Transportinfrastruktur des NOOTS.</p>
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/1.png" alt="" /></p>
<p><strong>Abb. 1: &Uuml;berblick Vermittlungsstelle</strong></p>
<p>Die Token-Architektur in Verbindung mit der Verankerung der Vermittlungsstelle &uuml;ber die Sicheren Anschlussknoten erm&ouml;glicht Zuverl&auml;ssigkeit der Vermittlungsstelle hinsichtlich der Durchsetzung ihres Ziels sowie eine L&ouml;sung, die gleichzeitig einfach umsetzbar f&uuml;r anschlie&szlig;ende Stellen ist und die Zukunftsf&auml;higkeit des NOOTS durch Entkopplung f&ouml;rdert.</p>

## 2. Einf&uuml;hrung und Ziele

### 2.1 &Uuml;berblick

<p>Mit der Einf&uuml;hrung der Identifikationsnummer (IDNr) nach dem Identifikationsnummerngesetz (IDNrG) wird ein &uuml;bergreifendes Ordnungsmerkmal f&uuml;r die &ouml;ffentliche Verwaltung geschaffen, das die verl&auml;ssliche Identifikation von B&uuml;rgerinnen und B&uuml;rgern in deutschen Registern erm&ouml;glichen soll, damit Daten ausdiesen Quellen eindeutig ermittelt und in der Verwaltung zuverl&auml;ssig verwendetwerden k&ouml;nnen. Dabei muss sichergestellt werden, dass die unzul&auml;ssige Zusammenf&uuml;hrung von Personendaten und die Bildung von Pers&ouml;nlichkeitsprofilen verhindert wird. Aus diesem Grund schreibt &sect; 7 Abs. 2IDNrG als Sicherungsma&szlig;nahme vor, dass Daten&uuml;bermittlungen unter Nutzung der Identifikationsnummer zwischen &ouml;ffentlichen Stellen verschiedener Verwaltungsbereiche durch die Vermittlungsstelle gepr&uuml;ft werden m&uuml;ssen. Gem. &sect; 12 Abs. 4 IDNrG ist eine Ausweitung des Verfahrens durch Beschluss m&ouml;glich, sodass eine abstrakte Berechtigungspr&uuml;fung auch innerhalb von einzelnen Verwaltungsbereichen durchgef&uuml;hrt wird.</p>

### 2.2 Ziele der Vermittlungsstelle

<p>Die Vermittlungsstelle soll als dritte &ouml;ffentliche Stelle unberechtigte Nachweisabrufe zwischen Data Consumer und Data Provider verhindern. Sie ist damit ein zentraler Baustein in der Kontrolle des Nachweisabrufs &uuml;ber das NOOTS.</p>
<p>Im Rahmen der abstrakten Berechtigungspr&uuml;fung pr&uuml;ft die Vermittlungsstelle, ob Data Consumer und Data Provider zu einem anzugebenden Zweck kommunizieren d&uuml;rfen. Liegt die abstrakte Berechtigung nicht vor, d.h. besteht f&uuml;r die Daten&uuml;bermittlung zwischen den Kommunikationspartnern zu einem gegebenen Zweck keine Rechtsgrundlage, d&uuml;rfen keine personenbezogenen Daten &uuml;bermitteltwerden.Dadurch soll der Gefahr begegnet werden, dass dasNOOTS als technisches System missbraucht werden kann, um umfassende Pers&ouml;nlichkeitsprofile der B&uuml;rgerinnen und B&uuml;rger zu erstellen (vgl. Begr&uuml;ndung von &sect; 7 Abs. 2 IDNrG, S. 73) (<a href="https://dserver.bundestag.de/btd/19/242/1924226.pdf">VS-GE</a>).</p>
<p>Durch die abstrakte Berechtigungspr&uuml;fung sollen unzul&auml;ssige Anfragen verhindert werden, die vor allem den Zweck der Profilbildung verfolgen k&ouml;nnten, denen also ggf. auch eine zielgerichtete Absicht zugrunde zu legen ist. Denkbar sind hier verschiedene Szenarien:</p>
<ul>
<li>Ein missbr&auml;uchlich agierender Data Consumer kann versuchen, Daten zu Personen abzurufen, um diese zu sammeln oder daraus Pers&ouml;nlichkeitsprofile zu erstellen.</li>
<li>Ein kompromittierter Data Consumer kann ebenso genutzt werden, um Datensammlung oder Profilbildung zu betreiben.</li>
</ul>
<p>Die Ziele der Vermittlungsstelle sind:</p>
<ol>
<li>Kontrolle des Nachweisabrufs mittels Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung bei verwaltungsbereichs&uuml;bergreifendem Nachweisabruf unter Verwendung der IDNr zur Verhinderung unzul&auml;ssiger Nachweisabrufe und insbesondere zur Verringerung des Risikos der Bildung von Pers&ouml;nlichkeitsprofilen,</li>
<li>Nachvollziebarkeit der Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung durch Protokollierung.</li>
</ol>

### 2.3 Begriffsdefinitionen

<p>Begriffsdefinitionen finden sich im Glossar. Im Folgenden sind Begriffe erl&auml;utert, die f&uuml;r das vorliegende Konzept besondere Relevanz haben.</p>
<p><strong>Tab. 1: Begriffsdefinitionen</strong></p>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>Begriff</th>
<th>Bedeutung</th>
</tr>
<tr>
<td>Vermittlungsstelle</td>
<td>
<p>Die Vermittlungsstelle nach &sect; 7 Abs. 2 IDNrG ist eine dritte &ouml;ffentliche Stelle, die beim bereichs&uuml;bergreifenden Nachweisabruf unter Verwendung der IDNr die abstrakte Berechtigungspr&uuml;fung durchf&uuml;hrt und so die Zul&auml;ssigkeit von Daten&uuml;bermittlung zwischen Data Consumer und Data Provider sicherstellt. Vorrangiges Ziel der Vermittlungsstelle ist, das Risiko der unzul&auml;ssigen Zusammenf&uuml;hrung von personenbezogenen Daten zur Bildung von Pers&ouml;nlichkeitsprofilen zu verringern.</p>
</td>
</tr>
<tr>
<td>Abstrakte Berechtigungspr&uuml;fung</td>
<td>Gem. &sect; 7 Abs. 2 IDNrG f&uuml;hrt die Vermittlungsstelle bei der bereichs&uuml;bergreifenden Daten&uuml;bermittlung unter Verwendung der IDNr eine abstrakte Berechtigungspr&uuml;fung durch und pr&uuml;ft, ob die beteiligten Kommunikationspartner zu einem anzugebenden Zweck miteinander kommunizieren d&uuml;rfen. Abstrakt bedeutet, dass die Pr&uuml;fung ohne Wissen &uuml;ber den Nachrichteninhalt und insbesondere die antragstellende Person erfolgt.</td>
</tr>
<tr>
<td>IDNr (Identifikationsnummer)</td>
<td>Die Identifikationsnummer nach &sect; 139 b Abgabenordnung (AO) wird nach IDNrG als zus&auml;tzliches Ordnungsmerkmal in allen von der Registermodernisierung betroffenen Register eingef&uuml;hrt, mit dem prim&auml;ren Zweck, die Daten einer nat&uuml;rlichen Person in einem Verwaltungsverfahren eindeutig zuordnen zu k&ouml;nnen.</td>
</tr>
<tr>
<td>Verwaltungsbereich (kurz: Bereich)</td>
<td>&sect; 7 Abs. 2 IDNrG sieht die Unterteilung der Gesamhtheit der Verwaltung in einzelne Verwaltungsbereiche (kurz: Bereiche) vor. Diese Bereiche sind so abzugrenzen, dass die verschiedenen Lebensbereiche einer Person unterschiedlichen Bereichen zugeordnet werden k&ouml;nnen (z.B. Inneres, Justiz, Wirtschaft und Finanzen, Arbeit und Soziales, Gesundheit, Statistik). F&uuml;r eine hinreichende Differenzierung soll es mindestens sechs Verwaltungsbereiche geben. Zahl und Abgrenzung der Bereiche ist gem. &sect; 12 Abs. 1 IDNrG durch Rechtsverordnung der Bundesregierung festzulegen.</td>
</tr>
<tr>
<td>Kommunikationszweck</td>
<td>Der Kommunikationszweck ist ein Pr&uuml;fkriterium im Rahmen der abstrakten Berechtigungspr&uuml;fung. Wesentlich f&uuml;r den Kommunikationszweck sind Art und Umfang der zu &uuml;bertragenden Daten. Der Kommunikationszweck wird abgebildet durch den Nachweistyp.Falls notwendig wird er durch die Angabe der dem Abruf zugrundeliegenden Rechtsnorm erg&auml;nzt.</td>
</tr>
<tr>
<td>Abstrakte Berechtigung</td>
<td>
<p>Eine abstrakte Berechtigung stellt die generelle Erlaubnis dar, dass zwei Kommunikationspartner zu einem bestimmten Kommunikationszweck miteinander kommunizieren d&uuml;rfen, d.h. ob ein Data Consumer berechtigt ist, von einem Data Provider bestimmte Daten zu einem bestimmten Zweck abzurufen.</p>
</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Ein Data Consumer ist eine IT-Komponente, die als nachweisabrufendes System am NOOTS teilnimmt. Er ist durch seine Komponenten-ID eindeutig identifizierbar (diese stammt aus dem IAM f&uuml;r Beh&ouml;rden).</td>
</tr>
<tr>
<td>Data Provider</td>
<td>Ein Data Provider ist eine IT-Komponente, die als nachweislieferndes System am NOOTS teilnimmt. Er ist durch seine Komponenten-ID eindeutig identifizierbar (diese stammt aus dem IAM f&uuml;r Beh&ouml;rden).</td>
</tr>
<tr>
<td>Verzeichnis / Verzeichnisdienst (f&uuml;r Berechtigungen)</td>
<td>
<p>F&uuml;r die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung ben&ouml;tigt die Vermittlungsstelle Zugriff auf ein Verzeichnis, in dem die abstrakten Berechtigungen hinterlegt sind. Dieses Verzeichnis kann sie selbst f&uuml;hren oder sie kann dazu auf einen Verzeichnisdienst zur&uuml;ckgreifen, aus dem sie die abstrakten Berechtigungen bezieht.</p>
<p>Aktuell wird evaluiert, inwieweit das Fachdatenkonzept (bzw. der Nachweiskatalog) in Betracht kommt,den Verzeichnisdienst und damit die Quelle f&uuml;r die abstrakten Berechtigungen bereitzustellen (vgl. <a href="#9-ausblick-und-weiterführende-aspekte">OP-VS-561</a>).</p>
</td>
</tr>
<tr>
<td>Fachdatenkonzept</td>
<td>
<p>Das Fachdatenkonzept liefert eine Semantik zum Nachweisdatenaustausch zwischen Data Consumern und Data Providern. Insbesondere stellt es das Bindeglied zwischen den Datenangeboten der Data Provider und den Datenbedarfen der Data Consumer dar. Dazu legt es eine Struktur der Daten zu Nachweisen (Nachweiskatalog) sowie ein Vorgehen zu deren Pflege fest.</p>
</td>
</tr>
<tr>
<td>IAM f&uuml;r Beh&ouml;rden</td>
<td>
<p>Das IAM f&uuml;r Beh&ouml;rden (Identity- und Access Managements f&uuml;r Beh&ouml;rden) ist eine zentrale IT-Komponente des NOOTS, die Identit&auml;tsinformationen und berechtigungsrelevante Daten zu anderen IT-Komponenten des NOOTS (insbes. zu Data Consumern und Data Providern) verwaltet und in Form des (IAM-)Zugriffstokens bereitstellt.</p>
</td>
</tr>
<tr>
<td>Token</td>
<td>
<p>Ein Token ist ein Datensatz, also eine Sammlung von Informationen, der durch den Herausgeber des Tokens gesiegelt wird und dessen Echtheit so f&uuml;r Dritte &uuml;berpr&uuml;fbar ist. Somit k&ouml;nnen mittels eines Tokens Daten zuverl&auml;ssig &uuml;bertragen und eine Manipulation ausgeschlossen werden.</p>
<p>Im NOOTS kommen zwei Tokens zum Einsatz: das Zugriffstoken des IAM f&uuml;r Beh&ouml;rden und das Abruftoken der Vermittlungsstelle.</p>
<ul>
<li>(IAM-) Zugriffstoken: Vor jedem Nachweisabruf muss sich der Data Consumer beim IAM f&uuml;r Beh&ouml;rden am NOOTS anmelden. Daraufhin erh&auml;lt er vom IAM f&uuml;r Beh&ouml;rden ein Zugriffstoken, das es ihm erm&ouml;glicht, auf NOOTS-Ressourcen zuzugreifen. Im Zugriffstoken ist u.a. die Komponenten-ID des Data Consumers, sein Verwaltungsbereich und seine Beh&ouml;rdenfunktion hinterlegt.</li>
<li>(VS-) Abruftoken: Vor dem Abruf des Nachweises beim Data Provider ist das Einholen der Abrufberechtigung (ggf. verbunden mit der Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung) notwendig. Dazu sendet der Sichere Anschlussknoten des Data Consumers die notwendigen Angaben an die Vermittlungsstelle. Bei positivem Ergebnis stellt die Vermittlungsstelle das Abruftoken als Nachweis der Berechtigung f&uuml;r den Nachweisabruf aus. Es berechtigt den Data Consumer zu diesem Nachweisabruf.</li>
</ul>
</td>
</tr>
<tr>
<td>Hash</td>
<td>
<p>Ein Hash ist eine kryptografische Funktion, die einen Datensatz (hier eine Nachricht) in eine feste L&auml;nge von Zeichen, den Hash-Wert, umwandelt.</p>
<p>Diese Funktion kann verwendet werden, um die Integrit&auml;t einer Nachricht zu &uuml;berpr&uuml;fen, da selbst geringf&uuml;gige &Auml;nderungen an der Nachricht zu einem v&ouml;llig anderen Hash-Wert f&uuml;hren.</p>
</td>
</tr>
<tr>
<td>Sicherer Anschlussknoten (SAK)</td>
<td>Ein Sicherer Anschlussknoten ist eine NOOTS-Komponente, die zentral bereitgestellt aber dezentral betrieben wird. Sie erm&ouml;glicht den Data Consumern und Data Providern einen einfachen und sicheren Anschluss an das NOOTS und den Versand und Empfang von Nachrichten &uuml;ber die NOOTS-Transportinfrastruktur. Der Sichere Anschlussknoten wird im Verantwortungsbereich der Data Consumers bzw. Data Providers betrieben. So hat er Zugriff auf die unverschl&uuml;sselten Nachrichteninhalte und kann Sicherheitsfunktionen &uuml;bernehmen. Insbesondere kontaktiert der Sichere Anschlussknoten die Vermittlungsstelle vor jedem Nachweisabruf, sodass ein Umgehen der Vermittlungsstelle nicht m&ouml;glich ist.</td>
</tr>
<tr>
<td>Zero-Trust-Prinzip</td>
<td>Das Zero-Trust-Prinzip ist ein Sicherheitskonzept, das davon ausgeht, dass kein Benutzer, Ger&auml;t oder Netzwerk von Natur aus vertrauensw&uuml;rdig ist. Nach dem Motto "niemals vertrauen, immer verifizieren" erfordert es kontinuierliche Authentifizierung und Autorisierung aller Nutzer und Systeme. Jede Zugriffsanfrage wird &uuml;berpr&uuml;ft, um sicherzustellen, dass nur autorisierte Nutzer und Systeme Zugang zu den Ressourcen erhalten.</td>
</tr>
</tbody>
</table>

## 3. Randbedingungen

### 3.1 Technische / Organisatorische / Rechtliche Randbedingungen

<p>Die Randbedingungen f&uuml;r die Konzeption der Vermittlungsstelle ergeben sich gro&szlig;teils aus rechtlichen und Datenschutz-Vorgaben.</p>

#### 3.1.1 Kein Zugriff auf personenbezogene Daten

<p>Um nicht selbst zu einer potentiellen Gefahrenquelle f&uuml;r den Datenschutz zu werden, darf die Vermittlungsstelle keine Kenntnis &uuml;ber die Inhalte der Kommunikation, die sie kontrolliert, erhalten. So ist sichergestellt, dass sie nicht selbst zur Bildung von Pers&ouml;nlichkeitsprofilen missbraucht werden kann.</p>

#### 3.1.2 &Ouml;ffentliche Stelle

<p>Die Vermittlungsstelle muss gem. &sect; 7 Abs. 2 IDNrG eine &ouml;ffentliche Stellen gem. &sect; 2 BDSG sein. Insbesondere muss sie eine dritte &ouml;ffentliche Stelle sein. Das bedeutet, sie muss technisch, organisatorisch und rechtlich von Data Consumer und Data Provider getrennt sein.</p>

#### 3.1.3 4-Corner-Modell

<p>Die Gesetzesbegr&uuml;ndung des RegMoG schl&auml;gt die Etablierung eines 4-Corner-Modells f&uuml;r die Umsetzung der Vermittlungsstelle vor, macht jedoch keine weiteren Angaben zu dessen Ausgestaltung. Das 4-Corner-Modell ist grunds&auml;tzlich ein Kommunikationsmodell, bei dem zwei Kommunikationspartner (Corner 1 und Corner 4) eine Nachricht austauschen wollen und bestimmte Aufgaben der Kommunikation an andere Rollen (Corner 2 und Corner 3) delegieren (vgl. Abb. 2).</p>
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/2.png" alt="" /></p>
<p><strong>Abb. 2: Allgemeines 4-Corner-Modell</strong></p>
<p>Ein solches 4-Corner-Modell wird in verschiedenen Formen und Kontexten angwandt, u.a. f&uuml;r Finanztransaktionen (vgl. Peppol, hier sind Corner 2 und Corner 3 durch Access Points repr&auml;sentiert).</p>
<p>Im &ouml;ffentlichen Bereich sind Corner 2 und Corner 3 oft gedanklich schon innerhalb einer Stelle von Sender bzw. Empf&auml;nger einer Nachricht verortet und &uuml;bernehmen dort Aufgaben wie bspw. Schema-Validierung, Verschl&uuml;sselung oder Versand der Nachricht. Eine Vermittlungsstelle, wie sie &sect; 7 Abs. 2 IDNrG vorsieht und insofern unabh&auml;ngig von den bereits etablierten Rollen ist, w&auml;re dabei gedanklich am ehesten zwischen Corner 2 und Corner 3 anzusiedeln, denn gem. &sect; 7 Abs. 2 IDNrG ist sie eine (dritte) &ouml;ffentliche Stelle und damit unabh&auml;ngig von Corner1/2 und Corner 3/4. Es handelt sich hier folglich eher um ein 5-Corner-Modell, in dem die Vermittlungsstelle eine unabh&auml;ngige Instanz darstellt, die die Kommuniktion zwischen Corner 1 und Corner 4 pr&uuml;ft (vgl. Abb. 3).</p>
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/3.png" alt="" /></p>
<p><strong>Abb. 3: Kommunikationsmodell mit einer unabh&auml;ngigen Instanz (Corner 5) mit Pr&uuml;ffunktion</strong></p>
<p>In diesem Modellansatz sind ebenso die Anforderungen zu ber&uuml;cksichtigen, die sich aus dem Wortlaut und dem Sinn und Zweck der Regelung ergeben.Sinn und Zweck des &sect; 7 Abs. 2 IDNrG ist entsprechend Gesetzesbegr&uuml;ndung die Schaffung einer Sicherung bei bereichs&uuml;bergreifender Daten&uuml;bermittlung, die ein besonderes Risiko der Pers&ouml;nlichkeitsprofilbildung bergen. Die wesentliche Sicherheitsfunktion der Vermittlungsstelle ist die unabh&auml;ngige Pr&uuml;fung der(abstrakten) &Uuml;bermittlungsberechtigung auf technischer Ebene, um dieses Risiko zu verringern. Dies findet in der beschrieben Architektur in Form des Token-Mechanismus und &uuml;ber eine Verankerung mittels der Sicheren Anschlussknoten Ber&uuml;cksichtigung.</p>

#### 3.1.4 Mitwirkungsnotwendigkeitder Kommunikationspartner

<p>Bei genauer Betrachtung stellt sich heraus, dass die Vermittlungsstelle der ihr aus Gr&uuml;nden des Datenschutzes zugedachten Aufgabe (Vermeidung des Risikos der Profilbildung) alleine (d.h. ohne die Mithilfe weiterer am Kommunikationsprozess beteiligter Akteure)nicht zufriedenstellend nachkommen kann.</p>
<p>F&uuml;r die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung ben&ouml;tigt sie Informationen &uuml;ber den Kommunikationszweck und die Verwendung der IDNr f&uuml;r den Nachweisabruf (sog. &ldquo;Metadaten der Kommunikation&rdquo;). Da sie keine Einsicht in den Nachrichteninhalt hat, muss sie diese Angaben vom Data Consumerer halten, d.h. von der Stelle, die die Anfrage stellt. Diese Angaben kann die Vermittlungsstelle jedoch nicht verifizieren, sie muss sich bei der Durchf&uuml;hrung der Pr&uuml;fung auf deren Richtigkeit verlassen. Die Vermittlungsstelle f&uuml;hrt die abstrakte Berechtigungspr&uuml;fung also vorbehaltlich der Richtigkeit der Angaben des Data Consumers durch.</p>
<p>Wegen der Ende-zu-Ende-Verschl&uuml;sselung hat au&szlig;er dem Data Consumer nur der Data Provider Einsicht in den Nachrichteninhalt. Folglich kann nur der Data Provider pr&uuml;fen, ob die Angaben des Data Consumers korrekt sind. F&uuml;r eine zuverl&auml;ssige abstrakte Berechtigungspr&uuml;fung ist es notwendig, dass der Data Provider die Angaben, die der abstrakten Berechtigungspr&uuml;fung zugrunde gelegt wurden, mit dem tats&auml;chlichen Nachrichteninhalt abgleicht und dadurch das Ergebnis der durch die Vermittlungsstelle durchgef&uuml;hrten abstrakten Berechtigungspr&uuml;fung best&auml;tigt.</p>
<p>Es wird somit deutlich, dass die Vermittlungsstelle f&uuml;r eine zuverl&auml;ssige Wahrnehmung ihrer Aufgabe die Mitwirkungder beteiligten Kommunikationspartner ben&ouml;tigt:</p>
<ul>
<li>Die Mitwirkung des Data Consumers, indem er wahrheitsgem&auml;&szlig;e Angaben &uuml;ber den beabsichtigten Nachweisabruf macht, und</li>
<li>die Mitwirkung des Data Providers, indem er pr&uuml;ft, ob der abstrakten Berechtigungspr&uuml;fung durch die Vermittlungsstelle wahrheitsgem&auml;&szlig;e Angaben zugrunde gelegt wurden.</li>
</ul>
<p>Diese Problematik wird durch die beschriebene L&ouml;sungsstrategie (ein Token-Ansatz, in dem die Vermittlungsstelle den Nachweisabruf durch Ausstellung eines Tokens autorisiert, welches einfach durch den Data Provider validiert werden kann) und im Zusammenwirken mit den "Sicheren Anschlussknoten" der NOOTS-Transportinfrastruktur adressiert.</p>

#### 3.1.5 Sichere Anschlussknoten

<p>F&uuml;r die Funktionsweise der Vermittlungsstelle ist der Aufbau der Transportinfrastruktur des NOOTS (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-19</a>) relevant. Vermittlungsstelle und Transportinfrastruktur sind konzeptionell so aufeinander abgestimmt, dass die Anforderungen des Datenschutzes an die Kontrolle des Nachweisabrufs im Allgemeinen und an die Vermittlungsstelle im Speziellen adressiert werden k&ouml;nnen.</p>
<p>Um die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung sicherzustellen, sieht die Konzeption zur Transportinfrastruktur den verpflichtenden Einsatz von sog. "Sicheren Anschlussknoten" bei Data Consumern und Data Providern vor. Dabei handelt es sich um NOOTS-Komponenten, die zentral bereitgestellt werden, jedoch dezentral betrieben werden. D.h. jeder Data Consumer und Data Provider betreibt seine eigene Instanz des Sicheren Anschlussknoten.</p>
<p>Der Sichere Anschlussknoten vereinfacht einerseits die Anbindung an das NOOTS und stellt andererseits sicherheitsrelevante Funktionalit&auml;ten zur Verf&uuml;gung. Da er im Verantwortungsbereich des Data Consumers bzw. Data Providers betrieben wird, hat er Zugriff auf die Inhaltsdaten und kann deshalb u.a. die Verschl&uuml;sselung des Requests &uuml;bernehmen. Durch die Einsicht in den XNachweis-Request hat der Sichere Anschlussknoten des Data Consumers die M&ouml;glichkeit, die relevanten Angaben f&uuml;r die abstrakte Berechtigungspr&uuml;fung direkt aus diesem zu ermitteln. So ist es m&ouml;glich, dass der Sichere Anschlussknoten</p>
<ul>
<li>auf Seite des Data Consumers die relevanten Angaben f&uuml;r die abstrakte Berechtigungspr&uuml;fung direkt aus dem XNachweis-Request ermittelt und an die Vermittlungsstelle &uuml;bergibt, und</li>
<li>auf Seite des Data Providers die relevanten Informationen ebenfalls aus dem XNachweis-Request ermittelt und mit den Daten, die die Vermittlungsstelle der abstrakten Berechtigungspr&uuml;fung zugrunde gelegt hat (und im Abruftokengespeichert hat), vergleicht.</li>
</ul>
<p>Dadurch ist die Vermittlungsstelle an zwei Punkten (jeweils in den Sicheren Anschlussknoten) im Nachrichtentransport relevant und ein negatives Ergebnis ihrer abstrakten Berechtigungspr&uuml;fung f&uuml;hrt an beiden Punkten dazu, dass der Nachrichtentransport unterbrochen wird.</p>
<ul>
<li>Meldet die Vermittlungsstelle ein negatives Ergebnis der abstrakten Berechtigungspr&uuml;fung und stellt kein Abruftoken aus, unterbricht der Sichere Anschlussknoten des Data Consumers die Kommunikation.</li>
<li>F&auml;llt die Pr&uuml;fung des Abruftokens negativ aus, unterbricht der Sichere Anschlussknoten des Data Providers die Kommunikation.</li>
</ul>
<p>Auf diese Weise ist es m&ouml;glich, gleichzeitig Zuverl&auml;ssigkeit bzgl. der Zielsetzung der Vermittlungsstelle sowie eine einfache L&ouml;sung mit wenig Aufwand f&uuml;r Data Consumer und Data Provider zu gew&auml;hrleisten.</p>

#### 3.1.6 Protokollierung

Die Protokollierung der Vermittlungsstelle verfolgt zwei grunds&auml;tzliche Zielsetzungen:
<ol>
<li>Fachliche Nachvollziehbarkeit (Protokollierung fachlich relevanter Ereignisse) mittels eine Audit Logs. Hier ist zu unterscheiden zwischen
<ol>
<li>Protokollierung der abstrakten Berechtigungspr&uuml;fung, und</li>
<li>Protokollierung der &Auml;nderungen an den abstrakten Berechtigungen.</li>
</ol>
</li>
</li>
<li>Technische Nachvollziehbarkeit (zur Fehleranalyse) mittels eines technischen Logs.</li>
</ol>
<p>Im Folgenden wird die fachliche Protokollierung betrachtet, die technische Protokollierung ist Gegenstand der Konzeption der Umsetzung.</p>
<ul>
<li>Gem. &sect; 7 Abs. 2 IDNrG muss die Vermittlungsstelle die abstrakte Berechtigungspr&uuml;fung protokollieren. Zielsetzung dieser Protokollierung ist die Analyse hinsichtlich Kontrollfunktion, Statistik und Qualit&auml;t des Dienstes. Die Vermittlungsstelle unterliegt nicht der Protokollierungspflicht gem. &sect; 9 IDNrG und liefert keine (Protokoll-)Daten an das Datenschutzcockpit. Sie verarbeitet keine personenbezogenen Daten (insbes. hat sie keinen Zugriff auf die IDNr).</li>
<li>&Auml;nderungen an den abstrakten Berechtigungen, dem zugrundeliegenden Regelwerk, m&uuml;ssen revisionssicher, d.h. insbes. vollst&auml;ndig, nachvollziehbar, unverz&uuml;glich und unver&auml;nderbar, protokolliert werden.</li>
</ul>
<p>Die fachlichen Protokolldaten unterliegen keiner L&ouml;schpflicht. Sie sind mindestens zwei Jahre aufzubewahren, um eine gemeinsame Auswertung mit anderen Protokolldaten, insbes. denen gem. &sect; 9 IDNrG, zu erm&ouml;glichen. Die Bereitstellung der Protokolldaten erfolgt in einer Weise die eine Analyse hinsichtlich der Zielsetzungen zul&auml;sst (bspw. durch einen strukturierten Export, ggf. auch aggregiert).</p>

### 3.2 Abgrenzungen

#### 3.2.1 Einschaltung der Vermittlungsstelle nur beim Nachweisabruf

<p>Die Vermittlungsstelle kommt nur beim Nachweisabruf zum Einsatz. Bei anderen Kommunikationsbeziehungen, bspw. beim Basisdatenabruf aus IDA, wird sie nicht eingeschaltet.</p>

#### 3.2.2 Verwaltungsbereiche

<p>Das IDNrG sieht die Definition von Verwaltungsbereichen vor (vgl. &sect; 12 Abs. 1 IDNrG). Bei bereichs&uuml;bergreifenden Daten&uuml;bermittlungen ist gem. &sect; 7 Abs. 2 IDNrG eine abstrakte Berechtigungspr&uuml;fung notwendig. Eine Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung innerhalb eines Verwaltungsbereichs ist m&ouml;glich (vgl. &sect; 12 Abs. 4 IDNrG).</p>
<p>Die Pflege der Verwaltungsbereiche inkl. der Zuordnung von Data Consumern und Data Providern zu Verwaltungsbereichen erfolgt au&szlig;erhalb der Vermittlungsstelle (ggf. im&nbsp;<a href="#4-kontextabgrenzung">Verzeichnisdienst</a>) und wird ihr &uuml;ber eine Schnittstelle zur Verf&uuml;gung gestellt.</p>

#### 3.2.3 IAM f&uuml;r Beh&ouml;rden

<p>Das IAM f&uuml;r Beh&ouml;rden ist eine zentrale NOOTS-Komponente, die Daten zu Identit&auml;t und Rollen von IT-Komponenten (also insbes. Data Consumern und Data Providern) bereitstellt. Am NOOTS teilnehmende IT-Komponenten erhalten vom IAM f&uuml;r Beh&ouml;rden nach Anmeldung ein IAM-Zugriffstoken, das f&uuml;r die weitere Kommunikation mit anderen NOOTS-Teilnehmern (zentralen Komponenten sowie bspw. Data Providern) ben&ouml;tigt wird. In diesem Token sind Identit&auml;tsdaten (insbes. die NOOTS-weit eindeutige Komponenten-ID und der Verwaltungsbereich) und Rollen sowie Teilnahmeart (bspw. "Data Consumer") hinterlegt.</p>
<p>Da sowohl die Vermittlungsstelle (das Abruftoken) als auch das IAM f&uuml;r Beh&ouml;rden (das Zugriffstoken) ein Token auf Basis &auml;hnlicher Standards verwenden, stellt sich die Frage, ob eine Zusammenlegung (auf fachlicher Ebene) zielf&uuml;hrend ist. Dies wurde aus verschiedenen Gr&uuml;nden verworfen, unter anderem:</p>
<ul>
<li>Vermittlungsstelle und IAM f&uuml;r Beh&ouml;rden haben eine unterschiedliche fachliche Zielstellung. W&auml;hrend das IAM f&uuml;r Beh&ouml;rden eine Authentifizierung von IT-Komponenten verbunden mit der Bereitstellung von Identit&auml;tsdaten f&uuml;r die Kontrolle des Zugriffs auf andere IT-Komponenten verfolgt, ist das Ziel der Vermittlungsstelle die fachliche Autorisierung eines einzelnen Nachweisabrufs.</li>
<li>Vermittlungsstelle und IAM f&uuml;r Beh&ouml;rden sind an verschiedenen Stellen in den Nachweisabrufprozess eingebunden, sodass kein Vorteil hinsichtlich der Anzahl der notwendigen Schnittstellen-Aufrufe erreicht werden kann. Das Zugriffstoken des IAM f&uuml;r Beh&ouml;rden muss direkt zu Beginn des Nachweisabrufprozesses abgerufen werden, bevor alle anderen Komponenten angefragt werden k&ouml;nnen. Das Abruftoken der Vermittlungsstelle kann erst abgerufen werden, nachdem zumindest der zust&auml;ndige Data Provider von der Registerdatennavigation (RDN) angefragt wurde (daf&uuml;r ist bereits das IAM-Zugriffstoken notwendig).</li>
<li>Das IAM f&uuml;r Beh&ouml;rden kann auf diese Weise perspektivisch als generischer Dienst auch &uuml;ber die Registermodernisierung hinaus verwendet werden, da es so keine registerspezifischen Vorgaben enth&auml;lt. Diest steht im Einklang mit den f&ouml;deralen Architekturrichtlinien u.a. zu Wiederverwendbarkeit und Modularit&auml;t.</li>
</ul>

#### 3.2.4 Verzeichnisdienst

F&uuml;r die Pflege der abstrakten Berechtigungen, d.h. die Zuordnung von Data Providern zu abrufberechtigten Data Consumern unter Angabe des zul&auml;ssigen Kommunkationszwecks, kann ein Verzeichnisdienst verwendet werden. Von diesem kann die Vermittlungsstelle die Daten abrufen, die sie f&uuml;r die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung ben&ouml;tigt. Zu beachten ist, dass eine Zuordnung der Daten des Verzeichnisdienstes (v.a. der Data Provider und abrufberechtigten Data Consumer) zu den Angaben aus der Berechtigungsanfrage des Data Consumers m&ouml;glich sein muss. Da diese Daten aus dem IAM f&uuml;r Beh&ouml;rden stammen, ist es notwendig, dass eine Synchronisation zwischen Verzeichnisdienst und IAM f&uuml;r Beh&ouml;rden zwecks Zuordenbarkeit stattfindet.
<p>Es wird aktuell davon ausgegangen,dass der Verzeichnisdienst durch das Fachdatenkonzept bzw. dessen Nachweiskatalog oder RegMo-Repository zur Verf&uuml;gung gestellt wird. Dies befindet sich in Pr&uuml;fung und Detaillierung (vgl. Kap. "<a href="#91-offene-punkte">Offene Punkte</a>").</p>

## 4. Kontextabgrenzung

### 4.1 Fachlicher Kontext

Folgend ist der fachliche Kontext der Vermittlungsstelle dargestellt.
<p><strong>Beachte</strong>: Hinsichtlich des Nachweisabrufsist dies eine <strong>vereinfachte Darstellung</strong>, die nur die <strong>aus Sicht der Vermittlungsstelle</strong> relevanten Aspekte enth&auml;lt. Eine vollst&auml;ndige Beschreibung findet sich in der High-Level-Architecture des NOOTS (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>) mit weiteren spezifischen Erg&auml;nzungen in den Architekturdokumentationen der einzelnen Komponenten, insbes. der Transportinfrastruktur.</p>
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/4.png" alt="" /></p>
<p><strong>Abb. 4: Fachlicher Kontext der Vermittlungsstelle</strong></p>
<p>Die Einbindung der Vermittlungsstelle erfolgt immer beim Abruf eines Nachweises durch einen Data Consumer bei einem Data Provider (ebenso bei EU-Nachweisabruf, hier agiert die Intermedi&auml;re Plattform jeweils als Data Consumer oder Data Provider).</p>
<ul>
<li>Dazu meldet sich der Data Consumer zun&auml;chst mittels des IAM f&uuml;r Beh&ouml;rden am NOOTS an. Er erh&auml;lt dann ein IAM-Zugriffstoken, das ihm die Kommunikation mit NOOTS-Komponenten und Data Providern erm&ouml;glicht.</li>
<li>Anschlie&szlig;end ermittelt er den f&uuml;r den gew&uuml;nschten Nachweis zust&auml;ndigen Data Provider mittels der Registerdatennavigation. Er ben&ouml;tigt daf&uuml;r das IAM-Zugriffstoken und erh&auml;lt die Daten des zust&auml;ndigen Data Providers (insbes. seine Komponenten-ID).</li>
<li>Vor dem Nachweisabruf selbst muss die abstrakte Berechtigungspr&uuml;fung erfolgen. Dazu &uuml;bermittelt der SAK des Data Consumers das IAM-Zugriffstoken (das die Identit&auml;t des Data Consumer &uuml;berpr&uuml;fbar enth&auml;lt) sowie weitere notwendige Angaben (u.a. Data Provider, Verwendung der IDNr und Kommunikationszweck) an die Vermittlungsstelle. Sie f&uuml;hrt (falls notwendig) die abstrakte Berechtigungspr&uuml;fung durch und stellt (bei positivem Ergebnis oder falls eine abstrakte Berechtigungspr&uuml;fung nicht notwendig ist) ein Abruftoken aus, das diesen Nachweisabruf autorisiert.</li>
<li>Schlie&szlig;lich ruft der Data Consumer den Nachweis ab. Dazu &uuml;bermittelt sein SAK den XNachweis-Request (inkl. Abruftoken, das im XNachweis-Request &uuml;bertragen wird) und das IAM-Zugriffstoken an den SAK des Data Providers.</li>
<li>Der SAK des Data Providers pr&uuml;ft zun&auml;chst die Echtheit des Abruftokens. Dazu ben&ouml;tigt er das Public-Key-Zertifikat der Vermittlungsstelle. Weiterhin pr&uuml;ft er die Daten des Abruftokens gegen die des XNachweis-Requests. Bei positivem Ergebnis gibt er den Request an den Data Provider weiter, woraufhin dieser den Request verarbeitet und die Response versendet.</li>
</ul>
<p>F&uuml;r die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung ben&ouml;tigt die Vermittlungsstelle Zugriff auf einen Verzeichnisdienst, der die abstrakten Berechtigungen bereitstellt.</p>
<ul>
<li>Von diesem Verzeichnisdienst ruft sie die Informationen f&uuml;r die abstrakte Berechtigungspr&uuml;fung ab (d.h. welcher Data Consumer bei welchem Data Provider zu welchem Zweck Daten abrufen darf).</li>
<li>Daf&uuml;r ist es notwendig, dass die Vermittlungsstelle die Informationen aus dem Verzeichnisdienst den im IAM f&uuml;r Beh&ouml;rden verwalteten IT-Komponenten (d.h. Data Consumern und Data Providern) eindeutig zuordnen kann, damit sie auf Basis der Informationen aus dem Zugriffstoken und der Komponenten-ID des Data Providers aus der RDN die Berechtigungspr&uuml;fung durchf&uuml;hren kann. Dazu muss eine Synchronisation zwischen Verzeichnisdienst und IAM f&uuml;r Beh&ouml;rden stattfinden.</li>
</ul>

### 4.2 Technischer Kontext

<p>Die Vermittlungsstelle wird (ggf. perspektivisch) ebenfalls &uuml;ber einen Sicheren Anschlussknoten an das NOOTS angebunden.</p>

## 5. Anforderungen

### 5.1 FunktionaleAnforderungen

<p><strong>Tab. 2: Funktionale Anforderungen zur Vermittlungsstelle</strong></p>
<div>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>
ID</th>
<th>Anforderung</th>
</tr>
<tr>
<td>
<p>AFO-VS-106</p>
</td>
<td>Die Vermittlungsstelle MUSS bei Berechtigungsanfragen f&uuml;r einen verwaltungsbereichs&uuml;bergreifenden Nachweisabruf unter Nutzung der IDNr eine abstrakte Berechtigungspr&uuml;fung durchf&uuml;hren.</td>
</tr>
<tr>
<td>
<p>AFO-VS-584</p>
</td>
<td>Die Vermittlungsstelle MUSS Berechtigungsanfragen und insbesondere die abstrakte Berechtigungspr&uuml;fung protokollieren.</td>
</tr>
<tr>
<td>
<p>AFO-VS-585</p>
</td>
<td>Die Vermittlungsstelle MUSS sicherstellen, dass die Abruferlaubnis nur dann erteilt wird, wenn sie die abstrakte Berechtigung positiv gepr&uuml;ft hat oder wenn sie festgestellt hat, dass diese Pr&uuml;fung nicht notwendig ist.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1004</p>
</td>
<td>Die Vermittlungsstelle MUSS Berechtigungsanfragenablehnen, falls die abstrakte Berechtigungspr&uuml;fung negativ ausf&auml;llt.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1005</p>
</td>
<td>Die Vermittlungsstelle MUSS die Protokolldaten zur Berechtigungspr&uuml;fung in einer Form bereitstellen, die eine Auswertung (insbes. hinsichtlich Kontrolle, Statistik und Qualit&auml;t des Dienstes) erm&ouml;glicht.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1006</p>
</td>
<td>Die Vermittlungsstelle MUSS die Durchf&uuml;hrung einer abstrakten Berechtigungspr&uuml;fung auch innerhalb eines Verwaltungsbereichs unterst&uuml;tzen.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1007</p>
</td>
<td>Die Vermittlungsstelle MUSS den Import der abstrakten Berechtigungen aus einem Verzeichnisdienst erm&ouml;glichen.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1008</p>
</td>
<td>Die Vermittlungsstelle MUSS &Auml;nderungen an den abstrakten Berechtigungen protokollieren.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1009</p>
</td>
<td>Die Vermittlungsstelle MUSS die Bereitstellung der Protokolldaten zu &Auml;nderungen an den abstrakten Berechtigungenin einer Form erm&ouml;glichen, die eine Auswertung (insbes. hinsichtlich Kontrolle) erm&ouml;glicht.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1014</p>
</td>
<td>Die Vermittlungsstelle MUSS die Datengrundlage f&uuml;r die abstrakte Berechtigungspr&uuml;fung (Kommunikationspartner, Kommunikationszweck, Verwendung IDNr) in der Abruferlaubnis hinterlegen, um eine Validierung auf Seite des Data Providers zu erm&ouml;glichen.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1015</p>
</td>
<td>Die Vermittlungsstelle MUSS die Abruferlaubnis siegeln, um eine Validierung auf Seite des Data Providers zu erm&ouml;glichen.</td>
</tr>
<tr>
<td>
<p>AFO-VS-1016</p>
</td>
<td>Die Vermittlungsstelle MUSS den Hash-Wert des XNachweis-Requests in der Abruferlaubnis hinterlegen (f&uuml;r eine Validierung auf Seite des Data Providers), um eine Wiederverwendung der Abruferlaubnis f&uuml;r andere Requests auszuschlie&szlig;en.</td>
</tr>
</tbody>
</table>
</div>

### 5.2 Nicht-funktionale Anforderungen

<p><strong>Tab. 3: Nicht-funktionale Anforderungen zur Vermittlungsstelle</strong></p>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>ID</th>
<th>Anforderung</th>
</tr>
<tr>
<td>
<p>AFO-VS-555</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS ihre Aufgaben ohne Kenntnis des Nachrichteninhalts erbringen.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-556</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS eine dritte (d.h. von Data Consumer und Data Provider unabh&auml;ngige) &ouml;ffentliche Stelle sein.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-1010</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS Berechtigungsanfragen innerhalb von weniger als 3 Sekunden beantworten.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-1011</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS 24/7 verf&uuml;gbar sein.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-1012</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS 99,9% der Zeit verf&uuml;gbar sein.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-1013</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS fachliche Protokolldaten mindestens 2 Jahre aufbewahren.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-1017</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS eine hohe noch zu definierende Anzahl von Anfragen pro Sekunde verarbeiten k&ouml;nnen.</p>
</td>
</tr>
<tr>
<td>
<p>AFO-VS-1018</p>
</td>
<td>
<p>Die Vermittlungsstelle MUSS skalierbar sein, d.h. mit Lastspitzen sowie einer Zunahme des Volumens &uuml;ber Zeit umgehen k&ouml;nnen.</p>
</td>
</tr>
</tbody>
</table>

## 6. L&ouml;sungsstrategie

### 6.1 Gepr&uuml;fte L&ouml;sungsentw&uuml;rfe und Hintergrund zur Architektur

<p>Im Rahmen der Konzeption sind zwei L&ouml;sungsans&auml;tze vertieft betrachtet worden:</p>
<ol>
<li>
Die Vermittlungsstelle ist Bestandteil der Kommunikationsverbindung zwischen Data Consumer und Data Provider.
</li>
<li>
Die Vermittlungsstelle ist nicht Bestandteil der Kommunikationsverbindung zwischen Data Consumer und Data Provider, sondern autorisiert die Kommunikation zwischen Data Consumer und Data Provider.
</li>
</ol>
<p>Der zweite Ansatz wird aus Architektursicht als geeigneter bewertet. Der Hintergrund ist im Folgenden erl&auml;utert.</p>
<p>Aufgabe der Vermittlungsstelle ist es, mittels Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung als Kontrollinstanz im Nachweisabruf das Risiko f&uuml;r die Profilbildung zu verringern (vgl. Kap. "<a href="#22-ziele-der-vermittlungsstelle">Ziele der Vermittlungsstelle</a>"). Dazu ist die Mitwirkung des Data Consumers bzw. des Data Providers notwendig (vgl. Kap. "<a href="#314-mitwirkungsnotwendigkeitder-kommunikationspartner">Mitwirkungsnotwendigkeit von mind. einem beteiligten Kommunikationspartner</a>"). Eine Vermittlungsstelle gem&auml;&szlig; dem ersten Ansatz, die allein auf der Transportstrecke verortet ist,kann ihrer Aufgabe nur dann gerecht werden, wenn die gleichen Anforderungen an Data Consumer und Data Provider gestellt werden und den beiden Kommunikationspartern die Mitwirkung entsprechend erm&ouml;glicht wird.</p>
<p>Aus Sicht der NOOTS-Gesamtarchitektur entspricht die abstrakte Berechtigungspr&uuml;fung der Autorisierung eines Zugriffs auf eine gesch&uuml;tzte Ressource (den Nachweis des Data Providers). Dies ist kein spezifisches Problem des NOOTS. In komplexen technischen Systemen wird dies regelm&auml;&szlig;ig mittels einer modernen Token-Architektur umgesetzt (oft wird dazu auf den verbreiteten Standard &ldquo;OAuth2&rdquo; zur&uuml;ckgegriffen). Ein solcher Token-Ansatz hat insbes. den Vorteil, dass die Validierung durch den Data Provider sehr einfach durchzuf&uuml;hren ist. Zun&auml;chst pr&uuml;ft er anhand der Siegelung, ob das Token durch die Vermittlungsstelle ausgestellt wurde. Dann gleicht er die im Token enthaltenen Informationen zu &bdquo;Verwendung der IDNr&ldquo;, &ldquo;Kommunikationspartner&ldquo; und &bdquo;Kommunikationszweck&rdquo; mit dem Nachrichteninhalt ab. Fallen beide Pr&uuml;fungen positiv aus, beginnt er mit der fachlichen Verarbeitung der Nachricht, andernfalls weist er die Anfrage zur&uuml;ck.</p>
<p>Die Transportinfrastruktur sieht den verpflichtenden Einsatz von "Sicheren Anschlussknoten" bei Data Consumern und Data Providern vor (vgl. Kap. "<a href="#315-sichere-anschlussknoten">NOOTS-Transportinfrastruktur &amp; Sichere Anschlussknoten</a>"). Diese k&ouml;nnen den Transport unterbrechen, noch bevor der Request den Data Provider erreicht.</p>
<p>Der beschriebene Ansatz einer Kombination aus Token-basierter Autorisierung des Nachweisabrufs im Zusammenspiel mit den Sicheren Anschlussknoten erm&ouml;glicht Zuverl&auml;ssigkeit und Sicherheit:</p>
<ul>
<li>Der verpflichtende Einsatz der Sicheren Anschlussknoten verankert den Aufruf der Vermittlungsstelle im Sicheren Anschlussknoten des Data Consumers.
<ul>
<li>
<strong>Der Aufruf der Vermittlungsstelle kann nicht umgangen werden</strong>.
</li>
</ul>
</li>
<li>Durch die Ermittlung der relevanten Daten f&uuml;r die abstrakte Berechtigungspr&uuml;fung durch den Sicheren Anschlussknoten des Data Consumers direkt aus XNachweis-Request ist es nicht m&ouml;glich, dass (versehentlich oder vors&auml;tzlich) vom Request abweichende Angaben an die Vermittlungsstelle &uuml;bermittelt werden. Der Sichere Anschlussknoten ermittelt die Pr&uuml;fungsgrundlage direkt aus dem XNachweis-Request, versendet die ermittelten Daten gemeinsam mit dem Zugriffstoken des Data Consumers an die Vermittlungsstelle und &uuml;bermittelt nach deren positiver R&uuml;ckmeldung den XNachweis-Requests unmittelbar an den Sicheren Anschlussknoten des Data Providers.<br />
<ul>
<li><strong>Die Richtigkeit der Daten, die an die Vermittlungsstelle f&uuml;r die abstrakte Berechtigungspr&uuml;fung &uuml;bermittelt werden, ist sichergestellt</strong>.</li>
</ul>
</li>
<li>Liegt die abstrakte Berechtigung nicht vor, versendet der Sichere Anschlussknoten des Data Consumers die Nachricht nicht.
<ul>
<li><strong>Eine Unterbrechung der Kommunikation bei negativem Pr&uuml;fergebnis noch auf Seite des Data Consumers ist sichergestellt</strong>.</li>
</ul>
</li>
<li>Der Sichere Anschlussknoten des Data Providers entschl&uuml;sselt die Nachricht und pr&uuml;ft das Token. Nur wenn das Token authentisch ist und zum Request passt, leitet er die entschl&uuml;sselte Nachricht an den Data Provider weiter (erneute Pr&uuml;fung auf Seite des Data Providers: Zero-Trust-Prinzip).
<ul>
<li><strong>Die Berechtigung wird erneut auf Seite des Data Providers gepr&uuml;ft (Zero-Trust-Prinzip), bevor der Request an den Data Provider weitergegeben wird</strong>.</li>
</ul>
</li>
</ul>
<p>Neben Zuverl&auml;ssigkeit, Sicherheit sowie geringenAufw&auml;nden f&uuml;r Data Consumer und Data Provider bietet dieserL&ouml;sungsansatz weitere Vorteile:</p>
<ul>
<li>Die Vermittlungsstelle ist nicht Bestandteil der Kommunikationsverbindung zwischen Data Consumer und Data Provider (wenn auch "integraler Bestandteil der Transportinfrastruktur" wegen der festen Verankerung in den Sicheren Anschlussknoten und der Unm&ouml;glichkeit des Transports ohne Token). Es findet also eine weitestm&ouml;gliche Entkopplung von Transportinfrastruktur und Vermittlungsstelle statt (die Vermittlungsstelle muss selbst keine Transportaufgaben wahrnehmen, es muss kein Routing "&uuml;ber die Vermittlungsstelle" erfolgen). Dies entspricht der Forderung der Architekturrichtlinie SR10 &ldquo;Sicherstellung von loser Kopplung/Modularit&auml;t&rdquo; des IT-Planungsrats (<a href="https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/">ITPLR-FAR</a>).</li>
<li>Dadurch wird die Komplexit&auml;t der Architektur und der Infrastruktur gesenkt. Dies beeinflusst Aufw&auml;nde f&uuml;r Umsetzung und Betrieb sowie die Fehleranf&auml;lligkeit in positiver Weise.</li>
<li>Durch die Vermeidung von Abh&auml;ngigkeiten schr&auml;nken sich Vermittlungsstelle und Transportinfrastruktur nicht gegenseitig ein. Beide k&ouml;nnen unabh&auml;ngig voneinander betrieben und weiterentwickelt werden, sodass die Zukunftsf&auml;higkeit steigt. Insbes. wird eine zuk&uuml;nftige Migration des Transportstandards vereinfacht.</li>
<li>Das Vorgehen erm&ouml;glicht es, auf markt&uuml;bliche Standards zur&uuml;ckzugreifen (z.B. OAuth). Dies entspricht der Forderung der Architekturrichtlinie SR3 &ldquo;Bestehende Marktstandards verwenden&rdquo; des IT-Planungsrats (<a href="https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/">ITPLR-FAR</a>).</li>
</ul>

### 6.2 Architekturentscheidungen

<strong>Es kommt im NOOTS eine zentrale Vermittlungsstelle zum Einsatz (NOOTS-AE: VS-2024-01)</strong>
<p>Die Vermittlungsstelle darf gem.&sect; 7 Abs. 2 IDNrG keine Kenntnis &uuml;ber die Inhalte der Kommunikation erhalten, die sie kontrolliert. Da sie also keine personenbezogenen Daten verarbeitet, ist es ihr folglich unm&ouml;glich, selbst Profilbildung zu betreiben. Es besteht somit aus Sicht des Datenschutz keine Notwendigkeit f&uuml;r eine h&ouml;here Anzahl von Vermittlungsstellen. Eine einzige zentrale Vermittlungsstelle f&uuml;r das NOOTS ist ausreichend und zielf&uuml;hrend.</p>
<p>Die Einrichtung einer einzigen zentralen Komponente entspricht den Ans&auml;tzen vergleichbarer NOOTS-Komponenten, die ebenfalls keine personenbezogenen Daten verarbeiten (bspw. Registerdatennavigation, IAM f&uuml;r Beh&ouml;rden). Auch f&uuml;r diese wird es nur eine zentrale Komponente f&uuml;r das gesamte NOOTS geben. Im Gegensatz dazu sind f&uuml;r die Intermedi&auml;re Plattform mehrere Instanzen vorgesehen, da die Intermedi&auml;re Plattform (anders als die Vermittlungsstelle und die vorgenannten Komponenten) Zugriff auf personenbezogene Daten hat.</p>
<p><strong>Die Vermittlungsstelle wird generell f&uuml;r alle Nachweisabrufe eingeschaltet (NOOTS-AE: VS-2024-02)</strong></p>
<p>Die Vermittlungsstelle wird f&uuml;r alle Nachweisabrufe im NOOTS eingeschaltet (d.h. f&uuml;r alle nationalen sowie f&uuml;r grenz&uuml;berschreitende Nachweisabrufe). Eine abstrakte Berechtigungspr&uuml;fung f&uuml;hrt sie nur durch, falls dies rechtlich notwendig ist. Ansonsten stellt sie das Token pauschal (d.h. ohne abstrakte Berechtigungspr&uuml;fung) aus.</p>
<p>Die generelle Einschaltung der Vermittlungsstelle bedeutet eine Gleichbehandlung aller Nachweisabrufe im NOOTS und hat eine wesentliche Komplexit&auml;tsreduktion zur Folge. Dadurch lassen sich verschiedene Vorteile realisieren.</p>
<ul>
<li>Der Data Consumer (bzw. dessen Sicherer Anschlussknoten) ben&ouml;tigt kein Wissen, ob die Vermittlungsstelle im konkreten Fall einzuschalten ist. Er wendet sich vor jedem Nachweisabruf an die Vermittlungsstelle und die Vermittlungsstelle entscheidet, ob eine abstrakte Berechtigungspr&uuml;fung notwendig ist.</li>
<li>Das gleiche gilt f&uuml;r den Data Provider (bzw. dessen Sicheren Anschlussknoten). Dadurch vereinfacht sich die Pr&uuml;fung des Tokens durch den Data Provider. Bei selektiver Einschaltung der Vermittlungsstelle m&uuml;sste diese Logik den Data Providern (bzw. deren Sicheren Anschlussknoten) bekannt sein, damit zun&auml;chst gepr&uuml;ft werden kann, ob ein Token f&uuml;r die vorliegende Kommunikation notwendig ist. Da die Vermittlungsstelle aber pauschal ein Token ausstellt, das signalisiert, dass diese Pr&uuml;fung bereits an anderer Stelle stattgefunden hat, muss diese Logik nicht beim Data Provider repliziert werden. Die Pr&uuml;fung des Tokens f&uuml;r den Data Provider vereinfacht sich ma&szlig;geblich.</li>
<li>Die generelle Einschaltung der Vermittlungsstelle in den Nachweisabruf erh&ouml;ht die Erweiterbarkeit der abstrakten Berechtigungspr&uuml;fung (bspw. die Ausweitung auf eine Anwendung auch innerhalb bestimmter Verwaltungsbereiche gem. &sect; 12 Abs. 4 IDNrG) und damit die Zukunftsf&auml;higkeit der Vermittlungsstelle. Es gibt so eine zentrale Komponente, f&uuml;r die diese Informationen relevant sind und gepflegt werden m&uuml;ssen. Aus Sicht der Data Consumer und Data Provider ist im Fall einer solchen Ausweitung der abstrakten Berechtigungspr&uuml;fung keine Anpassung notwendig.</li>
</ul>
<p>Die aus dieser Entscheidung resultierende Laststeigerung ist als gering und insbes. handhabbar einzusch&auml;tzen, da davon auszugehen ist, dass perspektivisch ein Großteil der Abrufe unter Verwendung der IDNr stattfinden wird.</p>
<p><strong>Eine Einbindung der Vermittlungsstelle im Nachweisabrufprozess ist nur einmal notwendig (auf Seite des Data Consumers)</strong></p>
<ul>
<li>Der Sichere Anschlussknoten des Data Providers kann die Pr&uuml;fung des Tokens alleine durchf&uuml;hren. Die Echtheit des Tokens stellt er durch Pr&uuml;fung der Siegelung des Tokens (mittels des Public-Key-Zertifikats der Vermittlungsstelle) sicher. Der Abgleich der Daten aus Token und Request erfolgt auf Basis der von der Vermittlungsstelle im Token hinterlegten Pr&uuml;fungsgrundlage.</li>
<li>Es ist auf Seite des Data Providers somit keine erneute Anfrage bei der Vermittlungsstelle erforderlich, um ein Token zu validieren.</li>
</ul>
<p><strong>Eine Pr&uuml;fung der Response durch den Sicheren Anschlussknoten des Data Consumers ist nicht notwendig</strong></p>
<ul>
<li>Die Kommunikation zwischen den Sicheren Anschlussknoten von Data Consumer und Data Provider erfolgt nur entsprechend dem Request-Response-Nachrichtenaustauschmuster (vgl. Kap.Nachrichtenaustauschmusterder High-Level-Architecture des NOOTS (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>)).</li>
<li>Es ist f&uuml;r den Sicheren Anschlussknoten des Data Consumers deshalb technisch nicht m&ouml;glich, Daten mittels einer Response zu erhalten, zu der er vorher keinen Request versendet hat.</li>
<li>Es kann sich bei einer eingehenden Nachricht also nur um eine Response auf einen Requests handeln, der bereits durch die Vermittlungsstelle freigegeben wurde.</li>
</ul>

## 7. Bausteinsicht

### 7.1 Datenmodell

<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/5.png" alt="" /></p>
<p><strong>Abb. 5: Fachliches Datenmodell</strong></p>
<p>Die Vermittlungsstelle ben&ouml;tigt f&uuml;r die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung die Information, welche Kommunikationspartner zu welchem Zweck miteinander kommunizieren d&uuml;rfen ("abstrakte Berechtigung"). Der Kommunikationszweck ergibt sich dabei aus dem angefragtem Nachweistyp, ggf. in Verbindung mit einer Rechtsgrundlage (alternativ ist eine Abbildung der Rechtsgrundlage durch Ber&uuml;cksichtigung bereits im Rahmen des Pflegeprozesses der abstrakten Berechtigungen im Verzeichnisdienst denkbar).</p>
<p>Dar&uuml;ber hinaus ben&ouml;tigt die Vermittlungsstelle die Information, ob in einem Verwaltungsbereich eine Berechtigungspr&uuml;fung auch innerhalb des Bereichs vorgesehen ist, falls beide Kommunikationspartner zum gleichen Verwaltungsbereich geh&ouml;ren.</p>
<p>Die dargestellten Daten erh&auml;lt die Vermittlungsstelle durch regelm&auml;&szlig;ige Synchronisation &uuml;ber eine Schnittstelle zum Verzeichnisdienst.</p>

### 7.2 Facharchitektur

#### 7.2.1 Bausteine undAbh&auml;ngigkeiten

<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/6.png" alt="" /></p>
<p><strong>Abb. 6: Fachliche Architektur der Vermittlungsstelle</strong></p>

#### 7.2.2 Abruftoken

<p>Nach positiver Pr&uuml;fung stellt die Vermittlungsstelle ein Abruftoken f&uuml;r einen Nachweisabruf aus. In diesem Token speichert sie die Informationen, auf deren Basis sie die Berechtigungspr&uuml;fung durchgef&uuml;hrt hat, und siegelt das Token, damit die Echtheit &uuml;berpr&uuml;ft werden kann.</p>
<p>Folgend ist der (fachliche) Inhalt des Abruftokens dargestellt.</p>
<p><strong>Tab. 4: Inhalt des Abruftokens der Vermittlungsstelle<br /></strong></p>
<div>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Name</th>
<th>Bedeutung</th>
<th>Herkunft</th>
</tr>
<tr>
<td>Data Consumer</td>
<td>Komponenten-ID des Data Consumer</td>
<td>IAM-Zugriffstoken des Data Consumers</td>
</tr>
<tr>
<td>Data Provider</td>
<td>Komponenten-ID des Data Providers</td>
<td>Angabe des Data Consumers (Teil des XNachweis-Requests)</td>
</tr>
<tr>
<td>Kommunikationszweck</td>
<td>Der angefragte Nachweistyp ggf. in Verbindung mit der zugrundeliegenden Rechtsnorm</td>
<td>Angabe des Data Consumers (Teil des XNachweis-Requests)</td>
</tr>
<tr>
<td>Verwendung der IDNr</td>
<td>Information, ob der Nachweisabruf mittels der IDNr erfolgt (boolean: ja/nein)</td>
<td>Angabe des Data Consumers (Teil des XNachweis-Requests)</td>
</tr>
<tr>
<td>Hash-Wert des XNachweis-Requests</td>
<td>Der Hash-Wert, der aus dem XNachweis-Request berechnet wurde. Damit erfolgt ein Binding des Abruftokens an einen konkreten Nachweisabruf, wodurch eine Wiederverwendung des Tokens ausgeschlossen wird.</td>
<td>Ermittlung auf Basis des XNachweis-Requests durch den Sicheren Anschlussknoten</td>
</tr>
<tr>
<td>Request-ID</td>
<td>Die ID des XNachweis-Requests</td>
<td>Angabe des Data Consumers (Teil des XNachweis-Requests)</td>
</tr>
</tbody>
</table>
</div>

#### 7.2.3 Schnittstellen zu anderen IT-Systemen

<p><strong>Tab. 5: Schnittstelle f&uuml;r die Berechtigungspr&uuml;fung</strong></p>
<div>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>Anbietendes System</th>
<td>Vermittlungsstelle</td>
</tr>
<tr>
<th>Nutzendes System</th>
<td>Sicherer Anschlussknoten des Data Consumers</td>
</tr>
<tr>
<th>Anwendungsfall</th>
<td>UC-VS-1 Berechtigung pr&uuml;fen</td>
</tr>
<tr>
<th>Kurzbeschreibung</th>
<td>Ein Sicherer Anschlussknoten eines Data Consumers fragt eine Abrufberechtigung f&uuml;r einen Kommunikationsvorgang (charakterisiert durch Kommunikationspartner, Kommunikationszweck, Verwendung der IDNr) an. Die Vermittlungsstelle pr&uuml;ft, ob f&uuml;r diesen Kommunikationsvorgang eine abstrakte Berechtigung notwendig ist und vorliegt und stellt ggf. ein Abruftoken aus, das den Nachweisabruf autorisiert.</td>
</tr>
<tr>
<th>Input-Parameter</th>
<td>
<ul>
<li>IAM-Zugriffstoken des Data Consumer, enth&auml;lt:
<ul>
<li>Komponenten-ID des Data Consumer</li>
</ul>
</li>
<li>Komponenten-ID des Data Providers</li>
<li>Kommunikationszweck (angefragter Nachweistyp, ggf. Rechtsgrundlage f&uuml;r den Abruf)</li>
<li>Verwendung der IDNr f&uuml;r den Abruf (ja/nein)</li>
<li>Hash-Wert des XNachweis-Requests</li>
<li>Request-ID</li>
</ul>
</td>
</tr>
<tr>
<th>Output-Parameter</th>
<td>
<ul>
<li>VS-Abruftoken, enth&auml;lt:<br />
<ul>
<li>Komponenten-ID des Data Consumers</li>
<li>Komponenten-ID des Data Providers</li>
<li>Kommunikationszweck (angefragter Nachweistyp, ggf. Rechtsgrundlage f&uuml;r den Abruf)</li>
<li>Verwendung der IDNr f&uuml;r den Abruf (ja/nein)</li>
<li>Hash-Wert des XNachweis-Requests</li>
<li>Request-ID</li>
</ul>
</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
<p><strong>Tab. 6: Schnittstelle f&uuml;r die Aktualisierung der Berechtigungsdaten</strong></p>
<div>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>Anbietendes System</th>
<td>Verzeichnisdienst</td>
</tr>
<tr>
<th>Nutzendes System</th>
<td>Vermittlungsstelle</td>
</tr>
<tr>
<th>Anwendungsfall</th>
<td>UC-VS-2 Abstrakte Berechtigungen aktualisieren</td>
</tr>
<tr>
<th>Kurzbeschreibung</th>
<td>Der Verzeichnisdienst stellt Informationen bereit, mittels der er die Vermittlungsstelle ihren Datenbestand der abstrakten Berechtigungen aktualisieren kann.</td>
</tr>
<tr>
<th>Input-Parameter</th>
<td>
<p><em>keine</em></p>
</td>
</tr>
<tr>
<th>Output-Parameter</th>
<td>
<ul>
<li>Aktualisierungsinformationen zum Datenbestand der abstrakten Berechtigungen</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>

## 8. Laufzeitsicht

### 8.1 Prozesse

Folgend ist die Einbindung der Vermittlungsstelle in den Nachweisabrufprozess dargestellt. Es handelt sich um eine (auf die aus Sicht der Vermittlungsstelle wesentlichen Aspekte) <strong>beschr&auml;nkende Betrachtung</strong>.
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/images8/media/7.png" alt="" /></p>
<p><strong>Abb. 7: Prozesssicht der Berechtigungspr&uuml;fung im Kontext des Nachweisabrufprozesses<br /></strong></p>
<p>Folgend ist die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung im Kontext eines nationalen Nachweisabrufs durch einen Data Consumer bei einem Data Provider beschrieben (Darstellung aus Perspektive der Vermittlungsstelle, d.h. Aspekte, die daf&uuml;r nicht relevant sind, sind aus Gr&uuml;nden der &Uuml;bersichtlichkeit weggelassen und k&ouml;nnen in der High-Level-Architecture sowie den Konzepten der jeweiligen Komponenten nachgeschlagen werden).</p>
<p><strong>Tab. 7: Nationaler Nachweisabrufpross aus Perspektive der Vermittlungsstelle</strong></p>
<div>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>&nbsp;</th>
<th>Beschreibung</th>
<th>Bemerkung</th>
</tr>
<tr>
<td>1</td>
<td>&Uuml;ber einen Data Consumer wird ein Nachweisabruf initiiert.</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>2</td>
<td>Der Data Consumer meldet sich beim IAM f&uuml;r Beh&ouml;rden am NOOTS an und erh&auml;lt vom IAM f&uuml;r Beh&ouml;rden ein Zugriffstoken.</td>
<td>
<ul>
<li>Das Zugriffstoken wird f&uuml;r jegliche weitere Kommunikation im NOOTS ben&ouml;tigt. Es enth&auml;lt u.a. die Angabe zur Komponenten-ID des DataConsumers.</li>
<li>Die Kommunikation des Data Consumers mitzentralen Komponentenund dem Data Provider erfolgt &uuml;ber seinen Sicheren Anschlussknoten.</li>
</ul>
</td>
</tr>
<tr>
<td>3</td>
<td>Der Data Consumer ermittelt &uuml;ber die Registerdatennavigation (RDN) den zust&auml;ndigen Data Provider f&uuml;r den gew&uuml;nschten Nachweis.</td>
<td>
<ul>
<li>Der zust&auml;ndige Data Provider ist durch dessen Komponenten-ID charakterisiert.</li>
</ul>
</td>
</tr>
<tr>
<td>4</td>
<td>Der Data Consumer erstellt einen XNachweis-Request und &uuml;bergibt ihn f&uuml;r den Abruf des Nachweises an seinen Sicheren Anschlussknoten.</td>
<td>
<ul>
<li>Der XNachweis-Request enth&auml;lt die Komponenten-ID des zust&auml;ndigen Data Providers.</li>
<li>Der Data Consumer &uuml;bergibt sein Zugriffstoken zusammen mit dem XNachweis-Request.</li>
<li>Auf die Beschreibung der Ermittlung der Identifikationsnummer des Nachweissubjekts wird hier verzichtet.</li>
</ul>
</td>
</tr>
<tr>
<td>5</td>
<td>
<p>Der Sichere Anschlussknoten des Data Consumers ermittelt aus dem Request die notwendigen Angaben, die die Vermittlungsstelle f&uuml;r die abstrakte Berechtigungspr&uuml;fung ben&ouml;tigt:</p>
<ul>
<li>Komponenten-ID des Data Providers</li>
<li>Kommunikationszweck (angefragter Nachweistyp, ggf. Rechtsgrundlage f&uuml;r den Abruf)</li>
<li>Verwendung der IDNr f&uuml;r den Abruf (ja/nein)</li>
<li>Hash-Wert des XNachweis-Requests ohne Abruftoken (diesen erzeugt der Sichere Anschlussknoten selbst)</li>
<li>Request-ID</li>
</ul>
</td>
<td>
<ul>
<li>&Uuml;ber den Hash-Wert erfolgt ein Binding des Abruftokens der Vermittlungsstelle an den XNachweis-Request und eine Wiederverwendung des Abruftokens wird ausgeschlossen. Da somit jeder Nachweisbaruf &uuml;ber die Vermittlungsstelle angefragt werden muss, ist auch sichergestellt, dass die Vermittlungsstelle jeden Abruf protokolliert.</li>
</ul>
</td>
</tr>
<tr>
<td>6</td>
<td>Der Sichere Anschlussknoten des Data Consumers fragt bei der Vermittlungsstelle die Abrufberechtigung f&uuml;r den Nachweisabruf an und &uuml;bergibt dazu die im vorhergehenden Schritt ermittelten Daten gemeinsam mit dem Zugriffstoken.</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>7</td>
<td>
<p>Die Vermittlungsstelle pr&uuml;ft, ob f&uuml;r die vorliegende Anfrage eine abstrakte Berechtigungspr&uuml;fung notwendig ist.</p>
</td>
<td>
<ul>
<li>Eine abstrakte Berechtigungungspr&uuml;fung ist dann notwendig, wenn die Kommunikation unter Verwendung der IDNr und entweder bereichs&uuml;bergreifend stattfindet (&sect; 7 Abs. 2 IDNrG) oder wenn eine Pr&uuml;fung auch bereichsintern vorgesehen ist (&sect; 12 Abs. 4 IDNrG).</li>
</ul>
</td>
</tr>
<tr>
<td>8</td>
<td>Falls notwendig pr&uuml;ft die Vermittlungsstelle die abstrakte Berechtigung f&uuml;r den Nachweisabruf. Dazu ermittelt sie, ob f&uuml;r die beiden Kommunikationspartner ein gemeinsamer Kommunikationszweck vorliegt, der mit dem angegebenen Zweck &uuml;bereinstimmt.</td>
<td>
<ul>
<li>Die daf&uuml;r notwendigen Informationen (die abstrakten Berechtigungen) erh&auml;lt die Vermittlungsstelle regelm&auml;&szlig;ig aus dem Verzeichnisdienst.</li>
</ul>
</td>
</tr>
<tr>
<td>9</td>
<td>
<p>Die Vermittlungsstelle protokolliert die Berechtigungspr&uuml;fung. M&ouml;gliche Pr&uuml;fergebnisse sind:</p>
<ul>
<li>Abstrakte Berechtigung liegt vor</li>
<li>Abstrakte Berechtigung liegt nicht vor</li>
<li>Abstrakte Berechtigung nicht notwendig</li>
</ul>
</td>
<td>
<ul>
<li>Falls die Vermittlungsstelle feststellt, dass die abstrakte Berechtigung nicht vorliegt, meldet sie dies dem aufrufenden Sicheren Anschlussknoten &uuml;ber eine Fehlermeldung. Insbesondere stellt sie kein Abruftoken aus.</li>
<li>Neben dem Pr&uuml;fergebnis protokolliert die Vermittlungsstelle auch die Pr&uuml;fungsgrundlage sowie Metadaten der Pr&uuml;fung (insbes. Zeitstempel).</li>
</ul>
</td>
</tr>
<tr>
<td>10</td>
<td>
<p>Die Vermittlungsstelle stellt ein Abruftoken aus und siegelt dieses. In dem Abruftoken speichert sie die Pr&uuml;fungsgrundlage:</p>
<ul>
<li>Komponenten-ID des Data Consumers</li>
<li>Komponenten-ID des Data Providers</li>
<li>Kommunikationszweck (angefragter Nachweistyp, ggf. Rechtsgrundlage f&uuml;r den Abruf)</li>
<li>Verwendung der IDNr f&uuml;r den Abruf (ja/nein)</li>
<li>Hash-Wert des XNachweis-Requests</li>
<li>Request-ID</li>
</ul>
</td>
<td>
<ul>
<li>Die Vermittlungsstelle stellt das Abruftoken aus, falls die abstrakte Berechtigung vorliegt oder falls die abstrakte Berechtigung nicht notwendig ist.</li>
<li>Das Abruftoken stellt die Abrufberechtigung f&uuml;r diesen Nachweisabruf dar. Es beweist, dass die Vermittlungsstelle
<ul>
<li>entweder die abstrakte Berechtigungspr&uuml;fung mit positivem Ergebnis durchgef&uuml;hrt hat,</li>
<li>oder festgestellt hat, dass eine abstrakte Berechtigungspr&uuml;fung nicht notwendig ist.</li>
</ul>
</li>
</ul>
</td>
</tr>
<tr>
<td>11</td>
<td>
<p>Die Vermittlungsstelle &uuml;bermittelt das Abruftoken an den Sicheren Anschlussknoten des Data Consumers.</p>
</td>
<td>
<ul>
<li>Falls die Vermittlungsstelle kein Abruftoken zur&uuml;ckliefert und somit keine Abrufberechtigung erteilt, bricht der Sichere Anschlussknoten des Data Consumers den Nachweisabruf ab.</li>
</ul>
</td>
</tr>
<tr>
<td>12</td>
<td>
<p>Der Sichere Anschlussknoten des Data Consumers f&uuml;hrt den Nachweisabruf durch. Dazu speichert er das Abruftoken im XNachweis-Request und &uuml;bermittelt den XNachweis-Request (inkl. dem Abruftoken) sowie das Zugriffstoken (des IAM f&uuml;r Beh&ouml;rden) an den Sicheren Anschlussknoten des zust&auml;ndigen Data Providers.</p>
</td>
<td>
<ul>
<li>Auf die Beschreibung der Ermittlung der Verbindungsparameter des Data Providers wird hier verzichtet.</li>
</ul>
</td>
</tr>
<tr>
<td>13</td>
<td>
<p>Der Sichere Anschlussknoten des Data Providers entnimmt das Abruftoken der Vermittlungsstelle aus dem XNachweis-Request und pr&uuml;ft die Echtheit des Abruftokens anhand der Siegelung mittels des Public-Key-Zertifikats der Vermittlungsstelle.</p>
</td>
<td>
<ul>
<li>Falls die Echtheit des Abruftokens nicht festgestellt werden kann, bricht der Sichere Anschlussknoten den Nachweisabruf ab und gibt eine XNachweis-ErrorResponse zur&uuml;ck.</li>
</ul>
</td>
</tr>
<tr>
<td>14</td>
<td>
<p>Der Sichere Anschlussknoten des Data Providers pr&uuml;ft, ob die Angaben im Abruftoken zum XNachweis-Request passen. Dazu ermittelt er die folgenden Daten direkt aus dem XNachweis-Request (bzw. aus dem Zugriffstoken des Data Consumers) und gleicht sie mit denen im Abruftoken ab:</p>
<ul>
<li>Komponenten-ID des Data Consumers (aus dem Zugriffstoken)</li>
<li>Komponenten-ID des Data Providers</li>
<li>Kommunikationszweck (angefragter Nachweistyp, ggf. Rechtsgrundlage f&uuml;r den Abruf)</li>
<li>Verwendung der IDNr f&uuml;r den Abruf (ja/nein)</li>
<li>Erzeugung des Hash-Wert des XNachweis-Requests</li>
<li>Request-ID</li>
</ul>
</td>
<td>
<ul>
<li>Falls die Validierung des Abruftokens fehlschl&auml;gt, d.h. falls der Sichere Anschlussknoten Abweichungen zwischen Request und Abruftoken feststellt, bricht er den Nachweisabruf ab und gibt eine XNachweis-ErrorResponse zur&uuml;ck.</li>
</ul>
</td>
</tr>
<tr>
<td>15</td>
<td>
<p>DerSichere Anschlussknoten des Data Providers &uuml;bergibt den XNachweis-Request an den Data Provider.</p>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>16</td>
<td>
<p>Der Data Provider verarbeitet den XNachweis-Request, erstellt eine XNachweis-Response mit dem angeforderten Nachweis und &uuml;bergibt sie an seinen Sicheren Anschlussknoten.</p>
</td>
<td>
<ul>
<li>Weitere Schritte des Data Providers sind hier nicht beschrieben (bspw. Protokollierung f&uuml;r das Datenschutzcockpit, konkrete Berechtigungspr&uuml;fung).</li>
</ul>
</td>
</tr>
<tr>
<td>17</td>
<td>
<p>Der Sichere Anschlussknoten des Data Provider versendet die XNachweis-Response an den Sicheren Anschlussknoten des Data Consumers. Dieser leitet ihn an den Data Consumer weiter.</p>
</td>
<td>
<ul>
<li>Eine erneute Einbindung der Vermittlungsstelle sowie eine Pr&uuml;fung der Response durch den Sicheren Anschlussknoten des Data Consumers ist nicht notwendig (vgl. Kap. <a href="#62-architekturentscheidungen">Architekturentscheidungen</a>).</li>
</ul>
</td>
</tr>
<tr>
<td>18</td>
<td>
<p>Der Data Consumer verarbeitet die Response.</p>
</td>
<td>
<ul>
<li>Der Nachweisabruf ist abgeschlossen, der Nachweis liegt dem Data Consumer vor und kann bspw. in der Preview gepr&uuml;ft und freigegeben werden.</li>
</ul>
</td>
</tr>
</tbody>
</table>

### 8.2 Anwendungsf&auml;lle

#### 8.2.1 Anwendungsfall 1: Berechtigung pr&uuml;fen

<p><strong>Tab. 8: Beschreibung des Anwendungsfalls "Berechtigung pr&uuml;fen"<br /></strong></p>
<div>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>Anwendungsfall ID
</th>
<th>UC-VS-1</th>
</tr>
<tr>
<td>
<p>Kurzbeschreibung</p>
</td>
<td>
<p>Pr&uuml;fung auf Notwendigkeit einer abstrakten Berechtigungspr&uuml;fung, bei Bedarf Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung, Protokollierung der Pr&uuml;fung und ggf. Ausstellung eines Abruftokens.</p>
</td>
</tr>
<tr>
<td>
<p>Vorbedingung / ausl&ouml;sendes Ereignis</p>
</td>
<td>
<p>Der Sichere Anschlussknoten eines Data Consumers ruft die Schnittstelle f&uuml;r die Berechtigungspr&uuml;fung der Vermittlungsstelle auf, da der Data Consumer einen Nachweisabruf durchf&uuml;hren m&ouml;chte und daf&uuml;r ein Abruftoken der Vermittlungsstelle ben&ouml;tigt.</p>
</td>
</tr>
<tr>
<td>
<p>Nachbedingung / Ergebnis</p>
</td>
<td>
<p>Die Notwendigkeit einer abstrakten Berechtigungspr&uuml;fung zum Nachweisabruf ist festgestellt, bei Bedarf ist die abstrakte Berechtigung gepr&uuml;ft, ggf. ist der Nachweisabruf durch ein Token autorisiert und die Pr&uuml;fung ist protokolliert.</p>
</td>
</tr>
<tr>
<td>
<p>Standardablauf</p>
</td>
<td>
<ol>
<li>Die Vermittlungsstelle erh&auml;lt eine Berechtigungsanfrage von einem Sicheren Anschlussknoten eines Data Consumers, d.h. f&uuml;r ein Abruftoken als Berechtigung f&uuml;r einen Nachweisabruf.
<ol>
<li>Sie erh&auml;lt dazu die Angaben zu Kommunikationspartnern (Komponenten-ID jeweils von Data Consumer und Data Provider), Kommunikationszweck (Nachweistyp, ggf. Rechtsgrundlage), Verwendung der IDNr (ja/nein-Angabe) und Hash-Wert des XNachweis-Requests.</li>
</ol>
</li>
<li>Die Vermittlungsstelle ermittelt die den Kommunikationspartnern zugeh&ouml;rigen Verwaltungsbereiche und zul&auml;ssige Kommunikationszwecke zwischen den Kommunikationspartnern.</li>
<li>Die Vermittlungsstelle pr&uuml;ft, ob eine abstrakte Berechtigungspr&uuml;fung notwendig ist.
<ol>
<li>Dazu pr&uuml;ft sie, ob f&uuml;r den Nachweisabruf eine IDNr verwendet wird.</li>
<li>Weiterhin pr&uuml;ft sie, ob die Kommunikationspartner unterschiedlichen Verwaltungsbereichen zugeordnet sind oder, falls sie dem gleichen Verwaltungsbereich zugeordnet sind, ob eine verwaltungsbereichsinterne abstrakte Berechtigungspr&uuml;fung vorgesehen ist.</li>
<li>Sind die beiden Bedingungen 1 und 2 erf&uuml;llt, so stellt die Vermittlungsstelle fest, dass eine abstrakte Berechtigungspr&uuml;fung notwendig ist.</li>
</ol>
</li>
<li>
<p>Die Vermittlungsstelle pr&uuml;ft, ob die beiden Kommunikationspartner zum angegebenen Zweck kommunizieren d&uuml;rfen ("abstrakte Berechtigungspr&uuml;fung").</p>
<ol>
<li>Dazu pr&uuml;ft sie, ob zu den beiden Kommunikationspartnern ein gemeinsamer Kommunikationszweck existiert, der mit dem angegebenen Kommunikationszweck &uuml;bereinstimmt.</li>
</ol>
</li>
<li>
<p>Die Vermittlungsstelle stellt ein Abruftoken aus.</p>
<ol>
<li>In dem Abruftoken speichert sie die Datengrundlage der Berechtigungspr&uuml;fung, d.h. beide Kommunikationspartner, Kommunikationszweck, Verwendung der IDNr und Hash-Wert des Requests.</li>
</ol>
</li>
<li>
<p>Die Vermittlungsstelle siegelt das Token.</p>
</li>
<li>
<p>Die Vermittlungsstelle stellt das Token dem Sicheren Anschlussknoten desData Consumers bereit.</p>
</li>
<li>
<p>Die Vermittlungsstelle protokolliert die Berechtigungsanfrage, d.h. Datengrundlage (inkl. Hash-Wert des Requests und Request-ID), Pr&uuml;fungsergebnis ("Abstrakte Berechtigung liegt vor", "Abstrakte Berechtigung liegt nicht vor" oder "Abstrakte Berechtigung nicht notwendig") und die Metadaten der Anfrage (Zeitstempel).</p>
</li>
</ol>
</td>
</tr>
<tr>
<td>
<p>Alternativer Ablauf 1</p>
</td>
<td>
<p>In Schritt 3 des Standardablaufs ergibt die Pr&uuml;fung auf Notwendigkeit einer abstrakten Berechtigungspr&uuml;fung, dass diese nicht notwendig ist.</p>
<p>Weiter mit Schritt 5 des Standardablaufs.</p>
</td>
</tr>
<tr>
<td>
<p>Alternativer Ablauf 2</p>
</td>
<td>
<p>In Schritt 4 des Standardablaufs ergibt die Pr&uuml;fung, ob die Kommunikationspartner zum angegebenen Zweck kommunizieren d&uuml;rfen (abstrakte Berechtigungspr&uuml;fung), dass kein Berechtigungseintrag vorliegt, der diese Kommunikation zul&auml;sst.</p>
<ol>
<li>Die Vermittlungsstelle stellt fest, dass eine abstrakte Berechtigung nicht vorliegt, sie gibt eine Fehlermeldung an den Sicheren Anschlussknoten des Data Consumers zur&uuml;ck, insbes. stellt sie kein Abruftoken aus.</li>
</ol>
<p>Weiter mit Schritt 8 des Standardablaufs.</p>
</td>
</tr>
</tbody>
</table>

#### 8.2.2 Anwendungsfall 2: Protokolldaten bereitstellen

<p><strong>Tab. 9: Beschreibung des Anwendungsfalls "Protokolldaten bereitstellen"</strong></p>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>
<p>Anwendungsfall ID</p>
</th>
<th>
<p>UC-VS-2</p>
</th>
</tr>
<tr>
<td>
<p>Kurzbeschreibung</p>
</td>
<td>
<p>Die Vermittlungsstelle stellt dem Administrator Protokolldaten zur Durchf&uuml;hrung der Berechtigungspr&uuml;fung oder &Auml;nderung an den Berechtigungen entsprechend seinen Kriterien bereit.</p>
</td>
</tr>
<tr>
<td>
<p>Vorbedingung / ausl&ouml;sendes Ereignis</p>
</td>
<td>
<p>Der Administrator der Vermittlungsstelle startet den Abruf von Protokolldaten.</p>
</td>
</tr>
<tr>
<td>
<p>Nachbedingung / Ergebnis</p>
</td>
<td>
<p>Dem Administrator sind die angeforderten Daten bereitgestellt.</p>
</td>
</tr>
<tr>
<td>
<p>Standardablauf</p>
</td>
<td>
<ol>
<li>Der Administrator der Vermittlungsstelle startet den Abruf von Protokolldaten.</li>
<li>Der Administrator w&auml;hlt aus, ob er die Protokolldaten zur Durchf&uuml;hrung der Berechtigungspr&uuml;fung oder zu &Auml;nderungen an den Berechtigungen abrufen m&ouml;chte.</li>
<li>Der Administrator gibt dazu ggf. Kriterien ein, um den Umfang der abzurufenden Daten einzuschr&auml;nken (z.B. Zeitraum, Pr&uuml;fungsergebnis, Data Consumer, Data Provider, Kommunikationszweck).</li>
<li>Die Vermittlungsstelle ermittelt die Protokolldaten entsprechend den Kriterien.</li>
<li>Die Vermittlungsstelle stellt dem Administrator die Protokolldaten bereit.</li>
</ol>
</td>
</tr>
</tbody>
</table>

#### 8.2.3 Anwendungsfall 3: Abstrakte Berechtigungen aktualisieren

<p><strong>Tab. 10: Beschreibung des Anwendungsfalls "Abstrakte Berechtigungen aktualisieren"</strong></p>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>Anwendungsfall ID</th>
<th>UC-VS-3</button></th>
</tr>
<tr>
<td>Kurzbeschreibung</td>
<td>Die Vermittlungsstelle ruft vom Verzeichnisdienst die Aktualisierungsinformationen zu den abstrakten Berechtigungen ab und aktualisiert ihren Datenbestand.</td>
</tr>
<tr>
<td>Vorbedingung / ausl&ouml;sendes Ereignis</td>
<td>periodisch</td>
</tr>
<tr>
<td>Nachbedingung / Ergebnis</td>
<td>Der Datenbestand der Vermittlungsstelle zu den abstrakten Berechtigungen ist aktualisiert, die &Auml;nderungen sind protokolliert.</td>
</tr>
<tr>
<td>Standardablauf</td>
<td>
<ol>
<li>Die Vermittlungsstelle ruft die vom Verzeichnisdienstangebotene Schnittstelle f&uuml;r die Aktualisierung der Berechtigungsdaten auf.</li>
<li>Die Vermittlungsstelle erh&auml;lt vom Verzeichnisdienst die Aktualisierungsinformationen zu den abstrakten Berechtigungen.</li>
<li>Die Vermittlungsstelle aktualisiert den Datenbestand der abstrakten Berechtigungen mittels der erhaltenen Aktualisierungsinformationen.</li>
<li>Die Vermittlungsstelle protokolliert die durchgef&uuml;hrten &Auml;nderungen.</li>
</ol>
</td>
</tr>
</tbody>
</table>

## 9. Ausblick und Weiterf&uuml;hrende Aspekte

### 9.1 Offene Punkte

Die konzeptionelle Ausgestaltung der Vermittlungsstelle ist fortgeschritten, jedoch in Details andauernd. Dabei werden die im Folgenden beschriebenen offenen Punkte adressiert werden.
<p><strong>Tab. 11: Offene Punkte<br /></strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>
Offener Punkt
</th>
<th>
Bezeichnung
</th>
<th>
Beschreibung

</th>
</tr>
<tr>
<td>
<p>OP-VS-561</p>
</td>
<td>
<p>Verzeichnisdienst bzw. Verzeichnis f&uuml;r Informationen zur abstrakten Berechtigungspr&uuml;fung</p>
</td>
<td>
F&uuml;r den Bezug der Daten zu den abstrakten Berechtigungenerscheint das Fachdatenkonzept (bzw. RegMo-Repository/ Nachweiskatalog) als geeignet. Abstimmungen dazu mit PB Register sind andauernd. Aktuell ist davon auszugehen, dass die abstrakten Berechtigungen dort gepflegt werden und von der Vermittlungsstelle genutzt werden k&ouml;nnen. Offen ist die Einbindung des IAM f&uuml;r Beh&ouml;rden, da insbes. die Daten zu Data Consumern und Data Provider (v.a. die Komponenten-ID, ggf. die Verwaltungsbereichszuordnung) mit den dortigen Daten &uuml;bereinstimmen m&uuml;ssen, damit eine Verwendung f&uuml;r die abstrakte Berechtigungspr&uuml;fung m&ouml;glich ist.
</td>
</tr>
<tr>
<td>
OP-VS-588
</td>
<td>
Gesetzeskonformit&auml;t des Token-Ansatzes
</td>
<td>
<p>Aufgrund der Formulierungen des IDNrG, insbes. von &sect; 7 Abs. 2 S. 1 und S. 4 IDNrG, ist noch offen, ob dieser Architekturentwurf mit dem Gesetzestext vereinbar ist. Erste juristische und datenschutzrechtliche Einsch&auml;tzungen haben zu einem positiven Ergebnis gef&uuml;hrt. Die Konzeption erfolgt vorbehaltlich einer abschlie&szlig;enden Bewertung.</p>
</td>
</tr>
</tbody>
</table>

## 10. &Auml;nderungsdokumentation

<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th></th>
<th>Release-Versionen</th>
<th>Kapitel</th>
<th>Beschreibung der &Auml;nderung</th>
<th>Art der &Auml;nderung</th>
<th>Priorisierung/ major changes</th>
<th>Quelle der &Auml;nderung</th>
</tr>
<tr>
<td>1</td>
<td>Q1/2024</td>
<td>Fachliches Konzept</td>
<td>
<ul>
<li>Beschreibung des Ansatzes der Einbindung der Vermittlungsstelle &uuml;ber Sichere Anschlussknoten</li>
<li>Anpassung der Anwendungsf&auml;lle zur Vermittlungsstelle</li>
<li>&Uuml;berarbeitung des Datenmodells</li>
<li>Grundlegende &Uuml;berarbeitung des Prozess&uuml;berblicks</li>
<li>Aktualisierung der Beschreibung der beteiligten Systeme</li>
<li>Grundlegende &Uuml;berarbeitung von Anwendungsfall 1</li>
</ul>
</td>
<td>Neuer Inhalt, Aktualisierung</td>
<td>⭐</td>
<td>Komponententeam, NOOTS Board, Abstimmung mit PB Recht sowie mit Datenschutzbeauftragten, Zusammenarbeit mit Komponententeam Transportinfrastruktur</td>
</tr>
<tr>
<td>2</td>
<td>Q1/2024</td>
<td>&Uuml;berblick</td>
<td>
<ul>
<li>Aktualisierung der Zielsetzung der Vermittlungsstelle, Erg&auml;nzungen zur Mitwirkungsnotwendigkeit von Data Consumer oder Data Provider, Ausarbeitung zur Integration in die Transportinfrastruktur</li>
</ul>
</td>
<td>neuer Inhalt, Aktualisierung</td>
<td>⭐</td>
<td>Komponententeam, Abstimmung mit PB Recht sowie mit Datenschutzbeauftragten</td>
</tr>
<tr>
<td>3</td>
<td>Q1/2024</td>
<td>Kontext</td>
<td>
<ul>
<li>Beschreibung der technischen Problemstellung, die sich aus den rechtlichen und Datenschutzvorgaben ergeben</li>
</ul>
</td>
<td>neuer Inhalt</td>
<td>⭐</td>
<td>Komponententeam</td>
</tr>
<tr>
<td>4</td>
<td>Q1/2024</td>
<td>Anforderungen und Rahmenbedingungen</td>
<td>
<ul>
<li>Aktualisierung an den aktuellen Konzeptionsstand</li>
</ul>
</td>
<td>Aktualisierung, Korrektur</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>5</td>
<td>Q1/2024</td>
<td>Architekturentscheidungen</td>
<td>
<ul>
<li>Dokumentation der Architekturentscheidungen aus dem NOOTS Board zur Vermittlungsstelle</li>
</ul>
</td>
<td>neuer Inhalt</td>
<td>⭐</td>
<td>NOOTS Board</td>
</tr>
<tr>
<td>6</td>
<td>Q1/2024</td>
<td>Gepr&uuml;fte L&ouml;sungsentw&uuml;rfe und Hintergrund zur Architektur</td>
<td>
<ul>
<li>Beschreibung gepr&uuml;fter und verworfener L&ouml;sungsans&auml;tze</li>
<li>Beschreibung der Motivation f&uuml;r den gew&auml;hlten L&ouml;sungsansatz</li>
</ul>
</td>
<td>Aktualisierung, neuer Inhalt</td>
<td></td>
<td>Komponententeam, NOOTS Board, Abstimmung mit PB Recht sowie mit Datenschutzbeauftragten</td>
</tr>
<tr>
<td>7</td>
<td>Q1/2024</td>
<td>Offene Punkte</td>
<td>
<ul>
<li>Aktualisierung zum Bearbeitungsstatus</li>
</ul>
</td>
<td>Aktualisierung</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>8</td>
<td>Q2/2024</td>
<td>Gesamtes Dokument</td>
<td>
<ul>
<li>&Uuml;berarbeitung der Dokumentenstruktur</li>
</ul>
</td>
<td>Aktualisierung</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>9</td>
<td>Q2/2024</td>
<td>Laufzeitsicht</td>
<td>
<ul>
<li>Grundlegende &Uuml;berarbeitung und Erg&auml;nzung zur &uuml;bergreifenden Einbindung der Vermittlungsstelle in den Nachweisabrufprozess</li>
<li>&Uuml;berarbeitung "Berechtigung pr&uuml;fen"</li>
<li>Erstellung "Protokolldaten bereitstellen" und "Abstrakte Berechtigungen aktualisieren"</li>
</ul>
</td>
<td>Aktualisierung, neuer Inhalt</td>
<td>⭐</td>
<td>Komponententeam, NOOTS-Board</td>
</tr>
<tr>
<td>10</td>
<td>Q2/2024</td>
<td>Bausteinsicht</td>
<td>
<ul>
<li>Erweiterung des Datenmodells</li>
<li>Beschreibung des Aufbaus des Abruftokens und der Schnittstellen</li>
</ul>
</td>
<td>Aktualisierung, neuer Inhalt</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>11</td>
<td>Q2/2024</td>
<td>Fachlicher Kontext</td>
<td>
<ul>
<li>Aktualisierung der Beschreibung des fachlichen Kontext der Vermittlungsstelle</li>
<li>Erstellung Diagramm zur Visualisierung</li>
</ul>
</td>
<td>Aktualisierung, neuer Inhalt</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>12</td>
<td>Q2/2024</td>
<td>Randbedingungen</td>
<td>
<ul>
<li>Beschreibung zum 4-Corner-Modell</li>
<li>Beschreibung der Protokollierung</li>
<li>Abgrenzung zum IAM f&uuml;r Beh&ouml;rden</li>
<li>Beschreibung des Verzeichnisdienstes und Zusammenhang Fachdatenkonzept</li>
</ul>
</td>
<td>Neuer Inhalt</td>
<td>⭐</td>
<td>Komponententeam, NOOTS-Board, PB Recht und Datenschutzbeauftragte, PB Register</td>
</tr>
<tr>
<td>13</td>
<td>Q2/2024</td>
<td>Anforderungen</td>
<td>
<ul>
<li>&Uuml;berarbeitung der Anforderungen gem. den neuen Erkenntnissen</li>
</ul>
</td>
<td>Aktualisierung, neuer Inhalt</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>14</td>
<td>Q2/2024</td>
<td>Begriffsdefinitionen</td>
<td>
<ul>
<li>Aufnahme von Erkl&auml;rungen zu relevanten Begriffen im Kontext der Vermittlungsstelle</li>
</ul>
</td>
<td>Neuer Inhalt</td>
<td></td>
<td>Komponententeam</td>
</tr>
</tbody>
</table>