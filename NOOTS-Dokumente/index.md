## Herzlich Willkommen zum Konsultationsprozess zu ausgewählten Architekturdokumenten zum National-Once-Only-Technical-System (NOOTS)
 
### Ausgangslage und Zielsetzung


> Es handelt sich um einen Konsultationsprozess zu ausgewählten Architekturdokumenten zum National-Once-Only-Technical-System (NOOTS), der federführend durch das Bundesministerium des Innern und für Heimat (BMI) geleitet wird. Der Architektur- und Entwicklungsprozess soll transparent und partizipativ von Ihnen mitgestaltet werden. Diese Plattform gibt benannten Stakeholdern mit Bezug zur Registermodernisierung, Unternehmen, Expertinnen und Experten aus der Wissenschaft, Wirtschaft und Verwaltung die Möglichkeit, ihre Expertise und Erfahrungen einzubringen. Durch die frühzeitige Einbindung sollen wichtige Erkenntnisse für zukünftige Architekturen und für künftige Konsultations- und Beteiligungsprozesse gesammelt werden.   

### Einladung Informationstermin zweite Iteration Konsultationsprozess der AD-NOOTS am 30.10.2024, 15.00 Uhr

Sehr geehrte Damen und Herren,
vielen Dank für Ihre rege Beteiligung am Konsultationsprozess zu den AD-NOOTS.
Um die zweite Iteration zu den Architekturdokumenten des NOOTS abzuschließen, wird das Bundesministerium des Innern und für Heimat (BMI) am 30.10.2024 um 15:00 Uhr erneut eine Online-Informationsveranstaltung durchführen. Hierzu möchten wir Sie herzlich einladen.
Wir möchten allen Konsultantinnen und Konsultanten aus der Zivilgesellschaft, Wirtschaft und Verwaltung einen informativen Überblick über den bisher gelaufenen Prozess, das eingegangene Feedback sowie einen Ausblick auf das weitere Vorgehen geben. Als zusätzlichen Schwerpunkt der Veranstaltung werden wir den Anschluss an das NOOTS (Sicherer Anschlussknoten - Transportinfrastruktur) noch einmal vorstellen. 
Das Feedback aller Konsultantinnen und Konsultanten zu den Leitfragen sowie alle veröffentlichen Dokumente können Sie sich gerne nochmal unter https://gitlab.opencode.de/bmi/noots <https://gitlab.opencode.de/bmi/noots>  aufrufen.

Beitrittsinformationen
Mittwoch, 30. Oktober 2024     

15:00  |  (UTC+02:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien  |  1 Stunde 30 Minuten                         

Meeting beitreten <https://bva-bund.webex.com/bva-bund/j.php?MTID=m81fc91585f10543a34b346bd2eb0fdcb>                

Weitere Methoden zum Beitreten:                     

Über den Meeting-Link beitreten           

https://bva-bund.webex.com/bva-bund/j.php?MTID=m81fc91585f10543a34b346bd2eb0fdcb            

Mit Meeting-Kennnummer beitreten    

Meeting-Kennnummer (Zugriffscode): 2734 371 8287   

Meeting Passwort: mAJ3Ez*$745 (62533900 beim Einwählen von einem Telefon)          

               

Hier tippen, um mit Mobilgerät beizutreten (nur für Teilnehmer)            

+49-619-6781-9736,,27343718287#62533900# <tel:%2B49-619-6781-9736,,*01*27343718287%2362533900%23*01*>  Germany Toll

Auf manchen Mobilgeräten müssen die Teilnehmer ein numerisches Passwort eingeben.               

Über Telefon beitreten

+49-619-6781-9736 Germany Toll           

Globale Einwahlnummern <https://bva-bund.webex.com/bva-bund/globalcallin.php?MTID=m78acf054b419c92f401764370ffadb70>                          

Über Videogerät oder -anwendung beitreten   

Wählen Sie 27343718287@bva-bund.webex.com <sip:27343718287@bva-bund.webex.com>

Sie können auch 62.109.219.4 wählen und Ihre Meeting-Nummer eingeben.





Für Rückfragen wenden Sie sich bitte an registermodernisierung@bva.bund.de <mailto:registermodernisierung@bva.bund.de>

### Nächste Iteration startet im Juli 2024


Sehr geehrte Damen und Herren,

vielen Dank für Ihre rege Beteiligung am Konsultationsprozess zur Architekturdokumentation des NOOTS (AD-NOOTS) in der ersten Iteration. Der Konsultationsprozess ist ein fortlaufender Prozess, mit dem Ziel schnellstmöglich alle aktuellen Arbeitsstände zu teilen. Ihr Feedback und Ihre Impulse helfen uns die Architekturdokumente kontinuierlich zu verbessern.

Im Zuge der zweiten Iteration des Konsultationsprozesses werden in der KW 29 drei weitere Architekturdokumente unter <a href="https://gitlab.opencode.de/bmi/noots">https://gitlab.opencode.de/bmi/noots"</a> veröffentlicht, welche von Ihnen als Konsultantinnen und Konsultanten mithilfe von Leitfragen kommentiert werden sollen. Bei den drei Dokumenten handelt es sich um:

- Grobkonzept Vermittlungsstelle (VS)
- Grobkonzept IAM für Behörden
- Grobkonzept Transportinfrastruktur (SAK)

Die Dokumente stehen anders als in der 1.Iteration diesmal für einen längeren Zeitraum bis zum 04.09.2024 zur Kommentierung zur Verfügung.

Begleitend zu der Kommentierungsphase lädt das Bundesministerium des Innern und für Heimat (BMI) Sie am **24.07.2024 **in der Zeit von **15:00 - 16:30 Uhr** ein, an einer Informationsveranstaltung zur zweiten Iteration teilzunehmen. Wir werden Ihnen in der Veranstaltung einen Überblick über die drei Dokumente verschaffen sowie weitere Informationen rund um den Konsultationsprozess geben. 


Wir freuen uns auf Ihre Teilnahme!

**Beitrittsinformationen**

Informationstermin Konsultationsprozess AD NOOTS am 24.07.2024

<a href="https://bva-bund.webex.com/bva-bund/j.php?MTID=mc40c42469766f4b2aa59c82a7f11b0f1">
https://bva-bund.webex.com/bva-bund/j.php?MTID=mc40c42469766f4b2aa59c82a7f11b0f1</a>

Mittwoch, 24. Juli 2024 15:00 | 1 Stunde 30 Minuten | (UTC+02:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien

Meeting-Kennnummer: 2786 557 0181

Passwort: uHq95Vy*@86 (84795890 beim Einwählen von einem Telefon)

Über Videosystem beitreten
Wählen Sie 27865570181@bva-bund.webex.com

Sie können auch 62.109.219.4 wählen und Ihre Meeting-Nummer eingeben.

Über Telefon beitreten
+49-619-6781-9736 Germany Toll

Zugriffscode: 278 655 70181

**Bearbeitung der Leitfragen**

Um eine Bearbeitung der Leitfragen vorzunehmen, folgen Sie bitte vorab der Anmeldungsanleitung im nachstehenden Kurzvideo.
Weiterhin möchten wir Sie darauf aufmerksam machen, dass wir Ihr Feedback als wertvolle Ergänzung betrachten und dass wir dieses ausgiebig prüfen werden. Dennoch können wir nicht garantieren, dass jeder Vorschlag umgesetzt werden kann.

Sollten weitere Fragen zum Konsultationsprozess bestehen, kontaktieren Sie bitte **registermodernisierung@bva.bund.de**

### Rückblick

Die erste Iteration des Konsultationsprozesses zu ausgewählten Architekturdokumenten zum National-Once-Only-Technical-System ist erfolgreich beendet. Es wurden fünf Architekturdokumente veröffentlicht:

- High-Level-Architecture (HLA)
- Anschlusskonzept Data Consumer (DC)
- Anschlusskonzept Data Provider (DP)
- Grobkonzept Intermediäre Plattform (IP)
- Grobkonzept Registerdatennavigation (RDN)

Mittels auf dieser Plattform liegenden Leitfragen konnte entsprechendes Feedback zu den Dokumenten abgegeben werden. Die Konsultantinnen und Konsultanten gaben rund 220 Kommentare ab. Dabei wurden verschiedene Aspekte angesprochen, darunter der Abstraktionsgrad der Dokumente, sowie Fragen zum Verständnis und zu Details. In einem anschließenden Review unserer Expertinnen und Experten wird basierend auf Ihrer Rückmeldung iterativ die Weiterentwicklung der Dokumente vorgenommen.

Wir freuen uns, dass Sie sich beteiligen und damit Ihre Perspektive einbringen.
