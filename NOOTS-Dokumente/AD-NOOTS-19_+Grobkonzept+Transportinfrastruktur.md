>**Redaktioneller Hinweis**
>
>Dokument aus der zweiten Iteration - nicht Teil der aktuellen Iteration.
Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## 1. Abstract

Mit der Registermodernisierung entsteht zum ersten Mal ein die gesamte Verwaltung &uuml;berspannender Informationsverbund. F&uuml;r den sicheren Nachrichtenaustausch in diesem Verbund wird eine Transportinfrastruktur ben&ouml;tigt, welche nachvollziehbare und Ende-zu-Ende sichere Kommunikation &uuml;ber bestehende Verwaltungsnetze (Netze des Bundes (NdB), Landesnetze, Kommunalnetze) hinweg erm&ouml;glicht. Die in Deutschland bestehende <a href="#anhang-a---bestehende-transportinfrastrukturen">OSCI Infrastruktur</a> hat sich nur in Teilen der Verwaltung durchgesetzt. Europ&auml;ische Infrastrukturen wie <a href="#edelivery-as4">eDelivery AS4</a> oder <a href="#x-road">X-Road</a> erf&uuml;llen die Anforderungen nicht oder sind nichtkompatibel zur bestehenden Infrastruktur, bspw. den Verwaltungsnetzen oder dem DVDV. Das NOOTS verfolgt daher eine Entkopplungsstrategie: Anstatt sich auf eine Transportinfrastruktur festzulegen, wird der Transport &uuml;ber Sichere Anschlussknoten (SAK) entkoppelt.
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/1.png" alt="" />
<p><strong>Abb. 1: Entkopplung vom Transport durch Sichere Anschlussknoten (SAK)</strong></p>
<p>Die von den Security Servern aus <a href="#x-road">X-Road</a> inspirierten Sichere Anschlussknoten sollen den Anschluss an die Transportinfrastruktur f&uuml;r dieTeilnehmer einfach und sicher gestalten, bspw. indem sie die Verschl&uuml;sselung und die Verwaltung der Zertifikate &uuml;bernehmen. Gleichzeitig entkoppeln sie die Teilnehmer vom Rest der Transportinfrastruktur im Sinne einer Fassade. So k&ouml;nnen mehrere Transportinfrastrukturen zeitgleich unterst&uuml;tzt werden. Zudem wird eine sanfte Migration auf zuk&uuml;nftige Transportinfrastrukturen, bspw. auf Grundlage eines europaweiten Transportstandards, unterst&uuml;tzt.</p>

## 2. Einf&uuml;hrung und Ziele

### 2.1 &Uuml;berblick

Die Teilnehmer des NOOTS kommen aus unterschiedlicher Fachlichkeit und Verwaltungsebenen. Beispielsweise werden f&uuml;r einen Kindergeldantrag (Arbeit &amp; Soziales, Bund) unter anderem Geburtsurkunden der Kinder (Personenstand, Kommunen) ben&ouml;tigt. Dabei sind Teilnehmer aus dem Bund in den Netzen des Bundes (NdB) angesiedelt, Teilnehmer aus den L&auml;ndern in Landesnetzen oder Kommunalnetzen des jeweiligen Landes. Diese Netze sind zum Teil direkt, zum Teil &uuml;ber das Internet und zum Teil &uuml;ber das Verbindungsnetz (NdB-VN) miteinander gekoppelt. Die Kopplung erlaubt allerdings keine freie Kommunikation &uuml;ber Netzgrenzen hinweg.
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/2.png" alt="" />
<p><strong>Abb. 2: Ausgangslage schematischer Aufbau der Verwaltungsnetze und Anschl&uuml;sse Data Provider und Data Consumer</strong></p>
<p>Die Abbildung zeigt den schematischen Aufbau der Netze. Die schraffierten Bl&ouml;cke stellen dabei Zugangspunkte zu oder &Uuml;bergangspunkte zwischen Weitbereichsnetzen dar. Jeder &Uuml;bergangspunkt beinhaltet typischerweise eine Firewall, die den zul&auml;ssigen Datenverkehr &uuml;berwacht, und eine Adressumsetzung, die die IPv4 Adressbereiche der Netze f&uuml;r Netz&uuml;bergreifende Verbindungen aufeinander abbildet. Um eine Kommunikation zwischen Teilnehmern verschiedener Netze zu erm&ouml;glichen, m&uuml;ssen sowohl Firewall als auch Adressabbildungen geeignet konfiguriert werden, ggfs. konsistent &uuml;ber mehrere &Uuml;bergangspunkte hinweg. Das dazu erforderliche Verfahren ist wenig transparent, fehlertr&auml;chtig und mit einer Wartezeit von Wochen bis Monaten verbunden. F&uuml;r kleine Informationsverb&uuml;nde ist dieser Weg gangbar, in der Registermodernisierung ist jedoch mit einer sehr gro&szlig;en Anzahl von Einzelverbindungen zu rechnen (obere Absch&auml;tzung <a href="#51-funktionale-anforderungen">1.000.000</a>). Daher wird eine Transportinfrastruktur ben&ouml;tigt, die eine Verwaltungsnetze &uuml;bergreifende Kommunikation ohne Einzelfreischaltung f&uuml;r jede Verbindung erm&ouml;glicht.</p>
<p>Ein &auml;hnliches Problem ergibt sich beim technischen Zugang der Data Consumer und Data Provider zu den Weitbereichsnetzen. Diese werden in der Regel in abgeschotteten Netzsegmenten betrieben, die aus den Verwaltungsnetzen nicht erreichbar sind. Auch hier werden einzelne Verbindungen &uuml;ber die Infrastruktur freigeschaltet, &uuml;ber die bspw. auch heute schon auf Register zugegriffen werden kann. F&uuml;r eine viel st&auml;rker vernetzte, digitalisierte Verwaltung wird eine Transportinfrastruktur ben&ouml;tigt, die eine gro&szlig;e Anzahl von Verbindungen erm&ouml;glich, ohne dass jede einzelne Verbindung von oder zu einem Nahbereichsnetz freigeschaltet werden muss.</p>

### 2.2 Ziele der Komponente

In diesem Dokument wird die Transportinfrastruktur des NOOTS vorgestellt. Diese soll eine einfache und sichere Kommunikation zwischen den Teilnehmern &uuml;ber die Netze von Bund und L&auml;ndern erm&ouml;glichen. Ein besonderer Fokus liegt auf der Unterst&uuml;tzung von NOOTS-Anwendungsf&auml;llen mit interaktiven Nachweisabrufen, die im Dialog mit einer nat&uuml;rlichen Person innerhalb kurzer Zeit ausgef&uuml;hrt werden m&uuml;ssen, wie es auch vom Europ&auml;ischen Once-Only Technical System (EU-OOTS) gefordert wird. Dar&uuml;ber hinaus soll die Transportinfrastruktur Qualit&auml;tsmerkmale bieten, die die Netze allein nicht gew&auml;hrleisten k&ouml;nnen. Dabei sind in Zusammenarbeit mit den anderen NOOTS-Komponenten und Teilnehmern die Gew&auml;hrleistungsziele gem&auml;&szlig; Standard-Datenschutzmodell (<a href="https://www.bfdi.bund.de/DE/Fachthemen/Inhalte/Technik/SDM.html">SQ-13</a>) wie Verf&uuml;gbarkeit, Vertraulichkeit, Integrit&auml;t, Transparenz, Nichtverkettung und Intervenierbarkeit relevant.
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/3.png" alt="" /></p>
<p><strong>Abb. 3: Zusammenhang Transportinfrastruktur und Netze</strong></p>
<p>Die folgenden Ausf&uuml;hrungen zur Transportinfrastruktur in diesem Dokument sind wie folgt strukturiert:</p>
<ul>
<li>Im Kapitel "<em>Randbedingungen</em>" werden die technischen, rechtlichen und organisatorischen Rahmenbedingungen betrachtet, wie beispielsweise Annahmen und Abgrenzungen.</li>
<li>Das Kapitel "<em>Kontextabgrenzung</em>" beschreibt das fachliche und technische Umfeld der Transportinfrastruktur, einschlie&szlig;lich der Interaktion mit NOOTS-Komponenten und NOOTS-Teilnehmern &uuml;ber Schnittstellen.</li>
<li>Im Kapitel "<em>Anforderungen</em>" werden wesentliche funktionale und nicht-funktionale Anforderungen f&uuml;r die NOOTS-Transportinfrastruktur mit Erl&auml;uterungen aufgelistet.</li>
<li>Im Kapitel "<em>L&ouml;sungsstrategie</em>" werden die wichtigsten Designentscheidungen f&uuml;r die NOOTS-Transportinfrastruktur zusammengefasst.</li>
<li>Das Kapitel "<em>Bausteinsicht</em>" leitet ein allgemeines Architekturmodell basierend auf einer Analyse bestehender Transportinfrastrukturen in Deutschland und im europ&auml;ischen Ausland ab. Verschiedene Ans&auml;tze werden verglichen und gegen&uuml;bergestellt, woraus die Bausteine "NOOTS-Sichere Anschlussknoten", "NOOTS-Anschlussprotokoll" und "NOOTS-Transportprotokoll" entwickelt und im Detail vorgestellt werden.</li>
<li>Die "<em>Laufzeitsicht</em>" beschreibt den Ablauf eines vom Data Consumer initiierten Nachweisabrufs f&uuml;r den HLA-Anwendungsfall "1a: Interaktiver Nachweisabruf zu einer nat&uuml;rlichen Person &uuml;ber das NOOTS" (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>) im Detail.</li>
<li>Im Kapitel "<em>Ausblick und Weiterf&uuml;hrende Aspekte</em>" wird ein Ausblick auf Themen gegeben, die in zuk&uuml;nftigen Releases dieses Dokuments behandelt werden sollen.</li>
<li>
Im Anhang "<em>A - Bestehende Transportinfrastrukturen</em>" werden bestehende Transportinfrastrukturen in Deutschland und im europ&auml;ischen Ausland betrachtet.
</li>
<li>Der Anhang &bdquo;<em>B - Nachrichtenverfolgung mit OpenTelemetry</em>&ldquo; f&uuml;hrt in den Open-Source-Standard OpenTelemetry f&uuml;r die verteilte Nachrichtenverfolgung ein, ein Projekt der (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads">Cloud Native Computing Foundation (CNCF)</a>.</li>
</ul>

### 2.3 Begriffsdefinitionen

In diesem Kapitel werden Begriffe und ihre Bedeutungen aufgelistet, die im Rahmen dieses Konzeptes von Bedeutung sind und verwendet werden. F&uuml;r allgemeine Begriffe der Registermodernisierung wird auf das zentrale Glossar Gesamtsteuerung Registermodernisierung (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads">SQ-14</a>) verwiesen.
<p><strong>Tab. 1: Begriffsdefinitionen</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Begriff</th>
<th>Bedeutung</th>
</tr>
<tr>
<td>Abstrakte Berechtigung</td>
<td>
<p>Eine abstrakte Berechtigung stellt die generelle Erlaubnis dar, dass zwei Kommunikationspartner zu einem bestimmten Kommunikationszweck miteinander kommunizieren d&uuml;rfen, d.h. ob ein Data Consumer berechtigt ist, von einem Data Provider bestimmte Daten zu einem bestimmten Zweck abzurufen.</p>
<p>Abstrakt bedeutet hier, dass eine Pr&uuml;fung ohne Wissen &uuml;ber den Nachrichteninhalt und die antragstellende Person erfolgt. Es handelt sich nicht um eine Einzelfall-Pr&uuml;fung, konkrete Berechtigungen werden nicht gepr&uuml;ft, bspw. ob der Datensatz f&uuml;r eine bestimmte Person abgerufen werden darf.</p>
</td>
</tr>
<tr>
<td>Deutsche Verwaltungscloud-Strategie (DVC)</td>
<td>
<p>Die Deutsche Verwaltungscloud-Strategie (DVC) ist eine Initiative der Bundesregierung, die darauf abzielt, durch den Einsatz von Cloud-Services flexiblere, kosteneffizientere und sicherere IT-L&ouml;sungen f&uuml;r die &ouml;ffentliche Verwaltung bereitzustellen. Ein besonderer Fokus der DVC liegt auf der Sicherheit, dem Datenschutz und der Interoperabilit&auml;t der genutzten Multi-Cloud-L&ouml;sungen (Private, Souver&auml;ne oder Public Clouds).</p>
</td>
</tr>
<tr>
<td>Hash</td>
<td>
<p>Ein Hash ist eine kryptografische Funktion, die eine Nachricht in eine feste L&auml;nge von Zeichen, dem Hash-Wert, umwandelt. Diese Funktion kann verwendet werden, um die Integrit&auml;t einer Nachricht zu &uuml;berpr&uuml;fen, da selbst geringf&uuml;gige &Auml;nderungen an der Nachricht zu einem v&ouml;llig anderen Hash-Wert f&uuml;hren.</p>
</td>
</tr>
<tr>
<td>Kommunikationszweck</td>
<td>
Der Kommunikationszweck ist ein Pr&uuml;fkriterium im Rahmen der abstrakten Berechtigungspr&uuml;fung. Wesentlich f&uuml;r den Kommunikationszweck sind Art und Umfang der zu &uuml;bertragenden Daten. Der Kommunikationszweck wird abgebildet durch den Nachweistyp. Falls notwendig wird er durch die Angabe, der dem Abruf zugrundeliegenden Rechtsnorm erg&auml;nzt.
</td>
</tr>
<tr>
<td>Erweitertes OSI-Schichtenmodell</td>
<td>
Das ISO/OSI-Referenzmodell (Open Systems Interconnection Model) definiert sieben Schichten mit klar abgegrenzten Funktionen und Schnittstellen, die durch verschiedene Netzwerkprotokolle implementiert werden. Transportprotokolle wie <a href="#anhang-a---bestehende-transportinfrastrukturen">OSCI/XTA2</a>, eDelivery AS4 (EU-EDM-02), <a href="#x-road">X-Road</a> oder die NOOTS-Spezifikation XNachweis (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">XS-01</a>) setzen auf der Anwendungsschicht 7 auf. Das OSI-Schichtenmodell wurde deshalb um die informelle Schicht 8, der "Transport auf Anwendungsebene", erweitert (<a href="https://www.it-planungsrat.de/beschluss/beschluss-2023-38">IT-PLR-B-10</a>).
<p><strong>siehe Abbildung A unten</strong></p>
</td>
</tr>
<tr>
<td>Nachrichtenaustauschmuster</td>
<td>
Nachrichtenaustauschmuster, auch bekannt als Message-Exchange-Pattern (MEP), definieren ein Muster f&uuml;r den Austausch von Nachrichten zwischen verschiedenen Systemen oder Komponenten. In der High-Level-Architektur (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>) werden im Kapitel "Nachrichtenaustauschmuster" synchrone und asynchrone Request-Response-Muster f&uuml;r NOOTS beschrieben.
</td>
</tr>
<tr>
<td>Nachweisangebot</td>
<td>
Ein Nachweisangebot enth&auml;lt Informationen f&uuml;r den Abruf eines Nachweises von einem Data Provider, wie etwa den Nachweistyp, das erforderliche Vertrauensniveau, die Nachweisformate, die Komponenten-ID des Data Providers und die ServiceID des NOOTS-Dienstes. Das Nachweisangebot kann &uuml;ber eine API der NOOTS-Komponente Registerdatennavigation abgerufen werden.
</td>
</tr>
<tr>
<td>
NOOTS-Dienst
</td>
<td>
Ein NOOTS-Dienst beschreibt einen Verbindungspunkt im NOOTS, &uuml;ber den jeweils spezifischen Diensten im NOOTS mittels ServiceID identifiziert und &uuml;ber die Verbindungsparameter angesprochen werden k&ouml;nnen.
</td>
</tr>
<tr>
<td>Sicherer Anschlussknoten (SAK)</td>
<td>
Ein Sicherer Anschlussknoten ist eine zentral bereitgestellte NOOTS-Komponente, die jedoch dezentral im Verantwortungsbereich der NOOTS-Teilnehmer, Data Consumer und Data Provider oder einer anderen NOOTS-Komponente betrieben wird. Er erm&ouml;glicht den NOOTS-Teilnehmern und -Komponenten &uuml;ber ein interoperables Anschlussprotokoll einen einfachen und sicheren Anschluss an das NOOTS sowie den Versand und Empfang von Nachrichten &uuml;ber die NOOTS-Transportinfrastruktur. Da der Sichere Anschlussknoten im Einflussbereich der NOOTS-Teilnehmer und -Komponenten betrieben wird, hat er Zugriff auf unverschl&uuml;sselte Nachrichteninhalte und kann Sicherheitsfunktionen &uuml;bernehmen.
</td>
</tr>
<tr>
<td>Token</td>
<td>
Ein Token ist ein Datensatz, also eine Sammlung von Informationen, der durch den Herausgeber des Tokens gesiegelt wird und dessen Echtheit so f&uuml;r Dritte &uuml;berpr&uuml;fbar ist. Somit k&ouml;nnen mittels eines Tokens Daten zuverl&auml;ssig &uuml;bertragen werden und eine Manipulation ausgeschlossen werden.
<p>Im NOOTS kommen zwei Tokens gem&auml;&szlig; <a href="https://datatracker.ietf.org/doc/html/rfc9068">RFC 9068</a> <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots"> (SQ-24</a>) zum Einsatz: das Zugriffstoken des IAM f&uuml;r Beh&ouml;rden und das Abruftoken der Vermittlungsstelle.</p>
<ul>
<li>(IAM-) Zugriffstoken: Vor jedem Nachweisabruf muss der Data Consumer sich beim IAM f&uuml;r Beh&ouml;rden am NOOTS anmelden. Daraufhin erh&auml;lt er vom IAM f&uuml;r Beh&ouml;rden ein Zugriffstoken, das es ihm erm&ouml;glicht, auf NOOTS-Ressourcen zuzugreifen. Im Zugriffstoken ist u.a. die Komponenten-ID des Data Consumers hinterlegt.</li>
<li>(VS-) Abruftoken: Vor dem Abruf des Nachweises beim Data Provider ist das Einholen der Abrufberechtigung (und ggf. die Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung) notwendig. Dazu sendet der Sichere Anschlussknoten des Data Consumers die notwendigen Angaben an die Vermittlungsstelle. Bei positivem Ergebnis stellt die Vermittlungsstelle das Abruftoken als Nachweis der Berechtigung f&uuml;r den Nachweisabruf aus. Es berechtigt den Data Consumer zu diesem Nachweisabruf.</li>
</ul>
</td>
</tr>
<tr>
<td>
Transport Layer Security (TLS)
</td>
<td>
Transport Layer Security (TLS) ist ein kryptografisches Protokoll, das den Datenschutz und die Datenintegrit&auml;t bei der &Uuml;bertragung von Daten &uuml;ber ein Netzwerk auf OSI-Schicht 5: Sitzungsschicht sicherstellt. Mutual TLS (mTLS) ist ein Bestandteil von TLS und eine Methode zur gegenseitigen Authentifizierung. Sie gew&auml;hrleistet, dass die Parteien an beiden Enden einer Netzwerkverbindung tats&auml;chlich diejenigen sind, die sie vorgeben zu sein. mTLS wird oft im Zero-Trust-Kontext angewendet, um die Authentifizierung von Systemen w&auml;hrend der HTTP-Kommunikation zu erm&ouml;glichen und APIs zu sch&uuml;tzen.
</td>
</tr>
<tr>
<td>
Verwaltungsbereich
</td>
<td>&sect; 7 Abs. 2 IDNrG sieht die Unterteilung der Gesamtheit der Verwaltung in einzelne Verwaltungsbereiche vor. Diese Bereiche sind so abzugrenzen, dass die verschiedenen Lebensbereiche einer Person unterschiedlichen Bereichen zugeordnet werden k&ouml;nnen (z.B. Inneres, Justiz, Wirtschaft und Finanzen, Arbeit und Soziales, Gesundheit, Statistik). F&uuml;r eine hinreichende Differenzierung soll es mindestens sechs Verwaltungsbereiche geben. Zahl und Abgrenzung der Bereiche ist gem. &sect; 12 Abs. 1 IDNrG durch Rechtsverordnung der Bundesregierung festzulegen.</td>
</tr>
<tr>
<td>
<p>Zero Trust Prinzip</p>
</td>
<td>Das Zero-Trust-Prinzip ist ein Sicherheitsansatz, der weder internen noch externen Netzwerken vertraut. Nach dem Motto "niemals vertrauen, immer verifizieren" erfordert es kontinuierliche Authentifizierung und Autorisierung aller Nutzer und Systeme. Jede Zugriffsanfrage wird &uuml;berpr&uuml;ft, um sicherzustellen, dass nur autorisierte Nutzer und Systeme Zugang zu den Ressourcen erhalten.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
Abbildung A
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/4.png" alt=""></p>

## 3. Randbedingungen

### 3.1 Technische / Organisatorische / Rechtliche Randbedingungen

#### 3.1.1 Verwaltungsnetze und Verbindungsnetz

Bund und L&auml;nder betreiben jeweils eigene Netze, &uuml;ber die ihre Verwaltungseinheiten sicher untereinander kommunizieren k&ouml;nnen. Dar&uuml;ber hinaus erm&ouml;glicht das Verbindungsnetz (NdB-VN) den Datenaustausch von &ouml;ffentlichen Stellen &uuml;ber den so gestalteten Verbund der Verwaltungsnetze von Bund, L&auml;ndern und Kommunen. Stellen &ouml;ffentlichen Rechts, wie beispielsweise Beh&ouml;rden, sowie beliehene Dritte und beauftragte Dienstleister von Bund, L&auml;ndern und Kommunen k&ouml;nnen sich direkt an das Verbindungsnetz anschlie&szlig;en oder bestehende Anschlusspunkte ihrer Verwaltungsnetze nutzen. Die Netzinfrastruktur dient zum einen dem Schutz vor Cyberangriffen aus dem Internet, zum anderen gew&auml;hrleistet sie die Souver&auml;nit&auml;t der Beh&ouml;rden-IT.

<p>Das Verbindungsnetz bietet initial das Vertrauensniveau "normal" gem&auml;&szlig; TR-3107-1, abgesichert durch die vom IT-Planungsrat beschlossenen Anschlussbedingungen und die Technischen Richtlinien des BSI. Es lassen sich zudem weitere geschlossene Netze mit h&ouml;herem Vertrauensniveau durch Erg&auml;nzung der Anschlussbedingungen abbilden. Eine durchg&auml;ngige Verschl&uuml;sselung ist im Verbindungsnetz zwischen den Teilnehmern gegeben. Auch IT-Systeme, die in den Verwaltungsnetzen betrieben werden, und deren Kommunikationsstrecken m&uuml;ssen abgesichert werden, bspw. mittels Sicheren Anschlussknoten und durchg&auml;ngiger Verschl&uuml;sselung.</p>

<p>Verwaltungsnetze sind sehr heterogen ausgebaut, zum Teil sind Kommunen mit Bandbreiten von nur 1 MBit angebunden. Die Zug&auml;nge der Kommunen zum Internet sind in den L&auml;ndern aufgrund teilweise zentraler Vorgaben sehr unterschiedlich ausgestattet. Die Kosten des Verbindungsnetzes sind nach Leistung und Bandbreiten gestaffelt und werden jeweils vom IT-Planungsrat beschlossen. Da das Verbindungsnetz auf dem Backbone und den Standardprodukten eines gro&szlig;en deutsche Carriers basiert, sind Leistung und Bandbreiten mit dem Angebot der Internet-Carrier vergleichbar. Im Verbindungsnetz wird durchg&auml;ngig IPv6 bereitgestellt, sowohl bei der durchg&auml;ngigen Verschl&uuml;sselung wie auch bei den Basisdiensten. FIT-Connect und die meisten OSCI-Verbindungen nutzen das Internet. Damit wird bewusst auf Schutzfunktionen und Souver&auml;nit&auml;t verzichtet.</p>
<p><strong>IT-Netzstrategie 2030</strong></p>
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/5.png" alt="" />
<p><strong>Abb. 4: Zielbild des IV&Ouml;V gem&auml;&szlig; Netzstrategie 2030 (<a href="https://www.cio.bund.de/SharedDocs/topthemen/Webs/CIO/DE/netzstrategie-2030.html">SQ-15</a>)</strong></p>
<p>Im Rahmen der 2019 durch den IT-Rat beschlossenen "Netzstrategie 2030 f&uuml;r die &ouml;ffentliche Verwaltung" sollen die bestehenden Verwaltungsnetze zu einem neuen Informationsverbund der &ouml;ffentlichen Verwaltung (IV&Ouml;V) konsolidiert und weiterentwickelt werden. Die konzeptionellen Arbeiten am IV&Ouml;V befinden sich in einer fr&uuml;hen Phase. Derzeit ist noch wenig &uuml;ber die konkret geplanten Merkmale bekannt.</p>
<p>Wie im Verbindungsnetz wird auch der IV&Ouml;V IPv6 vollumf&auml;nglich unterst&uuml;tzen. Erg&auml;nzt um eine &uuml;bergreifende Governance, die voraussichtlich auch mit technischen Mitteln wie Software Defined Networks (SDN) durchgesetzt wird, k&ouml;nnte dadurch eine Netzinfrastruktur entstehen, welche die Vorteile der Verwaltungsnetze (Informationssicherheit, Souver&auml;nit&auml;t) mit den praktischen Vorteilen eines transparent durchg&auml;ngigen Transportmediums verbindet.In einem solchen Szenario k&ouml;nnte eine verteilte Transportinfrastruktur &uuml;berfl&uuml;ssig werden. Die Integration in den IV&Ouml;V k&ouml;nnte mithilfe der Sicheren Anschlussknoten erfolgen.</p>
<p>Da der IV&Ouml;V zur Ausbaustufe 2025 nicht zur Verf&uuml;gung stehen wird, wird die Transportinfrastruktur vorerst auf die aktuell verf&uuml;gbaren Netze ausgelegt. Dabei soll sie einer Migration auf den neuen Informationsverbund jedoch nicht im Wege stehen, sobald die Voraussetzungen durch die verantwortlichen Stellen geschaffen sind.</p>

#### 3.1.2 Annahme und Rahmenbedingung

<p><strong>Tab. 2: Annahme und Rahmenbedingung</strong></p>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>ID</th>
<th>Annahme / Rahmenbedingung</th>
</tr>
<tr>
<td>ANN-01</td>
<td>
Der Aufbau einer komplett neuen, universellen Transportinfrastruktur wird mehrere Jahre dauern. Da diese Zeit nicht zur Verf&uuml;gung steht, muss entweder eine bestehende, weitverbreitete und etablierte Transportinfrastruktur nachgenutzt werden, die die NOOTS-Anforderungen abdeckt, oder es muss eine neue Infrastruktur eingef&uuml;hrt werden, die auf die NOOTS-Anforderungen zugeschnitten ist und auf etablierten internationalen Standards basiert, um einen schnellen Rollout zu erm&ouml;glichen.
</td>
</tr>
<tr>
<td>ANN-02</td>
<td>
<p>Die heutigen Verwaltungsnetze sind unzureichend ausgebaut, sodass derzeit die Kommunikationsstrecken meist auf das Internet angewiesen sind. Auch in naher Zukunft werden diese Netze nicht ausreichen, um die gesamte Kommunikation im NOOTS zu bew&auml;ltigen.</p>
</td>
</tr>
<tr>
<td>ANN-03</td>
<td>
<p>Gem&auml;&szlig; IT-NetzG &sect;3 ist die Kommunikation zwischen Bund und L&auml;ndern &uuml;ber das Verbindungsnetz vorgeschrieben. Das IDNrG bekr&auml;ftigt diese Regelung in &sect;7 insbesondere bei der Verwendung der IDNr. F&uuml;r die Kommunikation &uuml;ber das Internet ist eine Ausnahmereglung erforderlich.</p>
</td>
</tr>
<tr>
<td>ANN-04</td>
<td>Gem&auml;&szlig; Art 3 OZG&Auml;ndG kann der Datenaustausch auch &uuml;ber andere Netze des Bundes, die einen dem beabsichtigten Datenaustausch entsprechenden IT-Sicherheitsstandard aufweisen, erfolgen.</td>
</tr>
<tr>
<td>ANN-05</td>
<td>Die Transportinfrastruktur muss f&uuml;r das Zielbild 2025 bereitstehen, damit der Rollout im Zielbild 2028 auf Basis der Transportinfrastruktur erfolgen kann.</td>
</tr>
<tr>
<td>ANN-06</td>
<td>Verbindungen &uuml;ber das NdB-VN m&uuml;ssen einzeln auf Antrag freigeschaltet werden. F&uuml;r eine sehr gro&szlig;e Anzahl an Verbindungen skaliert dieser Ansatz nicht (siehe dazu Kapitel <a href="#311-verwaltungsnetze-und-verbindungsnetz">Verwaltungsnetze und Verbindungsnetz</a>).</td>
</tr>
<tr>
<td>ANN-07</td>
<td>Die allermeisten Teilnehmer verf&uuml;gen nicht direkt &uuml;ber einen Anschluss an das Verbindungsnetz. Sie sind &uuml;ber zum Teil sehr leistungsschwache Netze, zum Teil auch &uuml;ber das Internet an das NdB-VN angebunden (siehe Kapitel&nbsp;<a href="#311-verwaltungsnetze-und-verbindungsnetz">Verwaltungsnetze und Verbindungsnetz</a>).</td>
</tr>
</tbody>
</table>

### 3.2 Abgrenzungen

#### 3.2.1 EU-OOTS Transportinfrastruktur

Die NOOTS-Transportinfrastruktur erm&ouml;glicht den rechtskonformen Abruf von elektronischen Nachweisen aus den Registern der deutschen Verwaltung. &Uuml;ber eine Anbindung an die EU-OOTS Transportinfrastruktur (<a href="https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents">EU-02</a>)wird zudem der Austausch von Nachweisen mit dem EU-Ausland erm&ouml;glicht. Innerhalb Deutschlands erfolgt der Austausch von Nachweisen ausschlie&szlig;lich &uuml;ber das NOOTS. Beim Austausch von EU-Nachweisen ist das EU-OOTS f&uuml;r den Transport der Nachweise zwischen den EU-Mitgliedstaaten zust&auml;ndig, w&auml;hrend das NOOTS f&uuml;r den Transport der Nachweise innerhalb von Deutschland verantwortlich ist.

#### 3.2.2 Fachdatenstandard f&uuml;r Nachweise

Die Transportinfrastruktur ist f&uuml;r den sicheren technischen Transport von Nachweisen unabh&auml;ngig von deren Semantik, Struktur und Syntax verantwortlich. Im "Fachdatenkonzept" des PB Register wird beschrieben, wie der automatisierte Nachweisaustausch von Daten zwischen Data Consumer und Data Provider in verschiedenen Reifegraden &uuml;ber das NOOTS fachlich gew&auml;hrleistet werden kann. Die hierf&uuml;r ben&ouml;tigten Bedingungen, wie beispielsweise ein semantischer Fachdatenstandard, werden entsprechend definiert. Somit bildet das Fachdatenkonzept die fachliche Grundlage f&uuml;r den automatisierten Datenaustausch.

## 4. Kontextabgrenzung

### 4.1 Fachlicher Kontext

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/6.png" alt="" />
<p><strong>Abb. 5: Fachlicher Kontext Transportinfrastruktur</strong></p>
<p>In der folgenden Tabelle werden die Anwendungsf&auml;lle f&uuml;r Abrufe von elektronischen Nachweisen zwischen Data Consumer und Data Provider aufgelistet, die durch die NOOTS-Transportinfrastruktur gem&auml;&szlig; dem Architekturzielbild 2028 der High-Level-Architektur (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>) unterst&uuml;tzt werden. In dieser Konzeptversion sind die Anwendungsf&auml;lle f&uuml;r den Abruf von Registerdaten f&uuml;r den Registerzensus sowie f&uuml;r die Wissenschaft noch nicht ber&uuml;cksichtigt.</p>
<p><strong>Tab. 3: HLA-Anwendungsf&auml;lle</strong></p>
<table><colgroup><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>
ID
</th>
<th>
Anwendungsfall
</th>
<th>
Data Consumer
</th>
<th>
Data Provider
</th>
</tr>
<tr>
<td>
1a
</td>
<td>
Interaktiver Nachweisabruf zu einer nat&uuml;rlichen Person &uuml;ber das NOOTS
</td>
<td>
Nationaler Onlinedienst f&uuml;r nat&uuml;rliche Person
</td>
<td>
nationales Register
</td>
</tr>
<tr>
<td>
1b
</td>
<td>
Interaktiver Nachweisabruf zu einem Unternehmen &uuml;ber das NOOTS
</td>
<td>
Nationaler Onlinedienst f&uuml;r Unternehmen
</td>
<td>
nationales Register
</td>
</tr>
<tr>
<td>
2
</td>
<td>
Nicht-Interaktiver Nachweisabruf im NOOTS
</td>
<td>
Nationales Fachverfahren
</td>
<td>
nationales Register
</td>
</tr>
<tr>
<td>
3
</td>
<td>Abruf von nationalen Nachweisen aus EU-Mitgliedstaaten &uuml;ber das EU-OOTS</td>
<td>Intermedi&auml;re Plattform</td>
<td>nationales Register</td>
</tr>
<tr>
<td>
4
</td>
<td>
Abruf von europ&auml;ischen Nachweisen durch nationale Data Consumer &uuml;ber das Europ&auml;ische Once-Only-Technical-System (EU-OOTS)
</td>
<td>
Nationaler Onlinedienst f&uuml;r nat&uuml;rliche Person oder Unternehmen
</td>
<td>Intermedi&auml;re Plattform</td>
</tr>
</tbody>
</table>
<p>Die Data Consumer und Data Provider werden verpflichtend &uuml;ber das Anschlussprotokoll des Sicheren Anschlussknotens (SAK) an die Transportinfrastruktur angebunden. Die Intermedi&auml;re Plattform (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-06</a>) fungiert als Vermittler zwischen der NOOTS- und der EU-OOTS-Transportinfrastruktur, indem sie Nachweisabfragen aus dem EU-Ausland in nationale Abfragen umwandelt und umgekehrt. Die Vermittlungsstelle (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-08</a>) ist eine dritte &ouml;ffentliche Stelle, die gem&auml;&szlig; &sect; 7 Abs. 2 IDNrG (<a href="https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf">RGR-01</a>) bei Daten&uuml;bermittlungen unter Verwendung einer IDNr zwischen &ouml;ffentlichen Stellen verschiedener Verwaltungsbereiche eine abstrakte Berechtigungspr&uuml;fung durchf&uuml;hrt. Sie &uuml;berpr&uuml;ft und protokolliert, ob ein Data Consumer einen Nachweistyp von einem Data Provider zu einem angegebenen Kommunikationszweck abrufen darf. Wenn die erforderliche Abrufberechtigung fehlt, erfolgt keine &Uuml;bermittlung von Nachweisdaten. Die Pr&uuml;fung der abstrakten Berechtigung (PAB) wird durch die Sicheren Anschlussknoten der Transportinfrastruktur gew&auml;hrleistet und ist fest in den SAK f&uuml;r die Data Consumer und Data Provider integriert.</p>

### 4.2 Technischer Kontext

Die Transportinfrastruktur ist &uuml;ber die Sicheren Anschlussknoten f&uuml;r den sicheren und effizienten technischen Transport elektronischer Nachweise zwischen Data Consumer und Data Provider gem&auml;&szlig; der XNachweis-Spezifikation (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">XS-01</a>) verantwortlich. XNachweis ist ein Standard f&uuml;r die fachunabh&auml;ngige Anforderung und &Uuml;bermittlung von Nachweisen zu nat&uuml;rlichen Personen und Unternehmen. Dabei muss die Transportinfrastruktur eng mit einer Reihe weiterer NOOTS-Komponenten zusammenarbeiten, um eine reibungslose Daten&uuml;bertragung zu gew&auml;hrleisten und gleichzeitig die Einhaltung gesetzlicher und sicherheitsrelevanter Anforderungen sicherzustellen.
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/7.png" alt="" />
<p><strong>Abb. 6: Technischer Kontext Transportinfrastruktur</strong></p>
<p>Neben der NOOTS-Komponente Vermittlungsstelle muss der Sichere Anschlussknoten (SAK) f&uuml;r einen Data Consumer die Schnittstellen der folgenden NOOTS-Komponenten aufrufen, bevor ein Nachweisabruf des Data Consumer beim Data Provider erfolgen kann. Eine detaillierte Ablaufbeschreibung der Nachweislieferung und der Interaktion der NOOTS-Komponenten ist im Kapitel zur&nbsp;<a href="#8-laufzeitsicht">Laufzeitsicht </a>zu finden.</p>
<ul>
<li><em>IAM f&uuml;r Beh&ouml;rden&nbsp;</em>(<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-05</a>): stellt dem Data Consumer nach erfolgreicher Authentifizierung ein Zugriffstoken f&uuml;r den Zugriff auf andere Komponenten des NOOTS-&Ouml;kosystems aus.</li>
<li><em>IDM f&uuml;r Personen</em> (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-10</a>): erm&ouml;glicht dem Data Consumer den Abruf der Identifikationsnummer (IDNr) einer nat&uuml;rlichen Person anhand von deren Basisdaten (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten">XS-03</a>).</li>
<li><em>IDM f&uuml;r Unternehmen</em> (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-11</a>): erm&ouml;glicht dem Data Consumer den Abruf der bundeseinheitlichen Wirtschaftsnummer (beWiNr) eines Unternehmens anhand von dessen Unternehmensbasisdaten (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten">XS-06</a>).</li>
<li><em>Registerdatennavigation</em> (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-07</a>): erm&ouml;glicht dem Data Consumer die Ermittlung des f&uuml;r den gew&uuml;nschten Nachweis zust&auml;ndigen Data Providers (Nachweisangebot) und dem SAK des Data Consumers den Abruf der technischen Verbindungsparameter des Data Providers.</li>
</ul>

## 5. Anforderungen

### 5.1 Funktionale Anforderungen

Die nachfolgenden funktionalen Anforderungen definieren die erforderlichen Funktionalit&auml;ten oder Verhaltensweisen der Transportinfrastruktur und beschreiben, welche Aufgaben sie ausf&uuml;hren oder welche F&auml;higkeiten sie besitzen soll.
<p><strong>Tab. 4: Funktionale Anforderungen Transportinfrastruktur</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>ID</th>
<th>Anforderung</th>
<th>Erl&auml;uterung</th>
</tr>
<tr>
<td>
NOOTS-994
</td>
<td>
Die Transportinfrastruktur MUSS Nachrichtenaustausche nach dem Request-Response Muster unterst&uuml;tzen.
</td>
<td>F&uuml;r die Anwendungsf&auml;lle des NOOTS werden ausschlie&szlig;lich Nachrichtenaustausche nach dem Request-Response Muster ben&ouml;tigt. Dies beinhaltet die &Uuml;bermittlung von Nachrichten in beide Richtungen sowie die Korrelation von Anfrage und Antwort (siehe auchKapitel "Nachrichtenaustauschmuster" der High-Level-Architektur (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>))</td>
</tr>
<tr>
<td>
NOOTS-712
</td>
<td>
Die Transportinfrastruktur MUSS die Anbindung von Teilnehmern erlauben, die selbst keinen direkten Zugang zu den Beh&ouml;rdennetzen erhalten.
</td>
<td>NOOTS-Teilnehmer, die der mittelbaren Verwaltung angeh&ouml;ren, haben in der Regel keinen Zugang zu den Beh&ouml;rdennetzen. Auch f&uuml;r diese NOOTS-Teilnehmern muss eine Anbindung an das NOOTS erm&ouml;glicht werden.</td>
</tr>
<tr>
<td>
NOOTS-436</u>
</td>
<td>
Die Transportinfrastruktur MUSS sicherstellen, dass die Sicherheit des Transports durch unsachgem&auml;&szlig;en Anschluss der Teilnehmer nicht kompromittiert werden kann.
</td>
<td>
Wenn NOOTS-Teilnehmer komplexe Implementierungen und Konfigurationen des NOOTS-Anschlusses eigenst&auml;ndig durchf&uuml;hren m&uuml;ssen, erh&ouml;ht sich das Risiko fehlerhafter und unsicherer Anschl&uuml;sse. Daher sollte die Transportinfrastruktur den NOOTS-Anschluss durch die Bereitstellung Sicherer Anschlussknoten vereinfachen, die bei sachgem&auml;&szlig;er Nutzung nur ein minimales Fehlerpotenzial aufweisen.
</td>
</tr>
<tr>
<td>
NOOTS-435
</td>
<td>
Die Bestandteile der Transportinfrastruktur, die nicht im Zust&auml;ndigkeitsbereich der Teilnehmer liegen, M&Uuml;SSEN den sicheren Transport der Nachrichten gew&auml;hrleisten k&ouml;nnen, ohne dabei Kenntnis von den Inhaltsdaten zu haben.
</td>
<td>
Beim Abrufen der abstrakten Berechtigung von der Vermittlungsstelle darf diese beispielsweise keinen Zugriff auf die Inhaltsdaten der Nachweisabrufe erhalten.
</td>
</tr>
<tr>
<td>
NOOTS-429
</td>
<td>
Die Transportinfrastruktur MUSS sicherstellen, dass Nachrichten beim Transport nicht ver&auml;ndert werden k&ouml;nnen.
</td>
<td>Sowohl Nachweisabrufe als auch Nachweise enthalten in der Regel personenbezogene Daten. Eine F&auml;lschung oder ein Vertraulichkeitsverlust von Nachweisdaten birgt dar&uuml;ber hinaus je nach Verwaltungsverfahren erhebliches Missbrauchspotential.</td>
</tr>
<tr>
<td>
NOOTS-428
</td>
<td>
Die Transportinfrastruktur MUSS die Authentizit&auml;t der Teilnehmer bei der Zustellung von Nachrichten sicherstellen.
</td>
<td></td>
</tr>
<tr>
<td>
NOOTS-427
</td>
<td>
Die Transportinfrastruktur MUSS die Vertraulichkeit der Nachrichten f&uuml;r Nachweise des Schutzbedarfs <em>hoch</em> sicherstellen.
</td>
<td></td>
</tr>
<tr>
<td>
NOOTS-425
</td>
<td>
Die Transportinfrastruktur DARF Nachrichten beim Nachweisabruf nur dann an Teilnehmer zustellen, wenn die abstrakte Berechtigungspr&uuml;fung durch die Vermittlungsstelle best&auml;tigt hat, dass die &Uuml;bermittlung rechtm&auml;&szlig;ig ist.
</td>
<td></td>
</tr>
<tr>
<td>
NOOTS-413
</td>
<td>
Die Transportinfrastruktur MUSS in der Lage sein, Nachrichten zwischen einer gro&szlig;en Anzahl an Teilnehmern (&gt;100 Data Consumer; &gt;10.000 Data Provider; Komponenten des NOOTS) zu transportieren.
</td>
<td>
Data Consumer und Data Provider befinden sich in der Regel in unterschiedlichen Netzen. Eine Verbindung zwischen diesen erfordert konfigurative &Auml;nderungen an den Netzen (Routingtabellen, Firewall-Freischaltungen). Bei einer gro&szlig;en Anzahl von Teilnehmern wird der Aufwand, jede einzelne Verbindung freizuschalten, untragbar, was oft als "n*m Verbindungsproblem" bezeichnet wird. Daher muss die Transportinfrastruktur Mechanismen bereitstellen, Nachrichten zu &uuml;bermitteln, ohne dass jede Verbindung einzeln freigeschaltet wird.
</td>
</tr>
</tbody>
</table>

### 5.2 Nicht-funktionale Anforderungen

Nicht-funktionale &uuml;bergreifende Anforderungen an das NOOTS gelten auch f&uuml;r die Transportinfrastruktur und werden nachfolgend aufgelistet.
<p><strong>Tab. 5: Nicht-funktionale Anforderungen NOOTS</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>ID</th>
<th>Anforderung</th>
<th>Erl&auml;uterung</th>
</tr>
<tr>
<td>
NOOTS-713
</td>
<td>
Das NOOTS-Gesamtsystem SOLL die Migration zu moderneren Transportstandards nicht erschweren.
</td>
<td>Es ist zu erwarten, dass das NOOTS zuk&uuml;nftig Transportstandards unterst&uuml;tzen muss, die heute noch keine Relevanz besitzen oder noch nicht existieren. Das NOOTS soll daher keine Abh&auml;ngigkeiten zu bestehenden Transportstandards derart manifestieren (bspw. durch Nutzung von OSCI Spezifika in der API), dass diese nicht wieder aufgel&ouml;st werden k&ouml;nnen.</td>
</tr>
<tr>
<td>
NOOTS-878
</td>
<td>
Das NOOTS MUSS den Betrieb im Katastrophenfall gem&auml;&szlig; den Ergebnissen einer Risikoanalyse nach Vorgaben des BSI gew&auml;hrleisten.
</td>
<td></td>
</tr>
<tr>
<td>
NOOTS-741
</td>
<td>
Das NOOTS MUSS sicherstellen, dass Nachweise nur in notwendigem Umfang und f&uuml;r die notwendige Dauer innerhalb des NOOTS verarbeitet werden.
</td>
<td>
Nachrichtenaustausche werden ausschlie&szlig;lich nach dem Request-Response-Muster (NOOTS-994) und sofort ohne Zwischenspeicherung ausgef&uuml;hrt.
</td>
</tr>
<tr>
<td>
NOOTS-738
</td>
<td>
Das NOOTS MUSS die einschl&auml;gigen nationalen IT-Sicherheitsstandards erf&uuml;llen.
</td>
<td>Die relevanten Vorgaben aus dem IT-Grundschutz-Kompendium, insbesondere f&uuml;r Anwendungen, Netze und Kommunikation, sowie die Technischen Richtlinien f&uuml;r kryptografische Verfahren, Zertifikate und elektronische Identit&auml;ten und Vertrauensdienste im E-Government, sind zu beachten.</td>
</tr>
<tr>
<td>
NOOTS-735
</td>
<td>
Das NOOTS MUSS alle einschl&auml;gigen Datenschutzvorschriften einhalten.
</td>
<td>
Die Grunds&auml;tze der Datenminimierung, Richtigkeit, Speicherbegrenzung, Integrit&auml;t und Vertraulichkeit, Notwendigkeit, Verh&auml;ltnism&auml;&szlig;igkeit und Zweckbindung m&uuml;ssen eingehalten werden. Einschl&auml;gig sind die Regelungen der Datenschutz-Grundverordnung (DSGVO), insbesondere die Artikel 5-7 sowie 25, und des Bundesdatenschutzgesetzes.
</td>
</tr>
<tr>
<td>
NOOTS-726
</td>
<td>
Das NOOTS SOLL sich an dem Aufbau und den Standards des EU-OOTS orientieren und bei nationalen Abweichungen Interoperabilit&auml;t mit dem EU-OOTS gew&auml;hrleisten.
</td>
<td>
Das Request-Response-Muster f&uuml;r Nachrichtenaustausche (NOOTS-994) entspricht der Definition des Request-Reply Integration Patterns im Kapitel 7.2 "Integration and Service Interaction Patterns" der "Once-Only Technical System High Level Architecture" (<a href="https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents">EU-02</a>) des EU-OOTS.
F&uuml;r die fachunabh&auml;ngige Anforderung und &Uuml;bermittlung von Nachweisen zu nat&uuml;rlichen und juristischen Personen &uuml;ber die Transportinfrastruktur wird XNachweis verwendet. Der Standard ist kompatibel mit den europ&auml;ischen Spezifikationen (EU Technical Design Documents) der Verordnung (EU) 2018/ 1724 des Europ&auml;ischen Parlaments und des Rates.
</td>
</tr>
<tr>
<td>
NOOTS-722
</td>
<td>
Das NOOTS MUSS so gestaltet sein, dass es nicht durch nur ein einziges IT-Produkt oder einen einzigen Hersteller umgesetzt werden kann.
</td>
<td></td>
</tr>
<tr>
<td>
NOOTS-721
</td>
<td>
Das NOOTS MUSS Sicherheitseinstellungen bereits in der Grundkonfiguration seiner Dienste aktiviert haben (Security by default).
</td>
<td>
Alle Sicherheitseinstellungen der NOOTS-Komponenten, einschlie&szlig;lich des Sicheren Anschlussknotens, m&uuml;ssen bereits in der Grundkonfiguration des Dienstes aktiviert sein.
</td>
</tr>
<tr>
<td>
NOOTS-720
</td>
<td>
Das NOOTS SOLL so weitm&ouml;glich und zweckm&auml;&szlig;ig bestehende L&ouml;sungen, u.a. des IT-Planungsrats, wiederverwenden oder weiterentwickeln.
</td>
<td>
Der IT-Planungsrat koordiniert einerseits die Entwicklung und Implementierung von Produkten wie OSCI oder DVDV und legt andererseits &uuml;bergreifende IT-Interoperabilit&auml;ts- und Sicherheitsstandards wie den X&Ouml;V-Standardisierungsrahmen fest.
</tr>
<tr>
<td>NOOTS-105</td>
<td>
Das NOOTS MUSS f&uuml;r jede NOOTS-Komponente und NOOTS-Teilnehmer deren Schnittstellen so spezifizieren, dass deren Implementierungen durch Kompatibilit&auml;tstests verifiziert werden k&ouml;nnen.
</td>
<td>Das Design der Schnittstellen f&uuml;r den Anschluss an das NOOTS muss offenen, testbaren Standards wie beispielsweise REST, OpenAPI usw. folgen.</td>
</tr>
<tr>
<td>
NOOTS-717
</td>
<td>
Das NOOTS MUSS Mechanismen vorsehen, die den Parallelbetrieb unterschiedlicher Versionen einzelner Standards und Schnittstellen erlauben, so dass Umstellungen nicht zeitgleich in allen betroffenen Systemen erfolgen m&uuml;ssen.
</td>
<td>
An die Transportinfrastruktur sind zahlreiche Teilnehmer und Komponenten angeschlossen (NOOTS-413). Eine Umstellung der Schnittstellen und Standards f&uuml;r den Anschluss an das NOOTS kann nicht zu einem einheitlichen Stichtag erfolgen. Der Parallelbetrieb verschiedener Versionen von Standards (z.B. XNachweis) und Schnittstellen innerhalb des Gesamtsystems NOOTS muss gew&auml;hrleistet sein. Bei der Planung, Implementierung und im Betrieb der NOOTS-Komponenten muss ber&uuml;cksichtigt werden, dass angebotene Schnittstellen verschiedene Versionen eines Standards parallel liefern k&ouml;nnen und genutzte Services gegebenenfalls mit unterschiedlichen Versionen antworten. Bei der Einf&uuml;hrung neuer Versionen von Schnittstellen und Standards muss eine &Uuml;bergangszeit f&uuml;r den Parallelbetrieb festgelegt werden, mit einem klar definierten Enddatum f&uuml;r die Unterst&uuml;tzung veralteter Versionen.
</td>
</tr>
</tbody>
</table>

## 6. L&ouml;sungsstrategie

Die folgende Liste enth&auml;lt die Top-7 Designentscheidungen f&uuml;r die NOOTS-Transportinfrastruktur:
<ul>
<li><strong>Verpflichtender SAK f&uuml;r Einhaltung gesetzlicher Vorgaben, inklusive Vermittlungsstelle</strong>:<br/>
Die Transportinfrastruktur ist verpflichtet sicherzustellen, dass gesetzliche und sicherheitsrelevante Anforderungen eingehalten werden. Dazu geh&ouml;rt unter anderem die Verpflichtung, bei jedem Nachweisabruf mit einer IDNr die abstrakte Berechtigungspr&uuml;fung der Vermittlungsstellen gem&auml;&szlig; &sect; 7 Abs. 2 IDNrG (<a href="https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf">RGR-01</a>) durchzuf&uuml;hren. Der verbindliche Einsatz des SAK mit integrierter Pr&uuml;fung des Vorliegens der abstrakten Berechtigung bei jedem Nachweisabruf mit IDNr erm&ouml;glichtdie Erf&uuml;llung dieser gesetzlichen Anforderung.</li>
<li><strong>St&auml;rkung der Resilienz durch SAKs in der dezentralen Verantwortung der NOOTS-Teilnehmer</strong>:<br/>
Der SAK wird als zentrale Software-Komponente bereitgestellt, jedoch dezentral von den NOOTS-Teilnehmern betrieben. Der Datenaustausch erfolgt ausschlie&szlig;lich zwischen den dezentralen SAKs der NOOTS-Teilnehmer. Nach der Herstellung einer sicheren Verbindung zwischen den SAKs der Data Consumers und Data Providers h&auml;ngt der kontinuierliche Datenaustausch ausschlie&szlig;lich von der Verf&uuml;gbarkeit der dezentralen SAKs, den NOOTS-Teilnehmern und dem Netzwerk zwischen ihnen ab. Ein Ausfall eines dezentralen SAKs bei einem Data Consumer beeintr&auml;chtigt ausschlie&szlig;lich die daran angeschlossenen Data Consumers. Ein Ausfall eines dezentralen SAKs bei einem Data Provider betrifft lediglich die anfragenden SAKs. Die Gesamtverf&uuml;gbarkeit der &uuml;brigen SAKs bleibt davon unbeeinflusst. Eine Kompromittierung einer einzelnen dezentralen SAK-Instanz f&uuml;hrt nicht unmittelbar zur Kompromittierung anderer SAKs.</li>
<li><strong>Einfacher NOOTS-Anschluss durch interoperables Anschlussprotokoll:</strong><br/>
In einer heterogenen f&ouml;deralen IT-Anwendungslandschaft m&uuml;ssen zahlreiche NOOTS-Teilnehmer an NOOTS angeschlossen werden. Daher ist es erforderlich, dass die Anbindung &uuml;ber ein einfaches, stabiles und interoperables Anschlussprotokoll des SAK erfolgt. Auf diese Weise sollen &Auml;nderungen in der Transportinfrastruktur m&ouml;glichst geringe Auswirkungen auf die Anbindung der zahlreichen NOOTS-Teilnehmer haben.</li>
<li><strong>Nahtlose Integration des SAK in bestehende IT-Landschaften:</strong><br/>
Heutzutage betreiben zuk&uuml;nftige NOOTS-Teilnehmer ihre Onlinedienste, Fachverfahren und Register sowohl in traditionellen Betriebsumgebungen als auch in (DVC-konformen) Cloud-Umgebungen. Der SAK muss sowohl auf herk&ouml;mmlichen Serverplattformen als auch als Container in DVC-konformen Cloud-Umgebungen einsatzf&auml;hig sein.</li>
<li><strong>Vereinfachter NOOTS-Anschluss zur Komplexit&auml;tsreduktion:</strong><br/>
Durch die Auslagerung zentraler Transportfunktionen wie Berechtigungspr&uuml;fungen und Zertifikatsverwaltung in den SAK werden die NOOTS-Teilnehmer beim NOOTS-Anschluss erheblich entlastet.</li>
<li><strong>Unterst&uuml;tzung des Request-Response-Nachrichtenaustauschmusters:</strong><br/>
Die nationalen Onlinedienste und Verwaltungsleistungen der EU-Mitgliedstaaten (Evidence Requester) erm&ouml;glichen den Nutzern einen interaktiven Abruf von Nachweisen aus nationalen Registern mit sofortiger Vorschau (Preview) der Nachweisdaten. Die NOOTS-Transportinfrastruktur unterst&uuml;tzt dieses interaktive Nutzerverhalten durch Transport- und Anschlussprotokolle, die das synchrone Request-Response-Nachrichtenaustauschmuster implementieren. Diese Designentscheidung entspricht der Definition des Request-Reply-Integrationsmusters im Kapitel 7.2 "Integration and Service Interaction Patterns" der "Once-Only Technical System High Level Architecture" (<a href="https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents">EU-02</a>) des EU-OOTS.</li>
<li><strong>Nachhaltigkeit in Transport</strong>:<br/>
Die Transportinfrastruktur wird von Beginn an unter Ber&uuml;cksichtigung des Ressourcenverbrauchs entwickelt, zum Beispiel durch die Implementierung einer leichtgewichtigen SAK-L&ouml;sung oder den Einsatz effizienter Nachrichtenaustauschmuster. Dadurch entstehen kosteneffiziente und klimafreundliche L&ouml;sungen.</li>
</ul>

## 7. Bausteinsicht

### 7.1 Baustein &Uuml;bersicht

Im Rahmen des Entwurfs der NOOTS Transportinfrastruktur wurden verschiedene bestehende Transportinfrastrukturen untersucht. Dabei hat sich gezeigt, dass diese sehr unterschiedliche Ziele verfolgen und einen sehr unterschiedlichen Aufbau aufweisen. Daher wird in der Baustein &Uuml;bersicht zun&auml;chst ein abstraktes Architekturmodell abgeleitet. Dieses dient vor allem dazu, eine einheitliche Begriffsbildung f&uuml;r die wesentlichen Elemente einzuf&uuml;hren. Im <a href="#anhang-a---bestehende-transportinfrastrukturen">Anhang A - Bestehende Transportinfrastrukturen</a> werden die existierenden Transportinfrastrukturen unter Verwendung dieser Begriffe beschrieben.
<p>Basierend auf dem abstrakten Architekturmodell und den zugeh&ouml;rigen Begriffen werden in den folgenden Kapiteln ausgew&auml;hlte Bausteine im Kontext des NOOTS ausf&uuml;hrlich erl&auml;utert, darunter die Bausteine NOOTS Sicherer Anschlussknoten (SAK), NOOTS-Anschlussprotokoll und NOOTS-Transportprotokoll.</p>

#### 7.1.1 Abstraktes Architekturmodell

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/8.png" alt="" />

<p><strong>Abb. 7: Abstraktes Architekturmodell der Transportinfrastruktur des NOOTS</strong></p>
<p>Das Modell geht davon aus, dass Data Consumer und Data Provider in unterschiedlichen Netzen angesiedelt sind. In der Darstellung werden aus Gr&uuml;nden der Anschaulichkeit jeweils drei Data Consumer und drei Data Provider je Netz dargestellt. Tats&auml;chlich sind deutlich mehr Teilnehmer je Netz zu erwarten - allein die Melderegister machen im Mittel mehrere Hundert je Bundesland aus. Zudem gibt es auch mehr als nur zwei Netze. F&uuml;r eine einzelne Nachrichten&uuml;bermittlung spielen jedoch nur zwei Netze eine Rolle: Das des Data Consumer und das des Data Providers.</p>
<p>Data Consumer und Data Provider sind &uuml;ber Anschlussknoten an die Transportinfrastruktur angeschlossen. Die Anschlussknoten auf der Seite des Data Consumer &uuml;bermitteln Nachweisabrufe an einen Transportknoten in ihrem Netz. Diese Transportknoten sind an zentraler Stelle im jeweiligen Netz angesiedelt und von allen Teilnehmern innerhalb des Netzes erreichbar. Sie leiten die Nachrichten dann zum Transportknoten auf der Seite des Data Providers weiter. Dabei nutzen sie ein Medium f&uuml;r den Netz&uuml;bergang, das entweder eine direkte Kopplung zwischen den Netzen oder ein drittes Netz, das die erstgenannten miteinander verbindet, sein kann. Der Transportknoten des Data Providers kann alle Anschlussknoten der Data Provider in seinem Netz erreichen und &uuml;bermittelt die Nachricht an den geeigneten Anschlussknoten. Dort kann der Data Provider die Nachricht abholen, oder der Anschlussknoten leitet den Nachweisabruf aktiv an den Data Provider weiter.</p>
<p>Die Anschlussknoten sind unmittelbar im Verantwortungsbereich des Data Consumer bzw. Data Provider angesiedelt, in der Regel in derselben Betriebsumgebung oder gar auf derselben Maschine. Sie k&ouml;nnen daher als Teil des Data Consumer oder Data Provider betrachtet werden und Teile von deren Aufgaben in deren Auftrag &uuml;bernehmen, insbesondere die Ende-zu-Ende-Verschl&uuml;sselung. Die Kommunikation zwischen Data Consumer oder Data Provider und deren Anschlussknoten erfolgt &uuml;ber ein Anschlussprotokoll, das auf Einfachheit ausgelegt ist und Details der Transportinfrastruktur so weit wie m&ouml;glich vor diesen verbirgt.</p>
<p>Transportprotokolle werden zwischen Anschlussknoten, zwischen Anschlussknoten und Transportknoten sowie zwischen jeweils zwei Transportknoten eingesetzt. Diese sind in der Regel deutlich flexibler, aber auch komplexer als die Anschlussprotokolle, da sie beispielsweise neben den Fachdaten auch Informationen zur Ansteuerung der Transportknoten &uuml;bermitteln m&uuml;ssen.</p>
<p>Die wesentlichen Bausteine des Modells sind:</p>
<p><strong>Tab. 6: Bausteine der Transportinfrastruktur</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Baustein</th>
<th>Bedeutung</th>
<th>Bemerkung</th>
</tr>
<tr>
<td>Data Consumer (DC)</td>
<td>Technisches System, welches Nachweis-Anfragen &uuml;ber die Transportinfrastruktur versendet und Antworten dar&uuml;ber empf&auml;ngt.</td>
<td>Data Consumer und Anschlussknoten des DC sind in der gleichen lokalen Betriebsumgebung angesiedelt.</td>
</tr>
<tr>
<td>Data Provider (DP)</td>
<td>Technisches System, welches Nachweisabrufe &uuml;ber die Transportinfrastruktur empf&auml;ngt und Antworten dar&uuml;ber versendet.</td>
<td>
<p>Data Provider und Anschlussknoten des DP sind in der gleichen lokalen Betriebsumgebung angesiedelt.</p>
</td>
</tr>
<tr>
<td>Teilnehmernetz des DC bzw. DP</td>
<td>Verwaltungsnetz, in dem der Data Consumer oder Data Provider angesiedelt ist.</td>
<td>F&uuml;r Bundesbeh&ouml;rden ist dies das NdB (Netze des Bundes). Landesbeh&ouml;rden sind in Landesnetzen angesiedelt. Kommunalbeh&ouml;rden verf&uuml;gen je nach Bundesland &uuml;ber ein eigenes Kommunalnetz, haben Zugang zum Landesnetz oder k&ouml;nnen ausschlie&szlig;lich &uuml;ber das Internet kommunizieren.</td>
</tr>
<tr>
<td>Medium Netz&uuml;bergang</td>
<td>Abstraktes Konzept f&uuml;r einen Netz&uuml;bergang. In der Praxis kann das eine direkte Verbindung der Netze, ein eigenes Verbindungsnetz oder das Internet sein.</td>
<td>Im EU-OOTS wird das Internet als Medium f&uuml;r den Netz&uuml;bergang zwischen des Beh&ouml;rdennetzen der Mitgliedstaaten genutzt. In Deutschland ist das Verbindungsnetz (NdB-VN) eigentlich f&uuml;r diese Zweck vorgesehen. Vielfach wird aber auch hier das Internet daf&uuml;r verwendet.</td>
</tr>
<tr>
<td>Anschlussknoten</td>
<td>Technische Infrastrukturkomponente, die dem Data Consumer oder Data Provider den Anschluss an die Transportinfrastruktur erleichtern soll.</td>
<td>Anschlussknoten sind nah beim jeweiligen Teilnehmer angesiedelt, in der Regel in der selben Betriebsumgebung. Sie haben Zugriff auf die Daten und unterst&uuml;tzen den Teilnehmer bei der Verschl&uuml;sselung, Validierung und Rechtepr&uuml;fung. Aufgrund der N&auml;he zum Teilnehmer eignen sie sich als Endpunkt einer Ende-zu-Ende Verschl&uuml;sselung zwischen den Teilnehmern.</td>
</tr>
<tr>
<td>Transportknoten</td>
<td>Technische Infrastrukturkomponente, welche den Netz&uuml;bergang in benachbarte Netze erm&ouml;glicht.</td>
<td>
<p>Transportknoten sind in der Regel zentral in den Verwaltungsnetzen der Teilnehmer angesiedelt und &uuml;bernehmen die Weiterleitung von Nachrichten vom sendenden Anschlussknoten zum empfangenden Anschlussknoten in segmentierten Netzen. Ein Reverse Proxy kann beispielsweise eine Weiterleitung auf der OSI-Schicht 5 (Sitzung) ohne TLS-Terminierung durchf&uuml;hren und somit eine Ende-zu-Ende-Verschl&uuml;sselung zwischen den Anschlussknoten auf der OSI-Schicht 5 (Sitzung) gew&auml;hrleisten.</p>
</td>
</tr>
<tr>
<td>Anschlussprotokoll</td>
<td>Technisches Protokoll zur Kommunikation eines Teilnehmers mit seinem Anschlussknoten zum Versand und Erhalt von Nachrichten</td>
<td>Protokoll, mit dem die Teilnehmer mit ihrem Anschlussknoten kommunizieren. Das Protokoll ist auf Einfachheit ausgelegt und verbirgt die Komplexit&auml;t des dahinter liegenden Transports.</td>
</tr>
<tr>
<td>Transportprotokoll</td>
<td>Technisches Protokoll, mit dem Anschlussknoten mit Transportknoten oder Transportknoten untereinander kommunizieren.</td>
<td>Protokoll, mit dem die Anschlussknoten mit den Transportknoten und die Transportknoten untereinander kommunizieren. Neben den Netzdaten protokolliert es auch Informationen zu Steuerung des Transports und Quittungen. Die Inhaltsdaten werden zwischen den Anschlussknoten Ende-zu-Ende verschl&uuml;sselt.</td>
</tr>
<tr>
<td>Zentrale Dienste</td>
<td>Stellt von der Transportinfrastruktur ben&ouml;tigte Informationen bereit, die an zentraler Stelle gepflegt werden m&uuml;ssen.</td>
<td>Dazu geh&ouml;ren bspw. die Adressierung und Berechtigung. Im NOOTS &uuml;bernehmen die Registerdatennavigation und IAM f&uuml;r Beh&ouml;rden diese Aufgabe. Sie sind im NOOTS also nicht Gegenstand der Transportinfrastruktur.</td>
</tr>
</tbody>
</table>

#### 7.1.2 Topologien

Das abstrakte Architekturmodell sieht vor, dass sich vier Knoten (Anschlussknoten DC, Transportknoten DC, Transportknoten DP und Anschlussknoten DP) auf der Strecke zwischen Data Consumer und Data Provider befinden. In den untersuchten&nbsp;<a href="#anhang-a---bestehende-transportinfrastrukturen">Transportinfrastrukturen </a>im Anhang wird diese Topologie jedoch nicht verwendet. Stattdessen nutzen sie nur die Teile, die f&uuml;r ihre spezifischen Anforderungen erforderlich sind. Im Folgenden werden verschiedene Topologien betrachtet und miteinander verglichen.

##### 7.1.2.1 Nur Anschlussknoten, keine Transportknoten (siehe auch <a href="#x-road">X-Road</a>)

Transportknoten haben im abstrakten Architekturmodell vor allem die Aufgabe, Nachrichten &uuml;ber die Netzstrukturen zu routen. Verzichtet man auf Transportknoten, muss das darunterliegende Transportmedium das Routing der Nachrichten allein bew&auml;ltigen. Daf&uuml;r ben&ouml;tigt man ein nicht segmentiertes, alle Beh&ouml;rden &uuml;berspannendes Netz wie das Internet. So hat bspw. X-Road von vornherein den Ansatz verfolgt, den physischen Nachrichtentransport &uuml;ber das Internet zu realisieren. Es verf&uuml;gt daher nicht &uuml;ber Transportknoten und w&auml;re nicht in der Lage, Nachrichten durch die deutschen Verwaltungsnetze zu routen.
<p>Transportknoten verursachen hohe Betriebsaufw&auml;nde, da in jedem beteiligten Netzsegment mindestens ein Transportknoten mit hoher Verf&uuml;gbarkeit betrieben werden muss. Sie sind jedoch f&uuml;r ein Routing &uuml;ber segmentierte Netze unverzichtbar, wie am Beispiel des EU-OOTS zu sehen ist: F&uuml;r jeden Mitgliedsstaat (MS) wird ein eigener AS4 Access Point vorgesehen, der den Datenverkehr aus dem MS und in den MS kanalisiert und durch die Verbindung zu den AS4 Access Points der anderen MS den grenz&uuml;berschreitenden Datenverkehr erm&ouml;glicht.</p>
<p>Bei Verzicht auf Transportknoten muss nur noch sehr wenig Infrastruktur f&uuml;r den Transport betrieben werden. Da die Anschlussknoten in der Regel durch die Teilnehmer selbst betrieben werden, sind nur die zentralen Dienste durch eine dritte Stelle zu betreiben. Der Aufbau der &uuml;ber die Netze verteilten Transportknoten entf&auml;llt.</p>

##### 7.1.2.2 Nur Transportknoten, keine Anschlussknoten (siehe auch <a href="#edelivery-as4">AS4 eDelivery</a>)

Anschlussknoten vereinfachen den Zugang zur Transportinfrastruktur. Verzichtet man auf sie, bedeutet das zun&auml;chst nur, dass jeder Teilnehmer die Anschlussbedingungen des Transports selbst vollumf&auml;nglich erf&uuml;llen muss. Das ist komplex und fehlertr&auml;chtig. Insbesondere der Umgang mit Verschl&uuml;sselung und Zertifikaten birgt Risiken, wenn dies von sehr vielen Teilnehmern selbst realisiert werden muss. Aus diesem Grund wurde die OSCI Infrastruktur, die urspr&uuml;nglich ebenfalls ohne Anschlussknoten aufgebaut wurde, um Anschlussknoten auf Basis der XTA Spezifikation erweitert.
<p>Eine Alternative zu den Anschlussknoten sind Client-Bibliotheken, wie sie beispielsweise f&uuml;r OSCI schon lange im Einsatz sind. Diese Bibliotheken bieten vordefinierte Funktionen und Methoden, die den Zugang zur Transportinfrastruktur erleichtern, erfordern jedoch eine direkte Integration in den Programmcode des Nutzers. Bei der Verwendung einer Client-Bibliothek ist man auf die Aktualisierungen und Wartung dieser Bibliothek durch den Anbieter angewiesen und stark von der Implementierungstechnologie abh&auml;ngig, da sie sprachspezifisch sind. Ein Beispiel hierf&uuml;r ist die OSCI-Bibliothek, die nur in Java und .NET verf&uuml;gbar ist. In modernen Architekturen wird vermehrt auf eigenst&auml;ndig lauff&auml;hige modulare Services gesetzt, was auch im Falle der Anschlussknoten der Fall ist. Diese modularen Services kapseln die Funktionen und Methoden f&uuml;r den Zugang zur Transportinfrastruktur und sind &uuml;ber eine interoperable APIs ansprechbar.</p>
<p>Auch das EU-OOTS sieht auf den ersten Blick nur Transportknoten vor, das ist allerdings nicht ganz zutreffend: Die Anschl&uuml;sse der Teilnehmer schreibt das EU-OOTS nicht vor, da es diese in der Zust&auml;ndigkeit der Mitgliedsstaaten sieht. Vorgeschrieben werden nur die AS4-Strecken f&uuml;r den grenz&uuml;berschreitenden Datenverkehr. Inl&auml;ndischer Datenverkehr und die Anbindung der Teilnehmer bleibt den nationalen Transportmechanismen &uuml;berlassen. Dort sind Anschlussknoten also durchaus zu erwarten.</p>
<p>Bei der Kosten-/Nutzenbewertung von Anschlussknoten ist zu beachten, dass zwar sehr viele Anschlussknoten betrieben werden m&uuml;ssen, diese jedoch typischerweise von den Betreibern der Data Consumer bzw. Data Provider mit betrieben werden - ein in modernen serviceorientierten Architekturen g&auml;ngiger Ansatz. Der Verzicht auf Anschlussknoten bringt hingegen nur scheinbar eine Vereinfachung. Zwar entfallen sehr viele Knoten, deren Aufgaben verschwinden damit jedoch nicht - sie werden zu den Teilnehmern verschoben undm&uuml;ssen dort immer wieder neu umgesetzt werden. Dadurch bleiben Synergien ungenutzt und das Risiko fehlerhafter Implementierungen steigt mit jedem Teilnehmer.</p>

##### 7.1.2.3 Zentraler Transportknoten (siehe auch <a href="#fit-connect">FIT-Connect</a>)

Ein einzelner Transportknoten, &uuml;ber den alle Verbindungen laufen, kann bereits fast alle Anforderungen an den Transport erf&uuml;llen. Mittels Protokollierung oder Notifikation kann er Nachvollziehbarkeit und Nichtabstreitbarkeit gew&auml;hrleisten, empfangene Nachrichten zwischenspeichern und durch Initiativumkehr das n*m Verbindungen Problem (siehe auch&nbsp;<a href="#noots-413-">NOOTS-413</a>) l&ouml;sen. Nicht l&ouml;sen kann es jedoch das Routing &uuml;ber segmentierte Netze: Der Zentralknoten ben&ouml;tigt weiterhin Verbindungen zu allen Teilnehmern &uuml;ber Netzsegmentgrenzen hinweg. Das ist insbesondere problematisch, wenn mehrere Netzsegmente &uuml;berbr&uuml;ckt werden m&uuml;ssen. Durch den Verzichtet auf Anschlussknoten sind die Teilnehmer zudem mit der Komplexit&auml;t der Ende-zu-Ende Verschl&uuml;sselung und der Zertifikatsverwaltung konfrontiert.
<p>Dieser Ansatz kommt bei FIT-Connect zum Einsatz. Allerdings nutzt FIT-Connect das Internet, sodass die Problematik segmentierter Verwaltungsnetze nur dann eine Rolle spielt, wenn Teilnehmer nicht &uuml;ber das Internet erreichbar sind.</p>

##### 7.1.2.4 Weder Anschlussknoten noch Transportknoten (direkte Punkt-zu-Punkt Kommunikation)

Wird auf beide Arten von Knoten verzichtet, entf&auml;llt die Transportinfrastruktur vollst&auml;ndig. Sie kann demnach auch keinerlei Anforderungen erf&uuml;llen. Dieser Ansatz wird hier betrachtet, weil diese direkte Nutzung des Internets aus privaten oder privatwirtschaftlichen Nutzungsszenarien gut bekannt ist. Dabei werden Dienste im Internet &ouml;ffentlich zur Verf&uuml;gung gestellt. Eine Zustellung &uuml;ber segmentierte Netze ist damit allerdings nicht m&ouml;glich, ebenso wenig eine Erreichbarkeit nicht-&ouml;ffentlicher Dienste. Das k&ouml;nnen die Data Provider zwar durch g&auml;ngige DMZ und Reverse-Proxy L&ouml;sungen selbst implementieren. Dadurch entstehen jedoch sehr viele individuelle L&ouml;sungen,in denen Sicherheit nur noch schwer gew&auml;hrleistet werden kann. Auch die Nachvollziehbarkeit kann in diesen Szenarien nur durch die Dienstenutzer oder Diensteanbieter selbst realisiert werden. F&uuml;r das NOOTS wird dieser Ansatz nicht weiterverfolgt.

##### 7.1.2.5 Mischformen (OSCI/XTA)

In der deutschen Verwaltung kommen historisch bedingt unterschiedliche Topologien zum Einsatz:
<p><strong>Tab. 7: Mischformen (OSCI/XTA)</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Bezeichnung</th>
<th>Topologie</th>
<th>Bewertung</th>
</tr>
<tr>
<td>Direkte OSCI Verbindungen</td>
<td>Verzichten auf Anschlussknoten. Zudem kommt zwischen je zwei Teilnehmern nur ein Transportknoten auf der Seite des empfangenden Teilnehmers zum Einsatz.</td>
<td>Ein vollst&auml;ndiges Routing ist &uuml;ber segmentierte Netze nicht m&ouml;glich. Die Kommunikation zwischen sendendem Teilnehmer und OSCI Intermedi&auml;r erfolgt in der Regel &uuml;ber das Internet. Eine Ende-zu-Ende Verschl&uuml;sselung ist grunds&auml;tzlich m&ouml;glich, aber selten implementiert, da diese recht komplex ist.</td>
</tr>
<tr>
<td>OSCI Verbindungen mit dezentralen XTA-Implementierungen</td>
<td>Nah bei den Teilnehmern verortete XTA-Implementierungen werden als Anschlussknoten genutzt. Die XTA-Implementierungen kommunizieren in der Regel &uuml;ber einen OSCI Intermedi&auml;r.</td>
<td>
Das Routing erfolgt allein durch die OSCI Intermedi&auml;re. Analog zu direkten OSCI Verbindungen ist daher nur ein eingeschr&auml;nktes Routing &uuml;ber die Verwaltungsnetze m&ouml;glich.
<p>Die XTA-Implementierungen &uuml;bernehmen neben der Ansteuerung der OSCI Intermedi&auml;re auch die Verschl&uuml;sselung der Nachrichten, die Pr&uuml;fung der Zertifikate und fachliche Validierung.</p>
<p>Fachliche Funktionalit&auml;t aus verschiedenen Fachdom&auml;nen in den XTA-Implementierungen (siehe auch "<a href="#anhang-a---bestehende-transportinfrastrukturen">Anhang A - Bestehende Transportinfrastrukturen</a>") und die unterschiedliche Ausgestaltung der Verschl&uuml;sselungs- und Entschl&uuml;sselungsmethoden bei OSCI und XTA 2 (<a href="https://www.mhkbd.nrw/system/files/media/document/file/whitepaper_ende_zu_ende_verschluesselung_osci-xta.pdf">SQ-26</a>) sind Ursache f&uuml;r Interoperabilit&auml;ten.</p>
</td>
</tr>
<tr>
<td>OSCI Verbindungen mit zentralen XTA-Implementierungen</td>
<td>Zentral in den Verwaltungsnetzen verortete XTA-Implementierungen werden als Transportknoten genutzt. Die XTA-Implementierungen kommunizieren in der Regel &uuml;ber einen OSCI Intermedi&auml;r.</td>
<td>
<p>Die XTA Implementierungen k&ouml;nnen das Routing &uuml;ber die Netze &uuml;bernehmen. Aufgrund der zentralen Verortung sind sie als Intermedi&auml;r-Systeme zu sehen, die keine Einsicht in die Inhaltsdaten nehmen d&uuml;rfen. Sie sind daher keine geeigneten Stellen f&uuml;r die Verschl&uuml;sselung, Verwaltung der Zertifikate und Validierung.</p>
<p>Die OSCI Intermedi&auml;re werden in diesen Szenarien nicht f&uuml;r Routingzwecke, sondern nur f&uuml;r die Zwischenspeicherung von Nachrichten in asynchronen Kommunikationsszenarien ben&ouml;tigt.</p>
</td>
</tr>
</tbody>
</table>
<p>Theoretisch m&ouml;gliche weitere Topologien, wie die Kaskadierung mehrerer OSCI Intermedi&auml;re, werden technisch nicht unterst&uuml;tzt.</p>
<p>Das 4-Corner-Modell ist ein abstraktes Delegationsmodell in dem zwei Teilnehmer (Corner 1 und 4) miteinander interagieren, indem sie sich Dritter (Corner 2 und 3) bedienen, um Teile dieser Interaktion f&uuml;r sie abzuwickeln. Das Modell wird h&auml;ufig f&uuml;r Finanztransaktionen verwendet. Die Corner 2 und 3 werden dann von Finanzdienstleistern &uuml;bernommen. In der Registermodernisierung wird das Modell zur Beschreibung der Nachrichtenkommunikation &uuml;ber dritte Stellen genutzt (&sect; 7 Abs. 2 IDNrG). Allerdings ist unklar, welche Aufgaben die Corner 2 und 3 konkret &uuml;bernehmen sollen. Davon h&auml;ngt jedoch ab, wo diese Corner in der Architektur verortet sind. Drei der im Rahmen der Konzeption betrachteten <a href="#anhang-a---bestehende-transportinfrastrukturen">Transportinfrastrukturen</a> implementieren nach eigener Aussage das 4-Corner-Modell, sehen aber v&ouml;llig unterschiedliche Aufgaben bei den Cornern 2 und 3 vor. Dieses Dokument nutzt daher die konkreten Begriffe des abstrakten Architekturmodells anstelle der Corner. Dabei k&ouml;nnen sowohl die Anschlussknoten als auch die Transportknoten als Corner 2 und 3 interpretiert werden.</p>

#### 7.1.3 4-Corner-Modell und V-Modell

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/9.png" alt="" />
<p><strong>Abb. 8: &Uuml;berblick &uuml;ber das 4-Corner-Modell</strong></p>
<p>Die folgende Abbildung schl&auml;gt ein V-Modell des Nachrichtentransports als Erweiterung des 4-Corner-Modells vor. Dabei werden unterschiedliche Arten der Delegation unterschieden und in Ebenen dargestellt. Je nachdem welche Aufgabe delegiert werden soll, ist eine andere Ebene daf&uuml;r geeignet. So k&ouml;nnen Infrastrukturkomponenten der Ebene 1, wie die Anschlussknoten, die Ende-zu-Ende Verschl&uuml;sselung f&uuml;r die Teilnehmer &uuml;bernehmen, jedoch keine Steuerung des Nachrichtenroutings. F&uuml;r Infrastrukturkomponenten der Ebene 2, wie die Transportknoten, gilt genau das Gegenteil. Komponenten der Ebene 3, wie die Vermittlungsstellen, arbeiten nicht im Auftragsverh&auml;ltnis der Teilnehmer und eignen sich daher als einzige f&uuml;r unabh&auml;ngige &Uuml;berwachungsaufgaben. Das macht deutlich, warum eine Delegation an nur eine weitere Ebene, wie sie im 4-Corner-Modell vorgesehen ist, in der Praxis oft nichtausreicht.</p>
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/10.png" alt="" />
<p><strong>Abb. 9: Ein V-Modell des Nachrichtentransports</strong></p>

#### 7.1.4 Nachrichtenaustauschmuster

Im Kapitel "Nachrichtenaustauschmuster" der High-Level-Architektur (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>) werden die unterst&uuml;tzten NOOTS-Nachrichtenaustauschmuster aus der Perspektive von Data Consumer und Data Provider &uuml;ber das Anschlussprotokoll der Anschlussknoten erl&auml;utert. Dabei wird ausschlie&szlig;lich das synchrone Request-Response-Muster f&uuml;r die Abbildung synchroner und asynchroner Nachweisabrufe &uuml;ber NOOTS verwendet. Aufgrund der direkten Unterst&uuml;tzung dieses Musters durch das Transportprotokoll zwischen den Anschlussknoten kann eine durchg&auml;ngige synchrone Kommunikation zwischen Data Consumer und Data Provider gew&auml;hrleistet werden, was auch die Fehlerbehandlung vereinfacht. Es sei darauf hingewiesen, dass das Kommunikationsmodell aktiver Empf&auml;nger des Anschlussprotokolls f&uuml;r Data Provider in diesem Kontext Besonderheiten aufweist, die im Kapitel "<a href="#aktiver-empfänger-initiativumkehr">Aktiver Empf&auml;nger (Initiativumkehr)</a>" erl&auml;utert werden.
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/11.png" alt="" />
<p><strong>Abb. 10: Synchrones Request-Response Muster f&uuml;r Anschluss-und Transportprotokoll</strong></p>
<p>Wenn das Transportprotokoll das Request-Response-Muster nicht unterst&uuml;tzt, kann dieses durch den Austausch zweier separater Nachrichten zwischen dem Data Consumer und dem Data Provider nachgebildet werden. Der Data Consumer oder dessen Anschlussknoten muss die Korrelation dieser Nachrichten anhand vereinbarter Merkmale herstellen und die Verarbeitung der Antwortnachrichten explizit steuern. Dabei besteht grunds&auml;tzlich das Risiko, dass Angriffe auf den Data Consumer erfolgen k&ouml;nnen, indem Antworten auf nicht gestellte Anfragen gesendet werden. Um solche Angriffe zu verhindern, sind entsprechende Gegenma&szlig;nahmen erforderlich. Daher ist dieser Ansatz gangbar, aber nachteilhaft. Insbesondere eDelivery AS4 unterst&uuml;tzt nur unkorrelierte Nachrichten. Erg&auml;nzend sei erw&auml;hnt, dass das h&auml;ufig verwendete Publish-Subscribe-Muster, welches zur Verteilung von Informationen (Publish) an eine Gruppe von Interessenten (Subscribern) dient, im NOOTS-Kontext keine Rolle spielt und daher hier nicht weiter behandelt wird.</p>

### 7.2 Baustein NOOTS Sichere Anschlussknoten (SAK)

Ein Sicherer Anschlussknoten (SAK) ist eine NOOTS-Komponente, die den Anschlussknoten aus dem <a href="#711-abstraktes-architekturmodell">Abstrakten Architekturmodell</a> umsetzt. Der Hauptzweck des Sicheren Anschlussknotens besteht darin, Nachweisanfragen zwischen den NOOTS-Teilnehmern, Data Consumer und Data Provider, zu vermitteln. Die Motivation und der Nutzen eines Sicheren Anschlussknotens ergeben sich aus den Designentscheidungen im Kapitel &bdquo;<a href="#6-lösungsstrategie">L&ouml;sungsstrategie</a>&ldquo;.
<p><strong>Nachnutzung des SAK</strong></p>
<p>Die hier vorgestellte Transportinfrastruktur ist prim&auml;r f&uuml;r den Austausch von Nachweisen zwischen den Sicheren Anschlussknoten der Data Consumer und Data Provider konzipiert. Nach ihrer Implementierung kann sie jedoch, sofern geeignet, auch f&uuml;r andere Kommunikationsbeziehungen im NOOTS und im Rahmen der Registermodernisierung verwendet werden, um von einem einfachen und sicheren Transportmechanismus zu profitieren. Daher kann die Transportinfrastruktur neben dem Nachweisabruf auch die folgenden Kommunikationsbeziehungen unterst&uuml;tzen:</p>
<p><strong>Tab. 8: Kommunikationsbeziehungen f&uuml;r SAK Nachnutzung</strong></p>
<table><colgroup><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Von</th>
<th>Zu</th>
<th>Kontext</th>
<th>Zweck</th>
</tr>
<tr>
<td>Data Consumer</td>
<td>IAM f&uuml;r Beh&ouml;rden</td>
<td>Nachweisabruf</td>
<td>Ermittlung eines Zugriffstokens f&uuml;r den autorisierten Zugriff auf NOOTS-Komponenten.</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Registerdatennavigation</td>
<td>Nachweisabruf</td>
<td>Ermittlung des zust&auml;ndigen Data Provider f&uuml;r einen bestimmten Nachweistyp und seinen Verbindungsparametern</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>IDM f&uuml;r Personen</td>
<td>Nachweisabruf</td>
<td>Ermittlung der IDNr einer nat&uuml;rlichen Person zur Identifikation im Nachweisabruf</td>
</tr>
<tr>
<td>Data Provider</td>
<td>IDA</td>
<td>IDNrG</td>
<td>Gem&auml;&szlig; &sect; 6 Absatz 1 IDNrG sind aufgelistete Register gem&auml;&szlig; Anlage zu &sect; 1 IDNrG verpflichtet, die Basisdaten einschlie&szlig;lich der IDNr aus dem Identit&auml;tsdatenabruf (IDA) in ihren Datenbestand zu &uuml;bernehmen.</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>IDM f&uuml;r Unternehmen</td>
<td>Nachweisabruf</td>
<td>Ermittlung der beWiNr einer juristischen Person zur Identifikation im Nachweisabruf</td>
</tr>
<tr>
<td>Data Provider</td>
<td>Unternehmensbasisdatenregister</td>
<td>UBRegG</td>
<td>Register nach &sect; 4 Absatz 1 und &sect; 5 Absatz 1 des UBRegG d&uuml;rfen die bundeseinheitliche Wirtschaftsnummer f&uuml;r Unternehmen (beWiNr) oder sonstige Datenbest&auml;nde speichern und verwenden, soweit dies f&uuml;r ihre Aufgabenerf&uuml;llung erforderlich ist.</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Vermittlungsstelle</td>
<td>Nachweisabruf</td>
<td>Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung gem&auml;&szlig; &sect; 7 Abs. 2 des IdNrG</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Intermedi&auml;re Plattform</td>
<td>Nachweisabruf</td>
<td>Abruf von Nachweisen f&uuml;r nat&uuml;rliche Personen und Unternehmen aus einem EU-Mitgliedstaat</td>
</tr>
<tr>
<td>Intermedi&auml;re Plattform</td>
<td>Registerdatennavigation</td>
<td>Nachweisabruf</td>
<td>Ermittlung des zust&auml;ndigen Data Provider f&uuml;r einen bestimmten Nachweistyp und seinen Verbindungsparametern</td>
</tr>
<tr>
<td>Intermedi&auml;re Plattform</td>
<td>Data Provider</td>
<td>Nachweisabruf</td>
<td>Abruf von nationalen Nachweisen aus einem EU-Mitgliedstaat</td>
</tr>
<tr>
<td>Datenschutzcockpit</td>
<td>Data Provider</td>
<td>IDNrG</td>
<td>Abruf der Protokolldaten zu zuvor erfolgten Nachweisabrufen unter Verwendung der IDNr gem. &sect; 9 IDNrG</td>
</tr>
</tbody>
</table>
Im Gegensatz zum Nachweisabruf zwischen Data Consumer und Data Provider erfolgt bei den genannten Kommunikationsbeziehungen keine abstrakte Berechtigungspr&uuml;fung gem&auml;&szlig; IDNrG. Das <a href="#73-baustein-noots-transportprotokoll">NOOTS-Transportprotokoll</a> gew&auml;hrleistet f&uuml;r die Kommunikation zwischen den SAKs die gegenseitige Authentifizierung der Kommunikationspartner mittels mutual TLS und Zertifikaten. Abh&auml;ngig von der Kommunikationsbeziehung erfolgt zus&auml;tzlich die Autorisierung des Zugriffs auf eine NOOTS-Komponente &uuml;ber Zugriffstoken des IAM f&uuml;r Beh&ouml;rden. Die Anwendungsf&auml;lle f&uuml;r den Abruf von Registerdaten f&uuml;r den Registerzensus sowie f&uuml;r wissenschaftliche Zwecke sind in der aktuellen Auflistung noch nicht ber&uuml;cksichtigt.

#### 7.2.1 Systemkontext SAK beim Nachweisabruf

Der SAK ist eine dezentrale Komponente des NOOTS gem&auml;&szlig; Ebene 1 des <a href="#713-4-corner-modell-und-v-modell">V-Modells</a>, die den Transport zwischen den NOOTS-Teilnehmern Data Consumer und Data Provider sowie zu allen NOOTS-Komponenten kapselt. Sowohl der Data Consumer als auch der Data Provider kommunizieren ausschlie&szlig;lich mit dem SAK, was bedeutet, dass der Einsatz des SAKs f&uuml;r die NOOTS-Teilnehmer verpflichtend ist. Das Anschlussprotokoll, das als API realisiert ist, bedient alle erforderlichen Interaktionen zwischen Data Consumer und Data Provider, ohne fest an einen bestimmten Transportstandard gebunden zu sein. In dieser Hinsicht fungiert der SAK als stabile Fassade vor dem NOOTS, die die Kommunikation mit anderen Teilnehmersystemen oder den NOOTS-Komponenten delegiert und somit die Komplexit&auml;t der technischen Anbindung an das NOOTS maximal reduziert. Die SAKs haben Zugriff auf die &uuml;bermittelten Daten und unterst&uuml;tzen den Teilnehmer bei der Sicherstellung von Vertraulichkeit, Integrit&auml;t und Authentizit&auml;t beim Transport der Daten. Aufgrund ihrer N&auml;he zum Teilnehmer eignen sie sich als Endpunkt einer Ende-zu-Ende-Verschl&uuml;sselung zwischen den Teilnehmern.
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/12.png" alt="" />
<p><strong>Abb. 11: Systemkontext </strong><strong>SAK beim Nachweisabruf</strong></p>
<p>Im Folgenden werden die Kommunikationsbeziehungen f&uuml;r einen Nachweisabruf zwischen den NOOTS-Teilnehmern und den NOOTS-Komponenten tabellarisch aufgef&uuml;hrt. Der Datenfluss wird im Detail im Kapitel "<a href="#8-laufzeitsicht">Laufzeitsicht</a>" dargestellt.</p>
<p><strong>Tab. 9: Systemkontext <strong>SAK beim Nachweisabruf</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Von SAK des</th>
<th>Zu</th>
<th>Zweck</th>
</tr>
<tr>
<td>Data Consumer</td>
<td>IAM f&uuml;r Beh&ouml;rden</td>
<td>Ermittlung eines Zugriffstokens f&uuml;r den autorisierten Zugriff auf NOOTS-Komponenten.</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Registerdatennavigation</td>
<td>Ermittlung des zust&auml;ndigen Data Provider oder Intermedi&auml;re Plattform f&uuml;r einen bestimmten Nachweistyp und deren Verbindungsparametern</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>IDM f&uuml;r Personen</td>
<td>Ermittlung der IDNr einer nat&uuml;rlichen Person zur Identifikation im Nachweisabruf gem&auml;&szlig; &sect; 6 Abs. 3 IDNrG</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>IDM f&uuml;r Unternehmen</td>
<td>Ermittlung der beWiNr einer juristischen Person zur Identifikation im Nachweisabruf gem&auml;&szlig; &sect; 3 Abs. 1 UBRegG</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Vermittlungsstelle</td>
<td>Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung gem&auml;&szlig; &sect; 7 Abs. 2 IDNrG</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Data Provider</td>
<td>Abruf von nationalen Nachweisen f&uuml;r nat&uuml;rliche Personen und Unternehmen</td>
</tr>
<tr>
<td>Data Consumer</td>
<td>Intermedi&auml;re Plattform</td>
<td>Abruf von Nachweisen f&uuml;r nat&uuml;rliche Personen und Unternehmen aus einem EU-Mitgliedstaat</td>
</tr>
</tbody>
</table>

#### 7.2.2 Software-Architektur

Die Softwarearchitektur des SAK orientiert sich an den Prinzipien der F&uuml;nfzehn-Faktoren-App f&uuml;r Microservices, Container-Ans&auml;tze und Cloud-native Services und ber&uuml;cksichtigt dabei eine Reihe von Rahmenbedingungen:
<ul>
<li>Technologie-Stack: leichtgewichtig, modern, bew&auml;hrt und zukunftsf&auml;hig,</li>
<li>Betrieb: auf weit verbreiteten klassischen Server-Plattformen (Linux und Windows auf x86) sowie in einer DVC-konformen Cloud,</li>
<li>Skalierbarkeit: f&uuml;r unterschiedliche Lastanforderungen, sowohl vertikal als Single-Instance als auch horizontal in Cluster-Umgebungen,</li>
<li>Flexible Integration: mit verschiedenen zentralen Infrastrukturen f&uuml;r &Uuml;berwachung (Protokollierung, Monitoring und verteilte Nachrichtenverfolgung),</li>
<li>Verwaltung: mehrerer NOOTS-Teilnehmer innerhalb eines SAK,</li>
<li>Optionale datenbankbasierte Persistenz: Einfache Betriebsf&auml;higkeit des SAK auch in unsicheren Netzwerkbereichen, ohne Abh&auml;ngigkeiten von Datenbankdiensten.</li>
</ul>
<p>Die Software-Architektur des SAK setzt sich aus verschiedenen funktionalen Services zusammen, wie sie in der folgenden Abbildung veranschaulicht werden. Im Rahmen der technischen Konzeption wird festgelegt, ob und in welchem Umfang die funktionalen Services durch einen einzigen SAK oder durch mehrere SAKs mit einem Funktionsumfang, der auf den jeweiligen Einsatzzweck abgestimmt ist, abgebildet werden.</p>
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/13.png" alt="" />
<p><strong>Abb. 12: &Uuml;bersicht der funktionalen SAK-Services</strong></p>
<p>Im Folgenden wird der Zweck der funktionalenSAK-Services kurz erl&auml;utert.</p>

###### Sicherer Transport

<p>Dieser Service implementiert den API-Endpunkt f&uuml;r das verwendete Transportprotokoll, &uuml;ber das Nachrichten und Daten zwischen den SAKs unter Ber&uuml;cksichtigung der Schutzziele Vertraulichkeit, Integrit&auml;t und Authentizit&auml;t ausgetauscht werden. Die SAKs bilden die jeweiligen Endpunkte der Ende-zu-Ende-Verschl&uuml;sselung. Folgende APIs werden f&uuml;r den Sicheren Transport bereitgestellt:</p>
<ul>
<li>TransportClient-API: Aufruf von Operationen der SAKs der NOOTS-Komponenten</li>
<li>TransportServer-API: Bereitstellung von Operationen der SAKs der NOOTS-Komponenten.</li>
</ul>
<p>&rarr; Weiterf&uuml;hrende Informationen finden sich im Kapitel&nbsp;<a href="#73-baustein-noots-transportprotokoll">Baustein Transportprotokoll</a>.</p>

###### Sicherer Anschluss

<p>Der Sichere Anschluss Service implementiert das synchrone Anschlussprotokoll f&uuml;r den Datenaustausch von Nachweisen und anderen Nachrichten und stellt folgende APIs f&uuml;r die Anbindung an das NOOTS bereit:</p>
<ul>
<li>Consumer-API f&uuml;r den Data Consumer,</li>
<li>Provider-API (passiverEmpf&auml;nger)f&uuml;r den Data Provider und alternativ</li>
<li>Provider-API (aktiver Empf&auml;nger)f&uuml;r den Data Provider.</li>
</ul>
<p>&rarr; Weiterf&uuml;hrende Informationen finden sich im Kapitel&nbsp;<a href="#74-baustein-noots-anschlussprotokoll">Baustein Anschlussprotokoll</a>.</p>

###### Zugriffsberechtigung

<p>Um die Rechtm&auml;&szlig;igkeit der &Uuml;bermittlung zu gew&auml;hrleisten, ist abh&auml;ngig von der Kommunikationsbeziehung der Zugriff auf NOOTS-Komponenten nur mit einem g&uuml;ltigen Zugriffstoken aus dem Identity and Access Management (IAM) f&uuml;r Beh&ouml;rden m&ouml;glich. Der SAK f&uuml;hrt anhand des Zugriffstokens die folgenden Pr&uuml;fungen zur Sicherstellung der G&uuml;ltigkeit durch:</p>
<ol>
<li>
<<em>Vorhanden</em>: Zugriffstoken liegt als "Bearer Token" im HTTP-Header vor.
</li>
<li>
<em>Signatur</em>: Signatur des Zugriffstokens wird mit dem &ouml;ffentlichen Schl&uuml;ssel von IAM f&uuml;r Beh&ouml;rden &uuml;berpr&uuml;ft, um die Unversehrtheit und Herkunft des Tokens sicherzustellen. Dazu muss das Zertifikat von IAM f&uuml;r Beh&ouml;rden vorhanden sein.
</li>
<li>
<em>Inhalte</em>: Ablaufzeit darf nicht &uuml;berschritten sein.
</li>
<li>
<em>Berechtigungen</em>: Berechtigung zum Zugriff auf die NOOTS-Komponente ist vorhanden.
</li>
</ol>
Je nach Ergebnis der Token-Pr&uuml;fung stellt die integrierte Zugriffsberechtigungspr&uuml;fung im SAK sicher, dass:
<ul>
<li><em>beim Senden einer Anfrage</em>: wenn f&uuml;r die Ausf&uuml;hrung einer API-Operation im SAK ein g&uuml;ltiges Zugriffstoken erforderlich ist und kein g&uuml;ltiges Zugriffstoken vorliegt, wird keine Anfrage an eine NOOTS-Komponente oder Teilnehmer gesendet, einschlie&szlig;lich einer Nachweisanfrage an den SAK des Data Providers.</li>
<li><em>beim Empfangen einer Anfrage</em>: wenn f&uuml;r denEmpfang einer Anfrage ein g&uuml;ltiges Zugriffstoken erforderlich ist und kein g&uuml;ltiges Zugriffstoken vorliegt, wird die Anfrage abgelehnt.</li>
</ul>
<p>&rarr; Weitere Details zu den Zugriffstoken sind im AD-NOOTS Konzept IAM f&uuml;r Beh&ouml;rden (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-05</a>) zu finden, w&auml;hrend der Datenfluss zwischen den SAKs und den NOOTS-Komponenten im <a href="#8-laufzeitsicht">Kapitel Laufzeitsicht</a>&nbsp;behandelt wird.</p>

###### Abstrakte Berechtigung

<p>Um die abstrakte Rechtm&auml;&szlig;igkeit der &Uuml;bermittlung bei Nutzung einer Identifikationsnummer im Nachweisabruf gem&auml;&szlig; &sect; 7 (2) und &sect; 12 (4) IDNrG sicherzustellen, ist im SAK die Pr&uuml;fung der abstrakten Berechtigung auf Basis eines Abruftokens f&uuml;r die NOOTS-Teilnehmer, Data Consumer und Data Provider integriert:</p>
<ul>
<li><em>SAK des Data Consumers:</em><br/>
Der SAK fordert f&uuml;r die abstrakte Berechtigung gem&auml;&szlig; IDNrG ein Abruftoken von der NOOTS-Komponente &bdquo;Vermittlungsstelle&ldquo; an. Bei erfolgreicher Ausstellung wird das Abruftoken in die XNachweis-Anfrage integriert, und die erweiterte XNachweis-Anfrage wird an den SAK des Data Providers gesendet. Sollte das Abruftoken nicht ausgestellt werden k&ouml;nnen, bricht der SAK den Nachweisabruf mit einer Fehlermeldung ab.<br/>
Anmerkung: Der SAK des Data Consumers fordert auch bei Nachweisabrufen ohne Identifikationsnummer ein Abruftoken von der NOOTS-Komponente &bdquo;Vermittlungsstelle&ldquo; an.<em><br /></em></li>
<li><em>SAK des Data Providers:</em><br/>
Der SAK &uuml;berpr&uuml;ft bei jeder eingehenden Nachweisanfrage, unabh&auml;ngig davon, ob eine Identifikationsnummer im Nachweisabruf enthalten ist, die G&uuml;ltigkeit des Abruftokens sowie die &Uuml;bereinstimmung seiner Daten mit dem Inhalt der Nachweisanfrage. Bei erfolgreicher &Uuml;berpr&uuml;fung leitet der SAK die Nachweisanfrage an den Data Provider weiter; andernfalls wird sie abgelehnt.</li>
<li><em>SAK der Intermedi&auml;ren Plattform (IP)</em><br/>
Der SAK in der Rolle als Data Consumer behandelt Nachweisabrufe von Data Providers analog zu Nachweisabrufen ohne Identifikationsnummer des SAK des Data Consumers. Bei der IP in der Rolle als Data Provider verl&auml;uft der Abruf von EU-Nachweisen identisch zum SAK des Data Providers.</li>
</ul>
<p>Im SAK des Data Consumers und Data Providers werden folgende Pr&uuml;fungen anhand des Abruftokens durchgef&uuml;hrt:</p>
<ol>
<li><em>Vorhanden:</em> Abruftoken ist in der XNachweis-Anfrage vorhanden.</li>
<li><em>Signatur:</em> Signatur wird mit dem &ouml;ffentlichen Schl&uuml;ssel der Vermittlungsstelle &uuml;berpr&uuml;ft, um die Unversehrtheit und Herkunft des Tokens sicherzustellen. Dazu muss das Zertifikat der Vermittlungsstelle vorhanden sein.</li>
<li><em>Inhalte:</em> Ablaufzeit darf nicht &uuml;berschritten sein, und es muss ein Datenabgleich aller Inhalte des Abruftokens mit den Inhalten der XNachweis-Anfrage durchgef&uuml;hrt werden (siehe auch Konzept Vermittlungsstelle (<a  href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-08</a>): Kapitel "Abruftoken")</li>
<li><em>Hash-Wert:</em> Hash-Wert f&uuml;r die XNachweis-Anfrage wird berechnet und mit dem Hashwert aus dem Abruftoken verglichen. Falls der Vergleich negativ ausf&auml;llt, ist das Token ung&uuml;ltig.</li>
</ol>
<p>&rarr; Zus&auml;tzliche Details zur abstrakten Berechtigungspr&uuml;fung sind im AD-NOOTS Konzept Vermittlungsstelle (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-08</a>) verf&uuml;gbar.</p>

###### Validierung

<p>Der SAK &uuml;berpr&uuml;ft die Konformit&auml;t der XNachweis-Anfrage und der Antwort mit den unterst&uuml;tzten Versionen der XNachweis-Spezifikation sowie das Vorhandensein aller erforderlichen Informationen. Dar&uuml;ber hinaus werden Datenkonsistenzpr&uuml;fungen zwischen den gesicherten Daten im Zugriffstoken aus dem IAM f&uuml;r Beh&ouml;rden und dem Abruftoken aus der Vermittlungsstelle durchgef&uuml;hrt.</p>
<p>&rarr; Weiterf&uuml;hrende Informationen finden sich in derXNachweis-Spezifikation (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">XS-01</a>).</p>

###### Management

<p>Im SAK m&uuml;ssen die Konfigurationsdaten eines oder mehrerer Mandanten verwaltet werden. Mandanten sind hierbei die NOOTS-Komponenten oder NOOTS-Teilnehmer (Data Consumer und Data Provider). Die Konfigurationsdaten eines Mandanten umfassen Daten f&uuml;r das Anschlussprotokoll und das Transportprotokoll, die Verwaltung von Zertifikaten sowie Konfigurationen f&uuml;r die Protokollierung, das Monitoring und die verteilte Nachrichtenverfolgung.</p>

###### Routing

<p>Der SAK fungiert als Vermittler f&uuml;r den Transport zwischen den NOOTS-Teilnehmern Data Consumer und Data Provider sowie allen anderen NOOTS-Komponenten. Das bedeutet, dass alle direkten Aufrufe zu NOOTS-Komponenten vom SAK initiiert werden.</p>
<p><strong>Tab. 10: Weiterleitung in den SAKs</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>SAK des</th>
<th>Weiterleitung nach</th>
<th>Zweck</th>
</tr>
<tr>
<td rowspan=6>Data Consumer</td>
<td>
SAK f&uuml;r IDM f&uuml;r Personen
</td>
<td>
<ul>
<li>Abruf der IDNr und Basisdaten</li>
</ul>
</td>
</tr>
<tr>
<td>SAK f&uuml;r IDM f&uuml;r Unternehmen</td>
<td>
<ul>
<li>Abruf der beWiNr und Basisdaten</li>
</ul>
</td>
</tr>
<tr>
<td>SAK f&uuml;r Vermittlungsstelle</td>
<td>
<ul>
<li>Abruf des Abruftokens f&uuml;r die abstrakte Berechtigungspr&uuml;fung</li>
</ul>
</td>
</tr>
<tr>
<td>SAK f&uuml;r Registerdatennavigation</td>
<td>
<ul>
<li>Anforderung des fachlich zust&auml;ndigen Data Providers</li>
<li>Abruf der technischen Verbindungsparameter eines Data Providers</li>
</ul>
</td>
</tr>
<tr>
<td>SAK f&uuml;r Data Provider</td>
<td>
<ul>
<li>Abruf eines Nachweises von einem nationalen Register</li>
</ul>
</td>
</tr>
<tr>
<td>SAK f&uuml;r Intermedi&auml;re Plattform</td>
<td>
<ul>
<li>Ermittlung der zust&auml;ndigen IP</li>
<li>Abruf eines Nachweises aus einem EU-Mitgliedstaat via IP</li>
</ul>
</td>
</tr>
<tr>
<td rowspan=3>Intermedi&auml;re Plattform</td>
<td>
SAK f&uuml;r Registerdatennavigation
</td>
<td>
<ul>
<li>Anforderung des fachlich zust&auml;ndigen Data Providers</li>
<li>Abruf der technischen Verbindungsparameter eines Data Providers</li>
</ul>
</td>
</tr>
<tr>
<td>
SAK f&uuml;r Vermittlungsstelle
</td>
<td>
<ul>
<li>Abruf des Abruftokens</li>
</ul>
</td>
</tr>
<tr>
<td>SAK f&uuml;r Data Provider</td>
<td>
<ul>
<li>Abruf eines Nachweises von einem nationalen Register</li>
</ul>
</td>
</tr>
<tr>
<td>Data Provider</td>
<td>Data Provider</td>
<td>
<ul>
<li>Lieferung eines Nachweises via Kommunikationsmodell passiver Empf&auml;nger zwischen SAK und Data Provider</li>
</ul>
</td>
</tr>
</tbody>
</table>
Die SAKs der anderen NOOTS-Komponenten leiten die entsprechenden Anfragen direkt an die jeweilige NOOTS-Komponente weiter.
<p>Derzeit bestehen keine Anforderungen an eine weiterf&uuml;hrende Routing-Logik. Zum Beispiel kann f&uuml;r einen Mandanten im SAK im Kommunikationsmodell passiver Empf&auml;nger die Endpunkt-URL f&uuml;r die Provider-API des Anschlussprotokolls konfiguriert werden, an die die Nachweisanfrage zugestellt wird. Ein Routing an verschiedene Endpunkt-URLs basierend auf Daten aus der Nachweisanfrage ist derzeit nicht vorgesehen.</p>
<p>&rarr; Zus&auml;tzliche Informationen zum Anschlussprotokoll sind im Kapitel <a href="#74-baustein-noots-anschlussprotokoll">Baustein Anschlussprotokoll</a> zu finden, w&auml;hrend der Datenfluss zwischen den SAKs und den NOOTS-Komponenten im Kapitel <a href="#8-laufzeitsicht">Kapitel Laufzeitsicht</a> behandelt wird.</p>

###### Transformation

<p>Angesichts der hohen Anzahl von NOOTS-Teilnehmern ist es entscheidend, dass das Anschlussprotokoll nicht nur interoperabel, sondern auch robust gegen&uuml;ber Ver&auml;nderungen ist. Es soll m&ouml;glichst unempfindlich gegen&uuml;ber &Auml;nderungen in der Transportinfrastruktur oder bei NOOTS-Komponenten sein. Der SAK wandelt die Daten aus dem Anschlussprotokoll in API-Aufrufe der Ziel-NOOTS-Komponenten um und erg&auml;nzt sie gegebenenfalls vor dem Aufruf einer NOOTS-Komponente mit eigenen Konfigurationsdaten oder ermittelten Informationen wie beispielsweise dem Abruftoken.</p>

###### Teil-Orchestrierung

<p>Die Hauptaufgabe des SAK besteht <strong>nicht </strong>in der Orchestrierung der verschiedenen Interaktionen mit den NOOTS-Komponenten w&auml;hrend eines Nachweisabrufs durch den Data Consumer. In diesem Punkt unterscheidet sich der SAK von Adapterimplementierungen, die die Koordination einzelner Interaktionsschritte &uuml;bernehmen. Der SAK verf&uuml;gt lediglich &uuml;ber integrierte obligatorische Funktionen zur Gew&auml;hrleistung der Rechtm&auml;&szlig;igkeit der &Uuml;bermittlung und zur Kapselung des Transportprotokolls. Diese umfassen:</p>
<ul>
<li>die &Uuml;berpr&uuml;fung der abstrakten Berechtigung (siehe <a href="#abstrakte-berechtigung">SAK-Service Abstrakte Berechtigung</a>) und</li>
<li>
die Ermittlung der technischen Verbindungsparameter des NOOTS-Dienstes, wie etwa des Data Providers, anhand einer ServiceID. Die NOOTS-Teilnehmer ben&ouml;tigen lediglich die ServiceID des NOOTS-Dienstes. Der SAK setzt diese ServiceID in konkrete Verbindungsparameter des Transportprotokolls um. Dieser Mechanismus erm&ouml;glicht &Auml;nderungen an den Verbindungsparametern ohne Auswirkungen auf die NOOTS-Teilnehmer und verhindert eine fehlerhafte oder missbr&auml;uchliche Nutzung der Verbindungsparameter durch die NOOTS-Teilnehmer, wodurch die Kapselung des Transportprotokolls unterst&uuml;tzt wird.
</li>
</ul>
<p>&rarr; Weiterf&uuml;hrende Informationen finden sich im Kapitel Laufzeitsicht im Kapitel "<a href="#824-schritt-4-consumer-apigetnachweis">8.2.4 Schritt 4: Consumer-API::getNachweis</a>".</p>

###### Monitoring

<p>Der Monitoring-Service bietet Funktionen zur &Uuml;berwachung und Verf&uuml;gbarkeitspr&uuml;fung des Systemzustands des SAK. Die automatische Verf&uuml;gbarkeitspr&uuml;fung kann &uuml;ber folgende "Health-Checks" erfolgen:</p>
<ul>
<li>"Liveness Probe": &uuml;berpr&uuml;ft, ob der SAK betriebsbereit ist,</li>
<li>"Readiness Probe": stellt fest, ob der SAK bereit ist, Anfragen anzunehmen.</li>
</ul>
<p>Der Systemzustand des SAK kann &uuml;ber den Endpunkt der Metrik-API abgerufen werden und l&auml;sst sich mit g&auml;ngigen zentralen Monitoring-Infrastrukturen wie Prometheus/ Grafana visualisieren. &Uuml;ber die Metrik-API ist ebenfalls die installierte SAK-Version abrufbar.</p>

###### Verteilte Nachrichtenverfolgung

<p>In der verteilten f&ouml;deralen IT-Landschaft des NOOTS werden Nachweis- und Serviceanfragen nicht nur von einer NOOTS-Komponente beantwortet, sondern von verschiedenen verteilten NOOTS-Komponenten bearbeitet. H&auml;ufig sind auch Third-Party-Komponenten Bestandteil der Verarbeitungskette. Um das Verhalten der Anfragen &uuml;ber Komponentengrenzen hinweg nachverfolgen zu k&ouml;nnen, ist eine verteilte Nachrichtenverfolgung auf Anwendungsebene auf Basis eines anerkannten offenen Standards erforderlich.</p>
<p>&rarr; siehe auch <a href="#anhang-b---nachrichtenverfolgung-mit-opentelemetry">"Anhang B - Nachrichtenverfolgung mit OpenTelemetry"</a></p>

###### Protokollierung

<p>Die effiziente Lokalisierung und Analyse von Fehlersituationen im verteilten f&ouml;deralen NOOTS-&Ouml;kosystem und mit seiner Integration in das EU-OOTS der EU-Mitgliedstaaten stellen eine bedeutende Herausforderung dar. Hierbei kommt dem SAK als Vermittler s&auml;mtlicher Kommunikationsbeziehungen im NOOTS eine Schl&uuml;sselrolle zu. Daher ist es notwendig, dass der SAK nicht nur technische Anfragen protokolliert, sondern auch ein Audit-Protokoll f&uuml;r die Erfassung von fachlichen Daten und &Auml;nderungen am Systemstatus oder der Konfiguration f&uuml;hrt. Die Protokollinformationen m&uuml;ssen dabei effizient anhand von Suchparametern von berechtigten Personen analysiert werden k&ouml;nnen.</p>
<p>Die Speicherung der Protokollinformationen kann differenziert und flexibel &uuml;ber eine Konfiguration im SAK gesteuert werden, zum Beispiel durch die Auswahl von Datei, Datenbank oder Konsole f&uuml;r Container-Umgebungen. Optional besteht die M&ouml;glichkeit, Datenbanken f&uuml;r die Speicherung von Informationen des Audit-Protokolls einzubinden. In der Praxis haben sich zentrale Loganalyse-Plattformen wie Loki/ Grafana oder Elasticsearch, Fluentd und Kibana (EFK) bew&auml;hrt, um Protokollinformationen zu analysieren.</p>
<p><em>Technisches Protokoll</em></p>
<p>Der SAK protokolliert bei jeder eingehenden Anfrage Informationen zur Anfrage, zur Antwort oder zur Fehlerursache. Dar&uuml;ber hinaus werden auch Metainformationen wie der Schweregrad, Zeitpunkt der Anfrage und die Dauer der Bearbeitung der Anfrage protokolliert. Das technische Protokoll enth&auml;lt keine personenbezogenen oder personenbeziehbaren Daten gem&auml;&szlig; DSGVO.</p>
<p><em>Audit-Protokoll</em></p>
<p>Das Audit-Protokoll protokolliert fachlich- und sicherheitsrelevante Ereignisse, wie beispielsweise Token-Anforderungen, abgelehnte Token-Ausstellungen oder Nachweisabrufe mit ung&uuml;ltigem Token. Dabei werden keinerlei personenbezogene Daten im Audit-Protokoll gespeichert. Die Eintr&auml;ge des Audit-Protokolls m&uuml;ssen f&uuml;r einen Zeitraum von zwei Jahren aufbewahrt und anschlie&szlig;end gel&ouml;scht werden.</p>
<table>
<tbody>
<tr>
<td>
<strong>Fachliche Protokollierung durch Vermittlungsstelle und Data Provider</strong>
<p>Jeder Nachweisabruf, unabh&auml;ngig davon, ob eine Identifikationsnummer enthalten ist oder nicht, und die damit verbundene abstrakte Berechtigungsanfrage an die Vermittlungsstelle (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-08</a>), wird von dieser mit den zugrundeliegenden Datengrundlagen und dem Pr&uuml;fungsergebnis protokolliert. Zus&auml;tzlich ist eine fachliche Protokollierung der Nachweisabrufe durch den Data Provider erforderlich, sowohl bei nat&uuml;rlichen Personen mit einer IDNr gem&auml;&szlig; &sect; 9 IDNrG als auch bei Unternehmen im Sinne des &sect; 3 Abs. 1 UBRegG gem&auml;&szlig; &sect; 7 UBRegG (siehe auch Anschlusskonzept f&uuml;r Data Provider (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-02</a>): Abschnitt "Fachliche Protokollierung"). Durch den SAK erfolgt keine fachliche Protokollierung der Nachweisabrufe.</p>
</td>
</tr>
</tbody>
</table>

#### 7.2.3 &Uuml;bersicht der SAK-APIs

In der folgenden Tabelle werden alle APIs des SAK aufgelistet. Die Nutzung der Consumer-API und Provider-API wird im Kapitel <a href="#8-laufzeitsicht">&bdquo;Laufzeitsicht&ldquo;</a> am Beispiel im Detail beschrieben.
<p><strong>Tab. 11: &Uuml;bersicht der SAK-APIs</strong></p>
<table><colgroup><col /><col /><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th></th>
<th>API</th>
<th>Kontext</th>
<th>Beschreibung</th>
<th>API-Typ</th>
<th>Protokoll</th>
</tr>
<tr>
<td>1</td>
<td>Consumer-API</td>
<td>
Anwendungsprotokoll
</td>
<td>
SAK stellt eine API zur Verf&uuml;gung, die vom Data Consumer aufgerufen werden kann, um einen Nachweisabruf &uuml;ber das NOOTS an den Data Provider oder &uuml;ber die IP im EU-Mitgliedstaat durchzuf&uuml;hren. Weitere Einzelheiten zur API sind im Baustein "<a href="#74-baustein-noots-anschlussprotokoll">Anschlussprotokoll</a>" beschrieben.
</td>
<td>
&Ouml;ffentlich
</td>
<td>
REST / HTTP
</td>
</tr>
<tr>
<td>2</td>
<td>Provider-API (passiver Empf&auml;nger)</td>
<td>
Anwendungsprotokoll
</td>
<td>
SAK definiert eine API als Service Provider Interface (SPI), die f&uuml;r das Kommunikationsmodell "passiver Empf&auml;nger" vom Data Provider implementiert und bereitgestellt werden muss. Der SAK leitet Nachweis-Anfragen &uuml;ber die SPI an den Data Provider weiter. Weitere Einzelheiten zur API sind im Baustein "<a href="#74-baustein-noots-anschlussprotokoll">Anschlussprotokoll</a>" beschrieben.
</td>
<td>
&Ouml;ffentlich
</td>
<td>
REST / HTTP
</td>
</tr>
<tr>
<td>3</td>
<td>Provider-API (aktiver Empf&auml;nger)</td>
<td>
Anwendungsprotokoll
</td>
<td>
SAK bietet eine API f&uuml;r das Kommunikationsmodell "aktiver Empf&auml;nger" an, die vom Data Provider aufgerufen werden kann, um auf Basis einer Nachweis-Anfrage Nachweisdaten &uuml;ber das NOOTS an den Data Consumer zu liefern. Weitere Einzelheiten zur API sind im Baustein "<a href="#74-baustein-noots-anschlussprotokoll">Anschlussprotokoll</a>" beschrieben.
</td>
<td>
&Ouml;ffentlich
</td>
<td>
REST / HTTP
</td>
</tr>
<tr>
<td>4</td>
<td>TransportClient-API</td>
<td>
Transportprotokoll
</td>
<td>
Ein SAK nutzt die Client-API, um Operationen anderer SAKs &uuml;ber das im "<a href="#73-baustein-noots-transportprotokoll">Transportprotokoll</a>" spezifizierte sichere Transportverfahren aufzurufen. Diese Client-API wird ausschlie&szlig;lich f&uuml;r den internen NOOTS-Transport zwischen SAKs verwendet.
</td>
<td>
Intern
</td>
<td>
REST / HTTP
</td>
</tr>
<tr>
<td>5</td>
<td>TransportServer-API</td>
<td>
Transportprotokoll
</td>
<td>
&Uuml;ber die Server-API stellt ein SAK Operationen bereit, die von einem anderen SAK unter Verwendung des im "<a href="#73-baustein-noots-transportprotokoll">Transportprotokoll</a>" spezifizierten sicheren Transportverfahrens aufgerufen werden k&ouml;nnen. Diese Server-API wird ausschlie&szlig;lich f&uuml;r den internen NOOTS-Transport zwischen SAKs verwendet.
</td>
<td>
Intern
</td>
<td>
REST / HTTP
</td>
</tr>
<tr>
<td>6</td>
<td>Health-Check-API</td>
<td>Monitoring</td>
<td>API zur Bereitstellung von Endpunkten f&uuml;r "Health-Checks" zur &Uuml;berpr&uuml;fung der Verf&uuml;gbarkeit</td>
<td>Admin</td>
<td>REST / HTTP</td>
</tr>
<tr>
<td>7</td>
<td>Metrik-API</td>
<td>Monitoring</td>
<td>API zur Bereitstellung von Informationen &uuml;ber den Systemzustand des SAK</td>
<td>Admin</td>
<td>REST / HTTP</td>
</tr>
<tr>
<td>8</td>
<td>Protokollierung-API</td>
<td>Monitoring</td>
<td>API f&uuml;r die technische und fachliche Protokollierung von Nachrichten und Fehlersituationen</td>
<td>Admin</td>
<td>Datei/ Console/ JDBC(DB)</td>
</tr>
<tr>
<td>9</td>
<td>Tracing-API (optional)</td>
<td>Monitoring</td>
<td>API f&uuml;r die system&uuml;bergreifende Nachverfolgbarkeit von Nachweisanfragen</td>
<td>Admin</td>
<td>REST / HTTP</td>
</tr>
</tbody>
</table>
<p><strong>Legende A</strong><strong>PI-Typ</strong>:</p>
<ul>
<li><em>&Ouml;ffentlich:</em> Die API ist &ouml;ffentlich zug&auml;nglich f&uuml;r die NOOTS-Teilnehmer und kann von diesen verwendet werden.</li>
<li><em>Intern:</em> Interne APIs dienen ausschlie&szlig;lich der Kommunikation zwischen NOOTS-Komponenten wie den SAKs und sind daher f&uuml;r NOOTS-Teilnehmer nicht zug&auml;nglich.</li>
<li><em>Admin:</em> Die API ist ausschlie&szlig;lich f&uuml;r die Administration und &Uuml;berwachung des SAK vorgesehen.</li>
</ul>

#### 7.2.4 &Uuml;berblick Zertifikate f&uuml;r den SAK

<p>Elektronische Zertifikate im SAK erf&uuml;llen mehrere Funktionen, darunter die Ende-zu-Ende-Verschl&uuml;sselung und die Authentifizierung des SAK als Kommunikationspartner. Die Anforderungen an diese Zertifikate richten sich nach den folgenden BSI-Richtlinien:</p>
<ul>
<li>
<em>TR-02103: X.509 Zertifikate und Zertifizierungspfadvalidierung</em>
<ul>
<li>Kapitel "4 TLS Server Zertifikate"</li>
<li>Kapitel "5 TLS Client Zertifikate"</li>
</ul>
</li>
<li>
<em>TR-03107-1: Elektronische Identit&auml;ten und Vertrauensdienste im E-Government</em>
<ul>
<li>Kapitel "10.6 TLS-Verbindung und -Zertifikate"</li>
</ul>
</li>
</ul>
<p>Die folgende Tabelle f&uuml;hrt die verpflichtenden und optionalen Zertifikate auf.</p>
<p><strong>Tab. 12: Relevante Zertifikate f&uuml;r SAK und NOOTS-Teilnehmer</strong></p>
<table><colgroup><col/><col/><col/><col/><col/></colgroup>
<tbody>
<tr>
<th>Zertifikat</th>
<th>
Verwendungszweckin OSI-Schicht 5: Sitzung (TLS)
</th>
<th>Beschreibung</th>
<th>Verwendung in API</th>
<th>Pflicht / Optional</th>
</tr>
<tr>
<td>
TLS Server Zertifikat
</td>
<td>
Authentifizierung, Verschl&uuml;sselung und Siegelung
</td>
<td>
SAK als API-Anbieter f&uuml;r das Transportprotokoll
</td>
<td>
TransportServer-API
</td>
<td>
Pflicht
</td>
</tr>
<tr>
<td></td>
<td></td>
<td>
SAK des Data Consumers als API-Anbieter f&uuml;r das Anschlussprotokoll
</td>
<td>
Consumer-API
</td>
<td>
Pflicht
</td>
</tr>
<tr>
<td></td>
<td></td>
<td>
Data Provider als API-Anbieter f&uuml;r das Anschlussprotokoll
</td>
<td>
Provider-API (passiver Empf&auml;nger)
</td>
<td>
Pflicht
</td>
</tr>
<tr>
<td>
TLS Client Zertifikat
</td>
<td>
Authentisierung (mTLS)
</td>
<td>
SAK als API-Nutzer f&uuml;r das Transportprotokoll
</td>
<td>
TransportClient-API
</td>
<td>
Pflicht
</td>
</tr>
<tr>
<td></td>
<td></td>
<td>
Data Consumer als API-Nutzer f&uuml;r das Anschlussprotokoll<br/>Alternative Authentifizierungsmethode: BasicAuth (f&uuml;r Produktion nicht empfohlen)
</td>
<td>
Consumer-API
</td>
<td>
Optional
</td>
</tr>
<tr>
<td></td>
<td></td>
<td>
Data Provider als API-Nutzer f&uuml;r das Anschlussprotokoll<br/>Alternative Authentifizierungsmethode: BasicAuth (in Produktion nicht empfohlen)
</td>
<td>
Provider-API (aktiver Empf&auml;nger)
</td>
<td>
Optional
</td>
</tr>
<tr>
<td>
TLS Client Zertifikat
</td>
<td>
Authentifizierung(mTLS)<br/><br/>
</td>
<td>
SAK des Data Consumers als API-Nutzer f&uuml;r die Authentifizierungmittels der Komponenten-ID und Zertifikat seiner betriebsverantwortlichen Stelle bei IAM f&uuml;r Beh&ouml;rden
<td>Ermittlung Zugriffstoken aus IAM f&uuml;r Beh&ouml;rden (OAuth2)</td>
<td>
Pflicht
</td>
</tr>
</tbody>
</table>

#### 7.2.5 Deployment-Sicht

NOOTS stellt die Software f&uuml;r den Sicheren Anschlussknoten bereit, der dezentral und in der N&auml;he des NOOTS-Teilnehmers oder der NOOTS-Komponente betrieben werden muss. Zur &Uuml;berwachung des Systemzustandes der SAK-Instanzen kann die Health-Check-API der SAK-Monitoring-Komponente verwendet werden. Der SAK kann in verschiedenen Deployment-Szenarien eingesetzt werden.

##### 7.2.5.1 Standalone Deployment

In Szenarien, in denen die Verf&uuml;gbarkeit keine kritische Rolle spielt, wie beispielsweise in Entwicklungs- und Testumgebungen, kann eine einzelne SAK-Instanz eingesetzt werden. Diese wird entweder als ausf&uuml;hrbare Java-Archive (JAR) Anwendung f&uuml;rklassische Server-Plattformen oder alternativ als Container (Docker-Image) f&uuml;r eine DVC-konformen Containerumgebung bereitgestellt.
<p><strong>Klassische Server-Plattformen</strong></p>
<p>Das Deployment der SAK-Anwendung erfolgt auf weit verbreiteten Server-Plattformen auf Basis von Linux und Windows mit einer vorinstallierten Java-Runtime. Die SAK-Anwendung kann entweder direkt als ausf&uuml;hrbares Java-Archive (JAR) oder alternativ eingebettet in einem Applikationsserver (Tomcat) betrieben werden.</p>
<p><strong>DVC-konforme Containerumgebung</strong></p>
<p>Das Deployment der SAK-Anwendung in einer DVC-konformen Containerumgebung erfolgt mithilfe des bereitgestellten Containers (Docker-Image) und eines Helm-Charts.</p>

##### 7.2.5.2 Redundantes Deployment

Die Verf&uuml;gbarkeit des SAK kann durch den Einsatz von Redundanz erh&ouml;ht werden. Mithilfe einer Lastverteilungskomponente (Loadbalancer) auf TCP-Ebene k&ouml;nnen mehrere SAK-Instanzen parallel betrieben werden. Zur Sicherstellung der Ende-zu-Ende-Verschl&uuml;sselung auf OSI-Schicht 5: Sitzungsschichtdarf die TLS-Verbindung nicht an der Lastverteilungskomponente terminiert werden. H&auml;ufig verf&uuml;gen Lastverteilungskomponenten &uuml;ber einen "Rate Limiting"-Mechanismus, um die Anzahl gleichzeitiger Anfragen zu begrenzen. Mithilfe dieser Funktion kann die Komponente die Anzahl der Anfragen entsprechend den festgelegten Limits &uuml;berwachen und steuern, wodurch &Uuml;berlastungen vermieden werden.
<p><strong>Klassische Server-Plattformen</strong></p>
<p>F&uuml;r die SAK-Anwendung wird keine Lastverteilungskomponente durch NOOTS bereitgestellt. Es k&ouml;nnen jedoch beliebige Lastverteilungskomponenten wie beispielsweise HAProxy, NGINX oder Hardware-Loadbalancer eingesetzt werden, die eine Lastverteilung auf OSI-Schicht 5: Sitzungsschicht unterst&uuml;tzen.</p>
<p><strong>DVC-konforme Containerumgebung</strong></p>
<p>In einer DVC-konformen Containerumgebung (Kubernetes) fungieren die sogenannten "IngressController" als Gateway zwischen der Au&szlig;enwelt und dem SAK-Service. Jeglicher eingehende Datenverkehr wird &uuml;ber den "IngressController" geleitet und an die SAK-Instanzen im Kubernetes-Cluster weitergeleitet. Die SAK-Instanzen im Kubernetes-Cluster k&ouml;nnen von der Au&szlig;enwelt nicht direkt aufgerufen werden.</p>

### 7.3. Baustein NOOTS-Transportprotokoll

Da der Datenaustausch der NOOTS-Teilnehmer obligatorisch &uuml;ber deren dezentrale SAKs erfolgt, ist das verwendete Transportprotokoll f&uuml;r die Teilnehmer vollst&auml;ndig transparent. Ein sp&auml;terer Wechsel des Transportprotokolls ist somit ohne &Auml;nderungen an den Implementierungen der Teilnehmer m&ouml;glich. F&uuml;r Transportprotokollstandards bei interaktiver Nutzung gelten insbesondere die folgenden Anforderungen:
<ol>
<li>
Der Transportstandard muss ein synchrones Request-Response-Nachrichtenaustauschmuster (Message-Exchange-Pattern, MEP) unterst&uuml;tzen, d.h. der Nachweis in der Antwort des Data Providers wird &uuml;ber den HTTP-Response-Kanal transportiert. Zur Erzielung niedriger Antwortzeiten, hoher Zuverl&auml;ssigkeit und geringer Betriebskomplexit&auml;t soll auf Nachrichtenaustauschmuster, die auf asynchronem Messaging basieren ("Two Way / push &amp; push"), verzichtet werden.
</li>
<li>Der Transportstandard muss Mechanismen bereitstellen, auf die sich die Implementierung des SAK zur Gew&auml;hrleistung der Authentizit&auml;t, Vertraulichkeit und Integrit&auml;t st&uuml;tzen kann. Eine Ende-zu-Ende-Verschl&uuml;sselung mit den SAK-Instanzen als den jeweiligen Endpunkten muss m&ouml;glich sein.</li>
</ol>
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/14.png" alt="" />
<p><strong>Abb. 13: Entkopplung des Transportprotokolls mittels adaptierter, externer Protokollschnittstellen im SAK</strong></p>
<p>F&uuml;r die interaktive Nutzung hat sich der Einsatz des HTTP-Protokolls zur technischen Umsetzung eines synchronen Request-Response-Nachrichtenaustauschmusters bew&auml;hrt. Beispiele hierf&uuml;r sind <a href="#x-road">X-Road</a>, das in mehreren nordischen L&auml;ndern f&uuml;r den sicheren Datenaustausch in der &ouml;ffentlichen Verwaltung genutzt wird, und das NextGenPSD2 API Framework der Berlin Group, eine paneurop&auml;ische Initiative zur Schaffung von Interoperabilit&auml;tsstandards und zur Harmonisierung im Zahlungsverkehr.</p>

#### 7.3.1 NOOTS-Transportprotokoll: synchrones HTTP Request-Response mit mTLS</strong>

Die SAKs kommunizieren &uuml;ber das HTTP-Protokoll, das auf der Anwendungsschicht (Schicht 7) des OSI-Schichtenmodells liegt und durch Transport Layer Security (TLS) abgesichert wird. TLS ist ein kryptografisches Protokoll, das auf der Sitzungsschicht (Schicht 5) des OSI-Schichtenmodells operiert und die Sicherheit der Netzwerkkommunikation gew&auml;hrleistet. Im Folgenden werden die wesentlichen Aspekte der Umsetzung von Authentizit&auml;t, Vertraulichkeit und Integrit&auml;t durch mTLS, eine Erweiterung des TLS-Protokolls, unter Ber&uuml;cksichtigung der Richtlinien des Bundesamts f&uuml;r Sicherheit in der Informationstechnik (BSI) f&uuml;r TLS betrachtet. Die notwendigen Zertifikate sind im Kapitel "<a href="#724-überblick-zertifikate-für-den-sak">7.2.4 &Uuml;berblick Zertifikate f&uuml;r den SAK</a>" aufgef&uuml;hrt.
<ul>
<li><em>Authentizit&auml;t</em>: <br/>
Mit dem Einsatz von mTLS m&uuml;ssen sowohl der SAK des Data Providers als auch der SAK des Data Consumers ein g&uuml;ltiges und vertrauensw&uuml;rdiges Zertifikat vorweisen. Die gegenseitige Authentifizierung erfolgt durch den Austausch von Zertifikaten. Dabei sendet der SAK des Data Providers sein Zertifikat an den SAK des Data Consumers, um seine Identit&auml;t zu best&auml;tigen, und umgekehrt sendet der SAK des Data Consumers sein Zertifikat an den SAK des Data Providers, um ebenfalls seine Identit&auml;t zu best&auml;tigen. Beide Kommunikationspartner &uuml;berpr&uuml;fen die G&uuml;ltigkeit der erhaltenen Zertifikate, um sicherzustellen, dass sie von einer vertrauensw&uuml;rdigen Zertifizierungsstelle ausgestellt wurden und weder abgelaufen noch widerrufen sind. Zus&auml;tzlich m&uuml;ssen beide Kommunikationspartner &uuml;ber kryptografische Funktionen den Besitz des zum Zertifikat zugeh&ouml;rigen privaten Schl&uuml;ssels nachweisen.</li>
<li><em>Vertraulichkeit</em>: <br/>
Nach erfolgreicher Authentifizierung wird eine sichere, verschl&uuml;sselte Verbindung zwischen den SAKs aufgebaut, um einen sicheren Datenaustausch zu erm&ouml;glichen. Die SAKs fungieren dabei als die Endpunkte f&uuml;r die Ende-zu-Ende-Verschl&uuml;sselung, die s&auml;mtliche HTTP-Daten (Header und Body) abdeckt.</li>
<li><em>Integrit&auml;t</em>: <br/>
TLS gew&auml;hrleistet auch mithilfe von Hashfunktionen, dass die Daten w&auml;hrend der &Uuml;bertragung nicht von Dritten gef&auml;lscht oder manipuliert wurden.</li>
<li><em>BSI-Richtlinien f&uuml;r TLS</em>: <br/>
Der Sicherheitsgrad h&auml;ngt von der gew&auml;hlten TLS-Version und der Cipher-Suite ab, die Verschl&uuml;sselungs-, Authentifizierungs- und Integrit&auml;tsalgorithmen umfasst. Dabei sind die BSI-Richtlinien ma&szlig;gebend, zu denen geh&ouml;ren:
<ul>
<li>Mindeststandard des BSI zur Verwendung von Transport Layer Security (<a href="https://www.bsi.bund.de/DE/Themen/Oeffentliche-Verwaltung/Mindeststandards/TLS-Protokoll/TLS-Protokoll_node.html">SQ-16</a>)</li>
<li>BSI TR-02102-2 "Kryptographische Verfahren: Verwendung von Transport Layer Security (TLS)" Version: 2024-1 (<a href="https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR02102/BSI-TR-02102-2.html">SQ-17</a>) und</li>
<li>BSI TR-03116-4 Kryptographische Vorgaben f&uuml;r Projekte der Bundesregierung Teil 4 Stand 2023 (<a href="https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03116/TR-03116_node.html">SQ-10</a>).</li>
</ul>
</li>
</ul>
<p>mTLS wird oft im Zero-Trust-Kontext angewendet, um die Authentifizierung von Systemen w&auml;hrend der HTTP-Kommunikation zu erm&ouml;glichen und APIs zu sch&uuml;tzen. Die Verwendung von mTLS tr&auml;gt dazu bei, den Schutz vor verschiedenen Arten von Angriffen zu verbessern, darunter:</p>
<ul>
<li><em>On-Path-Angriffe</em>: Angreifer versuchen sich zwischen den SAKs zu platzieren und fangen die Kommunikation zwischen den beiden ab oder ver&auml;ndern sie. Angreifer ohne privaten Schl&uuml;ssel und Zertifikat des Data Consumer k&ouml;nnen sich aber beim SAK des Data Providers nicht authentifizieren, sodass dieser Angriff fast unm&ouml;glich ist.</li>
<li><em>Spoofing</em>: Angreifer versuchen den SAK des Data Consumer zu imitieren, wobei Angriffe fast unm&ouml;glich sind, wenn sich die SAKs mit TLS-Zertifikaten authentifizieren m&uuml;ssen.</li>
<li><em>Credential Stuffing</em>: Angreifer versuchen mit einem "abgefangenen" Zugriffstoken und Abruftoken sich als rechtm&auml;&szlig;iger Data Consumer bei einer API-Anfrage auszugeben. Ohne ein rechtm&auml;&szlig;ig ausgestelltes TLS-Zertifikat und den zugeh&ouml;rigen privaten Schl&uuml;ssel sind Angriffe erfolglos.</li>
</ul>

#### 7.3.2 Metadaten

Die Metadaten im Transportprotokoll werden im HTTP-Header &uuml;bertragen.
<p><strong>Tab.13: Metadaten Transportprotokoll (Auszug)</strong></p>
<table><colgroup><col /><col /></colgroup>
<tbody>
<tr>
<th>Attribute</th>
<th>Bedeutung</th>
</tr>
<tr>
<td>
<p>Authorization: Bearer &lt;Token&gt;</p>
</td>
<td>Zugriffstoken von IAM f&uuml;r Beh&ouml;rden gem&auml;&szlig; RFC6750 (<a href="https://datatracker.ietf.org/doc/html/rfc6750">SQ-18</a>).</td>
</tr>
<tr>
<td>traceparent</td>
<td>W3C Trace Context (<a href="https://www.w3.org/TR/trace-context/#abstract">SQ-19</a>) mit "trace-id" f&uuml;r <a href="#verteilte-nachrichtenverfolgung">verteilte Nachrichtenverfolgung</a></td>
</tr>
<tr>
<td>x-noots-service-id</td>
<td>ServiceID des NOOTS-Dienstes ist nicht Bestandteil der XNachweis-Anfrage und dient der R&uuml;ckverfolgbarkeit von Nachrichten und kann vom Data Provider zur weiteren Verarbeitung verwendet werden.</td>
</tr>
<tr>
<td>x-noots-xnachweis-id</td>
<td>Die eindeutige ID der XNachweis-Anfrage ist ausschlie&szlig;lich f&uuml;r den Nachweisabruf relevant. Bei Verarbeitungsproblemen der XNachweis-Anfrage, wie z.B. Fehler beim Parsen, erm&ouml;glicht die Protokollierung &uuml;ber diese ID eine R&uuml;ckverfolgung zur urspr&uuml;nglichen XNachweis-Anfrage.</td>
</tr>
</tbody>
</table>

#### 7.3.3 Integrit&auml;t auf OSI-Schicht 8: Transport &uuml;ber Anwendungsebene

Das Transportprotokoll gew&auml;hrleistet die Unversehrtheit der Daten w&auml;hrend der &Uuml;bermittlung in der NOOTS-Transportinfrastruktur mittels mTLS auf der Sitzungsschicht (Schicht 5) des OSI-Schichtenmodells. Zum Gesamtverst&auml;ndnis der Ma&szlig;nahmen zur Sicherstellung der Integrit&auml;t werden hier auch die zus&auml;tzlichen Integrit&auml;tspr&uuml;fungen der OSI-Schicht 8, "Transport &uuml;ber Anwendungsebene," aufgef&uuml;hrt:
<ul>
<li>
Die Integrit&auml;t der XNachweis-Anfrage wird durch den berechneten Hashwert im gesiegelten Abruftoken der Vermittlungsstellen und dessen Verifizierung im SAK sichergestellt (siehe auch Kapitel "Sichere Anschlussknoten:&nbsp;<a href="#abstrakte-berechtigung">Abstrakte Berechtigung</a>").
</li>
<li>
Der Data Provider kann die Integrit&auml;t der Nachweisdaten durch Siegelung sicherstellen. Die Transportinfrastruktur muss gew&auml;hrleisten, dass die Daten zur Siegelung der Nachweisdaten an den Data Consumer &uuml;bertragen und dabei nicht ver&auml;ndert werden k&ouml;nnen.
</li>
</ul>

### 7.4 Baustein NOOTS-Anschlussprotokoll

Die NOOTS-Teilnehmer und NOOTS-Komponenten verwenden das Anschlussprotokoll, um mit dem Sicheren Anschlussknoten zu kommunizieren. Es handelt sich um ein synchrones, zustandsloses und interoperables Protokoll im RPC-Stil, das entweder von den NOOTS-Teilnehmern oder den SAKs initiiert werden kann. Das Protokoll basiert auf Representational State Transfer (REST) &uuml;ber HTTP(S) und verwendet zus&auml;tzliche HTTP-Header-Attribute zur &Uuml;bertragung von Metainformationen. Das Anschlussprotokoll unterst&uuml;tzt die folgenden API-Typen:
<ul>
<li><em>Consumer-API f&uuml;r Data Consumer</em>: API zum Abrufen von Nachweisen,</li>
<li><em>Provider-API f&uuml;r Data Provider</em>: API f&uuml;r die Bereitstellung von Nachweisen aus nationalen Registern,</li>
<li><em>Provider-API f&uuml;r NOOTS-Komponenten, einschlie&szlig;lich der Intermedi&auml;ren Plattform: </em>API zur Bereitstellung zentraler Dienste im Rahmen der SAK-Nachnutzung (Details werden hier nicht behandelt).</li>
</ul>
<p>Im Kapitel "<a href="#8-laufzeitsicht">Laufzeitsicht</a>" wird ein detaillierter Ablauf f&uuml;r den Nachweisabruf im HLA-Anwendungsfall "1a: Interaktiver Nachweisabruf zu einer nat&uuml;rlichen Person &uuml;ber das NOOTS" (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-03</a>) beschrieben, wobei die Consumer-API f&uuml;r Data Consumer und die Provider-API f&uuml;r Data Provider verwendet werden.</p>

#### 7.4.1 Consumer-API f&uuml;r Data Consumer

<p>Die prim&auml;re Funktion der Consumer-API besteht darin, Data Consumer dazu zu bef&auml;higen, Nachweise vom Data Provider oder von einem EU-Mitgliedstaat &uuml;ber die Intermedi&auml;re Plattform abzurufen. Beim Zugriff von Onlinediensten als Data Consumer auf die Consumer-API m&uuml;ssen folgende Festlegungen aus dem "Anschlusskonzept Data Consumer (DC)" (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-01</a>) und dem Grobkonzept "IAM f&uuml;r Beh&ouml;rden" (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-05</a>) ber&uuml;cksichtigt werden:</p>
<ol>
<li>
<p>Jeder Onlinedienst, auch innerhalb einer Onlinedienst-Plattform, wird als eine IT-Komponente betrachtet und besitzt demnach eine eindeutige "Komponenten-ID". Somit fungiert jeder Onlinedienst als Data Consumer.</p>
</li>
<li>
<p>Mehrere Data Consumer k&ouml;nnen unter Verwendung desselben Zertifikats f&uuml;r die Fach- und betriebsverantwortlichen Stellen in IAM f&uuml;r Beh&ouml;rden registriert werden.</p>
</li>
</ol>
<p>Der SAK stellt folgende Operationen der Consumer-API zur Verf&uuml;gung, die vom Data Consumer aufgerufen werden k&ouml;nnen:</p>
<p><strong>Tab. 14: &Uuml;bersicht Consumer-API und deren Relevanz f&uuml;r Anwendungsf&auml;lle 1, 2 und 4 der HLA</strong></p>
<table><colgroup><col /><col /><col /><col /><col /><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th></th>
<th>Operationen</th>
<th>Information</th>
<th>Bereitstellung durch</th>
<th>Aufrufparameter</th>
<th>Antwort liefert</th>
<th>HLA-1</th>
<th>HLA-2</th>
<th>HLA-4</th>
</tr>
<tr>
<td>1</td>
<td>getZugriffsToken</td>
<td>Zugriffsberechtigung f&uuml;r nachfolgende Operationen</td>
<td>IAM f&uuml;r Beh&ouml;rden (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-05</a>)</td>
<td>
<ul>
<li>Komponenten-ID des registrierten Data Consumer</li>
</ul>
</td>
<td>
<ul>
<li>Zugriffs-Token<br />ODER</li>
<li>Fehlermeldung des IAM f&uuml;r Beh&ouml;rden</li>
</ul>
</td>
<td>x</td>
<td>x</td>
<td>x</td>
</tr>
<tr>
<td>2</td>
<td>getNachweisangebot</td>
<td>
<p>Nachweisangebot des f&uuml;r den Nachweistyp und<br />das Nachweissubjekt zust&auml;ndigen Data Providers</p>
</td>
<td>Registerdatennavigation (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-07</a>)</td>
<td>
<ul>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden</li>
<li>Nachweistyp</li>
<li>ggf. Zust&auml;ndigkeitsparameter</li>
</ul>
</td>
<td>
<ul>
<li>Nachweisangebot<br />ODER</li>
<li>Fehlermeldung der Registerdatennavigation</li>
</ul>
</td>
<td>x</td>
<td>x</td>
<td></td>
</tr>
<tr>
<td>3</td>
<td>getIdNr</td>
<td>IDNr einer nat&uuml;rlichen Person als Nachweissubjekt</td>
<td>IDM f&uuml;r Personen (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-10</a>)</td>
<td>
<ul>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden</li>
<li>Basisdaten der Person</li>
</ul>
</td>
<td>
<ul>
<li>XBasisdaten (einschlie&szlig;lich IDNr)<br />ODER</li>
<li>Fehlermeldung des IDM f&uuml;r Personen</li>
</ul>
</td>
<td>x<sup>1)</sup></td>
<td>x<sup>1)</sup></td>
<td></td>
</tr>
<tr>
<td>4</td>
<td>getBeWiNr</td>
<td>
<p>beWiNr eines Unternehmens als Nachweissubjekt</p>
</td>
<td>IDM f&uuml;r Unternehmen (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-11</a>)</td>
<td>
<ul>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden</li>
<li>Basisdaten des Unternehmens</li>
</ul>
</td>
<td>
<ul>
<li>beWiNr<br />ODER</li>
<li>Fehlermeldung des IDM f&uuml;r Unternehmen</li>
</ul>
</td>
<td>x<sup>1)</sup></td>
<td>x<sup>1)</sup></td>
<td></td>
</tr>
<tr>
<td>5</td>
<td>getNachweis</td>
<td>Nachweis <br /><br /></td>
<td>Data Provider (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-02</a>)</td>
<td>
<ul>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden</li>
<li>ServiceID ausNachweisangebot</li>
<li>XNachweis.DE.EvidenceRequest.0101</li>
</ul>
</td>
<td>
<ul>
<li>XNachweis.DE.EvidenceResponse.0102<sup><br /></sup>u.a. mit Nachweis<br />ODER</li>
<li>XNachweis.DE.EvidenceErrorResponse.0103<sup><br /></sup>ODER</li>
<li>Fehler der Vermittlungsstelle<br />ODER</li>
<li>Fehler der Registerdatennavigation</li>
</ul>
</td>
<td>x</td>
<td>x</td>
<td></td>
</tr>
<tr>
<td>6</td>
<td>getIPAufrufdaten</td>
<td>
<p>Verbindungsparameter der zust&auml;ndigen Intermedi&auml;ren Plattform,<br />Redirect-URL der zust&auml;ndigen IP (URLtoIP) und<br />Referenz f&uuml;r sp&auml;teren Nachweisabruf (ConversationID)</p>
</td>
<td>Registerdatennavigation (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-07</a>)<br />Intermedi&auml;re Plattform (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-06</a>)</td>
<td>
<ul>
<li>Zugriffs-Token (Antwort vongetZugriffsToken)</li>
<li>XNachweis.DE.EvidenceOrder.0401<br />u.a. mit BackToEvidenceRequesterURL</li>
</ul>
</td>
<td>
<ul>
<li>XNachweis.DE.EvidenceOrderResponse.0402<br />u.a. mitURLtoIP und ConversationID<br />ODER</li>
<li>XNachweis.DE.EvidenceOrderErrorResponse.0403<br />ODER</li>
<li>Fehlermeldung der Registerdatennavigation<br />ODER</li>
<li>Fehlermeldung der Vermittlungsstelle</li>
</ul>
</td>
<td></td>
<td></td>
<td>x</td>
</tr>
<tr>
<td>7</td>
<td>getNachweisVonIP</td>
<td>
<p>Nachweis</p>
</td>
<td>Intermedi&auml;re Plattform (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-06</a>)</td>
<td>
<ul>
<li>Zugriffs-Token (Antwort von getZugriffsToken)</li>
<li>XNachweis.DE.GetEvidence.0411<br />mit ConversationID (s. getIPAufrufdaten)</li>
</ul>
</td>
<td>
<ul>
<li>XNachweis.DE.GetEvidenceResponse.0412<br />u.a. mit Nachweis<br />ODER</li>
<li>XNachweis.DE.GetEvidenceErrorResponse.0413</li>
</ul>
</td>
<td></td>
<td></td>
<td>x</td>
</tr>
</tbody>
</table>
<p><strong>Legende</strong></p>
<ul>
<li><sup>1)</sup> Sofern eine Rechtsgrundlage f&uuml;r den Nachweisabruf mit diesem Identifikator gem&auml;&szlig; IDNrG oder UBRegG vorhanden ist.</li>
</ul>

#### 7.4.2 Provider-API f&uuml;r Data Provider

<p>Die Provider-API erm&ouml;glicht es einem Data Provider, auf Anfrage Nachweise bereitzustellen. Sie bietet dabei zwei alternative Kommunikationsmodelle f&uuml;r die Kommunikation mit dem SAK: den passiven und den aktiven Empf&auml;nger. Die Wahl des geeigneten Kommunikationsmodells richtet sich nach den Anforderungen des Data Providers an Durchsatz und netzseitige Sicherheitsmechanismen.</p>

##### Passiver Empf&auml;nger

In diesem Kommunikationsmodell definiert der SAK eine API als Service Provider Interface (SPI), die vom Data Provider implementiert werden muss. Der SAK des Data Providers leitet die Nachweisanfrage an das SPI des passiven Data Providers weiter. Ist der Data Provider nicht verf&uuml;gbar, erzeugt der SAK sofort eine Fehlermeldung und sendet diese an den Data Consumer. Dieses Kommunikationsmodell ist einfach umzusetzen und eignet sich f&uuml;r sichere Betriebsumgebungen, beispielsweise mit kontrollierten Zonen&uuml;berg&auml;ngen durch Anwendungs-Level-Gateways oder "P-A-P"-Umgebungen (Paketfilter - Application Layer Gateway - Paketfilter) gem&auml;&szlig; "BSI NET.1.1: Netzarchitektur und -design" (<a href="https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Kompendium_Einzel_PDFs_2021/09_NET_Netze_und_Kommunikation/NET_1_1_Netzarchitektur_und_design_Edition_2021.pdf?__blob=publicationFile&v=2">SQ-20</a>).
<p><strong>Tab. 15: &Uuml;bersicht Service Provider Interface f&uuml;r passive Empf&auml;nger und deren Relevanz f&uuml;r Anwendungsf&auml;lle 1, 2 und 3 der HLA</strong></p>
<table><colgroup><col /><col /><col /><col /><col /><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th></th>
<th>Operationen</th>
<th>Information</th>
<th>Bereitstellung durch</th>
<th>Aufrufparameter</th>
<th>Antwort liefert</th>
<th>HLA-1</th>
<th>HLA-2</th>
<th>HLA-3</th>
</tr>
<tr>
<td>1</td>
<td>getNachweis<br /><br /></td>
<td>Nachweis <br /><br /><br /></td>
<td>Data Provider (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-02</a>)<br /><br /></td>
<td>
<ul>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden</li>
<li>XNachweis.DE.EvidenceRequest.0101</li>
</ul>
</td>
<td>
<ul>
<li>XNachweis.DE.EvidenceResponse.0102</li>
<li>u.a. mit Nachweis<br />ODER</li>
<li>XNachweis.DE.EvidenceErrorResponse.0103</li>
</ul>
</td>
<td>x</td>
<td>x</td>
<td></td>
</tr>
<tr>
<td>2</td>
<td></td>
<td></td>
<td></td>
<td>
<ul>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden</li>
<li>XNachweis.DE.EvidenceRequest.0301</li>
</ul>
</td>
<td>
<ul>
<li>XNachweis.DE.EvidenceResponse.0302</li>
<li>u.a. mit Nachweis<br />ODER</li>
<li>XNachweis.DE.EvidenceErrorResponse.0303</li>
</ul>
</td>
<td></td>
<td></td>
<td>x</td>
</tr>
</tbody>
</table>

##### Aktiver Empf&auml;nger (Initiativumkehr)

Der SAK bietet als Sicherheitsmerkmal die Umkehr der Kommunikationsrichtung, um restriktive Netzarchitekturen bei Data Providern zu erm&ouml;glichen. Dieser Mechanismus erlaubt es, Verbindungen ausschlie&szlig;lich von der sicheren in eine weniger sichere Zone zu initiieren.
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/15.png" alt="" /></p>
<p><strong>Abb. 14: Initiativumkehr der Kommunikation durch Long-Polling-Technik<br /></strong></p>
<p>Beim Kommunikationsmodell &bdquo;aktiver Empf&auml;nger&ldquo; stellt der SAK die Operationen der Provider-API zur Verf&uuml;gung. Der Ablauf dieses Modells ("Long-Polling-Technik") gestaltet sich wie folgt:</p>
<ol>
<li>
<p><em>Data Provider sendet Anfrage &bdquo;warteAufAbruf&ldquo;:</em> Data Provider initiiert mit der Operation &bdquo;warteAufAbruf&ldquo; und einem Timeout als Parameter eine Anfrage beim SAK, um auf neue Nachweis-Anfragen zu warten. Bei einem redundanten Deployment sollten mehrere zeitgleiche Anfragen initiiert werden.</p>
</li>
<li>
<p><em>SAK wartet:</em> SAK h&auml;lt die Anfrage offen und wartet, bis eine neue Nachweis-Anfrage vom SAK des Data Consumers eingeht oder ein Timeout auftritt. Bei einem Timeout wird die Anfrage geschlossen, und der Data Provider startet sofort eine neue Anfrage gem&auml;&szlig; Schritt 1.</p>
</li>
<li>
<p><em>Nachweis-Anfrage ist verf&uuml;gbar:</em> Sobald eine neue Nachweis-Anfrage beim SAK eingeht, antwortet der SAK auf die offene Anfrage aus &bdquo;warteAufAbruf&ldquo; mit den Daten der Nachweis-Anfrage.</p>
</li>
<li>
<p><em>Data Provider empf&auml;ngt Nachweis-Anfrage und sendet Antwort:</em> Data Provider empf&auml;ngt die Nachweis-Anfrage, verarbeitet diese und sendet die Nachweis-Antwort oder eine Fehlermeldung mittels der Operation &bdquo;sendNachweis&ldquo; an den SAK. Anschlie&szlig;end initiiert der Data Provider sofort eine neue Anfrage mit der Operation &bdquo;warteAufAbruf&ldquo;, um auf weitere Nachweis-Anfragen zu warten.</p>
</li>
<li>
<p><em>Zyklus wiederholt sich:</em> Zyklus von &bdquo;warteAufAbruf&ldquo; und &bdquo;sendNachweis&ldquo; wiederholt sich kontinuierlich.</p>
</li>
</ol>
<p>Die &bdquo;Long-Polling-Technik&ldquo; erm&ouml;glicht deutlich geringere Latenzen und h&ouml;here Durchs&auml;tze als traditionelles Polling und erfordert keine dauerhafte Verbindung wie WebSockets. Sie wird im Vergleich zu WebSockets gut von Firewalls und Proxys unterst&uuml;tzt, da sie auf normale HTTP(S)-Anfragen basiert.Die Implementierung dieser Technik bringt einige Implikationen mit sich: Zum einen unterscheidet sich das Programmiermodell f&uuml;r Data Provider von dem &uuml;blichen Thread-Pool-gest&uuml;tzten WebController-Modell, da die Provider-Implementierung die Threads aktiv verwalten muss. Beispiel- oder Referenzimplementierungen k&ouml;nnen hierbei hilfreich sein. Zum anderen erfordert ein redundantes Deployment in horizontal skalierende Clusterumgebungen Mechanismen zur Lastverteilung. Diese Mechanismen d&uuml;rfen jedoch nicht die angestrebte Leichtgewichtigkeit des SAKs beeintr&auml;chtigen.</p>
<p>Der SAK stellt folgende Operationen zur Verf&uuml;gung, die vom Data Provider aufgerufen werden k&ouml;nnen:</p>
<p><strong>Tab. 16: &Uuml;bersicht Provider-API f&uuml;r aktive Empf&auml;nger und deren Relevanz f&uuml;r Anwendungsf&auml;lle 1, 2 und 3 der HLA</strong></p>
<table><colgroup><col /><col /><col /><col /><col /><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th></th>
<th>Operationen</th>
<th>Information</th>
<th>Bereitstellung durch</th>
<th>Aufrufparameter</th>
<th>Antwort liefert</th>
<th>HLA-1</th>
<th>HLA-2</th>
<th>HLA-3</th>
</tr>
<tr>
<td>1</td>
<td>
warteAufAbruf
</td>
<td>Nachweis-Anfrage</td>
<td>SAK</td>
<td>
<ul>
<li>Timeout: Maximale Wartezeit in Sekunden f&uuml;r das Long Polling (Default : 1800 Sekunden)</li>
</ul>
</td>
<td>
<ul>
<li>XNachweis.DE.EvidenceRequest.0101</li>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden<br />ODER</li>
<li>Timeout, wenn keine Nachweis-Anfrage im SAK verf&uuml;gbar</li>
</ul>
</td>
<td>x</td>
<td>x</td>
<td></td>
</tr>
<tr>
<td>2</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<ul>
<li>XNachweis.DE.EvidenceRequest.0301</li>
<li>Zugriffstoken IAM f&uuml;r Beh&ouml;rden<br />ODER</li>
<li>Timeout, wenn keine Nachweis-Anfrage im SAK verf&uuml;gbar</li>
</ul>
</td>
<td></td>
<td></td>
<td>x</td>
</tr>
<tr>
<td>3</td>
<td>sendNachweis</td>
<td>Nachweis-Antwort mit Nachweis</td>
<td>Data Provider (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-02</a>)</td>
<td>
XNachweis.DE.EvidenceResponse.0102<sup><br /></sup>u.a. mit Nachweis<br />ODER<br />XNachweis.DE.EvidenceErrorResponse.0103
</td>
<td>
<ul>
<li>OK</li>
</ul>
</td>
<td>x<br /><br /></td>
<td>x</td>
<td></td>
</tr>
<tr>
<td>4</td>
<td></td>
<td></td>
<td></td>
<td>XNachweis.DE.EvidenceResponse.0302 u.a. mit Nachweis<br/>ODER<br/>XNachweis.DE.EvidenceErrorResponse.0303</td>
<td></td>
<td></td>
<td></td>
<td>x</td>
</tr>
</tbody>
</table>

#### 7.4.3 Versionierung

Das Anschlussprotokoll muss bei &Auml;nderungen die gleichzeitige Nutzung verschiedener API-Versionen erm&ouml;glichen und folgt dabei den Regeln des Semantic Versioning. Eine API-Version setzt sich aus den drei Bestandteilen &lt;Hauptversion.Minorversion.Patchversion&gt; zusammen, z.B. 1.1.0:
<ul>
<li><em>Hauptversion:</em> Erh&ouml;hung bei nicht abw&auml;rtskompatiblen &Auml;nderungen </li>
<li><em>Minorversion:</em> Erh&ouml;hung bei neuen, abw&auml;rtskompatiblen Funktionen </li>
<li><em>Patchversion:</em> Erh&ouml;hung bei abw&auml;rtskompatiblen Fehlerkorrekturen </li>
</ul>
<p>Die Hauptversion des Anschlussprotokolls ist ein fester Bestandteil der REST-Endpunkt-URL. Bei Anwendung des Entwurfsmusters "Tolerant-Reader-Patterns" in den API-Implementierungen der NOOTS-Teilnehmer k&ouml;nnen neue abw&auml;rtskompatible API-Versionen mit zus&auml;tzlichen Funktionen eingef&uuml;hrt werden, ohne die bestehenden API-Implementierungen zu beeintr&auml;chtigen.</p>

#### 7.4.4 Sicherheit

Die REST-Endpunkte sowohl der SAKs als auch der NOOTS-Teilnehmer m&uuml;ssen ausschlie&szlig;lich &uuml;ber HTTP in Verbindung mit TLS (HTTPS) erreichbar sein. HTTPS gew&auml;hrleistet die Vertraulichkeit und Integrit&auml;t der transportierten Daten durch Verschl&uuml;sselung und Hashwert-Bildung.

#### 7.4.5 Autorisierung der API-Zugriffe

<p>Es wird dringend empfohlen, die Verbindung &uuml;ber das Anschlussprotokoll zwischen dem SAK des NOOTS-Teilnehmers und dem NOOTS-Teilnehmer (Data Consumer oder Data Provider) mittels gegenseitig authentifizierter Zertifikate zu autorisieren, auch bekannt als mutual TLS (mTLS). Die Verwendung von JWT-Tokens als Authentifizierungsmethode wird in der Consumer-API basierend auf dem Zugriffstoken aus IAM f&uuml;r Beh&ouml;rden unterst&uuml;tzt. In der Provider-API im Kommunikationsmodell "passiver Empf&auml;nger" wird das Zugriffstoken aus IAM f&uuml;r Beh&ouml;rden unver&auml;ndert an den Data Provider weitergeleitet. Bei dem Kommunikationsmodell "aktiver Empf&auml;nger" kann alternativ zu mTLS auch HTTP Basic verwendet werden.</p>

#### 7.4.6 Metadaten

Die Metadaten im Anschlussprotokoll werden im HTTP-Header &uuml;bertragen.
<p><strong>Tab.17: Metadaten Anschlussprotokoll (Auszug)</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>Attribute</th>
<th>Bedeutung</th>
<th>Gilt f&uuml;r API-Typ</th>
</tr>
<tr>
<td>
<p>Authorization: Bearer &lt;Token&gt;</p>
</td>
<td>Zugriffstoken von IAM f&uuml;r Beh&ouml;rden gem&auml;&szlig; RFC6750 (<a href="https://datatracker.ietf.org/doc/html/rfc6750">SQ-18</a>).</td>
<td>
<p>Consumer-API,<br />Provider-API</p>
</td>
</tr>
<tr>
<td>
<p>traceparent</p>
</td>
<td>W3C Trace Context (<a href="https://www.w3.org/TR/trace-context/#abstract">SQ-19</a>) mit "trace-id" f&uuml;r&nbsp;<a href="#verteilte-nachrichtenverfolgung">verteilte Nachrichtenverfolgung</a></td>
<td>Consumer-API,<br />Provider-API</td>
</tr>
<tr>
<td>
<p>x-noots-xnachweis-id</p>
</td>
<td>Eindeutige ID der XNachweis-Anfrage</td>
<td>Provider-API</td>
</tr>
<tr>
<td>x-noots-service-id</td>
<td>ServiceID des NOOTS-Dienstes aus der Registerdatennavigation</td>
<td>Provider-API</td>
</tr>
</tbody>
</table>

### 7.5 Baustein Data Provider

Register sind in der Regel &uuml;ber &ouml;ffentliche Schnittstellen nicht erreichbar und werden normalerweise in abgeschotteten Netzsegmenten betrieben. Der Zugriff ist nur f&uuml;r ausgew&auml;hlte Kommunikationspartner m&ouml;glich, wenn eine fachrechtliche Regelung dies vorsieht. Eine Exposition der Register im Internet erfordert umfassende zus&auml;tzliche Schutzma&szlig;nahmen wie beispielsweise "P-A-P" (Paketfilter - Application Layer Gateway - Paketfilter)-Umgebungen gem&auml;&szlig; "BSI NET.1.1: Netzarchitektur und -design" (<a href="https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Kompendium_Einzel_PDFs_2021/09_NET_Netze_und_Kommunikation/NET_1_1_Netzarchitektur_und_design_Edition_2021.pdf?__blob=publicationFile&v=2">SQ-20</a>).
<p><strong>Initiativumkehr</strong></p>
<p>Dar&uuml;ber hinaus kann der Ansatz der Initiativumkehr (Kommunikationsmodell <a href="#742-provider-api-für-data-provider">"aktiver Empf&auml;nger" der Provider-API</a>) eingesetzt werden. Dabei exponieren die Register ihre Schnittstellen nicht im Netz, sondern holen aktiv Nachrichten von der Transportinfrastruktur ab. Dadurch sind sie deutlich weniger anf&auml;llig f&uuml;r Angriffe. Allerdings ist die technische Umsetzung des Abholens und Beantwortens von Nachrichten komplexer, insbesondere bei hohen Nachrichtenlasten.</p>
<p>&rarr; Weiterf&uuml;hrende Informationen zum Anschluss eines Data Provider an das NOOTS sind im Anschlusskonzept f&uuml;r Data Provider (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-02</a>) zu finden.</p>

## 8. Laufzeitsicht

Im folgenden Sequenzdiagramm und der dazugeh&ouml;rigen Beschreibung wird der vom Data Consumer initiierte Ablauf eines Nachweisabrufs f&uuml;r den HLA-Anwendungsfall "1a: Interaktiver Nachweisabruf zu einer nat&uuml;rlichen Person &uuml;ber das NOOTS" mit einer IDNr gem&auml;&szlig; Identifikationsnummerngesetz (<a href="https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf">RGR-01</a>) im Detail dargestellt. Dabei steht die Kommunikation zwischen dem Sicheren Anschlussknoten (SAK) und den weiteren NOOTS-Komponenten im Vordergrund. In der grafischen Abbildung wird der Anschluss an das NOOTS des Data Providers ausschlie&szlig;lich &uuml;ber das Kommunikationsmodell des "aktiven Empf&auml;ngers" der Provider-API dargestellt. In der Beschreibung hingegen werden beide Kommunikationsmodelle, sowohl "aktiver Empf&auml;nger" als auch "passiver Empf&auml;nger", erl&auml;utert.

### 8.1 Nachweislieferung f&uuml;r den HLA-Anwendungsfall 1a mit IDNr

<table>
<tbody>
<tr>
<td>
Zur besseren &Uuml;bersicht wurde auf die explizite Darstellung der SAKs f&uuml;r IDM f&uuml;r Personen, Registerdatennavigation und Vermittlungsstelle im Sequenzdiagramm und in der Beschreibung verzichtet. Diese SAKs leiten die Anfragen an die entsprechenden NOOTS-Komponenten weiter.
</td>
</tr>
</tbody>
</table>
<p><img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/16.png" alt="" /></p>
<p><strong>Abb. 15:<strong> Ablauf einer Nachweislieferung f&uuml;r den HLA Anwendungsfall 1a</strong></strong></p>

### 8.2 Beschreibung Nachweislieferung f&uuml;r den HLA-Anwendungsfall 1a mit IDNr

Der Data Consumer kann &uuml;ber die Consumer-API seines SAK einen Nachweisabruf durchf&uuml;hren. Dazu muss er die folgenden Schritte in der angegebenen Reihenfolge durchlaufen:
<ol>
<li><em>Anfordern des Zugriffstokens aus IAM f&uuml;r Beh&ouml;rden<strong>:</strong></em> getZugriffsToken</li>
<li><em>Ermittlung des Nachweisangebotes aus der Registerdatennavigation:</em> getNachweisangebot</li>
<li><em>Ermittlung der IDNr anhand der Basisdaten aus IDM f&uuml;r Personen, sofern das Nachweisangebot die Verwendung der IDNr f&uuml;r den Nachweisabruf zul&auml;sst: </em>getIdNr</li>
<li><em>Abruf des Nachweises vom Data Provider:</em> getNachweis</li>
</ol>
<p>Im Folgenden werden die einzelnen Operationen der Consumer-API in der obigen Reihenfolge beschrieben und die damit verbundenen Aktivit&auml;ten des SAK detailliert erl&auml;utert. In der Spalte &bdquo;Ein-/Ausgabeparameter und Fehlerzust&auml;nde&ldquo; sind die erforderlichen fachlichen Eingabe- und Ausgabedaten f&uuml;r die jeweiligen Operationen sowie m&ouml;gliche fachliche Fehlerzust&auml;nde aufgef&uuml;hrt.</p>
<p>F&uuml;r die Anbindung der Teilnehmer (Data Consumer und Data Provider) an den SAK &uuml;ber die Consumer-API bzw. Provider-API des Anschlussprotokolls gelten allgemein folgende Grunds&auml;tze, die in der nachfolgenden tabellarischen Beschreibung nicht wiederholt aufgef&uuml;hrt werden:</p>
<ul>
<li><em>Sichere Kommunikationsverbindung: </em>Die Verbindung zwischen Teilnehmer und SAK ist TLS-verschl&uuml;sselt.</li>
<li><em>Authentifizierung der NOOTS-Teilnehmer</em>: Die Authentifizierung der NOOTS-Teilnehmer erfolgt entweder mittels &bdquo;mTLS&ldquo; (empfohlen) oder alternativ &uuml;ber &bdquo;HTTP Basic Authentication&ldquo; gem&auml;&szlig; RFC 7617. Bei fehlgeschlagener Authentifizierung wird die Verarbeitung abgebrochen. Bei der Consumer-API muss bei Verwendung von &bdquo;HTTP Basic Authentication&ldquo; nur die Operation &bdquo;getZugriffsToken&ldquo; abgesichert werden, da die anderen Operationen bereits durch das Zugriffstoken aus dem IAM f&uuml;r Beh&ouml;rden abgesichert sind.</li>
</ul>
<p>Folgende Abk&uuml;rzungen werden f&uuml;r die NOOTS-Teilnehmer und Komponenten verwendet:</p>
<ul>
<li><em>DC:</em> Data Consumer,</li>
<li><em>DP:</em> Data Provider,</li>
<li><em>SAK-DC:</em> SAK des Data Consumer,</li>
<li><em>SAK-IAM:</em> SAK des IAM f&uuml;r Beh&ouml;rden,</li>
<li><em>SAK-RDN: </em>SAK der Registerdatennavigation,</li>
<li><em>SAK-IDM-P:</em> SAK des IDM f&uuml;r Personen,</li>
<li><em>SAK-VS: </em>SAK der Vermittlungsstelle.</li>
</ul>

#### 8.2.1 Schritt 1: Consumer-API::getZugriffsToken

Ein Data Consumer ben&ouml;tigt zur Kommunikation mit NOOTS-Komponenten ein g&uuml;ltiges Zugriffstoken. Dieses Zugriffstoken wird von der NOOTS-Komponente IAM f&uuml;r Beh&ouml;rden ausgestellt und verf&uuml;gt &uuml;ber eine maximale Lebensdauer von 60 Sekunden.Sobald das Zugriffstoken abgelaufen ist, muss der Data Consumer ein neues Zugriffstoken anfordern.
<p><strong>Tab. 18: Abruf des Zugriffstokens</strong></p>
<table><colgroup><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th><strong>Von Komponente</strong></th>
<th><strong>nach Komponente</strong></th>
<th><strong>Beschreibung</strong></th>
<th>Ein-/Ausgabeparameter und Fehlerzust&auml;nde</th>
</tr>
<tr>
<td>
<p>DC</p>
</td>
<td>
<p>SAK-DC</p>
</td>
<td>
<ul>
<li>Data Consumer initiiert den Nachweisabruf, indem er die synchrone Operation "getZugriffsToken" der Consumer-API des SAK-DC aufruft.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>"Komponenten-ID" derIT-Komponente "Data Consumer" aus IAM f&uuml;r Beh&ouml;rden</li>
</ul>
</td>
</tr>
<tr>
<td>
<p>SAK-DC</p>
</td>
<td>
<p>IAM f&uuml;r Beh&ouml;rden (SAK-IAM)</p>
</td>
<td>
<ul>
<li>SAK-DC f&uuml;hrt denOAuth 2.0 Client Credentials (<a href="https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/">SQ-21</a>) Grant-Type aus, um ein g&uuml;ltiges Zugriffstoken f&uuml;r den Data Consumer anzufordern.</li>
<li>Die gegenseitige Authentifizierung des Data Consumer und IAM f&uuml;r Beh&ouml;rden erfolgt dabei unter Verwendung von mTLS.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>TLS Client Zertifikat Data Consumer der betriebsverantwortlichenStelle</li>
<li>"Komponenten-ID" desData Consumer</li>
</ul>
<p>Ausgabeparameter:</p>
<ul>
<li>Gesiegeltes Zugriffstoken (siehe Kapitel "8.2.1 Zugriffs-Token abrufen" im Konzept IAM f&uuml;r Beh&ouml;rden (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-10</a>) f&uuml;r weitere Informationen)</li>
</ul>
<table>
<tbody>
<tr>
<td>
<ul>
<b>Auszug Zugriffstoken aus IAM für Behörden</b>
<li>Komponenten-ID: Data Consumer</li>
<li>FV-Organisation: Organisation der fachverantwortlichen Stelle</li>
<li>FV-Anschrift: Anschrift der fachverantwortlichen Stelle</li>
<li>Rollen: Menge von Rollen gem&auml;&szlig; der Teilnahmeart f&uuml;r den Zugriff auf NOOTS-Komponenten</li>
<li>Teilnahmeart: "DC_Onlinedienst"</li>
<li>Verwaltungsbereich: Verwaltungsbereich der Beh&ouml;rdenfunktion der IT-Komponente</li>
<li>Beh&ouml;rdenfunktion: Beh&ouml;rdenfunktion der IT-Komponente</li>
</ul>
</td>
</tr>
</tbody>
</table>
<ul>
<li>G&uuml;ltigkeitszeit des Zugriffstokens</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>IT-Komponente 'Data Consumer' kann nicht erfolgreich in IAM f&uuml;r Beh&ouml;rden authentifiziert werden ( sieheKapitel "8.2.3IT-Komponente authentifizieren" imKonzept IAM f&uuml;r Beh&ouml;rden)</li>
</ul>
</td>
</tr>
<tr>
<td>
<p>SAK-DC</p>
</td>
<td>
<p>DC</p>
</td>
<td>
<ul>
<li>Die Operation "getZugriffsToken" der Consumer-API gibt die Ausgabeparameter zur&uuml;ck.</li>
</ul>
</td>
<td>
<p>Ausgabeparameter:</p>
<ul>
<li>Gesiegeltes Zugriffstoken</li>
<li>G&uuml;ltigkeitszeit des Zugriffstokens</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>siehe Fehlerzust&auml;nde aus vorherigen Schritten</li>
</ul>
</td>
</tr>
</tbody>
</table>

#### 8.2.2 Schritt 2:Consumer-API::getNachweisangebot

Der zust&auml;ndige Data Provider f&uuml;r den Nachweisabruf wird durch die Registerdatennavigation ermittelt. F&uuml;r zentrale Register gen&uuml;gt der Nachweistyp zur Bestimmung des Data Providers. Bei dezentralen Registern sind weitere Zust&auml;ndigkeitsparameter wie z. B. PLZ oder ARS erforderlich.
<p><strong>Tab. 19: Abruf des Nachweisangebotes</strong></p>
<table><colgroup><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th><strong>von Komponente</strong></th>
<th><strong>nach <br />Komponente</strong></th>
<th><strong>Beschreibung</strong></th>
<th>Ein-/ Ausgabeparameter und Fehlerzust&auml;nde</th>
</tr>
<tr>
<td>DC</td>
<td>SAK-DC</td>
<td>
<ul>
<li>Data Consumer ruft den SAK-DC &uuml;ber die synchrone Operation "getNachweisangebot" der Consumer-API des SAK-DC auf.</li>
<li>SAK-DC &uuml;berpr&uuml;ft die&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung </a>(Ressource Registerdatennavigation) anhand des Zugriffstokens. Bei fehlgeschlagener Pr&uuml;fung wird die Verarbeitung abgebrochen.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken "IAM f&uuml;r Beh&ouml;rden"</li>
<li>Nachweistyp</li>
<li>ggf. Zust&auml;ndigkeitsparameter</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
</ul>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>Registerdaten-navigation (SAK-RDN)</td>
<td>
<ul>
<li>SAK-DC ruft die RDN-API "findEvidenceService" auf, um das Nachweisangebot deszust&auml;ndigen Data Providers zu ermitteln.</li>
<li>Registerdatennavigation (SAK-RDN) &uuml;berpr&uuml;ft die <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens. Bei fehlgeschlagener Pr&uuml;fung wird die weitere Verarbeitung abgebrochen.</li>
<li>Bei erfolgreicher Ausf&uuml;hrung liefert die RDN-API das Nachweisangebot des zust&auml;ndigen Data Providers zur&uuml;ck.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken "IAM f&uuml;r Beh&ouml;rden"</li>
<li>Nachweistyp und</li>
<li>ggf. Zust&auml;ndigkeitsparameter.</li>
</ul>
<p>Ausgabeparameter: Nachweisangebot mit folgenden Angaben</p>
<ul>
<li>ProviderID: Komponenten-ID des Data Providers aus IAM f&uuml;r Beh&ouml;rden</li>
<li>ServiceID des Nachweisangebotes des Data Providers</li>
<li>Information zur "Verwendung der IDNr" (f&uuml;r den Nachweisabruf)</li>
<li>Liste der Formate und Schemata f&uuml;r Nachweise</li>
<li>erforderliches Vertrauensniveau</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Authentifizierung des SAK des Data Consumers mittels mTLS im Transportprotokoll war nicht erfolgreich.</li>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
<li>Eingabeparameter &bdquo;Nachweistyp&ldquo; und &bdquo;Zust&auml;ndigkeitsparameter&ldquo; reichen in der RDN nicht aus, um den zust&auml;ndigen Data Provider eindeutig zu bestimmen. Zus&auml;tzliche Zust&auml;ndigkeitsparameter sind erforderlich und m&uuml;ssen vom Data Consumer bei einem wiederholten Aufruf geliefert werden.</li>
</ul>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>DC</td>
<td>
<ul>
<li>Die Operation "getNachweisangebot" der Consumer-API gibt dieAusgabeparameter zur&uuml;ck.</li>
</ul>
</td>
<td>
<p>Ausgabeparameter:</p>
<ul>
<li>siehe Ausgabeparameter aus vorigem Schritt</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>siehe Fehlerzust&auml;nde aus vorherigen Schritten</li>
</ul>
</td>
</tr>
</tbody>
</table>

#### 8.2.3 Schritt 3:Consumer-API::getIdNr

Die Identifikationsnummer (IDNr) wird anhand der Basisdaten aus der Nutzer-Authentifizierung in der IDM f&uuml;r Personen (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten">Bestandteil des Systems Identit&auml;tsdatenabruf</a>) ermittelt. Der Aufruf dieser Operation der Consumer-API ist nur erforderlich, wenn aus dem Ergebnis der vorherigen Operation "getNachweisangebot" abgeleitet werden kann, dass die IDNr im lokalen Datenbestand des Data Providers gespeichert ist.
<p><strong>Tab. 20: Abruf der IDNr</strong></p>
<table><colgroup><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th><strong>von Komponente</strong></th>
<th><strong>nach Komponente</strong></th>
<th><strong>Beschreibung</strong></th>
<th>Ein-/Ausgabeparameter und Fehlerzust&auml;nde</th>
</tr>
<tr>
<td>DC</td>
<td>SAK-DC</td>
<td>
<ul>
<li>SAK-DC wird durch den Data Consumer &uuml;ber die synchrone Operation "getIdNr" der Consumer-API des SAK-DC aufgerufen.</li>
<li>SAK-DC &uuml;berpr&uuml;ft die&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung </a>(Ressource IDM f&uuml;r Personen) anhand des Zugriffstokens. Bei fehlgeschlagener Pr&uuml;fung wird die weitere Verarbeitung abgebrochen.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken "IAM f&uuml;r Beh&ouml;rden"</li>
<li>Basisdaten aus der Nutzer-Authentifizierung</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
</ul>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>IDM f&uuml;r Personen<br />(SAK-IDM-P)</td>
<td>
<ul>
<li>SAK-DC ruft die API "IDM f&uuml;r Personen" (3.3.1 Abrufersuchen mit Personendaten) (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten">XS-03</a>) mit den erhaltenen Eingabeparametern der Operation DC::getIDNr auf.</li>
<li>IDM f&uuml;r Personen (SAK-IDM-P) &uuml;berpr&uuml;ft die&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung.</a></li>
<li>Bei eindeutiger Identifizierung der nat&uuml;rlichen Person liefert das IDM f&uuml;r Personen deren IDNr undBasisdatenzur&uuml;ck.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken "IAM f&uuml;r Beh&ouml;rden"</li>
<li>Basisdaten aus der Nutzer-Authentifizierung</li>
</ul>
<p>Ausgabeparameter:</p>
<ul>
<li>Identifikationsnummer IDNr</li>
<li>Basisdaten</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Authentifizierung des SAK des Data Consumers mittels mTLS im Transportprotokoll war nicht erfolgreich.</li>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
<li>Wenn die Basisdaten aus der Nutzer-Authentifizierung zu keiner Person, mehreren Personen oder einer Person mit Auskunftssperre f&uuml;hren, wird eine neutrale Antwort zur&uuml;ckgegeben.</li>
</ul>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>DC</td>
<td>
<ul>
<li>Die Operation "getIdNr" der Consumer-API gibt die Ausgabeparameter zur&uuml;ck.</li>
</ul>
</td>
<td>
<p>Ausgabeparameter:</p>
<ul>
<li>Identifikationsnummer IDNr</li>
<li>Basisdaten</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>siehe Fehlerzust&auml;nde aus vorherigen Schritten</li>
</ul>
</td>
</tr>
</tbody>
</table>

#### 8.2.4 Schritt 4: Consumer-API::getNachweis

Diese Consumer-API liefert die Nachweisdaten auf Basis einer Nachweis-Anfrage und umfasst die folgenden Orchestrierungsaktivit&auml;ten:
<ol>
<li>Abstrakte Berechtigungspr&uuml;fung durch die Vermittlungsstelle (VS) ausf&uuml;hren,</li>
<li>Verbindungsparameter f&uuml;r den Data Provider aus der RDN ermitteln und</li>
<li>Nachweisabruf beim Data Provider ausf&uuml;hren.</li>
</ol>
<p><strong>Tab. 21: Abruf eines Nachweises</strong></p>
<table><colgroup><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>von Komponente</th>
<th>nach Komponente</th>
<th>Beschreibung</th>
<th>Ein-/ Ausgabeparameter und Fehlerzust&auml;nde</th>
</tr>
<tr>
<td>DC</td>
<td>SAK-DC</td>
<td>
<ul>
<li>DC erzeugt eine eindeutige RequestID (UUID) f&uuml;r die Nachweis-Anfrage.</li>
<li>DC ruft den SAK-DC &uuml;ber die synchrone Operation "getNachweis" der Consumer-API auf.</li>
<li>SAK-DC &uuml;berpr&uuml;ft die&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung </a>(Ressource Data Provider) anhand des Zugriffstokens. Bei fehlgeschlagener Pr&uuml;fung wird die weitere Verarbeitung abgebrochen.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken"IAM f&uuml;r Beh&ouml;rden"</li>
<li>ServiceID aus getNachweisangebot</li>
<li>XNachweis-Anfrage (siehe XNachweis-Spezifikation (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">XS-01</a>) f&uuml;r weitere Informationen)</li>
</ul>
<table>
<tbody>
<tr>
<td>
<ul>
<b>Auszug Nachweis-Anfrage (DE.EvidenceRequest.0101):</b>
<li>RequestID: Eindeutige ID f&uuml;r Nachweis-Anfrage</li>
<li>ProviderID (aus getNachweisangebot )</li>
<li>Daten zum Nachweissubjekt</li>
<li>Nachweistyp</li>
<li>Kommunikationszweck f&uuml;r die abstrakte Berechtigungspr&uuml;fung in der Vermittlungsstelle (in einer zuk&uuml;nftigenXNachweis-Version)</li>
<li>Anlass und Verarbeitungszwecke f&uuml;r die Protokollierung Datenschutzcockpit (in einer zuk&uuml;nftigen XNachweis-Version)</li>
<li>Vertrauensniveau aus der Nutzer-Authentifizierung</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Pr&uuml;fung der&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
</ul>
</td>
</tr>
<tr>
<td colspan="4">
<strong>4.1 Durchf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung durch die Vermittlungsstelle (VS)</strong>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>Vermittlungsstelle (SAK-VS)</td>
<td>
<ul>
<li>SAK-DC extrahiert aus der Nachweis-Anfrage die erforderlichen Eingabeparameter f&uuml;r den Aufruf der Vermittlungsstelle und als zus&auml;tzlicher Eingabeparameter wird der Hashwert f&uuml;r die XNachweis-Anfrage berechnet.</li>
<li>SAK-DC initiiert die Ausf&uuml;hrung der abstrakten Berechtigungspr&uuml;fung durch den Aufruf der API der Vermittlungsstelle.</li>
<li>Vermittlungsstelle (SAK-VS) &uuml;berpr&uuml;ft die <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens. Bei fehlgeschlagener Pr&uuml;fung wird die weitere Verarbeitung abgebrochen.</li>
<li>Vermittlungsstelle liefert im Falle einer positiven abstrakten Berechtigungspr&uuml;fung oder wenn keine abstrakte Berechtigungspr&uuml;fung erforderlich ist, ein Abruftoken an den SAK-DC zur&uuml;ck. Wenn kein Abruftoken geliefert wird, wird die Verarbeitung durch den SAK-DC abgebrochen.</li>
<li>SAK-DC speichert Abruftoken in der XNachweis-Anfrage.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken "IAM f&uuml;r Beh&ouml;rden"</li>
<li>ProviderID (aus getNachweisangebot)</li>
<li>RequestID (aus Nachweis-Anfrage)</li>
<li>Verwendung der IDNr
<ul>
<li>ja:wenn XNachweis-Anfrage eine IDNr enth&auml;lt, sonst "nein"</li>
</ul>
</li>
<li>Kommunikationszweck (NachweisTyp, Rechtsgrundlage)</li>
<li>Hash der XNachweis-Anfrage</li>
</ul>
<p>Ausgabeparameter:</p>
<ul>
<li>Abruftoken aus der Vermittlungsstelle (siehe Konzept Vermittlungsstelle (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-08</a>): Kapitel "Abruftoken")</li>
</ul>
<table>
<tbody>
<tr>
<td>
<ul>
<li>ConsumerID (Komponenten-ID des Data Consumer)</li>
<li>ProviderID (Komponenten-ID des Data Provider)</li>
<li>RequestID (Eindeutige ID der XNachweis-Anfrage)</li>
<li>Verwendung der IDNr</li>
<li>Kommunikationszweck</li>
<li>Hash-Wert des XNachweis-Anfrage (ohne Abruftoken)</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Authentifizierung des SAK des Data Consumers mittels mTLS im Transportprotokoll war nicht erfolgreich.</li>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
<li>
<p>Pr&uuml;fung des Hash-Werts in der Vermittlungsstelle ergibt, dass dieser bereits f&uuml;r die Beantragung eines Tokens verwendet wurde.</p>
</li>
<li>
<p>Eine abstrakte Berechtigung f&uuml;r den Kommunikationszweck zwischen Data Consumer und Data Provider liegt nicht vor.</p>
</li>
</ul>
<p>(siehe auch Kapitel "8.2.1 Anwendungsfall 1: Berechtigung pr&uuml;fen" im Konzept Vermittlungsstelle)</p>
</td>
</tr>
<tr>
<td colspan="4">
<strong>4.2 Ermittlung der Verbindungsparameter f&uuml;r den Data Provider aus der Registerdatennavigation</strong>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>Registerdaten-navigation (SAK-RDN)</td>
<td>
<ul>
<li>SAK-DC ermittelt die Verbindungsparameter des Data Provider &uuml;ber die RDN-API "findEvidenceServiceAddress".</li>
<li>Registerdatennavigation (SAK-RDN) &uuml;berpr&uuml;ft die&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens. Bei fehlgeschlagener Pr&uuml;fung wird die weitere Verarbeitung abgebrochen.</li>
<li>Die RDN liefert als Ergebnis die Liste von m&ouml;glichen Verbindungsparameter des Data Provider an den SAK-DC zur&uuml;ck.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken"IAM f&uuml;r Beh&ouml;rden"</li>
<li>ServiceID (ausgetNachweisangebot)</li>
</ul>
<p>Ausgabeparameter:</p>
<ul>
<li>Liste der Verbindungsparameter</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Authentifizierung des SAK des Data Consumers mittels mTLS im Transportprotokoll war nicht erfolgreich.</li>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
<li>F&uuml;r den Eingabeparameter "ServiceID" wurden kein NOOTS-Dienst oder entsprechende Verbindungsparameter gefunden.</li>
</ul>
</td>
</tr>
<tr>
<td colspan="4">
<strong>4.3 Nachweisabruf Data Provider</strong>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>SAK-DP</td>
<td>
<ul>
<li>SAK-DC w&auml;hlt anhand der SAK-Konfiguration einen Verbindungsparameter aus der verf&uuml;gbaren Liste aus.</li>
<li>SAK-DC ruft anhand der ausgew&auml;hlten Verbindungsparameter den SAK-DP &uuml;ber das Transportprotokoll auf.</li>
<li>Die Ende-zu-Ende-Verschl&uuml;sselung zwischen dem SAK-DC und SAK-DP wird durch das Transportprotokoll &uuml;ber mutual TLS umgesetzt. Die SAKs stellen also die Endpunkte dieser Ende-zu-Ende-Verschl&uuml;sselung dar.</li>
<li>SAK-DP f&uuml;hrt folgende Pr&uuml;fungen durch:&nbsp;<a href="#zugriffsberechtigung">Zugriffsberechtigung</a>, <a href="#validierung"> Validierung </a>und <a href="#abstrakte-berechtigung">abstrakte Berechtigung. </a>Die weitere Verarbeitung wird mit einer Fehlermeldung abgebrochen, sobald eine dieser Pr&uuml;fungen fehlschl&auml;gt.</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken"IAM f&uuml;r Beh&ouml;rden"</li>
<li>XNachweis-Anfrage</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>Authentifizierung des SAK des Data Consumers mittels mTLS im Transportprotokoll war nicht erfolgreich.</li>
<li>Pr&uuml;fung der <a href="#zugriffsberechtigung">Zugriffsberechtigung </a>anhand des Zugriffstokens war nicht erfolgreich.</li>
<li><a href="#validierung">Validierung </a>XNachweis-Anfrage ist fehlgeschlagen.</li>
<li>Abruftoken der <a href="#abstrakte-berechtigung">abstrakte Berechtigung</a> ist nicht g&uuml;ltig.</li>
</ul>
</td>
</tr>
<tr>
<td>
SAK-DP
</td>
<td>
DP
</td>
<td>
<p><em>bei Provider-API: aktiver Empf&auml;nger:</em></p>
<ul>
<li>DP hat aktiv eine HTTP-Verbindung zum SAK-DP im Long-Polling-Verfahren mit der Operation "warteAufAbruf" ge&ouml;ffnet</li>
<li>SAK-DP sendet die Nachweis-Anfrage im HTTP-Response der "warteAufAbruf"-Operation an den DP.</li>
</ul>
<p><em>bei Provider-API: passiver Empf&auml;nger </em>(Keine Darstellung im Sequenzdiagramm)</p>
<ul>
<li>SAK-DP leitet die Nachweis-Anfrage im HTTP-Request der "getNachweis"-Operation an den DP weiter</li>
</ul>
</td>
<td>
<p>Eingabeparameter:</p>
<ul>
<li>Zugriffstoken"IAM f&uuml;r Beh&ouml;rden"</li>
<li>XNachweis-Anfrage</li>
</ul>
</td>
</tr>
<tr>
<td>
DP
</td>
<td>
SAK-DP
</td>
<td>
Der Data Provider f&uuml;hrt die Schritte zur Bereitstellung eines Nachweises gem&auml;&szlig; Kapitel "5.2.1 Ablauf einer Nachweislieferung" des Konzepts "Anschlusskonzept Data Provider (DP)" durch.
<p><em>bei Provider-API: aktiver Empf&auml;nger:</em></p>
<ul>
<li>DP sendet die XNachweis-Antwort mit den Nachweisdaten im HTTP-Request der Operation "sendNachweis" der Provider-API an den SAK-DP.</li>
</ul>
<p><em>bei Provider-API: passiver Empf&auml;nger </em>(Keine Darstellung im Sequenzdiagramm)</p>
<ul>
<li>DP sendet die XNachweis-Antwort mit den Nachweisdaten im HTTP-Response der Operation "getNachweis" der Provider-API an den SAK-DP.</li>
</ul>
</td>
<td>
Ausgabeparameter:
<ul>
<li>XNachweis-Antwort (siehe XNachweis-Spezifikation (<a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">XS-01</a>) f&uuml;r weitere Informationen)</li>
</ul>
<table>
<tbody>
<tr>
<td>
<ul>
<li>ConsumerID (Komponenten-ID Data Consumer)</li>
<li>ProviderID (Komponenten-ID Data Provider)</li>
<li>(Gesiegelte) Nachweise mit verschl&uuml;sseltem Hash-Wert (Sieglung), Hash-Algorithmus, Signatur-Algorithmus und Zertifikat<br/>ODER</li>
<li>Wenn der Nachweis nicht innerhalb der vorgegebenen Antwortzeiten geliefert werden kann und das Nachrichtenmuster &bdquo;Request-Response/ Sync mit Polling&ldquo; durch den DP unterst&uuml;tzt wird, enth&auml;lt die Nachweis-Antwort einen Zeitstempel (&bdquo;ResponseAvailableDateTime&ldquo;). Dieser gibt an, ab wann der DC den Nachweisabruf erneut durchf&uuml;hren kann.</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>
<p>XNachweis-Anfrage kann vom DP nicht gelesen oder validiert werden.</p>
</li>
<li>
<p>Identifizierung des Nachweissubjektes anhand der IDNr oder Basisdaten ist nicht m&ouml;glich.</p>
</li>
<li>
<p>Zus&auml;tzliche Informationen &uuml;ber das Nachweissubjekt sind erforderlich, um den angeforderten Nachweis auszustellen. Der Data Consumer muss diese zus&auml;tzlichen Informationen bei einem erneuten Aufruf &uuml;bermitteln.</p>
</li>
<li>
<p>Nachweisdaten k&ouml;nnen nicht ausgestellt werden.</p>
</li>
</ul>
<p>(Details sind im Kapitel "5.2.1 Ablauf einer Nachweislieferung" des Konzepts "Anschlusskonzept Data Provider (DP)" zu finden)</p>
</td>
</tr>
<tr>
<td>SAK-DP</td>
<td>SAK-DC</td>
<td>
<ul>
<li>SAK-DP liefert die XNachweis-Antwort mit Nachweisdaten unver&auml;ndert &uuml;ber das Transportprotokoll an den SAK-DC.</li>
<li>Dabei wird die Nachweis-Antwort innerhalb derselben Ende-zu-Ende-verschl&uuml;sselten Kommunikationsverbindung wie die Nachweis-Anfrage &uuml;bertragen.</li>
</ul>
<p><em>bei Provider-API: aktiver Empf&auml;nger:</em></p>
<ul>
<li>SAK-DP quittiert die erfolgreiche &Uuml;bernahme der Daten im HTTP-Response von "sendNachweis" und die Zustellung an den SAK-DC</li>
</ul>
</td>
<td>
<p>Ausgabeparameter:</p>
<ul>
<li>XNachweis-Antwort (DE.EvidenceResponse.0102)</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>XNachweis.DE.EvidenceErrorResponse.0103</li>
</ul>
</td>
</tr>
<tr>
<td>SAK-DC</td>
<td>DC</td>
<td>
<ul>
<li>Die Operation "getNachweis" der Consumer-API gibt die Ausgabeparameter zur&uuml;ck.</li>
</ul>
</td>
<td><p>Ausgabeparameter:</p>
<ul>
<li>XNachweis-Antwort (DE.EvidenceResponse.0102) mit Nachweisdaten</li>
</ul>
<p>Fehlerzust&auml;nde:</p>
<ul>
<li>XNachweis-Fehler (DE.EvidenceErrorResponse.0103)</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>

## 9. Ausblick und Weiterf&uuml;hrende Aspekte

Die konzeptionelle Gestaltung der Transportinfrastruktur ist fortlaufend und wird in die n&auml;chsten Iterationen dieser Architekturdokumentation einflie&szlig;en. Dabei werden die im Folgenden beschriebenen weiterf&uuml;hrenden Aspekte ber&uuml;cksichtigt.
<p><strong>Tab. 22: Weiterf&uuml;hrende Aspekte</strong></p>
<table><colgroup><col /><col /><col /></colgroup>
<tbody>
<tr>
<th>ID</th>
<th>Thema</th>
<th>Erl&auml;uterung</th>
</tr>
<tr>
<td>NOOTS-939</td>
<td>&Uuml;bertragung gro&szlig;er Nachweisdaten vom Data Provider zum Data Consumer</td>
<td>
Im Rahmen des angestrebten RegMo-Reifegrads D1 &uuml;bermitteln Data Provider im Sinne der Datensparsamkeit lediglich die f&uuml;r eine Verwaltungsleistung tats&auml;chlich erforderlichen Nachweisdaten in maschinell auswertbarer Form an die Data Consumer. In diesem Kontext sind nur geringe Datenmengen f&uuml;r die Nachweisdaten zu erwarten. Es bedarf einer Pr&uuml;fung, ob Szenarien existieren, die eine &Uuml;bertragung gro&szlig;er Datenmengen f&uuml;r Nachweisdaten erfordern, und ob in solchen F&auml;llen die &Uuml;bertragung durch Streaming oder Splitting erfolgen sollte.
</td>
</tr>
<tr>
<td>NOOTS-1129</td>
<td>
Konkretisierung der Nachnutzung des SAK f&uuml;r zentrale NOOTS-Dienste
</td>
<td>
Konkretisierung der Nachnutzung des SAK f&uuml;r zentrale NOOTS-Dienste und Anwendungsf&auml;lle f&uuml;r den Abruf von Registerdaten f&uuml;r den Registerzensus sowie f&uuml;r die Wissenschaft (siehe auch Kapitel <a href="#72-nachnutzung-des-sak">Nachnutzung SAK</a>).
</td>
</tr>
<tr>
<td>NOOTS-1130</td>
<td>
<p>Schutz vor Manipulation der SAK-Auslieferungsartefakte durch Signierung</p>
</td>
<td>
<p>Pr&uuml;fung, ob der Schutz vor Manipulation der SAK-Auslieferungsartefakte (z. B. Container-Images) durch Signierung gew&auml;hrleistet werden kann. Das umfasst eine angemessene Infrastruktur, um die Signaturen der SAK-Auslieferungsartefakte zu verifizieren.</p>
</td>
</tr>
<tr>
<td>NOOTS-1131</td>
<td>
<p>Zuweisung der SAKs zu spezifischen Verwaltungsbereichen</p>
</td>
<td>
Kl&auml;rung der Frage, ob ein SAK Nachweisanfragen aus verschiedenen Verwaltungsbereichen bearbeiten darf oder ob jeder Sichere Anschlussknoten eindeutig einem Verwaltungsbereich zugeordnet werden muss.
</td>
</tr>
<tr>
<td>NOOTS-1132</td>
<td>
Bereitstellung des SAK als Open-Source-L&ouml;sung zur F&ouml;rderung von Transparenz und Vertrauen in der Gesellschaft.
</td>
<td>
Zur F&ouml;rderung von Transparenz und Vertrauen in der Gesellschaft sollte gepr&uuml;ft werden, inwieweit der SAK als Open-Source-L&ouml;sung implementiert werden kann.
</td>
</tr>
<tr>
<td>NOOTS-1133</td>
<td>
Mandantenf&auml;higes Management der NOOTS-Teilnehmer im SAK
</td>
<td>
Im SAK k&ouml;nnen mehrere NOOTS-Teilnehmer (auch als Mandanten bezeichnet) verwaltet werden. Es ist erforderlich, den Funktionsumfang zu definieren.
</td>
</tr>
<tr>
<td>NOOTS-833</td>
<td>
Die Transportinfrastruktur muss die Ermittlung der Transportendpunkte aller Ressourcen erm&ouml;glichen.
</td>
<td>
Der SAK des Data Consumers sollte &uuml;ber eine API des NOOTS-Diensteverzeichnisses (<a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads#architekturdokumente-noots">AD-NOOTS-07</a>) alle Endpunkte f&uuml;r die zentralen NOOTS-Dienste abrufen und in einem Cache zwischenspeichern.
</td>
</tr>
<tr>
<td>-</td>
<td>
Das Zugriffstoken soll nur f&uuml;r den Data Consumer funktionieren, an den es ausgegeben wurde.</p>
</td>
<td>
Das Zugriffstoken soll an die TLS-Verbindung (HTTPS) gebunden werden, die vom SAK des Data Consumers hergestellt wurde.
</td>
</tr>
<tr>
<td>-</td>
<td>
Begrenzung der Anzahl gleichzeitiger Anfragen an eine SAK-Instanz durch "Rate Limiting"-Mechanismus im SAK.
</td>
<td>
Lastverteilungskomponenten (Loadbalancer) werden verwendet, um die Last auf mehrere SAK-Instanzen in einem redundanten Deployment zu verteilen. Oft verf&uuml;gen sie &uuml;ber einen "Rate Limiting"-Mechanismus, um die Anzahl gleichzeitiger Anfragen zu begrenzen. Die Frage stellt sich, ob der SAK zus&auml;tzlich einen "Rate Limiting"-Mechanismus anbieten muss.
</td>
</tr>
<tr>
<td>-</td>
<td>
Das Kapitel Laufzeitsicht wird um weitere HLA-Anwendungsf&auml;lle erweitert.
</td>
<td>
Bei Bedarf werden neben dem HLA-Anwendungsfall 1a mit IDNr auch weitere HLA-Anwendungsf&auml;lle im Kapitel Laufzeitsicht beschrieben.
</td>
</tr>
</tbody>
</table>

## 10. &Auml;nderungsdokumentation

<table><colgroup><col /><col /><col /><col /><col /><col /><col /></colgroup>
<tbody>
<tr>
<th></th>
<th>Release-Versionen</th>
<th>Kapitel</th>
<th>Beschreibung der &Auml;nderung</th>
<th>Art der &Auml;nderung</th>
<th>Priorisierung/ major changes</th>
<th>Quelle der &Auml;nderung</th>
</tr>
<tr>
<td>1</td>
<td>Q1/2024</td>
<td>Allgemein</td>
<td>
<ul>
<li>
Erstellung der ersten Version
</li>
</ul>
</td>
<td>neuer Inhalt</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>2</td>
<td>Q2/2024</td>
<td>Allgemein</td>
<td>
<ul>
<li>Restrukturierung aufgrund der Standardisierung der Dokumentenstruktur f&uuml;r NOOTS-Grobkonzepte</li>
</ul>
</td>
<td>Aktualisierung</td>
<td></td>
<td>Komponententeam</td>
</tr>
<tr>
<td>3</td>
<td>Q2/2024</td>
<td>Bausteinsicht</td>
<td>
<ul>
<li>Konkretisierung des Funktionsumfanges der Bausteine "Sichere Anschlussknoten", "Anschlussprotokoll" und "Transportprotokoll".</li>
</ul>
</td>
<td>Aktualisierung</td>
<td>⭐</td>
<td>Komponententeam</td>
</tr>
<tr>
<td>4</td>
<td>Q2/2024</td>
<td>Laufzeitsicht</td>
<td>
<ul>
<li>Neues Sequenzdiagramm und die dazugeh&ouml;rigen Beschreibung f&uuml;r den vom Data Consumer initiierten Ablauf eines Nachweisabrufs f&uuml;r den HLA-Anwendungsfall "1a: Interaktiver Nachweisabruf zu einer nat&uuml;rlichen Person &uuml;ber das NOOTS"</li>
</ul>
</td>
<td>neuer Inhalt</td>
<td>⭐</td>
<td>Komponententeam</td>
</tr>
</tbody>
</table>

## Anhang A - Bestehende Transportinfrastrukturen

In diesem Anhang werden die Transportinfrastrukturen vorgestellt, aus denen das <a href="#711-abstraktes-architekturmodell">Abstrakte Architekturmodell</a> abgeleitet wurde. Dabei werden die im Architekturmodell gepr&auml;gten Begriffe genutzt, um die die Infrastrukturen leichter vergleichbar zu machen.

#### OSCI und XTA

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/17.png" alt="" />
<p><strong>Abb. 16: &Uuml;berblick Nachrichten&uuml;bermittlung mit OSCI und XTA (<a href="https://www.xoev.de/osci-xta/standard-xta-2-4835">XS-07</a>)</strong></p>
<p>Die OSCI Infrastruktur hat seit der Entwicklung im Jahr 2001 in Teilen der deutschen Verwaltung, insbesondere der Innenverwaltung und der Justiz, gro&szlig;e Verbreitung gefunden. Aufgrund der intensiven Nutzung im Meldewesen ist sie in praktisch allen Kommunen vorhanden. Sie basiert auf dezentralen OSCI Intermedi&auml;ren, welche Nachrichten von deren Autor entgegennehmen. Diese k&ouml;nnen vom Leser beim Intermedi&auml;r abgeholt werden oder durch den Intermedi&auml;r aktiv an den Leser zugestellt werden. F&uuml;r die Adressierung der Leser und den Austausch von Verbindungsparametern, insbesondere Zertifikaten, wird ein zentraler Verzeichnisdienst ben&ouml;tigt. In der Innenverwaltung &uuml;bernimmt das DVDV diese Rolle.</p>
<p>Seit 2012 befindet sich eine Erweiterung der Infrastruktur auf Basis des XTA Standards im Aufbau. Dabei handelt es sich im Kern um Anschlussknoten, die einen einfachen und einheitlichen Zugang zur Transportinfrastruktur erm&ouml;glichen sollen, ohne dass die Teilnehmer das recht komplexe OSCI Protokoll sprechen m&uuml;ssen. In einigen Bundesl&auml;ndern kommen XTA Implementierungen auch in der Rolle von Transportknoten vor. Sie werden dann nicht mehr bei den Autoren oder Lesern betrieben, sondern zentral in Landesrechenzentren. Aufgrund der ge&auml;nderten Rolle k&ouml;nnen sie die Aufgabe des Anschlussknotens nur noch bedingt &uuml;bernehmen, insbesondere sind sie ungeeignet als Enden der Ende-zu-Ende Verschl&uuml;sselung.</p>
<p>F&uuml;r das NOOTS ist die OSCI/XTA Infrastruktur nur bedingt geeignet. Die OSCI Intermedi&auml;re sind zun&auml;chst auf jeder Verbindungsstrecke nur einmal vorhanden, in der Regel im Transportmedium des Lesers. Damit erm&ouml;glichen sie ein Routing durch das Transportmedium des Lesers, k&ouml;nnen jedoch kein Routing &uuml;ber das Transportmedium des Autors unterst&uuml;tzen. F&uuml;r diesen Zweck wurde OSCI urspr&uuml;nglich auch nicht entwickelt. Der Schwerpunkt lag auf asynchronen Kommunikationsszenarien mit tempor&auml;r nicht verf&uuml;gbaren Empf&auml;ngern. Hier spielen Anforderungen wie Zustellgarantien und Nichtabstreitbarkeit eine gro&szlig;e Rolle, die wiederum im NOOTS derzeit nicht relevant sind. In den allermeisten F&auml;llen erfolgt die Kommunikation vom Autor zum OSCI Intermedi&auml;r daher &uuml;ber das Internet.</p>
<p><em>Request-Response Nachrichtenaustauschmuster</em></p>
<p>Das Nachrichtenaustauschmuster &bdquo;Request-Response, passiver Empf&auml;nger&ldquo; in OSCI verwendet Mechanismen aus den asynchronen Kommunikationsszenarien. Die Kommunikation zwischen dem Autor und Leser erfolgt immer &uuml;ber einen OSCI-Intermedi&auml;r und wird durch verschiedene Auftragsnachrichten umgesetzt: MessageId-Anforderungsauftrag, Dialoginitialisierungsauftrag, Abwicklungsauftrag, Bearbeitungsauftrag und Dialogendeauftrag. Die eingehenden Auftragsnachrichten werden vom OSCI-Intermedi&auml;r ggf. entschl&uuml;sselt und validiert. Neue Auftragsnachrichten werden ggf. signiert, verschl&uuml;sselt und an den Empf&auml;nger weitergeleitet.</p>
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/18.png" alt="" />
<p><strong>Abb. 17: Request-Response Nachrichtenaustauschmuster</strong> (Quelle: OSCI-Transport 1.2 mit integrierten Korrigenda (Fassung: 24. Februar 2021))</p>
<p>Erg&auml;nzt man dieses Bild um eine XTA Implementierung, die zentral im Transportmedium des Autors angesiedelt ist, kann diese im Zusammenspiel mit dem OSCI Intermedi&auml;r durchaus Nachrichten durch die Transportmedien von Autor und Leser zustellen. Dies ist jedoch nur gegeben, wenn zentrale XTA Implementierungen genutzt werden. H&auml;ufig werden XTA Implementierungen dezentral, nah bei Autor oder Leser betrieben. Sie k&ouml;nnen dann die Aufgaben der Anschlussknoten, nicht jedoch der Transportknoten &uuml;bernehmen. Damit kann die OSCI/XTA Infrastruktur die Anforderungen des NOOTS zwar erf&uuml;llen, jedoch nicht alle gleichzeitig.</p>
<p>Im Jahr 2022 wurde in NRW ein Praxistest OSCI durchgef&uuml;hrt, aus dem Handlungsempfehlungen abgeleitet wurden (siehe <a href="https://www.it-planungsrat.de/beschluss/beschluss-2022-30">IT-PLR-B-13</a>). In dem "Whitepaper Ende-zu-Ende-Verschl&uuml;sselung im Zusammenspiel OSCI - XTA" (<a href="https://www.mhkbd.nrw/system/files/media/document/file/whitepaper_ende_zu_ende_verschluesselung_osci-xta.pdf">SQ-26</a>) wurden festgestellt, dass die Verschl&uuml;sselung und Entschl&uuml;sselung von Daten bei OSCI und XTA unterschiedlich ausgepr&auml;gt ist. Im Jahr 2023 hat eine Studie zur Skalierbarkeit und Leistungsf&auml;higkeit von OSCI und XTA (siehe&nbsp;<a href="https://www.it-planungsrat.de/beschluss/beschluss-2023-38">IT-PLR-B-10</a>) aufgezeigt, dass die bestehende OSCI/XTA Infrastruktur zahlreiche Schw&auml;chen aufweist. So gibt es derzeit keine einzige XTA-Implementierung, die den XTA Standard vollumf&auml;nglich unterst&uuml;tzt. Die Implementierungen verschiedener Hersteller sind nicht interoperabel und die Unterst&uuml;tzung einzelner zu &uuml;bertragender Fachstandards - im NOOTS sind das XNachweis, XBasisdaten und XDatenschutzcockpit - erfordert Anpassungen an diesen. Auch die vom IDNrG geforderte Anbindung der Vermittlungsstellen ist derzeit weder in OSCI-Intermedi&auml;ren noch in XTA-Implementierungen vorhanden. Dar&uuml;ber hinaus wurde ein Teil der XTA Infrastruktur nicht dezentral, sondern jeweils zentral in den L&auml;ndern aufgebaut. Diese Infrastrukturelemente &uuml;bernehmen hier eher die Rolle von Transportknoten. Sie sind daher auch perspektivisch nur bedingt geeignet, die Rolle von Anschlussknoten zu &uuml;bernehmen.</p>
<p>In Teilen der Verwaltung, die bisher nicht auf OSCI setzen, gibt es erhebliche Widerst&auml;nde gegen OSCI/XTA. Der OSCI Transportstandard gilt als technologisch veraltet und schwer zug&auml;nglich, die verf&uuml;gbaren Infrastrukturbausteine als schwer zu betreiben und inperformant.</p>
<p>Es bleibt festzuhalten, dass OSCI/XTA zwar eine recht gut etablierte Transportinfrastruktur darstellt, die allerdings f&uuml;r einen anderen Zweck als synchrone Kommunikation entwickelt wurde und f&uuml;r die Anforderungen des NOOTS nur bedingt geeignet ist. Die OSCI Intermedi&auml;re k&ouml;nnten nachgenutzt werden, wo sie bereits vorhanden sind. Sie k&ouml;nnen die Anforderungen des NOOTS jedoch nicht alleine erf&uuml;llen, sondern m&uuml;ssen um weitere Elemente erg&auml;nzt werden. Die Nachnutzung der XTA Infrastruktur w&uuml;rde eine Ert&uuml;chtigung der Infrastruktur in der Fl&auml;che erfordern. Ihr starke Heterogenit&auml;t macht einen Nachnutzung im NOOTS schwierig. Voraussichtlich w&auml;re selbst bei entsprechender Ert&uuml;chtigung nur ein Teil der Infrastruktur daf&uuml;r geeignet.</p>

#### eDelivery AS4

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/19.png" alt="" />
<p><strong>Abb. 18: Nachrichten&uuml;bermittlung mittels eDelivery AS4 (<a href="https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/What+is+eDelivery">EU-EDM-03</a>)</strong></p>
<p>&Auml;hnlich wie in der deutschen Verwaltung, verf&uuml;gen auch viele Mitgliedsstaaten der EU &uuml;ber eigene Verwaltungsnetze und Transportinfrastrukturen, die zun&auml;chst abgeschottet voneinander sind und bisher durch Einzelma&szlig;nahmen gekoppelt wurden. Mit der Einf&uuml;hrung einer europaweiten Transportinfrastruktur auf Basis des auf ebMS basierenden AS4 Protokolls soll die Br&uuml;cke zwischen den nationalen Transportinfrastrukturen geschlagen und ein grenz&uuml;berschreitender Nachrichtenverkehr erm&ouml;glicht werden.</p>
<p>Die AS4 Infrastruktur basiert auf AS4 Access Points, welche die Rolle von Transportknoten innerhalb der Mitgliedsstaaten einnehmen. Anders als bei OSCI kommen dabei auf jeder Verbindungsstrecke zwei AS4 Access Points zum Einsatz: Einer im Land des Absenders einer Nachricht und einer im Land des Empf&auml;ngers. Dadurch k&ouml;nnen die AS4 Access Point vollst&auml;ndige Kontrolle &uuml;ber die Route grenz&uuml;berschreitender Nachrichten aus&uuml;ben. Die Kommunikation zwischen den Transportknoten erfolgt dabei &uuml;ber das Internet. Zentrale Dienste wie das DVDV werden in der ersten Ausbaustufe des EU-OOTS noch nicht angeboten. In sp&auml;teren Ausbaustufen soll jedoch ein Dienst auf Basis des OASIS Service Metadata Publishing Profils (SMP), einem Standard f&uuml;r die Ver&ouml;ffentlichung von Dienstmetadaten, zum Einsatz kommen.</p>
<p>Die AS4-Infrastruktur sieht keine expliziten Anschlussknoten vor. Dies liegt daran, dass die Zust&auml;ndigkeit f&uuml;r die Anschl&uuml;sse in der Verantwortung der Mitgliedsstaaten liegt und die EU in diesem Zusammenhang keine Vorgaben macht. Die AS4 Infrastruktur ist also gar nicht als einheitliche Transportinfrastruktur bis hin zu den Teilnehmern aus den Mitgliedsstaaten gedacht. Vielmehr koppelt sie bestehende Transportinfrastrukturen und &uuml;berl&auml;sst die Anschl&uuml;sse an die lokalen Transportinfrastrukturen den Mitgliedsstaaten.</p>
<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/20.png" alt="" />
<p><strong>Abb. 19: Grenz&uuml;berschreitender Datenverkehr &uuml;ber eDelivery AS4 Access Points</strong></p>
<p>Technologisch basiert AS4 auf ebMS 3.0 und SOAP und damit auf &auml;hnlich veralteten Technologien wie OSCI. &Auml;hnlich wie OSCI ist auch AS4 auf asynchrone Nachrichtenkommunikation ausgelegt. Es bietet Zustellgarantien und Mechanismen zur Sicherung der Nichtabstreitbarkeit, die f&uuml;r interaktive Nachweisabrufe nicht ben&ouml;tigt werden. Die Korrelation der Nachrichten f&uuml;r synchrone Kommunikationsmuster ist Aufgabe der Kommunikationspartner. Eine Ende-zu-Ende Verschl&uuml;sselung &uuml;ber die Access Points hinweg wird mangels einer europaweiten PKI nicht unterst&uuml;tzt.</p>
<p>F&uuml;r den Aufbau einer eDelivery AS4 Infrastruktur in Deutschland spricht aktuell wenig. Der verteilte Ansatz ist dem Ansatz von OSCI recht &auml;hnlich, m&uuml;sste jedoch von Grund auf neu aufgebaut werden. Zudem werden ben&ouml;tigte Funktionalit&auml;ten wie die Ende-zu-Ende Verschl&uuml;sselung derzeit nicht unterst&uuml;tzt. Perspektivisch k&ouml;nnte eine weitergehende Standardisierung von AS4 CEF eDelivery in Europa aber die Chance bergen, eine neue Transportinfrastruktur in Deutschland aufzubauen, die die Komplexit&auml;t und Heterogenit&auml;t der OSCI/XTA Infrastruktur vermeidet.</p>

#### X-Road

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/21.png" alt="" />
<p><strong>Abb. 20: Architektur&uuml;bersicht X-Road (<a href="https://x-road.global/architecture">SQ-22</a>)</strong></p>
<p>X-Road ist eine seit 2001 in Estland entwickelte Transportinfrastruktur, die mittlerweile in &uuml;ber 20 L&auml;ndern weltweit im Einsatz ist. Im Gegensatz zu den vorgenannten Infrastrukturen hat sie nie das Ziel verfolgt, getrennte Netze oder Transportinfrastrukturen zu verbinden. Daher bringt sie auch keine Transportknoten mit. Der Schwerpunkt liegt vielmehr auf einer effizienten und sicheren Punkt-zu-Punkt Kommunikation &uuml;ber das Internet. Das Nachrichtenrouting erfolgt alleine durch das Internet (OSI-Schicht 3: Vermittlung). Der Anschluss erfolgt &uuml;ber verpflichtende Anschlussknoten, die in X-Road "Security Server" genannt werden. Diese haben die Aufgabe, den Anschluss f&uuml;r die Teilnehmer m&ouml;glichst einfach und sicher zu gestalten. Erg&auml;nzt werden sie durch so genannte "Central Services", welche insbesondere zentrale Service Discovery &auml;hnlich der DVDV anbieten.</p>
<p>X-Road ist f&uuml;r die Verwendung im NOOTS nicht geeignet, da es das Problem der segmentierten Verwaltungsnetze nicht l&ouml;st und auch zuk&uuml;nftig nicht in der Lage ist, Nachrichten &uuml;ber die deutschen Verwaltungsnetze zuzustellen. Wesentliche Anforderungen an den Anschlussknoten, wie die Pr&uuml;fung des Vorliegens der abstrakten Berechtigung bei jedem Nachweisabruf mit IDNr zur Erf&uuml;llung der gesetzlichen Anforderungen, sind nicht erf&uuml;llt. Zudem ist X-Road nicht kompatibel mit den anderen NOOTS-Komponenten. Stattdessen m&uuml;sste mit den Central Services ein konkurrierendes Diensteverzeichnis eingef&uuml;hrt werden.</p>
<p>Auf der anderen Seite beweist X-Road, dass es mit geeigneter Transportinfrastruktur m&ouml;glich ist, auch &uuml;ber das Internet sicher zu kommunizieren. Zudem wurde es technologisch stetig weiterentwickelt und ist mit seiner REST-API (erg&auml;nzend zur &auml;lteren SOAP API) auf dem aktuellen Stand der Technik.</p>

#### FIT-Connect

<img src="https://gitlab.opencode.de/bmi/noots/-/raw/main/NOOTS-Dokumente/assets/Images19/media/22.png" alt="" />
<p><strong>Abb. 21: Kommunikation mit FIT-Connect (<a href="https://docs.fitko.de/fit-connect/docs/">SQ-23</a>)</strong></p>
<p>Im Unterschied zu den vorgenannten Transportinfrastrukturen ist FIT-Connect keine fachunabh&auml;ngige Transportinfrastruktur, sondern konkret auf die Einreichung vom Onlineantr&auml;gen im OZG Kontext ausgelegt. Es basiert auf einem zentralen Dienst, der Antr&auml;ge entgegennimmt und von dem die antragbearbeitenden Stellen diese abholen k&ouml;nnen. Dar&uuml;ber hinaus bringt FIT-Connect auch einen Routingdienst &auml;hnlich der Registerdatennavigation mit, &uuml;ber den die f&uuml;r einen Antrag zust&auml;ndige Stelle ermittelt werden kann.</p>
<p>Der zentrale Dienst ist nicht in der Lage, Nachrichten &uuml;ber verschiedene Verwaltungsnetze zu &uuml;bertragen. Stattdessen erfolgt der Transport ausschlie&szlig;lich &uuml;ber das Internet. Antragbearbeitende Stellen k&ouml;nnen sich direkt an den zentralen Dienst anbinden,um Antr&auml;ge abzuholen, oder &uuml;ber eine eigene Transportinfrastruktur, bspw. lokale Transportmechanismen der L&auml;nder. &Uuml;ber diese Konstellation k&ouml;nnen dann auch Antr&auml;ge &uuml;ber Netzgrenzen zugestellt werden. Allerdings muss dabei f&uuml;r jeden Netz&uuml;bergang eine individuelle L&ouml;sung gefunden werden.</p>
<p>Aufgrund der fachlichen Auslegung auf die Antragseinreichung sind bisher auch die API fachlich ausgepr&auml;gt. Das erm&ouml;glicht es den anzuschlie&szlig;enden Stellen, sich allein auf die Fachlichkeit der Antragseinreichung zu konzentrieren, ohne sich mit einer Transportschnittstellen besch&auml;ftigen zu m&uuml;ssen. F&uuml;r das NOOTS ist FIT-Connect derzeit nicht geeignet, da es eine die Netze &uuml;bergreifende Zustellung nicht unterst&uuml;tzt. Zudem stellt der zentralisierte Ansatz perspektivisch einen Flaschenhals dar, der die Skalierbarkeit der Infrastruktur auf Dauer einschr&auml;nkt und einen "Single Point of Failure" darstellt.</p>

## Anhang B - Nachrichtenverfolgung mit OpenTelemetry

OpenTelemetry ist ein Open-Source-Standard f&uuml;r die verteilte Nachrichtenverfolgung der Cloud Native Computing Foundation (CNCF) und der Nachfolger der beiden sich &uuml;berschneidenden Open-Source-Projekte f&uuml;r verteilte Nachrichtenverfolgung, OpenTracing und OpenCensus. Es bietet anbieterunabh&auml;ngige APIs, Software Development Kits und weitere Tools zum Erfassen von Daten f&uuml;r die verteilte Nachrichtenverfolgung sowie f&uuml;r weitere Performancemesspunkte. Anhand einer "trace-id" l&auml;sst sich das Verhalten von Nachweis- und Serviceanfragen &uuml;ber Komponentengrenzen hinweg nachverfolgen und Einblicke in deren Performance und Zustand gewinnen. Durch die "trace-id" k&ouml;nnen Protokollinformationen und Performancemesspunkte korreliert und ausgewertet werden. Die "trace-id" wird im Datenfluss bei Nachweis- und Serviceanfragen mitgegeben, wodurch eine komponenten&uuml;bergreifende Auswertung erm&ouml;glicht wird.<br />Bei einer Umsetzung mit OpenTelemetry kann der SAK folgende Funktionen f&uuml;r die verteilte Nachrichtenverfolgung anbieten:
<ul>
<li>"trace-id" wird in der Protokollierung der SAKs verwendet (weitere Details zur Protokollierung im n&auml;chsten Abschnitt).</li>
<li>Weitergabe des Tracing-Kontexts gem&auml;&szlig; W3C Trace Context (<a href="https://www.w3.org/TR/trace-context/#abstract">SQ-19</a>) zwischen NOOTS-Komponenten und NOOTS-Teilnehmern. Falls durch NOOTS-Teilnehmer kein Tracing-Kontext gesetzt wird, wird dieser durch den SAK erzeugt.</li>
<li>Optional: Export der Tracing-Informationen in eine zentrale Plattform (z.B.Jaeger) zur Nachrichtenverfolgung.</li>
</ul>
<p>&rarr; Weiterf&uuml;hrende Informationen finden sich in OpenTelemetry und W3C Trace Context (<a href="https://www.w3.org/TR/trace-context/#abstract">SQ-19</a>).</p>
<p>&nbsp;</p>