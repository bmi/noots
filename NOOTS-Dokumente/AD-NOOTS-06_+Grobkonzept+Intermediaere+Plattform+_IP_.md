>**Redaktioneller Hinweis**
>
>Dokument aus der ersten Iteration - nicht Teil der aktuellen Iteration.
Das Dokument stellt einen fortgeschrittenen Arbeitsstand dar, der wichtige Ergänzungen und Verbesserungen enthält. Die Finalisierung ist für das kommende Release geplant.

## Abstract

Das Grobkonzept *Intermediäre Plattform* beschreibt die
NOOTS-Komponente *Intermediäre Plattform* auf konzeptioneller Ebene.
Intermediäre Plattformen vermitteln zwischen dem *NOOTS* und dem
EU-OOTS, indem sie Nachweisabfragen aus dem EU-Ausland in nationale
Abfragen umwandeln und umgekehrt. Damit stellen die Intermediären
Plattformen eine bedeutende Komponente für die Umsetzung der
Verpflichtung zum Anschluss an das EU-OOTS gem. SDG-VO dar. Durch den
Einsatz Intermediärer Plattformen (IP) werden Funktionen für
grenzüberschreitenden Nachrichtenaustausch gebündelt, die sonst durch
die *Data Consumer* und *Data Provider* selbst erbracht werden müssten.

## Überblick

Um der Verpflichtung der **SDG**-Verordnung [(**EU-03**)](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724) zum
Nachweisaustausch mit dem EU-Ausland nachzukommen, hat der
IT-Planungsrat mit dem Beschluss 2022/34 vom 10.11.2022
([IT-PLR-E-06](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_EU-OOTS.pdf)) festgelegt, von der in der **SDG**-Verordnung
eingeräumte Möglichkeit sogenannter Intermediäre Plattformen Gebrauch zu
machen.

Intermediäre Plattformen dienen als Mittlerinnen zwischen
dem *NOOTS* und dem EU-**OOTS**, indem sie Nachweisabfragen aus dem
EU-Ausland in nationale Abfragen umwandeln und in
das *NOOTS* weiterleiten, und umgekehrt. Dank der Delegation der
EU-spezifischen Anforderungen und Aufgaben an Intermediäre Plattformen
müssen deutsche Data Provider und deutsche *Data Consumer* diese nicht
jeder einzeln umsetzen, sodass Umsetzungsaufwand eingespart und dadurch
eine zügigere Umsetzung des **SDG**-Anschlusses erreicht wird. Die
Nutzung von Intermediären Plattformen für **SDG**-anschlussverpflichtete
Behörden ist dabei verpflichtend; der direkte Anschluss der nationalen
Behörden an das EU-**OOTS** ist ausgeschlossen.

Aufgrund ihrer unterschiedlichen Aufgaben wird zwischen zwei Typen von
Intermediären Plattformen unterschieden, der Intermediären Plattform
für *Data Provider* <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-03_+High-Level-Architecture+_HLA_.md?ref_type=heads#user-content-anwendungsfall-3-abruf-von-nationalen-nachweisen-aus-eu-mitgliedstaaten-%C3%BCber-das-eu-oots" target="_blank">[Anwendungsfall 3 der HLA]</a> und der Intermediären Plattform für <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/website/docs/Glossar.md?ref_type=heads" target="_blank">*Data Consumer*</a> <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-03_+High-Level-Architecture+_HLA_.md?ref_type=heads#user-content-anwendungsfall-4-abruf-von-europ%C3%A4ischen-nachweisen-durch-nationale-data-consumer-%C3%BCber-das-europ%C3%A4ische-once-only-technical-system-eu-oots" target="_blank">[Anwendungsfall 4 der HLA]</a>. Ob in der Umsetzung beide
Typen in derselben technischen Komponente gebündelt oder getrennt
werden, ist, genau wie die Anzahl und der Zuschnitt der Intermediären
Plattform, von der vorliegenden fachlichen Konzeption unabhängig und aus
organisatorischer bzw. rechtlicher Perspektive zu entscheiden.

Intermediäre Plattformen müssen beim Nachweisaustausch die für das
EU-**OOTS** festgelegten Transportmechanismen verwenden. Für diese
Aufgabe wird in Deutschland ein bzw. mehrere sogenannte eDelivery Access
Points eingesetzt. Diese sind in einem eigenen Konzept
näher beschrieben und das Zusammenspiel in
der High-Level-Architecture im <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-03_+High-Level-Architecture+_HLA_.md?ref_type=heads#user-content-exkurs-nationale-anbindung-an-das-eu-oots" target= "_blank">Exkurs zur nationalen Anbindung an das EU-OOTS.</a>


## Rahmenbedingungen und Anforderungen des EU-OOTS

Der europäische Gesetzgeber fordert in der **SDG**-VO die Errichtung
eines technischen Systems für den grenzüberschreitenden Austausch von
Nachweisen (EU-**OOTS**), an das die zuständigen Behörden der
Mitgliedstaaten nach Artikel 14 der **SDG**-Verordnung angeschlossen
werden müssen. Die Anforderungen an das EU-**OOTS** werden durch die
zugehörige Durchführungsverordnung (EU) 2022/1463 ([**EU-01**](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX%3A32022R1463%3ADE%3AHTML)) zu
Artikel 14 der **SDG**-VO vom 05.08.2022 (im Folgenden kurz DVO) und die
begleitenden Technical Design Documents (TDD, [**EU-02**](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents)) weiter
konkretisiert. Sie lassen den Mitgliedstaaten erhebliche
Umsetzungsspielräume, auf Grundlage derer die nationale Anbindung an das
EU-**OOTS** konzipiert wurde.

Obgleich das *NOOTS* in Anlehnung an das EU-**OOTS** konzipiert wurde,
war eine vollständige Konvergenz beider Systeme in Hinblick auf Prozesse
und Technologien nicht möglich. Diese Differenzen sind von den
Intermediären Plattformen sowie von der Transportkomponente Access Point
auszugleichen, die den Übergang zwischen den beiden Systemen
(EU-**OOTS** und NOOTS) managen. Für den Anschluss an das
EU-**OOTS** zählen folgende Rahmenbedingungen und Anforderungen:

-   Anfragen werden ausschließlich aus Online-Verfahrensportalen (in
    der **SDG**-Verordnung eng. als "Online Procedure
    Portal" bezeichnet) ausgelöst, für deren Nutzung ein *Nachweis* zu
    erbringen ist.
-   Betrachtet werden ausschließlich synchrone Nachweisabrufe, die
    unmittelbar und vollautomatisiert beantwortet werden können.
-   Die grenzüberschreitende Kommunikation nutzt das *Exchange Data
    Model* (EDM, [**EU-EDM-01**](https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/4.1+-+Introduction+to+Exchange+Data+Model+and+Protocol+Snapshot+Q2)) als generischen
    Once-Only-Datenabrufstandard.
-   Auf Transportebene nutzt die grenzüberschreitende Kommunikation den
    Standard AS4 ([**EU-EDM-02**](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4+-+1.15)).
-   Das EU-**OOTS** verlässt sich darauf, dass nur zulässige und
    vertrauenswürdige Teilnehmer an die jeweiligen Access Points
    angebunden werden und hierüber einen Nachweisabruf vornehmen können.
    Eine auf den einzelnen Abruf bezogene Berechtigungsprüfung der
    Behörde findet nicht statt.
-   Da unterschiedliche Mitgliedstaaten den gleichen Sachverhalt
    (eng. "Requirement") durch unterschiedliche Nachweistypen
    dokumentieren, muss zunächst ermittelt werden,
    welcher *Nachweistyp* aus dem konkreten EU-Mitgliedstaat abgerufen
    werden soll. Dabei muss die Antwort nicht eindeutig sein und die
    Auswahl der tatsächlich gewünschten Nachweistypen kann eine
    Nutzerentscheidung erfordern.
-   Für die Ermittlung des notwendigen Nachweistyps und des
    zuständigen *Evidence* Providers stellt die Europäische Kommission
    die zentralen Verzeichnisdienste *Evidence* Broker und *Data Service
    Directory* zur Verfügung (im Folgenden auch eng. "Common
    Services" bezeichnet) .
-   Die abgerufenen Nachweise müssen den Nutzenden in der Regel zur
    Entscheidung angezeigt werden, ob sie diese für das Verfahren nutzen
    möchten. Dieser Vorgang wird als "Preview" bezeichnet. Ausnahmen von
    der Preview-Verpflichtung können nach Artikel 14 Abs.
    5 **SDG**-Verordnung im europäischen oder nationalen Recht definiert
    werden.

Hinweis zu den Rollenbegriffen in EU-**OOTS** und NOOTS: Im EU-**OOTS** werden im Wesentlichen zwei Rollen definiert, der Evidence Requester und der Evidence Provider. Diese Rollen werden in Deutschland durch die Intermediäre Plattform ausgefüllt. Da diese als Brücke ins NOOTS dient, nimmt sie gegenüber dem NOOTS die nationalen Rollen Data Consumer und Data Provider ein, siehe Abschnitt <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-03_+High-Level-Architecture+_HLA_.md?ref_type=heads#user-content-rollen-beim-nachweisabruf-%C3%BCber-das-eu-oots" target= "_blank">Rollen beim Nachweisabruf über das EU-OOTS</a> der HLA. Konkret heißt das: Ein europäischer Evidence Requester wendet sich mit einer Nachweisanfrage an eine Intermediäre Plattform, da diese für Deutschland den entsprechenden Data Service anbietet und somit gegenüber dem EU-Ausland als Evidence Provider auftritt. Um den Nachweis aus dem NOOTS abzufragen, nutzt die Intermediäre Plattform die dort definierten Prozesse und Technologien und nimmt in diesem Zuge national die Rolle des Data Consumers ein.<sup>(1)</sup> Für den <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-03_+High-Level-Architecture+_HLA_.md?ref_type=heads#user-content-anwendungsfall-4-abruf-von-europ%C3%A4ischen-nachweisen-durch-nationale-data-consumer-%C3%BCber-das-europ%C3%A4ische-once-only-technical-system-eu-oots" target= "_blank">[Anwendungsfall 4]</a> gilt dies spiegelbildlich.

<sup>(1)</sup> Welche Rechtsstellung der Intermediären Plattform hierfür notwendig
ist, wird aktuell noch im Programmbereich Recht und Datenschutz geklärt.

## Fachliches Konzept

### Fachliche Anforderungen

| ID                                                          | Anforderung                                                                                                                                                                                                     |                                                                                                                                                                                                                                                                               |
|-------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  | **Anforderungen an Intermediäre Plattformen für Data Provider**                                                                                                                                                                                                                | **Verweis auf Abb. 2 und ggf. Erläuterung**                                                                                                                                        |
| NOOTS-125                                                   | Die Intermediäre Plattform muss Nachweisabfragen von europäischen Evidence Requestern mit einem deutschen Nachweis oder einer Fehlermeldung beantworten.                                                        | Schritt 3200                                                                                                                                                                                                                                                                  |
| NOOTS-478                                                   | Die Intermediäre Plattform muss EDM-Nachrichten zu XNachweis-Nachrichten ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)) umwandeln können.                                                                                                            | Schritt 1700                                                                                                                                                                                                                                                                  |
| NOOTS-632                                                   | Die Intermediäre Plattform muss den Nutzerinnen und Nutzern eine menschenlesbare Vorschau (“Preview”) des Nachweises anzeigen.                                                                                  | Schritt 2600                                                                                                                                                                                                                                                                  |
| NOOTS-476                                                   | Die Intermediäre Plattform muss den Nutzerinnen und Nutzern nach Anzeige der Preview ermöglichen, die Verwendung des Nachweises freizugeben.                                                                    | Schritt 2700                                                                                                                                                                                                                                                                  |
| NOOTS-474                                                   | Die Intermediäre Plattform muss den Nutzerinnen und Nutzern aus dem EU-Ausland re-authentifizieren.                                                                                                             | Schritte 700-800 <p>Für diese Aufgabe leitet die Intermediäre Plattform die Nutzerinnen und Nutzer auf einen nationalen **eIDAS**-Server um. |                                                                 |
| NOOTS-614                                                   | Die Intermediäre Plattform muss sicherstellen, dass die Personendaten aus der ausländischen Nachweisanfrage mit denen des re-authentifizierten Nutzers übereinstimmen.                                          | Schritt 900 <p>Bei den Personendaten in der Nachweisanfrage handelt es sich entweder um die Daten des Nachweissubjekts selbst oder, falls zutreffend, um die Daten der vertretungsberechtigten Person. | 
| NOOTS-480                                                   | Die Intermediäre Plattform muss bei der Registerdatennavigation den zuständigen deutschen Data Provider und dessen Verbindungsdaten abfragen.                                                                   | Schritte 1000-1600                                                                                                                                                                                                                                                            |
| NOOTS-532                                                   | Die Intermediäre Plattform muss in ihrer Rolle als Evidence Provider die in den aktuellen EU-TDD für Evidence Provider beschriebenen Anforderungen erfüllen, darunter nicht-funktionale.                        | -                                                                                                                                                                                                                                                                             |
| NOOTS-533                                                  | Die Intermediäre Plattform muss in ihrer Rolle als nationaler Data Consumer die im NOOTS geltenden Anforderungen an Data Consumer erfüllen, darunter nicht-funktionale.                                         | Siehe "Anschlusskonzept Data Consumer" <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-01]</a> |
|| **Anforderungen an Intermediäre Plattformen für Data Consumer** | **Verweis auf Abb. 3 oder Abb. 4, und ggf. Erläuterung** | 
| NOOTS-473                                                   | Die Intermediäre Plattform muss Nachweisanfragen aus dem Inland mit dem Nachweis eines europäischen Evidence Providers oder einer Fehlermeldung beantworten.                                                    | Abb 3: Schritt 2200                                                                                                                                                                                                                                                               |
| NOOTS-509                                                   | Die Intermediäre Plattform muss auf Grundlage einer eigehenden XNachweis-Nachricht ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)) und weiterer Daten eine EDM-Nachricht erstellen können.                                                            | <p>A: Schritte 900-1000</p> <p>Die “weiteren Daten” werden durch Abfragen der Common Services sowie durch Nutzereingaben erhoben und umfassen: <ul><li>gewünschten Nachweistyp <li>gewünschten Evidence Provider <li>Explicit request <li>Land des gewünschten Nachweises <li>ggf. Zusatzattribute zur Bestimmung des Evidence Providers</ul>  |
| NOOTS-472                                                   | Die Intermediäre Plattform muss vom Evidence Broker die benötigten nicht-deutschen Nachweistypen abfragen.                                                                                                      | Abb. 4: Schritte 801 und 805                                                                                                                                                                                                                                                  |
| NOOTS-475                                                   | Die Intermediäre Plattform muss vom Data Service Directory den zuständigen nicht-deutschen Evidence Provider abfragen.                                                                                          | Abb. 4: Schritt 809                                                                                                                                                                                                                                                           |
| NOOTS-479                                                   | Die Intermediäre Plattform muss eine Nutzeroberfläche zur Eingabe von Daten zur Ermittlung des benötigten Nachweistyps und Evidence Providers bereitstellen.                                                    | Abb. 4: Schritte 803, 807, 811 und 816                                                                                                                                                                                                                                        |
| NOOTS-481                                                   | Die Intermediäre Plattform muss bei eingehenden Nachweisanfragen prüfen, ob sie von einem zuvor registrierten Data Consumer stammen, und nur diese beantworten.                                                 | <p>Nicht in Diagramm!</p> Die Registrierung erfolgt in der zentralen NOOTS-Komponente IAM für Behörden <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-05]</a>. Bis diese zur Verfügung steht, kann die Intermediäre Plattform ein eigenes Zugriffsmanagement einrichten. | 
| NOOTS-531                                                   | Die Intermediäre Plattform muss in ihrer Rolle als Evidence Requester die in den aktuellen EU-TDD für Evidence Requester beschriebenen Anforderungen erfüllen, darunter nicht-funktionale.                      | -                                                                                                                                                                                                                                                                             |
| NOOTS-534                                                   | Die Intermediäre Plattform muss in ihrer Rolle als nationaler Data Provider die im NOOTS geltenden Anforderungen an Data Provider erfüllen, darunter nicht-funktionale.                                         | Siehe "Anschlusskonzept Data Provider" <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-02]</a>|
| NOOTS-477                                                   | Die Intermediäre Plattform muss für die Kommunikation mit europäischen Evidence Requestern und Evidence Providern die europäische Transportinfrastruktur via dem national bereitgestellten Access Point nutzen. | *Nicht in Diagramm* aus Gründen der Übersichtlichkeit                                                                                                                                                                                                                           |


### **Facharchitektur**

Um die oben genannten fachlichen Anforderungen zu erfüllen, muss
die *Intermediäre Plattform* mindestens den in Abbildung 31
dargestellten und in Tabelle 54 erläuterten Funktionsumfang umfassen.
Der gezeigte Komponentenaufbau ist dabei nicht als normativ zu
verstehen. Auch eine andere interne Strukturierung einer Intermediären
Plattform ist zulässig, solange die gleiche Funktionalität erreicht
wird.

#### Gesamt-Architektur für grenzüberschreitende Nachweisabrufe mit Intermediärer Plattform

![C:\\f6a968aea64d284562a1344e855e9074](./assets/images6/media/image1.png)
<br>**Abb. 1: Gesamt-Architektur für grenzüberschreitende Nachweisabrufe mit Intermediärer Plattform**</br>


| Teilkomponente *(relevante Anforderung aus Fachliche Anforderungen an Intermediäre Plattformen)*                                                                           | Erläuterung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|-------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|| **Teilkomponenten Intermediäre Plattform für Data Provider**                                  | 
| Annahme der Anfrage (NOOTS-532)                                                                       | <p>Diese Teilkomponente hat die Aufgabe, Anfragen (EDM-Requests) aus dem EU-**OOTS** (via eDelivery Access Points) entgegenzunehmen und in die weitere Bearbeitung zu steuern.</p> <p>Diese Teilkomponente übernimmt ebenfalls die Protokollierung gemäß Artikel 17 **SDG**-DVO.</p> Dieser Dienst entspricht der in den EU-TDD als Evidence Query Service Handler bezeichneten Funktion. | 
| Abgleich Personendaten (NOOTS-614)                                                                    | In dieser Teilkomponente werden die Daten zum Nachweissubjekt (bzw. der vertretungsberechtigten Person) aus dem Request mit den Personendaten aus der Re-Authentisierung abgeglichen. Nur wenn sie übereinstimmen, darf fortgefahren werden. Der Grad der Übereinstimmung und das Verfahren bei nicht-Übereinstimmung wird zukünftig im übergreifenden Konzept “Nutzeridentifizierung, -authentifizierung und Datensatzabgleich” <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-14]</a> spezifiziert. |
| Umwandlung EDM <-> XNachweis (NOOTS-478)                                                             | “Umwandlung EDM <-> XNachweis” wandelt die eingegangene Anfragenachricht im EDM des EU-OOTS in eine Anfrage im NOOTS-Standard XNachweis um. Bei der Antwort geschieht das umgekehrte. |   
| Ermittlung Routingdaten (NOOTS-480)                                                                  | Diese Teilkomponente ermittelt auf Grundlage des benötigten Nachweistyps und weiterer Routingparameter den für die Nachweisanfrage zuständigen (ggf. dezentralen) Data Provider im NOOTS und dessen Verbindungsparameter mittels der NOOTS-Komponente Registerdatennavigation <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-07]</a>.  |   
| Abfrage Nachweis (NOOTS-533)                                                                           | Diese Teilkomponente versendet eine NOOTS-konforme XNachweis-Anfrage an den Data Provider und hält dabei die für das NOOTS geltenden Anforderungen für Data Consumer in den Bereichen Sicherheit, Autorisierung und Signierung ein <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">Data Consumer Anschlusskonzept</a>. | 
| Bereitstellung Preview (NOOTS-632)                                                                     | Vorbereitung der Anzeige der Nachweisdaten in menschenlesbarer Form im Preview-Space. Dies geschieht entweder, indem die Intermediäre Plattform die vom Data Provider erhaltenen Daten aus der Nachricht des Data Providers extrahiert und unverändert anzeigt, oder indem sie einen Dienst einbettet, der die Daten anhand von Formatvorlagen menschenlesbar aufbereitet.    | 
| Nutzeroberfläche/ “Preview-Space” (NOOTS-476)                                                        | <p>Die Nutzeroberfläche der Intermediären Plattform. Hier werden je nach Bedarf verschiedene Nutzerdialoge durchgeführt:</p> <ul><li>Immer: Nutzer Re-Authentifizierung, bei der auf weitere Komponenten (z.B. eID-Server) zurückgegriffen wird. <li>Bei Bedarf: “Zusatzattribute Routing” für die Ermittlung des zuständigen Data Providers. Die ermittelten Zusatzattribute werden von der Intermediären Plattform zur Abfrage der Registerdatennavigation genutzt. <li>Bei Bedarf: “Zusatzattribute Identifikation” für weitere Angaben, die der Data Provider zur Ermittlung des Datensatzes des Nachweissubjekts im Register benötigt. Über diese erhält die Intermediäre Plattform entweder über die Registerdatennavigation Kenntnis oder über eine Fehlermeldung vom Data Provider.<li>Im Normalfall Preview und Freigabe: Die Nutzerinnen und Nutzer werden gefragt, ob sie der Verwendung des Nachweises wie in der Preview angezeigt zustimmen. Die Entscheidung wird gespeichert und gemäß Artikel 17 Abs. 2 **SDG**-DVO protokolliert.</ul>  | 
| Übermittlung der Nachweise (NOOTS-125)                                                                 | <p>Die vom Data Provider gelieferten Nachweise werden in einer EDM-Nachricht an den Evidence Requester übermittelt. Dieser Vorgang wird protokolliert gemäß Artikel 17 **SDG**-DVO.</p> Alternativ wird als Antwort auf die Nachweisanfrage eine Fehlermeldung gesendet. |
| | **Teilkomponenten Intermediäre Plattform für Data Consumer**                                  |  
| Annahme der Anfrage (NOOTS-534)                                                                      | <p>Annahme der Anfrage von einem Data Consumer aus dem Inland.</p> Diese Teilkomponente übernimmt dabei die Protokollierung und Berechtigungsprüfung der eingehenden Anfrage gemäß der auf Intermediäre Plattformen zutreffenden Anforderungen für NOOTS Data Provider, siehe “Anschlusskonzept Data Provider” <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-02]</a>. |
| Ermittlung Nachweistyp und Data Service (NOOTS-472, NOOTS-475)                                                   | <p>Durch eine Abfrage des Evidence Brokers ermittelt diese Teilkomponente zunächst die für das betroffene Verfahren benötigten “Sachverhalte“ („Requirements”).</p> <p>Anschließend werden pro Requirement die für das gewünschte Herkunftsland des Nachweises zuständigen Verzeichnisse (zentrale oder nationale Evidence Broker bzw. DSD) ermittelt. Von diesen werden Nachweistyp sowie die Kontaktdaten des zuständigen Data Services ermittelt (vgl. Sequenzdiagramm in Abb. 4).</p> Im Kontext dieser Abfragen können Nutzereingaben erforderlich werden, welche die Teilkomponente “Nutzeroberfläche der Intermediären Plattform” erfasst. | 
| Nutzeroberfläche der Intermediären Plattform (NOOTS-479)                                              | <p>Je nach Bedarf werden in Nutzerdialogen folgende Informationen abgefragt, um den EDM-Request zu vervollständigen:</p> <ul><li>Auswahl, für welches/welche Requirements Nachweis(e) aus dem EU-Ausland gewünscht werden, und ggf. Ergänzung des Nachweis-Herkunftslands pro Requirement <li>Auswahl des gewünschten Nachweistyps und Erteilung “explicit request“ pro nachzuweisendem „Requirement” <li>Zusatzattribute zur Ermittlung des zuständigen Evidence Provider <li>Auswahl eines Evidence Providers.</ul> |   
| Umwandlung XNachweis <-> EDM (NOOTS-509)                                                                               | Auf Basis der Daten in der erhaltenen XNachweis-Nachricht sowie weiterer von den Nutzerinnen und Nutzern angegebenen Informationen und Daten aus den Common Services Evidence Broker ([EU-EB-01](https://oots.pages.code.europa.eu/tdd/apidoc/evidence-broker/)) und DSD ([EU-DSD-01](https://oots.pages.code.europa.eu/tdd/apidoc/data-services-directory/)), wird eine EDM-konforme Nachweisanfrage vom Nachrichtentyp QueryRequest erzeugt. | 
| Anfrage Nachweis (NOOTS-531)                                                                          | <p>Diese Teilkomponente versendet einem EDM-konformen QueryRequest an den Evidence Provider im EU-Ausland. Dies wird gemäß Artikel 17 SDG-DVO protokolliert.</p> Die Übermittlung der Anfrage erfolgt über einen deutschen Access Point <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-04]</a> über die von ihm definierte Schnittstelle und Mechanismen. | 
| Übermittlung der Nachweise (NOOTS-473)                                                               | <p>Weiterleitung der Antwort – im Erfolgsfall inklusive des Nachweises – im XNachweis-Standard an den Data Consumer.</p> Alternativ wird eine Fehlermeldung zurückgegeben. | 

## **Beschreibung der Funktionen im Einzelnen**

### Anforderungen an Intermediären Plattformen für Data Provider

Die Kernaufgabe der Intermediären Plattform besteht darin,
Nachweisanfragen (Nachricht *QueryRequest*) aus dem EU-Ausland zu
beantworten. Zulässige Antwortnachrichten des *Evidence* Providers sind
dabei laut EU-TDD (Zusatz "EDM"- ergänzt zur besseren Unterscheidung zu
XNachweis-Nachrichten):

 | Nachrichten der Intermediären Plattform Richtung EU-OOTS | Von Intermediärer Plattform gesendet oder empfangen | Beschreibung                                                                                                                                              |
|----------------------------------------------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| EDM-QueryRequest                                         | empfangen                                           | Anfrage eines Nachweises                                                                                                                                  |
| EDM-QueryResponse                                        | senden                                              | Übermittlung des angefragten Nachweises                                                                                                                   |
| EDM-QueryResponseError mit Exception                     | senden                                              | Fehlermeldung mit verschiedenen Fehlertypen, die um weitere Datenslots ergänzt werden, z.B. mit Preview-URL zum Preview-Space der Intermediären Plattform |


In ihrer Rolle als nationaler *Data Consumer* sendet und empfängt die
Intermediäre Plattform folgende Nachrichten, die in der \"Spezifikation
XNachweis" ([XS-01](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)) näher beschrieben werden:

 | Nationale Nachrichten der Intermediären Plattform Richtung NOOTS | Von Intermediärer Plattform gesendet oder empfangen | Beschreibung                                                      |
|------------------------------------------------------------------|-----------------------------------------------------|-------------------------------------------------------------------|
| XNachweis-EvidenceRequest                                        | senden                                              | Anfrage eines Nachweises von einem nationalen Data Provider       |
| XNachweis-EvidenceResponse                                       | empfangen                                           | Übermittlung des Nachweises                                       |
| XNachweis-ErrorResponse                                          | empfangen                                           | Fehlermeldung, weil der Nachweis nicht bereitgestellt werden kann |


### **Ablauf eines Nachweisabrufs aus dem EU-OOTS**

Die fachlichen Schritte des Nachweisabrufs sind in der High-Level-Architecture <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-03]</a>
beschrieben. Ergänzend dazu zeigt das folgende Sequenzdiagramm einen
möglichen Nachrichtenfluss zwischen den beteiligten Akteuren unter
Verwendung der zuvor vorgestellten Nachrichten. Schritte im Ausland sind
nur dargestellt, wenn sie eine direkte Schnittstelle zu einem der
NOOTS-Akteure haben.

> *Hinweis*: Gestrichelte Pfeile stehen für die Verwendung von HTTP als
> Protokoll, während die durchgezogenen für XNachweis bzw. EDM-Nachrichten
> bedeuten.


![C:\\89280deafa17429a22a9c4b92457bd95](./assets/images6/media/image2.png)
<br>**Abb. 2: Ablauf Nachweisanfrage durch ausländischem Evidence Requester von nationalem Data Provider**</br>


### **Erläuterung zur Funktionalität der Preview**

Das EU-OOTS sieht als Regelfall vor, dass die Nutzenden
den *Nachweis* vor der grenzüberschreitenden Übermittlung einsehen und
die Verwendung des Nachweises freigeben müssen (Preview, siehe Artikel
14 Abs. 3 lit. f SDG-VO). Prinzipiell sind für die Implementierung der Preview auf der Intermediären Plattform
zwei Szenarien denkbar:

1.  Der *Data Provider* liefert ein menschenlesbares Format des
    Nachweises, i.d.R. ein PDF. Dieses kann die *Intermediäre
    Plattform* unverändert als Preview anzeigen. Dieses Vorgehen ist im
    Zielbild 2023 zu erwarten, wird aber möglicherweise sukzessive um
    Szenario 2 ergänzt, wenn weitere Register angeschlossen werden.
2.  Der *Data Provider* liefert ein strukturiertes Datenformat. Dieses
    muss die *Intermediäre Plattform* mithilfe eines Diensts gemäß
    Formatvorlagen menschenlesbar aufbereitet werden.

Die Perspektive, dass ein EU-weit harmonisiertes Datenformat vom *Data
Provider* geliefert und entsprechend durch die *Intermediäre
Plattform* menschenlesbar aufbereitet werden müsste, liegt nicht
innerhalb des zeitlichen Rahmens des Zielbilds 2028.

Ausnahmen von der Preview: Es kann Datenabrufe geben, in denen die
Notwendigkeit der Preview durch europäisches oder nationales Recht
ausgeschlossen wurde (siehe Artikel 14 Abs. 5 SDG-VO); dies wird
vom *Evidence Requester* in der initialen Anfragenachricht
gekennzeichnet. In diesem Fall kann die *Intermediäre
Plattform* grundsätzlich bereits die erste Anfrage unmittelbar mit der
Übermittlung des betreffenden Nachweises beantworten. Wenn
die *Intermediäre Plattform* aber Zusatzangaben der Nutzenden benötigt,
um das richtige Register für den Datenabruf zu ermitteln, kann auch in
diesen Fällen eine Weiterleitung der Nutzenden auf die Nutzeroberfläche
("Preview-Space") der Intermediären Plattform angefordert werden
(Artikel 14 Abs. 2 i.V.m. Artikel 11 Abs. 3 DVO). Die Nutzenden können
diese Weiterleitung ablehnen; es ist jedoch zulässig, dass der
Nachweisabruf in diesen Fällen auch fehlschlagen kann.

### **Aufgaben der Intermediären Plattform in Abgrenzung zu denen der Data Provider oder registerführenden Stellen**

-   Obwohl die *Intermediäre Plattform* Nachweise nach den Regeln des
    EU-OOTS ausliefert (Data Service im Sinne des EU-OOTS), hält sie die
    Nachweisdaten nicht selbst vor, sondern fordert sie anlassbezogen
    über das *NOOTS* von der Stelle an, die im *NOOTS* für die
    Bereitstellung dieser Nachweise zuständig ist. Damit verbleiben die
    Aufgaben der originären *Datenhaltung und -pflege* bei den
    eigentlichen registerführenden Stellen. Diese verantworten die
    fachliche Richtigkeit der Daten.
-   Die Aufgabe der *Datenpräsentation* wird bei SDG-Abrufen in Form der
    Preview von der Intermediären Plattform übernommen. Obgleich sie
    nicht für die fachliche Richtigkeit der Nachweisdaten verantwortlich
    ist, muss sie sicherstellen, dass die vom Register gelieferten Daten
    korrekt angezeigt werden und bei etwaigen Umwandlungsprozessen keine
    inhaltlichen Änderungen geschehen.
-   Der *Data Provider* im *NOOTS* ist verantwortlich für
    die *Bereitstellung der Daten*. Dies wird in der Regel von der
    registerführenden Stelle selbst übernommen, kann jedoch -- anders
    als Datenhaltung- und pflege -- an zentrale Strukturen, wie
    Abrufportale oder Spiegelregister delegiert werden, sofern die
    rechtlichen Voraussetzungen dafür existieren.

### **Registrierung der Data Services der Intermediären Plattform im Data Service Directory**

Bei verteilten Registerlandschaften wie in Deutschland können häufig
mehrere *Data Provider* die gleichen Typen von Nachweisen ausstellen,
und es muss im Einzelfall ermittelt werden, welcher *Data Provider* für
die konkrete Anfrage zuständig ist. Dies kann sich z.B. bei einer
regionalen Gliederung aus dem Wohnort der betroffenen Person ableiten,
aber auch komplexeren Zuständigkeitslogiken folgen (siehe zu einer
ausführlicheren Erläuterung dieser Herausforderung im innerstaatlichen
Kontext auch das Grobkonzept Registerdatennavigation, AD-NOOTS-07).

Damit diese Komplexität nicht im *Data Service
Directory* ([EU-DSD-01](https://oots.pages.code.europa.eu/tdd/apidoc/data-services-directory/)) der EU aufwändig zu pflegen ist und
Antragstellende nicht aus einer Vielzahl von möglichen Providern
auswählen müssen, wurde für Deutschland entschieden, dass im DSD allein
die Data Services der Intermediären Plattform(en) hinterlegt werden. Die
konkrete Zuständigkeitsermittlung mittels Registernavigation, ggf. unter
Verwendung von zusätzlichen Angaben der Nutzenden, findet national auf
der Intermediären Plattform selbst statt.

Solange keine andere Regelung zur Pflege getroffen wird, hat der
Betreiber einer Intermediären Plattform dafür Sorge zu tragen, dass die
Daten zu den von ihr bereitgestellten Data Services im DSD jederzeit
aktuell sind.

## **Anforderungen an Intermediäre Plattformen für Data Consumer**

Die Kernaufgabe der Intermediären Plattform für *Data Consumer* besteht
darin, Nachweisanfragen aus dem Inland zu beantworten. Dazu tauscht sie
folgende Nachrichten mit dem *Data Consumer* aus, die in der
XNachweis-Spezifikation ([\[XS-01\]](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)) im Detail definiert werden
(Zusatz "XNachweis"- ergänzt zur besseren Unterscheidung zu
EDM-Nachrichten). Zu beachten ist, dass die
Nachrichten* EvidenceOrder* und *GetEvidences* mit ihren jeweiligen
Responses in der XNachweis-Spezifikation XNachweis-Version 0.1 zwar
erwähnt, aber noch nicht ausspezifiziert sind. Bei den Namen handelt es
sich um Arbeitstitel.

 | Nachrichten der Intermediären Plattform | Von Intermediärer Plattform gesendet oder empfangen | Beschreibung                                                                                  |
|-----------------------------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------------|
| XNachweis-EvidenceOrder                 | empfangen                                           | Beauftragung eines EU-**OOTS**-Nachweisabrufs für ein Verfahren (Procedure)                       |
| XNachweis-EvidenceOrderResponse         | senden                                              | Übermittlung der Redirect-URL zu Intermediären Plattform                                      |
| XNachweis-EvidenceOrderResponseError    | senden                                              | Fehlermeldung, weil XNachweis-EvidenceOrder nicht beantwortet werden kann                     |
| XNachweis-GetEvidences                  | empfangen                                           | Abfrage eines Nachweises in Verbindung mit einer laufenden EU-**OOTS**-Nachweisabruf-Beauftragung |
| XNachweis-GetEvidencesResponse          | senden                                              | Übermittlung des Nachweises                                                                   |
| XNachweis-GetEvidencesResponseError     | senden                                              | Fehlermeldung, falls XNachweis-GetEvidences nicht beantwortet werden kann                     |


Die *Intermediäre Plattform* muss im nächsten Schritt die erhaltenen
Nachweisanfragen um Informationen aus den Common Services anreichern.
Für die Kommunikation mit den Common Services der EU verwendet sie die
in den TDD ([EU-02](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents)) definierten Nachrichten, die heißen:

-   Evidence Broker: Get List of Requirements ([EU-EB-02](https://oots.pages.code.europa.eu/tdd/apidoc/evidence-broker/get-requirements))
-   Evidence Broker: Get Evidence Types ([EU-EB-03](https://oots.pages.code.europa.eu/tdd/apidoc/evidence-broker/get-evidence-types))
-   Data Service Directory: Find Data Services of Evidence Providers
    ([EU-DSD-02](https://oots.pages.code.europa.eu/tdd/apidoc/data-services-directory/find-data-services))

Anschließend tauscht sie in ihrer Rolle als *Evidence Requester* die im
EDM definierten Nachrichten mit den *Evidence Provider* im EU-Ausland
aus.

 | Nachrichten der Intermediären Plattform Richtung EU-OOTS | Von Intermediärer Plattform gesendet oder empfangen | Beschreibung                                                                                                                                                       |
|----------------------------------------------------------|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| EDM-QueryRequest                                         | senden                                              | Anfrage eines Nachweises                                                                                                                                           |
| EDM-QueryResponse                                        | empfangen                                           | Übermittlung des angefragten Nachweises                                                                                                                            |
| EDM-QueryResponseError mit Exception                     | empfangen                                           | Fehlermeldung mit verschiedenen Fehlertypen, die um weitere Datenslots ergänzt werden, z.B. mit Preview-URL zum Preview-Space des ausländischen Evidence Providers |


### **Ablauf eines Nachweisabrufs aus dem NOOTS**

Die fachlichen Schritte des Nachweisabrufs sind in der
High-Level-Architecture <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-03]</a> beschrieben. In dem Prozess sind
grundsätzlich zwei unterschiedliche, aufeinander folgende Phasen zu
unterscheiden:

-   Phase 1 - Vorbereitung des Nachweisabrufs: Zunächst sind eine Reihe
    von Schritten notwendig, um den richtigen \[Data Service\] und
    weitere Parameter für den Abruf unter Nutzung der entsprechenden
    Verzeichnisdienste zu ermitteln. In diesem Teilprozess kann an
    verschiedenen Stellen eine Interaktion mit den Nutzenden notwendig
    sein. Diesen Teilprozess teilen sich die beiden Akteure *Data
    Consumer* und Intermediäre Plattform. Die aktuelle Konzeption lässt
    flexibel verschiedene Varianten zu, welche der benötigten Angaben
    vom *Data Consumer* und welche von der Intermediären Plattform
    erfasst werden. Wichtig ist, dass im Ergebnis alle erforderlichen
    Daten für Phase 2 vorliegen.
-   Phase 2 - Durchführung des Nachweisabrufs: Anschließend erfolgt der
    eigentliche Nachweisabruf über das EU-OOTS.

Die beiden folgenden Sequenzdiagramme zeigen einen möglichen
Nachrichtenfluss zwischen den beteiligten Akteuren unter Verwendung der
zuvor vorgestellten Nachrichten. Schritte im Ausland sind nur
dargestellt, wenn sie eine direkte Schnittstelle zu einem der
NOOTS-Akteure haben. Ablauf Nachweisabfrage durch nationalen Data
Consumer von ausländischem Evidence Provider\[figure.32\] zeigt den
Gesamtablauf, während Ablauf Nachweisabfrage durch nationalen Data
Consumer von ausländischem Evidence Provider - Fokus
Nutzerdialoge\[figure.33\] die anfallenden Nutzerdialoge zur Ermittlung
der benötigten Nachweistypen und Data Services ausdetailliert.

*Hinweis*: Gestrichelte Pfeile stehen für die Verwendung von HTTP als
Protokoll, während die durchgezogenen für XNachweis bzw. EDM-Nachrichten
bedeuten.

![C:\\cd6e9f804b38ff10f0b23b4a02cad58f](./assets/images6/media/image3.png)
<br>**Abb. 3: Ablauf Nachweisabfrage durch nationalen Data Consumer von ausländischem Evidence Provider**</br>

![C:\\1972a34de70392077c99441201871bcc](./assets/images6/media/image4.png)
<br>**Abb. 4: Ablauf Nachweisabfrage durch nationalen Data Consumer von ausländischem Evidence Provider - Fokus Nutzerdialoge**</br>


### **Kommunikation zwischen Data Consumer und Intermediärer Plattform**

Die Phase "Vorbereitung der Nachweisabfrage" wird durch den Einsatz
einer Intermediären Plattform auf die beiden Akteure *Data Consumer* und
die *Intermediäre Plattform* aufgeteilt. Um die Kommunikation
zwischen *Data Consumer* und Intermediärer Plattform abzubilden, ist
deshalb eine Erweiterung des ursprünglich im EDM vorgesehenen
Nachrichtenmodells notwendig. Hierzu sieht das untenstehende Diagramm
zwei neue Nachrichten mit dem
Arbeitstiteln *EvidenceOrder* und *GetEvidences* mit jeweiliger Response
vor.

Da nicht abzusehen ist, welche Informationen der *Data Consumer* in der
Praxis über den gewünschten *Nachweistyp* und dessen Herkunftsland
vorhält, erlaubt die aktuelle Konzeption in dieser Hinsicht
Flexibilität: Sollte der *Data Consumer* über die Pflichtangabe des
eigenen Verfahrens (procedure-id) hinaus bereits weitere Informationen
haben, kann er diese in der initialen *EvidenceOrder* der Intermediären
Plattform mitgeben. Dazu zählen

-   Nachzuweisende(r) Sachverhalt(e) (requirement-id)
-   Herkunftsland des Nachweises (zugeordnet zum nachzuweisenden
    Sachverhalt, falls mehrere)

Deren Aufgabe besteht darin, den *QueryRequest* durch ergänzende
Abfragen der Common Services und Nutzereingaben zu vervollständigen.

### **Nutzer-Authentisierung**

Nachweisabrufe über das EU-OOTS setzen zwingend eine
Nutzer-Authentisierung unter Nutzung eines eIDAS-notifizierten
Identifizierungsmittel voraus (Artikel 11 Abs. 1 DVO).

Für die Authentisierung der inländischen Antragstellenden ist
prinzipiell der *Data Consumer* zuständig. Da die *Intermediäre
Plattform* jedoch nicht zweifelsfrei davon ausgehen kann, dass diese
ordnungsgemäß durchgeführt wurde, authentisiert sie die Antragstellenden
auf der eigenen Nutzeroberfläche gemäß einem \"Zero Trust Ansatz\"
erneut.

Sollte sich im Laufe der Feinkonzeption ein Weg finden, mit dem
der *Data Consumer* die Authentisierung des Nutzers nachweisen kann,
kann die erneute Authentisierung auf der Intermediären Plattform im
Interesse der Nutzerfreundlichkeit entfallen.

## **Nicht-funktionale Anforderungen**

Nicht-funktionalen Anforderungen müssen im Rahmen der Feinkonzeption
einzelner Intermediärer Plattformen definiert und mit entsprechenden
Schutzbedarfsfeststellungen unterlegt werden. Dennoch folgen Hinweise
auf einige besonders zu berücksichtigende Aspekte.

### **Datenschutz**

Die *Intermediäre Plattform* ist eine Infrastrukturkomponente, die
Registerabrufe unterstützt. Sie benötigt dafür Zugriff auf
personenbezogene Informationen zur Person, die um den Registerabruf
ersucht, und auf die in der Registerantwort enthaltenen
personenbezogenen Informationen. Die Kritikalität dieser Angaben ist
auch mit Blick auf die konkret angeschlossenen Register und die von
ihnen übermittelten Arten von Informationen zu bewerten. In jedem Fall
handelt es sich bei Intermediären Plattformen jedoch um eine sensible
Infrastruktur, die gegen einen Missbrauch dieser Daten abgesichert
werden muss. Hierzu gehört auch, dass durch geeignete Maßnahmen
ausgeschlossen werden muss, dass Daten aus verschiedenen Registerabrufen
über die gleiche *Intermediäre Plattform* zusammengeführt und so für
eine unzulässige Profilbildung missbraucht werden. Eine Überlegung zur
Verringerung dieses Risikos ist die Einrichtung mindestens einer
Intermediären Plattform pro Verwaltungsbereich, sodass zumindest Daten
über Verwaltungsbereiche hinweg nicht zusammengeführt werden.

### **IT-Sicherheit**

Auch aus der Perspektive der IT-Sicherheit sind hohe Anforderungen an
Intermediäre Plattformen zu stellen.

Das EU-OOTS bietet für Intermediärer Plattformen und/oder *Evidence
Provider* keine Möglichkeit, die Berechtigung einer Registerabfrage aus
dem Ausland im Rahmen einer abstrakten Berechtigungsprüfung im
Einzelfall vor der Datenübermittlung zu plausibilisieren. Die
entsprechende Verantwortung wird von der Durchführungsverordnung bei den
anfragenden Behörden verortet, die die Rechtmäßigkeit ihrer Anfragen
sicherstellen müssen, und bei den jeweiligen Mitgliedstaaten, die
sicherstellen müssen, dass nur zuständige Behörden Zugriff auf das
EU-OOTS erhalten. Im Mitgliedstaat des *Evidence Provider* muss die
Anfrage dann grundsätzlich als zulässig betrachtet werden. Diese aus
deutscher Sicht mit Blick auf IT-Sicherheit kritische Vorgabe lässt sich
nicht auf der Ebene einer Intermediären Plattform revidieren. Bei der
Umsetzung einer Intermediären Plattform muss jedoch darauf geachtet
werden, die potenziellen Folgen dieses Designs so gering wie möglich zu
halten.

Nach aktuellem Kenntnisstand bedingt dies mindestens zwei Aspekte, die
bei der weiteren Ausgestaltung Intermediärer Plattformen zu beachten
sind. Zunächst müssen alle Anfragen aus dem EU-OOTS über einen Access
Point als legitim betrachtet und von der Intermediären Plattform über
das *NOOTS* weitergegeben werden. Dafür muss die Intermediäre Plattform
im *NOOTS* eine entsprechende Stellung mit weitreichenden
Zugriffsrechten auf Register erhalten. Dabei muss jedoch strikt darauf
geachtet werden, dass jede *Intermediäre Plattform* trotzdem nur die
Zugriffsrechte erhält, die für ihre jeweiligen Data Services notwendig
sind, und nicht jede *Intermediäre Plattform* pauschal privilegierten
Zugriff auf alle *Data Provider* des *NOOTS* erhält. Wie genau das zu
regeln ist, ist rechtlich zu prüfen und wird in [Offener Punkt NOOTS-647](#offene-punkte) festgehalten.

Zudem sollte bei der Feinkonzeption einer Intermediären Plattform
geprüft werden, welche Möglichkeiten existieren, zumindest ex-post
anhand einer Auswertung von Protokolldaten Hinweise auf ungewöhnliches
Abrufverhalten einzelner *Evidence Requester* zu erhalten und so
potenzielle Missbrauchsfälle zumindest nachträglich verfolgen zu können.

Bei der umgekehrten Richtung, dass deutsche *Data Consumer* über
die *Intermediäre Plattform* Nachweise aus dem europäischen Ausland
anfordern, könnten die Mechanismen des *NOOTS* zum Identitätsmanagement
von Behörden und zur abstrakten Berechtigungsprüfung genutzt werden, um
sicherzustellen, dass die betreffende Behörde auf die *Intermediäre
Plattform* zugreifen und im Rahmen ihrer Zuständigkeit grundsätzlich die
betreffende Art von Nachweisen anfordern darf. Solange die
übergreifenden NOOTS-Mechanismen noch nicht vollständig zur Verfügung
stehen, ist dies ggf. durch lokale Maßnahmen der Intermediären Plattform
zum gleichen Zweck zu ergänzen.

Bei der Umsetzung einer Intermediären Plattform ist zudem zu beachten,
dass die Durchführungsverordnung zu Artikel 14 der SDG-Verordnung in
einigen sicherheitsrelevanten Bereichen unmittelbare Vorgaben macht
(Artikel 17, 28f DVO). So stellt die DVO explizite Vorgaben zu Art und
Umfang der Protokollierung auf, die neben die ohnehin in Deutschland
bereits bestehenden Anforderungen ebenfalls erfüllt werden müssen
(Artikel 17 DVO). Hierzu gehört auch, dass eine technische Möglichkeit
verfügbar sein muss, Protokolldaten ggf. im Rahmen der Aufklärung eines
grenzüberschreitenden Missbrauchsverdachts für andere Behörden
bereitzustellen. Zudem sind die Mitgliedstaaten u.a. verpflichtet, für
die innerstaatliche Kommunikation der Intermediären Plattform mindestens
ein mit dem grenzüberschreitenden AS4-Transportprotokoll vergleichbares
Sicherheitsniveau zu garantieren (Artikel 28 Abs. 4 lit. a DVO).

Aufgrund der Konstruktion des EU-OOTS müssen Intermediäre Plattformen
aus dem Internet erreichbar sein, sowohl für die Kommunikation mit den
Common Services als auch zur Bereitstellung der Nutzeroberfläche.
Zugleich kommunizieren Intermediäre Plattformen mit einem Teil der
angeschlossenen zuständigen Behörden in Deutschland über gesicherte
Verwaltungsnetze. In welchem Netz der Access Point liegt und wie
die *Intermediäre Plattform* mit diesem zu kommunizieren hat, ist noch
offen.

In jedem Fall muss die *Intermediäre Plattform* an die genannten Netze
angebunden sein. Daraus ergibt sich die Notwendigkeit eines sicheren
Managements des Netzübergangs, für das alle entsprechenden Vorgaben des
BSI einzuhalten sind.

### **Verfügbarkeit, Antwortzeit und Last**

Einen Hinweis auf die erforderliche Verfügbarkeit der Intermediären
Plattform gibt Artikel 27 der Durchführungsverordnung zu Artikel
14 SDG-VO. Dort wird normiert, dass das EU-OOTS rund um die Uhr
betrieben wird, sowie eine Mindestverfügbarkeit der Access Points und
Preview-Spaces von 98% (exklusive planmäßiger Wartungen).

Diese Anforderung bezieht sich jedoch auf den Access Point, nicht auf
die dahinter liegenden Teilnehmer des EU-OOTS, also die Data Provider,
angeschlossenen *Data Consumer* oder Intermediären Plattform. Deren
Verfügbarkeit wird in Service Level Agreements zwischen Kommission und
Mitgliedstaaten festgelegt (Artikel 27 Abs. 1 DVO).

Für Antwortzeitverhalten und Lastfähigkeit einer Intermediären Plattform
können aktuell keine präzisen Anforderungen benannt werden. Es ist
jedoch zu beachten, dass Nachweisabrufe im EU-OOTS aus der interaktiven
Benutzung eines Online-Antrags heraus erfolgen. Dies erfordert in jedem
Fall ausreichend kurze, quasi-synchrone Reaktionszeiten.

## **Offene Punkte**

| ID        | Offener Punkt                                                                                                                           | Erläuterung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|-----------|-----------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| NOOTS-192 | Welche Rechtsstellung haben Intermediären Plattformen?                                                                                  | Intermediäre Plattformen benötigen für die Erfüllung ihrer Rolle als Evidence Requester bzw. Provider die Befugnis, Nachweise anfragen und, im Falle von Intermediären Plattformen für Data Provider, die darin enthaltenen Daten verarbeiten zu dürfen. Ob Intermediäre Plattformen dies als technischer Dienst im Auftrag der an sie angeschlossenen Behörden tun oder eine eigenständige Rechtspersönlichkeit erhalten, wird im Rahmen einer rechtlichen Analyse geklärt (PB Rechtsfragen KT-EU-Int-087 und KT-EU-Int-088).                                             |
| NOOTS-244 | Wie viele Intermediäre Plattformen soll es in Deutschland geben und wie sollen ihre Zuständigkeitsbereiche zugeschnitten sein?          | Zum aktuellen Zeitpunkt sind keine Anforderungen für eine bestimmte Anzahl oder einen bestimmten Zuschnitt von Intermediären Plattformen bekannt. Es besteht die Möglichkeit, dass die Anzahl und der Zuschnitt sich an den durch die Rechtsverordnung nach § 12 Absatz 1 Satz 1 **IDNrG** zu bestimmenden Verwaltungsbereichen orientiert. <p>Zu beachten: Die Intermediäre Plattform benötigt für die Erfüllung ihrer Aufgaben Zugriff auf die Nachweisinhalte. Daher muss entweder durch die Aufteilung der Kommunikation zwischen verschiedenen Intermediären Plattformen, durch geeignete Schutzmechanismen innerhalb der Plattform, oder durch rechtliche und organisatorische Maßnahmen sichergestellt werden, dass die Gefahr einer unzulässigen Profilbildung sowie die Übernahme durch Angreifer ausgeschlossen ist. | 
| NOOTS-647 | Welche Zugriffsrechte erhalten die Intermediären Plattformen?                                                                           | Welche Zugriffsrechte erhalten die Intermediären Plattformen, wer legt sie fest und wo werden diese hinterlegt? Wie wird sichergestellt, dass diese Zuständigkeitslogik an das DSD weitergereicht wird?                                                                                                                                                                                                                                                                                                                                                                    |
| NOOTS-554 | Wie gleicht die Intermediäre Plattform den Personendatensatz aus dem Request mit der angemeldeten Person ab?                            | Die Intermediäre Plattform muss in ihrer Rolle als Evidence Provider sicherstellen, dass die angemeldete Person mit dem Nachweissubjekt im erhaltenen Request bzw. in Vertretungsfällen (von natürlichen oder juristischen Personen) übereinstimmt. <p>Wie sie das tut, ist im übergreifenden Konzept “Nutzeridentifizierung, -authentifizierung und Datensatzabgleich” <a href="https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/Glossar.md?ref_type=heads" target= "_blank">[AD-NOOTS-14]</a> zu klären. | 
| NOOTS-459 | Informiert die Intermediäre Plattform die Nutzerinnen und Nutzer über das für den Nachweisabruf benötigte Vertrauensniveau?             | Betrifft Intermediäre Plattform in der Rolle des Evidence Requesters: Die dem Vertrauensniveau entsprechende Re-Authentifizierung geschieht beim ausländischen Evidence Provider. Es könnte nutzerfreundlich sein, die Nutzerinnen und Nutzer vor dem Absprung zu informieren, damit sie den Absprung nicht umsonst machen, sollten sie nicht über das geforderte Authentifizierungsmittel verfügen.                                                                                                                                                                       |
| NOOTS-565 | Soll die IP die Transformation von Nachweisen übernehmen oder liefern die Register immer ein menschenlesbares oder europäisches Format? | Die **SDG**-DVO fordert, dass die Evidence Provider ein menschenlesbares Format oder einen EU-weit harmonisierten Nachweis bereitstellen. Es ist zu klären, ob die Einhaltung dieser Verpflichtung bei den Data Providern liegt oder ob die Intermediäre Plattform ggf. Umwandlungen in eine der beiden genannten im EU-**OOTS** akzeptierten Formatkategorien vornimmt.                                                                                                                                                                                                           |
| NOOTS-551 | Wie muss das ausdrückliche Ersuchen/“exlicit request” abgefragt werden?                                                                 | Der Wortlaut dieser Funktion wird in UX-Teams auf EU-Ebene erarbeitet und ist nach dortiger Klärung in das Konzept aufzunehmen.                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| NOOTS-564 | Darf die Intermediäre Plattform mit dem Zeitpunkt einer verzögerten Nachweis-Verfügbarkeit antworten?                                   | Das EU-**OOTS** bietet die Möglichkeit, bei einem nicht verfügbaren Nachweis einen Zeitpunkt anzugeben, zu dem dieser verfügbar sein wird (Artikel 15 Abs. 5 DVO). Gedacht ist dieser optionale Mechanismus für Verzögerungen von wenigen Stunden bis Tagen. Sinnvoll ist ein solches Szenario dann, wenn Registerdaten schrittweise digitalisiert werden und die Möglichkeit besteht, aktiv angeforderte Daten in diesem Prozess zu priorisieren, oder in Fällen, in denen das Register Daten aus einem zur Ressourcenschonung angelegten (elektronischen) Archiv holen muss. |
| NOOTS-222 | Welcher Standard für die Datenübermittlung wird im NOOTS verwendet?                                                                     | Die Bearbeitung der Frage liegt im übergreifenden Konzept “Transportinfrastruktur” (AD-NOOTS-19).                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
