>**Hinweis:**
>Diese Version des Glossars wurde aktualisiert am: 12.07.2024.


<table>
<tbody>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>Status / Stand</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### A-C

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Abstrakte Berechtigungspr&uuml;fung</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Gem. &sect; 7 Abs. 2 IDNrG f&uuml;hrt die Vermittlungsstelle bei der bereichs&uuml;bergreifenden Daten&uuml;bermittlung unter Verwendung der IDNr eine abstrakte Berechtigungspr&uuml;fung durch und pr&uuml;ft, ob die beteiligten Kommunikationspartner zu einem anzugebenden Zweck miteinander kommunizieren d&uuml;rfen. Abstrakt bedeutet, dass die Pr&uuml;fung ohne Kenntnis des konkreten Nachrichteninhalts erfolgt.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
<p>&nbsp;</p>
</div>
</td>
<td>&sect; 7 Abs 2 IDNrG</td>
</tr>
<tr>
<td>AmtlicherGemeindeschl&uuml;ssel</td>
<td>
<p>AGS</p>
</td>
<td>&nbsp;</td>
<td>
<p>8-stelliger Schl&uuml;ssel zur eindeutigen Identifizierung einer Gemeinde mit den Bestandteilen: Bundesland (2 Stellen), Regierungsbezirk (1 Stelle), Kreis (2 Stellen) und Gemeinde (3 Stellen).</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Destatis -<a href="https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Glossar/amtlicher-gemeindeschluessel.html">Glossar AGS</a></p>
</td>
</tr>
<tr>
<td>Amtlicher Regionalschl&uuml;ssel</td>
<td>
<p>ARS</p>
</td>
<td>&nbsp;</td>
<td>
<p>12-stelliger Schl&uuml;ssel zur eindeutigen Identifizierung einer Gemeinde mit den Bestandteilen: Bundesland (2 Stellen), Regierungsbezirk (1 Stelle), Kreis (2 Stellen), Gemeindeverband (4 Stellen) und Gemeinde (3 Stellen).</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2022</p>
</div>
</td>
<td>
<p>Destatis -<a href="https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Glossar/regionalschluessel.html">Glossar AGR</a></p>
</td>
</tr>
<tr>
<td>Anschlussbedingungen</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Anschlussbedingungen beschreiben die technischen Anforderungen an die IT-Systeme der Data Consumer / Data Provider (z.B. bereitzustellende Schnittstellen, umzusetzende Standards, einzuhaltende Service Level Agreements), um den automatisierten Abruf von Daten &uuml;ber die NOOTS-Infrastruktur zu erm&ouml;glichen. Technische Anschlussbedingungen werden in den Anschlusskonzepten des PB NOOTS spezifiziert.</p>
<p>Daneben gibt es organisatorische, fachliche und rechtliche Rahmenbedingungen und Voraussetzungen, die erf&uuml;llt sein m&uuml;ssen, damit eine Anschlussf&auml;higkeit hergestellt werden kann.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>AUG 2023</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Antragsverfahren</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>ein digitaler oder analoger Antrag auf eine Verwaltungsleistung, welcher an ein Fachverfahren &uuml;bermittelt wird.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
<p>Aktualisiert</p>
<p>AUG 2023</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Beh&ouml;rde</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Konkrete Beh&ouml;rde eines Beh&ouml;rdentyps. Eine Beh&ouml;rde f&uuml;hrt eine konkrete Registerinstanz und bietet einen technischen Endpunkt an, &uuml;ber den auf diese zugegriffen werden kann.<br /><em>Beispiel: Einwohnermeldeamt K&ouml;ln</em></p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>PB NOOTS</p>
</td>
</tr>
<tr>
<td>Beh&ouml;rde, zust&auml;ndige</td>
<td>&nbsp;</td>
<td>
<p>Competent Authority</p>
</td>
<td>
<p>Jede Stelle oder Beh&ouml;rde eines Mitgliedstaats auf nationaler, regionaler oder lokaler Ebene mit bestimmten Zust&auml;ndigkeiten f&uuml;r die unter diese Verordnung fallenden Informationen, Verfahren, Hilfs- und Probleml&ouml;sungsdienste.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 3 Nr. 4 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Beh&ouml;rdenfunktion</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Die Beh&ouml;rdenfunktion steht f&uuml;r den fachlichen Aufgabenbereich einer Beh&ouml;rde basierend auf einer rechtlichen Grundlage (&bdquo;Rolle der Beh&ouml;rde&ldquo;).</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Beh&ouml;rdentyp</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Kategorie von Beh&ouml;rden, die die gleichen Leistungen erbringen und die gleichen Prozesse ausf&uuml;hren. Je nach Fachdom&auml;ne gibt es eine oder mehrere Beh&ouml;rden vom selben Beh&ouml;rdentyp. Beh&ouml;rden desselben Beh&ouml;rdentyps f&uuml;hren Register desselben Registertyps. Beh&ouml;rdentypen k&ouml;nnen auch mehrere Registertypen f&uuml;hren.</p>
<p>Beispiel: Meldebeh&ouml;rden</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>PB NOOTS</p>
</td>
</tr>
<tr>
<td>
<p>Bund ID</p>
<p>&nbsp;</p>
</td>
<td>
<p>Bund ID</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>
<p>Die Bund ID ist die zentrale Identifizierungs- und Authentifizierungskomponente f&uuml;r B&uuml;rger des Bundes. Der Bund stellt die BundID auch f&ouml;deral zur Nachnutzung bereit. Mit der BundID wird zudem ein Postfach f&uuml;r B&uuml;rger zur Verf&uuml;gung gestellt.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p>OZG-Leitfaden</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>bundeseinheitliche Wirtschaftsnummer f&uuml;r Unternehmen</p>
</td>
<td>
<p>beWiNr.</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>
<p>Gem. &sect; 2 Unternehmensbasisdatenregistergesetz (UBRegG) wird einem Unternehmen nach &sect; 3 Abs. 1 UBRegG eine bundeseinheitliche Wirtschaftsnummer f&uuml;r Unternehmen (beWiNr.) zugeordnet. Als bundeseinheitliche Wirtschaftsnummer f&uuml;r Unternehmen dient die Wirtschafts-Identifikationsnummer (W-IdNr.) nach &sect; 139c der Abgabenordnung (AO), welche vom Bundeszentralamt f&uuml;r Steuern vergeben wird.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>
<p>&sect; 2 Unternehmensbasisdatenregistergesetz (UBRegG)</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>Status</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### D-F

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Datendienst</p>
</td>
<td>&nbsp;</td>
<td>
<p>Data Service</p>
</td>
<td>
<p>Ein technischer Dienst, &uuml;ber den ein Nachweislieferant die Nachweisanfrage bearbeitet und die Nachweise &uuml;bermittelt.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 12 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Datenmodell</p>
</td>
<td>&nbsp;</td>
<td>
<p>Data Model</p>
</td>
<td>
<p>Ein Datenmodell ist eine Abstraktion, in der Datenelemente organisiert und die ihre Beziehungen zueinander standardisiert werden. Im Datenmodell werden Entit&auml;ten, deren Merkmale und die Beziehungen zueinander spezifiziert.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 13 SDG-VO</p>
</td>
</tr>
<tr>
<td>Datenschutzcockpit</td>
<td>DSC</td>
<td>&nbsp;</td>
<td>
<p>Das Datenschutzcockpit (vormals "Datencockpit") (DSC, Art. 2 RegMoG) soll es B&uuml;rgerinnen und B&uuml;rgern erm&ouml;glichen, durchgef&uuml;hrte beh&ouml;rdliche Daten&uuml;bermittlungen unter Nutzung der Identifikationsnummer nach dem IDNrG nachzuvollziehen und die zur Person erfassten Registerdaten einsehen zu k&ouml;nnen. Gegenw&auml;rtig befinden sich insbesondere die fachliche Abstimmung und die technische Umsetzung der Anzeige der Bestandsdaten der Register im DSC in der Umsetzung.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>
<p>IT-PLR-Beschluss<a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-06_Bericht_Umsetzungsstand.pdf">2022/06</a><br />DSC, Art. 2 RegMoG</p>
</td>
</tr>
<tr>
<td>
<p>Deutsches Verwaltungsdiensteverzeichnis</p>
</td>
<td>DVDV</td>
<td>&nbsp;</td>
<td>
<p>Das Deutsche Verwaltungsdiensteverzeichnisist eine Infrastrukturkomponente im Kontext des E-Government in Deutschland. Es stellt einzentrales Verzeichnis mit technischen Verbindungsdaten f&uuml;r die verwaltungs&uuml;bergreifende, sichere Kommunikation der Verwaltungen von Bund, L&auml;ndern und Kommunen dar. In Kombination mit OSCI-Intermedi&auml;ren erm&ouml;glicht es den Datenaustausch zwischen verschiedenen Fachverfahren.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Durchf&uuml;hrungsverordnung</p>
</td>
<td>DVO, IA</td>
<td>Implementing Act, Implementing Regulation</td>
<td>
<p>Durchf&uuml;hrungsverordnungen der Kommission dienen der einheitlichen Umsetzung von EU-Rechtsvorschriften. Zu diesem Zweck hat die Kommission zu Art. 14 SDG-VO (vgl. Art. 14 Abs. 9 SDG-VO) die Durchf&uuml;hrungsverordnung (EU) 2022/1463 der Kommission vom 5. August 2022 zur Festlegung technischer und operativer Spezifikationen des technischen Systems f&uuml;r den grenz&uuml;berschreitenden automatisierten Austausch von Nachweisen und zur Anwendung des Grundsatzes der einmaligen Erfassung gem&auml;&szlig; der Verordnung (EU) 2018/1724 des Europ&auml;ischen Parlaments und des Rates erlassen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p><a href="https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A32022R1463">EUR-Lex - 32022R1463 - EN - EUR-Lex (europa.eu)</a></p>
</td>
</tr>
<tr>
<td>
<p>eDelivery-Zugangspunkt</p>
</td>
<td>&nbsp;</td>
<td>
<p>(eDelivery) Access Point</p>
</td>
<td>
<p>Eine Kommunikationskomponente, die Teil des elektronischen Zustelldienstes eDelivery ist und auf technischen Spezifikationen und Normen beruht, einschlie&szlig;lich des AS4-Daten&uuml;bermittlungsprotokolls und zus&auml;tzlicher Dienste, die im Rahmen der Fazilit&auml;t &ldquo;Connecting Europe&rdquo; entwickelt und im Rahmen des Programms &ldquo;Digitales Europa&rdquo; fortgef&uuml;hrt wurden, soweit sich diese technischen Spezifikationen und Normen mit der Norm ISO 15000-2 decken.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 4 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>eIDAS-Knoten</p>
</td>
<td>&nbsp;</td>
<td>
<p>eIDAS Node</p>
</td>
<td>
<p>Ein Knoten im Sinne von Artikel 2 Nummer 1 der Durchf&uuml;hrungsverordnung (EU) 2015/1501, der die technischen und operativen Anforderungen erf&uuml;llt, die in dieser Verordnung und auf deren Grundlage festgelegt sind.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 5 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Elektronische Identifizierung</p>
</td>
<td>
<p>eID</p>
</td>
<td>&nbsp;</td>
<td>
<p>&bdquo;Elektronische Identifizierung&ldquo; (eID) ist der Prozess der Verwendung von Personenidentifizierungsdaten in elektronischerForm, die eine nat&uuml;rliche oder juristische Person oder eine nat&uuml;rliche Person, die eine juristische Person vertritt,eindeutig repr&auml;sentieren.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>
<p>Explanatory Paper V3.0</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>Elektronisches Identifizierungsmittel</p>
</td>
<td>&nbsp;</td>
<td>
<p>Electronic Identification means</p>
</td>
<td>
<p>Eine materielle und/oder immaterielle Einheit, die Personenidentifizierungsdaten enth&auml;lt und zur Authentifizierung bei Online-Diensten verwendet wird.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 9 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Europ&auml;ische Technische Entwurfsdokumentation</p>
</td>
<td>EU-TDDs</td>
<td>European Technical Design Documents</td>
<td>
<p>Eine Reihe detaillierter technischer Dokumente, die von der Kommission in Zusammenarbeit mit den Mitgliedstaaten im Rahmen der Koordinierungsgruppe f&uuml;r das Zugangstor gem&auml;&szlig; Artikel 29 der Verordnung (EU) 2018/1724 oder etwaiger Untergruppen gem&auml;&szlig; Artikel 19 dieser Verordnung erstellt werden und die unter anderem eine oberste Ebene der Architektur, Austauschprotokolle, Normen und Zusatzdienste umfassen, die die Kommission, die Mitgliedstaaten, die Nachweislieferanten, die Nachweise anfordernden Beh&ouml;rden, die intermedi&auml;ren Plattformen und andere betroffene Beh&ouml;rden bei der Einrichtung des OOTS im Einklang mit dieser Verordnung unterst&uuml;tzen<em>.</em></p>
</td>
<td>ABGESTIMMT
<div>
<p>JUNI 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 11 SDG-VO</p>
<p><a href="https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/OOTS+Technical+Design+Documents+-+Q1+2023">Link zu den EU-TDDs</a></p>
</td>
</tr>
<tr>
<td>
<p>Europ&auml;isches Once-Only-Technical-System</p>
</td>
<td>EU-OOTS</td>
<td>(EU) Once-Only Technical System</td>
<td>
<p>Das technische System f&uuml;r den grenz&uuml;berschreitenden automatisierten Austausch von Nachweisen gem&auml;&szlig; Artikel 14 Absatz 1 der Verordnung (EU) 2018/1724.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 1 SDG-VO</p>
</td>
</tr>
<tr>
<td>Evidence Provider</td>
<td>
<p>EP</p>
</td>
<td>
<p>Nachweislieferant</p>
</td>
<td>
<p>Eine zust&auml;ndige Beh&ouml;rde im Sinne des Artikels 14 Absatz 2 der Verordnung (EU) 2018/1724, die strukturierte oder unstrukturierte Nachweise rechtm&auml;&szlig;ig ausstellt.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 2 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Evidence Requester</p>
</td>
<td>
<p>ER</p>
</td>
<td>
<p>Nachweise anfordernde Beh&ouml;rde</p>
</td>
<td>
<p>Eine zust&auml;ndige Beh&ouml;rde, die f&uuml;r eines oder mehrere der in Artikel 14 Absatz 1 der Verordnung (EU) 2018/1724 genannten Verfahren verantwortlich ist.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 3 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Evidence Survey</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Die Evidence Survey ist eine zentrale Erhebung durch die EU-Kommission, die durch innerstaatliche Vorarbeiten vorbereitet wird. Ziel der Evidence Survey ist die Identifikation von Nachweisen f&uuml;r den automatisierten grenz&uuml;berschreitenden Austausch zu SDG-relevanten Verfahren.</p>
<p>F&uuml;r die Erstellung der Evidence Survey m&uuml;ssen innerstaatliche Vorarbeiten erfolgen, die hier &bdquo;Deutsche Erhebung Evidence Survey&ldquo; genannt werden. Die deutsche Erhebung f&uuml;r die Evidence Survey wird durch den nationalen SDG-Koordinator gesteuert. Die Bearbeitung obliegt dem PB OZG-EU-OOTS. Die Pr&uuml;fung und Freigabe der deutschen Angaben f&uuml;r die EU KOM erfolgt durch die fachlich und rechtlich zust&auml;ndigen Stellen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p><a href="https://www.digitale-verwaltung.de/SharedDocs/downloads/Webs/DV/DE/beschluss-2022-06-bericht-umsetzungsstand.pdf?__blob=publicationFile&amp;v=1">beschluss-2022-06-bericht-umsetzungsstand.pdf (digitale-verwaltung.de)</a></p>
</td>
</tr>
<tr>
<td>
<p>Exchange Data Model</p>
<p>&nbsp;</p>
</td>
<td>
<p>EDM</p>
</td>
<td>&nbsp;</td>
<td>
<p>Ist das Datenaustauschmodell f&uuml;r die Anforderung und &Uuml;bermittlung von Nachweisen im EU OOTS. Das EDM bildet den Ausgangspunkt f&uuml;r die Entwicklung von XNachweis.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p><a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis</a></p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>Explanatory Paper</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Begleitdokument, welches Aufschluss &uuml;ber den Anwendungsbereich des Anhang 2 der SDG-Verordnung gibt. Die zu betrachtenden Richtlinien aus Artikel 14 sind bislang nicht im Explanatory Paper enthalten. Die Mitgliedstaaten haben die EU KOM darauf hingewiesen, dass f&uuml;r die Auslegung der Richtlinien ein vergleichbares Dokument hilfreich w&auml;re; eine entsprechende Erg&auml;nzung erfolgt m&ouml;glicherweise.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Fachverfahren</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Fachrechtliches Verwaltungsverfahren, welches digital oder analog umgesetzt wird. Ein Antragsverfahren muss dabei nicht zwingend Bestandteil des Fachverfahrens sein.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>AUG 2023</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>FIT-Connect</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>FIT-Connect ist eine Basisinfrastruktur-Komponente der Verwaltungsdigitalisierung in Deutschland. FIT-Connect wird im Auftrag des IT-Planungsrats von der F&ouml;deralen IT-Kooperation (FITKO) entwickelt und betrieben.</p>
<p>FIT-Connect stellt eine IT-Infrastruktur bereit, die das digitale Beantragen von Verwaltungsleistungen innerhalb der Bundesrepublik Deutschland wesentlich erleichtert. FIT-Connect wurde insbesondere konzipiert f&uuml;r die f&ouml;derale Nachnutzung zentraler Online-Dienste (Einer-f&uuml;r-Alle-, kurz EfA- Dienste). Dadurch tr&auml;gt FIT-Connect wesentlich zur schnelleren Umsetzung des Onlinezugangsgesetzes bei.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>NOV 2023</p>
</div>
</td>
<td><a href="https://docs.fitko.de/fit-connect/docs#was-leistet-fit-connect">Einf&uuml;hrung in FIT-Connect | FIT-Connect (fitko.de)</a></td>
</tr>
<tr>
<td>
<p>Front office</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Verfahrensschnittstelle, die es einem Benutzer erm&ouml;glicht, mit der zust&auml;ndigen Beh&ouml;rde von der Identifizierung des Benutzers bis zum Abschluss eines Online-Verfahrens zu interagieren.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Explanatory Paper<a href="https://ec.europa.eu/assets/grow/growth/_toolbox/sdg-docs/V03_Explanatory%20document%20on%20scope%20of%20Annex%20II_July%202022.pdf">V3.0</a></p>
<p>deutsche Verlinkung erg&auml;nzen</p>
</td>
</tr>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>Status</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### G-J

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Identifikationsnummer</td>
<td>IDNr</td>
<td>&nbsp;</td>
<td>
<p>Die Identifikationsnummer nach<a href="https://www.buzer.de/139b_AO.htm">&sect;139b der Abgabenordnung</a>, die nach dem IDNr-Gesetz als zus&auml;tzliches Ordnungsmerkmal in allen von der Registermodernisierung betroffenen Register eingef&uuml;hrt wird mit dem prim&auml;ren Zweck, die Daten einer nat&uuml;rlichen Person in einem Verwaltungsverfahren eindeutig zuordnen zu k&ouml;nnen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p><a href="https://www.gesetze-im-internet.de/idnrg/BJNR059110021.html">https://www.gesetze-im-internet.de/idnrg/BJNR059110021.html</a></p>
</td>
</tr>
<tr>
<td>Identit&auml;tsdatenabruf</td>
<td>IDA</td>
<td>&nbsp;</td>
<td>
<p>Das Verfahren "Identit&auml;tsdatenabruf" (IDA) soll gew&auml;hrleisten, dass berechtigte Register Identifikationsnummern und andere Basisdaten aus der Steuer-ID-Datenbank des Bundeszentralamts f&uuml;r Steuern abrufen k&ouml;nnen.</p>
<p>Mit der Umsetzung des Identifikationsnummerngesetzes wurde die Steuer-ID als gemeinsames Ordnungsmerkmal zur eindeutigen Identifizierung nat&uuml;rlicher Personen in den Registern der &ouml;ffentlichen Verwaltung eingef&uuml;hrt.</p>
<p>Die Entwicklung der Anwendung wird durch die Registermodernisierungsbeh&ouml;rde BVA gesteuert, nach Implementierung wird das BVA diese auch betreiben.</p>
</td>
<td>
<p>ABGESTIMMT</p>
<p>NOV 2023</p>
<div>&nbsp;</div>
</td>
<td><a href="https://www.bva.bund.de/DE/Services/Behoerden/Verwaltungsdienstleistungen/Registermodernisierung/Informationen-OeffentlicheStellen/Textbaustein_IDA_Service-Teaser.html">https://www.bva.bund.de/DE/Services/Behoerden/<br />Verwaltungsdienstleistungen/Registermodernisierung/<br />Informationen-OeffentlicheStellen/Textbaustein_IDA_Service-Teaser.html</a></td>
</tr>
<tr>
<td>Identit&auml;tsmanagement f&uuml;r Personen</td>
<td>IDM f&uuml;r Personen</td>
<td>&nbsp;</td>
<td>Das IDM f&uuml;r Personen (Art. 1 Registermodernisierungsgesetz) stellt die IDNr eines B&uuml;rgers bzw. einer B&uuml;rgerin und weitere Basisdaten zur Person bereit.</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>IT-PLR-Beschluss<a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-06_Bericht_Umsetzungsstand.pdf">2022/06</a></td>
</tr>
<tr>
<td>Identit&auml;tsmanagement f&uuml;r Unternehmen</td>
<td>IDM f&uuml;r Unternehmen</td>
<td>&nbsp;</td>
<td>Das Identit&auml;tsmanagement f&uuml;r Unternehmen befasst sich mit der register&uuml;bergreifenden eindeutigen Identifikation der Unternehmen anhand einer bundeseinheitlichen Wirtschaftsnummer. Die Bundeseinheitliche Wirtschaftsnummer wird durch das Bundeszentralamt f&uuml;r Steuern vergeben und von der W-IdNr.-Datenbank des Bundeszentralamtes f&uuml;r Steuern bereitgestellt.</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>IT-PLR-Beschluss<a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-06_Bericht_Umsetzungsstand.pdf">2022/06</a></td>
</tr>
<tr>
<td>
<p>Interaktiver Nachweisabruf</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Ein interaktiver Nachweisabruf bedeutet, dass der Nachweisabruf im Dialog mit einer Person, die zu sich selbst oder in Vertretung des Nachweissubjekts zu diesem, einen Nachweis abruft, stattfindet.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>
<p>IT-PLR-Beschluss 2022/22 - Entscheidung asynchrone Prozesse</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>Intermedi&auml;re Plattform</p>
</td>
<td>
<p>IP</p>
</td>
<td>
<p>Intermediary Platform</p>
</td>
<td>
<p>Eine technische L&ouml;sung, die je nach der Verwaltungsorganisation der Mitgliedstaaten, in denen die intermedi&auml;re Plattform t&auml;tig ist, in Erf&uuml;llung eigener Aufgaben oder im Namen anderer Beh&ouml;rden wie Nachweislieferanten oder Nachweise anfordernden Beh&ouml;rden t&auml;tig wird und &uuml;ber die Nachweislieferanten oder Nachweise anfordernde Beh&ouml;rden mit den in Artikel 4 Absatz 1 genannten gemeinsamen Diensten oder mit Nachweislieferanten oder Nachweise anfordernden Beh&ouml;rden aus anderen Mitgliedstaaten verbunden werden.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 6 SDG-VO</p>
</td>
</tr>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>
<div>
<p>Status</p>
</div>
</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### K-N

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Koordinierungsstelle f&uuml;r IT-Standards (KoSIT)</p>
</td>
<td>
<p>KoSIT</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>
<p>Die KoSIT hat die Aufgabe, die Entwicklung und den Betrieb von IT-Standards f&uuml;r den Datenaustausch in der &ouml;ffentlichen Verwaltung zu koordinieren.</p>
<p>&nbsp;</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p><a href="https://www.xoev.de/">https://www.xoev.de/</a></p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>Lawfully issued</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>"von einer staatlich erm&auml;chtigten Institution ausgestellt"</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Lokale Verwaltungseinheiten</p>
</td>
<td>
<p>LAU</p>
</td>
<td>&nbsp;</td>
<td>
<p>Der europ&auml;ische Schl&uuml;ssel, um Gemeinden zu identifizieren.</p>
<p>Um der Nachfrage nach Statistiken auf lokaler Ebene gerecht zu werden, unterh&auml;lt Eurostat ein System lokaler Verwaltungseinheiten (LAUs), das mit der NUTS kompatibel ist. Diese LAU sind die Bausteine der NUTS und umfassen die Gemeinden und Kommunen der Europ&auml;ischen Union.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Eurostat -<a href="https://ec.europa.eu/eurostat/de/web/nuts/local-administrative-units">LAU</a></p>
</td>
</tr>
<tr>
<td>
<p>Mein Unternehmenskonto</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Mit Mein Unternehmenskonto bietet sich die M&ouml;glichkeit, digitale Verwaltungsleistungen verschiedenster Beh&ouml;rden &uuml;ber einen deutschlandweit einheitlichen Zugang zu nutzen. Das staatlicherseits bereitgestellte Nutzerkonto mit integriertem Postfach f&uuml;r Mitteilungen und beh&ouml;rdliche Bescheide ist speziell f&uuml;r Organisationen entwickelt worden, die wirtschaftsbezogene Verwaltungsleistungen ben&ouml;tigen. Damit nur identifizierte und authentifizierte Organisationen Zugriff zu digitalen Verwaltungsleistungen erhalten, kommt &uuml;ber Mein Unternehmenskonto die ELSTER-Technologie zum Einsatz.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Mitgliedstaat Once-Only-Technical-System</p>
</td>
<td>
<p>MS-OOTS</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>
<p>Ist das Once-Only-Technical-System (OOTS) eines EU-Mitgliedstaats. Durch einen Anschluss an das EU-OOTS und andere MS-OOTS (wie das NOOTS) wird der grenz&uuml;berschreitende Nachweisabruf gem&auml;&szlig; Artikel 14 Absatz 1 der SDG-VO erm&ouml;glicht.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p>Artikel 14 Absatz 1 SDG-VO</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>Nachweis (europ&auml;isch)</p>
</td>
<td>&nbsp;</td>
<td>
<p>Evidence</p>
</td>
<td>
<p>Evidence</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2022</p>
</div>
</td>
<td>
<p>SDG-VO Art. 3 Nr. 5</p>
</td>
</tr>
<tr>
<td>
<p>Nachweis (national)</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Nachweise im Sinne dieses Gesetzes [EGovG Bund-Entwurf] sind Unterlagen und Daten jeder Art unabh&auml;ngig vom verwendeten Medium, die zur Ermittlung des Sachverhalts geeignet sind.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2022</p>
</div>
</td>
<td>
<p>&sect; 5 Absatz 2 Satz 1 EGovG Bund-Entwurf</p>
</td>
</tr>
<tr>
<td>
<p>Nachweisart (amtliche &Uuml;bersetzung)</p>
<p>/ Nachweistyp (&uuml;blicher Sprachgebrauch)</p>
</td>
<td>&nbsp;</td>
<td>
<p>Evidence Type</p>
</td>
<td>
<p>Nachweistypen dienen zur Klassifikation von Nachweisen nach gemeinsamem Zweck oder Inhalt. Nachweistypen sind selbst keine Nachweise, aber Nachweise geh&ouml;ren zu einem Nachweistyp.</p>
<p>Beispiel: Alle konkreten Geburtsurkunden sind Auspr&auml;gungen zum Nachweistyp "Geburtsurkunde".</p>
<p>&nbsp;</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2022</p>
</div>
</td>
<td>
<p>in Anlehnung an Art. 1 Nr. 17 SDG-DVO</p>
</td>
</tr>
<tr>
<td>
<p>Nachweisdienst</p>
</td>
<td>
<p>EB</p>
</td>
<td>
<p>Evidence Broker</p>
</td>
<td>
<p>Ein Dienst, der es einer Nachweise anfordernden Beh&ouml;rde erm&ouml;glicht, festzustellen, welche Nachweisart aus einem anderen Mitgliedstaat die Anforderungen an die Nachweise f&uuml;r die Zwecke eines nationalen Verfahrens erf&uuml;llt</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 8 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Nachweissubjekt</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Eine nat&uuml;rliche oder juristische Person, zu der ein Nachweis abgerufen wird.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Nationale zentrale Kontaktstelle f&uuml;r technische Unterst&uuml;tzung</p>
</td>
<td>
<p>nzK</p>
</td>
<td>&nbsp;</td>
<td>
<p>Jeder Mitgliedstaat benennt gem. SDG-DVOeine zentrale Kontaktstelle f&uuml;r technische Unterst&uuml;tzung, um den Betrieb und die Wartung der einschl&auml;gigen Komponenten des OOTS, f&uuml;r die der jeweilige Mitgliedstaat gem&auml;&szlig; Abschnitt 9 zust&auml;ndig ist, sicherzustellen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
AUG 2023</div>
</td>
<td>Art. 21 SDG-DVO (EU) 2022/1463</td>
</tr>
<tr>
<td>Nationales Once-Only-Technical-System</td>
<td>NOOTS</td>
<td>&nbsp;</td>
<td>
<p>Das Nationale Once-Only-Technical Systems (NOOTS) ist ein System aus technischen Komponenten, Schnittstellen und Standards sowie organisatorischen und rechtlichen Regelungen, das &ouml;ffentlichen Stellen den rechtskonformen Abruf von elektronischen Nachweisen aus den Registern der deutschen Verwaltung erm&ouml;glicht.&Uuml;ber einen Anschluss an das europ&auml;ische Once-Only-Technical-System (EU-OOTS) wird ein Austausch von Nachweisen mit dem EU-Ausland erm&ouml;glicht.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JULI 2023</p>
</div>
</td>
<td>
<p>Quelle erg&auml;nzen</p>
</td>
</tr>
<tr>
<td>
<p>Nicht-interaktiver Nachweisabruf</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Einnicht-interaktiver Nachweisabrufbedeutet, dass derNachweisabrufohne Beteiligung desNachweissubjektsoder seines Vertreters stattfindet.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>
<p>IT-PLR-Beschluss 2022/22 - Entscheidung asynchrone Prozesse</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>NOOTS-Komponente</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Ein f&uuml;r die Vorbereitung, Durchf&uuml;hrung oder Nachvollziehbarkeit eines Nachweisabrufs notwendiger Bestandteil des NOOTS.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>NOOTS-Teilnehmer</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Ein NOOTS-Teilnehmer ist ein im NOOTS registriertes technisches Verfahren zum Abruf oder zur Lieferung von Nachweisen (Teilnahmeart).</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>
<p><a href="https://www.nrw-connect-extern.nrw.de/confluence/pages/viewpage.action?pageId=235410562">Anschlusskonzept Data Consumer</a></p>
<p><a href="https://www.nrw-connect-extern.nrw.de/confluence/pages/viewpage.action?pageId=235410705">Anschlusskonzept Data Provider</a></p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>Nutzerinnen und Nutzer</td>
<td>&nbsp;</td>
<td>
<p>User</p>
</td>
<td>
<p>nat&uuml;rliche Personen mit Staatsangeh&ouml;rigkeit eines EU-Mitgliedstaates oder eine nat&uuml;rliche Person mit Wohnsitz in einem EU-Mitgliedstaat sowie eine juristische Person mit eingetragenem Sitz in einem EU-Mitgliedstaat.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>Status</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### O-R

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Onlinedienst</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Ein &bdquo;Onlinedienst" ist eine IT-Komponente, die ein eigenst&auml;ndiges elektronisches Angebot an die Nutzer darstellt, welches die Abwicklung einer oder mehrerer elektronischer Verwaltungsleistungen von Bund oder L&auml;ndern erm&ouml;glicht. Der Onlinedienst dient dem elektronischen Ausf&uuml;llen der Onlineformulare f&uuml;r Verwaltungsleistungen von Bund oder L&auml;ndern, der Offenlegung dieser Daten an die zust&auml;ndige Fachbeh&ouml;rde sowie der &Uuml;bermittlung elektronischer Dokumente und Informationen zu Verwaltungsvorg&auml;ngen an die Nutzer, gegebenenfalls unter Einbindung von Nutzerkonten einschlie&szlig;lich deren Funktion zur &Uuml;bermittlung von Daten aus einem Nutzerkonto an eine f&uuml;r die Verwaltungsleistung zust&auml;ndige Beh&ouml;rde. Der Onlinedienst kann auch verfahrensunabh&auml;ngig und l&auml;nder&uuml;bergreifend, insbesondere in der Verantwortung einer Landesbeh&ouml;rde zur Nutzung durch weitere L&auml;nder, bereitgestellt werden.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>
<p>OZG &Auml;ndG &sect; 2 Abs. 8</p>
<p>Hinweis: ggf. Ver&auml;nderung der Schreibweise mit Ver&ouml;ffentlichung des OZG 2.0 beobachten</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>Online Services Computer Interface</p>
</td>
<td>
<p>OSCI</p>
</td>
<td>&nbsp;</td>
<td>
<p>OSCI (Online Services Computer Interface) ist ein Transportstandard f&uuml;r die sichere, vertrauliche und rechtsverbindliche &Uuml;bertragung elektronischer Daten im e-Government. Er erg&auml;nzt hierf&uuml;r die Kommunikation &uuml;ber HTTP um zus&auml;tzliche Informationen und Datenstrukturen. Der Standard und zugeh&ouml;rige nicht-normative Hilfsmittel k&ouml;nnen kostenfrei genutzt werden. Es werden die klassischen Schutzziele Integrit&auml;t, Authentizit&auml;t, Vertraulichkeit und Nachvollziehbarkeit bei der &Uuml;bermittlung von Nachrichten gew&auml;hrleistet. OSCI findet insbesondere Einsatz in unsicheren Netzen wie dem Internet, bietet aber auch in sicheren Netzen erg&auml;nzende Funktionalit&auml;ten und sorgt insbesondere f&uuml;r Interoperabilit&auml;t.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>OKT 2023</p>
</div>
</td>
<td>
<p><a href="https://www.xoev.de/osci-xta-3355">https://www.xoev.de/osci-xta-3355</a></p>
</td>
</tr>
<tr>
<td>
<p>Onlinezugangsgesetz</p>
</td>
<td>
<p>OZG</p>
</td>
<td>&nbsp;</td>
<td>
<p>Das im Jahr 2017 in Kraft getretene "Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen" (OZG) verpflichtet Bund und L&auml;nder, ihre Verwaltungsleistungen auch elektronisch &uuml;ber Verwaltungsportale anzubieten. Konkret beinhaltet das zwei Aufgaben: Digitalisierung und Vernetzung.</p>
<p>&nbsp;</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>NOV 2023</p>
</div>
</td>
<td>
<p><a href="https://www.gesetze-im-internet.de/ozg/BJNR313800017.html">OZG</a></p>
</td>
</tr>
<tr>
<td>
<p>OZG-Leistung</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Eine OZG-Leistung ist ein Leistungsb&uuml;ndel, welches sich aus mehreren einzelnen Verwaltungsleistungen zusammensetzt. Aus Sicht der Nutzerinnen und Nutzer besteht zwischen diesen Verwaltungsleistungen ein thematischer Zusammenhang. Die Grundlage zur Identifikation dieser Leistungsb&uuml;ndel bildet der Leistungskatalog der &ouml;ffentlichen Verwaltung (LeiKa). Alle OZG-Leistungen sollen im Sinne des Onlinezugangsgesetzes (OZG) online verf&uuml;gbar sein. Sie sind verschiedenen Themenfeldern zugeordnet und in unterschiedliche Lebens- und Gesch&auml;ftslagen unterteilt. Die OZG-Leistungen sind im OZG-Umsetzungskatalog dokumentiert, der seit September 2019 auf der OZG-Informationsplattform kontinuierlich fortgeschrieben wird.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>MAI 2023</p>
</div>
</td>
<td>
<p><a href="https://informationsplattform.ozg-umsetzung.de/iNG/app/intro">OZG-Informationsplattform FAQs</a><br />im Lexikon der DV HP bereits enthalten:</p>
<p>Der BegriffOZG-Leistung beschreibt ein Leistungsb&uuml;ndel bestehend aus bis zu mehreren Hundert einzelnen Verwaltungsleistungen aus dem "Leistungskatalog der &ouml;ffentlichen Verwaltung" (LeiKa), die thematisch aus Nutzersicht zusammenh&auml;ngen. Es gibt 575OZG-Leistungen, davon werden 115 im Bundesprogramm und 460 im Digitalisierungsprogramm F&ouml;deral umgesetzt.</p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>OZG-Reifegradmodell</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Das Reifegradmodell dient als Grundlage zur Beurteilung der OZG-Konformit&auml;t einer Verwaltungsleistung. Es umfasst f&uuml;nf Reifegrade &ndash; von 0 (Offline) bis 4 (Online-Transaktion).</p>
<p>Bei Reifegrad 4 kann die Leistung vollst&auml;ndig digital abgewickelt werden und f&uuml;r Nachweise wird das Once-Only-Prinzip angewendet.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>NOV 2023</p>
</div>
</td>
<td>
<p><a href="https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/ozg-grundlagen/info-reifegradmodell/info-reifegradmodell-node.html">Digitale Verwaltung - Reifegradmodell (digitale-verwaltung.de)</a></p>
</td>
</tr>
<tr>
<td>Portal</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>Siehe Verfahrensportal</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Registerdatennavigation</p>
</td>
<td>
<p>RDN</p>
</td>
<td>&nbsp;</td>
<td>
<p>Komponente im Zielbild und zentraler Routingdienst des NOOTS. Liefert auf Anfrage die Information, von welchem technischen Dienst einer Beh&ouml;rde ein gesuchter Nachweistyp abgerufen werden kann. Dazu &uuml;bermittelt die RDN notwendige Verbindungsparameter.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>PB NOOTS</p>
</td>
</tr>
<tr>
<td>
<p>registerf&uuml;hrende Stelle</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Die registerf&uuml;hrende Stelle ist die Beh&ouml;rde, die rechtlich zur F&uuml;hrung eines Registers berechtigt oder verpflichtet ist.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>OKT 2023</p>
</div>
</td>
<td>
<p>Redaktionsboard</p>
</td>
</tr>
<tr>
<td>
<p>Registermodernisierung</p>
</td>
<td>
<p>RegMo</p>
</td>
<td>&nbsp;</td>
<td>
<p>Das Programm Registermodernisierung (RegMo) ist eines der gr&ouml;&szlig;ten Projekte im Rahmen der Digitalisierungsbestrebungen von Bund, L&auml;ndern und Kommunen.</p>
<p>Einheitlich gestaltete, inhaltlich aktuelle, vernetzte Register stellen einen wichtigen Meilenstein dar f&uuml;r eine digitale, b&uuml;rokratiearme und serviceorientierte Verwaltung, die B&uuml;rgerinnen und B&uuml;rger sowie Unternehmen entlastet.</p>
<p>Ein wesentliches Ziel ist, dass B&uuml;rgerinnen und B&uuml;rger in Zukunft ihre Daten und Nachweise nicht immer wieder erneut vorlegen m&uuml;ssen, um Verwaltungsleistungen zu erhalten, sondern - wenn sie dem eingewilligt haben - nur einmal (Once-Only-Prinzip).</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>OKT 2023</p>
</div>
</td>
<td>
<p>IT-Planungsratsbeschluss</p>
</td>
</tr>
<tr>
<td>
<p>RegMo-Koordinatorin / RegMo-Koordinator</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>F&uuml;r eine erfolgreiche fl&auml;chendeckende Umsetzung der Registermodernisierung<br />werden RegMo-Koordinatorinnen und Koordinatoren in allen Bundesl&auml;ndern und dem Bund etabliert.<br />Die RegMo-Koordinatorinnen und Koordinatoren &uuml;bernehmen folgende Aufgaben:<br />1. Sie stehen im kontinuierlichen Austausch mit der Gesamtsteuerung Registermodernisierung und stellen die Kommunikation mit den jeweiligen Ressorts im eigenen Land oder dem Bund sowie den Kommunen des eigenen Landes sicher.<br />2. Sie &uuml;berblicken die notwendigen Ma&szlig;nahmen, um die Registermodernisierung im Bund, den Bundesl&auml;ndern und Kommunen umsetzen zu k&ouml;nnen und kommunizieren diese an die hierf&uuml;r zust&auml;ndigen Stellen.<br />3. Sie weisen auf die notwendige Haushaltsvorsorge in Bund, dem jeweiligen Land und den Kommunen hin.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>SEP 2023</p>
<p>&nbsp;</p>
</div>
</td>
<td>
<p><a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2023/Beschluss2023-22_RegMo_Koordinatoren.pdf">Beschluss2023-22_RegMo_Koordinatoren.pdf (it-planungsrat.de)</a></p>
</td>
</tr>
<tr>
<td>
<p>Registerlandkarte</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Die Registerlandkarte ist eine umfassende &Uuml;bersicht aller bestehenden Register, sie gibt Auskunft &uuml;ber deren Anschlussf&auml;higkeit und deren Entwicklung sowie zur Datenspeicherung. Zu den Daten, die die Eintr&auml;ge in der Registerlandkarte beinhalten wird, geh&ouml;ren u. a.: Name des jeweiligen Registers, Beschreibung, Kategorisierung, Gr&ouml;&szlig;e/Umfang, Verwaltungsebene, Gesetzesgrundlage, enthaltene Metadaten, Nachweise&uuml;bersicht, Schnittstellen, OZG-Dienste, Fachstandards.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>OKT 2023</p>
</div>
</td>
<td>
<p><a href="https://www.bva.bund.de/DE/Services/Behoerden/Verwaltungsdienstleistungen/Registermodernisierung/Informationen-Buerger/Registerlandkarte/registerlandkarte_inhalt.html">BVA - Registermodernisierung - Registerlandkarte (bund.de)</a></p>
</td>
</tr>
<tr>
<td>
<p>Registertyp</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Registertypen dienen zur Klassifikation von Registern nachgemeinsamem Zweck oder Inhalt. Registertypen sind selbst keine Register, aber Register geh&ouml;ren zu einem Registertyp.</p>
<p>Beispiel: Alle Registerinstanzen der Melderegister sind Auspr&auml;gungen zum Registertyp "Melderegister".</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>AUG 2023</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Reifegradmodell Nachweis-abruf</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Modell zur Beschreibung von m&ouml;glichen Reifegraden, in denen ein Nachweis in den Registern der deutschen Verwaltung vorliegen kann. Das Modell umfasst die Stufe A (Offline), die Stufe B (Elektronisch &uuml;bermittelte Nachweise), die Stufe C (Elektronisch auswertbare Nachweise) und die Stufe D (bedarfsgerecht &uuml;bermittelte Informationen).</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>
<div>
<p>Status</p>
</div>
</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### S-U

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Semantischer Datenspeicher</p>
</td>
<td>&nbsp;</td>
<td>
<p>Semantic Repository</p>
</td>
<td>
<p>Ein Archiv semantischer Spezifikationen, die mit dem Nachweisdienst und dem Verzeichnis der Datendienste verkn&uuml;pft sind und aus Definitionen von Namen, Datentypen und Datenelementen bestehen, die mit bestimmten Nachweisarten verbunden sind, um das gegenseitige Verst&auml;ndnis und die sprachen&uuml;bergreifende Auslegung f&uuml;r Nachweislieferanten, Nachweise anfordernde Beh&ouml;rden und Nutzer beim Austausch von Nachweisen &uuml;ber das OOTS sicherzustellen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 10 SDG-VO</p>
</td>
</tr>
<tr>
<td>Service Gateways</td>
<td>SGs</td>
<td>&nbsp;</td>
<td>
<p>Service Gateways (SGs) sind optionale Produkte, die die Anbindung von Data Consumers und Data-Providern an das NOOTS vereinfachen und beschleunigen k&ouml;nnen. Sie b&uuml;ndeln Authentifizierung und Autorisierung, Schemavalidierung, Protokoll&uuml;bersetzung oder Datentransformation. Data Consumers und Data-Provider k&ouml;nnen Anbindungen wahlweise &uuml;ber eigene Schnittstellen/Komponenten oder &uuml;ber SGs umsetzen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>IT-PLR-Beschluss<a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-06_Bericht_Umsetzungsstand.pdf">2022/06</a></p>
</td>
</tr>
<tr>
<td>Single Digital Gateway Durchf&uuml;hrungsverordnung</td>
<td>
<p>SDG-DVO</p>
</td>
<td>&nbsp;</td>
<td>
<p>Durchf&uuml;hrungsverordnung (EU) 2022/1463 der Kommission vom 5. August 2022 zur Festlegung technischer und operativer Spezifikationen des technischen Systems f&uuml;r den grenz&uuml;berschreitenden automatisierten Austausch von Nachweisen und zur Anwendung des Grundsatzes der einmaligen Erfassung gem&auml;&szlig; der Verordnung (EU) 2018/1724 des Europ&auml;ischen Parlaments und des Rates<strong><br /></strong></p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>AUG 2023</p>
</div>
</td>
<td>
<p><a href="https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32022R1463">https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32022R1463</a></p>
</td>
</tr>
<tr>
<td>
<p>Single Digital Gateway Procedure</p>
</td>
<td>
<p>SDG-Procedure</p>
</td>
<td>&nbsp;</td>
<td>
<p>Siehe Verfahren.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Single Digital Gateway Verordnung</td>
<td>
<p>SDG-VO</p>
</td>
<td>&nbsp;</td>
<td>
<p>Verordnung (EU) 2018/1724 des Europ&auml;ischen Parlaments und des Rates vom 2. Oktober 2018 &uuml;ber die Einrichtung eines einheitlichen digitalen Zugangstors zu Informationen, Verfahren, Hilfs- und Probleml&ouml;sungsdiensten und zur &Auml;nderung der Verordnung (EU) Nr. 1024/2012</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>AUG 2023</p>
</div>
</td>
<td>
<p><a href="https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724">https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724</a></p>
</td>
</tr>
<tr>
<td>
<p>SDG1-Relevanz</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Nationale Verfahren auf LeiKa-Ebene werden bei Erf&uuml;llung der in Anhang I der SDG-VO definierten Kriterien als SDG-1-relevant bezeichnet. Der Evidence Survey erhebt keine Informationen zur SDG1-Relevanz. SDG1-relevant sind Informationsbereiche im Zusammenhang mit B&uuml;rgern.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>SDG-VO Anhang 1</p>
</td>
</tr>
<tr>
<td>
<p>SDG2-Relevanz</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Nationale Verfahren auf LeiKa-Ebene werden bei Erf&uuml;llung der in Artikel 14 der SDG-VO definierten Kriterien als SDG2-relevant bezeichnet. Die Zahl 2 bezieht sich dabei auf den Anhang 2 der SDG-Verordnung, wenngleich von Artikel 14 mehr als nur die in Anhang 2 aufgef&uuml;hrten 21 SDG-Verfahren betroffen sind, n&auml;mlich auch die Verfahren nach den Richtlinien 2005/36/EG, 2006/123/EG, 2014/24/EU und 2014/25/EU werden hier als SDG2-relevante Verfahren bezeichnet.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 14 Abs. 1 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Strukturierter Nachweis</p>
</td>
<td>&nbsp;</td>
<td>
<p>Structured Evidence</p>
</td>
<td>
<p>Ein Archiv semantischer Spezifikationen, die mit dem Nachweisdienst und dem Verzeichnis der Datendienste verkn&uuml;pft sind und aus Definitionen von Namen, Datentypen und Datenelementen bestehen, die mit bestimmten Nachweisarten verbunden sind, um das gegenseitige Verst&auml;ndnis und die sprachen&uuml;bergreifende Auslegung f&uuml;r Nachweislieferanten, Nachweise anfordernde Beh&ouml;rden und Nutzer beim Austausch von Nachweisen &uuml;ber das OOTS sicherzustellen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 15 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Technischer Dienst</p>
</td>
<td>&nbsp;</td>
<td>
<p>Data Service</p>
</td>
<td>
<p>Unter dem technischen Dienst wird die konkrete Implementierung zur Ausstellung eines Nachweises bei einer konkreten Registerinstanz bzw. nachweisliefernden Stelle verstanden. Der technische Dienst verf&uuml;gt &uuml;ber einen eindeutigen Identifier (Service-ID).</p>
<p>Andere technische Dienste, die Softwarekomponenten anbieten, sind im Kontext dieses Konzepts nicht gemeint.</p>
<p>F&uuml;r den Abruf von Nachweisen von einem technischen Dienst sind Verbindungsparameter erforderlich.</p>
<p>Beispiel: Dienstinstanz von NOOTS_Meldebescheinigung f&uuml;r die Stadt K&ouml;ln</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p>PB NOOTS</p>
</td>
</tr>
<tr>
<td>Unstrukturierter Nachweis</td>
<td>&nbsp;</td>
<td>
<p>Unstructured Evidence</p>
</td>
<td>
<p>Nachweise in elektronischem Format, die f&uuml;r die in Artikel 14 Absatz 1 der Verordnung (EU) 2018/1724 genannten Verfahren erforderlich sind und die nicht in vordefinierten Elementen oder Feldern organisiert sind, die eine bestimmte Bedeutung und ein bestimmtes technisches Format haben, sondern durch die Metadatenelemente des allgemeinen Metadatenmodells des OOTS gem&auml;&szlig; Artikel 7 Absatz 1 der vorliegenden Verordnung erg&auml;nzt werden.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 16 SDG-DVO</p>
</td>
</tr>
<tr>
<th>Begriff</th>
<th>Abk&uuml;rzung</th>
<th>&Uuml;bersetzung</th>
<th>Definition</th>
<th>Status</th>
<th>Quelle</th>
</tr>
<tr>
<td>

##### V-Z

</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Verbindungsparameter</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Die Gesamtheit aller Informationen, die ben&ouml;tigt werden, um einen Technischen Dienst zu nutzen. Dazu geh&ouml;rt mindestens die URL des Dienstes, Zertifikate, etc.</p>
<p>Beispiel: Dienstinstanz von NOOTS_Meldebescheinigung f&uuml;r die Stadt K&ouml;ln &amp; Zertifikat des Einwohnermeldeamts K&ouml;ln</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>PB NOOTS</p>
</td>
</tr>
<tr>
<td>Verfahren</td>
<td>&nbsp;</td>
<td>
<p>Procedure</p>
</td>
<td>
<p>Eine Abfolge von Ma&szlig;nahmen, die die Nutzer ergreifen m&uuml;ssen, um den Anforderungen zu entsprechen oder einen Beschluss einer zust&auml;ndigen Beh&ouml;rde zu erwirken, um ihre Rechte nach Artikel 2 Absatz 2 Buchstabe a aus&uuml;ben zu k&ouml;nnen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>SDG-VO Art. 3 Nr. 3</p>
</td>
</tr>
<tr>
<td>verfahrensbezogene Nachweisanforderungen</td>
<td>
<p>PR</p>
</td>
<td>
<p>Procedural requirements</p>
</td>
<td>
<p>Aus den erforderlichen Nachweisen ergeben sich Procedural Requirements (PR), auf deutsch bestimmte (abstrakte) verfahrensbezogene Nachweisanforderungen genannt.</p>
<p>Beispiel: Der erforderliche Nachweis "Ausweisdokument" hat die &uuml;bergeordnete Nachweisanforderung &bdquo;Nachweis der Identit&auml;t&ldquo;.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Verfahrensportal</td>
<td>&nbsp;</td>
<td>
<p>Procedure Portal</p>
</td>
<td>
<p>Eine Webseite oder eine mobile Anwendung, &uuml;ber die ein Nutzer Zugang zu einem Online-Verfahren im Sinne von Artikel 14 Absatz 1 der Verordnung (EU) 2018/1724 hat und es abschlie&szlig;en kann.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 19 SDG-VO</p>
</td>
</tr>
<tr>
<td>Vermittlungsstelle</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Die Vermittlungsstelleist eine dritte &ouml;ffentliche Stelle, die beim bereichs&uuml;bergreifenden Nachweisabruf unter Verwendung der IDNr die abstrakte Berechtigungspr&uuml;fung durchf&uuml;hrt und so die Zul&auml;ssigkeit von Daten&uuml;bermittlungen zwischen Data Consumer und Data Provider sicherstellt. Vorrangiges Ziel der Vermittlungsstelle ist, das Risiko der unzul&auml;ssigen Zusammenf&uuml;hrung von personenbezogenen Daten zur Bildung von Pers&ouml;nlichkeitsprofilen zu verringern.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>JUNI 2024</p>
</div>
</td>
<td>
<p>&sect; 7 Abs. 2 IDNrG</p>
</td>
</tr>
<tr>
<td>Verwaltungs-Public-Key-Infrastructure</td>
<td>V-PKI</td>
<td>&nbsp;</td>
<td>
<p>Die Verwaltungs-Public-Key-Infrastructure (V-PKI) stellt eine zertifikatsbasierte Infrastruktur f&uuml;r elektronische Signatur und Verschl&uuml;sselung zum Schutz der Vertraulichkeit, Integrit&auml;t und Authentizit&auml;t in der digitalen Kommunikation zur Verf&uuml;gung.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>IT-PLR-Beschluss<a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-06_Bericht_Umsetzungsstand.pdf">2022/06</a></p>
</td>
</tr>
<tr>
<td>Verwaltungsverfahren</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Ein Verwaltungsverfahren liefert oder ben&ouml;tigt Nachweise. Es kann, muss sich aber dabei nicht um eine Leistung handeln.</p>
<p>Beispiel: &bdquo;Meldedatensatz zum Abruf Bereitstellung&ldquo; (elektronische Meldebescheinigung)</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>KT Architektur - RDN</p>
</td>
</tr>
<tr>
<td>Verzeichnis der Datendienste</td>
<td>
<p>DSD</p>
</td>
<td>
<p>Data Service Directory</p>
</td>
<td>
<p>Ein Register, das die Liste der Nachweislieferanten und der von ihnen herausgegebenen Nachweisarten zusammen mit den entsprechenden Begleitinformationen enth&auml;lt.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 7 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Vorfall</p>
</td>
<td>&nbsp;</td>
<td>
<p>Incident</p>
</td>
<td>
<p>Eine Situation, in der das technische System (OOTS) die Leistung nicht erbringt, die Nachweise nicht &uuml;bermittelt oder Nachweise &uuml;bermittelt, die nicht angefordert wurden, oder in der die Nachweise w&auml;hrend der &Uuml;bermittlung ver&auml;ndert oder offengelegt wurden, sowie jede Verletzung der Sicherheit gem&auml;&szlig; Artikel 29.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 18 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>Vorschaufunktion</p>
</td>
<td>&nbsp;</td>
<td>
<p>Preview,die</p>
</td>
<td>
<p>Eine Funktion, die es dem Nutzer erm&ouml;glicht, die angeforderten Nachweise gem&auml;&szlig;Artikel15 Absatz 1 Buchstabe b Ziffer ii vorab einzusehen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>
<p>Art. 1 Nr. 14 SDG-VO</p>
</td>
</tr>
<tr>
<td>
<p>XNachweis</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>XNachweis ist ein Standard des Bundesverwaltungsamtes f&uuml;r die fach&uuml;bergreifende Anforderung und &Uuml;bermittlung von Nachweisen zu nat&uuml;rlichen und juristischen Personen. Mittels XNachweis werden nationale Verwaltungsportale, registerf&uuml;hrende wie auch weitere &ouml;ffentlichen Stellen an das NOOTS und das EU-OOTS angebunden. Der Standard ist kompatibel mit den europ&auml;ischen Spezifikationen (EU-Technical Design Documents, kurz [EU TDD]) der Verordnung (EU) 2018/1724 des Europ&auml;ischen Parlaments und des Rates (kurz [SDG VO]) und gleichzeitig ausgerichtet auf die spezifischen Anforderungen eines nationalen OOTS.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p><a href="https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis">https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis</a></p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td>
<p>X&Ouml;V-Fachstandards</p>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Das K&uuml;rzel X&Ouml;V steht f&uuml;r<strong>X</strong>ML in der<strong>&Ouml;</strong>ffentlichen<strong>V</strong>erwaltung. X&Ouml;V-Standards sind Spezifikationen zum systematischen Datenaustausch in der &ouml;ffentlichen Verwaltung beziehungsweise zwischen der &ouml;ffentlichen Verwaltung und ihren B&uuml;rgerinnen, B&uuml;rger und Unternehmen.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>OKT 2023</p>
</div>
</td>
<td>
<p><a href="https://www.xoev.de/xoev-4987">https://www.xoev.de/xoev-4987</a></p>
</td>
</tr>
<tr>
<td>
<p>XTA</p>
<p>&nbsp;</p>
</td>
<td>
<p>XTA</p>
<p>&nbsp;</p>
</td>
<td>
<p>(auf Ausschreibung von XTA wird explizit verzichtet da keine eineindeutige Quelle gefunden werden kann und Ausschreibung allgemeingebr&auml;uchlich auch nicht verwendet wird)</p>
</td>
<td>
<p>XTAist ein Standard f&uuml;r den Austausch von Nachrichten zwischen Fach- und Transportverfahren in der &ouml;ffentlichen Verwaltung. Ziel ist, dass der Nachrichtentransport auf Basis eines einheitlichen Transportauftrags und eines generischen Nachrichtencontainers fachunabh&auml;ngig und fach&uuml;bergreifend ausgef&uuml;hrt werden kann.<br /><br /></p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
OKT 2023</div>
</td>
<td>
<p>KoSIT<a href="https://www.xoev.de/osci-xta/xta-4835">https://www.xoev.de/osci-xta/xta-4835</a></p>
</td>
</tr>
<tr>
<td>
<p>Zentrale Dienste (des EU-OOTS)</p>
</td>
<td>&nbsp;</td>
<td>
<p>Central Services</p>
</td>
<td>
<p>Von der europ&auml;ischen Kommission bereitgestellte zentrale Komponenten des EU-OOTS. Sie dienen alle als "Nachschlagewerke" und verarbeiten selbst keine personenbezogenen Daten.</p>
<ul>
<li>
<p>Evidence Broker (siehe Begriffsdefinition oben)</p>
</li>
<li>
<p>Data Service Directory (siehe Begriffsdefinition oben)</p>
</li>
<li>
<p>Semantic Repository (siehe Begriffsdefinition oben)</p>
</li>
</ul>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>DEZ 2022</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Zertifikat</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Ein elektronisches Zertifikat zur Authentifizierung, Siegelung und/oder Verschl&uuml;sselung.</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>APRIL 2024</p>
</div>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<p>Zust&auml;ndigkeitsparameter</p>
</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
<p>Je Nachweistyp festgelegte Parameter zur Ermittlung der nachweisliefernden Stelle. Nur bei Registertypen mit verteilten Zust&auml;ndigkeiten erforderlich. Vom Data Consumer werden die Werte aller Parameter spezifisch f&uuml;r den jeweiligen Nachweisabruf an die Registerdatennavigation &uuml;bermittelt. Die Registerdatennavigation ermittelt die zust&auml;ndige Beh&ouml;rde und deren technischen Dienst.</p>
<p>Beispiel: Postleitzahloder DVDV-Schl&uuml;ssel (bspw. ags&hellip;)</p>
</td>
<td>
<div>
<p>ABGESTIMMT</p>
<p>FEB 2024</p>
</div>
</td>
<td>
<p>PB NOOTS</p>
</td>
</tr>
</tbody>
</table>

## Architekturdokumente NOOTS

[AD-NOOTS-01: Anschlusskonzept Data Consumer (DC)](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-01_+Anschlusskonzept+Data+Consumer+_DC_.md?ref_type=heads)

[AD-NOOTS-02: Anschlusskonzept Data Provider (DP)](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-02_+Anschlusskonzept+Data+Provider+_DP_.md?ref_type=heads)

[AD-NOOTS-03: Architektur Gesamtsystem (High-Level-Architecture (HLA))](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-03_+High-Level-Architecture+_HLA_.md?ref_type=heads)

AD-NOOTS-04: Grobkonzept Access Point (AP)

[AD-NOOTS-05: Grobkonzept IAM für Behörden](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-05_+Grobkonzept+IAM+für+Behoerden.md?ref_type=heads)

[AD-NOOTS-06: Grobkonzept Intermediäre Plattform (IP)](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-06_+Grobkonzept+Intermediaere+Plattform+_IP_.md)

[AD-NOOTS-07: Grobkonzept Registerdatennavigation (RDN)](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-07_+Grobkonzept+Registerdatennavigation+_RDN_+-+Q4+_+2023.md?ref_type=heads)

[AD-NOOTS-08: Grobkonzept Vermittlungsstellen (DSC)](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-08_+Grobkonzept+Vermittlungsstelle+_VS_.md?ref_type=heads)

AD-NOOTS-09: Komponentensteckbrief Datenschutzcockpit

AD-NOOTS-10: Komponentensteckbrief IDM Personen

AD-NOOTS-11: Komponentensteckbrief IDM Unternehmen

AD-NOOTS-12: Übergreifendes Konzept Betriebliche Überwachung

AD-NOOTS-13: Übergreifendes Konzept Nachweisreifegrad

AD-NOOTS-14: Übergreifendes Konzept Nutzeridentifizierung und Nutzerauthentifizierung, Datensatzabgleich

AD-NOOTS-15: Übergreifendes Konzept Preview (National und EU)

AD-NOOTS-16: Übergreifendes Konzept Zertifikate (V-PKI)

AD-NOOTS-17: Übergreifendes Konzept Datenschutz und IT-Sicherheit

AD-NOOTS-18: Übergreifendes Konzept Betrieb/SLAs

[AD-NOOTS-19: Übergreifendes Konzept Transportinfrastruktur](https://gitlab.opencode.de/bmi/noots/-/blob/main/NOOTS-Dokumente/AD-NOOTS-19_+Grobkonzept+Transportinfrastruktur.md?ref_type=heads)

AD-NOOTS-20: Übergreifendes Konzept Umsetzungsstufen

AD-NOOTS-21: Standard XNachweis


## Beschlüsse des IT-Planungsrats zur Registermodernisierung

**IT-PLR-B-01**

[IT-PLR-Beschluss 2019/03. 28. Sitzung des IT-Planungsrats am 12.03.2019: Einrichtung des Koordinierungsprojekts Registermodernisierung unter FF Bund, HH & BY](https://www.it-planungsrat.de/beschluss/beschluss-2019-03)

**IT-PLR-B-02**

[IT-PLR-Beschluss 2019/23. 29. Sitzung des IT-Planungsrats am 27.06.2019: IT-Planungsrat beauftragt das Koordinierungsprojekt mit verschiedenen Aufgaben](https://www.it-planungsrat.de/beschluss/beschluss-2019-23)

**IT-PLR-B-03**

[IT-PLR-Beschluss 2020/25. 32. Sitzung des IT-Planungsrats am 24.06.2020: Kenntnisnahme der Eckpunkte der Registermodernisierung](https://www.it-planungsrat.de/beschluss/beschluss-2020-25)

**IT-PLR-B-04**

[IT-PLR-Beschluss 2021/05. 34. Sitzung des IT-Planungsrats am 17.03.2021: Beschluss des Zielbildes der Registermodernisierung sowie der Umsetzungsplanung](https://www.it-planungsrat.de/beschluss/beschluss-2021-05)

**IT-PLR-B-05**

[IT-PLR-Beschluss 2021/25. 35. Sitzung des IT-Planungsrats am 23.06.2021: Einrichtung des Projekts „Gesamtsteuerung Registermodernisierung“](https://www.it-planungsrat.de/beschluss/beschluss-2021-25)

**IT-PLR-B-06**

[IT-PLR-Beschluss 2021/35. 36. Sitzung des IT-Planungsrats am 29.10.2021: Umsetzungsplanung des Zielbilds der Registermodernisierung](https://www.it-planungsrat.de/beschluss/beschluss-2021-35)

**IT-PLR-B-07**

[IT-PLR-Beschluss 2022/06. 37. Sitzung des IT-Planungsrats am 09.03.2022: Beschluss der Programmplanung inklusive der Meilensteine bis 2025](https://www.it-planungsrat.de/beschluss/beschluss-2022-06)

**IT-PLR-B-08**

[IT-PLR-Beschluss 2022/22. 38. Sitzung des IT-Planungsrats am 22.06.2022: Insb. Beschluss verschiedener Beschlüsse des Lenkungskreises der Registermodernisierung zur technischen Architektur und Bericht zum aktuellen Umsetzungsstand](https://www.it-planungsrat.de/beschluss/beschluss-2022-22)

**IT-PLR-B-09**

[IT-PLR-Beschluss 2022/34. 39. Sitzung des IT-Planungsrats am 10.11.2022: Bericht zum Umsetzungsstand und NOOTS-Registeranschluss](https://www.it-planungsrat.de/beschluss/beschluss-2022-34)

**IT-PLR-B-10**

[IT-PLR-Beschluss 2023/38. 42. Sitzung des IT-Planungsrats am 03.11.2023: OSCI-Studie, Kommunikationsinfrastruktur für den Nachweisabruf und Bericht zum Umsetzungsstand](https://www.it-planungsrat.de/beschluss/beschluss-2023-38)

**IT-PLR-B-11**

[IT-PLR-Beschluss 2023/22. 41. Sitzung des IT-Planungsrats am 04.07.2023: Konkretisierung des Zielbildes Registermodernisierung in Form von zwei Aufträgen](https://www.it-planungsrat.de/beschluss/beschluss-2023-22)


## Entscheidungen des IT-Planungsrats zur Registermodernisierung

**IT-PLR-E-01**

[IT-PLR-Beschluss 2022/22: Entscheidung zur Einführung eines Reifegradmodells für Nachweisabrufe und Ausrichtung des NOOTS auf Reifegrad D](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL2_Reifegradmodell.pdf)

**IT-PLR-E-02**

[IT-PLR-Beschluss 2022/22: Entscheidung zur Unterstützung asynchroner Prozesse in der Architektur der Registermodernisierung](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL3_Asynchrone_Prozesse.pdf)

**IT-PLR-E-03**

[IT-PLR-Beschluss 2022/22: Entwicklung eines allgemeinen Standards für den Nachweisabruf für die nationale Registermodernisierung](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL4_Nachweisabruf.pdf)

**IT-PLR-E-04**

[IT-PLR-Beschluss 2022/22: Aufbau eines nationalen Data Service Directory und Nutzung des europäischen Evidence Brokers](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL5_DSD.pdf)

**IT-PLR-E-05**

[IT-PLR-Beschluss 2022/22: Entscheidung zur Umsetzung der Komponente Registerdatennavigation als zentralen Routing-Dienst (Routing As a Service) auf Grundlage des Deutschen Verwaltungsdienste Verzeichnis (DVDV) unter Wiederverwendung von Lösungsansätzen aus FIT-Connect](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-22_RegMo_AL6_Registerdatennavigation.pdf)

**IT-PLR-E-06**

[IT-PLR-Beschluss 2022/34: Entscheidung über die Anbindung der Register und Onlineservices an das EU-NOOTS über Intermediärer Plattformen](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_EU-OOTS.pdf)

**IT-PLR-E-07**

[IT-PLR-Beschluss 2022/34: Entscheidung über die Anbindung der Register an das NOOTS zum Nachweisabruf](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_NOOTS-Registeranbindung.pdf)

**IT-PLR-E-08**

[IT-PLR-Beschluss 2022/34: Entscheidung über die Umsetzung einer standardisiert einheitlich nutzbaren Basiskomponente (sog. SDG-Connector) für die Anbindung des NOOTS - und somit den nationalen Registern und Onlinediensten/Portalen - an das EU-OOTS](https://www.it-planungsrat.de/fileadmin/beschluesse/2022/Beschluss2022-34_SDG-Connector.pdf)

**IT-PLR-E-09**

[IT-PLR-Beschluss 2023/22: Gesamtplan/Finanzplan Registermodernisierung für 2023/ 2024 /2025](https://www.it-planungsrat.de/fileadmin/beschluesse/2023/Beschluss2023-22_RegMo_Finanzplan.pdf)

**ITPLR-FAR**

[Architekturrichtlinien des IT-Planungsrats für die Entwicklung und den Betrieb föderaler IT-Architekturen](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien/)


## Dokumente der Europäischen Kommission zu SDG


**EU-01**

[Durchführungsverordnung (EU) 2022/1463 zur Festlegung technischer und operativer Spezifikationen des technischen Systems für den grenzüberschreitenden automatisierten Austausch von Nachweisen und zur Anwendung des Grundsatzes der einmaligen Erfassung gemäß der Verordnung (EU) 2018/1724 des Europäischen Parlaments und des Rates](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX%3A32022R1463%3ADE%3AHTML)

**EU-02**

[EU Technical Design Documents (TDD): Technische Beschreibung des EU-OOTS - Release Oktober 2022](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/Technical+Design+Documents)

**EU-03**

[Single-Digital-Gateway-Verordnung (SDG-VO): Verordnung (EU) 2018/1724 des Europäischen Parlaments und des Rates über die Einrichtung eines einheitlichen digitalen Zugangstors zu Informationen, Verfahren, Hilfs- und Problemlösungsdiensten und zur Änderung der Verordnung (EU) Nr. 1024/2012.](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018R1724)

**EU-EB-01**

[Evidence Broker, Teil der Common Services der Europäischen Kommission für das EU-OOTS](https://oots.pages.code.europa.eu/tdd/apidoc/evidence-broker/)

**EU-EB-02**

[Beschreibung der Abfrage "Get List of Requirements" beim Europäischen Evidence Broker](https://oots.pages.code.europa.eu/tdd/apidoc/evidence-broker/get-requirements)

**EU-EB-03**

[Beschreibung der Abfrage "Get Evidence Types" beim Europäischen Evidence Broker](https://oots.pages.code.europa.eu/tdd/apidoc/evidence-broker/get-evidence-types)

**EU-DSD-01**

[Data Service Directory, Teil der Common Services der Europäischen Kommission für das EU-OOTS](https://oots.pages.code.europa.eu/tdd/apidoc/data-services-directory/)

**EU-DSD-02**

[Beschreibung der Abfrage "Find Data Services of Evidence Providers" beim Europäischen Data Service Directory](https://oots.pages.code.europa.eu/tdd/apidoc/data-services-directory/find-data-services)

**EU-EDM-01**

[Exchange Data Model: Beschreibung des Datenmodells und darin enthaltenen Nachrichten für die Kommunikation zwischen zwei Stellen im EU-OOTS](https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/4.1+-+Introduction+to+Exchange+Data+Model+and+Protocol+Snapshot+Q2)

**EU-EDM-02**

[Spezifikation von eDelivery AS4, dem Standard des EU-OOTS für Datenübermittlung, in der aktuellen Version](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery+AS4+-+1.15)

**EU-CB-01**

[Core Business Vocabulary](https://joinup.ec.europa.eu/collection/registered-organization-vocabulary/solution/core-business-vocabulary/release/200)

## Dokumente aus dem Kompetenzteam Architektur (Vorgänger des Programmbereich NOOTS)

**KT-Arch-003**

Prüffrage KT-Arch-003: Rechtliche Prüffrage KT-Architektur zur „Durchführung einer abstrakten Berechtigungsprüfung“

## Weiterführende Dokumente zur Registermodernisierung und sonstige Quellen

**SQ-01**

[Normenkontrollrat Faktencheck Registermodernisierung: Bericht des Normenkontrollrat zum Beschluss des Registermodernisierungsgesetzes](https://www.normenkontrollrat.bund.de/Webs/NKR/SharedDocs/Downloads/DE/Positionspapiere/faktencheck-registermodernisierungsgesetz.pdf?__blob=publicationFile)

**SQ-03**

[Gemeinsamer Arbeitsbereich Gesamtsteuerung Registermodernisierung](https://www.verwaltungskooperation.de/conf/display/RMPH/Gesamtsteuerung+Registermodernisierung)

**SQ-04**

[Bundesamt für Sicherheit in der Informationstechnik: IT-Grundschutz-Kompendium](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/IT-Grundschutz-Kompendium/it-grundschutz-kompendium_node.html)

**SQ-05**

[Europäische Kommission: Europäisches Interoperabilitätsrahmenwerk](https://eur-lex.europa.eu/resource.html?uri=cellar:2c2f2554-0faf-11e7-8a35-01aa75ed71a1.0017.02/DOC_1&format=PDF)

**SQ-06**

[Europäische Kommission: Interoperable Europe Act Proposal](https://commission.europa.eu/publications/interoperable-europe-act-proposal_en)

**SQ-07**

[Bundesamt für Sicherheit in der Informationstechnik: Technische Richtlinie BSI TR-03107 Elektronische Identitäten und Vertrauensdienste im E-Government](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03107/TR-03107_node.html)

## Rechtliche Grundlagen und Rahmenbedingungen

**RGR-01**

[Identifikationsnummerngesetz (IDNrG): Gesetz zur Einführung und Verwendung einer Identifikationsnummer in der öffentlichen Verwaltung und zur Änderung weiterer Gesetze](https://www.gesetze-im-internet.de/idnrg/IDNrG.pdf)

**RGR-02**

[Unternehmensbasisdatenregistergesetz (UBRegG): Gesetz zur Errichtung und Führung eines Registers über Unternehmensbasisdaten und zur Einführung einer bundeseinheitlichen Wirtschaftsnummer für Unternehmen und zur Änderung weiterer Gesetze](https://www.gesetze-im-internet.de/ubregg/UBRegG.pdf)

**RGR-03**

[Onlinezugangsgesetz (OZG): Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen](https://www.gesetze-im-internet.de/ozg/OZG.pdf)

**RGR-04**

[Registerzensuserprobungsgesetz (RegZensErpG): Gesetz zur Erprobung von Verfahren eines Registerzensus](https://www.gesetze-im-internet.de/regzenserpg/RegZensErpG.pdf)

**VS-GE**

[Entwurf eines Gesetzes zur Einführung und Verwendung einer Identifikationsnummer in der öffentlichen Verwaltung und zur Änderung weiterer Gesetze (Registermodernisierungsgesetz – RegMoG)](https://dserver.bundestag.de/btd/19/242/1924226.pdf)

## XÖV-Standards

**XS-01**

[Spezifikation des Standards XNachweis in der aktuellen Version](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xnachweis)

**XS-02**

[Spezifikation des Standards XDatenschutzcockpit in der aktuellen Version](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xdatenschutzcockpit)

**XS-03**

[Spezifikation des Standards XBasisdaten in der aktuellen Version](https://www.xrepository.de/details/urn:xoev-de:bva:standard:xbasisdaten)

**XS-04**

[Spezifikation des Standards XUnternehmen in der aktuellen Version](https://www.xrepository.de/details/urn:xoev-de:xunternehmen:standard:basismodul)

**XS-05**

[Spezifikation der XÖV-Kernkomponenten in der aktuellen Version](https://www.xoev.de/xoev/xoev-produkte/kernkomponenten-5065#:~:text=X%C3%96V-Kernkomponenten%20sind%20fach%C3%BCbergreifende%20Datenstrukturen%2C%20die%20die%20Grundlage%20f%C3%BCr,zur%20Abbildung%20von%20Anschriften%20oder%20Namen%20nat%C3%BCrlicher%20Personen.)

