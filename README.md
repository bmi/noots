## Herzlich Willkommen zum Konsultationsprozess zu ausgewählten Architekturdokumenten zum National-Once-Only-Technical-System (NOOTS)

### Ausgangslage und Zielsetzung


> Es handelt sich um einen Konsultationsprozess zu ausgewählten Architekturdokumenten zum National-Once-Only-Technical-System (NOOTS), der durch das Bundesministerium des Innern und für Heimat (BMI) im Rahmen des Gesamtprogramms Registermodernisierung durchgeführt wird. Der Architektur- und Entwicklungsprozess soll transparent und partizipativ von Ihnen mitgestaltet werden. Diese Plattform gibt Stakeholdern mit Bezug zur Registermodernisierung, Unternehmen, Expertinnen und Experten aus der Wissenschaft, Wirtschaft und Verwaltung die Möglichkeit, ihre Expertise und Erfahrungen einzubringen. Durch die frühzeitige Einbindung sollen wichtige Erkenntnisse für zukünftige Architekturen und für künftige Konsultations- und Beteiligungsprozesse gesammelt werden.   

**Bearbeitung der Leitfragen**

Um eine Bearbeitung der Leitfragen vorzunehmen, folgen Sie bitte vorab der Anmeldungsanleitung im nachstehenden [Kurzvideo](https://gitlab.opencode.de/bmi/noots/-/blob/main/Anleitungen/Account_Erstellen.mp4?ref_type=heads).

Weiterhin möchten wir Sie darauf aufmerksam machen, dass wir Ihr Feedback als wertvolle Ergänzung betrachten und dass wir dieses ausgiebig prüfen werden. Dennoch können wir nicht garantieren, dass jeder Vorschlag umgesetzt werden kann.



### [Link zu den FAQ's](https://gitlab.opencode.de/bmi/noots/-/blob/main/FAQ.md?ref_type=heads)

 
Diese FAQ's basieren auf dem Feedback, das im Rahmen des Konsultationsprozesses der AD-NOOTS eingegangen ist.  

### Dritte Iteration

Wir laden Sie herzlich zur Teilnahme an der 3. Iteration ein. 

In der dritten Iteration werden ab dem 12.03.2025 weitere Architekturdokumente unter https://gitlab.opencode.de/bmi/noots für die Kommentierung anhand von Leitfragen veröffentlicht.

Die folgenden Dokumente werden bereitgestellt:  

- High-Level-Architecture (HLA)
- Anschlusskonzept Data Consumer (DC)
- Anschlusskonzept Data Provider (DP)
- Grobkonzept Registerdatennavigation (RDN)
- ggfs. weitere Begleitdokumente

Es handelt sich dabei u. a. um fortgeschriebene  Architekturdokumente aus der ersten Iteration, in die Feedback aus dem Konsultationsprozess eingeflossen ist. 

Kommentierungszeitraum:
Die Dokumente stehen über einen Zeitraum vom 12.03.2025 bis 02.05.2025 zur Kommentierung zur Verfügung. Wir sind gespannt auf Ihre Rückmeldungen und hoffen, dass Sie auch in dieser Iteration wieder aktiv mitwirken.

Informationsveranstaltung:
Begleitend zu der Kommentierungsphase laden wir Sie am 12.03.2025 in der Zeit von 14:00 - 15:30 Uhr zu einer Informationsveranstaltung ein. Dort geben wir einen Überblick über die aktuellen Dokumente sowie über weitere Informationen rund um den Konsultationsprozess.


**Beitrittsinformationen**

Mittwoch, 12. März 2025            
14:00  |  (UTC+01:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien  |  1 Stunde 30 Minuten                           
Meeting beitreten <https://bva-bund.webex.com/bva-bund/j.php?MTID=me12fd40971de8811304bea9ee40cebc1>               

Weitere Methoden zum Beitreten:                                     

Meeting-Kennnummer (Zugriffscode): 2792 070 1753   
Meeting Passwort: FjThX$$2265 (35849002 beim Einwählen von einem Telefon)                    
+49-619-6781-9736,,27920701753#35849002# <tel:%2B49-619-6781-9736,,*01*27920701753%2335849002%23*01*>  Germany Toll                       

Über Telefon beitreten:
+49-619-6781-9736 Germany Toll           
Globale Einwahlnummern <https://bva-bund.webex.com/bva-bund/globalcallin.php?MTID=m7ed012f952207eef362569cf69f6edc2>                            

Über Videogerät oder -anwendung beitreten:   
Wählen Sie 27920701753@bva-bund.webex.com <sip:27920701753@bva-bund.webex.com>
Sie können auch 62.109.219.4 wählen und Ihre Meeting-Nummer eingeben.
              
Für Rückfragen wenden Sie sich bitte an registermodernisierung@bva.bund.de. Hilfestellungen zur Registrierung finden Sie unter https://gitlab.opencode.de/bmi/noots/-/wikis/home. 


### Zweite Iteration -  Rückblick  

Das Bundesministerium des Innern und für Heimat (BMI) schloss die zweite Iteration mit einer Informationsveranstaltung am 30.10.2024 ab. Neben der Feedbackauswertung wurde auch die Komponente Sicherer Anschlussknoten (SAK) kurz vorgestellt. Es wurden drei Architekturdokumente veröffentlicht:

- Grobkonzept Vermittlungsstelle (VS)
- Grobkonzept IAM für Behörden
- Grobkonzept Transportinfrastruktur (SAK)

Die Konsultantinnen und Konsultanten gaben rund 228 Kommentare ab. Wir bedanken uns bei allen Beteiligten für ihre wertvollen Beiträge.
 
Die Bearbeitung und Einordnung der Kommentare erfolgt durch ein Expertenteam im BMI und BVA, um die Anregungen und Kritik konstruktiv in die weitere Entwicklung der Architekturdokumentation einfließen zu lassen. Ziel ist es, die Dokumentation weiter zu optimieren und sicherzustellen, dass alle relevanten Perspektiven angemessen berücksichtigt werden.

Die Präsentation der Infoveranstaltung entnehmen Sie dem Ordner "Veranstaltungen".




### Erste Iteration - Rückblick


Die erste Iteration des Konsultationsprozesses zu ausgewählten Architekturdokumenten zum National-Once-Only-Technical-System ist erfolgreich beendet. Es wurden fünf Architekturdokumente veröffentlicht:


- High-Level-Architecture (HLA)
- Anschlusskonzept Data Consumer (DC)
- Anschlusskonzept Data Provider (DP)
- Grobkonzept Intermediäre Plattform (IP)
- Grobkonzept Registerdatennavigation (RDN)


Mittels auf dieser Plattform liegenden Leitfragen konnte entsprechendes Feedback zu den Dokumenten abgegeben werden. Die Konsultantinnen und Konsultanten gaben rund 220 Kommentare ab. Dabei wurden verschiedene Aspekte angesprochen, darunter der Abstraktionsgrad der Dokumente, sowie Fragen zum Verständnis und zu Details. In einem anschließenden Review unserer Expertinnen und Experten wird basierend auf Ihrer Rückmeldung iterativ die Weiterentwicklung der Dokumente vorgenommen.


Sollten weitere Fragen zum Konsultationsprozess bestehen, kontaktieren Sie bitte **registermodernisierung@bva.bund.de** 