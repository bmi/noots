## 2-Faktor-Authentifizierung ohne Smartphone

Sollten Sie die Registrierung ohne ein zweites mobiles Endgerät (z.B. Smartphone) vornehmen wollen, folgen Sie der nachstehenden Anleitung.

![Verifikation](./assets/image1.png)

Um den Vorgang ohne Smartphone fortzuführen, folgen Sie der nachstehenden Anleitung.

![Scannen](./assets/image2.png)

Sollten Sie keine Smartphone-App besitzen oder diese nicht nutzen wollen, dann klicken Sie bitte auf „Sie können den Barcode nicht scannen?“.

![Nicht-Scannen](./assets/image3.png)

Bitte speichern Sie den Schlüssel. Diesen benötigen Sie für beispielsweise KeePass, um einen One-time-Code zu erhalten. Den One-time-Code generieren Sie für jede Anmeldung neu.


## Anmeldung und Zwei-Faktor-Authentifizierung mit der App „KeePass“

Die [Kee-Passwort Manager Anwendung (KeePass)](https://chromewebstore.google.com/detail/kee-password-manager/mmhlniccooihdimnnjhamobppdhaolme?hl=de&utm_source=ext_sidebar) ist eine kostenlose Opensource-Erweiterung/App, die Sie sich aus dem Chrome Webstore herunterladen können. 


![Aufrufen](./assets/image4.png)

![Text4](./assets/Text4.png)

![DatenBankAnlegen](./assets/image5.png)

![Text5](./assets/Text5.png)

![KeePass Datenbank anlegen](./assets/image6.png)

![Text6](./assets/Text6.png)

![KeePass Datenbank anlegen2](./assets/image7.png)

![Text7](./assets/Text7.png)

![Zugangsdaten](./assets/image8.png)

![Text8](./assets/Text8.png)

![ZweitenFaktorHinzufügen](./assets/Text9.png)

![VerwendungDesZweitenFaktors](./assets/image10.png)

![Text10](./assets/Text10.png)